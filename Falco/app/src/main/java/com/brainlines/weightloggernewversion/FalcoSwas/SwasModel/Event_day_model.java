package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

import java.util.Date;

public class Event_day_model {
    Date AssignedDate;
    String user_appoint_id;
    String selected_user_id;
    String CommonName;
    String LOCATIONNAME;
    String ServiceEngineerDetailsID,OEMID,Service_Call_ID,CustomerID,ServiceEngineerID,AssignedTime,Note,CreatedBy,CreatedDate,UpdatedBy,IsActive,UpdatedDate;

    public String getCommonName() {
        return CommonName;
    }

    public void setCommonName(String commonName) {
        CommonName = commonName;
    }

    public String getLOCATIONNAME() {
        return LOCATIONNAME;
    }

    public void setLOCATIONNAME(String LOCATIONNAME) {
        this.LOCATIONNAME = LOCATIONNAME;
    }

    public Date getAssignedDate() {
        return AssignedDate;
    }

    public void setAssignedDate(Date assignedDate) {
        AssignedDate = assignedDate;
    }

    public String getUser_appoint_id() {
        return user_appoint_id;
    }

    public void setUser_appoint_id(String user_appoint_id) {
        this.user_appoint_id = user_appoint_id;
    }

    public String getSelected_user_id() {
        return selected_user_id;
    }

    public void setSelected_user_id(String selected_user_id) {
        this.selected_user_id = selected_user_id;
    }

    public String getServiceEngineerDetailsID() {
        return ServiceEngineerDetailsID;
    }

    public void setServiceEngineerDetailsID(String serviceEngineerDetailsID) {
        ServiceEngineerDetailsID = serviceEngineerDetailsID;
    }

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getService_Call_ID() {
        return Service_Call_ID;
    }

    public void setService_Call_ID(String service_Call_ID) {
        Service_Call_ID = service_Call_ID;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getServiceEngineerID() {
        return ServiceEngineerID;
    }

    public void setServiceEngineerID(String serviceEngineerID) {
        ServiceEngineerID = serviceEngineerID;
    }

    public String getAssignedTime() {
        return AssignedTime;
    }

    public void setAssignedTime(String assignedTime) {
        AssignedTime = assignedTime;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getUpdatedBy() {
        return UpdatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        UpdatedBy = updatedBy;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String getUpdatedDate() {
        return UpdatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        UpdatedDate = updatedDate;
    }
}
