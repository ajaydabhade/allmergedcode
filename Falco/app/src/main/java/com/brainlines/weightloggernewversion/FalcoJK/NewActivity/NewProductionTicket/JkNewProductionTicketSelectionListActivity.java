package com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.ProductionTicketistWithoutSelection_Adapter;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.SpinnerAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.Model.NewProductionTicketModel;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.adapter.NewProductionSelectionAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.listener.NewProductionTicketListener;
import com.brainlines.weightloggernewversion.FalcoJK.UrlConstants.JK_URL_Constants;
import com.brainlines.weightloggernewversion.FalcoJK.activity.FalcoJKHomeActivity;
import com.brainlines.weightloggernewversion.FalcoJK.activity.JK_ProductionTicket_Update_Actvity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

@SuppressLint("Registered")
public class JkNewProductionTicketSelectionListActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, NewProductionTicketListener {

    Spinner spin_pro_Release,spin_ticket;
    RecyclerView rv_produtcion_list,rv_pro_all_tk_list;
    NewProductionSelectionAdapter adapter;
    ImageView img_back,img_use_info,nav_view_img;
    DrawerLayout drawer;
    String user_email,user_role,user_token;
    String tickettype,str_active_close_ticket;
    ProductionTicketistWithoutSelection_Adapter productionTicketistWithoutSelection_adapter;
    SwipeRefreshLayout swipeto_refresh;
    private TextView profile_user,profile_role;
    DataBaseHelper helper;
    SQLiteDatabase db;
    String ticket_release,str_tk_no;
    ArrayList<NewProductionTicketModel> newProductionTicketModelArrayList= new ArrayList<>();
    private String ticket_active;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_production_ticket_selection_navigation);
        setData();
        helper = new DataBaseHelper(JkNewProductionTicketSelectionListActivity.this);
        db = helper.getWritableDatabase();
        rv_produtcion_list = findViewById(R.id.rv_production_ticket_list);
        swipeto_refresh = findViewById(R.id.swipeto_refresh);
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img = findViewById(R.id.nav_view_img);
        initUi();
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i=new Intent(JkNewProductionTicketSelectionListActivity.this, Falco_Home_Main_Activity.class);
//                startActivity(i);
            }
        });

        img_use_info = findViewById(R.id.img_userinfo);
        img_use_info.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_swas).setVisible(false);


        final ArrayList<String> arrayticket_type = new ArrayList<>();
        arrayticket_type.add("Select Released");
        arrayticket_type.add("ReleasedTicket");
        arrayticket_type.add("UnReleasedTicket");
        SpinnerAdapter arrayAdapter = new SpinnerAdapter(this, arrayticket_type);
        spin_pro_Release.setAdapter(arrayAdapter);

        final ArrayList<String> arrayticket = new ArrayList<>();
        arrayticket.add("Select Ticket");
        arrayticket.add("ActiveTicket");
        arrayticket.add("ClosedTicket");
        SpinnerAdapter arrayAdapter1 = new SpinnerAdapter(this, arrayticket);
        spin_ticket.setAdapter(arrayAdapter1);

        spin_pro_Release.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i!=0) {
                    ticket_release = arrayticket_type.get(i).toString();
                    newProductionTicketModelArrayList.clear();
                    if (ticket_release.equals("ReleasedTicket")) {
                        ticket_release = "true";
                    } else {
                        ticket_release = "false";
                    }
                    Cursor tickettypecursor = helper.getProductionReleaseTickets(ticket_release);
                    while (tickettypecursor.moveToNext()) {
                        NewProductionTicketModel model = new NewProductionTicketModel();
                        model.setPtkd_PktId1(tickettypecursor.getString(0));
                        model.setPtkd_PktId(tickettypecursor.getString(1));
                        model.setPtkd_TkNo(tickettypecursor.getString(2));
                        model.setPtkd_PSKU(tickettypecursor.getString(3));
                        model.setPtkd_Plant(tickettypecursor.getString(4));
                        model.setPtkd_ValueStreamCode(tickettypecursor.getString(5));
                        model.setPtkd_Ftypecode(tickettypecursor.getString(6));
                        model.setPtkd_Qty(tickettypecursor.getString(7));
                        model.setPtkd_IsRelease(tickettypecursor.getString(8));
                        model.setTk_Actual_Start_Date(tickettypecursor.getString(9));
                        model.setTk_Actual_End_Date(tickettypecursor.getString(10));
                        model.setPtkd_IsRelease1(tickettypecursor.getString(11));
                        model.setPtkd_ReleaseDate(tickettypecursor.getString(12));
                        model.setPtkd_Active(tickettypecursor.getString(13));
                        model.setTk_End_Date(tickettypecursor.getString(14));
                        newProductionTicketModelArrayList.add(model);
                    }
                    adapter=new NewProductionSelectionAdapter(JkNewProductionTicketSelectionListActivity.this,newProductionTicketModelArrayList,JkNewProductionTicketSelectionListActivity.this);
                    rv_produtcion_list.setLayoutManager(new LinearLayoutManager(JkNewProductionTicketSelectionListActivity.this,LinearLayoutManager.VERTICAL, false));
                    rv_produtcion_list.setAdapter(adapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        spin_ticket.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i!=0)
                {
                    str_active_close_ticket=arrayticket.get(i).toString();
                    if (str_active_close_ticket.equals("ActiveTicket"))
                    {
                        ticket_active = "true";
                    }
                    else
                    {
                        ticket_active = "false";

                    }
                    newProductionTicketModelArrayList.clear();
                    Cursor tickettypecursor = helper.getProductionActiveTickets(ticket_active);
                    while (tickettypecursor.moveToNext()) {
                        NewProductionTicketModel model = new NewProductionTicketModel();
                        model.setPtkd_PktId1(tickettypecursor.getString(0));
                        model.setPtkd_PktId(tickettypecursor.getString(1));
                        model.setPtkd_TkNo(tickettypecursor.getString(2));
                        model.setPtkd_PSKU(tickettypecursor.getString(3));
                        model.setPtkd_Plant(tickettypecursor.getString(4));
                        model.setPtkd_ValueStreamCode(tickettypecursor.getString(5));
                        model.setPtkd_Ftypecode(tickettypecursor.getString(6));
                        model.setPtkd_Qty(tickettypecursor.getString(7));
                        model.setPtkd_IsRelease(tickettypecursor.getString(8));
                        model.setTk_Actual_Start_Date(tickettypecursor.getString(9));
                        model.setTk_Actual_End_Date(tickettypecursor.getString(10));
                        model.setPtkd_IsRelease1(tickettypecursor.getString(11));
                        model.setPtkd_ReleaseDate(tickettypecursor.getString(12));
                        model.setPtkd_Active(tickettypecursor.getString(13));
                        model.setTk_End_Date(tickettypecursor.getString(14));
                        newProductionTicketModelArrayList.add(model);
                    }
                    adapter=new NewProductionSelectionAdapter(JkNewProductionTicketSelectionListActivity.this,newProductionTicketModelArrayList,JkNewProductionTicketSelectionListActivity.this);
                    rv_produtcion_list.setLayoutManager(new LinearLayoutManager(JkNewProductionTicketSelectionListActivity.this,LinearLayoutManager.VERTICAL, false));
                    rv_produtcion_list.setAdapter(adapter);

                }


            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        swipeto_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeto_refresh.setRefreshing(false);
            }
        });

    }

    public void initUi()
    {
        spin_pro_Release = findViewById(R.id.spin_pro_release);
        spin_ticket = findViewById(R.id.spin_pro_ticket);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(JkNewProductionTicketSelectionListActivity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);
        setData();
        // Set content width and height
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(JkNewProductionTicketSelectionListActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }

    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(JkNewProductionTicketSelectionListActivity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);
        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();
                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout()
    {
        finish();
        Toast toast= Toast.makeText(JkNewProductionTicketSelectionListActivity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(JkNewProductionTicketSelectionListActivity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);

        //Preferences.Logout();

    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id==R.id.nav_planning)
        {
            Intent i=new Intent(JkNewProductionTicketSelectionListActivity.this, FalcoJKHomeActivity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    @Override
    public void isupdateClicked(NewProductionTicketModel model) {
        if (model!=null)
        {
            Intent i = new Intent(JkNewProductionTicketSelectionListActivity.this, JK_ProductionTicket_Update_Actvity.class);
            //i.putExtra("model",model);
            Bundle bun = new Bundle();
            bun.putString("ptk_tk_id",model.getPtkd_PktId());
            bun.putString("tk_no",model.getPtkd_TkNo());
            bun.putString("tk_start_date",model.getTk_Actual_Start_Date());
            bun.putString("tk_end_date",model.getTk_Actual_End_Date());
            bun.putString("tk_sku",model.getPtkd_PSKU());
            bun.putString("tk_plant",model.getPtkd_Plant());
            bun.putString("tk_quantity", model.getPtkd_Qty());
            bun.putString("ptyk_Active",model.getPtkd_Active());
            bun.putString("filetype",model.getPtkd_Ftypecode());
            bun.putString("plant",model.getPtkd_Plant());
            bun.putString("valuestream",model.getPtkd_ValueStreamCode());
            i.putExtras(bun);
            startActivity(i);
        }
    }

    @Override
    public void isreleaseClicked(NewProductionTicketModel model, TextView txtreleasedate, ImageButton btnrelease, ImageButton btnupdate, ImageButton btnview, ImageView imagerelease) {

        if (model!=null)
        {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
            String str_tk_release_date = sdf.format(c.getTime());
            String[] string =str_tk_release_date.split(" ");
            String main_string_release_date=string[0];
            txtreleasedate.setText(main_string_release_date);
            btnrelease.setVisibility(View.GONE);
            btnupdate.setEnabled(true);
            btnview.setEnabled(true);
            imagerelease.setVisibility(View.GONE);
            str_tk_no = model.getPtkd_TkNo();

            helper.updateProdStartEndReleasedateinDatabase(model.getPtkd_TkNo(),main_string_release_date,"Releasedate");
            new ReleaseProductionTkCall().execute();

        }
    }

    @Override
    public void isviewClicked(NewProductionTicketModel model) {

    }

    public class ReleaseProductionTkCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JkNewProductionTicketSelectionListActivity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String str_created_by="capacity@decintell.com";
            String url= JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.RELEASE_PROD_TICKETS + str_tk_no + "&tk_CreatedBy=" + str_created_by;
            response= URL_Constants.makeHttpPutRequest(url);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try
                { JSONObject object=new JSONObject(response);
                    String data=object.getString("data");
                    String status=object.getString("status");
                    String message=object.getString("message");

                    if (status.equals("200"))
                    {
                        Toast.makeText(JkNewProductionTicketSelectionListActivity.this, "Successfully Released", Toast.LENGTH_SHORT).show();

                    }

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

        }
    }
}