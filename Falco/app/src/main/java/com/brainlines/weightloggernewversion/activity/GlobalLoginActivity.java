package com.brainlines.weightloggernewversion.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModelToStoreInDB;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.ModuleSelectionActivity;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.Constants;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class GlobalLoginActivity extends AppCompatActivity implements View.OnClickListener {

    private final String TAG = "ConnectionCall";
    Button btn_login;
    EditText ed_username,ed_password;
    String str_username,str_password;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_global_login);
        initUi();
        btn_login.setOnClickListener(this);
        ed_username.setOnClickListener(this);
        ed_password.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view.getId()==ed_username.getId())
        {
            ed_username.setCursorVisible(true);
        }
        if (view.getId()==ed_password.getId())
        {
            ed_password.setCursorVisible(true);
        }
        if (view.getId()==btn_login.getId())
        {
           /* Intent i=new Intent(LoginActivity.this,Falco_Home_Main_Activity.class);
            startActivity(i);*/
            str_username=ed_username.getText().toString();
            str_password=ed_password.getText().toString();

            if (TextUtils.isEmpty(str_username))
            {
                //ed_username.setError("Please enter username");
            }
            else if (TextUtils.isEmpty(str_password))
            {
                //ed_password.setError("Please enter password");
            }
            else {
                new LoginCall().execute();
                //new DecryptedApi().execute();
            }

        } }

    public void initUi()
    {
        ed_username=(EditText)findViewById(R.id.edt_enter_user_name);
        ed_password=(EditText)findViewById(R.id.edt_enter_password);
        btn_login=(Button)findViewById(R.id.btn_login);
    }

    public class DecryptedApi extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(GlobalLoginActivity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String str_de_password;
            str_de_password=ed_password.getText().toString();
            String url2= URL_Constants.encrypted + str_de_password;
            response= URL_Constants.makeHttpPostRequest(url2);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (response.length()==0){
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Invalid username & password", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                    else {
                        str_password=jsonObject.getString("Password");
                        new LoginCall().execute();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    public class LoginCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(GlobalLoginActivity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String getResponse="";
            try {
                HttpParams httpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(httpParams, 60000);
                HttpConnectionParams.setSoTimeout(httpParams, 60000);
                HttpClient client = new DefaultHttpClient(httpParams);
                HttpPost httpPost = new HttpPost(URL_Constants.LoginApi);
                httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
               /* try {
                    String password = "password";
                    String encryptedMsg = AESCrypt.encrypt(password, str_password);
                    str_password = encryptedMsg;
                }catch (GeneralSecurityException e){
                    //handle error
                }
                str_password = Base64.encodeToString( str_password.getBytes(), Base64.DEFAULT );*/

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("grant_type","password"));
                nameValuePairs.add(new BasicNameValuePair("username",str_username));
                //nameValuePairs.add(new BasicNameValuePair("password",str_password));
                nameValuePairs.add(new BasicNameValuePair("password","UJkGMEMpSWXJnOKrd9bxYSVnXF0uQ7zKFzGlygZdKl8="));
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                httpPost.setParams(httpParams);

                HttpResponse response = client.execute(httpPost);
                StatusLine statusLine = response.getStatusLine();

                if (statusLine.getStatusCode() == HttpStatus.SC_OK || statusLine.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();

                    getResponse = URL_Constants.convertStreamToString(inputStream);
                }
                else {
                    response.getEntity().getContent().close();
                }
            }
            catch (UnknownHostException e) {
                Log.i(TAG, "Error Message in UnknownHostException :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_UNKNOWN_HOST_EXCEPTION;

            }
            catch (ConnectTimeoutException e) {
                Log.i(TAG, "Error Message in ConnectTimeoutException :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_CONNECTION_TIMEOUT_EXCEPTION;


            }
            catch (ClientProtocolException e) {
                Log.i(TAG, "Error Message in ClientProtocolException :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_CLIENT_PROTOCOL_EXCEPTION;

            }
            catch (IOException e) {
                Log.i(TAG, "Error Message in IOException :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_IO_EXCEPTION;

            }
            catch (Exception e) {
                Log.i(TAG, "Error Message in Exception :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_EXCEPTION;

            }
            return getResponse;
        }
        @Override
        protected void onPostExecute(String getResponse) {
            super.onPostExecute(getResponse);
            progressDialog.dismiss();
            if (getResponse.contains("error"))
            {
                Toast.makeText(GlobalLoginActivity.this, "Invalid username & password", Toast.LENGTH_SHORT).show();
                ed_username.getText().clear();
                ed_password.getText().clear();
            }
            else {
                if (!getResponse.equals("Error"))
                {
                    try {
                        JSONObject jsonObject=new JSONObject(getResponse);
                        String token=jsonObject.getString("access_token");
                        String token_type=jsonObject.getString("token_type");
                        String user_emailid=jsonObject.getString("UserEmailId");
                        String user_role=jsonObject.getString("RoleName");
                        String oemid=jsonObject.getString("OEMID");
                        UserModelToStoreInDB model = new UserModelToStoreInDB(jsonObject.getString("access_token"),
                                jsonObject.getString("token_type"),jsonObject.getString("OEMID"),
                                jsonObject.getString("UserEmailId"),jsonObject.getString("RoleName"));

//                        DataBaseHelper helper=new DataBaseHelper(GlobalLoginActivity.this);
//                        SQLiteDatabase db = helper.getWritableDatabase();
//                        ContentValues cv=new ContentValues();
//                        cv.put("UserToken",token);
//                        cv.put("UserTokenType",token_type);
//                        cv.put("UserEmailID",user_emailid);
//                        cv.put("UserOEMID",oemid);
//                        cv.put("UserRoleName",user_role);
//
//                        long d=db.insert(Constants.TABLE_USER_DETAILS,null,cv);
//                        Log.d("user check", String.valueOf(d));
                        Intent i=new Intent(GlobalLoginActivity.this, ModuleSelectionActivity.class);
                        i.putExtra("token",token);
                        i.putExtra("email_id",user_emailid);
                        i.putExtra("user_role",user_role);
                        i.putExtra("oemid",oemid);
                        i.putExtra("UserModel",model);

                        startActivity(i);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }

        }
    }
}
