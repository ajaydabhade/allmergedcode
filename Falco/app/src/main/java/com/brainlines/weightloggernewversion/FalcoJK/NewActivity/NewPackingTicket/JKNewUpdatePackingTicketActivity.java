package com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewPackingTicket;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.PacketTicketTrackingModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.PackingTicketDatewiseQuantityModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.StartTicketPackModel;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewPackingTicket.Listener.NewUpdatePackingTicketListener;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewPackingTicket.Model.NewPackingTicketModel;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewPackingTicket.adapter.UpdatePackingTicketAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.UrlConstants.JK_URL_Constants;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class JKNewUpdatePackingTicketActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener, NewUpdatePackingTicketListener {

    String str_tk_id,str_tk_no,str_tk_startdate,str_tk_end_date,str_tk_qty,str_plant,str_sku;
    TextView txt_up_ticket_no,txt_up_plant,txt_up_tk_qty,txt_sku_13,txt_up_startdate,txt_up_enddate,txt_up_act_strat_date,txt_up_act_end_date;
    RecyclerView rv_update_pack_Data,rv_add_day_data;
    ImageButton img_start_ticket,img_end_ticket,img_add_day;
    TextView txt_up_starttk,txt_up_end_ticket,txt_up_add_Day;
    UpdatePackingTicketAdapter updatePackTicketRvAdapter;
    ImageView img_back,img_user_info,nav_view_img;
    DrawerLayout drawer;
    String user_email,user_role,user_token,str_approved_by,str_created_by,str_approve;
    ArrayList<PacketTicketTrackingModel> ticketmodel= new ArrayList<>();
    SwipeRefreshLayout swipeto_refresh;
    String dateformat,add_day_date,str_total_qty,str_pendingqty,str_coml_qty,cal_pendingqty;
    String end_pending_qty;
    private TextView profile_user,profile_role;
    DataBaseHelper helper;
    SQLiteDatabase db;
    Calendar c = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
    NewPackingTicketModel model;
    ArrayList<PackingTicketDatewiseQuantityModel> packingTicketDatewiseQuantityModelArrayList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_packing_ticket_update_navigation);
        initUi();
        model = getIntent().getParcelableExtra("model");
        assert model != null;
        txt_sku_13.setText(model.getTk_SKU());
        txt_up_ticket_no.setText(model.getTk_TicketNo());
        txt_up_plant.setText(model.getTk_Plant());
        txt_up_tk_qty.setText(model.getTk_Quantity());
        txt_up_startdate.setText(model.getTk_Start_Date());
        txt_up_enddate.setText(model.getTk_End_Date());
        txt_up_act_strat_date.setText(model.getTk_Actual_start_date());
        txt_up_act_end_date.setText(model.getTk_Actual_end_date());
        str_tk_no = model.getTk_TicketNo();

        helper = new DataBaseHelper(JKNewUpdatePackingTicketActivity.this);
        db = helper.getWritableDatabase();
        setData();
        getDatewiseQuantity();


        swipeto_refresh=(SwipeRefreshLayout)findViewById(R.id.swipeto_refresh);
        swipeto_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ticketmodel.clear();
                //new GetTicketDetailsCall().execute();
                swipeto_refresh.setRefreshing(false);
            }
        });
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        dateformat = sdf.format(c.getTime());
    }

    private void initUi()
    {
        txt_up_ticket_no = findViewById(R.id.txt_up_ticket_no);
        txt_up_plant = findViewById(R.id.txt_up_plant);
        txt_up_tk_qty = findViewById(R.id.txt_up_tk_qty);
        txt_sku_13 = findViewById(R.id.txt_sku_13);
        txt_up_startdate = findViewById(R.id.txt_up_startdate);
        txt_up_enddate = findViewById(R.id.txt_up_enddate);
        txt_up_act_strat_date = findViewById(R.id.txt_up_act_strat_date);
        txt_up_act_end_date = findViewById(R.id.txt_up_act_end_date);
        rv_update_pack_Data = findViewById(R.id.rv_update_pack_data);
        txt_up_starttk = findViewById(R.id.txt_up_starttk);
        rv_add_day_data = findViewById(R.id.rv_add_day_data);

        img_start_ticket = findViewById(R.id.img_start_ticket);
        img_end_ticket = findViewById(R.id.img_end_ticket);
        img_add_day = findViewById(R.id.img_add_day);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_swas).setVisible(false);
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img=(ImageView)findViewById(R.id.nav_view_img);
        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });

        Bundle bundle=getIntent().getExtras();
        str_tk_id=bundle.getString("tk_id");
        str_tk_no=bundle.getString("tk_no");
        str_tk_startdate=bundle.getString("tk_start_date");
        str_tk_end_date=bundle.getString("tk_end_date");
        str_tk_qty=bundle.getString("tk_quantity");
        str_plant=bundle.getString("tk_plant");
        str_sku = bundle.getString("tk_sku_13");
        img_back = findViewById(R.id.img_back);
        img_user_info = findViewById(R.id.img_userinfo);

        txt_up_add_Day = findViewById(R.id.txt_up_add_Day);
        txt_up_end_ticket = findViewById(R.id.txt_up_end_ticket);
        txt_up_starttk = findViewById(R.id.txt_up_starttk);


        img_add_day.setOnClickListener(this);
        img_start_ticket.setOnClickListener(this);
        img_end_ticket.setOnClickListener(this);
        img_back.setOnClickListener(this);
        img_user_info.setOnClickListener(this);
        txt_up_starttk.setOnClickListener(this);
        txt_up_end_ticket.setOnClickListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(JKNewUpdatePackingTicketActivity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);
        setData();

        TextView profile_user = popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role = popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout = popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel =  popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(JKNewUpdatePackingTicketActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }

    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(JKNewUpdatePackingTicketActivity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();

                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout()
    {
        finish();
        Toast toast= Toast.makeText(JKNewUpdatePackingTicketActivity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(JKNewUpdatePackingTicketActivity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_start_ticket:

                String str_tk_release_date = sdf.format(c.getTime());
                String[] string =str_tk_release_date.split(" ");
                String main_string_start_date=string[0];
                txt_up_act_strat_date.setText(main_string_start_date);
                helper.updateStartdateinDatabase(str_tk_no,main_string_start_date,"Startdate");
                img_start_ticket.setVisibility(View.INVISIBLE);
                txt_up_starttk.setVisibility(View.INVISIBLE);
                PackingTicketDatewiseQuantityModel model2=new PackingTicketDatewiseQuantityModel();
                ArrayList<PackingTicketDatewiseQuantityModel> models=new ArrayList<>();
                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                String str_date = df.format(Calendar.getInstance().getTime());
                model2.setToday(str_date);
                model2.setTt_PendingQty(txt_up_tk_qty.getText().toString());
                model2.setTk_End_Date(model.getTk_End_Date());
                model2.setTt_id(model.getTk_Id());
                model2.setTk_SKU(model.getTk_SKU());
                model2.setTt_TicketID(model.getTk_Id());
                model2.setTt_TicketNo(model.getTk_TicketNo());
                model2.setTt_Plant(model.getTk_Plant());
                model2.setActualenddate(model.getTk_Actual_end_date());
                model2.setTt_Qty(model.getTotalqty());
                model2.setActualstartdate(model.getTk_Actual_start_date());
                model2.setTk_Start_Date(model.getTk_Start_Date());
                model2.setTt_Active(model.getTk_Active());
                model2.setTt_ApprovedBy(model.getTk_Approved_by());
                model2.setTt_CreatedBy(model.getTk_Created_by());
                model2.setTt_ApprovedDate(model.getTk_Approved_date());
                models.add(model2);
                String pendingqty = txt_up_tk_qty.getText().toString();
                updatePackTicketRvAdapter.refreshData1(models,str_tk_id,str_tk_no,rv_update_pack_Data,pendingqty);
                break;

            case R.id.img_end_ticket:
                String str_tk_end_date = sdf.format(c.getTime());
                String[] stringend =str_tk_end_date.split(" ");
                String stringenddate=stringend[0];
                txt_up_act_end_date.setText(stringenddate);
                helper.EndTicketinPackingDatabase(str_tk_no,stringenddate,"Enddate");

            case R.id.img_add_day:
                PackingTicketDatewiseQuantityModel model1=new PackingTicketDatewiseQuantityModel();
                ArrayList<PackingTicketDatewiseQuantityModel> models1=new ArrayList<>();
                SimpleDateFormat df1 = new SimpleDateFormat("dd-MMM-yyyy");
                String str_date1 = df1.format(Calendar.getInstance().getTime());
                model1.setToday(str_date1);
                model1.setTt_PendingQty(model1.getTt_PendingQty());
                model1.setTk_End_Date(model1.getTk_End_Date());
                models1.add(model1);
                updatePackTicketRvAdapter.refreshData(models1,str_tk_id,str_tk_no,rv_update_pack_Data);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    public void getDatewiseQuantity()
    {
        Cursor getdatewisequantity = helper.getPackingTicketdaywisequantity(str_tk_no);
        while (getdatewisequantity.moveToNext())
        {
            PackingTicketDatewiseQuantityModel model = new PackingTicketDatewiseQuantityModel();
            model.setTt_id(getdatewisequantity.getString(1));
            model.setTt_TicketID(getdatewisequantity.getString(1));
            model.setTt_TicketNo(getdatewisequantity.getString(2));
            model.setTt_Plant(getdatewisequantity.getString(3));
            model.setTt_Date(getdatewisequantity.getString(4));
            model.setTt_TotalQuantity(getdatewisequantity.getString(5));
            model.setTt_Qty(getdatewisequantity.getString(6));
            model.setTt_PendingQty(getdatewisequantity.getString(7));
            model.setTt_CreatedDate(getdatewisequantity.getString(8));
            model.setTt_CreatedBy(getdatewisequantity.getString(9));
            model.setTt_ApprovedDate(getdatewisequantity.getString(10));
            model.setTt_ApprovedBy(getdatewisequantity.getString(11));
            model.setTt_Active(getdatewisequantity.getString(12));
            model.setTt_Approve(getdatewisequantity.getString(13));
            model.setFlag1(getdatewisequantity.getString(14));
            model.setTk_SKU(getdatewisequantity.getString(15));
            model.setTk_Start_Date(getdatewisequantity.getString(16));
            model.setTk_End_Date(getdatewisequantity.getString(17));
            model.setActualstartdate(getdatewisequantity.getString(18));
            model.setToday(getdatewisequantity.getString(19));
            model.setActualenddate(getdatewisequantity.getString(20));
            packingTicketDatewiseQuantityModelArrayList.add(model);

            String actualenddate = getdatewisequantity.getString(20);

            txt_up_act_strat_date.setText(getdatewisequantity.getString(19));
            txt_up_act_end_date.setText(getdatewisequantity.getString(20));
            if (getdatewisequantity.getString(20)!=null)
            {
                img_add_day.setVisibility(View.INVISIBLE);
                txt_up_add_Day.setVisibility(View.INVISIBLE);
                img_start_ticket.setVisibility(View.INVISIBLE);
                txt_up_act_strat_date.setVisibility(View.INVISIBLE);
                img_end_ticket.setVisibility(View.INVISIBLE);
                txt_up_act_end_date.setVisibility(View.INVISIBLE);
            }


        }
        updatePackTicketRvAdapter=new UpdatePackingTicketAdapter(JKNewUpdatePackingTicketActivity.this,packingTicketDatewiseQuantityModelArrayList,JKNewUpdatePackingTicketActivity.this);
        rv_update_pack_Data.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL, false));
        rv_update_pack_Data.setAdapter(updatePackTicketRvAdapter);

    }


    @Override
    public void imgactionSubmitIsClicked(StartTicketPackModel model) {

    }

    @Override
    public void imgactintrackingSubmit(PackingTicketDatewiseQuantityModel model) {
        if (model != null) {
            {
                str_tk_id = model.getTt_id();
                str_tk_no = model.getTt_TicketNo();
                str_plant=model.getTt_Plant();
                add_day_date=model.getTt_Date();
                /*str_total_qty=model.getTt_TotalQuantity();*/
                cal_pendingqty = model.getTt_PendingQty();
                str_coml_qty = model.getTt_Qty();
                cal_pendingqty= URL_Constants.substraction(cal_pendingqty,str_coml_qty);


                ContentValues cv=new ContentValues();
                cv.put("tt_id",model.getTt_id());
                cv.put("tt_TicketID",model.getTt_TicketID());
                cv.put("tt_TicketNo",model.getTt_TicketNo());
                cv.put("tt_Plant",model.getTt_Plant());
                cv.put("tt_Date",model.getToday());
                cv.put("tt_TotalQuantity",model.getTt_TotalQuantity());
                cv.put("tt_Qty",model.getTt_Qty());
                cv.put("tt_PendingQty",model.getTt_PendingQty());
                cv.put("tt_CreatedDate",model.getTt_CreatedDate());
                cv.put("tt_CreatedBy",model.getTt_CreatedBy());
                cv.put("tt_ApprovedDate",model.getTt_ApprovedDate());
                cv.put("tt_ApprovedBy",model.getTt_ApprovedBy());
                cv.put("tt_Active",model.getTt_Active());
                cv.put("tt_Approve",model.getTt_Approve());
                cv.put("flag1",model.getFlag1());
                cv.put("tk_SKU",model.getTk_SKU());
                cv.put("tk_Start_Date",model.getTk_Start_Date());
                cv.put("tk_End_Date",model.getTk_End_Date());
                cv.put("actualstartdate",model.getActualstartdate());
                cv.put("today",model.getToday());
                cv.put("actualenddate",model.getActualenddate());


                String tt_id = model.getTt_TicketID();

                String q = "SELECT * FROM "+Constants.DATE_WISE_QUANTITY_PACKING_TABLE+" where "+Constants.DATE_WISE_QUANTITY_PACKING_TICKET_ID+"='"+tt_id+"'";
                Cursor c = db.rawQuery(q,null);
                if(c.moveToFirst())
                {
                    //showMessage("Error", "Record exist");
                }
                else
                {
                    long d=db.insert(Constants.DATE_WISE_QUANTITY_PACKING_TABLE,null,cv);
                    new SavePackingTicketTrackingCall().execute();

                    Log.d("user check", String.valueOf(d));
                }

            }
        }

    }

    public class SavePackingTicketTrackingCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JKNewUpdatePackingTicketActivity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String str_user_id="capacity@decintell.com";

            str_pendingqty=URL_Constants.substraction(str_coml_qty,cal_pendingqty);
            String url1= JK_URL_Constants.MAIN_JK_URL;
            String url2=url1 + JK_URL_Constants.FIRST_TIME_SAVE_PACKUNPACKTK + str_tk_id + "&tt_TicketNo=" + str_tk_no + "&tt_Plant=" + "CHNM" + "&tt_Date=" + add_day_date
                    + "&tt_TotalQuantity=" + str_total_qty + "&tt_Qty=" + str_coml_qty + "&tt_PendingQty=" + str_pendingqty + "&tt_CreatedBy="
                    + str_user_id + "&tt_ApprovedBy=" + str_user_id + "&tt_Approve=" + "N";

            response= URL_Constants.makeHttpPostRequest(url2);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try {
                    JSONObject object=new JSONObject(response);
                    String status=object.getString("data");
                    String abc=object.getString("status");
                    if (abc.equals("true"))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(JKNewUpdatePackingTicketActivity.this);

                        //builder.setTitle("Confirm");
                        builder.setMessage("You successfully inserted the quantity");

                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {

                                // Do nothing, but close the dialog
                                dialog.dismiss();
                                //new JK_Packing_Tk_Update_Activity.GetTicketDetailsCall().execute();
                            }
                        });
                    }

                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }

            }
        }
    }

}