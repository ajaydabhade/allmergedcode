package com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.InProcessCall;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnCalendarPageChangeListener;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.CompletedCall.Swas_Details_Of_Completed_Call;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Home_Actvity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.Event_day_model;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.model.ScheduledVisitsModel;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.android.material.navigation.NavigationView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants.convertStreamToString;
import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.getInstance;

public class ScheduledVisitActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "ScheduledVisitActivity";
    ArrayList<Event_day_model> event_day_modelArrayList =new ArrayList<>();
    @SuppressLint("SimpleDateFormat")
    final SimpleDateFormat myformat = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    private final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy");
    LinearLayout enter_visit_details,display_visit_details;
    private Calendar _calendar;
    int year, month, dayofmonth;
    private static final String tag = "Scheduled Visit";
    TextView txt_note,txtcreated_by,txtcustomer_name,txt_service_call_id;
    ImageButton img_btn_schedule_visit;
    String status_id,service_call_id,type_of_service_status_id,customer_id,oem_id,string_start_date,string_end_date;
    ImageView img_user_info,nav_view_img,img_back;
    String user_email,user_token,user_role,created_by,service_eng_id;
    DrawerLayout drawer;
    TextView profile_user,profile_role;
    Date start_Cal_date;
    Date end_Cal_date;
    List<Date> dates = new ArrayList<Date>();
    String first_Date;
    String currentDate,locationName;
    DateFormat dateFormat = new SimpleDateFormat("E, MMMM dd, yyyy HH:mm:ss.SSS aa");
    private DataBaseHelper helper;
    String dummyScheduledVisitData = "{\"data\":[{\"ServiceEngineerDetailsID\":85,\"OEMID\":\"1234\",\"Service_Call_ID\":32,\"CustomerID\":\"C000442\",\"ServiceEngineerID\":\"431934cb-ce4c-415d-92ad-4b91c5d1bba5\",\"IsAssigned\":\"1\",\"IsAccepted\":\"1\",\"AssignedDate\":\"22/06/2020\",\"AssignedTime\":\"\",\"Note\":\"\",\"CreatedBy\":\"Deepak Kulkarni\",\"CreatedDate\":\"23/06/2020\",\"UpdatedBy\":\"Deepak Kulkarni\",\"IsActive\":\"1         \",\"UpdatedDate\":\"23/06/2020\",\"LOCATIONNAME\":\"Hatsun Agro Products Ltd. - Thalaivasal\",\"CommonName\":\"C000442 - Hatsun Agro Products Ltd. - Thalaivasal\"},{\"ServiceEngineerDetailsID\":86,\"OEMID\":\"1234\",\"Service_Call_ID\":32,\"CustomerID\":\"C000442\",\"ServiceEngineerID\":\"431934cb-ce4c-415d-92ad-4b91c5d1bba5\",\"IsAssigned\":\"1\",\"IsAccepted\":\"1\",\"AssignedDate\":\"23/06/2020\",\"AssignedTime\":\"\",\"Note\":\"\",\"CreatedBy\":\"Deepak Kulkarni\",\"CreatedDate\":\"23/06/2020\",\"UpdatedBy\":\"Deepak Kulkarni\",\"IsActive\":\"1         \",\"UpdatedDate\":\"23/06/2020\",\"LOCATIONNAME\":\"Hatsun Agro Products Ltd. - Thalaivasal\",\"CommonName\":\"C000442 - Hatsun Agro Products Ltd. - Thalaivasal\"},{\"ServiceEngineerDetailsID\":87,\"OEMID\":\"1234\",\"Service_Call_ID\":27,\"CustomerID\":\"C000004\",\"ServiceEngineerID\":\"431934cb-ce4c-415d-92ad-4b91c5d1bba5\",\"IsAssigned\":\"1\",\"IsAccepted\":\"1\",\"AssignedDate\":\"25/06/2020\",\"AssignedTime\":\"\",\"Note\":\"\",\"CreatedBy\":\"Deepak Kulkarni\",\"CreatedDate\":\"23/06/2020\",\"UpdatedBy\":\"Deepak Kulkarni\",\"IsActive\":\"1         \",\"UpdatedDate\":\"23/06/2020\",\"LOCATIONNAME\":\"Jalani Enterprises Pvt. Ltd.\",\"CommonName\":\"C000004 - Jalani Enterprises Pvt. Ltd.\"},{\"ServiceEngineerDetailsID\":88,\"OEMID\":\"1234\",\"Service_Call_ID\":27,\"CustomerID\":\"C000004\",\"ServiceEngineerID\":\"431934cb-ce4c-415d-92ad-4b91c5d1bba5\",\"IsAssigned\":\"1\",\"IsAccepted\":\"1\",\"AssignedDate\":\"26/06/2020\",\"AssignedTime\":\"\",\"Note\":\"\",\"CreatedBy\":\"Deepak Kulkarni\",\"CreatedDate\":\"23/06/2020\",\"UpdatedBy\":\"Deepak Kulkarni\",\"IsActive\":\"1         \",\"UpdatedDate\":\"23/06/2020\",\"LOCATIONNAME\":\"Jalani Enterprises Pvt. Ltd.\",\"CommonName\":\"C000004 - Jalani Enterprises Pvt. Ltd.\"},{\"ServiceEngineerDetailsID\":91,\"OEMID\":\"1234\",\"Service_Call_ID\":33,\"CustomerID\":\"C002574\",\"ServiceEngineerID\":\"431934cb-ce4c-415d-92ad-4b91c5d1bba5\",\"IsAssigned\":\"1\",\"IsAccepted\":\"1\",\"AssignedDate\":\"29/06/2020\",\"AssignedTime\":\"2020/06/29\",\"Note\":\"\",\"CreatedBy\":\"Deepak Kulkarni\",\"CreatedDate\":\"24/06/2020\",\"UpdatedBy\":\"Deepak Kulkarni\",\"IsActive\":\"1         \",\"UpdatedDate\":\"24/06/2020\",\"LOCATIONNAME\":\"M/s JASMER FOODS PVT LTD\",\"CommonName\":\"C002574 - M/s JASMER FOODS PVT LTD\"},{\"ServiceEngineerDetailsID\":85,\"OEMID\":\"1234\",\"Service_Call_ID\":32,\"CustomerID\":\"C000442\",\"ServiceEngineerID\":\"431934cb-ce4c-415d-92ad-4b91c5d1bba5\",\"IsAssigned\":\"1\",\"IsAccepted\":\"1\",\"AssignedDate\":\"20/05/2020\",\"AssignedTime\":\"\",\"Note\":\"\",\"CreatedBy\":\"Deepak Kulkarni\",\"CreatedDate\":\"20/05/2020\",\"UpdatedBy\":\"Deepak Kulkarni\",\"IsActive\":\"1         \",\"UpdatedDate\":\"23/05/2020\",\"LOCATIONNAME\":\"Hatsun Agro Products Ltd. - Thalaivasal\",\"CommonName\":\"C000442 - Hatsun Agro Products Ltd. - Thalaivasal\"},{\"ServiceEngineerDetailsID\":86,\"OEMID\":\"1234\",\"Service_Call_ID\":32,\"CustomerID\":\"C000442\",\"ServiceEngineerID\":\"431934cb-ce4c-415d-92ad-4b91c5d1bba5\",\"IsAssigned\":\"1\",\"IsAccepted\":\"1\",\"AssignedDate\":\"15/05/2020\",\"AssignedTime\":\"\",\"Note\":\"\",\"CreatedBy\":\"Deepak Kulkarni\",\"CreatedDate\":\"15/05/2020\",\"UpdatedBy\":\"Deepak Kulkarni\",\"IsActive\":\"1         \",\"UpdatedDate\":\"20/05/2020\",\"LOCATIONNAME\":\"Hatsun Agro Products Ltd. - Thalaivasal\",\"CommonName\":\"C000442 - Hatsun Agro Products Ltd. - Thalaivasal\"},{\"ServiceEngineerDetailsID\":87,\"OEMID\":\"1234\",\"Service_Call_ID\":27,\"CustomerID\":\"C000004\",\"ServiceEngineerID\":\"431934cb-ce4c-415d-92ad-4b91c5d1bba5\",\"IsAssigned\":\"1\",\"IsAccepted\":\"1\",\"AssignedDate\":\"05/07/2020\",\"AssignedTime\":\"\",\"Note\":\"\",\"CreatedBy\":\"Deepak Kulkarni\",\"CreatedDate\":\"23/06/2020\",\"UpdatedBy\":\"Deepak Kulkarni\",\"IsActive\":\"1         \",\"UpdatedDate\":\"23/06/2020\",\"LOCATIONNAME\":\"Jalani Enterprises Pvt. Ltd.\",\"CommonName\":\"C000004 - Jalani Enterprises Pvt. Ltd.\"},{\"ServiceEngineerDetailsID\":88,\"OEMID\":\"1234\",\"Service_Call_ID\":27,\"CustomerID\":\"C000004\",\"ServiceEngineerID\":\"431934cb-ce4c-415d-92ad-4b91c5d1bba5\",\"IsAssigned\":\"1\",\"IsAccepted\":\"1\",\"AssignedDate\":\"10/07/2020\",\"AssignedTime\":\"\",\"Note\":\"\",\"CreatedBy\":\"Deepak Kulkarni\",\"CreatedDate\":\"23/06/2020\",\"UpdatedBy\":\"Deepak Kulkarni\",\"IsActive\":\"1         \",\"UpdatedDate\":\"23/06/2020\",\"LOCATIONNAME\":\"Jalani Enterprises Pvt. Ltd.\",\"CommonName\":\"C000004 - Jalani Enterprises Pvt. Ltd.\"},{\"ServiceEngineerDetailsID\":91,\"OEMID\":\"1234\",\"Service_Call_ID\":33,\"CustomerID\":\"C002574\",\"ServiceEngineerID\":\"431934cb-ce4c-415d-92ad-4b91c5d1bba5\",\"IsAssigned\":\"1\",\"IsAccepted\":\"1\",\"AssignedDate\":\"29/08/2020\",\"AssignedTime\":\"2020/06/29\",\"Note\":\"\",\"CreatedBy\":\"Deepak Kulkarni\",\"CreatedDate\":\"24/06/2020\",\"UpdatedBy\":\"Deepak Kulkarni\",\"IsActive\":\"1         \",\"UpdatedDate\":\"24/06/2020\",\"LOCATIONNAME\":\"M/s JASMER FOODS PVT LTD\",\"CommonName\":\"C002574 - M/s JASMER FOODS PVT LTD\"}],\"statusCode\":\"200\",\"error\":null,\"status\":true,\"message\":\"Success\"}";
    private ArrayList<ScheduledVisitsModel> assignedDateList = new ArrayList<>();
    private List<Calendar> calendarList = new ArrayList<>();
    private CalendarView calendarView;
    private List<EventDay> events = new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swas_scheduled_visit_navigation);
        setData();
        helper = new DataBaseHelper(this);
        currentDate = dateFormat.format(new Date());
        img_back=(ImageView)findViewById(R.id.img_back);
        calendarView = (CalendarView) findViewById(R.id.calendarView);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ScheduledVisitActivity.this, Swas_Home_Actvity.class);
                Bundle bundle = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_service_status_id);
                bundle.putString("customer_id",customer_id);
                i.putExtras(bundle);
                startActivity(i);
                finish();
            }
        });
        if (user_email.equals("yogesh@brainlines.in"))
        {
            created_by = "Yogesh Jadhav";
//            service_eng_id = "1";
        }
        else
        {
            created_by = "Deepak Kulkarni";
//            service_eng_id = "2";
        }
        img_user_info=(ImageView)findViewById(R.id.img_userinfo);
        img_user_info.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img=(ImageView)findViewById(R.id.nav_view_img);

        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_planning).setVisible(false);
        //txt_Assign_time = (TextView)findViewById(R.id.txt_Assign_time);
        txt_service_call_id = (TextView)findViewById(R.id.txt_service_call_id);
        txt_note = (TextView)findViewById(R.id.txt_note);
        txtcreated_by = (TextView)findViewById(R.id.txtcreated_by);
        txtcustomer_name = (TextView)findViewById(R.id.txtcustomer_name);
        img_btn_schedule_visit = (ImageButton)findViewById(R.id.img_btn_schedule_visit);
        enter_visit_details = (LinearLayout)findViewById(R.id.enter_visit_details);
        display_visit_details = (LinearLayout)findViewById(R.id.display_visit_details);
        img_btn_schedule_visit.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
        {
            status_id = bundle.getString("status_id");
            service_call_id = bundle.getString("service_call_id");
            txt_service_call_id.setText(service_call_id);
            type_of_service_status_id = bundle.getString("Typeofservicecall_id");
            customer_id = bundle.getString("customer_id");
            locationName = bundle.getString("location_name");
        }

        if (status_id.equals("5"))
        {
            enter_visit_details.setVisibility(View.GONE);
        }

        _calendar = Calendar.getInstance(Locale.getDefault());month = _calendar.get(Calendar.MONTH) + 1;
        year = _calendar.get(Calendar.YEAR);
        Log.d(tag, "Calendar Instance:= " + "Month: " + month + " " + "Year: "
                + year);

        Calendar c = getInstance();
        c.set(DAY_OF_MONTH, 1);

        first_Date = myformat.format(c.getTime());

        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();

        Log.d(TAG, "onCreate: ");

        setScheduledVisitsToCalender();
    }

    private void setScheduledVisitsToCalender() {
        try {
            calendarList = getSelectedDays();
            calendarView.setSelectedDates(calendarList);
            Calendar cal = Calendar.getInstance();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        for (int i = 0 ; i < calendarList.size() ; i++){
            Calendar calendar = calendarList.get(i);
            events.add(new EventDay(calendar, R.drawable.maintenance));
        }
        calendarView.setEvents(events);

        calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                Date date = null;
                SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
                String temp = eventDay.getCalendar().getTime().toString();
                try {
                    date = formatter.parse(temp);
                    String formateDate = new SimpleDateFormat("dd/MM/yyyy").format(date);
                    getDataFromJsonObjectAccTODate(formateDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private ArrayList<ScheduledVisitsModel> getAlreadyBookedDataFromLocaDB() {
        Cursor cursor = helper.getDataFromScheduledVisitLocalDB();
        while (cursor.moveToNext()){
            int ServiceEngineerDetailsID = cursor.getInt(1);
            String OEMID = cursor.getString(2);
            int Service_Call_ID = cursor.getInt(3);
            String CustomerID = cursor.getString(4);
            String ServiceEngineerID = cursor.getString(5);
            String IsAssigned = cursor.getString(6);
            String IsAccepted = cursor.getString(7);
            String AssignedDate = cursor.getString(8);
            String AssignedTime = cursor.getString(9);
            String Note = cursor.getString(10);
            String CreatedBy = cursor.getString(11);
            String CreatedDate = cursor.getString(12);
            String UpdatedBy = cursor.getString(13);
            String IsActive = cursor.getString(14);
            String UpdatedDate = cursor.getString(15);
            String LOCATIONNAME = cursor.getString(16);
            String CommonName = cursor.getString(17);
            assignedDateList.add(new ScheduledVisitsModel(ServiceEngineerDetailsID, OEMID, Service_Call_ID, CustomerID, ServiceEngineerID, IsAssigned, IsAccepted,AssignedDate, AssignedTime, Note, CreatedBy, CreatedDate,UpdatedBy, IsActive, UpdatedDate, LOCATIONNAME, CommonName
            ));
        }
        return assignedDateList;
    }

    private void getDataFromJsonObjectAccTODate(String formateDate) {
        for (ScheduledVisitsModel model : getAlreadyBookedDataFromLocaDB()){
            if (model != null){
                if (formateDate.equals(model.getAssignedDate())){
                    txtcustomer_name.setText(model.getCommonName());
                    txtcreated_by.setText(model.getCreatedBy());
                    txt_note.setText(model.getNote());
                }else {

                }
            }else {
            }
        }
    }

    private List<Calendar> getSelectedDays() throws ParseException {
        SimpleDateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        List<Calendar> calendars = new ArrayList<>();
        for (ScheduledVisitsModel model : getAlreadyBookedDataFromLocaDB()){
            Date date = inputFormat.parse(model.getAssignedDate());
            String formattedDate = outputFormat.format(date);
            Date opDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(formattedDate);
            Calendar cal = Calendar.getInstance();
            cal.setTime(opDate);
            calendars.add(cal);
        }
        return calendars;
    }

    @Override
    public void onClick(View v) {
        if (v==img_btn_schedule_visit)
        {
            ScheduledVisit();
        }
    }

    private void ScheduledVisit() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.alert_scheduled_visit, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        ImageButton img_btn = (ImageButton) dialogView.findViewById(R.id.img_btn_btn_visit);
        final EditText ed_alert_start_date = (EditText)dialogView.findViewById(R.id.ed_alert_start_date);
        final EditText ed_alert_end_date = (EditText)dialogView.findViewById(R.id.ed_alert_end_date);

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        ed_alert_end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar myCalendar = Calendar.getInstance();
                DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        year = Calendar.YEAR;
                        month = Calendar.MONTH;
                        dayofmonth = Calendar.DAY_OF_MONTH;
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "yyyy/MM/dd"; // your format
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                        start_Cal_date = myCalendar.getTime();
                        ed_alert_end_date.setText(sdf.format(myCalendar.getTime()));
                    }
                };
                new DatePickerDialog(ScheduledVisitActivity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        ed_alert_start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar myCalendar = Calendar.getInstance();
                DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        year = Calendar.YEAR;
                        month = Calendar.MONTH;
                        dayofmonth = Calendar.DAY_OF_MONTH;
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "yyyy/MM/dd"; // your format
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                        end_Cal_date = myCalendar.getTime();

                        ed_alert_start_date.setText(sdf.format(myCalendar.getTime()));
                    }
                };
                new DatePickerDialog(ScheduledVisitActivity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        img_btn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View view) {
                /*DateFormat formatter ;
                formatter = new SimpleDateFormat("yyyy/MM/dd");
                Date startDate = null;
                try {
                    startDate = (Date)formatter.parse(string_start_date);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                Date endDate = null;
                try {
                    endDate = (Date)formatter.parse(string_end_date);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                long interval = 24*1000 * 60 * 60; // 1 hour in millis
                long endTime =endDate.getTime() ; // create your endtime here, possibly using Calendar or Date
                long curTime = startDate.getTime();
                while (curTime <= endTime) {
                    dates.add(new Date(curTime));
                    curTime += interval;
                }
                for(int i=0;i<dates.size();i++){
                    Date lDate =(Date)dates.get(i);
                    String ds = formatter.format(lDate);
                    System.out.println(" Date is ..." + ds);
                }*/
                try {
                    alertDialog.dismiss();
                    dates.clear();
                    string_start_date = ed_alert_start_date.getText().toString();
                    string_end_date = ed_alert_end_date.getText().toString();
                    SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy/MM/dd");
                    SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    List<Calendar> calendars = new ArrayList<>();
                    Date startDate = inputFormat.parse(string_start_date);
                    Date endDate = inputFormat.parse(string_end_date);
                    for (Date date : getDatesBetweenStartDateAndEndDate(startDate,endDate)){
                        String formattedDate = outputFormat.format(date);
                        Date opDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(formattedDate);
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(opDate);
                        calendars.add(cal);
                    }
                    calendarList.addAll(calendars);

                    for (int i = 0 ; i < calendarList.size() ; i++){
                        Calendar calendar = calendarList.get(i);
                        events.add(new EventDay(calendar, R.drawable.maintenance));
                    }
                    calendarView.setSelectedDates(calendarList);
                    calendarView.setEvents(events);
                    if (TextUtils.isEmpty(string_start_date)) {
                        ed_alert_start_date.setError("Please select the start date");
                    } else if (TextUtils.isEmpty(string_end_date)) {
                        ed_alert_end_date.setError("Please select the end date");
                    } else
                    {
//                        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
//                        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
//                        boolean isConnected = activeNetwork != null &&
//                                activeNetwork.isConnected();
//                        if(isConnected) {
//                            Log.d("Network", "Connected");
//
//                            new Insert_Scheduled_Visit().execute();
//                            //save_scheduled_visit_call();
//
//                        }
//                        else{
//                            checkNetworkConnection();
//                            Log.d("Network","Not Connected");
//                            insertScheduledVisitInLocalDb(string_start_date,string_end_date);
//                        }
                        insertScheduledVisitInLocalDb(string_start_date,string_end_date);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public List<Date> getDatesBetweenStartDateAndEndDate(Date startDate, Date endDate) {
        List<Date> datesInRange = new ArrayList<>();
        Calendar start = Calendar.getInstance();
        start.setTime(startDate);
        Calendar end = Calendar.getInstance();
        end.setTime(endDate);
        end.add(Calendar.DAY_OF_YEAR, 1);

        while (start.before(end)) {
            datesInRange.add(start.getTime());
            start.add(Calendar.DAY_OF_YEAR, 1);
        }
        return datesInRange;
    }

    public class Insert_Scheduled_Visit extends AsyncTask<String,String,String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ScheduledVisitActivity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String getResponse="";
            //String created_by = "Yogesh Jadhav";

            String url= Retroconfig.BASEURL_SWAS + "updateassigneddatesbyserviceengineer";

            try {
                HttpParams httpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(httpParams, 60000);
                HttpConnectionParams.setSoTimeout(httpParams, 60000);
                HttpClient client = new DefaultHttpClient(httpParams);
                HttpPost httpPost = new HttpPost(url);

                httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
                JSONArray array=new JSONArray();

               /* for (int i= 0; i<dates.size();i++)
                {
                    String myFormat = "yyyy/MM/dd"; // your format
                    DateFormat formatter ;

                    formatter = new SimpleDateFormat("yyyy/MM/dd");
                    String date = formatter.format(dates.get(i));

                    object.put("OEMID",oem_id);
                    object.put("Service_Call_ID",service_call_id);
                    object.put("CustomerID",customer_id);
                    object.put("ServiceEngineerID","1");
                    object.put("IsAssigned","1");
                    object.put("IsAccepted","1");
                    object.put("AssignedDate",date);
                    object.put("AssignedTime","10.00");
                    object.put("Note","dsff");
                    object.put("CreatedBy",created_by);
                    object.put("UpdatedBy",created_by);
                    array.put(object);

                }*/

                JSONObject object=new JSONObject();
                object.put("OEMID",oem_id);
                object.put("Service_Call_ID",service_call_id);
                object.put("CustomerID",customer_id);
                object.put("ServiceEngineerID",service_eng_id);
                object.put("IsAssigned","1");
                object.put("IsAccepted","1");
                object.put("AssignedDate",string_start_date);
                object.put("AssignedTime","");
                object.put("Note","");
                object.put("CreatedBy",created_by);
                object.put("UpdatedBy",created_by);
                array.put(object);

                JSONObject object1=new JSONObject();
                object1.put("OEMID",oem_id);
                object1.put("Service_Call_ID",service_call_id);
                object1.put("CustomerID",customer_id);
                object1.put("ServiceEngineerID",service_eng_id);
                object1.put("IsAssigned","1");
                object1.put("IsAccepted","1");
                object1.put("AssignedDate",string_end_date);
                object1.put("AssignedTime","");
                object1.put("Note","");
                object1.put("CreatedBy",created_by);
                object1.put("UpdatedBy",created_by);
                array.put(object1);

                httpPost.setEntity(new StringEntity(array.toString(), "UTF-8"));
                httpPost.setParams(httpParams);

                HttpResponse response = client.execute(httpPost);
                StatusLine statusLine = response.getStatusLine();

                if (statusLine.getStatusCode() == HttpStatus.SC_OK || statusLine.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();

                    getResponse = convertStreamToString(inputStream);
                } else if (statusLine.getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();

                    getResponse = convertStreamToString(inputStream);
                } else {
                    response.getEntity().getContent().close();
                }
            }
            catch (Exception e) {
                Log.i(URL_Constants.TAG, "Error Message in Exception :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_EXCEPTION;

            }
            return getResponse;
        }

        @Override
        protected void onPostExecute(String getResponse) {
            super.onPostExecute(getResponse);
            progressDialog.dismiss();
            if (!getResponse.equals("Error"))
            {
                Toast.makeText(ScheduledVisitActivity.this, "Successfully inserted", Toast.LENGTH_SHORT).show();

                Intent i = new Intent(ScheduledVisitActivity.this,ScheduledVisitActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_service_status_id);
                bundle.putString("customer_id",customer_id);
                i.putExtras(bundle);
                startActivity(i);
            }
        }
    }

    private void insertScheduledVisitInLocalDb(String strStartDate,String strEndDate) throws ParseException {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        List<Calendar> calendars = new ArrayList<>();
        Date startDate = inputFormat.parse(strStartDate);
        Date endDate = inputFormat.parse(strEndDate);
        for (Date date : getDatesBetweenStartDateAndEndDate(startDate,endDate)){
            DataBaseHelper helper=new DataBaseHelper(ScheduledVisitActivity.this);
            SQLiteDatabase db = helper.getWritableDatabase();
            ContentValues cv=new ContentValues();
            cv.put("ServiceEngineerDetailsID",service_eng_id);
            cv.put("OEMID",oem_id);
            cv.put("CustomerID",customer_id);
            cv.put("Service_Call_ID",service_call_id);
            cv.put("ServiceEngineerID",service_eng_id);
            cv.put("IsAssigned","1");
            cv.put("IsAccepted","1");
            cv.put("AssignedDate",inputFormat.format(date));
            cv.put("AssignedTime",string_start_date);
            cv.put("Note","");
            cv.put("CreatedBy",created_by);
            cv.put("CreatedDate", currentDate);
            cv.put("UpdatedBy",created_by);
            cv.put("IsActive","1");
            cv.put("UpdatedDate",currentDate);
            cv.put("LOCATIONNAME",locationName);
            cv.put("CommonName",customer_id +" - "+locationName);
            cv.put("IsSync","0");

            long d=db.insert(Constants.TABLE_SCHEDULED_DATES,null,cv);
            Log.d("user check", String.valueOf(d));

            String formattedDate = outputFormat.format(date);
            Date opDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(formattedDate);
            Calendar cal = Calendar.getInstance();
            cal.setTime(opDate);
            calendars.add(cal);
        }
        calendarList.addAll(calendars);

        for (int i = 0 ; i < calendarList.size() ; i++){
            Calendar calendar = calendarList.get(i);
            events.add(new EventDay(calendar, R.drawable.maintenance));
        }
//        calendarView.setSelectedDates(calendarList);
        calendarView.setEvents(events);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(ScheduledVisitActivity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);
        setData();
        // Set content width and height
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(ScheduledVisitActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }

    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(ScheduledVisitActivity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();

                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    oem_id= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));
                    service_eng_id = cursor.getString(cursor.getColumnIndex(Constants.USER_LOGIN_ID));

                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout() {
        finish();
        Toast toast= Toast.makeText(ScheduledVisitActivity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(ScheduledVisitActivity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);

        //Preferences.Logout();

    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id==R.id.nav_swas)
        {
            Intent i=new Intent(ScheduledVisitActivity.this, Swas_Home_Actvity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    public void checkNetworkConnection(){
        AlertDialog.Builder builder =new AlertDialog.Builder(ScheduledVisitActivity.this);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static List<Date> getDaysBetweenDates(Date startdate, Date enddate) {
        List<Date> dates = new ArrayList<Date>();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startdate);

        while (calendar.getTime().before(enddate))
        {
            Date result = calendar.getTime();
            dates.add(result);
            calendar.add(Calendar.DATE, 1);
        }
        return dates;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (status_id.equals("5")){
            Intent i= new Intent(ScheduledVisitActivity.this, Swas_Details_Of_Completed_Call.class);
            Bundle bundle = new Bundle();
            bundle.putString("service_call_id",service_call_id);
            bundle.putString("status_id",status_id);
            bundle.putString("Typeofservicecall_id",type_of_service_status_id);
            bundle.putString("customer_id",customer_id);
            i.putExtras(bundle);
            startActivity(i);
            finish();
        }else {
            Intent i= new Intent(ScheduledVisitActivity.this,Swas_Deatails_of_Inprocess_Call_Activity.class);
            Bundle bundle = new Bundle();
            bundle.putString("service_call_id",service_call_id);
            bundle.putString("status_id",status_id);
            bundle.putString("Typeofservicecall_id",type_of_service_status_id);
            bundle.putString("customer_id",customer_id);
            i.putExtras(bundle);
            startActivity(i);
            finish();
        }
    }
}
