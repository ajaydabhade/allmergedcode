package com.brainlines.weightloggernewversion.FalcoJK.MainAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.ProdTkOperationModel;
import com.brainlines.weightloggernewversion.FalcoJK.Listeners.JKProductionUpdateListener;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class NewProViewTicketAdapter extends RecyclerView.Adapter<NewProViewTicketAdapter.MyViewHolder> {
    public String TAG = "NewTicketListAdapter";
    Context context;
    ArrayList<ProdTkOperationModel> ticketList = new ArrayList<>();
    JKProductionUpdateListener jKProductionUpdateListener;

    String upedOkayValue,upedReworkValue,upedRejectValue,upedWipValue,upedSecondValue;

    public NewProViewTicketAdapter(Context context, ArrayList<ProdTkOperationModel> ticketList, JKProductionUpdateListener JKProductionUpdateListener) {
        this.context = context;
        this.ticketList = ticketList;
        this.jKProductionUpdateListener = JKProductionUpdateListener;
    }

    @NonNull

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_new_view_product,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final ProdTkOperationModel model = ticketList.get(position);
        if (model!=null){
            if (!model.getOpr_operation_name().equals("")){
                holder.operation.setText(model.getOpr_operation_name());
            }
            if (!model.getUni_unit_name().equals("")){
                holder.changeroute.setText(model.getUni_unit_name());
            }
            /*if (model.getPtkd_Qty()!=0){
                holder.txtTotalQty.setText(model.getPtkd_Qty() +"");
            }*/
            /*if (model.getVsCode()!=null){
                holder.vsCode.setText(model.getVsCode());
            }*/
            if (model.getPtk_OkQty()!=0){
                holder.upedokay.setText(model.getPtk_OkQty()+"");
            }
            if (model.getPtk_ReworkQty()!=0){
                holder.upedrework.setText(model.getPtk_ReworkQty()+"");
            }
            if (model.getPtk_RejectQty()!=0){
                holder.upedreject.setText(model.getPtk_RejectQty()+"");
            }
            if (model.getPtk_SecondQty()!=0){
                holder.upedsecond.setText(model.getPtk_SecondQty()+"");
            }
            if (model.getPtk_WIPQty()!=0){
                holder.upedwip.setText(model.getPtk_WIPQty()+"");
            }
           /* holder.img_route.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JKProductionUpdateListener.imgRouteIsClicked(true,model);
                }
            });*/

            /*holder.upedokay.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    upedOkayValue = s.toString();
                    if (s!=null){
                        ticketList.get(position).setPtk_OkQty_ans(upedOkayValue);
                    }else if (s==null){
                        ticketList.get(position).setPtk_OkQty_ans("0");
                    }
                }
            });
            holder.upedrework.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    upedReworkValue = s.toString();
                    if (s!=null){
                        ticketList.get(position).setPtk_ReworkQty_ans(upedReworkValue);
                    }else if (s==null){
                        ticketList.get(position).setPtk_ReworkQty_ans("0");
                    }
                }
            });
            holder.upedreject.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    upedRejectValue = s.toString();
                    if (s!=null){
                        ticketList.get(position).setPtk_RejectQty_ans(upedRejectValue);
                    }else if (s==null){
                        ticketList.get(position).setPtk_RejectQty_ans("0");
                    }
                }
            });
            holder.upedwip.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    upedWipValue = s.toString();
                    if (s!=null){
                        ticketList.get(position).setPtk_WIPQty_ans(upedWipValue);
                    }else if (s==null) {
                        ticketList.get(position).setPtk_WIPQty_ans("0");
                    }
                }
            });
            holder.upedsecond.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    upedSecondValue = s.toString();
                    if (s!=null){
                        ticketList.get(position).setPtk_SecondQty_ans(upedSecondValue);
                    }else if (s==null){
                        ticketList.get(position).setPtk_SecondQty_ans("0");
                    }
                }
            });
*/


            /*holder.img_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JKProductionUpdateListener.imgSubmitButtonIsClicked(ticketList);
                }
            });*/
        }
    }

    @Override
    public int getItemCount() {
        return ticketList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView operation,changeroute, vsCode,txtTotalQty;
        EditText upedokay,upedrework,upedreject,upedwip,upedsecond;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            operation = (itemView).findViewById(R.id.upopeartions);
            changeroute = (itemView).findViewById(R.id.up_current_route);
            vsCode = (itemView).findViewById(R.id.up_VS);
            txtTotalQty = (itemView).findViewById(R.id.txtTotalQty);

            upedokay = (itemView).findViewById(R.id.upedokay);
            upedrework = (itemView).findViewById(R.id.upedrework);
            upedreject = (itemView).findViewById(R.id.upedreject);
            upedwip = (itemView).findViewById(R.id.upedwip);
            upedsecond = (itemView).findViewById(R.id.upedsecond);

           /* img_submit = (itemView).findViewById(R.id.img_submit);
            img_route = (itemView).findViewById(R.id.img_route);*/
        }
    }
}
