package com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewPackingTicket.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class NewPackingTicketModel implements Parcelable {
    String ID,tk_Id, tk_TicketNo, tk_type, tk_SONO, tk_UploadRefNumber, tk_Plant, tk_SKU, tk_Prod_SKU, tk_Release_Date, tk_Start_Date, EDD, tk_End_Date, tk_Quantity, pln_Packing_Norms, tk_Unpaking_SKU, tk_Unpacking, MonthStart, MonthEnd, tk_Active, tk_Release, tk_produce_qty;
    String totalqty,tk_Created_date,tk_Created_by,tk_Approved_date,tk_Approved_by,tk_Actual_start_date,tk_Actual_end_date;

    protected NewPackingTicketModel(Parcel in) {
        ID = in.readString();
        tk_Id = in.readString();
        tk_TicketNo = in.readString();
        tk_type = in.readString();
        tk_SONO = in.readString();
        tk_UploadRefNumber = in.readString();
        tk_Plant = in.readString();
        tk_SKU = in.readString();
        tk_Prod_SKU = in.readString();
        tk_Release_Date = in.readString();
        tk_Start_Date = in.readString();
        EDD = in.readString();
        tk_End_Date = in.readString();
        tk_Quantity = in.readString();
        pln_Packing_Norms = in.readString();
        tk_Unpaking_SKU = in.readString();
        tk_Unpacking = in.readString();
        MonthStart = in.readString();
        MonthEnd = in.readString();
        tk_Active = in.readString();
        tk_Release = in.readString();
        tk_produce_qty = in.readString();
        totalqty = in.readString();
        tk_Created_date = in.readString();
        tk_Created_by = in.readString();
        tk_Approved_date = in.readString();
        tk_Approved_by = in.readString();
        tk_Actual_start_date = in.readString();
        tk_Actual_end_date = in.readString();
    }

    public static final Creator<NewPackingTicketModel> CREATOR = new Creator<NewPackingTicketModel>() {
        @Override
        public NewPackingTicketModel createFromParcel(Parcel in) {
            return new NewPackingTicketModel(in);
        }

        @Override
        public NewPackingTicketModel[] newArray(int size) {
            return new NewPackingTicketModel[size];
        }
    };

    public NewPackingTicketModel() {

    }

    public String getTk_Created_date() {
        return tk_Created_date;
    }

    public void setTk_Created_date(String tk_Created_date) {
        this.tk_Created_date = tk_Created_date;
    }

    public String getTk_Created_by() {
        return tk_Created_by;
    }

    public void setTk_Created_by(String tk_Created_by) {
        this.tk_Created_by = tk_Created_by;
    }

    public String getTk_Approved_date() {
        return tk_Approved_date;
    }

    public void setTk_Approved_date(String tk_Approved_date) {
        this.tk_Approved_date = tk_Approved_date;
    }

    public String getTk_Approved_by() {
        return tk_Approved_by;
    }

    public void setTk_Approved_by(String tk_Approved_by) {
        this.tk_Approved_by = tk_Approved_by;
    }

    public String getTk_Actual_start_date() {
        return tk_Actual_start_date;
    }

    public void setTk_Actual_start_date(String tk_Actual_start_date) {
        this.tk_Actual_start_date = tk_Actual_start_date;
    }

    public String getTk_Actual_end_date() {
        return tk_Actual_end_date;
    }

    public void setTk_Actual_end_date(String tk_Actual_end_date) {
        this.tk_Actual_end_date = tk_Actual_end_date;
    }

    public String getTotalqty() {
        return totalqty;
    }

    public void setTotalqty(String totalqty) {
        this.totalqty = totalqty;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getTk_Id() {
        return tk_Id;
    }

    public void setTk_Id(String tk_Id) {
        this.tk_Id = tk_Id;
    }

    public String getTk_TicketNo() {
        return tk_TicketNo;
    }

    public void setTk_TicketNo(String tk_TicketNo) {
        this.tk_TicketNo = tk_TicketNo;
    }

    public String getTk_type() {
        return tk_type;
    }

    public void setTk_type(String tk_type) {
        this.tk_type = tk_type;
    }

    public String getTk_SONO() {
        return tk_SONO;
    }

    public void setTk_SONO(String tk_SONO) {
        this.tk_SONO = tk_SONO;
    }

    public String getTk_UploadRefNumber() {
        return tk_UploadRefNumber;
    }

    public void setTk_UploadRefNumber(String tk_UploadRefNumber) {
        this.tk_UploadRefNumber = tk_UploadRefNumber;
    }

    public String getTk_Plant() {
        return tk_Plant;
    }

    public void setTk_Plant(String tk_Plant) {
        this.tk_Plant = tk_Plant;
    }

    public String getTk_SKU() {
        return tk_SKU;
    }

    public void setTk_SKU(String tk_SKU) {
        this.tk_SKU = tk_SKU;
    }

    public String getTk_Prod_SKU() {
        return tk_Prod_SKU;
    }

    public void setTk_Prod_SKU(String tk_Prod_SKU) {
        this.tk_Prod_SKU = tk_Prod_SKU;
    }

    public String getTk_Release_Date() {
        return tk_Release_Date;
    }

    public void setTk_Release_Date(String tk_Release_Date) {
        this.tk_Release_Date = tk_Release_Date;
    }

    public String getTk_Start_Date() {
        return tk_Start_Date;
    }

    public void setTk_Start_Date(String tk_Start_Date) {
        this.tk_Start_Date = tk_Start_Date;
    }

    public String getEDD() {
        return EDD;
    }

    public void setEDD(String EDD) {
        this.EDD = EDD;
    }

    public String getTk_End_Date() {
        return tk_End_Date;
    }

    public void setTk_End_Date(String tk_End_Date) {
        this.tk_End_Date = tk_End_Date;
    }

    public String getTk_Quantity() {
        return tk_Quantity;
    }

    public void setTk_Quantity(String tk_Quantity) {
        this.tk_Quantity = tk_Quantity;
    }

    public String getPln_Packing_Norms() {
        return pln_Packing_Norms;
    }

    public void setPln_Packing_Norms(String pln_Packing_Norms) {
        this.pln_Packing_Norms = pln_Packing_Norms;
    }

    public String getTk_Unpaking_SKU() {
        return tk_Unpaking_SKU;
    }

    public void setTk_Unpaking_SKU(String tk_Unpaking_SKU) {
        this.tk_Unpaking_SKU = tk_Unpaking_SKU;
    }

    public String getTk_Unpacking() {
        return tk_Unpacking;
    }

    public void setTk_Unpacking(String tk_Unpacking) {
        this.tk_Unpacking = tk_Unpacking;
    }

    public String getMonthStart() {
        return MonthStart;
    }

    public void setMonthStart(String monthStart) {
        MonthStart = monthStart;
    }

    public String getMonthEnd() {
        return MonthEnd;
    }

    public void setMonthEnd(String monthEnd) {
        MonthEnd = monthEnd;
    }

    public String getTk_Active() {
        return tk_Active;
    }

    public void setTk_Active(String tk_Active) {
        this.tk_Active = tk_Active;
    }

    public String getTk_Release() {
        return tk_Release;
    }

    public void setTk_Release(String tk_Release) {
        this.tk_Release = tk_Release;
    }

    public String getTk_produce_qty() {
        return tk_produce_qty;
    }

    public void setTk_produce_qty(String tk_produce_qty) {
        this.tk_produce_qty = tk_produce_qty;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ID);
        dest.writeString(tk_Id);
        dest.writeString(tk_TicketNo);
        dest.writeString(tk_type);
        dest.writeString(tk_SONO);
        dest.writeString(tk_UploadRefNumber);
        dest.writeString(tk_Plant);
        dest.writeString(tk_SKU);
        dest.writeString(tk_Prod_SKU);
        dest.writeString(tk_Release_Date);
        dest.writeString(tk_Start_Date);
        dest.writeString(EDD);
        dest.writeString(tk_End_Date);
        dest.writeString(tk_Quantity);
        dest.writeString(pln_Packing_Norms);
        dest.writeString(tk_Unpaking_SKU);
        dest.writeString(tk_Unpacking);
        dest.writeString(MonthStart);
        dest.writeString(MonthEnd);
        dest.writeString(tk_Active);
        dest.writeString(tk_Release);
        dest.writeString(tk_produce_qty);
        dest.writeString(totalqty);
        dest.writeString(tk_Created_date);
        dest.writeString(tk_Created_by);
        dest.writeString(tk_Approved_date);
        dest.writeString(tk_Approved_by);
        dest.writeString(tk_Actual_start_date);
        dest.writeString(tk_Actual_end_date);
    }
}
