package com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.CompletedCall;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.InProcessCall.ScheduledVisitActivity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Feedback_Activity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Home_Actvity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Service_Call_List_Activity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter.ServiceIdListAdapter;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.Checklist_Model;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.FaultyCharactristics_Model;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.PartOrderByCustomerModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasIdDetailsModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasServiceCallIdModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Swas_Details_Of_Completed_Call extends AppCompatActivity  implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    RelativeLayout rl_Details,rl_scheduled_visit,rl_visit_log,rl_machine_para,rl_competitors_para,rl_travelling_expen,rl_report;
    ImageButton cancel,feedback;
    String status_id,service_call_id,type_of_service_status_id,customer_id;
    TextView txt_customer_name,txt_location,txt_call_creadted,txt_created_on,txt_modified_on,txt_service_call_id;
    TextView numberOfVisitsValue,txt_rootCauseValue,txtDescriptionValue,dpartMentValue,
            txtLongTermResolTextValue,txtQERemarkValue,txtShortTextValue,SERemarkValue;
    FrameLayout documents;
    ImageView img_user_info,nav_view_img,img_back;
    String user_email,user_token,user_role;
    DrawerLayout drawer;
    TextView profile_user,profile_role;
    private ArrayList<Checklist_Model> serviceCallCheckListModels = new ArrayList<>();
    private ArrayList<PartOrderByCustomerModel> partOrderModels = new ArrayList<>();
    private ArrayList<FaultyCharactristics_Model> faultCharTextList = new ArrayList<>();
    DataBaseHelper helper;

    private ArrayList<SwasServiceCallIdModel> model = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swas_details_of_completed_call_navigation);

        helper  = new DataBaseHelper(this);

        setData();
        img_back=(ImageView)findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Swas_Details_Of_Completed_Call.this, Swas_Home_Actvity.class);
                startActivity(i);
                finish();
            }
        });

        img_user_info=(ImageView)findViewById(R.id.img_userinfo);
        img_user_info.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img=(ImageView)findViewById(R.id.nav_view_img);

        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_planning).setVisible(false);

        initUi();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
        {
            status_id = bundle.getString("status_id");
            service_call_id = bundle.getString("service_call_id");
            type_of_service_status_id = bundle.getString("Typeofservicecall_id");
            customer_id = bundle.getString("customer_id");
        }

        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
//        if(isConnected) {
//            Log.d("Network", "Connected");
////            Id_Details();
//            serviceId1();
//
//        }
//        else{
//            checkNetworkConnection();
//            Log.d("Network","Not Connected");
//            getDataFromListOfServiceCallByStatus();
//        }
        getDataFromListOfServiceCallByStatus();

        cancel.setVisibility(View.GONE);

        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Swas_Details_Of_Completed_Call.this, Swas_Feedback_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_service_status_id);
                bundle.putString("customer_id",customer_id);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    public void initUi() {
        documents = (FrameLayout)findViewById(R.id.documents);
        rl_Details = (RelativeLayout)findViewById(R.id.rl_Details);
        rl_scheduled_visit = (RelativeLayout)findViewById(R.id.rl_scheduled_visit);
        rl_visit_log = (RelativeLayout)findViewById(R.id.rl_visit_log);
        rl_machine_para = (RelativeLayout)findViewById(R.id.rl_machine_para);
        rl_competitors_para = (RelativeLayout)findViewById(R.id.rl_competitors_para);
        rl_travelling_expen = (RelativeLayout)findViewById(R.id.rl_travelling_expen);
        rl_report = (RelativeLayout)findViewById(R.id.rl_report);

        txt_customer_name = (TextView)findViewById(R.id.txt_customer_name);
        //txt_location = (TextView)findViewById(R.id.txt_location);
        txt_call_creadted = (TextView)findViewById(R.id.txt_call_creadted);
        txt_created_on = (TextView)findViewById(R.id.txt_created_on);
        txt_modified_on = (TextView)findViewById(R.id.txt_modified_on);
        txt_service_call_id = (TextView)findViewById(R.id.txt_service_call_id);
        numberOfVisitsValue = (TextView)findViewById(R.id.numberOfVisitsValue);
        txt_rootCauseValue = (TextView)findViewById(R.id.txt_rootCauseValue);
        txtDescriptionValue = (TextView)findViewById(R.id.txtDescriptionValue);
        dpartMentValue = (TextView)findViewById(R.id.dpartMentValue);
        txtLongTermResolTextValue = (TextView)findViewById(R.id.txtLongTermResolTextValue);
        txtQERemarkValue = (TextView)findViewById(R.id.txtQERemarkValue);
        txtShortTextValue = (TextView)findViewById(R.id.txtShortTextValue);
        SERemarkValue = (TextView)findViewById(R.id.SERemarkValue);

        cancel = findViewById(R.id.cancel);
        feedback = findViewById(R.id.feedback);

        rl_Details.setOnClickListener(this);
        rl_scheduled_visit.setOnClickListener(this);
        rl_visit_log.setOnClickListener(this);
        rl_machine_para.setOnClickListener(this);
        rl_competitors_para.setOnClickListener(this);
        rl_travelling_expen.setOnClickListener(this);
        rl_report.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == rl_Details.getId())
        {
            Intent i = new Intent(Swas_Details_Of_Completed_Call.this,Swas_Completed_Call_Service_Id_Details_Activity.class);
            Bundle bundle = new Bundle();
            bundle.putString("service_call_id",service_call_id);
            bundle.putString("status_id",status_id);
            bundle.putString("Typeofservicecall_id",type_of_service_status_id);
            bundle.putString("customer_id",customer_id);
            i.putExtras(bundle);
            startActivity(i);
            //View_details();
        }
        else if (view.getId() == rl_scheduled_visit.getId())
        {
            Intent i = new Intent( Swas_Details_Of_Completed_Call.this, ScheduledVisitActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("service_call_id",service_call_id);
            bundle.putString("status_id",status_id);
            bundle.putString("Typeofservicecall_id",type_of_service_status_id);
            bundle.putString("customer_id",customer_id);
            i.putExtras(bundle);
            startActivity(i);

        }
        else if (view.getId() == rl_visit_log.getId())
        {
            Intent intent = new Intent( Swas_Details_Of_Completed_Call.this,Swas_Completed_Visit_Log_Activity.class);
            Bundle bundle = new Bundle();
            bundle.putString("service_call_id",service_call_id);
            bundle.putString("status_id",status_id);
            bundle.putString("Typeofservicecall_id",type_of_service_status_id);
            bundle.putString("customer_id",customer_id);
            intent.putExtras(bundle);
            startActivity(intent);
        }
        else if (view.getId() == rl_machine_para.getId())
        {
            Intent intent = new Intent( Swas_Details_Of_Completed_Call.this,Swas_Completed_Machine_Parameters_Activity.class);
            Bundle bundle = new Bundle();
            bundle.putString("service_call_id",service_call_id);
            bundle.putString("status_id",status_id);
            bundle.putString("Typeofservicecall_id",type_of_service_status_id);
            bundle.putString("customer_id",customer_id);
            intent.putExtras(bundle);
            startActivity(intent);
        }
        else if (view.getId() == rl_competitors_para.getId())
        {
            Intent intent = new Intent( Swas_Details_Of_Completed_Call.this,Swas_Completed_Competitors_Parameters_Activity.class);
            Bundle bundle = new Bundle();
            bundle.putString("service_call_id",service_call_id);
            bundle.putString("status_id",status_id);
            bundle.putString("Typeofservicecall_id",type_of_service_status_id);
            bundle.putString("customer_id",customer_id);
            intent.putExtras(bundle);
            startActivity(intent);

        }
        else if (view.getId() == rl_travelling_expen.getId())
        {
            Intent intent = new Intent( Swas_Details_Of_Completed_Call.this,Swas_Completed_Travelling_Expenses_Activity.class);
            Bundle bundle = new Bundle();
            bundle.putString("service_call_id",service_call_id);
            bundle.putString("status_id",status_id);
            bundle.putString("Typeofservicecall_id",type_of_service_status_id);
            bundle.putString("customer_id",customer_id);
            intent.putExtras(bundle);
            startActivity(intent);

        }
        else if (view.getId() == rl_report.getId())
        {
            Intent intent = new Intent( Swas_Details_Of_Completed_Call.this,Swas_Completed_Upload_Report_Document.class);
            Bundle bundle = new Bundle();
            bundle.putString("service_call_id",service_call_id);
            bundle.putString("status_id",status_id);
            bundle.putString("Typeofservicecall_id",type_of_service_status_id);
            bundle.putString("customer_id",customer_id);
            intent.putExtras(bundle);
            startActivity(intent);

        }
    }

    private void Id_Details() {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getalldetailsofsingleservicecall(service_call_id,status_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {

                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            ArrayList<SwasIdDetailsModel> model = new ArrayList<>();
                            if (array.length() != 0) {

                                for (int i = 0; i < array.length(); i++) {
                                    SwasIdDetailsModel swasIdDetailsModel = new SwasIdDetailsModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    swasIdDetailsModel.setService_Id(object1.getString("ServiceCallID"));
                                    swasIdDetailsModel.setLOCATIONNAME(object1.getString("LOCATIONNAME"));
                                    swasIdDetailsModel.setCreatedBy(object1.getString("CreatedBy"));
                                    swasIdDetailsModel.setCreatedDate(object1.getString("CreatedDate"));
                                    swasIdDetailsModel.setUpdatedBy(object1.getString("UpdatedBy"));
                                    swasIdDetailsModel.setUpdatedDate(object1.getString("UpdatedDate"));
                                    swasIdDetailsModel.setTypeOfServiceCallText(object1.getString("TypeOfServiceCallText"));
                                    swasIdDetailsModel.setNAME(object1.getString("NAME"));
                                    swasIdDetailsModel.setBriefComplaint(object1.getString("BriefComplaint"));
                                    swasIdDetailsModel.setTypeOfFaultText(object1.getString("TypeOfFaultText"));
                                    swasIdDetailsModel.setStatusText(object1.getString("StatusText"));
                                    swasIdDetailsModel.setFaultyPart(object1.getString("FaultyPart"));
                                    swasIdDetailsModel.setCustomerID(object1.getString("CustomerID"));
                                    swasIdDetailsModel.setITEMID(object1.getString("ITEMID"));
                                    swasIdDetailsModel.setCURRENCY(object1.getString("CURRENCY"));
                                    swasIdDetailsModel.setDocumentPath(object1.getString("DocumentPath"));
                                    swasIdDetailsModel.setTypeOfServiceCallID(object1.getString("TypeOfServiceCallID"));
                                    swasIdDetailsModel.setCurrentlyAssignedToSE(object1.getString("CurrentlyAssignedToSE"));
                                    swasIdDetailsModel.setServiceEngineerName(object1.getString("ServiceEngineerName"));
                                    swasIdDetailsModel.setClosedDateTime(object1.getString("ClosedDateTime"));
                                    swasIdDetailsModel.setClosedBy(object1.getString("ClosedBy"));
                                    swasIdDetailsModel.setServiceCallStatusID(object1.getString("ServiceCallStatusID"));
                                    swasIdDetailsModel.setComments(object1.getString("Comments"));
                                    swasIdDetailsModel.setServiceEngineerID(object1.getString("ServiceEngineerID"));
                                    swasIdDetailsModel.setNumberOfVisits(object1.getString("NumberOfVisits"));
                                    txt_service_call_id.setText(object1.getString("ServiceCallID"));
                                    txt_customer_name.setText(object1.getString("LOCATIONNAME"));
                                    txt_call_creadted.setText(object1.getString("CreatedBy"));
                                    txt_created_on.setText(object1.getString("CreatedDate"));
                                    txt_modified_on.setText(object1.getString("UpdatedDate"));


                                    customer_id = object1.getString("CustomerID");
                                    status_id = object1.getString("ServiceCallStatusID");
                                    service_call_id = object1.getString("ServiceCallID");
                                    type_of_service_status_id = object1.getString("TypeOfServiceCallID");
                                    model.add(swasIdDetailsModel);
                                }
                            }
                            else if (array.length() == 0) {
                            }
                        }
                    }


                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void serviceId1() {
        model.clear();
        API api = Retroconfig.swasretrofit().create(API.class);
        // String service_eng_id = "1";
        Call<ResponseBody> call = api.getcompletedservicecalls(Integer.parseInt(service_call_id),5,3);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            if (array.length() != 0) {
                                for (int i = 0; i < array.length(); i++) {
                                    SwasServiceCallIdModel swasServiceModel = new SwasServiceCallIdModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    swasServiceModel.setServiceCallID(object1.getString("ServiceCallID"));
                                    swasServiceModel.setCustomerID(object1.getString("CustomerID"));
                                    swasServiceModel.setLOCATIONNAME(object1.getString("LOCATIONNAME"));
                                    swasServiceModel.setITEMID(object1.getString("ITEMID"));
                                    swasServiceModel.setServiceEngineerName(object1.getString("ServiceEngineerName"));
                                    swasServiceModel.setStatusText(object1.getString("StatusText"));
                                    swasServiceModel.setTypeOfServiceCallText(object1.getString("TypeOfServiceCallText"));
                                    swasServiceModel.setCreatedBy(object1.getString("CreatedBy"));
                                    swasServiceModel.setCreatedDate(object1.getString("CreatedDate"));
                                    swasServiceModel.setUpdatedBy(object1.getString("UpdatedBy"));
                                    swasServiceModel.setUpdatedDate(object1.getString("UpdatedDate"));
                                    swasServiceModel.setServiceCallStatusID(object1.getString("ServiceCallStatusID"));

                                    txt_service_call_id.setText(object1.getString("ServiceCallID"));
                                    txt_customer_name.setText(object1.getString("LOCATIONNAME"));
                                    txt_call_creadted.setText(object1.getString("CreatedBy"));
                                    txt_created_on.setText(object1.getString("CreatedDate"));
                                    txt_modified_on.setText(object1.getString("UpdatedDate"));
                                    numberOfVisitsValue.setText(object1.getInt("NumberOfVisits")+"");
                                    txt_rootCauseValue.setText(object1.getString("RootCauseName"));
                                    txtDescriptionValue.setText(object1.getString("Description"));
                                    dpartMentValue.setText(object1.getString("Departments"));
                                    txtLongTermResolTextValue.setText(object1.getString("LongTermResolutionText"));
                                    txtQERemarkValue.setText(object1.getString("QERemark"));
                                    txtShortTextValue.setText(object1.getString("ShortTermResolutionText"));
                                    SERemarkValue.setText(object1.getString("SERemark"));


                                    customer_id = object1.getString("CustomerID");
                                    status_id = object1.getString("ServiceCallStatusID");
                                    service_call_id = object1.getString("ServiceCallID");
                                    type_of_service_status_id = object1.getString("TypeOfServiceCallID");

                                    model.add(swasServiceModel);
                                }


                            } else if (array.length() == 0) {
//                                Toast.makeText(Swas_Service_Call_List_Activity.this, "Sorry there is no data", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else
                    {
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(Swas_Details_Of_Completed_Call.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);
        setData();
        // Set content width and height
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(Swas_Details_Of_Completed_Call.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }

    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(Swas_Details_Of_Completed_Call.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();

                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));


                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout() {
        finish();
        Toast toast= Toast.makeText(Swas_Details_Of_Completed_Call.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(Swas_Details_Of_Completed_Call.this);

        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);

        //Preferences.Logout();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();


        if (id==R.id.nav_swas)
        {
            Intent i=new Intent(Swas_Details_Of_Completed_Call.this, Swas_Home_Actvity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    public void checkNetworkConnection(){
        AlertDialog.Builder builder =new AlertDialog.Builder(Swas_Details_Of_Completed_Call.this);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(Swas_Details_Of_Completed_Call.this, Swas_Home_Actvity.class);
        Bundle bundle = new Bundle();
        bundle.putString("service_call_id",service_call_id);
        bundle.putString("status_id",status_id);
        bundle.putString("Typeofservicecall_id",type_of_service_status_id);
        bundle.putString("customer_id",customer_id);
        i.putExtras(bundle);
        startActivity(i);
        finish();
    }

    private void getDataFromListOfServiceCallByStatus() {
        Cursor cursor = helper.getDetailsDataFromServiceCallStatusTable(service_call_id);
        while (cursor.moveToNext()){
            String customer_name = cursor.getString(0);
            String created_By = cursor.getString(1);
            String created_on = cursor.getString(2);
            String last_modified_on = cursor.getString(3);
            String product = cursor.getString(4);
            String complaint = cursor.getString(5);
            String typeOfServiceCallText= cursor.getString(6);
            String type_of_fault_text= cursor.getString(7);
            String serviceCallId= cursor.getString(8);
            String noOfVisits= cursor.getString(9);
            txt_service_call_id.setText(service_call_id);
//            txt_service_call_id.setText(customer_name);
            txt_call_creadted.setText(created_By);
            txt_created_on.setText(created_on);
            txt_modified_on.setText(last_modified_on);
            txt_customer_name.setText(customer_name);
            numberOfVisitsValue.setText(noOfVisits);
            txt_rootCauseValue.setText(cursor.getString(10));
            txtDescriptionValue.setText(cursor.getString(11));
            dpartMentValue.setText(cursor.getString(12));
            txtLongTermResolTextValue.setText(cursor.getString(13));
            txtQERemarkValue.setText(cursor.getString(14));
            txtShortTextValue.setText(cursor.getString(15));
            SERemarkValue.setText(cursor.getString(16));

        }
    }
}

