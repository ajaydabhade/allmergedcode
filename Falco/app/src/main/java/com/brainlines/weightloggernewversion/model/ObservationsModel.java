package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ObservationsModel implements Parcelable {
    String OEMID;
    String ObservationsID;
    String MacID;
    String LoggerID;
    String TagID;
    String AugerCount;
    String AugerSpeed;
    String AugerPreUsed;
    String AugerNumberOfAgitators;
    String McVerticalTeflon;
    String McHorizontalJawOpeneing;
    String McHorizontalJawMovement;
    String McVerticalAlignCenter;
    String McVerticalJawMovement;
    String PkgWireCup;
    String PkgEmptyPouches;
    String PkgBaglengthVariation;
    String PkgPaperShifting;
    String PkgVerticalOverlap;
    String PkgPouchSymmetry;
    String ObservSpeed;
    String SealRejectionReason;

    public ObservationsModel(String OEMID, String observationsID, String macID, String loggerID, String tagID, String augerCount, String augerSpeed, String augerPreAuger, String augerNoOfAgitators, String mcVerticalTeflon, String mcHorizJawOpeneing, String mcHorizJawMovement, String mcVertAlignCenter, String mcVertJawMovement, String pkgWireCup, String pkgEmptyPouches, String pkgBaglenVariation, String pkgPaperShifting, String pkgVerticalOverlap, String pkgPouchSymmetry, String observSpeed, String sealRejectionReason) {
        this.OEMID = OEMID;
        ObservationsID = observationsID;
        MacID = macID;
        LoggerID = loggerID;
        TagID = tagID;
        AugerCount = augerCount;
        AugerSpeed = augerSpeed;
        AugerPreUsed = augerPreAuger;
        AugerNumberOfAgitators = augerNoOfAgitators;
        McVerticalTeflon = mcVerticalTeflon;
        McHorizontalJawOpeneing = mcHorizJawOpeneing;
        McHorizontalJawMovement = mcHorizJawMovement;
        McVerticalAlignCenter = mcVertAlignCenter;
        McVerticalJawMovement = mcVertJawMovement;
        PkgWireCup = pkgWireCup;
        PkgEmptyPouches = pkgEmptyPouches;
        PkgBaglengthVariation = pkgBaglenVariation;
        PkgPaperShifting = pkgPaperShifting;
        PkgVerticalOverlap = pkgVerticalOverlap;
        PkgPouchSymmetry = pkgPouchSymmetry;
        ObservSpeed = observSpeed;
        SealRejectionReason = sealRejectionReason;
    }

    protected ObservationsModel(Parcel in) {
        OEMID = in.readString();
        ObservationsID = in.readString();
        MacID = in.readString();
        LoggerID = in.readString();
        TagID = in.readString();
        AugerCount = in.readString();
        AugerSpeed = in.readString();
        AugerPreUsed = in.readString();
        AugerNumberOfAgitators = in.readString();
        McVerticalTeflon = in.readString();
        McHorizontalJawOpeneing = in.readString();
        McHorizontalJawMovement = in.readString();
        McVerticalAlignCenter = in.readString();
        McVerticalJawMovement = in.readString();
        PkgWireCup = in.readString();
        PkgEmptyPouches = in.readString();
        PkgBaglengthVariation = in.readString();
        PkgPaperShifting = in.readString();
        PkgVerticalOverlap = in.readString();
        PkgPouchSymmetry = in.readString();
        ObservSpeed = in.readString();
        SealRejectionReason = in.readString();
    }

    public static final Creator<ObservationsModel> CREATOR = new Creator<ObservationsModel>() {
        @Override
        public ObservationsModel createFromParcel(Parcel in) {
            return new ObservationsModel(in);
        }

        @Override
        public ObservationsModel[] newArray(int size) {
            return new ObservationsModel[size];
        }
    };

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getObservationsID() {
        return ObservationsID;
    }

    public void setObservationsID(String observationsID) {
        ObservationsID = observationsID;
    }

    public String getMacID() {
        return MacID;
    }

    public void setMacID(String macID) {
        MacID = macID;
    }

    public String getLoggerID() {
        return LoggerID;
    }

    public void setLoggerID(String loggerID) {
        LoggerID = loggerID;
    }

    public String getTagID() {
        return TagID;
    }

    public void setTagID(String tagID) {
        TagID = tagID;
    }

    public String getAugerCount() {
        return AugerCount;
    }

    public void setAugerCount(String augerCount) {
        AugerCount = augerCount;
    }

    public String getAugerSpeed() {
        return AugerSpeed;
    }

    public void setAugerSpeed(String augerSpeed) {
        AugerSpeed = augerSpeed;
    }

    public String getAugerPreAuger() {
        return AugerPreUsed;
    }

    public void setAugerPreAuger(String augerPreAuger) {
        AugerPreUsed = augerPreAuger;
    }

    public String getAugerNoOfAgitators() {
        return AugerNumberOfAgitators;
    }

    public void setAugerNoOfAgitators(String augerNoOfAgitators) {
        AugerNumberOfAgitators = augerNoOfAgitators;
    }

    public String getMcVerticalTeflon() {
        return McVerticalTeflon;
    }

    public void setMcVerticalTeflon(String mcVerticalTeflon) {
        McVerticalTeflon = mcVerticalTeflon;
    }

    public String getMcHorizJawOpeneing() {
        return McHorizontalJawOpeneing;
    }

    public void setMcHorizJawOpeneing(String mcHorizJawOpeneing) {
        McHorizontalJawOpeneing = mcHorizJawOpeneing;
    }

    public String getMcHorizJawMovement() {
        return McHorizontalJawMovement;
    }

    public void setMcHorizJawMovement(String mcHorizJawMovement) {
        McHorizontalJawMovement = mcHorizJawMovement;
    }

    public String getMcVertAlignCenter() {
        return McVerticalAlignCenter;
    }

    public void setMcVertAlignCenter(String mcVertAlignCenter) {
        McVerticalAlignCenter = mcVertAlignCenter;
    }

    public String getMcVertJawMovement() {
        return McVerticalJawMovement;
    }

    public void setMcVertJawMovement(String mcVertJawMovement) {
        McVerticalJawMovement = mcVertJawMovement;
    }

    public String getPkgWireCup() {
        return PkgWireCup;
    }

    public void setPkgWireCup(String pkgWireCup) {
        PkgWireCup = pkgWireCup;
    }

    public String getPkgEmptyPouches() {
        return PkgEmptyPouches;
    }

    public void setPkgEmptyPouches(String pkgEmptyPouches) {
        PkgEmptyPouches = pkgEmptyPouches;
    }

    public String getPkgBaglenVariation() {
        return PkgBaglengthVariation;
    }

    public void setPkgBaglenVariation(String pkgBaglenVariation) {
        PkgBaglengthVariation = pkgBaglenVariation;
    }

    public String getPkgPaperShifting() {
        return PkgPaperShifting;
    }

    public void setPkgPaperShifting(String pkgPaperShifting) {
        PkgPaperShifting = pkgPaperShifting;
    }

    public String getPkgVerticalOverlap() {
        return PkgVerticalOverlap;
    }

    public void setPkgVerticalOverlap(String pkgVerticalOverlap) {
        PkgVerticalOverlap = pkgVerticalOverlap;
    }

    public String getPkgPouchSymmetry() {
        return PkgPouchSymmetry;
    }

    public void setPkgPouchSymmetry(String pkgPouchSymmetry) {
        PkgPouchSymmetry = pkgPouchSymmetry;
    }

    public String getObservSpeed() {
        return ObservSpeed;
    }

    public void setObservSpeed(String observSpeed) {
        ObservSpeed = observSpeed;
    }

    public String getSealRejectionReason() {
        return SealRejectionReason;
    }

    public void setSealRejectionReason(String sealRejectionReason) {
        SealRejectionReason = sealRejectionReason;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(OEMID);
        dest.writeString(ObservationsID);
        dest.writeString(MacID);
        dest.writeString(LoggerID);
        dest.writeString(TagID);
        dest.writeString(AugerCount);
        dest.writeString(AugerSpeed);
        dest.writeString(AugerPreUsed);
        dest.writeString(AugerNumberOfAgitators);
        dest.writeString(McVerticalTeflon);
        dest.writeString(McHorizontalJawOpeneing);
        dest.writeString(McHorizontalJawMovement);
        dest.writeString(McVerticalAlignCenter);
        dest.writeString(McVerticalJawMovement);
        dest.writeString(PkgWireCup);
        dest.writeString(PkgEmptyPouches);
        dest.writeString(PkgBaglengthVariation);
        dest.writeString(PkgPaperShifting);
        dest.writeString(PkgVerticalOverlap);
        dest.writeString(PkgPouchSymmetry);
        dest.writeString(ObservSpeed);
        dest.writeString(SealRejectionReason);
    }
}
