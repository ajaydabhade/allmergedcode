package com.brainlines.weightloggernewversion.FalcoJK.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.AssetNoModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.EnterWIPDataModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.EquipTypeModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.OperationEquipModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.ProdTkOperationModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.SkuDetailsModel;
import com.brainlines.weightloggernewversion.FalcoJK.Listeners.EnterWipListener;
import com.brainlines.weightloggernewversion.FalcoJK.Listeners.JKProductionUpdateListener;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.AssetNoAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.EnterWipAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.EquipTypeSpinAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.NewTicketListAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.OpearionEqupSpinAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.SKU_Adapter;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.SpinnerAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.UrlConstants.JK_URL_Constants;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.ModuleSelectionActivity;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.navigation.NavigationView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JK_ProductionTicket_Update_Actvity extends FragmentActivity implements NavigationView.OnNavigationItemSelectedListener, JKProductionUpdateListener, EnterWipListener {

    TextView txt_up_ticket_no,txt_up_plant,txt_up_valuestream,txt_up_tk_filetype,txt_up_tk_qty,txt_sku_8,txt_sku_13,txt_up_startdate;
    TextView txt_up_enddate,txt_up_act_strat_date,txt_up_act_end_date,txt_up_Release_qty;
    ImageButton img_start_ticket,img_end_ticket,previous_nav,next_nav,view_sku_details,img_enter_wip;
    TextView txt_img_up_end_ticket,txt_up_img_starttk,txt_enter_wip;
    RecyclerView rv_update_pro_ticket;
    String str_tk_id,user_email,user_role,user_token,str_tk_no;
    ImageView img_back,img_userinfo,nav_view_img;
    DrawerLayout drawer;
    TextView txt_updated_griddate;
    RelativeLayout relativelayout_sku;
    RecyclerView rv_enterwipdetails;
    ArrayList<ProdTkOperationModel> ticketmodel= new ArrayList<>();
    ArrayList<EnterWIPDataModel> enterWIPDataModelArrayList = new ArrayList<>();
    AlertDialog alertDialog;
    NewTicketListAdapter newTicketListAdapter;
    EnterWipAdapter enterWipAdapter;
    ArrayList<OperationEquipModel> models=new ArrayList<>();
    ArrayList<EquipTypeModel>equipTypelist=new ArrayList<>();
    ArrayList<AssetNoModel>assetNoModels=new ArrayList<>();
    ArrayList<ProdTkOperationModel> getTicketList = new ArrayList<>();
    OpearionEqupSpinAdapter opearionEqupSpinAdapter;
    EquipTypeSpinAdapter equipTypeSpinAdapter;
    AssetNoAdapter assetNoAdapter;
    Spinner spin_equipment, spin_equip_type,spin_select_operation,spin_spin_asset_no;
    String xmlData,jsonstring;
    String date1,date,psku ,start_date,end_date;
    BottomSheetDialog mBottomSheetDialog;
    private String str_valuestream_code,str_ticketIDNo,str_plant,te_NoofShipt,cRRemark;
    int str_operation_no;
    private String te_ReseasenDt,str_equip_name;
    private String str_plant_code;
    private String str_eqip_id;
    private String te_AssetNo;
    //SwipeRefreshLayout swipeRefreshLayout;
    String lastitemokqty,okqty,lastitemtotalqty;
    ImageButton img_btn_refresh;
    String flag="IsFirst";
    String dateformat1 = "";
    Calendar c = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    private TextView profile_user,profile_role;
    String str_validate_wip,str_validate_okqty,str_validate_rejectqty,str_validate_secondqty,str_validate_total_qty;
    RecyclerView rv_sku_details;
    TextView txt_first_opr_qty;
    SKU_Adapter sku_adapter;
    int firstoperationokqty =0;
    DataBaseHelper helper;
    SQLiteDatabase db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_production_ticket_update_navigation);
        setData();
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img = findViewById(R.id.nav_view_img);
        helper = new DataBaseHelper(JK_ProductionTicket_Update_Actvity.this);
        db = helper.getWritableDatabase();
        initUi();
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(JK_ProductionTicket_Update_Actvity.this, ModuleSelectionActivity.class);
                startActivity(i);
            }
        });

        img_userinfo = findViewById(R.id.img_userinfo);
        img_userinfo.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_swas).setVisible(false);

        img_btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getViewData();
            }
        });
        Bundle bundle = getIntent().getExtras();
        if (!bundle.getString("ptk_tk_id").equals("")&&bundle.getString("ptk_tk_id")!=null){
            str_tk_id=bundle.getString("ptk_tk_id");
        }
        if (!bundle.getString("tk_no").equals("")&&bundle.getString("tk_no")!=null){
            str_tk_no=bundle.getString("tk_no");
        }
        if (!bundle.getString("tk_sku").equals("")&&bundle.getString("tk_sku")!=null){
            psku=bundle.getString("tk_sku");
        }
        txt_up_plant.setText(bundle.getString("tk_plant"));
        txt_up_tk_qty.setText(bundle.getString("tk_quantity"));
        txt_up_Release_qty.setText(bundle.getString("tk_quantity"));
        txt_up_valuestream.setText(bundle.getString("valuestream"));
        txt_up_startdate.setText(bundle.getString("tk_start_date"));
        txt_up_enddate.setText(bundle.getString("tk_end_date"));
        txt_up_ticket_no.setText(bundle.getString("tk_no"));
        view_sku_details = (ImageButton)findViewById(R.id.view_sku_details);
        txt_sku_8.setText(psku);

        date = sdf.format(c.getTime());



        dateformat1 = sdf.format(c.getTime());
        txt_updated_griddate.setText(dateformat1);

        getViewData();
        new GetSkuOfTickets().execute();

        view_sku_details.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if(relativelayout_sku.getVisibility()==View.GONE) {
                    relativelayout_sku.setVisibility(View.VISIBLE);
                }
                else
                {
                    relativelayout_sku.setVisibility(View.GONE);
                }
            }
        });
        img_start_ticket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag="IsFirst";
                new GetProductionTicketDetails().execute();
            }
        });

        img_end_ticket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new EndProductionTicketCall().execute();
            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(JK_ProductionTicket_Update_Actvity.this, JK_Production_Selection_list.class);
                startActivity(i);
            }
        });
        img_userinfo.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        txt_up_img_starttk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new GetProductionTicketDetails().execute();

            }
        });
        txt_img_up_end_ticket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new EndProductionTicketCall().execute();
            }
        });


       txt_enter_wip.setOnClickListener(new View.OnClickListener() {
           @RequiresApi(api = Build.VERSION_CODES.M)
           @Override
           public void onClick(View view) {
               displayEnterWIp();
           }
       });
       img_enter_wip.setOnClickListener(new View.OnClickListener() {
           @RequiresApi(api = Build.VERSION_CODES.M)
           @Override
           public void onClick(View view) {
               displayEnterWIp();
           }
       });

       next_nav.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               String date = txt_updated_griddate.getText().toString();
               /*SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMMyyyy");*/
               Date convertedDate = new Date();
               try {
                   convertedDate = sdf.parse(date);
               }
               catch (ParseException e) {
                   // TODO Auto-generated catch block
                   e.printStackTrace();
               }
               System.out.println(convertedDate);
               Calendar cal = Calendar.getInstance();
               cal.setTime(convertedDate);
               cal.add(Calendar.DAY_OF_MONTH, 1);

               String nextdate= sdf.format(cal.getTime());
               dateformat1 = nextdate;
               txt_updated_griddate.setText(nextdate);
               getViewData();


           }
       });
       previous_nav.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               String date = txt_updated_griddate.getText().toString();
               /*SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMMyyyy");*/
               Date convertedDate = new Date();
               try {
                   convertedDate = sdf.parse(date);
               } catch (ParseException e) {
                   // TODO Auto-generated catch block
                   e.printStackTrace();
               }
               System.out.println(convertedDate);
               Calendar cal = Calendar.getInstance();
               cal.setTime(convertedDate);
               cal.add(Calendar.DAY_OF_MONTH, -1);

               String previous_date= sdf.format(cal.getTime());
               dateformat1 = previous_date;
               getViewData();
               txt_updated_griddate.setText(previous_date);

           }
       });
    }
    public void initUi()
    {
        txt_up_ticket_no = findViewById(R.id.txt_up_ticket_no);
        txt_up_plant = findViewById(R.id.txt_up_plant);
        txt_up_valuestream = findViewById(R.id.txt_up_valuestream);
        txt_up_tk_filetype = findViewById(R.id.txt_up_tk_filetype);
        txt_up_tk_qty = findViewById(R.id.txt_up_tk_qty);
        txt_sku_8 = findViewById(R.id.txt_sku_8);
        txt_up_startdate = findViewById(R.id.txt_up_startdate);
        txt_up_enddate = findViewById(R.id.txt_up_enddate);
        txt_up_act_strat_date = findViewById(R.id.txt_up_act_strat_date);
        txt_up_act_end_date = findViewById(R.id.txt_up_act_end_date);
        txt_img_up_end_ticket = findViewById(R.id.txt_img_up_end_ticket);
        txt_up_img_starttk = findViewById(R.id.txt_up_img_starttk);
        txt_enter_wip = findViewById(R.id.txt_enter_wip);
        txt_up_Release_qty = findViewById(R.id.txt_up_Release_qty);

        img_start_ticket = findViewById(R.id.img_start_ticket);
        img_end_ticket = findViewById(R.id.img_end_ticket);
        img_enter_wip = findViewById(R.id.img_enter_wip);

        rv_update_pro_ticket = findViewById(R.id.rv_prod_view_ticket);
        rv_sku_details = findViewById(R.id.rv_sku_details);

        relativelayout_sku=findViewById(R.id.relativelayout_sku);
        previous_nav = findViewById(R.id.previous_nav);
        next_nav = findViewById(R.id.next_nav);
        txt_first_opr_qty =  findViewById(R.id.txt_first_opr_qty);

        txt_updated_griddate = findViewById(R.id.txt_updated_griddate);
        img_btn_refresh = findViewById(R.id.img_btn_refresh);

    }

    @Override
    public void imgRouteIsClicked(boolean flag, ProdTkOperationModel model) {
        if (flag == true){
            ProdTkOperationModel data = model;
            showbottomsheetdialog(data);
        }
    }

    @Override
    public void imgSubmitButtonIsClicked(ArrayList<ProdTkOperationModel> ticketList) {
        ArrayList<ProdTkOperationModel> list = ticketList;
        String s = list.toString();
        Log.d("List", "imgSubmitButtonIsClicked: "+list.toString());
        StringBuilder stringBuilder=new StringBuilder();
        stringBuilder.append('[');
        for (int i = 0 ; i < list.size() ; i++){

            stringBuilder.append('{' + "ptk_PtkdId" + ':' + '"').append(list.get(i).getPtk_PtkdId()).append('"').append(',');
            stringBuilder.append("ptk_TkNo" + ':' + '"').append(list.get(i).getPtkd_TkNo()).append('"').append(',');
            stringBuilder.append("ptk_Plant" + ':' + '"').append(list.get(i).getPtkd_Plant()).append('"').append(',');
            stringBuilder.append("ptk_Operation" + ':' + '"').append(list.get(i).getPtkd_Operation()).append('"').append(',');
            stringBuilder.append("ptk_UpdateDate"+':'+'"' + date +'"'+',');
            if (list.get(i).getPtk_OkQty_ans()==null){
                stringBuilder.append("ptk_OkQty" + ':' + '"').append(list.get(i).getPtk_OkQty()).append('"').append(',');
            }else {
                stringBuilder.append("ptk_OkQty" + ':' + '"').append(list.get(i).getPtk_OkQty_ans()).append('"').append(',');
            }
            stringBuilder.append("ptk_ActualPlant" + ':' + '"').append(list.get(i).getPtkd_Plant()).append('"').append(',');
            if (list.get(i).getPtk_ReworkQty_ans()==null){

                stringBuilder.append("ptk_ReworkQty" + ':' + '"').append(list.get(i).getPtk_ReworkQty()).append('"').append(',');
            }else {
                stringBuilder.append("ptk_ReworkQty" + ':' + '"').append(list.get(i).getPtk_ReworkQty_ans()).append('"').append(',');
            }

            if (list.get(i).getPtk_RejectQty_ans()==null){

               stringBuilder.append("ptk_RejectQty" + ':' + '"').append(list.get(i).getPtk_RejectQty()).append('"').append(',');
            }else {
                stringBuilder.append("ptk_RejectQty" + ':' + '"').append(list.get(i).getPtk_RejectQty_ans()).append('"').append(',');
            }
            if (list.get(i).getPtk_WIPQty_ans()==null){

                stringBuilder.append("ptk_WIPQty" + ':' + '"').append(list.get(i).getPtk_WIPQty()).append('"').append(',');
            }
            else {
                stringBuilder.append("ptk_WIPQty" + ':' + '"').append(list.get(i).getPtk_WIPQty_ans()).append('"').append(',');
            }
            if (list.get(i).getPtk_SecondQty_ans()==null){
                stringBuilder.append("ptk_SecondQty" + ':' + '"').append(list.get(i).getPtk_SecondQty()).append('"').append(',');
            }else {
                stringBuilder.append("ptk_SecondQty" + ':' + '"').append(list.get(i).getPtk_SecondQty_ans()).append('"').append(',');
            }
            stringBuilder.append("ptk_TotalQty" + ':' + '"').append(list.get(i).getPtkd_Qty()).append('"').append(',');
            stringBuilder.append("ptk_CreatedBy"+':'+'"' + JK_URL_Constants.PLANT_USER +'"'+',' );
            stringBuilder.append("ptk_CreatedDate"+':'+'"' + date + '"'+',');
            stringBuilder.append("ptk_ApprovedBy"+':'+'"' + JK_URL_Constants.PLANT_USER +'"'+',');
            stringBuilder.append("ptk_ApprovedDate"+':'+'"' + date +'"'+',');
            stringBuilder.append("ptk_Approve"+':'+'"' + "N" + '"'+',');
            stringBuilder.append("ptk_ValueStream" + ':' + '"').append(list.get(i).getVsCode()).append('"').append(',');
            stringBuilder.append("ptk_Active"+':'+ "1" +'}'+',');

            if (i==list.size() - 1)
            {
                lastitemokqty=list.get(i).getPtk_OkQty_ans();
                lastitemtotalqty = String.valueOf(list.get(i).getPtkd_Qty());

            }

        }

        stringBuilder.append(']');
        xmlData = stringBuilder.toString().replace("&", "&amp;").replace(" <", "<").replace("> ", ">");
        jsonstring=xmlData.substring(0, xmlData.length() - 2)+ ']';
        updateTicketDetails();

    }
    @Override
    public void getOkValue(String str_ok, String rejectqty,String secondqty,String str_wip)
    {
        if (str_ok != null && !str_ok.equals(""))
        {
            str_validate_okqty = str_ok;
        }
        if (rejectqty != null && !rejectqty.equals(""))
        {
            str_validate_rejectqty = rejectqty;
        }
        if (secondqty != null && !secondqty.equals(""))
        {
            str_validate_secondqty = secondqty;
        }
        if (str_wip != null && !str_wip.equals(""))
        {
            str_validate_wip = str_wip;
        }
        int totalokrejectsecond = Integer.parseInt(str_validate_okqty) + Integer.parseInt(str_validate_rejectqty) + Integer.parseInt(str_validate_secondqty);


        if (totalokrejectsecond > Integer.parseInt(str_validate_wip))
        {
            Toast.makeText(this, "you can't enter ok qty greater than wip", Toast.LENGTH_SHORT).show();
            getViewData();

        }
    }

    @Override
    public void checktotalokqty(String totalqty, String str_ok, String rejectqty, String secondqty, String str_wip) {

    }

    private void updateTicketDetails() {
        API api = Retroconfig.retrofit().create(API.class);
        Call<ResponseBody> call = api.updateData(jsonstring);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body().string()!=null)
                    {
                        String res = response.body().string();
                        Log.d("", "onResponse: "+res);
                        if (res!=null){
                            if (res.contains("")){
                                Toast.makeText(JK_ProductionTicket_Update_Actvity.this, "SuccessFully Inserted", Toast.LENGTH_SHORT).show();

                                String str=txt_up_tk_qty.getText().toString();
                                String str1[]=str.split(" ");
                                String qty=str1[0];
                                if (lastitemokqty!= null)
                                {
                                    //lastitemtotalqty = txt_up_Release_qty.getText().toString().concat(".0");
                                    if (lastitemokqty.equals(qty) || Integer.parseInt(lastitemokqty)>= Integer.parseInt(qty))
                                    {
                                        new EndProductionTicketCall().execute();
                                    }
                                    else if (Integer.parseInt(lastitemokqty)>= Integer.parseInt(qty)) {
                                        new EndProductionTicketCall().execute();

                                    }
                                }
                              getViewData();
                            }
                        }else{

                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(JK_ProductionTicket_Update_Actvity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getViewData() {

        API api = Retroconfig.retrofit().create(API.class);
       // dateformat1 = "2020-02-07";
        //dateformat1 = sdf1.format(dateformat1);
        Call<ResponseBody> call = api.getViewData(str_tk_id,dateformat1,psku);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if(response.body()!=null) {
                        String res = response.body().string();
                        if (res!=null){
                            getTicketList.clear();
                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("Data");

                            if (array.length()!=0){
                                for (int i = 0 ; i < array.length() ; i++){
                                    JSONObject obj = array.getJSONObject(i);
                                    ProdTkOperationModel model = new ProdTkOperationModel();
                                    okqty=String.valueOf(obj.has("ptk_OkQty"));

                                    if (obj.has("ptk_OkQty"))
                                    {
                                        ContentValues cv = new ContentValues();

                                        model.setPtk_PtkdId(obj.getInt("ptk_PtkdId"));
                                        model.setPtkd_TkNo(obj.getString("ptk_TkNo"));
                                        model.setPtkd_Plant(obj.getString("ptkd_Plant"));
                                        model.setVsCode(obj.getString("ptk_ValueStream"));
                                        model.setPtkd_Operation(obj.getInt("ptk_Operation"));
                                        model.setPtk_UpdateDate(obj.getString("ptk_UpdateDate"));
                                        model.setPtk_OkQty(obj.getInt("ptk_OkQty"));
                                        model.setPtk_RejectQty(obj.getInt("ptk_RejectQty"));
                                        model.setPtk_ReworkQty(obj.getInt("ptk_ReworkQty"));
                                        model.setPtk_WIPQty(obj.getInt("ptk_WIPQty"));
                                        model.setPtk_SecondQty(obj.getInt("ptk_SecondQty"));
                                        model.setPtk_TotalQty(obj.getString("ptkd_Qty"));
                                        model.setOpr_operation_name(obj.getString("opr_operation_name"));
                                        model.setPtkd_Qty(obj.getInt("ptkd_Qty"));
                                        model.setUni_unit_name(obj.getString("uni_unit_name"));
                                        model.setVal_valuestream_name(obj.getString("val_valuestream_name"));
                                        model.setTk_Actual_Start_Date(obj.getString("tk_Actual_Start_Date"));
                                        cv.put("",obj.getInt("ptk_PtkdId"));
                                        cv.put("",obj.getString("ptk_TkNo"));
                                        cv.put("",obj.getString("ptkd_Plant"));
                                        cv.put("",obj.getString("ptk_ValueStream"));
                                        cv.put("",obj.getString("ptk_UpdateDate"));
                                        cv.put("",obj.getInt("ptk_RejectQty"));
                                        cv.put("",obj.getInt("ptk_ReworkQty"));
                                        cv.put("",obj.getInt("ptk_WIPQty"));
                                        cv.put("",obj.getInt("ptk_SecondQty"));
                                        cv.put("",obj.getString("ptkd_Qty"));
                                        cv.put("",obj.getString("opr_operation_name"));
                                        cv.put("",obj.getString("uni_unit_name"));


                                        if (i==0)
                                        {
                                            String Totalqty = obj.getString("ptkd_Qty");
                                            String okqty = obj.getString("ptk_OkQty");
                                            Totalqty= Totalqty.substring(0, Totalqty.length() - 2);
                                            okqty= okqty.substring(0, okqty.length() - 2);
                                            //okqty = okqty.substring(okqty.length()-2);
                                            String firstoperationqty = URL_Constants.substraction(Totalqty,okqty);
                                            if (txt_first_opr_qty.getText().toString().equals(""))
                                            {
                                                txt_first_opr_qty.setText(firstoperationqty);
                                                firstoperationokqty = Integer.parseInt(firstoperationqty);

                                            }
                                            else if (!txt_first_opr_qty.getText().equals("0"))
                                            {
                                                txt_first_opr_qty.setText(firstoperationqty);
                                                firstoperationokqty = Integer.parseInt(firstoperationqty);
                                            }
                                        }
                                        else
                                        {
                                            //txt_first_opr_qty.setText(obj.getString("ptkd_Qty"));
                                        }
                                        if(obj.has("tk_Actual_End_Date"))
                                        {
                                            model.setTk_Actual_End_Date(obj.getString("tk_Actual_End_Date"));
                                            txt_up_act_end_date.setText(obj.getString("tk_Actual_End_Date"));
                                            if(!obj.getString("tk_Actual_End_Date").equals("null"))
                                            {
                                                txt_up_act_end_date.setText(obj.getString("tk_Actual_End_Date"));
                                            }
                                            else
                                            {
                                                txt_up_act_end_date.setText("NA");

                                            }
                                            if (obj.getString("tk_Actual_End_Date") != null && !obj.getString("tk_Actual_End_Date").equals("null"))
                                            {
                                                img_end_ticket.setVisibility(View.INVISIBLE);
                                                txt_img_up_end_ticket.setVisibility(View.INVISIBLE);
                                                end_date = obj.getString("tk_Actual_End_Date");
                                            }
                                            else {
                                                img_end_ticket.setVisibility(View.VISIBLE);
                                                txt_img_up_end_ticket.setVisibility(View.VISIBLE);
                                            }
                                        }
                                        else
                                        {
                                            model.setTk_Actual_End_Date("null");
                                            txt_up_act_end_date.setText("NA");
                                            txt_up_act_end_date.setText("NA");

                                        }
                                        model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                                        model.setTk_End_Date(obj.getString("tk_End_Date"));
                                        if (obj.has("ptk_ChangeRoute_Flag"))
                                        {
                                            model.setPtk_ChangeRoute_Flag(obj.getString("ptk_ChangeRoute_Flag"));
                                        }
                                        else
                                        {
                                            model.setPtk_ChangeRoute_Flag("");
                                        }

                                        str_plant_code = obj.getString("ptkd_Plant");
                                        txt_up_ticket_no.setText(obj.getString("ptk_TkNo"));
                                        txt_up_plant.setText(obj.getString("ptkd_Plant"));
                                        txt_up_valuestream.setText(obj.getString("val_valuestream_name"));
                                        txt_up_startdate.setText(obj.getString("tk_Start_Date"));
                                        txt_up_enddate.setText(obj.getString("tk_End_Date"));
                                        //txt_up_tk_qty.setText(obj.getString("ptkd_Qty").concat(" no"));
                                        txt_up_act_strat_date.setText(obj.getString("tk_Actual_Start_Date"));
                                        //txt_updated_griddate.setText(obj.getString("tk_Actual_Start_Date"));
                                        //txt_up_Release_qty.setText(obj.getString("ptkd_Qty").concat(" no"));
                                        /*model.setCurrentProdSKUExist(obj.getString("CurrentProdSKUExist"));
                                        String current_sku = obj.getString("CurrentProdSKUExist");
                                        String na= "NA";

                                        if (!obj.getString("CurrentProdSKUExist").equals(na))
                                        {
                                            img_enter_wip.setVisibility(View.INVISIBLE);
                                            txt_enter_wip.setVisibility(View.INVISIBLE);
                                            img_start_ticket.setEnabled(true);
                                            txt_up_img_starttk.setClickable(true);
                                        }
                                        else
                                        {
                                            img_enter_wip.setVisibility(View.VISIBLE);
                                            txt_enter_wip.setVisibility(View.VISIBLE);
                                            img_start_ticket.setEnabled(false);
                                            txt_up_img_starttk.setClickable(false);
                                        }
*/

                                        if (obj.getString("tk_Actual_Start_Date") != null && !obj.getString("tk_Actual_Start_Date").equals("null")) {

                                            img_start_ticket.setVisibility(View.INVISIBLE);
                                            txt_up_img_starttk.setVisibility(View.INVISIBLE);
                                            start_date = obj.getString("tk_Actual_Start_Date");
                                            img_enter_wip.setVisibility(View.INVISIBLE);
                                            txt_enter_wip.setVisibility(View.INVISIBLE);

                                        }
                                        else {
                                            img_start_ticket.setVisibility(View.VISIBLE);
                                            txt_up_img_starttk.setVisibility(View.VISIBLE);
                                        }

                                        if(!obj.getString("tk_Actual_Start_Date").equals("null")) {
                                            txt_up_act_strat_date.setText(obj.getString("tk_Actual_Start_Date"));
                                        }
                                        else {
                                            txt_up_act_strat_date.setText("NA");
                                        }

                                        getTicketList.add(model);

                                    }
                                }
                                ArrayList<ProdTkOperationModel> list = getTicketList;
                                LinearLayoutManager manager = new LinearLayoutManager(JK_ProductionTicket_Update_Actvity.this);
                                rv_update_pro_ticket.setLayoutManager(manager);
                                newTicketListAdapter = new NewTicketListAdapter(JK_ProductionTicket_Update_Actvity.this,getTicketList, JK_ProductionTicket_Update_Actvity.this,str_plant_code);
                                rv_update_pro_ticket.setAdapter(newTicketListAdapter);
                                if (okqty.equals("false"))
                                {
                                    if (flag.equals("IsFirst"))
                                    {
                                        getTicketList.clear();
                                        // new GetProductionTicketDetails().execute();
                                        new GetCurrentSkudetailsForEnterWipData().execute();
                                    }
                                }

                            }
                            else if (array.length()==0) {
                                if (flag.equals("IsFirst"))
                                {
                                    getTicketList.clear();
                                    rv_update_pro_ticket.setAdapter(null);
                                    //new GetProductionTicketDetails().execute();
                                    new GetCurrentSkudetailsForEnterWipData().execute();
                                }
                            }
                        }
                    }
                }
                catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void getWIPData(EnterWIPDataModel model) {
        if (model != null)
        {
            //enterWIPDataModelArrayList.add(model);
        }
    }

    public class GetProductionTicketDetails extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JK_ProductionTicket_Update_Actvity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String url= JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.START_PROD_TICKET_ID_DETAILS + str_tk_id + "&flag=" + "Startbtn";
            response= URL_Constants.makeHttpPostRequest(url);
            Log.d(JK_URL_Constants.TAG, "Whole data = " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();

            if (!response.equals("Error"))
            {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray array=jsonObject.getJSONArray("Data");
                    ticketmodel.clear();
                    if (array.length()==0)
                    {
                        getViewData();

                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }

                    else {

                        for (int i=0;i<array.length();i++) {
                            JSONObject obj = new JSONObject(array.get(i).toString());
                            ProdTkOperationModel model = new ProdTkOperationModel();

                            if (obj.has("tk_Actual_End_Date"))
                            {
                                model.setPtk_PtkdId(obj.getInt("ptkd_Id"));
                                model.setPtkd_TkNo(obj.getString("ptkd_TkNo"));
                                model.setPtkd_PSKU(obj.getString("ptkd_PSKU"));
                                model.setPtkd_Plant(obj.getString("ptkd_Plant"));
                                model.setPtkd_ValueStreamCode(obj.getString("ptkd_ValueStreamCode"));
                                model.setPtkd_Ftypecode(obj.getString("ptkd_Ftypecode"));
                                model.setPtkd_Operation(obj.getInt("ptkd_Operation"));
                                model.setPtkd_Qty(obj.getInt("ptkd_Qty"));
                                model.setPtkd_IsRelease(obj.getString("ptkd_IsRelease"));
                                model.setUni_unit_name(obj.getString("uni_unit_name"));
                                model.setOpr_operation_name(obj.getString("opr_operation_name"));
                                model.setVal_valuestream_name(obj.getString("val_valuestream_name"));
                                model.setFt_ftype_desc(obj.getString("ft_ftype_desc"));
                                model.setTk_Actual_Start_Date(obj.getString("tk_Actual_Start_Date"));
                                model.setTk_Actual_End_Date(obj.getString("tk_Actual_End_Date"));
                                model.setPtkd_ReleaseDate(obj.getString("ptkd_ReleaseDate"));
                                model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                                model.setTk_End_Date(obj.getString("tk_End_Date"));
                                model.setFlag(obj.getString("flag"));
                                model.setVsCode(obj.getString("ptkd_ValueStreamCode"));
                                model.setPtk_ChangeRoute_Flag("C");

                                if (obj.getString("tk_Actual_Start_Date") != null && !obj.getString("tk_Actual_Start_Date").equals("null"))
                                {
                                    img_start_ticket.setVisibility(View.INVISIBLE);
                                    txt_up_img_starttk.setVisibility(View.INVISIBLE);
                                    start_date = obj.getString("tk_Actual_Start_Date");
                                    img_enter_wip.setVisibility(View.INVISIBLE);
                                    txt_up_img_starttk.setVisibility(View.INVISIBLE);
                                    txt_enter_wip.setVisibility(View.INVISIBLE);
                                }
                                else {
                                    img_start_ticket.setVisibility(View.VISIBLE);
                                    txt_up_img_starttk.setVisibility(View.VISIBLE);
                                }

                                if (obj.getString("tk_Actual_End_Date") != null && !obj.getString("tk_Actual_End_Date").equals("null"))
                                {
                                    img_end_ticket.setVisibility(View.INVISIBLE);
                                    txt_img_up_end_ticket.setVisibility(View.INVISIBLE);
                                    end_date = obj.getString("tk_Actual_End_Date");
                                }
                                else {
                                    img_end_ticket.setVisibility(View.VISIBLE);
                                    txt_img_up_end_ticket.setVisibility(View.VISIBLE);
                                }

                                if(obj.getString("tk_Actual_Start_Date") != null &&!obj.getString("tk_Actual_Start_Date").equals("null"))
                                {
                                    txt_up_act_strat_date.setText(obj.getString("tk_Actual_Start_Date"));
                                }
                                else
                                {
                                    txt_up_act_strat_date.setText("NA");

                                }
                                if(!obj.getString("tk_Actual_End_Date").equals("null"))
                                {
                                    txt_up_act_end_date.setText(obj.getString("tk_Actual_End_Date"));
                                }
                                else
                                {
                                    txt_up_act_end_date.setText("NA");

                                }
                                txt_up_ticket_no.setText(obj.getString("ptkd_TkNo"));
                                txt_up_plant.setText(obj.getString("ptkd_Plant"));
                                txt_up_valuestream.setText(obj.getString("val_valuestream_name"));
                                txt_up_tk_filetype.setText(obj.getString("ft_ftype_desc"));
                                txt_sku_8.setText(obj.getString("ptkd_PSKU"));
                                txt_up_startdate.setText(obj.getString("tk_Start_Date"));
                                txt_up_enddate.setText(obj.getString("tk_End_Date"));
                                //txt_up_tk_qty.setText(obj.getString("ptkd_Qty").concat(" no"));
                                //txt_updated_griddate.setText(obj.getString("tk_Actual_Start_Date"));
                                //txt_up_Release_qty.setText(obj.getString("ptkd_Qty").concat(" no"));
                                ticketmodel.add(model);
                            }

                            else
                            {
                                model.setPtk_PtkdId(obj.getInt("ptkd_Id"));
                                model.setPtkd_TkNo(obj.getString("ptkd_TkNo"));
                                model.setPtkd_PSKU(obj.getString("ptkd_PSKU"));
                                model.setPtkd_Plant(obj.getString("ptkd_Plant"));
                                model.setPtkd_ValueStreamCode(obj.getString("ptkd_ValueStreamCode"));
                                model.setPtkd_Ftypecode(obj.getString("ptkd_Ftypecode"));
                                model.setPtkd_Operation(obj.getInt("ptkd_Operation"));
                                model.setPtkd_Qty(obj.getInt("ptkd_Qty"));
                                model.setPtkd_IsRelease(obj.getString("ptkd_IsRelease"));
                                model.setUni_unit_name(obj.getString("uni_unit_name"));
                                model.setOpr_operation_name(obj.getString("opr_operation_name"));
                                model.setVal_valuestream_name(obj.getString("val_valuestream_name"));
                                model.setFt_ftype_desc(obj.getString("ft_ftype_desc"));
                                model.setTk_Actual_Start_Date(obj.getString("tk_Actual_Start_Date"));
                                model.setTk_Actual_End_Date("null");
                                model.setPtkd_ReleaseDate(obj.getString("ptkd_ReleaseDate"));
                                model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                                model.setTk_End_Date(obj.getString("tk_End_Date"));
                                model.setFlag(obj.getString("flag"));
                                model.setVsCode(obj.getString("ptkd_ValueStreamCode"));
                                model.setPtk_ChangeRoute_Flag("C");


                                if (obj.getString("tk_Actual_Start_Date") != null && !obj.getString("tk_Actual_Start_Date").equals("null"))
                                {
                                    img_start_ticket.setVisibility(View.INVISIBLE);
                                    txt_up_img_starttk.setVisibility(View.INVISIBLE);
                                    start_date = obj.getString("tk_Actual_Start_Date");
                                    img_enter_wip.setVisibility(View.INVISIBLE);
                                    txt_up_img_starttk.setVisibility(View.INVISIBLE);
                                    txt_enter_wip.setVisibility(View.INVISIBLE);
                                }
                                else {
                                    img_start_ticket.setVisibility(View.VISIBLE);
                                    txt_up_img_starttk.setVisibility(View.VISIBLE);
                                }
                                if(obj.getString("tk_Actual_Start_Date") != null &&!obj.getString("tk_Actual_Start_Date").equals("null"))
                                {
                                    txt_up_act_strat_date.setText(obj.getString("tk_Actual_Start_Date"));
                                }
                                else
                                {
                                    txt_up_act_strat_date.setText("NA");

                                }
                                txt_up_act_end_date.setText("NA");

                                txt_up_ticket_no.setText(obj.getString("ptkd_TkNo"));
                                txt_up_plant.setText(obj.getString("ptkd_Plant"));
                                txt_up_valuestream.setText(obj.getString("val_valuestream_name"));
                                txt_up_tk_filetype.setText(obj.getString("ft_ftype_desc"));
                                txt_sku_8.setText(obj.getString("ptkd_PSKU"));
                                txt_up_startdate.setText(obj.getString("tk_Start_Date"));
                                txt_up_enddate.setText(obj.getString("tk_End_Date"));
                                //txt_up_tk_qty.setText(obj.getString("ptkd_Qty").concat(" no"));
                                //txt_updated_griddate.setText(obj.getString("tk_Actual_Start_Date"));
                                ticketmodel.add(model);
                            }


                            getViewData();
                            if (getTicketList.size()==0)
                            {
                                ArrayList<ProdTkOperationModel> list = ticketmodel;
                                LinearLayoutManager manager = new LinearLayoutManager(JK_ProductionTicket_Update_Actvity.this);
                                rv_update_pro_ticket.setLayoutManager(manager);
                                newTicketListAdapter = new NewTicketListAdapter(JK_ProductionTicket_Update_Actvity.this,ticketmodel, JK_ProductionTicket_Update_Actvity.this,str_plant_code);
                                rv_update_pro_ticket.setAdapter(newTicketListAdapter);
                            }
                        }

                        /* ArrayList<ProdTkOperationModel> list = ticketmodel;
                        LinearLayoutManager manager = new LinearLayoutManager(JK_ProductionTicket_Update_Actvity.this);
                        rv_update_pro_ticket.setLayoutManager(manager);
                        newTicketListAdapter = new NewTicketListAdapter(JK_ProductionTicket_Update_Actvity.this,ticketmodel,JK_ProductionTicket_Update_Actvity.this,str_plant_code);
                        rv_update_pro_ticket.setAdapter(newTicketListAdapter);*/
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }}
    }

    public class EndProductionTicketCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JK_ProductionTicket_Update_Actvity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String url= JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.END_PROD_TICKET + str_tk_id ;
            response= URL_Constants.makeHttpPutRequest(url);
            Log.d(JK_URL_Constants.TAG, "Whole data = " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if(!response.equals("Error"))
            {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    String message=jsonObject.getString("message");
                    String status=jsonObject.getString("status");

                    if (message.equals("Success"))
                    {
                       /* Intent i=new Intent(JK_ProductionTicket_Update_Actvity.this,JK_Production_Selection_list.class);
                        startActivity(i);*/
                        Toast.makeText(JK_ProductionTicket_Update_Actvity.this, "This ticket is Ended", Toast.LENGTH_SHORT).show();

                        getViewData();
                    }
                    else
                    {
                        Toast.makeText(JK_ProductionTicket_Update_Actvity.this, "This ticket is not Ended", Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }


            }

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(JK_ProductionTicket_Update_Actvity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);
        setData();
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });
        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(JK_ProductionTicket_Update_Actvity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayEnterWIp() {

        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_enter_wip_data_layout, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        Button alert_button= dialogView.findViewById(R.id.btn_wip_save);
        Button alert_cancel= dialogView.findViewById(R.id.btn_wip_cancel);
        rv_enterwipdetails= (RecyclerView)dialogView.findViewById(R.id.rv_wip_details_Data);
        getEnterWipData();

        alert_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SaveEnterWipDataCall().execute();
            }
        });
        alertDialog = builder.create();

        alertDialog.show();
        alertDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

        alert_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    private void getEnterWipData() {

        API api = Retroconfig.retrofit().create(API.class);
        Call<ResponseBody> call = api.getEnterWipData(str_tk_no);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if(response.body()!=null)
                    {}
                    String res = response.body().string();
                    if (res!=null){
                        JSONObject object = new JSONObject(res);
                        JSONArray array = object.getJSONArray("Data");
                        enterWIPDataModelArrayList.clear();
                        if (array.length()!=0){
                            for (int i = 0 ; i < array.length() ; i++){
                                JSONObject obj = array.getJSONObject(i);
                                EnterWIPDataModel model = new EnterWIPDataModel();
                                model.setPtkd_Id(obj.getString("ptkd_Id"));
                                model.setPtkd_PktId(obj.getString("ptkd_PktId"));
                                model.setPtkd_TkNo(obj.getString("ptkd_TkNo"));
                                model.setPtkd_PSKU(obj.getString("ptkd_PSKU"));
                                model.setPtkd_Plant(obj.getString("ptkd_Plant"));
                                model.setPtkd_ValueStreamCode(obj.getString("ptkd_ValueStreamCode"));
                                model.setPtkd_Operation(obj.getString("ptkd_Operation"));
                                if (obj.has("ptkd_WIP"))
                                {
                                    model.setPtkd_WIP(obj.getString("ptkd_WIP"));
                                }
                                else
                                {
                                    model.setPtkd_WIP("0");
                                }
                                model.setOpr_operation_name(obj.getString("opr_operation_name"));
                                model.setUni_unit_name(obj.getString("uni_unit_name"));
                                model.setPtkd_Qty(obj.getString("ptkd_Qty"));
                                enterWIPDataModelArrayList.add(model);
                            }
                        }
                        ArrayList<EnterWIPDataModel> list = enterWIPDataModelArrayList;
                        LinearLayoutManager manager = new LinearLayoutManager(JK_ProductionTicket_Update_Actvity.this);
                        rv_enterwipdetails.setLayoutManager(manager);
                        enterWipAdapter = new EnterWipAdapter(JK_ProductionTicket_Update_Actvity.this , enterWIPDataModelArrayList, JK_ProductionTicket_Update_Actvity.this);
                        rv_enterwipdetails.setAdapter(enterWipAdapter);

                    }
                    else {
                    }
                }
                catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(JK_ProductionTicket_Update_Actvity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();

                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));


                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout()
    {
        finish();
        Toast toast= Toast.makeText(JK_ProductionTicket_Update_Actvity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(JK_ProductionTicket_Update_Actvity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);

        //Preferences.Logout();

    }

    public class GetSkuOfTickets extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JK_ProductionTicket_Update_Actvity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String url= JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.GETSKUBLACKIDIZING + str_tk_id ;
            response= URL_Constants.makeHttpPostRequest(url);
            Log.d(JK_URL_Constants.TAG, "Whole data = " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("ERROR"))
            {
                try {

                    JSONObject object=new JSONObject(response);
                    JSONArray array= object.getJSONArray("Data");
                    int orgqty = 0;
                    int releaseqty = 0;
                    ArrayList<SkuDetailsModel> skuDetailsModelArrayList = new ArrayList<>();
                    if (array.length()==0){
                       /* Toast toast = Toast.makeText(getApplicationContext(),
                           "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();*/
                    }
                    else
                    {
                        for (int i=0;i<array.length();i++)
                        {
                            JSONObject obj=new JSONObject(array.get(i).toString());
                            SkuDetailsModel model = new SkuDetailsModel();

                            ContentValues cv=new ContentValues();
                            if(obj.has("SKU_No"))
                            {
                                model.setSKU_No(obj.getString("SKU_No"));
                            }
                            else
                            {
                                model.setSKU_No("-");
                            }
                            if (obj.has("dp_stamp_type")){
                                model.setDp_stamp_type(obj.getString("dp_stamp_type"));
                            }
                            else {                               model.setDp_stamp_type("-");

                            }
                            if (obj.has("dp_stamp_type")){
                                model.setDp_stamp_chart(obj.getString("dp_stamp_chart"));
                            }
                            else {
                                model.setDp_stamp_chart("-");


                            }
                            if (obj.has("dp_blko_flg")){
                                model.setDp_blko_flg(obj.getString("dp_blko_flg"));
                            }
                            else {                             model.setDp_blko_flg("-");


                            }
                            if (obj.has("dp_tang_color")){
                                model.setDp_tang_color(obj.getString("dp_tang_color"));
                            }
                            else {                              model.setDp_tang_color("-");


                            }
                            if (obj.has("QTY")){
                                model.setQTY(obj.getString("QTY"));
                                //orgqty = Integer.parseInt(obj.getString("QTY"));
                                orgqty +=Integer.parseInt(obj.getString("QTY"));
                                //txt_up_tk_qty.setText(String.valueOf(orgqty));
                            }
                            else {    model.setQTY("-"); }
                            if (obj.has("OrgQTY")){
                                model.setReleaseQty(obj.getString("OrgQTY"));
                              //  int releaseqty = Integer.parseInt(obj.getString("OrgQTY"));
                                releaseqty +=Integer.parseInt(obj.getString("OrgQTY"));
                                txt_up_Release_qty.setText(String.valueOf(releaseqty));
                            }
                            else {  model.setReleaseQty("-");
                            }
                            skuDetailsModelArrayList.add(model);
                        }
                        LinearLayoutManager manager = new LinearLayoutManager(JK_ProductionTicket_Update_Actvity.this);
                        rv_sku_details.setLayoutManager(manager);
                        sku_adapter = new SKU_Adapter(JK_ProductionTicket_Update_Actvity.this,skuDetailsModelArrayList);
                        rv_sku_details.setAdapter(sku_adapter);


                    }

                }
                catch (Exception e)
                {
                    e.printStackTrace();}
            }

        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id== R.id.nav_planning)
        {
            Intent i=new Intent(JK_ProductionTicket_Update_Actvity.this, FalcoJKHomeActivity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }
    public void showbottomsheetdialog(final ProdTkOperationModel model)
    {
        mBottomSheetDialog = new BottomSheetDialog(JK_ProductionTicket_Update_Actvity.this);
        View modelBottomSheet = LayoutInflater.from(JK_ProductionTicket_Update_Actvity.this).inflate(R.layout.layout_downtime_prodution, null);

        mBottomSheetDialog.setContentView(modelBottomSheet);
        spin_equipment= mBottomSheetDialog.findViewById(R.id.spin_equip_name);
        spin_equip_type= mBottomSheetDialog.findViewById(R.id.spin_equi_type);
        spin_select_operation= mBottomSheetDialog.findViewById(R.id.spin_opearation);
        spin_spin_asset_no=mBottomSheetDialog.findViewById(R.id.spin_asset_no);

        ImageButton img_btn_cancel=(ImageButton)mBottomSheetDialog.findViewById(R.id.img_downtime_cancel);
        ImageButton img_btn_submit=(ImageButton)mBottomSheetDialog.findViewById(R.id.img_downtime_submit);
        final EditText ed_no_of_shifts=(EditText)mBottomSheetDialog.findViewById(R.id.ed_no_of_shifts);
        final EditText ed_remark=(EditText)mBottomSheetDialog.findViewById(R.id.ed_remark);
        TextView operationname=(TextView)mBottomSheetDialog.findViewById(R.id.operationname);
        String changerout="Downtime for ";
        operationname.setText(changerout.concat(model.getOpr_operation_name()));
        str_valuestream_code=model.getVsCode();
        str_operation_no=model.getPtkd_Operation();
        str_ticketIDNo=model.getPtkd_TkNo();
        str_plant_code=model.getPtkd_Plant();
        str_plant=model.getPtkd_Plant();

        img_btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //str_operation_no=model.getPtk_Operation();
                str_plant=model.getPtk_Plant();
                str_ticketIDNo=model.getPtkd_TkNo();
                te_NoofShipt=ed_no_of_shifts.getText().toString().trim();
                cRRemark=ed_remark.getText().toString().trim();
                str_valuestream_code=model.getVsCode();

                new DowntimeCall().execute();
            }
        });
        img_btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });
        final ArrayList<String> operation = new ArrayList<>();
        operation.add("Select Operation");
        operation.add("Breakdown");
        operation.add("Abseentism");
        operation.add("Quality");
        operation.add("Setting time");
        operation.add("Feed not available");
        operation.add("Other");
        SpinnerAdapter arrayAdapter = new SpinnerAdapter(JK_ProductionTicket_Update_Actvity.this, operation);
        spin_select_operation.setAdapter(arrayAdapter);

        spin_select_operation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                models.clear();
                String string="Reason";
                te_ReseasenDt=string.concat(operation.get(i));
                new GetOperationEquipment().execute();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spin_equipment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //equipId=equipTypelist.get(i).getEq_id();
                // String eqip_id=models.get(i).getEq_id();
                equipTypelist.clear();
                str_equip_name=models.get(i).getEq_name();
                new GetQuipmentType().execute();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spin_equip_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                assetNoModels.clear();
                str_eqip_id=equipTypelist.get(i).getEq_id();
                new GetAssetNo().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spin_spin_asset_no.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    te_AssetNo=assetNoModels.get(i).getAssetno();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mBottomSheetDialog.show();

    }

    public class GetOperationEquipment extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JK_ProductionTicket_Update_Actvity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String user= JK_URL_Constants.PLANT_USER;
            //String user="Pithampur";
            String url= JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.GET_OPERATION_EQUIPMENT + str_valuestream_code + "&plantCode=" + str_plant_code + "&operationNo=" + str_operation_no;
            response= URL_Constants.makeHttpPostRequest(url);
            Log.d(JK_URL_Constants.TAG, "Whole data = " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray array=jsonObject.getJSONArray("Data");
                    if (array.length()==0)
                    {
                        Toast toast = Toast.makeText(JK_ProductionTicket_Update_Actvity.this,
                                "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();

                    }
                    else {
                        if (array.length() > 0) {
                            OperationEquipModel model=new OperationEquipModel();
                            model.setEq_name("Select Equipment Name");
                            models.add(model);
                        }


                        for (int i=0;i<array.length();i++)
                        {
                            OperationEquipModel model=new OperationEquipModel();
                            JSONObject object=array.getJSONObject(i);

                            model.setEq_id(object.getString("Eq_id"));
                            model.setEq_name(object.getString("eq_name"));
                            models.add(model);
                        }
                        opearionEqupSpinAdapter=new OpearionEqupSpinAdapter(JK_ProductionTicket_Update_Actvity.this,models);
                        spin_equipment.setAdapter(opearionEqupSpinAdapter);

                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    public class GetQuipmentType extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JK_ProductionTicket_Update_Actvity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";

            String url= JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.GET_EQUIPMENT_WISE_TYPE + str_valuestream_code + "&plantCode=" + str_plant_code + "&OpretionNo=" + str_operation_no + "&equipmentName=" + str_equip_name;
            String flag1 = url.replaceAll(" ", "%20");
            response= URL_Constants.makeHttpPostRequest(flag1);
            Log.d(JK_URL_Constants.TAG, "Whole data = " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray array= jsonObject.getJSONArray("Data");
                    if (array.length()==0)
                    {
                        Toast.makeText(JK_ProductionTicket_Update_Actvity.this, "Sorry no data available", Toast.LENGTH_SHORT).show();
                    }
                    else {

                        if (array.length() > 0) {
                            EquipTypeModel model=new EquipTypeModel();
                            model.setEq_type("Select Equipment Type");
                            equipTypelist.add(model);
                        }


                        for (int i=0;i<array.length();i++)
                        {
                            EquipTypeModel model=new EquipTypeModel();
                            JSONObject obj=array.getJSONObject(i);
                            model.setEq_id(obj.getString("Eq_id"));
                            model.setEq_type(obj.getString("eq_type"));

                            equipTypelist.add(model);
                        }
                        equipTypeSpinAdapter=new EquipTypeSpinAdapter(JK_ProductionTicket_Update_Actvity.this,equipTypelist);
                        spin_equip_type.setAdapter(equipTypeSpinAdapter);


                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }


            }
        }
    }

    public class GetAssetNo extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JK_ProductionTicket_Update_Actvity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String url= JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.GET__EQUIPMENT_ASSET_NO + str_valuestream_code + "&plantCode=" + str_plant_code + "&Euid=" + str_eqip_id;
            //String flag1 = url.replaceAll(" ", "%20");
            response= URL_Constants.makeHttpPostRequest(url);
            Log.d(JK_URL_Constants.TAG, "Whole data = " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray array= jsonObject.getJSONArray("Data");
                    if (array.length()==0)
                    {
                        Toast.makeText(JK_ProductionTicket_Update_Actvity.this, "Sorry no data available", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        if (array.length() > 0) {
                            AssetNoModel model=new AssetNoModel();
                            model.setAssetno("Select Asset No");
                            assetNoModels.add(model);
                        }
                        for (int i=0;i<array.length();i++)
                        {
                            AssetNoModel model=new AssetNoModel();
                            JSONObject object = array.getJSONObject(i);
                            model.setAssetno(object.getString("aa_asset_no"));
                            assetNoModels.add(model);
                        }
                        assetNoAdapter=new AssetNoAdapter(JK_ProductionTicket_Update_Actvity.this,assetNoModels);
                        spin_spin_asset_no.setAdapter(assetNoAdapter);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }


            }
        }
    }

    public class DowntimeCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JK_ProductionTicket_Update_Actvity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            String response="";
            String url= JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.SAVE_DOWNTIME_OPERATION + "CHNM" + "&operationNo=" + str_operation_no
                    + "&te_EquiId=" + str_eqip_id + "&ticketIDNo=" + str_ticketIDNo + "&te_ReseasenDt=" + "Reason4" + "&te_NoofShipt=" + te_NoofShipt + "&te_Ab_Br_Qt=" + "0"
                    + "&te_MovedQuntity=" + "0" + "&te_AssetNo=" + te_AssetNo + "&userId=" + JK_URL_Constants.PLANT_USER + "&cRRemark=" + cRRemark + "&te_TE_Flag=" + "Downtime";

            String str=url.replaceAll(" ","%20");
            response= URL_Constants.makeHttpPostRequest(str);
            Log.d(JK_URL_Constants.TAG, "Whole data = " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try {
                    JSONObject object=new JSONObject(response);
                    String status=object.getString("status");
                    //int data=object.getInt("data");
                    Toast.makeText(JK_ProductionTicket_Update_Actvity.this, "Successfully insert", Toast.LENGTH_SHORT).show();
                    mBottomSheetDialog.dismiss();

                    if (status.equals("true"))
                    {
                        Toast.makeText(JK_ProductionTicket_Update_Actvity.this, "Successfully insert", Toast.LENGTH_SHORT).show();
                        mBottomSheetDialog.dismiss();
                    }

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

        }
    }

    public class SaveEnterWipDataCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JK_ProductionTicket_Update_Actvity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String getResponse="";
            String url="https://fgmobileapi.decintell.co.in/PLN/api/UpdateTicketWIP1";

            try {
                HttpParams httpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(httpParams, 60000);
                HttpConnectionParams.setSoTimeout(httpParams, 60000);
                HttpClient client = new DefaultHttpClient(httpParams);
                HttpPost httpPost = new HttpPost(url);
                httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
                JSONArray array=new JSONArray();

                for (int i= 0;i<enterWIPDataModelArrayList.size();i++)
                {
                    JSONObject object=new JSONObject();
                    object.put("ptkd_Id",enterWIPDataModelArrayList.get(i).getPtkd_Id());
                    if (enterWIPDataModelArrayList.get(i).getPtkd_WIP().equals("null") || enterWIPDataModelArrayList.get(i).getPtkd_WIP().equals(""))
                    {
                        object.put("ptkd_WIP","0");
                    }
                    else
                    {
                        object.put("ptkd_WIP",enterWIPDataModelArrayList.get(i).getPtkd_WIP());
                    }

                    object.put("ptkd_TkNo",enterWIPDataModelArrayList.get(i).getPtkd_TkNo());

                    array.put(object);
                }
                httpPost.setEntity(new StringEntity(array.toString(),"UTF-8"));
                httpPost.setParams(httpParams);

                HttpResponse response = client.execute(httpPost);
                StatusLine statusLine = response.getStatusLine();

                if (statusLine.getStatusCode() == HttpStatus.SC_OK || statusLine.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();

                    getResponse = URL_Constants.convertStreamToString(inputStream);
                }
                else if (statusLine.getStatusCode()==HttpStatus.SC_UNAUTHORIZED)
                {
                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();

                    getResponse = URL_Constants.convertStreamToString(inputStream);
                }
                else {
                    response.getEntity().getContent().close();
                }
            }

            catch (Exception e) {
                Log.i(URL_Constants.TAG, "Error Message in Exception :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_EXCEPTION;

            }
            return getResponse;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                if (response.equals(""))
                {
                    Toast.makeText(JK_ProductionTicket_Update_Actvity.this,"Successfully inserted",Toast.LENGTH_SHORT).show();
                    img_start_ticket.setEnabled(true);
                    img_enter_wip.setVisibility(View.INVISIBLE);
                    txt_enter_wip.setVisibility(View.INVISIBLE);
                    txt_up_img_starttk.setClickable(true);
                    alertDialog.dismiss();
                }
                else
                {
                    Toast.makeText(JK_ProductionTicket_Update_Actvity.this,"Successfully inserted",Toast.LENGTH_SHORT).show();
                    img_start_ticket.setEnabled(true);
                    img_enter_wip.setVisibility(View.INVISIBLE);
                    txt_enter_wip.setVisibility(View.INVISIBLE);
                    txt_up_img_starttk.setClickable(true);
                    alertDialog.dismiss();
                }

            }

        }

    }

    public class GetCurrentSkudetailsForEnterWipData extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(JK_ProductionTicket_Update_Actvity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String url= JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.START_PROD_TICKET_ID_DETAILS + str_tk_id  + "&flag=" + "null" ;
            response= URL_Constants.makeHttpPostRequest(url);
            Log.d(JK_URL_Constants.TAG, "Whole data = " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            progressDialog.dismiss();

            if (!response.equals("Error"))
            {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray array=jsonObject.getJSONArray("Data");
                    if (array.length()==0){
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                    else {

                        for (int i=0;i<array.length();i++) {
                            JSONObject obj = new JSONObject(array.get(i).toString());
                            ProdTkOperationModel model = new ProdTkOperationModel();
                            model.setPtk_PtkdId(obj.getInt("ptkd_Id"));
                            model.setPtkd_TkNo(obj.getString("ptkd_TkNo"));
                            model.setPtkd_PSKU(obj.getString("ptkd_PSKU"));
                            model.setPtkd_Plant(obj.getString("ptkd_Plant"));
                            model.setPtkd_ValueStreamCode(obj.getString("ptkd_ValueStreamCode"));
                            model.setPtkd_Ftypecode(obj.getString("ptkd_Ftypecode"));
                            model.setPtkd_Operation(obj.getInt("ptkd_Operation"));
                            model.setPtkd_Qty(obj.getInt("ptkd_Qty"));
                            model.setPtkd_IsRelease(obj.getString("ptkd_IsRelease"));
                            model.setUni_unit_name(obj.getString("uni_unit_name"));
                            model.setOpr_operation_name(obj.getString("opr_operation_name"));
                            model.setVal_valuestream_name(obj.getString("val_valuestream_name"));
                            model.setFt_ftype_desc(obj.getString("ft_ftype_desc"));
                            model.setTk_Actual_Start_Date(obj.getString("tk_Actual_Start_Date"));
                            model.setTk_Actual_End_Date(obj.getString("tk_Actual_End_Date"));
                            model.setPtkd_ReleaseDate(obj.getString("ptkd_ReleaseDate"));
                            model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                            model.setTk_End_Date(obj.getString("tk_End_Date"));
                            model.setFlag(obj.getString("flag"));
                            model.setVsCode(obj.getString("ptkd_ValueStreamCode"));
                            model.setCurrentProdSKUExist(obj.getString("CurrentProdSKUExist"));
                            String current_sku = obj.getString("CurrentProdSKUExist");
                            String na= "NA";

                            if (obj.getString("CurrentProdSKUExist").equals(na))
                            {
                                img_enter_wip.setVisibility(View.INVISIBLE);
                                txt_enter_wip.setVisibility(View.INVISIBLE);
                                img_start_ticket.setEnabled(true);
                                txt_up_img_starttk.setClickable(true);
                            }
                            else
                            {
                                img_enter_wip.setVisibility(View.VISIBLE);
                                txt_enter_wip.setVisibility(View.VISIBLE);
                                img_start_ticket.setEnabled(false);
                                txt_up_img_starttk.setClickable(false);
                            }

                           /* if (obj.getString("tk_Actual_Start_Date") != null && !obj.getString("tk_Actual_Start_Date").equals("null")) {

                                img_start_ticket.setVisibility(View.VISIBLE);
                                txt_up_img_starttk.setVisibility(View.VISIBLE);
                            }*/
                           /* else
                            {
                                img_start_ticket.setVisibility(View.VISIBLE);
                                txt_up_img_starttk.setVisibility(View.VISIBLE);
                            }*/

                            if (obj.getString("tk_Actual_End_Date") != null && !obj.getString("tk_Actual_End_Date").equals("null"))
                            {
                                img_end_ticket.setVisibility(View.INVISIBLE);
                                txt_img_up_end_ticket.setVisibility(View.INVISIBLE);
                                end_date = obj.getString("tk_Actual_End_Date");

                            }
                            else {
                                img_end_ticket.setVisibility(View.VISIBLE);
                                txt_img_up_end_ticket.setVisibility(View.VISIBLE);
                            }
                            /*if (obj.getString("tk_Actual_End_Date")!=null || !obj.getString("tk_Actual_End_Date").equals(""))
                            {
                               img_end_ticket.setVisibility(View.INVISIBLE);
                               txt_img_up_end_ticket.setVisibility(View.INVISIBLE);
                                end_date = obj.getString("tk_Actual_End_Date");
                            }
                            */

                           /* if(!obj.getString("tk_Actual_Start_Date").equals("null"))
                            {
                                txt_up_act_strat_date.setText(obj.getString("tk_Actual_Start_Date"));
                            }*/
                           /* else
                            {
                                txt_up_act_strat_date.setText("NA");

                            }*/
                            if(!obj.getString("tk_Actual_End_Date").equals("null"))
                            {
                                txt_up_act_end_date.setText(obj.getString("tk_Actual_End_Date"));
                            }
                            else
                            {
                                txt_up_act_end_date.setText("NA");

                            }
                            txt_up_ticket_no.setText(obj.getString("ptkd_TkNo"));
                            txt_up_plant.setText(obj.getString("ptkd_Plant"));
                            txt_up_valuestream.setText(obj.getString("val_valuestream_name"));
                            txt_up_tk_filetype.setText(obj.getString("ft_ftype_desc"));
                            txt_sku_8.setText(obj.getString("ptkd_PSKU"));
                            txt_up_startdate.setText(obj.getString("tk_Start_Date"));
                            txt_up_enddate.setText(obj.getString("tk_End_Date"));
                            txt_up_tk_qty.setText(obj.getString("ptkd_Qty").concat(" no"));
                            //txt_updated_griddate.setText(obj.getString("tk_Actual_Start_Date"));
                            ticketmodel.add(model);
                        }

                      /*  ArrayList<ProdTkOperationModel> list = ticketmodel;
                        LinearLayoutManager manager = new LinearLayoutManager(JK_ProductionTicket_Update_Actvity.this);
                        rv_update_pro_ticket.setLayoutManager(manager);
                        newTicketListAdapter = new NewTicketListAdapter(JK_ProductionTicket_Update_Actvity.this,ticketmodel,JK_ProductionTicket_Update_Actvity.this);
                        rv_update_pro_ticket.setAdapter(newTicketListAdapter);*/

                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }}
        }
    }






