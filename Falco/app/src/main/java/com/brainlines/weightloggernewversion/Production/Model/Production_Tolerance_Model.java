package com.brainlines.weightloggernewversion.Production.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Production_Tolerance_Model implements Parcelable {
    String ProductionToleranceID,Grammage,TolerancePositive,ToleranceNegetive,CreatedDate,ModifiedDate;
    int OEMID;
    boolean Isactive,IsDeleted;

    protected Production_Tolerance_Model(Parcel in) {
        ProductionToleranceID = in.readString();
        Grammage = in.readString();
        TolerancePositive = in.readString();
        ToleranceNegetive = in.readString();
        CreatedDate = in.readString();
        ModifiedDate = in.readString();
        OEMID = in.readInt();
        Isactive = in.readByte() != 0;
        IsDeleted = in.readByte() != 0;
    }

    public boolean isIsactive() {
        return Isactive;
    }

    public String getProductionToleranceID() {
        return ProductionToleranceID;
    }

    public void setProductionToleranceID(String productionToleranceID) {
        ProductionToleranceID = productionToleranceID;
    }

    public void setIsactive(boolean isactive) {
        Isactive = isactive;
    }

    public boolean isDeleted() {
        return IsDeleted;
    }

    public void setDeleted(boolean deleted) {
        IsDeleted = deleted;
    }

    public static final Creator<Production_Tolerance_Model> CREATOR = new Creator<Production_Tolerance_Model>() {
        @Override
        public Production_Tolerance_Model createFromParcel(Parcel in) {
            return new Production_Tolerance_Model(in);
        }

        @Override
        public Production_Tolerance_Model[] newArray(int size) {
            return new Production_Tolerance_Model[size];
        }
    };

    public int getOEMID() {
        return OEMID;
    }

    public void setOEMID(int OEMID) {
        this.OEMID = OEMID;
    }

    public Production_Tolerance_Model(String productionTolerance, int OEMID, String grammage, String tolerancePositive, String toleranceNegetive, boolean isactive, boolean isDeleted, String createdDate, String modifiedDate) {
        ProductionToleranceID = productionTolerance;
        this.OEMID = OEMID;
        Grammage = grammage;
        TolerancePositive = tolerancePositive;
        ToleranceNegetive = toleranceNegetive;
        Isactive = isactive;
        IsDeleted = isDeleted;
        CreatedDate = createdDate;
        ModifiedDate = modifiedDate;
    }

    public Production_Tolerance_Model() {

    }


    public String getProductionTolerance() {
        return ProductionToleranceID;
    }

    public void setProductionTolerance(String productionTolerance) {
        ProductionToleranceID = productionTolerance;
    }



    public String getGrammage() {
        return Grammage;
    }

    public void setGrammage(String grammage) {
        Grammage = grammage;
    }

    public String getTolerancePositive() {
        return TolerancePositive;
    }

    public void setTolerancePositive(String tolerancePositive) {
        TolerancePositive = tolerancePositive;
    }

    public String getToleranceNegetive() {
        return ToleranceNegetive;
    }

    public void setToleranceNegetive(String toleranceNegetive) {
        ToleranceNegetive = toleranceNegetive;
    }





    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public static Creator<Production_Tolerance_Model> getCREATOR() {
        return CREATOR;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ProductionToleranceID);
        dest.writeString(Grammage);
        dest.writeString(TolerancePositive);
        dest.writeString(ToleranceNegetive);
        dest.writeString(CreatedDate);
        dest.writeString(ModifiedDate);
        dest.writeInt(OEMID);
        dest.writeByte((byte) (Isactive ? 1 : 0));
        dest.writeByte((byte) (IsDeleted ? 1 : 0));
    }
}
