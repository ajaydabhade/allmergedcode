package com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.InProcessCall;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Feedback_Activity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Home_Actvity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.ShortTermListModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasIdDetailsModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.AppPreferences;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.brainlines.weightloggernewversion.utils.GlobalClass;
import com.google.android.material.navigation.NavigationView;

import org.apache.http.ConnectionReuseStrategy;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.SimpleTimeZone;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Swas_Deatails_of_Inprocess_Call_Activity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "Swas_Deatails_of_Inproc";
    RelativeLayout rl_Details,rl_scheduled_visit,rl_visit_log,rl_machine_para,rl_competitors_para,rl_travelling_expen,rl_report;
    ImageButton cancel,feedback;
    String status_id,service_call_id,type_of_service_status_id,customer_id;
    TextView txt_customer_name,txt_location,txt_call_creadted,txt_created_on,txt_modified_on,txt_service_call_id;

    ImageView img_user_info,nav_view_img,img_back;
    String user_email,user_token,user_role,oem_id;
    DrawerLayout drawer;
    TextView profile_user,profile_role;
    String created_by,service_eng_id,short_resolution,long_resolution;
    DataBaseHelper helper;
    private String locationName;
    private String dbName;
    private SQLiteDatabase db;
    private ArrayList<ShortTermListModel> shortTermListModelArrayList = new ArrayList<>();
    private int strShortResolutionSelectedId;
    private ProgressDialog dialog;
    private String customer_name;
    private String longTermText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swas_details_of_inprocess_call_navigation);
        initUi();
        setData();

        img_back=(ImageView)findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Swas_Deatails_of_Inprocess_Call_Activity.this, Swas_Home_Actvity.class);
                Bundle bundle = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_service_status_id);
                bundle.putString("customer_id",customer_id);
                i.putExtras(bundle);
                startActivity(i);
                finish();
            }
        });
        if (user_email.equals("yogesh@brainlines.in"))
        {
            created_by = "Yogesh Jadhav";
//            service_eng_id = "1";
        }
        else
        {
            created_by = "Deepak Kulkarni";
//            service_eng_id = "2";
        }
        img_user_info=(ImageView)findViewById(R.id.img_userinfo);
        img_user_info.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img=(ImageView)findViewById(R.id.nav_view_img);

        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_planning).setVisible(false);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
        {
            status_id = bundle.getString("status_id");
            service_call_id = bundle.getString("service_call_id");
            type_of_service_status_id = bundle.getString("Typeofservicecall_id");
            customer_id = bundle.getString("customer_id");
            locationName = bundle.getString("location_name");
        }
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
//        if(isConnected) {
//            Log.d("Network", "Connected");
//            Id_Details();
//
//        }
//        else{
//            checkNetworkConnection();
//            Log.d("Network","Not Connected");
//            getDataFromListOfServiceCallByStatus();
//
//        }
        getDataFromListOfServiceCallByStatus();
//        getShortTermResolutionList();
        getShortTermResolutionListFromLocalDB();
        getPermenantCorrectiveReaction();

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View_details();
            }
        });
        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Swas_Deatails_of_Inprocess_Call_Activity.this, Swas_Feedback_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_service_status_id);
                bundle.putString("customer_id",customer_id);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void getPermenantCorrectiveReaction() {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getpermanantcorrectiveaction(Integer.parseInt(service_call_id));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body() != null){
                    try {
                        String res = response.body().string();
                        JSONObject object = new JSONObject(res);
                        switch (object.getString("statusCode")){
                            case "200" :
                                JSONArray jsonArray = object.getJSONArray("data");
                                for (int i = 0 ; i < jsonArray.length() ; i++){
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    if (jsonObject.getString("LongTermResolutionText").equals("")){
                                        longTermText = jsonObject.getString("Permanent Corrective Action Not yet Applied");
                                    }else {
                                        longTermText = jsonObject.getString("LongTermResolutionText");
                                    }
                                }
                                break;
                            default:
                                Log.d(TAG, "onResponse: Unexpected StatusCode");
                                break;
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    private void getShortTermResolutionListFromLocalDB() {
        shortTermListModelArrayList.clear();
        Cursor cursor = helper.getShortTermListFromDB();
        while (cursor.moveToNext()){
            int ShortTermResolutionID = cursor.getInt(0);
            String ShortTermResolutionText = cursor.getString(1);
            shortTermListModelArrayList.add(new ShortTermListModel(ShortTermResolutionID,ShortTermResolutionText));
        }
    }

    private void getShortTermResolutionList() {
        shortTermListModelArrayList.clear();
        dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait");
        dialog.setCancelable(false);
        dialog.show();
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getshorttermresolutionlist();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body() != null){
                    try {
                        String res = response.body().string();
                        JSONObject object = new JSONObject(res);
                        switch (object.getString("statusCode")){
                            case "200" :
                                JSONArray jsonArray = object.getJSONArray("data");
                                for (int i = 0 ; i < jsonArray.length() ; i++){
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    int ShortTermResolutionID = jsonObject.getInt("ShortTermResolutionID");
                                    String ShortTermResolutionText = jsonObject.getString("ShortTermResolutionText");
                                    shortTermListModelArrayList.add(new ShortTermListModel(ShortTermResolutionID,ShortTermResolutionText));
                                }
                                break;
                            default:
                                Log.d(TAG, "onResponse: Unexpected StatusCode");
                                break;
                        }
                        dialog.dismiss();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
            }
        });
    }

    public void initUi()
    {
        helper = new DataBaseHelper(this);
        rl_Details = (RelativeLayout)findViewById(R.id.rl_Details);
        rl_scheduled_visit = (RelativeLayout)findViewById(R.id.rl_scheduled_visit);
        rl_visit_log = (RelativeLayout)findViewById(R.id.rl_visit_log);
        rl_machine_para = (RelativeLayout)findViewById(R.id.rl_machine_para);
        rl_competitors_para = (RelativeLayout)findViewById(R.id.rl_competitors_para);
        rl_travelling_expen = (RelativeLayout)findViewById(R.id.rl_travelling_expen);
        rl_report = (RelativeLayout)findViewById(R.id.rl_report);

        txt_customer_name = (TextView)findViewById(R.id.txt_customer_name);
        //txt_location = (TextView)findViewById(R.id.txt_location);
        txt_call_creadted = (TextView)findViewById(R.id.txt_call_creadted);
        txt_created_on = (TextView)findViewById(R.id.txt_created_on);
        txt_modified_on = (TextView)findViewById(R.id.txt_modified_on);
        txt_service_call_id = (TextView)findViewById(R.id.txt_service_call_id);

        cancel = findViewById(R.id.cancel);
        feedback = findViewById(R.id.feedback);

        rl_Details.setOnClickListener(this);
        rl_scheduled_visit.setOnClickListener(this);
        rl_visit_log.setOnClickListener(this);
        rl_machine_para.setOnClickListener(this);
        rl_competitors_para.setOnClickListener(this);
        rl_travelling_expen.setOnClickListener(this);
        rl_report.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == rl_Details.getId()) {
            Intent i = new Intent(Swas_Deatails_of_Inprocess_Call_Activity.this,Swas_Inprocess_Call_Service_Id_Details_Activity.class);
            Bundle bundle = new Bundle();
            bundle.putString("service_call_id",service_call_id);
            bundle.putString("status_id",status_id);
            bundle.putString("Typeofservicecall_id",type_of_service_status_id);
            bundle.putString("customer_id",customer_id);
            i.putExtras(bundle);
            startActivity(i);
            //View_details();
        }
        else if (view.getId() == rl_scheduled_visit.getId()) {
            Intent i = new Intent( Swas_Deatails_of_Inprocess_Call_Activity.this,ScheduledVisitActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("service_call_id",service_call_id);
            bundle.putString("status_id",status_id);
            bundle.putString("Typeofservicecall_id",type_of_service_status_id);
            bundle.putString("customer_id",customer_id);
            bundle.putString("location_name",locationName);
            i.putExtras(bundle);
            startActivity(i);

        }
        else if (view.getId() == rl_visit_log.getId()) {
            Intent intent = new Intent( Swas_Deatails_of_Inprocess_Call_Activity.this,SwasVisit_Log_Details_Activity.class);
            Bundle bundle = new Bundle();
            bundle.putString("service_call_id",service_call_id);
            bundle.putString("status_id",status_id);
            bundle.putString("Typeofservicecall_id",type_of_service_status_id);
            bundle.putString("customer_id",customer_id);
            intent.putExtras(bundle);
            startActivity(intent);
        }
        else if (view.getId() == rl_machine_para.getId()) {
            Intent intent = new Intent( Swas_Deatails_of_Inprocess_Call_Activity.this,SwasMachineParametersActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("service_call_id",service_call_id);
            bundle.putString("status_id",status_id);
            bundle.putString("Typeofservicecall_id",type_of_service_status_id);
            bundle.putString("customer_id",customer_id);
            intent.putExtras(bundle);
            startActivity(intent);
        }
        else if (view.getId() == rl_competitors_para.getId()) {
            Intent intent = new Intent( Swas_Deatails_of_Inprocess_Call_Activity.this,SwasCompetitors_Parameters_Details_Activity.class);
            Bundle bundle = new Bundle();
            bundle.putString("service_call_id",service_call_id);
            bundle.putString("status_id",status_id);
            bundle.putString("Typeofservicecall_id",type_of_service_status_id);
            bundle.putString("customer_id",customer_id);
            intent.putExtras(bundle);
            startActivity(intent);

        }
        else if (view.getId() == rl_travelling_expen.getId()) {
            Intent intent = new Intent( Swas_Deatails_of_Inprocess_Call_Activity.this,SwasTravellingExpensesDetailsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("service_call_id",service_call_id);
            bundle.putString("status_id",status_id);
            bundle.putString("Typeofservicecall_id",type_of_service_status_id);
            bundle.putString("customer_id",customer_id);
            bundle.putString("customerName",customer_name);
            intent.putExtras(bundle);
            startActivity(intent);

        }
        else if (view.getId() == rl_report.getId()) {
            Intent intent = new Intent( Swas_Deatails_of_Inprocess_Call_Activity.this,Swas_Layout_Upload_Report_Activity.class);
            Bundle bundle = new Bundle();
            bundle.putString("service_call_id",service_call_id);
            bundle.putString("status_id",status_id);
            bundle.putString("Typeofservicecall_id",type_of_service_status_id);
            bundle.putString("customer_id",customer_id);
            intent.putExtras(bundle);
            startActivity(intent);

        }
    }

    public void View_details() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.swas_alert_closed_call, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        ImageButton button = (ImageButton)dialogView.findViewById(R.id.btnSubmit);
        final EditText ed_remark = (EditText)dialogView.findViewById(R.id.ed_remark);
        final Spinner spinner_shortresolution = (Spinner) dialogView.findViewById(R.id.spinner_shorttermresolution);
        final TextView txtLongTermText = (TextView) dialogView.findViewById(R.id.txtLongTermText);
        txtLongTermText.setText(longTermText);
        ArrayAdapter<ShortTermListModel> adapter = new ArrayAdapter<ShortTermListModel>(getApplicationContext(),  android.R.layout.simple_spinner_dropdown_item, shortTermListModelArrayList);
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);
        spinner_shortresolution.setAdapter(adapter);
        spinner_shortresolution.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                ShortTermListModel user = (ShortTermListModel) parent.getSelectedItem();
                strShortResolutionSelectedId = user.getShortTermResolutionID();
            }
            public void onNothingSelected(AdapterView<?> parent) {
                strShortResolutionSelectedId = -1;
            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String remark = ed_remark.getText().toString();
                int sht = strShortResolutionSelectedId;
//                if (longTermText.equals("") || longTermText==null || strShortResolutionSelectedId == -1){
//                    Toast.makeText(Swas_Deatails_of_Inprocess_Call_Activity.this, "Temporary Corrective Action Text", Toast.LENGTH_SHORT).show();
//                }else if (remark.equals("")){
//                    Toast.makeText(Swas_Deatails_of_Inprocess_Call_Activity.this, "Please Enter Remark", Toast.LENGTH_SHORT).show();
//                }else {
//                    Log.d(TAG, "onClick: ");
//                    alertDialog.dismiss();
//                    closedCall(remark,strShortResolutionSelectedId);
//                }
                alertDialog.dismiss();
                closedCall(remark,strShortResolutionSelectedId);
            }
        });
    }

    private void Id_Details() {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getalldetailsofsingleservicecall(service_call_id,status_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {

                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            ArrayList<SwasIdDetailsModel> model = new ArrayList<>();
                            if (array.length() != 0) {

                                for (int i = 0; i < array.length(); i++) {
                                    SwasIdDetailsModel swasIdDetailsModel = new SwasIdDetailsModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    swasIdDetailsModel.setService_Id(object1.getString("ServiceCallID"));
                                    swasIdDetailsModel.setLOCATIONNAME(object1.getString("LOCATIONNAME"));
                                    swasIdDetailsModel.setCreatedBy(object1.getString("CreatedBy"));
                                    swasIdDetailsModel.setCreatedDate(object1.getString("CreatedDate"));
                                    swasIdDetailsModel.setUpdatedBy(object1.getString("UpdatedBy"));
                                    swasIdDetailsModel.setUpdatedDate(object1.getString("UpdatedDate"));
                                    swasIdDetailsModel.setTypeOfServiceCallText(object1.getString("TypeOfServiceCallText"));
                                    swasIdDetailsModel.setNAME(object1.getString("NAME"));
                                    swasIdDetailsModel.setBriefComplaint(object1.getString("BriefComplaint"));
                                    swasIdDetailsModel.setTypeOfFaultText(object1.getString("TypeOfFaultText"));
                                    swasIdDetailsModel.setStatusText(object1.getString("StatusText"));
                                    swasIdDetailsModel.setFaultyPart(object1.getString("FaultyPart"));
                                    swasIdDetailsModel.setCustomerID(object1.getString("CustomerID"));
                                    swasIdDetailsModel.setITEMID(object1.getString("ITEMID"));
                                    swasIdDetailsModel.setCURRENCY(object1.getString("CURRENCY"));
                                    swasIdDetailsModel.setDocumentPath(object1.getString("DocumentPath"));
                                    swasIdDetailsModel.setTypeOfServiceCallID(object1.getString("TypeOfServiceCallID"));
                                    swasIdDetailsModel.setCurrentlyAssignedToSE(object1.getString("CurrentlyAssignedToSE"));
                                    swasIdDetailsModel.setServiceEngineerName(object1.getString("ServiceEngineerName"));
                                    swasIdDetailsModel.setClosedDateTime(object1.getString("ClosedDateTime"));
                                    swasIdDetailsModel.setClosedBy(object1.getString("ClosedBy"));
                                    swasIdDetailsModel.setServiceCallStatusID(object1.getString("ServiceCallStatusID"));
                                    swasIdDetailsModel.setComments(object1.getString("Comments"));
                                    swasIdDetailsModel.setServiceEngineerID(object1.getString("ServiceEngineerID"));
                                    swasIdDetailsModel.setNumberOfVisits(object1.getString("NumberOfVisits"));

                                    txt_service_call_id.setText(object1.getString("ServiceCallID"));
                                    txt_customer_name.setText(object1.getString("LOCATIONNAME"));
                                    txt_call_creadted.setText(object1.getString("CreatedBy"));
                                    txt_created_on.setText(object1.getString("CreatedDate"));
                                    txt_modified_on.setText(object1.getString("UpdatedDate"));
                                    customer_id = object1.getString("CustomerID");
                                    status_id = object1.getString("ServiceCallStatusID");
                                    service_call_id = object1.getString("ServiceCallID");
                                    type_of_service_status_id = object1.getString("TypeOfServiceCallID");
                                    locationName = object1.getString("LOCATIONNAME");

                                    model.add(swasIdDetailsModel);
                                }
                            }
                            else if (array.length() == 0) {
                            }
                        }
                    }


                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public class closedServiceCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Swas_Deatails_of_Inprocess_Call_Activity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String ismobile = "1";
            String iscurrent = "1";
            String ServiceCallStatusID = "5";

            String url="http://apps.brainlines.net/SWAS-ServiceCallWebAPI/api/servicecall/savecloseservicecallstatus";
            String url1=  url + "&ServiceCallID=" +  service_call_id + "&ServiceCallStatusID=" + ServiceCallStatusID + "&IsCurrent=" + iscurrent + "&CreatedBy=" + created_by + "&IsMobile=" + ismobile;
            response= URL_Constants.makeHttpPostRequest(url1);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                Toast.makeText(Swas_Deatails_of_Inprocess_Call_Activity.this,"You closed this Call",Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Swas_Deatails_of_Inprocess_Call_Activity.this, Swas_Inprocess_Call_Service_Id_Details_Activity.class);
                Bundle bundle  = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_service_status_id);
                bundle.putString("customer_id",customer_id);
                i.putExtras(bundle);
                startActivity(i);
                finish();

            }
        }
    }

    private void closedCall(String remark,int short_resolutionID) {
        API api = Retroconfig.swasretrofit().create(API.class);
        int ServiceCallStatusID = 9;
        int iscurrent = 1;
        int QEStatusID = 2;
        Call<ResponseBody> call = api.savependingshorttermresolutionbymobile(
                Integer.parseInt(service_call_id),
                short_resolutionID,
                remark,
                ServiceCallStatusID,
                QEStatusID,
                iscurrent,
                created_by);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null){
                        String res = response.body().string();
                        Toast.makeText(Swas_Deatails_of_Inprocess_Call_Activity.this,"call completed successfully",Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(Swas_Deatails_of_Inprocess_Call_Activity.this, Swas_Home_Actvity.class);
                        startActivity(i);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(Swas_Deatails_of_Inprocess_Call_Activity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);
        setData();
        // Set content width and height
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(Swas_Deatails_of_Inprocess_Call_Activity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }


    public void setData() {
//        GlobalClass.db_global_name = AppPreferences.getDatabseName(this);
        DataBaseHelper dbHelper = new DataBaseHelper(Swas_Deatails_of_Inprocess_Call_Activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();

                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    oem_id= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));
                    service_eng_id= cursor.getString(cursor.getColumnIndex(Constants.USER_LOGIN_ID));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));


                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout()
    {
        finish();
        Toast toast= Toast.makeText(Swas_Deatails_of_Inprocess_Call_Activity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(Swas_Deatails_of_Inprocess_Call_Activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);

        //Preferences.Logout();

    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();

        if (id==R.id.nav_swas)
        {
            Intent i=new Intent(Swas_Deatails_of_Inprocess_Call_Activity.this, Swas_Home_Actvity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    public void checkNetworkConnection(){
        AlertDialog.Builder builder =new AlertDialog.Builder(Swas_Deatails_of_Inprocess_Call_Activity.this);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(Swas_Deatails_of_Inprocess_Call_Activity.this, Swas_Home_Actvity.class);
        Bundle bundle = new Bundle();
        bundle.putString("service_call_id",service_call_id);
        bundle.putString("status_id",status_id);
        bundle.putString("Typeofservicecall_id",type_of_service_status_id);
        bundle.putString("customer_id",customer_id);
        i.putExtras(bundle);
        startActivity(i);
        finish();
    }

    private void getDataFromListOfServiceCallByStatus() {
        Cursor cursor = helper.getDetailsDataFromServiceCallStatusTable(service_call_id);
        while (cursor.moveToNext()){
            customer_name = cursor.getString(0);
            String created_By = cursor.getString(1);
            String created_on = cursor.getString(2);
            String last_modified_on = cursor.getString(3);
            String product = cursor.getString(4);
            String complaint = cursor.getString(5);
            String type_of_fault_text= cursor.getString(6);
            String typeOfServiceCallText= cursor.getString(7);
//            String statusText= cursor.getString(8);
            txt_service_call_id.setText(service_call_id);
            txt_service_call_id.setText(customer_name);
            txt_call_creadted.setText(created_By);
            txt_created_on.setText(created_on);
            txt_modified_on.setText(last_modified_on);

        }
    }
}