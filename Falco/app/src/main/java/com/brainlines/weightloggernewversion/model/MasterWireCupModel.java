package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MasterWireCupModel implements Parcelable {

    String wireCupId;
    int oemId;
    String wireCup;
    boolean isActive;
    boolean IsDeleted;
    String createdDate;
    String modifiedDate;
    String createdBy;
    String modifiedBy;

    public MasterWireCupModel(String wireCupId, int oemId, String wireCup, boolean isActive, boolean isDeleted, String createdDate, String modifiedDate, String createdBy, String modifiedBy) {
        this.wireCupId = wireCupId;
        this.oemId = oemId;
        this.wireCup = wireCup;
        this.isActive = isActive;
        IsDeleted = isDeleted;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.createdBy = createdBy;
        this.modifiedBy = modifiedBy;
    }

    protected MasterWireCupModel(Parcel in) {
        wireCupId = in.readString();
        oemId = in.readInt();
        wireCup = in.readString();
        isActive = in.readByte() != 0;
        IsDeleted = in.readByte() != 0;
        createdDate = in.readString();
        modifiedDate = in.readString();
        createdBy = in.readString();
        modifiedBy = in.readString();
    }

    public static final Creator<MasterWireCupModel> CREATOR = new Creator<MasterWireCupModel>() {
        @Override
        public MasterWireCupModel createFromParcel(Parcel in) {
            return new MasterWireCupModel(in);
        }

        @Override
        public MasterWireCupModel[] newArray(int size) {
            return new MasterWireCupModel[size];
        }
    };

    public String getWireCupId() {
        return wireCupId;
    }

    public void setWireCupId(String wireCupId) {
        this.wireCupId = wireCupId;
    }

    public int getOemId() {
        return oemId;
    }

    public void setOemId(int oemId) {
        this.oemId = oemId;
    }

    public String getWireCup() {
        return wireCup;
    }

    public void setWireCup(String wireCup) {
        this.wireCup = wireCup;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isDeleted() {
        return IsDeleted;
    }

    public void setDeleted(boolean deleted) {
        IsDeleted = deleted;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(wireCupId);
        dest.writeInt(oemId);
        dest.writeString(wireCup);
        dest.writeByte((byte) (isActive ? 1 : 0));
        dest.writeByte((byte) (IsDeleted ? 1 : 0));
        dest.writeString(createdDate);
        dest.writeString(modifiedDate);
        dest.writeString(createdBy);
        dest.writeString(modifiedBy);
    }
}
