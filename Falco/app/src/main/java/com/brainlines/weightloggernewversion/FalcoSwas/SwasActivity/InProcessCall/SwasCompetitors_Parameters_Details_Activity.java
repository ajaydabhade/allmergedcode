package com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.InProcessCall;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Feedback_Activity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Home_Actvity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter.SwasCompetitorParameterAdapter;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter.SwasCompletedCompetitorAdapter;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasListener.CompetitorParameterListener;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompetitorParameterModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompletedCompetitorModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasIdDetailsModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.android.material.navigation.NavigationView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants.convertStreamToString;

public class SwasCompetitors_Parameters_Details_Activity extends AppCompatActivity implements CompetitorParameterListener, NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "SwasCompetitors_Paramet";
    ImageButton btnFeedback,btnSubParam,btn_comp_list_display;
    RecyclerView rv_competitor_parameter,alert_rv_list;
    SwasCompetitorParameterAdapter adapter;
    String service_call_id,created_by,customer_id,oem_id,status_id,type_of_Service_Call_id;
    ArrayList<SwasCompetitorParameterModel> model = new ArrayList<>();
    EditText ed_come_start_date;
    int yearr, month, dayofmonth;
    TextView txt_service_call_id;
    SwasCompletedCompetitorAdapter completedCompetitorAdapter;
    ImageView img_user_info,nav_view_img,img_back;
    String user_email,user_token,user_role;
    DrawerLayout drawer;
    TextView profile_user,profile_role;
    String Created_by="",service_eng_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_swas_inprocess_competitors_parameter_navigation);
        initUi();
        setData();
        img_back=(ImageView)findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SwasCompetitors_Parameters_Details_Activity.this, Swas_Deatails_of_Inprocess_Call_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_Service_Call_id);
                bundle.putString("customer_id",customer_id);
                i.putExtras(bundle);
                startActivity(i);
                finish();
            }
        });
        if (user_email.equals("yogesh@brainlines.in"))
        {
            created_by = "Yogesh Jadhav";
//            service_eng_id = "1";
        }
        else
        {
            created_by = "Deepak Kulkarni";
//            service_eng_id = "2";
        }
        img_user_info=(ImageView)findViewById(R.id.img_userinfo);
        img_user_info.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img=(ImageView)findViewById(R.id.nav_view_img);

        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_planning).setVisible(false);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
        {
            status_id = bundle.getString("status_id");
            service_call_id = bundle.getString("service_call_id");
            type_of_Service_Call_id = bundle.getString("Typeofservicecall_id");
            customer_id = bundle.getString("customer_id");
        }
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
        if(isConnected) {
            Log.d("Network", "Connected");
            Id_Details();
            CompetitorParameter_details();
            txt_service_call_id.setText(service_call_id);

        }
        else{
            checkNetworkConnection();
            Log.d("Network","Not Connected");
            getCompParamsFromLocalDB();
        }

        btnFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(SwasCompetitors_Parameters_Details_Activity.this, Swas_Feedback_Activity.class);
                startActivity(i);
            }
        });
        ed_come_start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar myCalendar = Calendar.getInstance();
                DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        yearr = Calendar.YEAR;
                        month = Calendar.MONTH;
                        dayofmonth = Calendar.DAY_OF_MONTH;
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "yyyy-MM-dd"; // your format
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                        ed_come_start_date.setText(sdf.format(myCalendar.getTime()));

                    }

                };
                DatePickerDialog datePickerDialog=new DatePickerDialog(SwasCompetitors_Parameters_Details_Activity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));

                //following line to restrict future date selection
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
/*
                new DatePickerDialog(SwasCompetitors_Parameters_Details_Activity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
*/
            }
        });
        btnSubParam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(ed_come_start_date.getText().toString()))
                {
                    ed_come_start_date.setError("Please select the date");
                }
                else
                {
                    ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

                    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                    boolean isConnected = activeNetwork != null &&
                            activeNetwork.isConnected();
                    if(isConnected) {
                        new SaveCompdetails().execute();
                    }else {
                        insertCompParamsToSqliteDB();
                    }
                }

            }
        });
        btn_comp_list_display.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnected();
//                if(isConnected) {
//                    Log.d("Network", "Connected");
//                    listDisplay();
//
//                }
//                else{
//                    checkNetworkConnection();
//                    listDisplay1();
//
//                }
                listDisplay1();
            }
        });

        btnFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SwasCompetitors_Parameters_Details_Activity.this, Swas_Feedback_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_Service_Call_id);
                bundle.putString("customer_id",customer_id);
                i.putExtras(bundle);

                startActivity(i);
            }
        });
        setData();

    }

    private void insertCompParamsToSqliteDB() {
        String comp_date = ed_come_start_date.getText().toString();
        for (int i= 0;i<model.size();i++)
        {
            DataBaseHelper helper=new DataBaseHelper(SwasCompetitors_Parameters_Details_Activity.this);
            SQLiteDatabase db = helper.getWritableDatabase();
            ContentValues cv=new ContentValues();
            cv.put(Constants.SERVICE_CALL_COMP_PARAMETERS_COMPITITORS_PARMETER_ID,"1");
            cv.put(Constants.SERVICE_CALL_COMP_PARAMETERS_OEM_ID,oem_id);
            cv.put(Constants.SERVICE_CALL_COMP_PARAMETERS_CUSTOMER_ID,customer_id);
            cv.put(Constants.SERVICE_CALL_COMP_PARAMETERS_SERVICE_CALL_ID,service_call_id);
            cv.put(Constants.SERVICE_CALL_COMP_PARAMETERS_COMPITITORS_PARMETER,model.get(i).getCompetitorsParameters());
            cv.put(Constants.SERVICE_CALL_COMP_PARAMETERS_SERVICE_CALL_START_DATE,comp_date);
            cv.put(Constants.SERVICE_CALL_COMP_PARAMETERS_MACHINE_PARA_VALUE,model.get(i).getMachineParameterValues());
            cv.put(Constants.SERVICE_CALL_COMP_PARAMETERS_REMARK,model.get(i).getRemark());
            cv.put(Constants.SERVICE_CALL_COMP_PARAMETERS_CREATED_BY,created_by);
            cv.put(Constants.SERVICE_CALL_COMP_PARAMETERS_CREATED_DATE,comp_date);
            cv.put(Constants.SERVICE_CALL_COMP_PARAMETERS_UNIT,model.get(i).getUnit());
            cv.put(Constants.SERVICE_CALL_COMP_PARAMETERS_IS_SYNC,"0");
            long res = db.insert(Constants.TABLE_SERVICE_CALL_COMP_PARAMETERS,null,cv);
            Log.d(TAG, "insertMachineParamData: ");
        }
    }

    private void getCompParamsFromLocalDB() {
//        ArrayList<SwasCompetitorParameterModel> model = new ArrayList<>();
        Cursor cursor = new DataBaseHelper(this).getCompPAramInProcessCallFromLocalDB();
        while (cursor.moveToNext()){
            String id = cursor.getString(1);
            String compParam = cursor.getString(2);
            String unit = cursor.getString(3);
            SwasCompetitorParameterModel swasCompetitorParameterModel = new SwasCompetitorParameterModel();
            swasCompetitorParameterModel.setCompetitorsParameters(compParam);
            swasCompetitorParameterModel.setUnit(unit);
            swasCompetitorParameterModel.setRemark("NA");
            model.add(swasCompetitorParameterModel);
        }
        adapter = new SwasCompetitorParameterAdapter(SwasCompetitors_Parameters_Details_Activity.this, model,SwasCompetitors_Parameters_Details_Activity.this);
        rv_competitor_parameter.setLayoutManager(new LinearLayoutManager(SwasCompetitors_Parameters_Details_Activity.this, LinearLayoutManager.VERTICAL, false));
        rv_competitor_parameter.setAdapter(adapter);
    }

    public void initUi() {
        btnFeedback=(ImageButton) findViewById(R.id.feedback4);
        rv_competitor_parameter=(RecyclerView)findViewById(R.id.rv_competitor_parameter);
        ed_come_start_date = (EditText)findViewById(R.id.ed_come_start_date);
        btnSubParam= (ImageButton)findViewById(R.id.btnSubParam);
        txt_service_call_id = (TextView)findViewById(R.id.txt_service_call_id);
        btn_comp_list_display = (ImageButton)findViewById(R.id.comp_list_display);

    }

    private void CompetitorParameter_details() {
        API api = Retroconfig.swasretrofit().create(API.class);
        /*dateformat1 = "2020-01-20";*/
        Call<ResponseBody> call = api.getcompetitorsparameterslist();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            if (array.length() != 0) {
                                for (int i = 0; i < array.length(); i++) {
                                    SwasCompetitorParameterModel swasCompetitorParameterModel = new SwasCompetitorParameterModel();
                                    JSONObject object1 = array.getJSONObject(i);
                               /* swasCompetitorParameterModel.setCompetitorsParameterID(object1.getString("CompetitorsParameterID"));
                                swasCompetitorParameterModel.setOEMID(object1.getString("OEMID"));
                                swasCompetitorParameterModel.setCustomerID(object1.getString("CustomerID"));
                                swasCompetitorParameterModel.setServiceCallID(object1.getString("ServiceCallID"));*/
                                    swasCompetitorParameterModel.setCompetitorsParameters(object1.getString("CompetitorsParameters"));
                                    swasCompetitorParameterModel.setUnit(object1.getString("UNIT"));
                                    swasCompetitorParameterModel.setRemark("NA");
                               /* swasCompetitorParameterModel.setServiceCallStartDate(object1.getString("ServiceCallStartDate"));
                                swasCompetitorParameterModel.setMachineParameterValues(object1.getString("MachineParameterValues"));
                                swasCompetitorParameterModel.setCreatedBy(object1.getString("CreatedBy"));
                                swasCompetitorParameterModel.setCreatedDate(object1.getString("CreatedDate"));*/
                                    model.add(swasCompetitorParameterModel);
                                }
                                adapter = new SwasCompetitorParameterAdapter(SwasCompetitors_Parameters_Details_Activity.this, model,SwasCompetitors_Parameters_Details_Activity.this);
                                rv_competitor_parameter.setLayoutManager(new LinearLayoutManager(SwasCompetitors_Parameters_Details_Activity.this, LinearLayoutManager.VERTICAL, false));
                                rv_competitor_parameter.setAdapter(adapter);
                            } else if (array.length() == 0) {
                            }
                        }
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    private void Id_Details() {
        API api = Retroconfig.swasretrofit().create(API.class);

        Call<ResponseBody> call = api.getalldetailsofsingleservicecall(service_call_id,status_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {

                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            ArrayList<SwasIdDetailsModel> model = new ArrayList<>();
                            if (array.length() != 0) {

                                for (int i = 0; i < array.length(); i++) {
                                    SwasIdDetailsModel swasIdDetailsModel = new SwasIdDetailsModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    service_call_id=object1.getString("ServiceCallID");
                                    created_by = object1.getString("CreatedBy");
                                    customer_id = object1.getString("CustomerID");
                                    txt_service_call_id.setText(object1.getString("ServiceCallID"));
                                }
                            } else if (array.length() == 0) {
                            }
                        }
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void listDisplay() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.swas_alert_competitors_display, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        alert_rv_list = (RecyclerView)dialogView.findViewById(R.id.rv_alert_competitor);

        alertlist_parameter();

        /*Button button = (Button)dialogView.findViewById(R.id.btn_reports_feedback);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();*/

       /* button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =  new Intent(SwasCompetitors_Parameters_Details_Activity.this,Swas_Feedback_Activity.class);
                startActivity(i);

            }
        });*/
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    public void listDisplay1() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.swas_alert_competitors_display, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        alert_rv_list = (RecyclerView)dialogView.findViewById(R.id.rv_alert_competitor);

        getCompParamFromLocalDB();

        /*Button button = (Button)dialogView.findViewById(R.id.btn_reports_feedback);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();*/

       /* button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =  new Intent(SwasCompetitors_Parameters_Details_Activity.this,Swas_Feedback_Activity.class);
                startActivity(i);

            }
        });*/
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    private void getCompParamFromLocalDB() {
        ArrayList<SwasCompletedCompetitorModel> model = new ArrayList<>();
        Cursor cursor = new DataBaseHelper(this).getCompParamFromLocalDB(service_call_id);
        while (cursor.moveToNext()){
            SwasCompletedCompetitorModel compModel = new SwasCompletedCompetitorModel();
            String CompetitorsParameterID = cursor.getString(1);
            String OEMID = cursor.getString(2);
            String CustomerID = cursor.getString(3);
            String ServiceCallID = cursor.getString(4);
            String CompetitorsParameters = cursor.getString(5);
            String ServiceCallStartDate = cursor.getString(6);
            String MachineParameterValues = cursor.getString(7);
            String Remark = cursor.getString(8);
            String CreatedBy = cursor.getString(9);
            String CreatedDate = cursor.getString(10);
            compModel.setCompetitorsParameterID(CompetitorsParameterID);
            compModel.setOEMID(OEMID);
            compModel.setCustomerID(CustomerID);
            compModel.setServiceCallID(ServiceCallID);
            compModel.setCompetitorsParameters(CompetitorsParameters);
            compModel.setServiceCallStartDate(ServiceCallStartDate);
            compModel.setMachineParameterValues(MachineParameterValues);
            compModel.setRemark(Remark);
            compModel.setCreatedBy(CreatedBy);
            compModel.setCreatedDate(CreatedDate);
            model.add(compModel);
        }
        completedCompetitorAdapter = new SwasCompletedCompetitorAdapter(SwasCompetitors_Parameters_Details_Activity.this,model);
        alert_rv_list.setLayoutManager(new LinearLayoutManager(SwasCompetitors_Parameters_Details_Activity.this, LinearLayoutManager.VERTICAL, false));
        alert_rv_list.setAdapter(completedCompetitorAdapter);
    }

    @Override
    public void gettravel_expenses(SwasCompetitorParameterModel model) {

    }

    private void alertlist_parameter() {
        API api = Retroconfig.swasretrofit().create(API.class);
        /*dateformat1 = "2020-01-20";*/
        Call<ResponseBody> call = api.getservicecallcompetitorsparameters(service_call_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {

                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");


                            ArrayList<SwasCompletedCompetitorModel> model = new ArrayList<>();

                            if (array.length() != 0) {

                                for (int i = 0; i < array.length(); i++) {
                                    SwasCompletedCompetitorModel compModel = new SwasCompletedCompetitorModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    compModel.setCompetitorsParameterID(object1.getString("CompetitorsParameterID"));
                                    compModel.setOEMID(object1.getString("OEMID"));
                                    compModel.setCustomerID(object1.getString("CustomerID"));
                                    compModel.setServiceCallID(object1.getString("ServiceCallID"));
                                    compModel.setCompetitorsParameters(object1.getString("CompetitorsParameters"));
                                    compModel.setServiceCallStartDate(object1.getString("ServiceCallStartDate"));
                                    compModel.setMachineParameterValues(object1.getString("MachineParameterValues"));
                                    compModel.setRemark(object1.getString("Remark"));
                                    compModel.setCreatedBy(object1.getString("CreatedBy"));
                                    compModel. setCreatedDate(object1.getString("CreatedDate"));

                                    //swasServiceModel .setServicecall_status_id(object1.getString("ServiceCallStatusID"));

                                    model.add(compModel);

                                }
                                completedCompetitorAdapter = new SwasCompletedCompetitorAdapter(SwasCompetitors_Parameters_Details_Activity.this,model);
                                alert_rv_list.setLayoutManager(new LinearLayoutManager(SwasCompetitors_Parameters_Details_Activity.this, LinearLayoutManager.VERTICAL, false));
                                alert_rv_list.setAdapter(completedCompetitorAdapter);

                            }
                            else if (array.length() == 0) {
                            }
                        }

                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public class SaveCompdetails extends AsyncTask<String,String,String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SwasCompetitors_Parameters_Details_Activity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String getResponse="";
            String comp_date = ed_come_start_date.getText().toString();

            String url= Retroconfig.BASEURL_SWAS + "insertcompetitorsparametersforservicecall";

            try {
                HttpParams httpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(httpParams, 60000);
                HttpConnectionParams.setSoTimeout(httpParams, 60000);
                HttpClient client = new DefaultHttpClient(httpParams);
                HttpPost httpPost = new HttpPost(url);

                httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
                JSONArray array=new JSONArray();
                for (int i= 0;i<model.size();i++)
                {
                    try {
                        JSONObject object=new JSONObject();
                        object.put("OEMID",oem_id);
                        object.put("CustomerID",customer_id);
                        object.put("ServiceCallID",service_call_id);
                        object.put("ServiceEngineerID",service_eng_id);
                        object.put("CompetitorsParameters",model.get(i).getCompetitorsParameters());
                        object.put("ServiceStartDate",comp_date);
                        object.put("MachineParameterValues",model.get(i).getMachineParameterValues());
                        object.put("Remark",model.get(i).getRemark());
                        object.put("CreatedBy",created_by);
                        array.put(object);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }
                httpPost.setEntity(new StringEntity(array.toString(),"UTF-8"));
                httpPost.setParams(httpParams);

                HttpResponse response = client.execute(httpPost);
                StatusLine statusLine = response.getStatusLine();

                if (statusLine.getStatusCode() == HttpStatus.SC_OK || statusLine.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();

                    getResponse = convertStreamToString(inputStream);
                }
                else if (statusLine.getStatusCode()==HttpStatus.SC_UNAUTHORIZED)
                {
                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();

                    getResponse = convertStreamToString(inputStream);
                }
                else {
                    response.getEntity().getContent().close();
                }
            }

            catch (Exception e) {
                Log.i(URL_Constants.TAG, "Error Message in Exception :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_EXCEPTION;

            }
            return getResponse;
        }

        @Override
        protected void onPostExecute(String getResponse) {
            super.onPostExecute(getResponse);
            progressDialog.dismiss();
            if (!getResponse.equals("Error"))
            {
                Toast.makeText(SwasCompetitors_Parameters_Details_Activity.this, "Competitors parameter inserted successfully", Toast.LENGTH_SHORT).show();
                model.clear();
                Intent i = new Intent( SwasCompetitors_Parameters_Details_Activity.this,Swas_Deatails_of_Inprocess_Call_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_Service_Call_id);
                bundle.putString("customer_id",customer_id);
                i.putExtras(bundle);
                startActivity(i);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(SwasCompetitors_Parameters_Details_Activity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);

        // Set content width and height
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(SwasCompetitors_Parameters_Details_Activity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }

    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(SwasCompetitors_Parameters_Details_Activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();

                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    oem_id= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));
                    service_eng_id= cursor.getString(cursor.getColumnIndex(Constants.USER_LOGIN_ID));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));


                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout() {
        finish();
        Toast toast= Toast.makeText(SwasCompetitors_Parameters_Details_Activity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(SwasCompetitors_Parameters_Details_Activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);

        //Preferences.Logout();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();

        if (id==R.id.nav_swas)
        {
            Intent i=new Intent(SwasCompetitors_Parameters_Details_Activity.this, Swas_Home_Actvity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    public void checkNetworkConnection(){
        AlertDialog.Builder builder =new AlertDialog.Builder(SwasCompetitors_Parameters_Details_Activity.this);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i= new Intent(SwasCompetitors_Parameters_Details_Activity.this,Swas_Deatails_of_Inprocess_Call_Activity.class);
        Bundle bundle = new Bundle();
        bundle.putString("service_call_id",service_call_id);
        bundle.putString("status_id",status_id);
        bundle.putString("Typeofservicecall_id",type_of_Service_Call_id);
        bundle.putString("customer_id",customer_id);
        i.putExtras(bundle);
        startActivity(i);
        finish();
    }
}
