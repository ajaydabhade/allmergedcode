package com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompletedCompetitorModel;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class SwasCompletedCompetitorAdapter extends RecyclerView.Adapter<SwasCompletedCompetitorAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<SwasCompletedCompetitorModel> comp_list;
    View.OnClickListener mClickListener;
    Context context;
    String str_start_date,str_end_Date,str_tk_id,str_approved_by,str_tk_release_date,main_string_release_date;
    String str_tkid;
    RecyclerView rv_packingtickets;
    SwasCompletedCompetitorAdapter adapter;

    public SwasCompletedCompetitorAdapter(Context ctx, ArrayList<SwasCompletedCompetitorModel> comp_list){
        inflater = LayoutInflater.from(ctx);
        this.comp_list = comp_list;
        this.context=ctx;
    }


    @Override
    public int getItemCount() {
        return comp_list.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_swas_completed_competitors_parameters, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final  SwasCompletedCompetitorModel model = comp_list.get(position);
        if (model!=null) {

            holder. txt_Date.setText(model.getServiceCallStartDate());
            holder. txt_Name.setText(model. getCompetitorsParameters());
            holder. txt_Values.setText(model.getMachineParameterValues());
            holder.txt_Remark.setText(model.getRemark());
           // holder.txt_AddedBy.setText(model. getCreatedBy());



        }
    }






    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txt_Date,txt_Name,txt_Values,txt_Remark,txt_AddedBy;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_Date=itemView.findViewById(R.id.txt_Date);
            txt_Name=itemView.findViewById(R.id.txt_Name);
            txt_Values=itemView.findViewById(R.id.txt_Values);
            txt_Remark=itemView.findViewById(R.id.txt_Remark);
           // txt_AddedBy=itemView.findViewById(R.id.txt_AddedBy);


        }
    }

}


