package com.brainlines.weightloggernewversion.Production;

import com.brainlines.weightloggernewversion.Production.Model.PausedTestListModel;
import com.brainlines.weightloggernewversion.Production.Model.ProductionTagHistoryListModel;

public interface ProductionPausedBatchListener {
    void isTagClicked(PausedTestListModel model);

}
