package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

public class SwasCompletedCallMachineParameterModel {
    String MachineParameterLogID,Machine_Id,CustomerID,
            ServiceCallID,MachineParameters,ServiceCallStartDate,
            ParameterValue,Remark,CreatedBy,CreatedDate;
    String oem_id;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    String unit;

    public String getOem_id() {
        return oem_id;
    }

    public void setOem_id(String oem_id) {
        this.oem_id = oem_id;
    }

    public String getMachineParameterLogID() {
        return MachineParameterLogID;
    }

    public void setMachineParameterLogID(String machineParameterLogID) {
        MachineParameterLogID = machineParameterLogID;
    }

    public String getMachine_Id() {
        return Machine_Id;
    }

    public void setMachine_Id(String machine_Id) {
        Machine_Id = machine_Id;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getServiceCallID() {
        return ServiceCallID;
    }

    public void setServiceCallID(String serviceCallID) {
        ServiceCallID = serviceCallID;
    }

    public String getMachineParameters() {
        return MachineParameters;
    }

    public void setMachineParameters(String machineParameters) {
        MachineParameters = machineParameters;
    }

    public String getServiceCallStartDate() {
        return ServiceCallStartDate;
    }

    public void setServiceCallStartDate(String serviceCallStartDate) {
        ServiceCallStartDate = serviceCallStartDate;
    }

    public String getParameterValue() {
        return ParameterValue;
    }

    public void setParameterValue(String parameterValue) {
        ParameterValue = parameterValue;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }
}
