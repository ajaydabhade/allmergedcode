package com.brainlines.weightloggernewversion.FalcoJK.MainAdapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.PacketTicketTrackingModel;
import com.brainlines.weightloggernewversion.FalcoJK.Listeners.UpdatePackingTicketeListener;
import com.brainlines.weightloggernewversion.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class UpdatePackTrackingTicketRvAdapter extends RecyclerView.Adapter<UpdatePackTrackingTicketRvAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<PacketTicketTrackingModel> ticket_list;
    View.OnClickListener mClickListener;
    Context context;
    String str_tk_no;
    UpdatePackingTicketeListener updatePackingTicketeListener;

    RecyclerView recyclerView;
    String str_coml_qty,pending_qty,str_tk_id,str_plant,str_date,str_total_qty,str_pendingqty,adddaypendingqty,strtkid,strtkno;

    public UpdatePackTrackingTicketRvAdapter(Context ctx, ArrayList<PacketTicketTrackingModel> ticket_list, UpdatePackingTicketeListener updatePackingTicketeListener){
        inflater = LayoutInflater.from(ctx);
        this.updatePackingTicketeListener = updatePackingTicketeListener;
        this.ticket_list = ticket_list;
        this.context=ctx;
    }

    @Override
    public int getItemCount() {
        return ticket_list.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_update_packing_ticket_data, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

            /*holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mClickListener.onClick(view);

                }
            });*/


        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int i) {
        final PacketTicketTrackingModel model = ticket_list.get(i);
        holder.txt_date.setText(ticket_list.get(i).getTt_Date());
        holder.txt_compl_qty.setText(ticket_list.get(i).getTt_Qty());
        holder.txt_pending_qty.setText(ticket_list.get(i).getTt_PendingQty());
        adddaypendingqty=holder.txt_pending_qty.getText().toString();

        // String actual_end_date = ticket_list.get(i).getActualenddate();

        if (!holder.txt_compl_qty.getText().equals(""))
        {
            holder.img_action_pack_submit.setEnabled(false);
        }
        else
        {
            holder.img_action_pack_submit.setEnabled(true);

        }


            /*if(!actual_end_date.equals("null"))
            {
                holder.img_action_pack_submit.setEnabled(false);
            }
            else
            {


            }*/
        holder.txt_compl_qty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                str_coml_qty = s.toString();
                if (s != null) {

                    if (!str_coml_qty.equals("")) {
                        ticket_list.get(i).setStr_txt_compl_qty(str_coml_qty);
                        ticket_list.get(i).setStr_pendingqty(adddaypendingqty);
                        holder.img_action_pack_submit.setEnabled(true);
                    }
                }
            }
        });


        holder.img_action_pack_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatePackingTicketeListener.imgactintrackingSubmit(model);
            }
        });
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView txt_date;
        EditText txt_compl_qty,txt_pending_qty;
        ImageButton img_action_pack_submit;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_date= itemView.findViewById(R.id.txtdate);
            txt_compl_qty= itemView.findViewById(R.id.txtcompletedqty1);
            txt_pending_qty= itemView.findViewById(R.id.txtpendingqty1);
            img_action_pack_submit=itemView.findViewById(R.id.img_action_pack_submit);
        }
    }

    public void refreshData(ArrayList<PacketTicketTrackingModel> model, String tk_id, String tk_no, RecyclerView recyclerView) {

        PacketTicketTrackingModel model1=new PacketTicketTrackingModel();

        Calendar c = Calendar .getInstance();
        System.out.println("Current time => "+c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c.getTime());
        strtkid=tk_id;
        strtkno=tk_no;

        recyclerView=recyclerView;
        model1.setTt_Date(formattedDate);
        model1.setTt_PendingQty(adddaypendingqty);
        model1.setTt_id(strtkid);
        model1.setTt_TicketNo(strtkno);
        ticket_list.add(model1);
        notifyDataSetChanged();

    }



}



