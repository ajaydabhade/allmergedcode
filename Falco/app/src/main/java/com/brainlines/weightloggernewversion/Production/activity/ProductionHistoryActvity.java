package com.brainlines.weightloggernewversion.Production.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.Production.Model.ProductionTagHistoryListModel;
import com.brainlines.weightloggernewversion.Production.Model.ProductionTagModel;
import com.brainlines.weightloggernewversion.Production.adapter.ProductionTagListAdapter;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.listener.ProductionTagListListener;
import com.brainlines.weightloggernewversion.utils.AppPreferences;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

@SuppressLint("Registered")
public class ProductionHistoryActvity extends AppCompatActivity implements View.OnClickListener, ProductionTagListListener {
    TextView txt_search_select_date;
    Spinner spin_product_search;
    ProductionTagListAdapter adapter;
    ImageButton btn_search_history,btn_search_submit;
    ArrayList<String> shiftlist = new ArrayList<>();
    String str_product,str_selected_date;
    DataBaseHelper helper;
    String dataloggerName,dataloggerIp;
    private String dbIpAddress = "192.168.10.95";
    private String dbUserName = "test2";
    private String dbPassword = "test12345";
    private String dbDatabaseName = "decitalpool";
    private String serverport = "1433";
    private String dbUrl = "jdbc:jtds:sqlserver://"+dbIpAddress+":"+serverport+"/"+dbDatabaseName;
    private int DATA_LOGGER_PORT = 8080;
    ArrayList<ProductionTagHistoryListModel> production_tag_list = new ArrayList<>();
    RecyclerView rv_production_tag_list;
    ArrayList<String> product_list = new ArrayList<>();
    private String dbName;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_production_history_search);
        initUi();
        dbName = AppPreferences.getDatabseName(this);
//        helper = new DataBaseHelper(this,dbName);
        helper = new DataBaseHelper(this);
        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));

        getProductsUsedForTrial();
        ArrayAdapter<String> spProductUsdForTrialList = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, product_list);
        spProductUsdForTrialList.setDropDownViewResource(R.layout.item_spinner_latyout);
        spin_product_search.setAdapter(spProductUsdForTrialList);

        spin_product_search.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                str_product = spin_product_search.getSelectedItem().toString();

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btn_search_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_selected_date = txt_search_select_date.getText().toString();

               getProductionTags(str_selected_date,str_product.trim());
            }
        });

    }

    private void initUi()
    {
        txt_search_select_date = findViewById(R.id.txt_search_select_date);
        spin_product_search = findViewById(R.id.spin_product_search);btn_search_history = findViewById(R.id.btn_search_history);
        rv_production_tag_list = findViewById(R.id.rv_production_tag_list);

        txt_search_select_date.setOnClickListener(this);
        btn_search_history.setOnClickListener(this);
    }
    private void getProductsUsedForTrial() {
        Cursor productName = helper.getProducts();
        while (productName.moveToNext()){
            String product = productName.getString(0);
            product_list.add(product);
            Log.d("TAG", "getProductsUsedForTrial: ");
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == txt_search_select_date.getId())
        {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            final Calendar myCalendar = Calendar.getInstance();
            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    // TODO Auto-generated method stub
                    myCalendar.set(Calendar.YEAR, year);
                    int  yearr = Calendar.YEAR;
                    int month = Calendar.MONTH;
                    int dayofmonth = Calendar.DAY_OF_MONTH;
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                    txt_search_select_date.setText(sdf.format(myCalendar.getTime()));

                }
            };
            new DatePickerDialog(ProductionHistoryActvity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();

        }

        else if (v.getId() == btn_search_history.getId())
        {
            str_selected_date = txt_search_select_date.getText().toString();

        }
    }

    private void getProductionTags(String str_selected_date, String str_product) {
        production_tag_list.clear();
        Cursor p_tag = helper.getProductionBatchDetails(str_selected_date, str_product);
        while (p_tag.moveToNext()){
            try {
                /*String p_tag_string = p_tag.getString(0);
                String batchid = p_tag.getString(4);
                String product = p_tag.getString(6);
                String gm_pouch = p_tag.getString(7);
                String total_weight = p_tag.getString(9);
                String no_of_pouches = p_tag.getString(16);
*/
                String p_tag_string = p_tag.getString(0);
                String batchid = p_tag.getString(4);
                String product = p_tag.getString(5);
                String gm_pouch = p_tag.getString(6);
                String total_weight = p_tag.getString(8);
                String no_of_pouches = p_tag.getString(13);

                ProductionTagHistoryListModel model = new ProductionTagHistoryListModel();
                model.setGm_pouch(gm_pouch);
                model.setNo_of_pouches(no_of_pouches);
                model.setP_tag_string(p_tag_string);
                model.setProduct(product);
                model.setTotal_weight(total_weight);
                model.setBatch_id(batchid);
                production_tag_list.add(model);

                // production_tag_list.add(p_tag_string);
                Log.d("TAG", "p_tag: ");
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
        if (production_tag_list.size()!=0)
        {
            LinearLayoutManager manager = new LinearLayoutManager(this);
            rv_production_tag_list.setLayoutManager(manager);
            adapter = new ProductionTagListAdapter(this,production_tag_list,this);
            rv_production_tag_list.setAdapter(adapter);
        }
    }

    @Override
    public void isTagClicked(ProductionTagHistoryListModel model) {
        Intent i = new Intent(ProductionHistoryActvity.this,BatchReportDetails.class);
        i.putExtra("selected_date",str_selected_date);
        i.putExtra("product",str_product);
        i.putExtra("batch_id",model.getBatch_id());
        startActivity(i);
    }
}
