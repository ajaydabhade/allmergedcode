package com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.Model;

public class NewDateWiseProductionTicketProcessList {

    String ptk_PtkdId,ptk_TkNo,ptkd_Plant,ptk_ValueStream,ptk_Operation,ptk_UpdateDate,ptk_OkQty,ptk_RejectQty,ptk_ReworkQty,ptk_WIPQty;
    String ptk_SecondQty,opr_operation_name,ptkd_Qty,uni_unit_name,val_valuestream_name,tk_Actual_Start_Date,tk_Actual_End_Date,tk_Start_Date;
    String tk_End_Date,ptk_ChangeRoute_Flag,createdby,createddate;

    public String getPtk_PtkdId() {
        return ptk_PtkdId;
    }

    public void setPtk_PtkdId(String ptk_PtkdId) {
        this.ptk_PtkdId = ptk_PtkdId;
    }

    public String getPtk_TkNo() {
        return ptk_TkNo;
    }

    public void setPtk_TkNo(String ptk_TkNo) {
        this.ptk_TkNo = ptk_TkNo;
    }

    public String getPtkd_Plant() {
        return ptkd_Plant;
    }

    public void setPtkd_Plant(String ptkd_Plant) {
        this.ptkd_Plant = ptkd_Plant;
    }

    public String getPtk_ValueStream() {
        return ptk_ValueStream;
    }

    public void setPtk_ValueStream(String ptk_ValueStream) {
        this.ptk_ValueStream = ptk_ValueStream;
    }

    public String getPtk_Operation() {
        return ptk_Operation;
    }

    public void setPtk_Operation(String ptk_Operation) {
        this.ptk_Operation = ptk_Operation;
    }

    public String getPtk_UpdateDate() {
        return ptk_UpdateDate;
    }

    public void setPtk_UpdateDate(String ptk_UpdateDate) {
        this.ptk_UpdateDate = ptk_UpdateDate;
    }

    public String getPtk_OkQty() {
        return ptk_OkQty;
    }

    public void setPtk_OkQty(String ptk_OkQty) {
        this.ptk_OkQty = ptk_OkQty;
    }

    public String getPtk_RejectQty() {
        return ptk_RejectQty;
    }

    public void setPtk_RejectQty(String ptk_RejectQty) {
        this.ptk_RejectQty = ptk_RejectQty;
    }

    public String getPtk_ReworkQty() {
        return ptk_ReworkQty;
    }

    public void setPtk_ReworkQty(String ptk_ReworkQty) {
        this.ptk_ReworkQty = ptk_ReworkQty;
    }

    public String getPtk_WIPQty() {
        return ptk_WIPQty;
    }

    public void setPtk_WIPQty(String ptk_WIPQty) {
        this.ptk_WIPQty = ptk_WIPQty;
    }

    public String getPtk_SecondQty() {
        return ptk_SecondQty;
    }

    public void setPtk_SecondQty(String ptk_SecondQty) {
        this.ptk_SecondQty = ptk_SecondQty;
    }

    public String getOpr_operation_name() {
        return opr_operation_name;
    }

    public void setOpr_operation_name(String opr_operation_name) {
        this.opr_operation_name = opr_operation_name;
    }

    public String getPtkd_Qty() {
        return ptkd_Qty;
    }

    public void setPtkd_Qty(String ptkd_Qty) {
        this.ptkd_Qty = ptkd_Qty;
    }

    public String getUni_unit_name() {
        return uni_unit_name;
    }

    public void setUni_unit_name(String uni_unit_name) {
        this.uni_unit_name = uni_unit_name;
    }

    public String getVal_valuestream_name() {
        return val_valuestream_name;
    }

    public void setVal_valuestream_name(String val_valuestream_name) {
        this.val_valuestream_name = val_valuestream_name;
    }

    public String getTk_Actual_Start_Date() {
        return tk_Actual_Start_Date;
    }

    public void setTk_Actual_Start_Date(String tk_Actual_Start_Date) {
        this.tk_Actual_Start_Date = tk_Actual_Start_Date;
    }

    public String getTk_Actual_End_Date() {
        return tk_Actual_End_Date;
    }

    public void setTk_Actual_End_Date(String tk_Actual_End_Date) {
        this.tk_Actual_End_Date = tk_Actual_End_Date;
    }

    public String getTk_Start_Date() {
        return tk_Start_Date;
    }

    public void setTk_Start_Date(String tk_Start_Date) {
        this.tk_Start_Date = tk_Start_Date;
    }

    public String getTk_End_Date() {
        return tk_End_Date;
    }

    public void setTk_End_Date(String tk_End_Date) {
        this.tk_End_Date = tk_End_Date;
    }

    public String getPtk_ChangeRoute_Flag() {
        return ptk_ChangeRoute_Flag;
    }

    public void setPtk_ChangeRoute_Flag(String ptk_ChangeRoute_Flag) {
        this.ptk_ChangeRoute_Flag = ptk_ChangeRoute_Flag;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }
}
