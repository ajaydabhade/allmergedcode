package com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.InProcessCall;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Feedback_Activity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Home_Actvity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter.SwasCompletedMachineAdapter;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter.SwasMachineParameterAdapter;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompletedMachineModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasIdDetailsModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompletedCallMachineParameterModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.android.material.navigation.NavigationView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants.convertStreamToString;

public class SwasMachineParametersActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "SwasMachineParametersAc";
    ImageButton btnSubParam,btn_machine_list_display;
    ImageButton btnFeedback;
    RecyclerView rv_machine_parameter;
    SwasMachineParameterAdapter adapter;
    String created_by,oem_id,service_call_id,customer_id,status_id,type_of_Service_Call_id;
    EditText ed_machine_start_date;
    int yearr,month,dayofmonth;
    TextView txt_service_call_id;
    RecyclerView alert_rv_list;
    SwasCompletedMachineAdapter swasCompletedMachineAdapter;
    ImageView img_user_info,nav_view_img,img_back;
    String user_email,user_token,user_role;
    DrawerLayout drawer;
    TextView profile_user,profile_role;
    String Created_by="",service_eng_id;

    ArrayList<SwasCompletedCallMachineParameterModel> model = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_swas_inprocess_machine_parameters_navigation);
        initUi();
        setData();
        img_back=(ImageView)findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SwasMachineParametersActivity.this, Swas_Deatails_of_Inprocess_Call_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_Service_Call_id);
                bundle.putString("customer_id",customer_id);
                i.putExtras(bundle);
                startActivity(i);
                finish();
            }
        });
        if (user_email.equals("yogesh@brainlines.in"))
        {
            created_by = "Yogesh Jadhav";
//            service_eng_id = "1";
        }
        else
        {
            created_by = "Deepak Kulkarni";
//            service_eng_id = "2";
        }
        img_user_info=(ImageView)findViewById(R.id.img_userinfo);
        img_user_info.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img=(ImageView)findViewById(R.id.nav_view_img);
        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_planning).setVisible(false);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
        {
            status_id = bundle.getString("status_id");
            service_call_id = bundle.getString("service_call_id");
            type_of_Service_Call_id = bundle.getString("Typeofservicecall_id");
            customer_id = bundle.getString("customer_id");
        }

        setData();

        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        final boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
//        if(isConnected) {
//            Log.d("Network", "Connected");
//            Id_Details();
//            MachineParameter_details();
//            txt_service_call_id.setText(service_call_id);
//        }
//        else{
//            checkNetworkConnection();
//            Log.d("Network","Not Connected");
//            getMachineParameterFromDB();
//
//        }
        getMachineParameterFromDB();
        btnSubParam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(ed_machine_start_date.getText().toString()))
                {
                    ed_machine_start_date.setError("Please select the date");
                }
                else
                {
                    ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                    final boolean isConnected = activeNetwork != null &&
                            activeNetwork.isConnected();
                    if (isConnected){
                        new SaveMachineParameters().execute();
                    }else {
                        insertMachineParamData();
                    }

                }
            }
        });
        btnFeedback=findViewById(R.id.feedback4);
        btn_machine_list_display = findViewById(R.id.btn_machine_list_display);
        btnFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SwasMachineParametersActivity.this, Swas_Feedback_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_Service_Call_id);
                bundle.putString("customer_id",customer_id);
                i.putExtras(bundle);

                startActivity(i);
            }
        });
        ed_machine_start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar myCalendar = Calendar.getInstance();
                DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        yearr = Calendar.YEAR;
                        month = Calendar.MONTH;
                        dayofmonth = Calendar.DAY_OF_MONTH;
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "yyyy-MM-dd"; // your format
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                        ed_machine_start_date.setText(sdf.format(myCalendar.getTime()));
                    }
                };
                DatePickerDialog datePickerDialog=new DatePickerDialog(SwasMachineParametersActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));

                //following line to restrict future date selection
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
/*
                new DatePickerDialog(SwasMachineParametersActivity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
*/

            }
        });
        btn_machine_list_display.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnected();
                if(isConnected) {
                    Log.d("Network", "Connected");
                    listDisplay();
                }
                else{
                    checkNetworkConnection();
                    Log.d("Network","Not Connected");
                    listDisplay1();
                }
            }
        });
    }

    private void insertMachineParamData() {
        for (int i = 0 ; i < model.size() ; i++){
            DataBaseHelper helper=new DataBaseHelper(SwasMachineParametersActivity.this);
            SQLiteDatabase db = helper.getWritableDatabase();
            ContentValues cv=new ContentValues();
            cv.put(Constants.SERVICE_CALL_MC_PARAMETERS_MACHINE_PARAMETER_LOG_ID,0);
            cv.put(Constants.SERVICE_CALL_MC_PARAMETERS_OEM_ID,oem_id);
            cv.put(Constants.SERVICE_CALL_MC_PARAMETERS_CUSTOMER_ID,customer_id);
            cv.put(Constants.SERVICE_CALL_MC_PARAMETERS_SERVICE_CALL_ID,Integer.parseInt(service_call_id));
            cv.put(Constants.SERVICE_CALL_MC_PARAMETERS_MACHINE_PARAMETERS,model.get(i).getMachineParameters());
            cv.put(Constants.SERVICE_CALL_MC_PARAMETERS_SERVICE_CALL_START_DATE,ed_machine_start_date.getText().toString());
            cv.put(Constants.SERVICE_CALL_MC_PARAMETERS_SERVICE_CALL_END_DATE,"");
            cv.put(Constants.SERVICE_CALL_MC_PARAMETERS_PARAMETER_VALUE,model.get(i).getParameterValue());
            cv.put(Constants.SERVICE_CALL_MC_PARAMETERS_UNIT,"");
            cv.put(Constants.SERVICE_CALL_MC_PARAMETERS_REMARK,model.get(i).getRemark());
            cv.put(Constants.SERVICE_CALL_MC_PARAMETERS_CREATED_BY,created_by);
            cv.put(Constants.SERVICE_CALL_MC_PARAMETERS_CREATED_DATE,0);
            cv.put(Constants.SERVICE_CALL_MC_PARAMETERS_IS_SYNC,0);
            long res = db.insert(Constants.TABLE_SERVICE_CALL_MC_PARAMETERS,null,cv);
            Log.d(TAG, "insertMachineParamData: ");
        }
    }

    private void getMachineParameterFromDB() {
        Cursor cursor = new DataBaseHelper(this).getMachineParameterFromLocalDB();
//        ArrayList<SwasCompletedCallMachineParameterModel> model = new ArrayList<>();
        while (cursor.moveToNext()){
            String machineParaID = cursor.getString(1);
            String machinePara = cursor.getString(2);
            String unit = cursor.getString(3);
            SwasCompletedCallMachineParameterModel swasCompletedCallMachineParameterModel = new SwasCompletedCallMachineParameterModel();
            swasCompletedCallMachineParameterModel.setMachineParameterLogID(machineParaID);
            swasCompletedCallMachineParameterModel.setMachineParameters(machinePara);
            swasCompletedCallMachineParameterModel.setUnit(unit);
            swasCompletedCallMachineParameterModel.setRemark("NA");
            model.add(swasCompletedCallMachineParameterModel);
        }
        adapter = new SwasMachineParameterAdapter(SwasMachineParametersActivity.this, model);
        rv_machine_parameter.setLayoutManager(new LinearLayoutManager(SwasMachineParametersActivity.this, LinearLayoutManager.VERTICAL, false));
        rv_machine_parameter.setAdapter(adapter);
    }

    public void initUi() {
        btnSubParam=findViewById(R.id.btnSubParam);
        btnFeedback=findViewById(R.id.feedback4);
        ed_machine_start_date = (EditText)findViewById(R.id.ed_machine_start_date);
        rv_machine_parameter=(RecyclerView)findViewById(R.id.rv_machine_parameter);
        txt_service_call_id = (TextView)findViewById(R.id.txt_service_call_id);
    }

    private void Id_Details() {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getalldetailsofsingleservicecall(service_call_id,status_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {

                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            ArrayList<SwasIdDetailsModel> model = new ArrayList<>();
                            if (array.length() != 0) {

                                for (int i = 0; i < array.length(); i++) {
                                    SwasIdDetailsModel swasIdDetailsModel = new SwasIdDetailsModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    service_call_id=object1.getString("ServiceCallID");
                                    created_by = object1.getString("CreatedBy");
                                    customer_id = object1.getString("CustomerID");
                                    txt_service_call_id.setText(service_call_id);

                                }
                            } else if (array.length() == 0) {
                            }
                        }
                    }

                    MachineParameter_details();

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void MachineParameter_details() {
        API api = Retroconfig.swasretrofit().create(API.class);
        //String service_call_id= "28";
        Call<ResponseBody> call = api.getmachineparameterslist();

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                    }
                    String res = response.body().string();
                    if (res != null) {

                        JSONObject object = new JSONObject(res);
                        JSONArray array = object.getJSONArray("data");

                        if (array.length() != 0) {

                            for (int i = 0; i < array.length(); i++) {
                                SwasCompletedCallMachineParameterModel swasCompletedCallMachineParameterModel = new SwasCompletedCallMachineParameterModel();
                                JSONObject object1 = array.getJSONObject(i);
                                swasCompletedCallMachineParameterModel.setMachineParameterLogID(object1.getString("MachineParameterID"));
                                //swasMachineParameterModel.setOem_id(object1.getString("OEMID"));
                                //swasMachineParameterModel.setCustomerID(object1.getString("CustomerID"));
                                swasCompletedCallMachineParameterModel.setMachineParameters(object1.getString("MachineParameters"));
                                /*swasMachineParameterModel.setMachineParameters(object1.getString("MachineParameters"));
                                swasMachineParameterModel.setServiceCallStartDate(object1.getString("ServiceCallStartDate"));
                                swasMachineParameterModel.setParameterValue(object1.getString("ParameterValue"));
                                swasMachineParameterModel.setRemark(object1.getString("Remark"));
                                swasMachineParameterModel.setCreatedBy(object1.getString("CreatedBy"));
                                swasMachineParameterModel.setCreatedDate(object1.getString("CreatedDate"));*/
                                swasCompletedCallMachineParameterModel.setUnit(object1.getString("UNIT"));
                                swasCompletedCallMachineParameterModel.setRemark("NA");

                                model.add(swasCompletedCallMachineParameterModel);

                            }
                            adapter = new SwasMachineParameterAdapter(SwasMachineParametersActivity.this, model);
                            rv_machine_parameter.setLayoutManager(new LinearLayoutManager(SwasMachineParametersActivity.this, LinearLayoutManager.VERTICAL, false));
                            rv_machine_parameter.setAdapter(adapter);


                        } else if (array.length() == 0) {
                        }
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void listDisplay() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.alert_machine_list, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        alert_rv_list = (RecyclerView)dialogView.findViewById(R.id.rv_alert_competitor);
        alertlist_parameter();
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    public void listDisplay1() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.alert_machine_list, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        alert_rv_list = (RecyclerView)dialogView.findViewById(R.id.rv_alert_competitor);
        getServiceMachineParameter();
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    private void getServiceMachineParameter() {
        Cursor cursor = new DataBaseHelper(this).getMachineParameter(service_call_id);
        ArrayList<SwasCompletedMachineModel> model = new ArrayList<>();
        while (cursor.moveToNext()){
            SwasCompletedMachineModel machineModel = new SwasCompletedMachineModel();
            String MachineParameterLogID = cursor.getString(1);
            String OEMID = cursor.getString(2);
            String CustomerID = cursor.getString(3);
            String ServiceCallID = cursor.getString(4);
            String MachineParameters = cursor.getString(5);
            String ServiceCallStartDate = cursor.getString(6);
            String ServiceCallEndDate = cursor.getString(7);
            String ParameterValue = cursor.getString(8);
            String UNIT = cursor.getString(9);
            String Remark = cursor.getString(10);
            String CreatedBy = cursor.getString(11);
            String CreatedDate = cursor.getString(12);
            machineModel.setMachineParameterLogID(MachineParameterLogID);
            machineModel.setOEMID(OEMID);
            machineModel.setCustomerID(CustomerID);
            machineModel.setServiceCallID(ServiceCallID);
            machineModel.setMachineParameters(MachineParameters);
            machineModel.setServiceCallStartDate(ServiceCallStartDate);
            machineModel.setServiceCallEndDate(ServiceCallEndDate);
            machineModel.setParameterValue(ParameterValue);
            machineModel.setRemark(Remark);
            machineModel. setCreatedBy(CreatedBy);
            machineModel.setCreatedDate(CreatedDate);
            model.add(machineModel);
        }
        swasCompletedMachineAdapter = new SwasCompletedMachineAdapter(SwasMachineParametersActivity.this,model);
        alert_rv_list.setLayoutManager(new LinearLayoutManager(SwasMachineParametersActivity.this, LinearLayoutManager.VERTICAL, false));
        alert_rv_list.setAdapter(swasCompletedMachineAdapter);
    }

    private void alertlist_parameter() {

        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getservicecallmachineparameters(service_call_id);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                    }
                    String res = response.body().string();
                    if (res != null) {

                        JSONObject object = new JSONObject(res);
                        JSONArray array = object.getJSONArray("data");
                        ArrayList<SwasCompletedMachineModel> model = new ArrayList<>();

                        if (array.length() != 0) {

                            for (int i = 0; i < array.length(); i++) {
                                SwasCompletedMachineModel machineModel = new SwasCompletedMachineModel();
                                JSONObject object1 = array.getJSONObject(i);
                                machineModel.setMachineParameterLogID(object1.getString("MachineParameterLogID"));
                                machineModel.setOEMID(object1.getString("OEMID"));
                                machineModel.setCustomerID(object1.getString("CustomerID"));
                                machineModel.setServiceCallID(object1.getString("ServiceCallID"));
                                machineModel.setMachineParameters(object1.getString("MachineParameters"));
                                machineModel.setServiceCallStartDate(object1.getString("ServiceCallStartDate"));
                                machineModel.setServiceCallEndDate(object1.getString("ServiceCallEndDate"));
                                machineModel.setParameterValue(object1.getString("ParameterValue"));
                                machineModel.setRemark(object1.getString("Remark"));
                                machineModel. setCreatedBy(object1.getString("CreatedBy"));
                                machineModel.setCreatedDate(object1.getString("CreatedDate"));

                                //swasServiceModel .setServicecall_status_id(object1.getString("ServiceCallStatusID"));

                                model.add(machineModel);

                            }
                            swasCompletedMachineAdapter = new SwasCompletedMachineAdapter(SwasMachineParametersActivity.this,model);
                            alert_rv_list.setLayoutManager(new LinearLayoutManager(SwasMachineParametersActivity.this, LinearLayoutManager.VERTICAL, false));
                            alert_rv_list.setAdapter(swasCompletedMachineAdapter);


                        } else if (array.length() == 0) {
                        }
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public class SaveMachineParameters extends AsyncTask<String,String,String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SwasMachineParametersActivity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String getResponse="";
            String machine_start_date = ed_machine_start_date.getText().toString();
            String url= Retroconfig.BASEURL_SWAS + "insertmachineparametersforservicecall";

            try {
                HttpParams httpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(httpParams, 60000);
                HttpConnectionParams.setSoTimeout(httpParams, 60000);
                HttpClient client = new DefaultHttpClient(httpParams);
                HttpPost httpPost = new HttpPost(url);

                httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
                JSONArray array=new JSONArray();
                for (int i= 0;i<model.size();i++)
                {
                    try {
                        JSONObject object=new JSONObject();
                        object.put("OEMID",oem_id);
                        object.put("CustomerID",customer_id);
                        object.put("ServiceCallID",service_call_id);
                        object.put("ServiceEngineerID",service_eng_id);
                        object.put("MachineParameters",model.get(i).getMachineParameters());
                        object.put("ServiceStartDate",machine_start_date);
                        object.put("ParameterValue",model.get(i).getParameterValue());
                        object.put("Remark",model.get(i).getRemark());
                        object.put("CreatedBy",created_by);
                        array.put(object);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }
                httpPost.setEntity(new StringEntity(array.toString(),"UTF-8"));
                httpPost.setParams(httpParams);

                HttpResponse response = client.execute(httpPost);
                StatusLine statusLine = response.getStatusLine();

                if (statusLine.getStatusCode() == HttpStatus.SC_OK || statusLine.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();

                    getResponse = convertStreamToString(inputStream);
                }
                else if (statusLine.getStatusCode()==HttpStatus.SC_UNAUTHORIZED)
                {
                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();

                    getResponse = convertStreamToString(inputStream);
                }
                else {
                    response.getEntity().getContent().close();
                }
            }

            catch (Exception e) {
                Log.i(URL_Constants.TAG, "Error Message in Exception :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_EXCEPTION;

            }
            return getResponse;
        }

        @Override
        protected void onPostExecute(String getResponse) {
            super.onPostExecute(getResponse);
            progressDialog.dismiss();
            if (!getResponse.equals("Error"))
            {
                Toast.makeText(SwasMachineParametersActivity.this, "Machine parameter inserted successfully", Toast.LENGTH_SHORT).show();
                model.clear();
                Intent i = new Intent( SwasMachineParametersActivity.this,Swas_Deatails_of_Inprocess_Call_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("status_id",status_id);
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("Typeofservicecall_id",type_of_Service_Call_id);
                bundle.putString("customer_id",customer_id);
                i.putExtras(bundle);
                startActivity(i);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(SwasMachineParametersActivity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);

        // Set content width and height
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(SwasMachineParametersActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }

    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(SwasMachineParametersActivity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();

                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    oem_id= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));
                    service_eng_id= cursor.getString(cursor.getColumnIndex(Constants.USER_LOGIN_ID));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));


                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout() {
        finish();
        Toast toast= Toast.makeText(SwasMachineParametersActivity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(SwasMachineParametersActivity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();


        if (id==R.id.nav_swas)
        {
            Intent i=new Intent(SwasMachineParametersActivity.this, Swas_Home_Actvity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    public void checkNetworkConnection(){
        AlertDialog.Builder builder =new AlertDialog.Builder(SwasMachineParametersActivity.this);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i= new Intent(SwasMachineParametersActivity.this,Swas_Deatails_of_Inprocess_Call_Activity.class);
        Bundle bundle = new Bundle();
        bundle.putString("service_call_id",service_call_id);
        bundle.putString("status_id",status_id);
        bundle.putString("Typeofservicecall_id",type_of_Service_Call_id);
        bundle.putString("customer_id",customer_id);
        i.putExtras(bundle);
        startActivity(i);
        finish();
    }
}