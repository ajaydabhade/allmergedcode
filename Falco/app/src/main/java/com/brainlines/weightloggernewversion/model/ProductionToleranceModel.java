package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ProductionToleranceModel implements Parcelable {

    String ProductionToleranceID,Grammage,TolerancePositive,ToleranceNegetive,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy;
    int OEMID;
    boolean Isactive,IsDeleted;

    public ProductionToleranceModel(String productionToleranceID, String grammage, String tolerancePositive, String toleranceNegetive, String createdDate, String modifiedDate, String createdBy, String modifiedBy, int OEMID, boolean isactive, boolean isDeleted) {
        ProductionToleranceID = productionToleranceID;
        Grammage = grammage;
        TolerancePositive = tolerancePositive;
        ToleranceNegetive = toleranceNegetive;
        CreatedDate = createdDate;
        ModifiedDate = modifiedDate;
        CreatedBy = createdBy;
        ModifiedBy = modifiedBy;
        this.OEMID = OEMID;
        Isactive = isactive;
        IsDeleted = isDeleted;
    }

    protected ProductionToleranceModel(Parcel in) {
        ProductionToleranceID = in.readString();
        Grammage = in.readString();
        TolerancePositive = in.readString();
        ToleranceNegetive = in.readString();
        CreatedDate = in.readString();
        ModifiedDate = in.readString();
        CreatedBy = in.readString();
        ModifiedBy = in.readString();
        OEMID = in.readInt();
        Isactive = in.readByte() != 0;
        IsDeleted = in.readByte() != 0;
    }

    public static final Creator<ProductionToleranceModel> CREATOR = new Creator<ProductionToleranceModel>() {
        @Override
        public ProductionToleranceModel createFromParcel(Parcel in) {
            return new ProductionToleranceModel(in);
        }

        @Override
        public ProductionToleranceModel[] newArray(int size) {
            return new ProductionToleranceModel[size];
        }
    };

    public String getProductionToleranceID() {
        return ProductionToleranceID;
    }

    public void setProductionToleranceID(String productionToleranceID) {
        ProductionToleranceID = productionToleranceID;
    }

    public String getGrammage() {
        return Grammage;
    }

    public void setGrammage(String grammage) {
        Grammage = grammage;
    }

    public String getTolerancePositive() {
        return TolerancePositive;
    }

    public void setTolerancePositive(String tolerancePositive) {
        TolerancePositive = tolerancePositive;
    }

    public String getToleranceNegetive() {
        return ToleranceNegetive;
    }

    public void setToleranceNegetive(String toleranceNegetive) {
        ToleranceNegetive = toleranceNegetive;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public int getOEMID() {
        return OEMID;
    }

    public void setOEMID(int OEMID) {
        this.OEMID = OEMID;
    }

    public boolean isIsactive() {
        return Isactive;
    }

    public void setIsactive(boolean isactive) {
        Isactive = isactive;
    }

    public boolean isDeleted() {
        return IsDeleted;
    }

    public void setDeleted(boolean deleted) {
        IsDeleted = deleted;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ProductionToleranceID);
        dest.writeString(Grammage);
        dest.writeString(TolerancePositive);
        dest.writeString(ToleranceNegetive);
        dest.writeString(CreatedDate);
        dest.writeString(ModifiedDate);
        dest.writeString(CreatedBy);
        dest.writeString(ModifiedBy);
        dest.writeInt(OEMID);
        dest.writeByte((byte) (Isactive ? 1 : 0));
        dest.writeByte((byte) (IsDeleted ? 1 : 0));
    }
}
