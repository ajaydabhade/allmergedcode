package com.brainlines.weightloggernewversion.listener;

import com.brainlines.weightloggernewversion.model.MenuModel;

public interface MenuClickListenener {

    void clickonmenu(MenuModel model);

}
