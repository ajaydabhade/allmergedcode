package com.brainlines.weightloggernewversion.FalcoJK.JKModel;

public class StartTicketPackModel {
    String day,tk_Id,tk_TicketNo,tk_Plant,tk_Quantity,tk_Pending,tk_NormsQty,tk_SKU,tk_Start_Date,tk_End_Date,flag1;
    String today1;
    String actualstartdate1;
    String actualenddate1;
    String today2;

    public String getTotal_qty() {
        return total_qty;
    }

    public void setTotal_qty(String total_qty) {
        this.total_qty = total_qty;
    }

    String total_qty;
    String Created_date,Created_by,approve_date,approve_by,actvie,approve,actual_start_date,actual_end_date,today,TicketId;

    public String getTicketId() {
        return TicketId;
    }

    public void setTicketId(String ticketId) {
        TicketId = ticketId;
    }

    public String getCreated_date() {
        return Created_date;
    }

    public void setCreated_date(String created_date) {
        Created_date = created_date;
    }

    public String getCreated_by() {
        return Created_by;
    }

    public void setCreated_by(String created_by) {
        Created_by = created_by;
    }

    public String getApprove_date() {
        return approve_date;
    }

    public void setApprove_date(String approve_date) {
        this.approve_date = approve_date;
    }

    public String getApprove_by() {
        return approve_by;
    }

    public void setApprove_by(String approve_by) {
        this.approve_by = approve_by;
    }

    public String getActvie() {
        return actvie;
    }

    public void setActvie(String actvie) {
        this.actvie = actvie;
    }

    public String getApprove() {
        return approve;
    }

    public void setApprove(String approve) {
        this.approve = approve;
    }

    public String getActual_start_date() {
        return actual_start_date;
    }

    public void setActual_start_date(String actual_start_date) {
        this.actual_start_date = actual_start_date;
    }

    public String getActual_end_date() {
        return actual_end_date;
    }

    public void setActual_end_date(String actual_end_date) {
        this.actual_end_date = actual_end_date;
    }

    public String getToday() {
        return today;
    }

    public void setToday(String today) {
        this.today = today;
    }

    public String getStr_txt_pending_qty() {
        return str_txt_pending_qty;
    }

    public void setStr_txt_pending_qty(String str_txt_pending_qty) {
        this.str_txt_pending_qty = str_txt_pending_qty;
    }

    String str_txt_pending_qty;

    public String getStr_compl_qty() {
        return str_compl_qty;
    }

    public void setStr_compl_qty(String str_compl_qty) {
        this.str_compl_qty = str_compl_qty;
    }

    String str_compl_qty;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTk_Id() {
        return tk_Id;
    }

    public void setTk_Id(String tk_Id) {
        this.tk_Id = tk_Id;
    }

    public String getTk_TicketNo() {
        return tk_TicketNo;
    }

    public void setTk_TicketNo(String tk_TicketNo) {
        this.tk_TicketNo = tk_TicketNo;
    }

    public String getTk_Plant() {
        return tk_Plant;
    }

    public void setTk_Plant(String tk_Plant) {
        this.tk_Plant = tk_Plant;
    }

    public String getTk_Quantity() {
        return tk_Quantity;
    }

    public void setTk_Quantity(String tk_Quantity) {
        this.tk_Quantity = tk_Quantity;
    }

    public String getTk_Pending() {
        return tk_Pending;
    }

    public void setTk_Pending(String tk_Pending) {
        this.tk_Pending = tk_Pending;
    }

    public String getTk_NormsQty() {
        return tk_NormsQty;
    }

    public void setTk_NormsQty(String tk_NormsQty) {
        this.tk_NormsQty = tk_NormsQty;
    }

    public String getTk_SKU() {
        return tk_SKU;
    }

    public void setTk_SKU(String tk_SKU) {
        this.tk_SKU = tk_SKU;
    }

    public String getTk_Start_Date() {
        return tk_Start_Date;
    }

    public void setTk_Start_Date(String tk_Start_Date) {
        this.tk_Start_Date = tk_Start_Date;
    }

    public String getTk_End_Date() {
        return tk_End_Date;
    }

    public void setTk_End_Date(String tk_End_Date) {
        this.tk_End_Date = tk_End_Date;
    }

    public String getFlag1() {
        return flag1;
    }

    public void setFlag1(String flag1) {
        this.flag1 = flag1;
    }

    public String getToday1() {
        return today1;
    }

    public void setToday1(String today1) {
        this.today1 = today1;
    }

    public String getActualstartdate1() {
        return actualstartdate1;
    }

    public void setActualstartdate1(String actualstartdate1) {
        this.actualstartdate1 = actualstartdate1;
    }

    public String getActualenddate1() {
        return actualenddate1;
    }

    public void setActualenddate1(String actualenddate1) {
        this.actualenddate1 = actualenddate1;
    }

    public String getToday2() {
        return today2;
    }

    public void setToday2(String today2) {
        this.today2 = today2;
    }
}
