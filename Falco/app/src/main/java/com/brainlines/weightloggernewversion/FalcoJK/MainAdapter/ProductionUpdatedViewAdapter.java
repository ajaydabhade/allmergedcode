package com.brainlines.weightloggernewversion.FalcoJK.MainAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.ProductionViewTrackingModel;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class ProductionUpdatedViewAdapter extends RecyclerView.Adapter<ProductionUpdatedViewAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<ProductionViewTrackingModel> ticket_list;
    View.OnClickListener mClickListener;
    Context context;
    String str_tk_no;

    public ProductionUpdatedViewAdapter(Context ctx, ArrayList<ProductionViewTrackingModel> ticket_list) {
        inflater = LayoutInflater.from(ctx);
        this.ticket_list = ticket_list;
        this.context = ctx;
    }


    @Override
    public int getItemCount() {
        return ticket_list.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_update_production_ticket_data, parent, false);
        final MyViewHolder holder = new MyViewHolder(view);

       /* holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onClick(view);

            }
        });*/


        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int i) {
        holder.upopeartions.setText(ticket_list.get(i).getOpr_operation_name());
        holder.upedokay.setText(ticket_list.get(i).getPtk_OkQty());
        holder.upedreject.setText(ticket_list.get(i).getPtk_RejectQty());
        holder.upedrework.setText(ticket_list.get(i).getPtk_ReworkQty());
        holder.upedsecond.setText(ticket_list.get(i).getPtk_SecondQty());
        holder.upedwip.setText(ticket_list.get(i).getPtk_WIPQty());

    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        //TextView viewopeartions,view_current_route,viewokay,viewrework,viewreject,viewwip,viewsecond;
        TextView upopeartions,up_current_route;
        ImageButton img_route,img_submit;
        EditText upedokay,upedrework,upedreject,upedwip,upedsecond;


        public MyViewHolder(View itemView) {
            super(itemView);
            upopeartions=(TextView)itemView.findViewById(R.id.upopeartions);
            up_current_route=(TextView)itemView.findViewById(R.id.up_current_route);
            upedrework=(EditText) itemView.findViewById(R.id.upedrework);
            upedokay=(EditText)itemView.findViewById(R.id.upedokay);
            upedreject=(EditText)itemView.findViewById(R.id.upedreject);
            upedwip=(EditText)itemView.findViewById(R.id.upedwip);
            upedsecond=(EditText)itemView.findViewById(R.id.upedsecond);
            img_route=(ImageButton)itemView.findViewById(R.id.img_route);
            img_submit=(ImageButton)itemView.findViewById(R.id.img_submit);
        }
    }
}
