package com.brainlines.weightloggernewversion.FalcoJK.JKModel;

public class PackingTicketModel {
 String ID;
    String tk_Id;
    String tk_TicketNo;
    String tk_SONO;
    String tk_UploadRefNumber;
    String tk_Plant;
    String tk_SKU;
    String tk_Prod_SKU;
    String tk_Release_Date;
    String tk_Start_Date;
    String EDD;
    String tk_End_Date;
    String tk_Quantity;
    String pln_Packing_Norms;
    String tk_Unpaking_SKU;
    String tk_Unpacking;
    String MonthStart;
    String MonthEnd;
    String tk_Active;
    String tk_Release;
    String tk_produce_qty;

    public String getTk_produce_qty() {
        return tk_produce_qty;
    }

    public void setTk_produce_qty(String tk_produce_qty) {
        this.tk_produce_qty = tk_produce_qty;
    }

    public String getTk_actual_end_Date() {
        return tk_actual_end_Date;
    }

    public void setTk_actual_end_Date(String tk_actual_end_Date) {
        this.tk_actual_end_Date = tk_actual_end_Date;
    }

    String tk_actual_end_Date;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getTk_Id() {
        return tk_Id;
    }

    public void setTk_Id(String tk_Id) {
        this.tk_Id = tk_Id;
    }

    public String getTk_TicketNo() {
        return tk_TicketNo;
    }

    public void setTk_TicketNo(String tk_TicketNo) {
        this.tk_TicketNo = tk_TicketNo;
    }

    public String getTk_SONO() {
        return tk_SONO;
    }

    public void setTk_SONO(String tk_SONO) {
        this.tk_SONO = tk_SONO;
    }

    public String getTk_UploadRefNumber() {
        return tk_UploadRefNumber;
    }

    public void setTk_UploadRefNumber(String tk_UploadRefNumber) {
        this.tk_UploadRefNumber = tk_UploadRefNumber;
    }

    public String getTk_Plant() {
        return tk_Plant;
    }

    public void setTk_Plant(String tk_Plant) {
        this.tk_Plant = tk_Plant;
    }

    public String getTk_SKU() {
        return tk_SKU;
    }

    public void setTk_SKU(String tk_SKU) {
        this.tk_SKU = tk_SKU;
    }

    public String getTk_Prod_SKU() {
        return tk_Prod_SKU;
    }

    public void setTk_Prod_SKU(String tk_Prod_SKU) {
        this.tk_Prod_SKU = tk_Prod_SKU;
    }

    public String getTk_Release_Date() {
        return tk_Release_Date;
    }

    public void setTk_Release_Date(String tk_Release_Date) {
        this.tk_Release_Date = tk_Release_Date;
    }

    public String getTk_Start_Date() {
        return tk_Start_Date;
    }

    public void setTk_Start_Date(String tk_Start_Date) {
        this.tk_Start_Date = tk_Start_Date;
    }

    public String getEDD() {
        return EDD;
    }

    public void setEDD(String EDD) {
        this.EDD = EDD;
    }

    public String getTk_End_Date() {
        return tk_End_Date;
    }

    public void setTk_End_Date(String tk_End_Date) {
        this.tk_End_Date = tk_End_Date;
    }

    public String getTk_Quantity() {
        return tk_Quantity;
    }

    public void setTk_Quantity(String tk_Quantity) {
        this.tk_Quantity = tk_Quantity;
    }

    public String getPln_Packing_Norms() {
        return pln_Packing_Norms;
    }

    public void setPln_Packing_Norms(String pln_Packing_Norms) {
        this.pln_Packing_Norms = pln_Packing_Norms;
    }

    public String getTk_Unpaking_SKU() {
        return tk_Unpaking_SKU;
    }

    public void setTk_Unpaking_SKU(String tk_Unpaking_SKU) {
        this.tk_Unpaking_SKU = tk_Unpaking_SKU;
    }

    public String getTk_Unpacking() {
        return tk_Unpacking;
    }

    public void setTk_Unpacking(String tk_Unpacking) {
        this.tk_Unpacking = tk_Unpacking;
    }

    public String getMonthStart() {
        return MonthStart;
    }

    public void setMonthStart(String monthStart) {
        MonthStart = monthStart;
    }

    public String getMonthEnd() {
        return MonthEnd;
    }

    public void setMonthEnd(String monthEnd) {
        MonthEnd = monthEnd;
    }

    public String getTk_Active() {
        return tk_Active;
    }

    public void setTk_Active(String tk_Active) {
        this.tk_Active = tk_Active;
    }

    public String getTk_Release() {
        return tk_Release;
    }

    public void setTk_Release(String tk_Release) {
        this.tk_Release = tk_Release;
    }
}
