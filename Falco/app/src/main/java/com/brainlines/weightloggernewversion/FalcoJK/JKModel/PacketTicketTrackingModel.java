package com.brainlines.weightloggernewversion.FalcoJK.JKModel;

public class PacketTicketTrackingModel {
    String tt_id,tt_TicketID,tt_TicketNo,tt_Plant,tt_Date,tt_TotalQuantity,tt_Qty,tt_PendingQty,tt_CreatedDate,tt_CreatedBy;
    String tt_ApprovedDate,tt_ApprovedBy,tt_Active,tt_Approve,flag1,tk_SKU,tk_Start_Date,tk_End_Date,actualstartdate,actualenddate;
    String today;

    public String getStr_pendingqty() {
        return str_pendingqty;
    }

    public void setStr_pendingqty(String str_pendingqty) {
        this.str_pendingqty = str_pendingqty;
    }

    String str_pendingqty;

    public String getStr_txt_compl_qty() {
        return str_txt_compl_qty;
    }

    public void setStr_txt_compl_qty(String str_txt_compl_qty) {
        this.str_txt_compl_qty = str_txt_compl_qty;
    }

    String str_txt_compl_qty;

    public String getTt_id() {
        return tt_id;
    }

    public void setTt_id(String tt_id) {
        this.tt_id = tt_id;
    }

    public String getTt_TicketID() {
        return tt_TicketID;
    }

    public void setTt_TicketID(String tt_TicketID) {
        this.tt_TicketID = tt_TicketID;
    }

    public String getTt_TicketNo() {
        return tt_TicketNo;
    }

    public void setTt_TicketNo(String tt_TicketNo) {
        this.tt_TicketNo = tt_TicketNo;
    }

    public String getTt_Plant() {
        return tt_Plant;
    }

    public void setTt_Plant(String tt_Plant) {
        this.tt_Plant = tt_Plant;
    }

    public String getTt_Date() {
        return tt_Date;
    }

    public void setTt_Date(String tt_Date) {
        this.tt_Date = tt_Date;
    }

    public String getTt_TotalQuantity() {
        return tt_TotalQuantity;
    }

    public void setTt_TotalQuantity(String tt_TotalQuantity) {
        this.tt_TotalQuantity = tt_TotalQuantity;
    }

    public String getTt_Qty() {
        return tt_Qty;
    }

    public void setTt_Qty(String tt_Qty) {
        this.tt_Qty = tt_Qty;
    }

    public String getTt_PendingQty() {
        return tt_PendingQty;
    }

    public void setTt_PendingQty(String tt_PendingQty) {
        this.tt_PendingQty = tt_PendingQty;
    }

    public String getTt_CreatedDate() {
        return tt_CreatedDate;
    }

    public void setTt_CreatedDate(String tt_CreatedDate) {
        this.tt_CreatedDate = tt_CreatedDate;
    }

    public String getTt_CreatedBy() {
        return tt_CreatedBy;
    }

    public void setTt_CreatedBy(String tt_CreatedBy) {
        this.tt_CreatedBy = tt_CreatedBy;
    }

    public String getTt_ApprovedDate() {
        return tt_ApprovedDate;
    }

    public void setTt_ApprovedDate(String tt_ApprovedDate) {
        this.tt_ApprovedDate = tt_ApprovedDate;
    }

    public String getTt_ApprovedBy() {
        return tt_ApprovedBy;
    }

    public void setTt_ApprovedBy(String tt_ApprovedBy) {
        this.tt_ApprovedBy = tt_ApprovedBy;
    }

    public String getTt_Active() {
        return tt_Active;
    }

    public void setTt_Active(String tt_Active) {
        this.tt_Active = tt_Active;
    }

    public String getTt_Approve() {
        return tt_Approve;
    }

    public void setTt_Approve(String tt_Approve) {
        this.tt_Approve = tt_Approve;
    }

    public String getFlag1() {
        return flag1;
    }

    public void setFlag1(String flag1) {
        this.flag1 = flag1;
    }

    public String getTk_SKU() {
        return tk_SKU;
    }

    public void setTk_SKU(String tk_SKU) {
        this.tk_SKU = tk_SKU;
    }

    public String getTk_Start_Date() {
        return tk_Start_Date;
    }

    public void setTk_Start_Date(String tk_Start_Date) {
        this.tk_Start_Date = tk_Start_Date;
    }

    public String getTk_End_Date() {
        return tk_End_Date;
    }

    public void setTk_End_Date(String tk_End_Date) {
        this.tk_End_Date = tk_End_Date;
    }

    public String getActualstartdate() {
        return actualstartdate;
    }

    public void setActualstartdate(String actualstartdate) {
        this.actualstartdate = actualstartdate;
    }

    public String getActualenddate() {
        return actualenddate;
    }

    public void setActualenddate(String actualenddate) {
        this.actualenddate = actualenddate;
    }

    public String getToday() {
        return today;
    }

    public void setToday(String today) {
        this.today = today;
    }
}
