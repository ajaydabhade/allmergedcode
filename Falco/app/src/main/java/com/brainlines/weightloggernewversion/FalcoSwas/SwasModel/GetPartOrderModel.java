package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

import android.os.Parcel;
import android.os.Parcelable;

public class GetPartOrderModel implements Parcelable {

    String ServiceCallID;
    String CustomerID;

    public GetPartOrderModel(String serviceCallID, String customerID) {
        ServiceCallID = serviceCallID;
        CustomerID = customerID;
    }

    protected GetPartOrderModel(Parcel in) {
        ServiceCallID = in.readString();
        CustomerID = in.readString();
    }

    public static final Creator<GetPartOrderModel> CREATOR = new Creator<GetPartOrderModel>() {
        @Override
        public GetPartOrderModel createFromParcel(Parcel in) {
            return new GetPartOrderModel(in);
        }

        @Override
        public GetPartOrderModel[] newArray(int size) {
            return new GetPartOrderModel[size];
        }
    };

    public String getServiceCallID() {
        return ServiceCallID;
    }

    public void setServiceCallID(String serviceCallID) {
        ServiceCallID = serviceCallID;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ServiceCallID);
        dest.writeString(CustomerID);
    }
}
