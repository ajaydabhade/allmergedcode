package com.brainlines.weightloggernewversion.adapter.releaseadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.adapter.LoggerListAdapter;
import com.brainlines.weightloggernewversion.model.LoggersFromDBModel;

import java.util.ArrayList;

public class EngageLoggerListAdapter extends RecyclerView.Adapter<EngageLoggerListAdapter.MyViewHolder> {
    Context context;
    ArrayList<LoggersFromDBModel> loggerList = new ArrayList<>();

    public EngageLoggerListAdapter(Context context, ArrayList<LoggersFromDBModel> loggerList) {
        this.context = context;
        this.loggerList = loggerList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_engage_logger_list_for_releasing,parent,false);
        return new EngageLoggerListAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        LoggersFromDBModel model = loggerList.get(position);
        if (model != null){
            holder.txt_Logger_name.setText(model.getLogger_name());
        }
    }

    @Override
    public int getItemCount() {
        return loggerList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_Logger_name;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_Logger_name = (itemView).findViewById(R.id.item_logger_name);
        }
    }
}
