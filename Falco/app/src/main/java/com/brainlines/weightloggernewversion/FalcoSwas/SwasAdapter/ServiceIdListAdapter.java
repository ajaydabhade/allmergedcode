package com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasListener.SendSingleServiceBundleToSwasServiceCallActivity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasServiceCallIdModel;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class ServiceIdListAdapter extends RecyclerView.Adapter<ServiceIdListAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<SwasServiceCallIdModel> service_id_list;
    View.OnClickListener mClickListener;
    Context context;
    String str_start_date, str_end_Date, str_tk_id, str_approved_by, str_tk_release_date, main_string_release_date;
    String str_tkid;
    RecyclerView rv_packingtickets;
    TicketListRvAdapter adapter;
    SendSingleServiceBundleToSwasServiceCallActivity listener;

    public ServiceIdListAdapter(Context ctx, ArrayList<SwasServiceCallIdModel> ServiceIdList,SendSingleServiceBundleToSwasServiceCallActivity listener) {
        inflater = LayoutInflater.from(ctx);
        this.service_id_list = ServiceIdList;
        this.context = ctx;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_swas_service_id_details, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final SwasServiceCallIdModel model = service_id_list.get(position);
        if (model!=null) {

            holder.item_txt_service_id.setText(model.getServiceCallID());
            holder.item_txt_customer_name.setText(model.getLOCATIONNAME());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String status_name = model.getStatusText();
                    if (status_name.equals("Assigned Call"))
                    {
                        //Intent i = new Intent(context, Swas_Assigned_Call_Activity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("service_call_id",model.getServiceCallID());
                        bundle.putString("status_id",model.getServiceCallStatusID());
                        bundle.putString("Typeofservicecall_id",model.getServiceCallStatusID());
                        bundle.putString("customer_id",model.getCustomerID());
                        //i.putExtras(bundle);
                        //context.startActivity(i);
                        listener.sendBundle(bundle,"Assigned Call");
                    }
                    else if (status_name.equals("In process call"))
                    {
                        //Intent i = new Intent(context, Swas_Deatails_of_Inprocess_Call_Activity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("service_call_id",model.getServiceCallID());
                        bundle.putString("status_id",model.getServiceCallStatusID());
                        bundle.putString("Typeofservicecall_id",model.getServiceCallStatusID());
                        bundle.putString("customer_id",model.getCustomerID());
                        bundle.putString("location_name",model.getLOCATIONNAME());
                        //i.putExtras(bundle);
                        //context.startActivity(i);
                        listener.sendBundle(bundle,"In process call");
                    }
                    else if (status_name.equals("Completed Call"))
                    {
                        //Intent i = new Intent(context, Swas_Details_Of_Completed_Call.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("service_call_id",model.getServiceCallID());
                        bundle.putString("status_id",model.getServiceCallStatusID());
                        bundle.putString("Typeofservicecall_id",model.getServiceCallStatusID());
                        bundle.putString("customer_id",model.getCustomerID());
                        //i.putExtras(bundle);
                        //context.startActivity(i);
                        listener.sendBundle(bundle,"Completed Call");
                    }

                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return service_id_list.size();
    }

    class MyViewHolder  extends RecyclerView.ViewHolder{
        TextView item_txt_service_id,item_txt_customer_name;

        public MyViewHolder(View itemView) {
            super(itemView);
            item_txt_service_id = itemView.findViewById(R.id.item_txt_service_id);
            item_txt_customer_name = itemView.findViewById(R.id.item_txt_customer_name);
        }
    }
}