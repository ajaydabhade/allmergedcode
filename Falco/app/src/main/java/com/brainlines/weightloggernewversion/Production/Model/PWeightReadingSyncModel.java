package com.brainlines.weightloggernewversion.Production.Model;

public class PWeightReadingSyncModel {


    String OEMID,MacID,LoggerID,PWeightID,BatchID,ProductionTagName,Weight,Shift,TargetGmPerPouch,ActualGmPerPouch,ExcessGiveawayPerPouch,CumilativeExcessGiveaway,
            CumilativeQuantity,CumilativePouches,NegetiveGigiveawayFromTag,UCLgm,LCLgm,Status,CreatedDate,CreatedBy,ModifiedBy,ModifiedDate;

    public PWeightReadingSyncModel(String OEMID, String macID, String loggerID, String PWeightID,String batchID, String productionTagName, String weight, String shift, String targetGmPerPouch, String actualGmPerPouch, String excessGiveawayPerPouch, String cumilativeExcessGiveaway, String cumilativeQuantity, String cumilativePouches, String negetiveGigiveawayFromTag, String UCLgm, String LCLgm, String status, String createdDate, String createdBy, String modifiedBy, String modifiedDate) {
        this.OEMID = OEMID;
        MacID = macID;
        LoggerID = loggerID;
        this.PWeightID = PWeightID;
        BatchID = batchID;
        ProductionTagName = productionTagName;
        Weight = weight;
        Shift = shift;
        TargetGmPerPouch = targetGmPerPouch;
        ActualGmPerPouch = actualGmPerPouch;
        ExcessGiveawayPerPouch = excessGiveawayPerPouch;
        CumilativeExcessGiveaway = cumilativeExcessGiveaway;
        CumilativeQuantity = cumilativeQuantity;
        CumilativePouches = cumilativePouches;
        NegetiveGigiveawayFromTag = negetiveGigiveawayFromTag;
        this.UCLgm = UCLgm;
        this.LCLgm = LCLgm;
        Status = status;
        CreatedDate = createdDate;
        CreatedBy = createdBy;
        ModifiedBy = modifiedBy;
        ModifiedDate = modifiedDate;
    }

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getMacID() {
        return MacID;
    }

    public void setMacID(String macID) {
        MacID = macID;
    }

    public String getLoggerID() {
        return LoggerID;
    }

    public void setLoggerID(String loggerID) {
        LoggerID = loggerID;
    }

    public String getBatchID() {
        return BatchID;
    }

    public void setBatchID(String batchID) {
        BatchID = batchID;
    }

    public String getProductionTagName() {
        return ProductionTagName;
    }

    public void setProductionTagName(String productionTagName) {
        ProductionTagName = productionTagName;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    public String getShift() {
        return Shift;
    }

    public void setShift(String shift) {
        Shift = shift;
    }

    public String getTargetGmPerPouch() {
        return TargetGmPerPouch;
    }

    public void setTargetGmPerPouch(String targetGmPerPouch) {
        TargetGmPerPouch = targetGmPerPouch;
    }

    public String getActualGmPerPouch() {
        return ActualGmPerPouch;
    }

    public void setActualGmPerPouch(String actualGmPerPouch) {
        ActualGmPerPouch = actualGmPerPouch;
    }

    public String getExcessGiveawayPerPouch() {
        return ExcessGiveawayPerPouch;
    }

    public void setExcessGiveawayPerPouch(String excessGiveawayPerPouch) {
        ExcessGiveawayPerPouch = excessGiveawayPerPouch;
    }

    public String getCumilativeExcessGiveaway() {
        return CumilativeExcessGiveaway;
    }

    public void setCumilativeExcessGiveaway(String cumilativeExcessGiveaway) {
        CumilativeExcessGiveaway = cumilativeExcessGiveaway;
    }

    public String getCumilativeQuantity() {
        return CumilativeQuantity;
    }

    public void setCumilativeQuantity(String cumilativeQuantity) {
        CumilativeQuantity = cumilativeQuantity;
    }

    public String getCumilativePouches() {
        return CumilativePouches;
    }

    public void setCumilativePouches(String cumilativePouches) {
        CumilativePouches = cumilativePouches;
    }

    public String getNegetiveGigiveawayFromTag() {
        return NegetiveGigiveawayFromTag;
    }

    public void setNegetiveGigiveawayFromTag(String negetiveGigiveawayFromTag) {
        NegetiveGigiveawayFromTag = negetiveGigiveawayFromTag;
    }

    public String getUCLgm() {
        return UCLgm;
    }

    public void setUCLgm(String UCLgm) {
        this.UCLgm = UCLgm;
    }

    public String getLCLgm() {
        return LCLgm;
    }

    public void setLCLgm(String LCLgm) {
        this.LCLgm = LCLgm;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }
}
