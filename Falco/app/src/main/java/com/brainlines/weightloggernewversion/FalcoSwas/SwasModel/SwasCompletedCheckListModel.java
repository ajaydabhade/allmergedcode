package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

public class SwasCompletedCheckListModel {
    String ChecklistForSiteReadynessText,ChecklistForSiteReadynessID,Checked;

    public String getChecklistForSiteReadynessText() {
        return ChecklistForSiteReadynessText;
    }

    public void setChecklistForSiteReadynessText(String checklistForSiteReadynessText) {
        ChecklistForSiteReadynessText = checklistForSiteReadynessText;
    }

    public String getChecklistForSiteReadynessID() {
        return ChecklistForSiteReadynessID;
    }

    public void setChecklistForSiteReadynessID(String checklistForSiteReadynessID) {
        ChecklistForSiteReadynessID = checklistForSiteReadynessID;
    }

    public String getChecked() {
        return Checked;
    }

    public void setChecked(String checked) {
        Checked = checked;
    }
}
