package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MasterTablesModels implements Parcelable {

    int MasterTableId;
    String MasterTableName;
    String MasterTableDisplayName;

    public MasterTablesModels(int masterTableId, String masterTableName, String masterTableDisplayName) {
        MasterTableId = masterTableId;
        MasterTableName = masterTableName;
        MasterTableDisplayName = masterTableDisplayName;
    }

    protected MasterTablesModels(Parcel in) {
        MasterTableId = in.readInt();
        MasterTableName = in.readString();
        MasterTableDisplayName = in.readString();
    }

    public static final Creator<MasterTablesModels> CREATOR = new Creator<MasterTablesModels>() {
        @Override
        public MasterTablesModels createFromParcel(Parcel in) {
            return new MasterTablesModels(in);
        }

        @Override
        public MasterTablesModels[] newArray(int size) {
            return new MasterTablesModels[size];
        }
    };

    public int getMasterTableId() {
        return MasterTableId;
    }

    public void setMasterTableId(int masterTableId) {
        MasterTableId = masterTableId;
    }

    public String getMasterTableName() {
        return MasterTableName;
    }

    public void setMasterTableName(String masterTableName) {
        MasterTableName = masterTableName;
    }

    public String getMasterTableDisplayName() {
        return MasterTableDisplayName;
    }

    public void setMasterTableDisplayName(String masterTableDisplayName) {
        MasterTableDisplayName = masterTableDisplayName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(MasterTableId);
        dest.writeString(MasterTableName);
        dest.writeString(MasterTableDisplayName);
    }
}
