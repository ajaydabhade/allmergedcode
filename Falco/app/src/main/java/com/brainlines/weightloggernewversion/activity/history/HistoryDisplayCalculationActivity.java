package com.brainlines.weightloggernewversion.activity.history;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.activity.recalltest.RecallTestTagInfoModel;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.AppPreferences;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class HistoryDisplayCalculationActivity extends AppCompatActivity {
    TextView txt_report_fy,txt_report_customer,txt_report_oi_no,txt_report_pro_type,txt_report_product,txt_report_targetweight,txt_report_machine_model,txt_machine_sr_no,txt_report_target_wt_uom;
    DataBaseHelper helper;
    String loggerName,loggerIp,tagId,oinum;
    TextView txt_report_weight_min,txt_report_weight_max,txt_report_weight_mean,txt_report_weight_range,txt_report_weight_gm,txt_report_weight_sd,txt_report_weight_gmsd,txt_report_weight_status;
    TextView txt_report_bag_min,txt_report_bag_max,txt_report_bag_mean,txt_report_bag_range,txt_report_bag_status;
    TextView txt_report_paper_min,txt_report_paper_max,txt_report_paper_mean,txt_report_paper_range,txt_report_paper_status1;
    TextView txt_final_speedtest_status,txt_final_droptest_status,txt_final_sealquality_status,txt_final_papershift_status,txt_final_baglength_status;
    TextView txt_final_weight_status,txt_report_drop_test,txt_report_seal_quality,txt_headline_for_final_Result;
    TextView txtWeightReadingHeader,txt_header_papershift_calculation,txt_report_header_baglength;
    ArrayList<String> grammage_values_list = new ArrayList<>();
    DecimalFormat decimalFormat = new DecimalFormat("0.000");
    double str_kg_value_from_local_db;
    private String str_tarhetwt_uom;
    private String dbName;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_report);
        initUi();
        getProductionGrammageValues();
        getTagInfoAgainstOinum();
        getCalculatedValuesFromWeightTestAnalyticsTable(tagId);
        getCalculatedValuesFromBagLengthAnalyticsTable(tagId);
        getCalculatedValuesFromPaperShiftAnalyticsTable(tagId);



    }
    private void initUi()
    {
        dbName = AppPreferences.getDatabseName(this);
//        helper = new DataBaseHelper(this,dbName);
        helper = new DataBaseHelper(this);
        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));

        tagId = getIntent().getStringExtra("TagId");
        loggerName = getIntent().getStringExtra("LoggerName");
        loggerIp = getIntent().getStringExtra("LoggerIP");
        oinum = getIntent().getStringExtra("oinum");

        //Final result status
        txt_final_speedtest_status = findViewById(R.id.txt_final_speedtest_status);
        txt_final_droptest_status = findViewById(R.id.txt_final_droptest_status);
        txt_final_sealquality_status = findViewById(R.id.txt_final_sealquality_status);
        txt_final_papershift_status = findViewById(R.id.txt_final_papershift_status);
        txt_final_baglength_status = findViewById(R.id.txt_final_baglength_status);
        txt_final_weight_status = findViewById(R.id.txt_final_weight_status);
        txt_report_seal_quality = findViewById(R.id.txt_report_seal_quality);
        txt_report_drop_test = findViewById(R.id.txt_report_drop_test);
        txt_headline_for_final_Result = findViewById(R.id.txt_headline_for_final_Result);

        //batch details
        txt_report_fy = findViewById(R.id.txt_report_fy);
        txt_report_customer = findViewById(R.id.txt_report_customer);
        txt_report_oi_no = findViewById(R.id.txt_report_oi_no);
        txt_report_pro_type = findViewById(R.id.txt_report_pro_type);
        txt_report_product = findViewById(R.id.txt_report_product);
        txt_report_targetweight = findViewById(R.id.txt_report_targetweight);
        txt_report_machine_model = findViewById(R.id.txt_report_machine_model);
        txt_machine_sr_no = findViewById(R.id.txt_machine_sr_no);
        txt_report_target_wt_uom = findViewById(R.id.txt_report_target_wt_uom);

        //weight_reading_analytics;
        txt_report_weight_min = findViewById(R.id.txt_report_weight_min);
        txt_report_weight_max = findViewById(R.id.txt_report_weight_max);
        txt_report_weight_mean = findViewById(R.id.txt_report_weight_mean);
        txt_report_weight_range = findViewById(R.id.txt_report_weight_range);
        txt_report_weight_gm = findViewById(R.id.txt_report_weight_gm);
        txt_report_weight_sd = findViewById(R.id.txt_report_weight_sd);
        txt_report_weight_gmsd = findViewById(R.id.txt_report_weight_gmsd);
        txt_report_weight_status = findViewById(R.id.txt_report_weight_status);

        //bag_length_reading
        txt_report_bag_min = findViewById(R.id.txt_report_bag_min);
        txt_report_bag_max = findViewById(R.id.txt_report_bag_max);
        txt_report_bag_mean = findViewById(R.id.txt_report_bag_mean);
        txt_report_bag_range = findViewById(R.id.txt_report_bag_range);
        txt_report_bag_status = findViewById(R.id.txt_report_bag_status);

        //paper shift reading
        txt_report_paper_min = findViewById(R.id.txt_report_paper_min);
        txt_report_paper_max = findViewById(R.id.txt_report_paper_max);
        txt_report_paper_mean = findViewById(R.id.txt_report_paper_mean);
        txt_report_paper_range = findViewById(R.id.txt_report_paper_range);
        txt_report_paper_status1 = findViewById(R.id.txt_report_paper_status1);

        txtWeightReadingHeader = findViewById(R.id.txtWeightReadingHeader);
        txt_header_papershift_calculation = findViewById(R.id.txt_header_papershift_calculation);
        txt_report_header_baglength = findViewById(R.id.txt_report_header_baglength);

        txt_report_header_baglength.setText("Bag Length Reading Calculations" + " (" + "mm" + ")");
        txt_header_papershift_calculation.setText("Paper Shift Reading Calculations" + " (" + "mm" + ")");



    }

    public void getTagInfoAgainstOinum()
    {
        Cursor taginfo = helper.getTagInfoAgainstoinum(oinum);
        RecallTestTagInfoModel model = new RecallTestTagInfoModel();

        while (taginfo.moveToNext()){


            String tagid= taginfo.getString(0);
            String oemid= taginfo.getString(1);
            String macid= taginfo.getString(2);
            String loggerid= taginfo.getString(3);
            String oinumber= taginfo.getString(5);
            String targetweight= taginfo.getString(6);
            String customername= taginfo.getString(8);
            String producttype= taginfo.getString(10);
            String product= taginfo.getString(11);
            String operator= taginfo.getString(12);
            String targetweightunit= taginfo.getString(13);
            String targetaccuracytype= taginfo.getString(24);
            String targetaccuracyvalue= taginfo.getString(25);
            String machinesrno= taginfo.getString(26);
            String productbulk_main= taginfo.getString(27);
            String targetspeed= taginfo.getString(28);
            String customer_product= taginfo.getString(29);
            String head= taginfo.getString(30);
            String machineType = taginfo.getString(9);
            //targetweightunit =str_tarhetwt_uom;
            str_tarhetwt_uom = targetweightunit;



            String overall_status = taginfo.getString(58);
            String seal_quality = taginfo.getString(60);
            String drop_status = taginfo.getString(61);
            String speed1_status = taginfo.getString(62);
            String weight_status = taginfo.getString(63);
            String bag_status = taginfo.getString(64);
            String paper_status = taginfo.getString(65);

            txtWeightReadingHeader.setText("Weight Reading Calculations"+ " (" + targetweightunit + ")");

            txt_report_drop_test.setText(drop_status);
            txt_report_seal_quality.setText(seal_quality);

            txt_final_speedtest_status.setText(speed1_status);
            txt_final_baglength_status.setText(bag_status);
            txt_final_weight_status.setText(weight_status);
            txt_final_papershift_status.setText(paper_status);
            if (seal_quality.equals("OK"))
            {
                txt_final_sealquality_status.setText("PASS");
            }
            else
            {
                txt_final_sealquality_status.setText("FAIL");

            }
            if (drop_status.equals("OK"))
            {
                txt_final_droptest_status.setText("PASS");
            }
            else
            {
                txt_final_droptest_status.setText("FAIL");

            }
            txt_headline_for_final_Result.setText("Report for SO number " + oinumber + " is" + " " + overall_status);

            txt_report_customer.setText(customername);
            txt_report_oi_no.setText(oinumber);
            txt_report_pro_type.setText(producttype);
            txt_report_product.setText(product);
            txt_report_target_wt_uom.setText(targetweightunit);
            if (targetweightunit.equals("gm")) {

                txt_report_targetweight.setText(decimalFormat.format(targetweight));

            }
            else
            {
                if (str_tarhetwt_uom.equals("Kg"))
                {

                    String kgvalue = grammage_values_list.get(0);
                    str_kg_value_from_local_db = Double.parseDouble(kgvalue);
                }
                Double dweight = Double.parseDouble(targetweight)/str_kg_value_from_local_db;
                txt_report_targetweight.setText(decimalFormat.format(dweight));
            }

            txt_report_machine_model.setText(machineType);
            txt_machine_sr_no.setText(machinesrno);

            Log.d("TAG", "getSealTypes: ");
        }
    }

    private void getCalculatedValuesFromWeightTestAnalyticsTable(String tagId) {
        if (str_tarhetwt_uom.equals("gm")){
            Cursor calculatedValues = helper.getCalculatedValuesFromWeightResultsAnalytics(tagId);
            while (calculatedValues.moveToNext()){
                String minWeightReading = calculatedValues.getString(0);
                String maxWeightReading = calculatedValues.getString(1);
                String rangeWeightReading = calculatedValues.getString(2);
                String meanWeightReading = calculatedValues.getString(3);
                String sdWeightReading = calculatedValues.getString(4);
                String runWeightStatus = calculatedValues.getString(5);
                String gmWeight = calculatedValues.getString(6);

                String gmsdPercentWeight = calculatedValues.getString(9);
                txt_report_weight_min.setText(decimalFormat.format(Double.parseDouble(minWeightReading)));
                txt_report_weight_max.setText(decimalFormat.format(Double.parseDouble(maxWeightReading)));
                txt_report_weight_range.setText(decimalFormat.format(Double.parseDouble(rangeWeightReading)));
                txt_report_weight_mean.setText(decimalFormat.format(Double.parseDouble(meanWeightReading)));
                txt_report_weight_sd.setText(decimalFormat.format(Double.parseDouble(sdWeightReading)));
                txt_report_weight_status.setText(runWeightStatus);
                txt_report_weight_gm.setText(decimalFormat.format(Double.parseDouble(gmWeight)));
                txt_report_weight_gmsd.setText(decimalFormat.format(Double.parseDouble(gmsdPercentWeight)));

            }
        }
        else if(str_tarhetwt_uom.equals("Kg")){
            Cursor calculatedValues = helper.getCalculatedValuesFromWeightResultsAnalytics(tagId);
            while (calculatedValues.moveToNext()){
                String minWeightReading = calculatedValues.getString(0);
                String maxWeightReading = calculatedValues.getString(1);

                String meanWeightReading = calculatedValues.getString(3);
                String sdWeightReading = calculatedValues.getString(4);
                String runWeightStatus = calculatedValues.getString(5);
                String gmWeight = calculatedValues.getString(6);
                String gmsdPercentWeight = calculatedValues.getString(9);

                String rangeWeightReading = calculatedValues.getString(2);
                String srangeWeightReading= rangeWeightReading.replaceAll("[^0-9]", "");

                Double dmin = (Double.parseDouble(minWeightReading))/str_kg_value_from_local_db;
                Double dmax = (Double.parseDouble(maxWeightReading))/str_kg_value_from_local_db;
                Double drange = (Double.parseDouble(srangeWeightReading))/str_kg_value_from_local_db;
                Double dmean = (Double.parseDouble(meanWeightReading))/str_kg_value_from_local_db;
                Double dsd = (Double.parseDouble(sdWeightReading))/str_kg_value_from_local_db;
                Double dgm = (Double.parseDouble(gmWeight))/str_kg_value_from_local_db;
                Double dgmsd = (Double.parseDouble(gmsdPercentWeight))/str_kg_value_from_local_db;
                String strweightrange = "+/- " + decimalFormat.format(drange);
                txt_report_weight_min.setText(decimalFormat.format(dmin));
                txt_report_weight_max.setText(decimalFormat.format(dmax));
                txt_report_weight_range.setText(strweightrange);
                txt_report_weight_mean.setText(decimalFormat.format(dmean));
                txt_report_weight_sd.setText(decimalFormat.format(dsd));
                txt_report_weight_status.setText(runWeightStatus);
                txt_report_weight_gm.setText(decimalFormat.format(dgm));
                txt_report_weight_gmsd.setText(decimalFormat.format(dgmsd));
            }
        }
    }
    private void getCalculatedValuesFromBagLengthAnalyticsTable(String tagId) {
        Cursor calculatedValues = helper.getCalculatedValuesFromBaglengthAnalytics(tagId);
        while (calculatedValues.moveToNext()) {
            String minWeightReading = calculatedValues.getString(0);
            String maxWeightReading = calculatedValues.getString(1);
            String rangeWeightReading = calculatedValues.getString(2);
            String meanWeightReading = calculatedValues.getString(4);
            String runWeightStatus = calculatedValues.getString(3);

            txt_report_bag_min.setText(minWeightReading);
            txt_report_bag_max.setText(maxWeightReading);
            txt_report_bag_range.setText(rangeWeightReading);
            txt_report_bag_mean.setText(meanWeightReading);
            txt_report_bag_status.setText(runWeightStatus);
        }

    }

    private void getCalculatedValuesFromPaperShiftAnalyticsTable(String tagId) {
        Cursor calculatedValues = helper.getCalculatedValuesFromPaperShiftAnalytics(tagId);
        while (calculatedValues.moveToNext()){
            String minWeightReading = calculatedValues.getString(0);
            String maxWeightReading = calculatedValues.getString(1);
            String rangeWeightReading = calculatedValues.getString(2);
            String meanWeightReading = calculatedValues.getString(4);
            String runWeightStatus = calculatedValues.getString(3);

            txt_report_paper_min.setText(minWeightReading);
            txt_report_paper_max.setText(maxWeightReading);
            txt_report_paper_mean.setText(meanWeightReading);
            txt_report_paper_range.setText(rangeWeightReading);
            txt_report_paper_status1.setText(runWeightStatus);
        }
    }


    private void getProductionGrammageValues() {
        Cursor grammageValues = helper.getProductionGrammageValues();
        while (grammageValues.moveToNext()){
            String kgid = grammageValues.getString(0);
            String kgname = grammageValues.getString(1);
            String kgvalues = grammageValues.getString(2);

            grammage_values_list.add(kgvalues);
            Log.d("TAG", "getProductsUsedForTrial: ");
        }

    }

}
