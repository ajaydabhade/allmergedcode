package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class TableListModelForInsertion implements Parcelable {

    int ID;
    String TableName;
    String PKColumnName;

    public TableListModelForInsertion(int ID, String tableName, String PKColumnName) {
        this.ID = ID;
        TableName = tableName;
        this.PKColumnName = PKColumnName;
    }

    protected TableListModelForInsertion(Parcel in) {
        ID = in.readInt();
        TableName = in.readString();
        PKColumnName = in.readString();
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getTableName() {
        return TableName;
    }

    public void setTableName(String tableName) {
        TableName = tableName;
    }

    public String getPKColumnName() {
        return PKColumnName;
    }

    public void setPKColumnName(String PKColumnName) {
        this.PKColumnName = PKColumnName;
    }

    public static Creator<TableListModelForInsertion> getCREATOR() {
        return CREATOR;
    }

    public static final Creator<TableListModelForInsertion> CREATOR = new Creator<TableListModelForInsertion>() {
        @Override
        public TableListModelForInsertion createFromParcel(Parcel in) {
            return new TableListModelForInsertion(in);
        }

        @Override
        public TableListModelForInsertion[] newArray(int size) {
            return new TableListModelForInsertion[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ID);
        dest.writeString(TableName);
        dest.writeString(PKColumnName);
    }
}
