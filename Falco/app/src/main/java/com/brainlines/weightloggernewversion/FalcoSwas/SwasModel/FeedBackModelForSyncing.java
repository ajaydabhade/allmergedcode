package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

import android.os.Parcel;
import android.os.Parcelable;

public class FeedBackModelForSyncing implements Parcelable {

    String serviceCallId;
    String oemId;
    String userId;
    String feedBack;
    String createdBy;
    String feedbackId;

    public FeedBackModelForSyncing(String serviceCallId, String oemId, String userId, String feedBack, String createdBy, String feedbackId) {
        this.serviceCallId = serviceCallId;
        this.oemId = oemId;
        this.userId = userId;
        this.feedBack = feedBack;
        this.createdBy = createdBy;
        this.feedbackId = feedbackId;
    }

    protected FeedBackModelForSyncing(Parcel in) {
        serviceCallId = in.readString();
        oemId = in.readString();
        userId = in.readString();
        feedBack = in.readString();
        createdBy = in.readString();
        feedbackId = in.readString();
    }

    public static final Creator<FeedBackModelForSyncing> CREATOR = new Creator<FeedBackModelForSyncing>() {
        @Override
        public FeedBackModelForSyncing createFromParcel(Parcel in) {
            return new FeedBackModelForSyncing(in);
        }

        @Override
        public FeedBackModelForSyncing[] newArray(int size) {
            return new FeedBackModelForSyncing[size];
        }
    };

    public String getServiceCallId() {
        return serviceCallId;
    }

    public void setServiceCallId(String serviceCallId) {
        this.serviceCallId = serviceCallId;
    }

    public String getOemId() {
        return oemId;
    }

    public void setOemId(String oemId) {
        this.oemId = oemId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFeedBack() {
        return feedBack;
    }

    public void setFeedBack(String feedBack) {
        this.feedBack = feedBack;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(String feedbackId) {
        this.feedbackId = feedbackId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(serviceCallId);
        dest.writeString(oemId);
        dest.writeString(userId);
        dest.writeString(feedBack);
        dest.writeString(createdBy);
        dest.writeString(feedbackId);
    }
}
