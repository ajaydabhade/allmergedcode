package com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewPackingTicket;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.PackingTicketDatewiseQuantityModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.StartTicketPackModel;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.ViewTicketTableAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewPackingTicket.Listener.NewUpdatePackingTicketListener;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewPackingTicket.Model.NewPackingTicketModel;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewPackingTicket.adapter.UpdatePackingTicketAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.activity.FalcoJKHomeActivity;
import com.brainlines.weightloggernewversion.FalcoJK.activity.JKPackingTicketSelection;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;

public class JKNewViewPackingTicketActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, NewUpdatePackingTicketListener {

    RecyclerView grid_table_details;
    ViewTicketTableAdapter adapter1;
    String str_tk_id,str_tk_no,str_tk_startdate,str_tk_end_date,str_tk_qty,str_plant,str_sku;
    TextView txt_up_ticket_no,txt_up_plant,txt_up_tk_qty,txt_sku_13,txt_up_startdate,txt_up_enddate,txt_up_act_strat_date,txt_up_act_end_date;
    ImageView img_back,img_userinfo,nav_view_img;
    DrawerLayout drawer;
    String user_email,user_role,user_token;
    private TextView profile_user,profile_role;
    DataBaseHelper helper;
    SQLiteDatabase db;
    NewPackingTicketModel model;
    UpdatePackingTicketAdapter updatePackTicketRvAdapter;
    ArrayList<PackingTicketDatewiseQuantityModel> packingTicketDatewiseQuantityModelArrayList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_packing_ticket_view_navigation);
        grid_table_details = findViewById(R.id.grid_table_contents);
        initUi();
        setData();
        helper = new DataBaseHelper(JKNewViewPackingTicketActivity.this);
        db = helper.getWritableDatabase();
        getDatewiseQuantity();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_swas).setVisible(false);
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img=(ImageView)findViewById(R.id.nav_view_img);
        Bundle bundle=getIntent().getExtras();
        str_tk_id=bundle.getString("tk_id");
        str_tk_no=bundle.getString("tk_no");
        str_tk_startdate=bundle.getString("tk_start_date");
        str_tk_end_date=bundle.getString("tk_end_date");
        str_tk_qty=bundle.getString("tk_quantity");
        str_sku=bundle.getString("tk_sku_13");
        str_plant=bundle.getString("tk_plant");

        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }
        });


        //str_plant=bundle.getString("tk_plant");
        txt_up_ticket_no.setText(str_tk_no);
        txt_up_plant.setText(str_plant);
        txt_up_tk_qty.setText(str_tk_qty.concat(" no"));
        txt_up_enddate.setText(str_tk_end_date);
        txt_up_startdate.setText(str_tk_startdate);
        txt_sku_13.setText(str_sku);
        txt_up_plant.setText(str_plant);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(JKNewViewPackingTicketActivity.this, JKPackingTicketSelection.class);
                startActivity(i);
            }
        });
        img_userinfo.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
    }
    public void initUi()
    {
        txt_up_startdate=(TextView)findViewById(R.id.txt_up_startdate);
        txt_up_ticket_no=(TextView)findViewById(R.id.txt_up_ticket_no);
        txt_up_enddate=(TextView)findViewById(R.id.txt_up_enddate);
        txt_up_tk_qty=(TextView)findViewById(R.id.txt_up_tk_qty);
        txt_up_plant=(TextView)findViewById(R.id.txt_up_plant);
        txt_sku_13=(TextView)findViewById(R.id.txt_sku_13);
        txt_up_act_strat_date=(TextView)findViewById(R.id.txt_up_act_strat_date);
        txt_up_act_end_date=(TextView)findViewById(R.id.txt_up_act_end_date);
        img_back=(ImageView)findViewById(R.id.img_back);
        img_userinfo=(ImageView)findViewById(R.id.img_userinfo);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(JKNewViewPackingTicketActivity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);
        setData();
        // Set content width and height
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(JKNewViewPackingTicketActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }


    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(JKNewViewPackingTicketActivity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();
                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout()
    {
        finish();
        Toast toast= Toast.makeText(JKNewViewPackingTicketActivity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(JKNewViewPackingTicketActivity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);
    }

    public void getDatewiseQuantity()
    {
        Cursor getdatewisequantity = helper.getPackingTicketdaywisequantity(str_tk_no);
        while (getdatewisequantity.moveToNext())
        {
            PackingTicketDatewiseQuantityModel model = new PackingTicketDatewiseQuantityModel();
            model.setTt_id(getdatewisequantity.getString(1));
            model.setTt_TicketID(getdatewisequantity.getString(1));
            model.setTt_TicketNo(getdatewisequantity.getString(2));
            model.setTt_Plant(getdatewisequantity.getString(3));
            model.setTt_Date(getdatewisequantity.getString(4));
            model.setTt_TotalQuantity(getdatewisequantity.getString(5));
            model.setTt_Qty(getdatewisequantity.getString(6));
            model.setTt_PendingQty(getdatewisequantity.getString(7));
            model.setTt_CreatedDate(getdatewisequantity.getString(8));
            model.setTt_CreatedBy(getdatewisequantity.getString(9));
            model.setTt_ApprovedDate(getdatewisequantity.getString(10));
            model.setTt_ApprovedBy(getdatewisequantity.getString(11));
            model.setTt_Active(getdatewisequantity.getString(12));
            model.setTt_Approve(getdatewisequantity.getString(13));
            model.setFlag1(getdatewisequantity.getString(14));
            model.setTk_SKU(getdatewisequantity.getString(15));
            model.setTk_Start_Date(getdatewisequantity.getString(16));
            model.setTk_End_Date(getdatewisequantity.getString(17));
            model.setActualstartdate(getdatewisequantity.getString(18));
            model.setToday(getdatewisequantity.getString(19));
            model.setActualenddate(getdatewisequantity.getString(20));
            packingTicketDatewiseQuantityModelArrayList.add(model);

            String actualenddate = getdatewisequantity.getString(20);

            txt_up_act_strat_date.setText(getdatewisequantity.getString(19));
            txt_up_act_end_date.setText(getdatewisequantity.getString(20));
            if (getdatewisequantity.getString(20)!=null)
            {
                txt_up_act_strat_date.setVisibility(View.INVISIBLE);
                txt_up_act_end_date.setVisibility(View.INVISIBLE);
            }


        }
        updatePackTicketRvAdapter=new UpdatePackingTicketAdapter(JKNewViewPackingTicketActivity.this,packingTicketDatewiseQuantityModelArrayList,JKNewViewPackingTicketActivity.this);
        grid_table_details.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL, false));
        grid_table_details.setAdapter(updatePackTicketRvAdapter);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        Intent i=new Intent(JKNewViewPackingTicketActivity.this, JKPackingTicketSelection.class);
        startActivity(i);
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id==R.id.nav_planning)
        {
            Intent i=new Intent(JKNewViewPackingTicketActivity.this, FalcoJKHomeActivity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    @Override
    public void imgactionSubmitIsClicked(StartTicketPackModel model) {

    }

    @Override
    public void imgactintrackingSubmit(PackingTicketDatewiseQuantityModel model) {

    }
}
