package com.brainlines.weightloggernewversion.activity.release;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.adapter.LoggerListAdapter;
import com.brainlines.weightloggernewversion.adapter.releaseadapter.EngageLoggerListAdapter;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.model.LoggersFromDBModel;
import com.brainlines.weightloggernewversion.model.UserLoginModel;
import com.brainlines.weightloggernewversion.utils.AppPreferences;
import com.brainlines.weightloggernewversion.utils.AsyncTasks;
import com.brainlines.weightloggernewversion.utils.Constants;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

@SuppressLint("Registered")
public class ReleaseLoggerListActivity extends AppCompatActivity {
    private static final String TAG = "Release Actvity";
    RecyclerView rv_engage_logger_list;
    DataBaseHelper helper;
    EngageLoggerListAdapter engageLoggerListAdapter;
    ArrayList<LoggersFromDBModel> loggersFromDBModels = new ArrayList<>();
    ArrayList<LoggersFromDBModel> loggerListfromLocalDB = new ArrayList<>();
    private String dbName;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_release_logger_list);
        initUI();
        //insertLoggers();
       ArrayList<LoggersFromDBModel> loggers = null;
        try {
            loggers = getLoggers();
            LinearLayoutManager manager = new LinearLayoutManager(this);
            rv_engage_logger_list.setLayoutManager(manager);
            engageLoggerListAdapter = new EngageLoggerListAdapter(this,loggers);
            rv_engage_logger_list.setAdapter(engageLoggerListAdapter);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "initUI: ");


    }

    private void getloggerData()
    {
//        DataBaseHelper dbHelper = new DataBaseHelper(ReleaseLoggerListActivity.this);
//        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = db.query(Constants.DATA_LOGGER_TABLE, null, null, null, null, null, null);
        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    /*LoggersFromDBModel model = new LoggersFromDBModel();
                    user_email = cursor.getString(cursor.getColumnIndex(Table_Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Table_Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Table_Constants.USER_ROLE_NAME));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Table_Constants.USER_OEMID));*/
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    /*private void insertLoggers()
    {
        try {
            ArrayList<LoggersFromDBModel> logger_list = getLoggers();
            for (int i = 0 ; i < logger_list.size() ; i++){
                LoggersFromDBModel loggersFromDBModel = logger_list.get(i);
                helper.addLoggersToLoggerTable(loggersFromDBModel.getId(),loggersFromDBModel.getOem_id(),loggersFromDBModel.getSerial_num(),loggersFromDBModel.getLogger_name(),loggersFromDBModel.getIp_address(),loggersFromDBModel.getMac_id(),loggersFromDBModel.getLogger_location(),loggersFromDBModel.getCreate_timestamp(),loggersFromDBModel.getCreate_by_id(),loggersFromDBModel.getUpdate_timestamp(),loggersFromDBModel.getModified_date(),loggersFromDBModel.getUpdate_by_id(),loggersFromDBModel.isActive(),loggersFromDBModel.getInuse());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }*/

    private void initUI() {
        dbName = AppPreferences.getDatabseName(this);
//        helper = new DataBaseHelper(this,dbName);
        helper = new DataBaseHelper(this);
        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));
        rv_engage_logger_list = findViewById(R.id.rv_engage_logger_list);

    }


    private ArrayList<LoggersFromDBModel> getLoggers() throws SQLException, ExecutionException, InterruptedException {
        loggersFromDBModels.clear();
        loggerListfromLocalDB = new AsyncTasks.GetLoggerListFromDB(ReleaseLoggerListActivity.this).execute().get();
        return loggersFromDBModels;
    }
}
