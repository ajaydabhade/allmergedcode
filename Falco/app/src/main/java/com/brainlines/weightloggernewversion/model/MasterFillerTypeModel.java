package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MasterFillerTypeModel implements Parcelable {
    String id;
    String oem_id;
    String productTypeId;
    String fillerType;
    String createdDate;
    String modifiedDate;
    String createdBy;
    String modifiedBy;
    boolean isActive;
    boolean isDeleted;

    public MasterFillerTypeModel(String id, String oem_id, String productTypeId, String fillerType, String createdDate, String modifiedDate, String createdBy, String modifiedBy, boolean isActive, boolean isDeleted) {
        this.id = id;
        this.oem_id = oem_id;
        this.productTypeId = productTypeId;
        this.fillerType = fillerType;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.createdBy = createdBy;
        this.modifiedBy = modifiedBy;
        this.isActive = isActive;
        this.isDeleted = isDeleted;
    }

    protected MasterFillerTypeModel(Parcel in) {
        id = in.readString();
        oem_id = in.readString();
        productTypeId = in.readString();
        fillerType = in.readString();
        createdDate = in.readString();
        modifiedDate = in.readString();
        createdBy = in.readString();
        modifiedBy = in.readString();
        isActive = in.readByte() != 0;
        isDeleted = in.readByte() != 0;
    }

    public static final Creator<MasterFillerTypeModel> CREATOR = new Creator<MasterFillerTypeModel>() {
        @Override
        public MasterFillerTypeModel createFromParcel(Parcel in) {
            return new MasterFillerTypeModel(in);
        }

        @Override
        public MasterFillerTypeModel[] newArray(int size) {
            return new MasterFillerTypeModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOem_id() {
        return oem_id;
    }

    public void setOem_id(String oem_id) {
        this.oem_id = oem_id;
    }

    public String getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(String productTypeId) {
        this.productTypeId = productTypeId;
    }

    public String getFillerType() {
        return fillerType;
    }

    public void setFillerType(String fillerType) {
        this.fillerType = fillerType;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(oem_id);
        dest.writeString(productTypeId);
        dest.writeString(fillerType);
        dest.writeString(createdDate);
        dest.writeString(modifiedDate);
        dest.writeString(createdBy);
        dest.writeString(modifiedBy);
        dest.writeByte((byte) (isActive ? 1 : 0));
        dest.writeByte((byte) (isDeleted ? 1 : 0));
    }
}
