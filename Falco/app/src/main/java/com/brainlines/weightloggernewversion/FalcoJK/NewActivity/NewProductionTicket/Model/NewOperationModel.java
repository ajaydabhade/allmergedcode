package com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.Model;

public class NewOperationModel {

        String ptkd_PktId,ptkd_TkNo,ptkd_PSKU,ptkd_Plant,ptkd_ValueStreamCode,ptkd_Ftypecode,ptkd_IsRelease,uni_unit_name;
        String opr_operation_name,val_valuestream_name,ft_ftype_desc,tk_Actual_Start_Date,tk_Actual_End_Date,ptkd_ReleaseDate,tk_Start_Date,tk_End_Date,flag;

        String ptk_OkQty_ans,ptk_ReworkQty_ans,ptk_RejectQty_ans,ptk_WIPQty_ans,ptk_SecondQty_ans;
        String vsCode,createdby,createddate;

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }

    public String getPtk_ChangeRoute_Flag() {
            return ptk_ChangeRoute_Flag;
        }

        public void setPtk_ChangeRoute_Flag(String ptk_ChangeRoute_Flag) {
            this.ptk_ChangeRoute_Flag = ptk_ChangeRoute_Flag;
        }

        String ptk_ChangeRoute_Flag;

        public String getCurrentProdSKUExist() {
            return CurrentProdSKUExist;
        }

        public void setCurrentProdSKUExist(String currentProdSKUExist) {
            CurrentProdSKUExist = currentProdSKUExist;
        }

        String CurrentProdSKUExist;

        String ptk_Id,ptk_TkNo,ptk_Plant,ptk_UpdateDate,ptk_TotalQty;
        int ptk_PtkdId,ptkd_Operation,ptk_OkQty,ptk_ReworkQty,ptk_RejectQty,ptk_WIPQty,ptk_SecondQty,ptkd_Qty,ptk_Operation;
        public String getPtk_Id() {
            return ptk_Id;
        }

        public void setPtk_Id(String ptk_Id) {
            this.ptk_Id = ptk_Id;
        }
        public int getPtk_PtkdId() {
            return ptk_PtkdId;
        }
        public void setPtk_PtkdId(int ptk_PtkdId) {
            this.ptk_PtkdId = ptk_PtkdId;
        }
        public String getPtk_TkNo() {
            return ptk_TkNo;
        }
        public void setPtk_TkNo(String ptk_TkNo) {
            this.ptk_TkNo = ptk_TkNo;
        }
        public String getPtk_Plant() {
            return ptk_Plant;
        }
        public void setPtk_Plant(String ptk_Plant) {
            this.ptk_Plant = ptk_Plant;
        }
        public int getPtk_Operation() {
            return ptk_Operation;
        }
        public void setPtk_Operation(int ptk_Operation) {
            this.ptk_Operation = ptk_Operation;
        }
        public String getPtk_UpdateDate() {
            return ptk_UpdateDate;
        }
        public void setPtk_UpdateDate(String ptk_UpdateDate) {
            this.ptk_UpdateDate = ptk_UpdateDate;
        }
        public int getPtk_OkQty() {
            return ptk_OkQty;
        }
        public void setPtk_OkQty(int ptk_OkQty) {
            this.ptk_OkQty = ptk_OkQty;
        }
        public int getPtk_ReworkQty() {
            return ptk_ReworkQty;
        }
        public void setPtk_ReworkQty(int ptk_ReworkQty) {
            this.ptk_ReworkQty = ptk_ReworkQty;
        }
        public int getPtk_RejectQty() {
            return ptk_RejectQty;
        }
        public void setPtk_RejectQty(int ptk_RejectQty) {
            this.ptk_RejectQty = ptk_RejectQty;
        }
        public int getPtk_WIPQty() {
            return ptk_WIPQty;
        }
        public void setPtk_WIPQty(int ptk_WIPQty) {
            this.ptk_WIPQty = ptk_WIPQty;
        }
        public int getPtk_SecondQty() {
            return ptk_SecondQty;
        }
        public void setPtk_SecondQty(int ptk_SecondQty) {
            this.ptk_SecondQty = ptk_SecondQty;
        }
        public String getPtk_TotalQty() {
            return ptk_TotalQty;
        }

        public void setPtk_TotalQty(String ptk_TotalQty) {
            this.ptk_TotalQty = ptk_TotalQty;
        }

        public String getVsCode() {
            return vsCode;
        }

        public void setVsCode(String vsCode) {
            this.vsCode = vsCode;
        }

        public String getPtk_OkQty_ans() {
            return ptk_OkQty_ans;
        }

        public void setPtk_OkQty_ans(String ptk_OkQty_ans) {
            this.ptk_OkQty_ans = ptk_OkQty_ans;
        }

        public String getPtk_ReworkQty_ans() {
            return ptk_ReworkQty_ans;
        }

        public void setPtk_ReworkQty_ans(String ptk_ReworkQty_ans) {
            this.ptk_ReworkQty_ans = ptk_ReworkQty_ans;
        }

        public String getPtk_RejectQty_ans() {
            return ptk_RejectQty_ans;
        }

        public void setPtk_RejectQty_ans(String ptk_RejectQty_ans) {
            this.ptk_RejectQty_ans = ptk_RejectQty_ans;
        }

        public String getPtk_WIPQty_ans() {
            return ptk_WIPQty_ans;
        }

        public void setPtk_WIPQty_ans(String ptk_WIPQty_ans) {
            this.ptk_WIPQty_ans = ptk_WIPQty_ans;
        }

        public String getPtk_SecondQty_ans() {
            return ptk_SecondQty_ans;
        }

        public void setPtk_SecondQty_ans(String ptk_SecondQty_ans) {
            this.ptk_SecondQty_ans = ptk_SecondQty_ans;
        }

        public String getPtkd_PktId() {
            return ptkd_PktId;
        }

        public void setPtkd_PktId(String ptkd_PktId) {
            this.ptkd_PktId = ptkd_PktId;
        }

        public String getPtkd_TkNo() {
            return ptkd_TkNo;
        }

        public void setPtkd_TkNo(String ptkd_TkNo) {
            this.ptkd_TkNo = ptkd_TkNo;
        }

        public String getPtkd_PSKU() {
            return ptkd_PSKU;
        }

        public void setPtkd_PSKU(String ptkd_PSKU) {
            this.ptkd_PSKU = ptkd_PSKU;
        }

        public String getPtkd_Plant() {
            return ptkd_Plant;
        }

        public void setPtkd_Plant(String ptkd_Plant) {
            this.ptkd_Plant = ptkd_Plant;
        }

        public String getPtkd_ValueStreamCode() {
            return ptkd_ValueStreamCode;
        }

        public void setPtkd_ValueStreamCode(String ptkd_ValueStreamCode) {
            this.ptkd_ValueStreamCode = ptkd_ValueStreamCode;
        }

        public String getPtkd_Ftypecode() {
            return ptkd_Ftypecode;
        }

        public void setPtkd_Ftypecode(String ptkd_Ftypecode) {
            this.ptkd_Ftypecode = ptkd_Ftypecode;
        }

        public int getPtkd_Operation() {
            return ptkd_Operation;
        }

        public void setPtkd_Operation(int ptkd_Operation) {
            this.ptkd_Operation = ptkd_Operation;
        }

        public int getPtkd_Qty() {
            return ptkd_Qty;
        }

        public void setPtkd_Qty(int ptkd_Qty) {
            this.ptkd_Qty = ptkd_Qty;
        }

        public String getPtkd_IsRelease() {
            return ptkd_IsRelease;
        }

        public void setPtkd_IsRelease(String ptkd_IsRelease) {
            this.ptkd_IsRelease = ptkd_IsRelease;
        }

        public String getUni_unit_name() {
            return uni_unit_name;
        }

        public void setUni_unit_name(String uni_unit_name) {
            this.uni_unit_name = uni_unit_name;
        }

        public String getOpr_operation_name() {
            return opr_operation_name;
        }

        public void setOpr_operation_name(String opr_operation_name) {
            this.opr_operation_name = opr_operation_name;
        }

        public String getVal_valuestream_name() {
            return val_valuestream_name;
        }

        public void setVal_valuestream_name(String val_valuestream_name) {
            this.val_valuestream_name = val_valuestream_name;
        }

        public String getFt_ftype_desc() {
            return ft_ftype_desc;
        }

        public void setFt_ftype_desc(String ft_ftype_desc) {
            this.ft_ftype_desc = ft_ftype_desc;
        }

        public String getTk_Actual_Start_Date() {
            return tk_Actual_Start_Date;
        }

        public void setTk_Actual_Start_Date(String tk_Actual_Start_Date) {
            this.tk_Actual_Start_Date = tk_Actual_Start_Date;
        }

        public String getTk_Actual_End_Date() {
            return tk_Actual_End_Date;
        }

        public void setTk_Actual_End_Date(String tk_Actual_End_Date) {
            this.tk_Actual_End_Date = tk_Actual_End_Date;
        }

        public String getPtkd_ReleaseDate() {
            return ptkd_ReleaseDate;
        }

        public void setPtkd_ReleaseDate(String ptkd_ReleaseDate) {
            this.ptkd_ReleaseDate = ptkd_ReleaseDate;
        }

        public String getTk_Start_Date() {
            return tk_Start_Date;
        }

        public void setTk_Start_Date(String tk_Start_Date) {
            this.tk_Start_Date = tk_Start_Date;
        }

        public String getTk_End_Date() {
            return tk_End_Date;
        }

        public void setTk_End_Date(String tk_End_Date) {
            this.tk_End_Date = tk_End_Date;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }
    }


