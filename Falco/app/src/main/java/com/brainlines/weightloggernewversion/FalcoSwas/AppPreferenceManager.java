package com.brainlines.weightloggernewversion.FalcoSwas;

import android.content.Context;
import android.content.SharedPreferences;

public class AppPreferenceManager {
    private static final String FILE_NAME = "falcoglobalapp.shapref";

    private static final String IS_DATA_SAVED = "isSaved";

    public static void setIsDataSaved(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(FILE_NAME,Context.MODE_PRIVATE);
        preferences.edit().putString(IS_DATA_SAVED,value).commit();
    }

    public static String getIsDataSaved(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(FILE_NAME,Context.MODE_PRIVATE);
        return preferences.getString(IS_DATA_SAVED,"");
    }
}
