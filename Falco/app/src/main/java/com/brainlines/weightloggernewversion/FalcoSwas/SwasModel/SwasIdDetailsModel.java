package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

public class SwasIdDetailsModel  {
    String Service_Id,Service_call_status_Id,LOCATIONNAME,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate,TypeOfServiceCallText,NAME,BriefComplaint,TypeOfFaultText,StatusText,FaultyPart,CustomerID,ITEMID,CURRENCY,DocumentPath,TypeOfServiceCallID,CurrentlyAssignedToSE,ServiceEngineerName,ClosedDateTime,ClosedBy,ServiceCallStatusID,Comments,ServiceEngineerID,NumberOfVisits;

    public String getService_Id() {
        return Service_Id;
    }

    public void setService_Id(String service_Id) {
        Service_Id = service_Id;
    }

    public String getService_call_status_Id() {
        return Service_call_status_Id;
    }

    public void setService_call_status_Id(String service_call_status_Id) {
        Service_call_status_Id = service_call_status_Id;
    }

    public String getLOCATIONNAME() {
        return LOCATIONNAME;
    }

    public void setLOCATIONNAME(String LOCATIONNAME) {
        this.LOCATIONNAME = LOCATIONNAME;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getUpdatedBy() {
        return UpdatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        UpdatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return UpdatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        UpdatedDate = updatedDate;
    }

    public String getTypeOfServiceCallText() {
        return TypeOfServiceCallText;
    }

    public void setTypeOfServiceCallText(String typeOfServiceCallText) {
        TypeOfServiceCallText = typeOfServiceCallText;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getBriefComplaint() {
        return BriefComplaint;
    }

    public void setBriefComplaint(String briefComplaint) {
        BriefComplaint = briefComplaint;
    }

    public String getTypeOfFaultText() {
        return TypeOfFaultText;
    }

    public void setTypeOfFaultText(String typeOfFaultText) {
        TypeOfFaultText = typeOfFaultText;
    }

    public String getStatusText() {
        return StatusText;
    }

    public void setStatusText(String statusText) {
        StatusText = statusText;
    }

    public String getFaultyPart() {
        return FaultyPart;
    }

    public void setFaultyPart(String faultyPart) {
        FaultyPart = faultyPart;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getITEMID() {
        return ITEMID;
    }

    public void setITEMID(String ITEMID) {
        this.ITEMID = ITEMID;
    }

    public String getCURRENCY() {
        return CURRENCY;
    }

    public void setCURRENCY(String CURRENCY) {
        this.CURRENCY = CURRENCY;
    }

    public String getDocumentPath() {
        return DocumentPath;
    }

    public void setDocumentPath(String documentPath) {
        DocumentPath = documentPath;
    }

    public String getTypeOfServiceCallID() {
        return TypeOfServiceCallID;
    }

    public void setTypeOfServiceCallID(String typeOfServiceCallID) {
        TypeOfServiceCallID = typeOfServiceCallID;
    }

    public String getCurrentlyAssignedToSE() {
        return CurrentlyAssignedToSE;
    }

    public void setCurrentlyAssignedToSE(String currentlyAssignedToSE) {
        CurrentlyAssignedToSE = currentlyAssignedToSE;
    }

    public String getServiceEngineerName() {
        return ServiceEngineerName;
    }

    public void setServiceEngineerName(String serviceEngineerName) {
        ServiceEngineerName = serviceEngineerName;
    }

    public String getClosedDateTime() {
        return ClosedDateTime;
    }

    public void setClosedDateTime(String closedDateTime) {
        ClosedDateTime = closedDateTime;
    }

    public String getClosedBy() {
        return ClosedBy;
    }

    public void setClosedBy(String closedBy) {
        ClosedBy = closedBy;
    }

    public String getServiceCallStatusID() {
        return ServiceCallStatusID;
    }

    public void setServiceCallStatusID(String serviceCallStatusID) {
        ServiceCallStatusID = serviceCallStatusID;
    }

    public String getComments() {
        return Comments;
    }

    public void setComments(String comments) {
        Comments = comments;
    }

    public String getServiceEngineerID() {
        return ServiceEngineerID;
    }

    public void setServiceEngineerID(String serviceEngineerID) {
        ServiceEngineerID = serviceEngineerID;
    }

    public String getNumberOfVisits() {
        return NumberOfVisits;
    }

    public void setNumberOfVisits(String numberOfVisits) {
        NumberOfVisits = numberOfVisits;
    }
}
