package com.brainlines.weightloggernewversion.Production.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Weight_Report_Model implements Parcelable {
    int ReportID,OEMID;
    String BatchID,TagID;
    String MinWeight,MaxWeight,MeanWeight,Average,Range,SD,UnfilledQuantity,FilledQuantity,UnfilledPouches,FilledPouches,ExcessGiveawayPrice;
    String ExcessGiveawayGm,IsActive,CreatedDate,ModifiedDate;

    public Weight_Report_Model(int reportID, int OEMID, String batchID, String tagID, String minWeight, String maxWeight, String meanWeight, String average, String range, String SD, String unfilledQuantity, String filledQuantity, String unfilledPouches, String filledPouches, String excessGiveawayPrice, String excessGiveawayGm, String isActive, String createdDate, String modifiedDate) {
        ReportID = reportID;
        this.OEMID = OEMID;
        BatchID = batchID;
        TagID = tagID;
        MinWeight = minWeight;
        MaxWeight = maxWeight;
        MeanWeight = meanWeight;
        Average = average;
        Range = range;
        this.SD = SD;
        UnfilledQuantity = unfilledQuantity;
        FilledQuantity = filledQuantity;
        UnfilledPouches = unfilledPouches;
        FilledPouches = filledPouches;
        ExcessGiveawayPrice = excessGiveawayPrice;
        ExcessGiveawayGm = excessGiveawayGm;
        IsActive = isActive;
        CreatedDate = createdDate;
        ModifiedDate = modifiedDate;
    }

    protected Weight_Report_Model(Parcel in) {
        ReportID = in.readInt();
        OEMID = in.readInt();
        BatchID = in.readString();
        TagID = in.readString();
        MinWeight = in.readString();
        MaxWeight = in.readString();
        MeanWeight = in.readString();
        Average = in.readString();
        Range = in.readString();
        SD = in.readString();
        UnfilledQuantity = in.readString();
        FilledQuantity = in.readString();
        UnfilledPouches = in.readString();
        FilledPouches = in.readString();
        ExcessGiveawayPrice = in.readString();
        ExcessGiveawayGm = in.readString();
        IsActive = in.readString();
        CreatedDate = in.readString();
        ModifiedDate = in.readString();
    }

    public static final Creator<Weight_Report_Model> CREATOR = new Creator<Weight_Report_Model>() {
        @Override
        public Weight_Report_Model createFromParcel(Parcel in) {
            return new Weight_Report_Model(in);
        }

        @Override
        public Weight_Report_Model[] newArray(int size) {
            return new Weight_Report_Model[size];
        }
    };

    public int getReportID() {
        return ReportID;
    }

    public void setReportID(int reportID) {
        ReportID = reportID;
    }

    public int getOEMID() {
        return OEMID;
    }

    public void setOEMID(int OEMID) {
        this.OEMID = OEMID;
    }

    public String getBatchID() {
        return BatchID;
    }

    public void setBatchID(String batchID) {
        BatchID = batchID;
    }

    public String getTagID() {
        return TagID;
    }

    public void setTagID(String tagID) {
        TagID = tagID;
    }

    public String getMinWeight() {
        return MinWeight;
    }

    public void setMinWeight(String minWeight) {
        MinWeight = minWeight;
    }

    public String getMaxWeight() {
        return MaxWeight;
    }

    public void setMaxWeight(String maxWeight) {
        MaxWeight = maxWeight;
    }

    public String getMeanWeight() {
        return MeanWeight;
    }

    public void setMeanWeight(String meanWeight) {
        MeanWeight = meanWeight;
    }

    public String getAverage() {
        return Average;
    }

    public void setAverage(String average) {
        Average = average;
    }

    public String getRange() {
        return Range;
    }

    public void setRange(String range) {
        Range = range;
    }

    public String getSD() {
        return SD;
    }

    public void setSD(String SD) {
        this.SD = SD;
    }

    public String getUnfilledQuantity() {
        return UnfilledQuantity;
    }

    public void setUnfilledQuantity(String unfilledQuantity) {
        UnfilledQuantity = unfilledQuantity;
    }

    public String getFilledQuantity() {
        return FilledQuantity;
    }

    public void setFilledQuantity(String filledQuantity) {
        FilledQuantity = filledQuantity;
    }

    public String getUnfilledPouches() {
        return UnfilledPouches;
    }

    public void setUnfilledPouches(String unfilledPouches) {
        UnfilledPouches = unfilledPouches;
    }

    public String getFilledPouches() {
        return FilledPouches;
    }

    public void setFilledPouches(String filledPouches) {
        FilledPouches = filledPouches;
    }

    public String getExcessGiveawayPrice() {
        return ExcessGiveawayPrice;
    }

    public void setExcessGiveawayPrice(String excessGiveawayPrice) {
        ExcessGiveawayPrice = excessGiveawayPrice;
    }

    public String getExcessGiveawayGm() {
        return ExcessGiveawayGm;
    }

    public void setExcessGiveawayGm(String excessGiveawayGm) {
        ExcessGiveawayGm = excessGiveawayGm;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ReportID);
        dest.writeInt(OEMID);
        dest.writeString(BatchID);
        dest.writeString(TagID);
        dest.writeString(MinWeight);
        dest.writeString(MaxWeight);
        dest.writeString(MeanWeight);
        dest.writeString(Average);
        dest.writeString(Range);
        dest.writeString(SD);
        dest.writeString(UnfilledQuantity);
        dest.writeString(FilledQuantity);
        dest.writeString(UnfilledPouches);
        dest.writeString(FilledPouches);
        dest.writeString(ExcessGiveawayPrice);
        dest.writeString(ExcessGiveawayGm);
        dest.writeString(IsActive);
        dest.writeString(CreatedDate);
        dest.writeString(ModifiedDate);
    }
}
