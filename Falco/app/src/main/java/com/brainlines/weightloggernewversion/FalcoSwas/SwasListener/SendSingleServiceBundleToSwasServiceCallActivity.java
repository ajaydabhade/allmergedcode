package com.brainlines.weightloggernewversion.FalcoSwas.SwasListener;

import android.os.Bundle;

public interface SendSingleServiceBundleToSwasServiceCallActivity {
    void sendBundle(Bundle bundle, String callType);
}
