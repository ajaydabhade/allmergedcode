package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

public class SwasServiceCallIdModel {
    String ServiceCallID;
    String CustomerID;
    String LOCATIONNAME;
    String ITEMID;
    String ServiceEngineerName;
    String StatusText;
    String TypeOfServiceCallText;
    String CreatedBy;
    String CreatedDate;
    String UpdatedBy;
    String UpdatedDate;
    String ServiceCallStatusID;

    public String getTypeofServiceCall_id() {
        return TypeofServiceCall_id;
    }

    public void setTypeofServiceCall_id(String typeofServiceCall_id) {
        TypeofServiceCall_id = typeofServiceCall_id;
    }

    String TypeofServiceCall_id;

    public String getServiceCallID() {
        return ServiceCallID;
    }

    public void setServiceCallID(String serviceCallID) {
        ServiceCallID = serviceCallID;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getLOCATIONNAME() {
        return LOCATIONNAME;
    }

    public void setLOCATIONNAME(String LOCATIONNAME) {
        this.LOCATIONNAME = LOCATIONNAME;
    }

    public String getITEMID() {
        return ITEMID;
    }

    public void setITEMID(String ITEMID) {
        this.ITEMID = ITEMID;
    }

    public String getServiceEngineerName() {
        return ServiceEngineerName;
    }

    public void setServiceEngineerName(String serviceEngineerName) {
        ServiceEngineerName = serviceEngineerName;
    }

    public String getStatusText() {
        return StatusText;
    }

    public void setStatusText(String statusText) {
        StatusText = statusText;
    }

    public String getTypeOfServiceCallText() {
        return TypeOfServiceCallText;
    }

    public void setTypeOfServiceCallText(String typeOfServiceCallText) {
        TypeOfServiceCallText = typeOfServiceCallText;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getUpdatedBy() {
        return UpdatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        UpdatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return UpdatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        UpdatedDate = updatedDate;
    }

    public String getServiceCallStatusID() {
        return ServiceCallStatusID;
    }

    public void setServiceCallStatusID(String serviceCallStatusID) {
        ServiceCallStatusID = serviceCallStatusID;
    }
}
