package com.brainlines.weightloggernewversion.Production.activity;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.Production.Model.PWeighReadingsModel;
import com.brainlines.weightloggernewversion.Production.Model.Production_Reason_Table;
import com.brainlines.weightloggernewversion.Production.adapter.PausedBatchListAdapter;
import com.brainlines.weightloggernewversion.Production.adapter.ProductionWeightReadingAdapter;
import com.brainlines.weightloggernewversion.ProductionMenuActivity;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.AppPreferences;
import com.brainlines.weightloggernewversion.utils.Constants;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.eclipse.paho.android.service.MqttAndroidClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class PausedBatchWeightReadingActivity extends AppCompatActivity implements View.OnClickListener {

//    private static final String TAG = "weightREading";
//    DecimalFormat decimalformat = new DecimalFormat("0.000");
//
//    ImageButton btnStop, img_btn_test_complete;
//    RecyclerView rv_weight_reading;
//    MqttAndroidClient client = null;
//    MqttAndroidClient clientObj = null;
//    private boolean isDone;
//    private int DATA_LOGGER_PORT = 8080;
//    public static final String MQTT_BROKER = "tcp://192.168.10.95:1883";
//
//    String current_tag_id;
//    ArrayList<Production_Reason_Table> productionReason_list = new ArrayList<>();
//    ArrayList<PWeighReadingsModel> pWeighReadingsModels = new ArrayList<>();
//    ArrayList<String> productionReasonList = new ArrayList<>();
//    TextView txtTagid;
//    Handler handler = new Handler();
//    Runnable runnable;
//    int delay = 5000;
//    String gmperpouch;
//    double max_reading_value = 0.0;
//    double min_reading_value = 0.0;
//    double mean_of_readings = 0.0;
//    double range = 0.0;
//    double average = 0.0;
//    double standard_deviation = 0.0;
//    double totals_sum = 0.0;
//    DataBaseHelper helper;
//    String Tolal_no_of_pouches,Total_qty,str_shifts;
//    String Status,actualQuantityFilled, Actual_filled_pouches, unfilled_quantity, unfilled_pouches;
//    String str_batch_id, str_positive_tolerance, str_negative_tolerance, str_product, str_selected_date, str_cancel_Reason, str_cancel_remark,strkgvalue;
//    ImageButton img_btn_test_paused;
//    int count = 0;
//    int uclgm = 103;
//    int lclgm = 97;
//    String struom,BatchStatus,dataloggerName,dataloggerMacId;
//    private int readingcount=0;
//
//    ArrayList<String> arrayList1 = new ArrayList<>();
//    DecimalFormat decimalFormat = new DecimalFormat("0.000");
//    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//    SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//    String currentDateandTime = sdf.format(new Date());
//
//    private int index = 0;
//    Double ducl = 0.0;
//    Double dlcl = 0.0;
//    Double dintkgvalue = 0.0;
//    TextView txt_to_be_packed_quantity, txt_total_quantity, txt_report_product, txt_report_pro_type, txt_excess_give_Away, txt_no_of_pouches_of_pouches_packed, txt_to_be_packed_pouches;
//    private String strUnfilledPouches, strUnfilledQty, strFilledQty, strFilledPouches, strTotalexcessgiveaway;
//    private String dbName;
//    private SQLiteDatabase db;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_paused_weight_reading);
//
//        dbName = AppPreferences.getDatabseName(this);
//        helper = new DataBaseHelper(this);
////        helper = new DataBaseHelper(this,dbName);
//        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));
//
//        initUi();
//
//        getWeightReadingData(str_batch_id);
//        getWeightReportData(str_batch_id);
//
//
//        txt_to_be_packed_pouches.setText(strFilledPouches);
//        txt_no_of_pouches_of_pouches_packed.setText(Tolal_no_of_pouches);
//
//        if (struom.equals("Kg"))
//        {
//            Double dfilledqty = Double.parseDouble(strFilledQty) /dintkgvalue;
//            Double dtotalqty = Double.parseDouble(Total_qty)/dintkgvalue;
//            Double dexcess= Double.parseDouble(strTotalexcessgiveaway)/dintkgvalue;
//            txt_to_be_packed_quantity.setText(decimalFormat.format(dfilledqty));
//            txt_total_quantity.setText(decimalFormat.format(dtotalqty));
//            txt_excess_give_Away.setText(decimalFormat.format(dexcess));
//
//        }
//        else
//        {
//            txt_total_quantity.setText(Total_qty);
//            txt_to_be_packed_quantity.setText(strFilledQty);
//            txt_excess_give_Away.setText(strTotalexcessgiveaway);
//
//
//        }
//
//
//       /* arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("93");
//        arrayList1.add("103");
//        arrayList1.add("104");
//        arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("101");*/
//        //abort
//        /*arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("93");
//        arrayList1.add("104");
//        arrayList1.add("104");
//        arrayList1.add("103");
//        arrayList1.add("103");
//        arrayList1.add("103");
//        arrayList1.add("100");
//        arrayList1.add("101");*/
//
//
//        // all ok  // stop  // pause
//        arrayList1.add("100");
//        arrayList1.add("101");
//        arrayList1.add("100");
//        arrayList1.add("101");
//        arrayList1.add("100");
//        arrayList1.add("101");
//        arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("101");
//
//        /*arrayList1.add("0.1");
//        arrayList1.add("0.1");
//        arrayList1.add("0.093");
//        arrayList1.add("0.102");
//        arrayList1.add("0.104");
//        arrayList1.add("0.1");
//        arrayList1.add("0.1");
//        arrayList1.add("0.098");
//        arrayList1.add("0.1");
//        arrayList1.add("0.101");
//        arrayList1.add("0.1");
//        arrayList1.add("0.1");
//        arrayList1.add("0.1");
//        arrayList1.add("0.1");
//        arrayList1.add("0.1");*/
//
//        //insufficient
//      /*  arrayList1.add("100");
//        arrayList1.add("103");
//        arrayList1.add("103");
//        arrayList1.add("103");
//        arrayList1.add("100");
//        arrayList1.add("103");
//        arrayList1.add("103");
//        arrayList1.add("103");
//        arrayList1.add("100");
//        arrayList1.add("101");*/
////        helper = new DataBaseHelper(this);
//
//
//        masterProductionREjectionReson();
//    }
//
//    private void initUi() {
//        btnStop = findViewById(R.id.btnStop);
//        txtTagid = findViewById(R.id.txtTagid);
//        img_btn_test_complete = findViewById(R.id.img_btn_test_complete);
//        img_btn_test_paused = findViewById(R.id.img_btn_test_paused);
//
//        btnStop.setOnClickListener(this);
//        img_btn_test_paused.setOnClickListener(this);
//        img_btn_test_paused.setOnClickListener(this);
//
//        current_tag_id = getIntent().getStringExtra("current_tag_id");
//        Total_qty = getIntent().getStringExtra("total_qty");
//        gmperpouch = getIntent().getStringExtra("gmperpouch");
//        Tolal_no_of_pouches = getIntent().getStringExtra("no_of_pouches");
//        str_shifts = getIntent().getStringExtra("shift");
//        str_batch_id = getIntent().getStringExtra("batch_id");
//        str_positive_tolerance = getIntent().getStringExtra("positive_tolerance");
//        str_negative_tolerance = getIntent().getStringExtra("negative_tolerance");
//        str_selected_date = getIntent().getStringExtra("select_date");
//        str_product = getIntent().getStringExtra("product");
//        String ucl = getIntent().getStringExtra("ducl");
//        String lcl = getIntent().getStringExtra("dlcl");
//        struom = getIntent().getStringExtra("uom");
//        strkgvalue = getIntent().getStringExtra("intkgvalue");
//        dataloggerMacId = getIntent().getStringExtra("dataloggerMacid");
//        dataloggerName = getIntent().getStringExtra("dataloggername");
//
//        if (struom.equals("Kg"))
//        {
//            if (strkgvalue != null && !strkgvalue.equals("null")) {
//                dintkgvalue = Double.parseDouble(strkgvalue);
//
//            }
//        }
//
//        if (struom.equals("gm")) {
//            if (ucl != null) {
//                uclgm = Integer.parseInt(ucl);
//            }
//            if (lcl != null) {
//                lclgm = Integer.parseInt(lcl);
//            }
//        } else {
//            if (ucl != null) {
//                Double uc = Double.parseDouble(ucl);
//                ducl = Double.parseDouble(decimalFormat.format(uc));
//            }
//            if (lcl != null) {
//                Double lc = Double.parseDouble(lcl);
//                dlcl = Double.parseDouble(decimalFormat.format(lc));
//            }
//        }
//
//
//        txtTagid.setText(str_batch_id);
//        rv_weight_reading = (RecyclerView) findViewById(R.id.rv_weight_reading);
//        txt_to_be_packed_quantity = findViewById(R.id.txt_to_be_packed_quantity);
//        txt_total_quantity = findViewById(R.id.txt_total_quantity);
//        txt_report_product = findViewById(R.id.txt_report_product);
//        txt_report_pro_type = findViewById(R.id.txt_report_pro_type);
//        txt_excess_give_Away = findViewById(R.id.txt_excess_give_Away);
//        txt_no_of_pouches_of_pouches_packed = findViewById(R.id.txt_no_of_pouches_of_pouches_packed);
//        txt_to_be_packed_pouches = findViewById(R.id.txt_to_be_packed_pouches);
//
//        txt_no_of_pouches_of_pouches_packed.setText(Tolal_no_of_pouches);
//        Double dtotalqtty = Double.valueOf(Total_qty);
//        txt_total_quantity.setText(decimalFormat.format(dtotalqtty));
//        //txt_total_quantity.setText(Total_qty);
//
//
//    }
//
//    @Override
//    protected void onResume() {
//
//        handler.postDelayed(runnable = new Runnable() {
//            public void run() {
//                handler.postDelayed(runnable, delay);
//
//                if (struom.equals("gm"))
//                {
//                    gmperpouchcalculation();
//                }
//                else
//                {
//                    kgperpouchCalculation();
//                }
//            }
//
//        }, delay);
//
//        super.onResume();
//    }
//
//    private void Calculation()
//    {
//        for (int cnt =1;cnt < pWeighReadingsModels.size(); cnt++) {
//            String status = pWeighReadingsModels.get(cnt).getStatus();
//            if (status.equals("Okay")) {
//                if (Double.parseDouble(pWeighReadingsModels.get(cnt).getWeight()) > max_reading_value) {
//                    max_reading_value = Double.parseDouble(pWeighReadingsModels.get(cnt).getWeight());
//                }
//            }
//        }
//
//        min_reading_value = Double.parseDouble(pWeighReadingsModels.get(0).getWeight());
//        for (int cnt =1; cnt < pWeighReadingsModels.size(); cnt++)
//        {
//            String status = pWeighReadingsModels.get(cnt).getStatus();
//            if (status.equals("Okay")) {
//                if (Double.parseDouble(pWeighReadingsModels.get(cnt).getWeight()) < min_reading_value) {
//                    min_reading_value = Double.parseDouble(pWeighReadingsModels.get(cnt).getWeight());
//                }
//            } }
//        //mean calculation
//        for (int cnt = 0 ; cnt< pWeighReadingsModels.size() ; cnt++)
//        {
//            String status = pWeighReadingsModels.get(cnt).getStatus();
//            if (status.equals("Okay"))
//            {
//                totals_sum += Double.parseDouble(pWeighReadingsModels.get(cnt).getWeight());
//
//            }
//
//        }
//        mean_of_readings = (max_reading_value - min_reading_value)/2 + min_reading_value;
//        //range calculation
//        range = max_reading_value - min_reading_value;
//
//        //average_calculation
//        average = mean_of_readings;
//
//
//        double[] deviations = new double[pWeighReadingsModels.size()];
//        //Taking the deviation of mean from each number
//        for (int i = 0;i < deviations.length; i++)
//        {
//            String status = pWeighReadingsModels.get(i).getStatus();
//            if (status.equals("Okay"))
//            {
//                deviations[i] = Double.parseDouble(pWeighReadingsModels.get(i).getWeight()) - average;
//            }
//        }
//        //getting the squares of deviations
//        double[] squares = new double[pWeighReadingsModels.size()];
//        for (int i = 0;i<pWeighReadingsModels.size();i++)
//        {
//            String status = pWeighReadingsModels.get(i).getStatus();
//            if (status.equals("Okay"))
//            {
//                for (int i1 = 0;i1 < squares.length; i1++)
//                {
//                    squares[i1] = deviations[i1] * deviations[i1];
//                }
//            }
//        }
//        //Adding all the squares
//        double sum_of_squares = 0;
//        for (int i =0;i<squares.length;i++)
//        {
//            sum_of_squares = sum_of_squares + squares[i];
//        }
//        double result = sum_of_squares / (pWeighReadingsModels.size() - 1);
//        standard_deviation = Math.sqrt(result);
//
//        SQLiteDatabase db = helper.getWritableDatabase();
//        String currentDateandTime = dateformat.format(new Date());
//        Double  totalexcessgiveaway = 0.000;
//
//        if (struom.equals("Kg"))
//        {
//            try {
//                for(int i=0;i<pWeighReadingsModels.size();i++)
//                {
//                    String totalexcess = pWeighReadingsModels.get(i).getExcessGiveawayPerPouch();
//                    totalexcessgiveaway += Double.parseDouble(totalexcess);
//                }
//                totalexcessgiveaway = Double.parseDouble(txt_excess_give_Away.getText().toString());
//
//                Double doubleunfilledqty = Double.parseDouble(unfilled_quantity) * dintkgvalue;
//                Double doublefilledqty = Double.parseDouble(actualQuantityFilled) * dintkgvalue;
//                Double dmin = min_reading_value * dintkgvalue;
//                Double dmax = max_reading_value * dintkgvalue;
//                Double dmean = mean_of_readings * dintkgvalue;
//                Double daverage = average*dintkgvalue;
//                Double drange = range*dintkgvalue;
//                Double dsd = standard_deviation*dintkgvalue;
//                Double dtotalexcess = totalexcessgiveaway*dintkgvalue;
//                ContentValues cv=new ContentValues();
//                cv.put("ReportID",1);
//
//                cv.put("OEMID","1234");
//                cv.put("TagID",current_tag_id);
//                cv.put("BatchID",str_batch_id);
//                cv.put("MinWeight",Float.parseFloat(String.valueOf(dmin)));
//                cv.put("MaxWeight",Float.parseFloat(String.valueOf(dmax)));
//                cv.put("MeanWeight",Float.parseFloat(String.valueOf(dmean)));
//                cv.put("Average",Float.parseFloat(String.valueOf(daverage)));
//                cv.put("Range",Float.parseFloat(String.valueOf(drange)));
//                cv.put("SD",Float.parseFloat(String.valueOf(dsd)));
//                cv.put("UnfilledQuantity",Float.parseFloat(String.valueOf(doubleunfilledqty)));
//                cv.put("FilledQuantity", Float.parseFloat(String.valueOf(doublefilledqty)));
//                cv.put("UnfilledPouches",unfilled_pouches);
//                cv.put("FilledPouches",Integer.parseInt(Actual_filled_pouches));
//                cv.put("ExcessGiveawayPrice","");
//                cv.put("ExcessGiveawayGm",decimalformat.format(dtotalexcess));
//                cv.put("Reason",str_cancel_Reason);
//                cv.put("Remark",str_cancel_remark);
//                cv.put("IsActive",1);
//                cv.put("CreatedDate",currentDateandTime);
//                cv.put("ModifiedDate",currentDateandTime);
//                cv.put("IsSync",0);
//
//                long d=db.insert(Constants.PRODUCTION_WEIGHT_REPORT,null,cv);
//                Log.d("Success", String.valueOf(d));
//            }
//            catch (Exception e)
//            {
//                e.printStackTrace();
//            }
//        }
//        else
//        {
//            try {
//                for(int i=0;i<pWeighReadingsModels.size();i++)
//                {
//                    String totalexcess = pWeighReadingsModels.get(i).getExcessGiveawayPerPouch();
//                    totalexcessgiveaway += Double.parseDouble(totalexcess);
//                }
//                totalexcessgiveaway = Double.parseDouble(txt_excess_give_Away.getText().toString());
//
//                Double doubleunfilledqty = Double.valueOf(unfilled_quantity);
//                Double doublefilledqty = Double.valueOf(actualQuantityFilled);
//                ContentValues cv=new ContentValues();
//                cv.put("ReportID",1);
//                cv.put("OEMID",1234);
//                cv.put("TagID",current_tag_id);
//                cv.put("BatchID",str_batch_id);
//                cv.put("MinWeight",Float.parseFloat(String.valueOf(min_reading_value)));
//                cv.put("MaxWeight",Float.parseFloat(String.valueOf(max_reading_value)));
//                cv.put("MeanWeight",Float.parseFloat(String.valueOf(mean_of_readings)));
//                cv.put("Average",Float.parseFloat(String.valueOf(average)));
//                cv.put("Range",Float.parseFloat(String.valueOf(range)));
//                cv.put("SD",Float.parseFloat(String.valueOf(standard_deviation)));
//                cv.put("UnfilledQuantity",Float.parseFloat(String.valueOf(doubleunfilledqty)));
//                cv.put("FilledQuantity", Float.parseFloat(String.valueOf(doublefilledqty)));
//                cv.put("UnfilledPouches",Integer.parseInt(unfilled_pouches));
//                cv.put("FilledPouches",Integer.parseInt(Actual_filled_pouches));
//                cv.put("ExcessGiveawayPrice","");
//                cv.put("ExcessGiveawayGm",Float.parseFloat(String.valueOf(totalexcessgiveaway)));
//                cv.put("Reason",str_cancel_Reason);
//                cv.put("Remark",str_cancel_remark);
//                cv.put("IsActive",1);
//                cv.put("CreatedDate",currentDateandTime);
//                cv.put("ModifiedDate",currentDateandTime);
//                cv.put("IsSync",0);
//
//                long d=db.insert(Constants.PRODUCTION_WEIGHT_REPORT,null,cv);
//                Log.d("Success", String.valueOf(d));
//            }
//            catch (Exception e)
//            {
//                e.printStackTrace();
//            }
//        }
//
//
//
//    }
//
//    private void masterProductionREjectionReson() {
//        Cursor masterRejectionReason = helper.getMasterProductionRejectionReason();
//        while (masterRejectionReason.moveToNext()) {
//            String reasonId = masterRejectionReason.getString(0);
//            String reasonDesc = masterRejectionReason.getString(1);
//            productionReasonList.add(reasonDesc);
//
//            Production_Reason_Table model = new Production_Reason_Table();
//            model.setReasonID(reasonId);
//            model.setReasonDesc(reasonDesc);
//
//            productionReason_list.add(model);
//            Log.d("TAG", "getProductsUsedForTrial: ");
//        }
//    }
//
//    public void InsertIntoWeightReadingDb() {
//        SQLiteDatabase db = helper.getWritableDatabase();
//        String currentDateandTime = dateformat.format(new Date());
//        try {
//            if (struom.equals("Kg"))
//            {
//                for (int i = 0; i < pWeighReadingsModels.size(); i++) {
//
//                    String weight = pWeighReadingsModels.get(i).getWeight();
//                    String status = pWeighReadingsModels.get(i).getStatus();
//                    Double dweight = Double.parseDouble(weight) * dintkgvalue;
//
//
//
//                    Double excess_giveaway = dweight-Double.parseDouble(gmperpouch) * dintkgvalue;
//                    Double dbweight = Double.parseDouble(pWeighReadingsModels.get(i).getWeight()) * dintkgvalue;
//                    Double dcumulativeexcess = Double.parseDouble(pWeighReadingsModels.get(i).getCumilativeExcessGiveaway()) * dintkgvalue;
//                    Double dactualgmperpouch = Double.parseDouble(pWeighReadingsModels.get(i).getActualGmPerPouch()) * dintkgvalue;
//                    Double dtargetgmperpouch = Double.parseDouble(pWeighReadingsModels.get(i).getTargetGmPerPouch()) * dintkgvalue;
//                    Double dcumulativequantity = Double.parseDouble(pWeighReadingsModels.get(i).getCumilativeQuantity()) * dintkgvalue;
//
//                    ContentValues cv = new ContentValues();
//                    cv.put(Constants.P_WEIGHT_READING_ID,i+1);
//                    cv.put(Constants.P_WEIGHT_READING_OEM_ID,1234);
//                    cv.put(Constants.P_WEIGHT_READING_MAC_ID,dataloggerMacId);
//                    cv.put(Constants.P_WEIGHT_READING_LOGGER_ID,dataloggerName);
//                    cv.put(Constants.P_WEIGHT_READING_BATCHID, str_batch_id);
//
//                    cv.put(Constants.P_WEIGHT_READING_WEIGHT_ID, i+1);
//                    cv.put(Constants.P_WEIGHT_READING_PRODUCTION_TAG_NAME, current_tag_id);
//                    cv.put(Constants.P_WEIGHT_READING_SHIFT, str_shifts);
//                    cv.put(Constants.P_WEIGHT_READING_WEIGHT, Float.parseFloat(String.valueOf(dbweight)));
//                    cv.put(Constants.P_WEIGHT_READING_TARGETGMPERPOUCH, Float.parseFloat(String.valueOf(dtargetgmperpouch)));
//                    cv.put(Constants.P_WEIGHT_READING_ACTUALGMPERPOUCH, Float.parseFloat(String.valueOf(dactualgmperpouch)));
//                    if (status.equals("Okay"))
//                    {
//                        cv.put(Constants.P_WEIGHT_READING_EXCESSGIVEAWAYPERPOUCH, Float.parseFloat(String.valueOf(excess_giveaway)));
//                    }
//                    else
//                    {
//                        cv.put(Constants.P_WEIGHT_READING_EXCESSGIVEAWAYPERPOUCH, 0.0);
//                    }
//
//                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVEEXCESSGIVEAWAY,Float.parseFloat(String.valueOf(dcumulativeexcess)));
//                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVEQUANTITY,Float.parseFloat(String.valueOf(dcumulativequantity)));
//                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVE_POUCHES,Integer.parseInt(pWeighReadingsModels.get(i).getCumilativePouches()));
//                    cv.put(Constants.P_WEIGHT_READING_NEGATIVE_GIVEAWAY_FROM_TAG, 0.0);
//                    cv.put(Constants.P_WEIGHT_READING_UCL, 0.0);
//                    cv.put(Constants.P_WEIGHT__READING_LCL,0.0);
//                    cv.put(Constants.P_WEIGHT_READING_STATUS, pWeighReadingsModels.get(i).getStatus());
//                    cv.put(Constants.P_WEIGHT_READING_CREATED_DATE,currentDateandTime);
//                    cv.put(Constants.P_WEIGHT_READING_CREATED_BY,"Pooja");
//                    cv.put(Constants.P_WEIGHT_READING_MODIFIED_BY,"Pooja");
//                    cv.put(Constants.P_WEIGHT_READING_MODIFIED_DATE, currentDateandTime);
//                    cv.put(Constants.P_WEIGHT_READING_IS_SYNC,0);
//
//                    long d = db.insert(Constants.P_WEIGHT_READING_TABLE, null, cv);
//                    Log.d("Success", String.valueOf(d));
//                    if (d == -1){
//                        Toast.makeText(this, "Data not Inserted To Sqlite", Toast.LENGTH_SHORT).show();
//                    }else {
//                        Toast.makeText(this, "Data Inserted To Sqlite", Toast.LENGTH_SHORT).show();
//                        //wakeup();
//                    } }
//            }
//            else
//            {
//                for (int i = 0; i < pWeighReadingsModels.size(); i++) {
//
//                    String weight = pWeighReadingsModels.get(i).getWeight();
//                    String status = pWeighReadingsModels.get(i).getStatus();
//                    Double dweight = Double.parseDouble(weight);
//
//
//
//                    Double excess_giveaway = dweight-Double.parseDouble(gmperpouch);
//                    Double dbweight = Double.parseDouble(pWeighReadingsModels.get(i).getWeight());
//                    Double dcumulativeexcess = Double.parseDouble(pWeighReadingsModels.get(i).getCumilativeExcessGiveaway());
//                    Double dactualgmperpouch = Double.parseDouble(pWeighReadingsModels.get(i).getActualGmPerPouch());
//                    Double dtargetgmperpouch = Double.parseDouble(pWeighReadingsModels.get(i).getTargetGmPerPouch());
//                    Double dcumulativequantity = Double.parseDouble(pWeighReadingsModels.get(i).getCumilativeQuantity());
//
//                    ContentValues cv = new ContentValues();
//                    cv.put(Constants.P_WEIGHT_READING_ID,i+1);
//                    cv.put(Constants.P_WEIGHT_READING_OEM_ID,1234);
//                    cv.put(Constants.P_WEIGHT_READING_MAC_ID,dataloggerMacId);
//                    cv.put(Constants.P_WEIGHT_READING_LOGGER_ID,dataloggerName);
//                    cv.put(Constants.P_WEIGHT_READING_WEIGHT_ID, i+1);
//                    cv.put(Constants.P_WEIGHT_READING_BATCHID, str_batch_id);
//                    cv.put(Constants.P_WEIGHT_READING_PRODUCTION_TAG_NAME, current_tag_id);
//                    cv.put(Constants.P_WEIGHT_READING_SHIFT, str_shifts);
//                    cv.put(Constants.P_WEIGHT_READING_UCL, 0.0);
//                    cv.put(Constants.P_WEIGHT__READING_LCL,0.0);
//                    cv.put(Constants.P_WEIGHT_READING_WEIGHT, decimalformat.format(dbweight));
//                    cv.put(Constants.P_WEIGHT_READING_TARGETGMPERPOUCH, Float.parseFloat(String.valueOf(dtargetgmperpouch)));
//                    cv.put(Constants.P_WEIGHT_READING_ACTUALGMPERPOUCH, Float.parseFloat(String.valueOf(dactualgmperpouch)));
//                    if (status.equals("Okay"))
//                    {
//                        cv.put(Constants.P_WEIGHT_READING_EXCESSGIVEAWAYPERPOUCH, Float.parseFloat(String.valueOf(excess_giveaway)));
//                    }
//                    else
//                    {
//                        cv.put(Constants.P_WEIGHT_READING_EXCESSGIVEAWAYPERPOUCH, 0.0);
//                    }
//                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVEEXCESSGIVEAWAY,Float.parseFloat(String.valueOf(dcumulativeexcess)));
//                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVEQUANTITY,Float.parseFloat(String.valueOf(dcumulativequantity)));
//                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVE_POUCHES,Integer.parseInt(pWeighReadingsModels.get(i).getCumilativePouches()));
//                    cv.put(Constants.P_WEIGHT_READING_NEGATIVE_GIVEAWAY_FROM_TAG, 0.0);
//                    cv.put(Constants.P_WEIGHT_READING_MODIFIED_DATE, currentDateandTime);
//                    cv.put(Constants.P_WEIGHT_READING_CREATED_DATE,currentDateandTime);
//                    cv.put(Constants.P_WEIGHT_READING_STATUS, pWeighReadingsModels.get(i).getStatus());
//                    cv.put(Constants.P_WEIGHT_READING_MODIFIED_BY,"Pooja");
//                    cv.put(Constants.P_WEIGHT_READING_CREATED_BY,"Pooja");
//                    cv.put(Constants.P_WEIGHT_READING_IS_SYNC,0);
//
//                    long d = db.insert(Constants.P_WEIGHT_READING_TABLE, null, cv);
//                    Log.d("Success", String.valueOf(d));
//                    if (d == -1){
//                        Toast.makeText(this, "Data not Inserted To Sqlite", Toast.LENGTH_SHORT).show();
//                    }else {
//                        Toast.makeText(this, "Data Inserted To Sqlite", Toast.LENGTH_SHORT).show();
//                        //wakeup();
//                    } }
//
//            }
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public void onClick(View v) {
//        if (v.getId() == btnStop.getId()) {
//            final ArrayAdapter<String> productionReasonlist = new ArrayAdapter<String>(this, R.layout.item_spinner_latyout, productionReasonList);
//
//            handler.removeCallbacks(runnable);
//
//            ViewGroup viewGroup = findViewById(android.R.id.content);
//            View dialogView = LayoutInflater.from(PausedBatchWeightReadingActivity.this).inflate(R.layout.cancel_readings_layout, viewGroup, false);
//            AlertDialog.Builder builder = new AlertDialog.Builder(PausedBatchWeightReadingActivity.this);
//            builder.setView(dialogView);
//            Button btn_cancel_qty = dialogView.findViewById(R.id.btn_cancel_quantity);
//            Button btn_ok_qty = dialogView.findViewById(R.id.btn_ok_quantity);
//            final EditText ed_cancel_reason = dialogView.findViewById(R.id.ed_reason);
//
//            final Spinner spin_rejection_Reason = dialogView.findViewById(R.id.spin_reason);
//            productionReasonlist.setDropDownViewResource(R.layout.item_spinner_latyout);
//            spin_rejection_Reason.setAdapter(productionReasonlist);
//
//            spin_rejection_Reason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                    str_cancel_Reason = spin_rejection_Reason.getSelectedItem().toString();
//                }
//
//                @Override
//                public void onNothingSelected(AdapterView<?> parent) {
//
//                }
//            });
//
//            final AlertDialog alertDialog = builder.create();
//            alertDialog.show();
//
//            btn_ok_qty.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    alertDialog.dismiss();
//                    InsertIntoWeightReadingDb();
//                    //InsertIntoWeightReportDb();
//                    str_cancel_remark = ed_cancel_reason.getText().toString();
//                    Calculation();
//
////                    DataBaseHelper helper = new DataBaseHelper(PausedBatchWeightReadingActivity.this);
////                    SQLiteDatabase db = helper.getWritableDatabase();
//                    String currentDateandTime = sdf.format(new Date());
//                    ContentValues cv = new ContentValues();
//                    cv.put(Constants.P_TAG_RESULT_OEM_ID, "1234");
//                    cv.put(Constants.P_TAG_RESULT_MAC_ID, "1234");
//                    cv.put(Constants.P_TAG_RESULT_LOGGER_ID, "DiA0008");
//                    cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID, "1");
//                    cv.put(Constants.P_TAG_RESULT_TAG_ID, str_batch_id);
//                    cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT, Total_qty);
//                    cv.put(Constants.P_TAG_RESULT_HEAD, "Right");
//                    cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED, "Abort");
//                    cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE, currentDateandTime);
//                    cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED, "Yes");
//                    cv.put(Constants.P_TAG_RESULT_MODIFIED_BY, "Pooja");
//                    cv.put(Constants.P_TAG_RESULT_IS_SYNC, "0");
//
//
//                    long d = db.insert(Constants.P_TAG_RESULT_TABLE, null, cv);
//                    Log.d("Success", String.valueOf(d));
//                    Intent i = new Intent(PausedBatchWeightReadingActivity.this, BatchReportDetails.class);
//                    i.putExtra("selected_date", str_selected_date);
//                    i.putExtra("product", str_product);
//                    i.putExtra("batch_id", str_batch_id);
//                    startActivity(i);
//
//                }
//            });
//
//            btn_cancel_qty.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    alertDialog.dismiss();
//                    InsertIntoWeightReadingDb();
//                    str_cancel_remark = ed_cancel_reason.getText().toString();
//                    Calculation();
////                    DataBaseHelper helper = new DataBaseHelper(PausedBatchWeightReadingActivity.this);
////                    SQLiteDatabase db = helper.getWritableDatabase();
//                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss");
//                    String currentDateandTime = sdf.format(new Date());
//                    ContentValues cv = new ContentValues();
//                    cv.put(Constants.P_TAG_RESULT_OEM_ID, "1234");
//                    cv.put(Constants.P_TAG_RESULT_MAC_ID, "1234");
//                    cv.put(Constants.P_TAG_RESULT_LOGGER_ID, "DiA0008");
//                    cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID, "1");
//                    cv.put(Constants.P_TAG_RESULT_TAG_ID, str_batch_id);
//                    cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT, Total_qty);
//                    cv.put(Constants.P_TAG_RESULT_HEAD, "Right");
//                    cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED, "Abort");
//                    cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE, currentDateandTime);
//                    cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED, "Yes");
//                    cv.put(Constants.P_TAG_RESULT_MODIFIED_BY, "Pooja");
//                    cv.put(Constants.P_TAG_RESULT_IS_SYNC, "0");
//
//                    long d = db.insert(Constants.P_TAG_RESULT_TABLE, null, cv);
//                    Log.d("Success", String.valueOf(d));
//
//                    Intent i = new Intent(PausedBatchWeightReadingActivity.this, BatchReportDetails.class);
//                    i.putExtra("selected_date", str_selected_date);
//                    i.putExtra("product", str_product);
//                    i.putExtra("batch_id", str_batch_id);
//                    startActivity(i);
//
//                }
//            });
//
//
//        } else if (v.getId() == img_btn_test_paused.getId()) {
//            handler.removeCallbacks(runnable);
//            Toast.makeText(PausedBatchWeightReadingActivity.this, "Test has been paused", Toast.LENGTH_SHORT).show();
//
//            ViewGroup viewGroup = findViewById(android.R.id.content);
//            View dialogView = LayoutInflater.from(PausedBatchWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
//            AlertDialog.Builder builder = new AlertDialog.Builder(PausedBatchWeightReadingActivity.this);
//            builder.setView(dialogView);
//            TextView txt_status = dialogView.findViewById(R.id.txt_status);
//            txt_status.setText("Batch Paused");
//            ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
//            //ImageButton btn_sync = dialogView.findViewById(R.id.btn_sync);
//            //btn_sync.setVisibility(View.INVISIBLE);
//            final AlertDialog alertDialog = builder.create();
//            alertDialog.show();
//
//            btn_test_complete.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    alertDialog.dismiss();
//                    BatchStatus ="Paused";
//                    helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
//                    InsertIntoWeightReadingDb();
////                    DataBaseHelper helper = new DataBaseHelper(PausedBatchWeightReadingActivity.this);
////                    SQLiteDatabase db = helper.getWritableDatabase();
//                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss");
//                    String currentDateandTime = sdf.format(new Date());
//                    ContentValues cv = new ContentValues();
//                    cv.put(Constants.P_TAG_RESULT_OEM_ID, "1234");
//                    cv.put(Constants.P_TAG_RESULT_MAC_ID, "1234");
//                    cv.put(Constants.P_TAG_RESULT_LOGGER_ID, "DiA0008");
//                    cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID, "1");
//                    cv.put(Constants.P_TAG_RESULT_TAG_ID, str_batch_id);
//                    cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT, Total_qty);
//                    cv.put(Constants.P_TAG_RESULT_HEAD, "Right");
//                    cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED, "Paused");
//                    cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE, currentDateandTime);
//                    cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED, "Yes");
//                    cv.put(Constants.P_TAG_RESULT_MODIFIED_BY, "Pooja");
//                    cv.put(Constants.P_TAG_RESULT_IS_SYNC, "0");
//
//                    long d = db.insert(Constants.P_TAG_RESULT_TABLE, null, cv);
//                    Log.d("Success", String.valueOf(d));
//
//                    Intent i = new Intent(PausedBatchWeightReadingActivity.this, ProductionMenuActivity.class);
//                    i.putExtra("selected_date", str_selected_date);
//                    i.putExtra("product", str_product);
//                    i.putExtra("batch_id", str_batch_id);
//                    startActivity(i);
//                }
//            });
//        }
//
//    }
//    private void getWeightReadingData(String Batchid) {
//        Cursor reading = helper.getProductionweightReading(Batchid);
//        Double totalexcessgiveaway = 0.0;
//        while (reading.moveToNext()) {
//            try {
//                String pweightId = reading.getString(4);
//                String batchid = reading.getString(5);
//                String weight = reading.getString(7);
//                String shift = reading.getString(8);
//                String targetgmperpouch = reading.getString(9);
//                String actualgmperpouch = reading.getString(10);
//                String excess = reading.getString(11);
//                String cumulativeexcessgiveaway = reading.getString(12);
//                String cumulativeqty = reading.getString(13);
//                String status = reading.getString(18);
//                String time = reading.getString(19);
//                Double intexcess = Double.parseDouble(excess);
//
//                totalexcessgiveaway += intexcess;
//
//                strTotalexcessgiveaway = String.valueOf(totalexcessgiveaway);
//
//                if(struom.equals("Kg"))
//                {
//                    readingcount++;
//                    double dweightReading = Double.parseDouble(weight)/dintkgvalue;
//                    double dexcessgiveaway = Double.parseDouble(excess)/dintkgvalue;
//                    PWeighReadingsModel model = new PWeighReadingsModel();
//                    model.setWeight(String.valueOf(dweightReading));
//                    model.setStatus(status);
//                    model.setExcessGiveawayPerPouch(String.valueOf(dexcessgiveaway));
//                    model.setCreatedDate(time);
//                    model.setPWeightReadingID(pweightId);
//                    model.setBatchId(batchid);
//                    model.setCumilativeQuantity(cumulativeqty);
//                    model.setActualGmPerPouch(actualgmperpouch);
//                    model.setCumilativeExcessGiveaway(cumulativeexcessgiveaway);
//                    model.setPWeightReadingID(pweightId);
//                    model.setShift(shift);
//                    model.setTargetGmPerPouch(targetgmperpouch);
//                    pWeighReadingsModels.add(model);
//                    Log.d("TAG", "baglengthReadingList: ");
//                }
//                else
//
//                {
//                    readingcount++;
//                    PWeighReadingsModel model = new PWeighReadingsModel();
//                    model.setWeight(weight);
//                    model.setStatus(status);
//                    model.setExcessGiveawayPerPouch(excess);
//                    model.setCreatedDate(time);
//                    model.setPWeightReadingID(pweightId);
//                    model.setBatchId(batchid);
//                    model.setCumilativeQuantity(cumulativeqty);
//                    model.setActualGmPerPouch(actualgmperpouch);
//                    model.setCumilativeExcessGiveaway(cumulativeexcessgiveaway);
//                    model.setPWeightReadingID(pweightId);
//                    model.setShift(shift);
//                    model.setTargetGmPerPouch(targetgmperpouch);
//                    pWeighReadingsModels.add(model);
//                    Log.d("TAG", "baglengthReadingList: ");
//
//                }
//
//
//
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PausedBatchWeightReadingActivity.this);
//            rv_weight_reading.setLayoutManager(linearLayoutManager);
//            rv_weight_reading.setAdapter(new PausedBatchListAdapter(PausedBatchWeightReadingActivity.this, pWeighReadingsModels));
//
//        }
//
//
//    }
//
//    private void getWeightReportData(String Batchid) {
//        Cursor report = helper.getProductionBatchWeightReport(Batchid);
//        while (report.moveToNext()) {
//            try {
//
//                String max = report.getString(4);
//                String average = report.getString(5);
//                String range = report.getString(6);
//                String sd = report.getString(7);
//                String unfilled_qty = report.getString(8);
//                String filled_qty = report.getString(9);
//                String unfilled_pouches = report.getString(10);
//                String filled_pouches = report.getString(11);
//                String excess_give_away_gm = report.getString(13);
//                String excess_give_away_price = report.getString(12);
//                String min = report.getString(14);
//                String mean = report.getString(15);
//                String reason = report.getString(16);
//                String remark = report.getString(17);
//
//                strUnfilledPouches = unfilled_pouches;
//                strUnfilledQty = unfilled_qty;
//                strFilledQty = filled_qty;
//                strFilledPouches = filled_pouches;
//
//                Log.d("TAG", "p_tag: ");
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        }
//    }
//
//    public void gmperpouchcalculation() {
//        Double sumoftotalweights = 0.0;
//        Double packing_total_qty = 0.0;
//        int tobepackednoofpouches = 0;
//        Double to_be_packed_quantity = 0.0;
//        Double previous_cumu_qty = 0.0;
//        Double totalexcessgiveaway = 0.0;
//        Double excessgiveaway = 0.0;
//        int cumulativep_pouches = 0;
//        Double remaining_pouch_qty = 0.0;
//        Double cumulativeexcessfiveaway = 0.0;
//
//        try {
//            if (pWeighReadingsModels.size() != 0) {
//                for (int i = 0; i < pWeighReadingsModels.size(); i++) {
//                    int pouches_count = pWeighReadingsModels.size();
//
//                    String status = pWeighReadingsModels.get(i).getStatus();
//
//                    if (status.equals("Okay")) {
//                        String value = pWeighReadingsModels.get(i).getWeight();
//                        sumoftotalweights += Double.parseDouble(value);
//
//                        to_be_packed_quantity += Double.parseDouble(pWeighReadingsModels.get(i).getWeight());
//                        tobepackednoofpouches = pouches_count++;
//                        excessgiveaway = Double.parseDouble(value) - Double.parseDouble(gmperpouch);
//
//                        String strprevious_cumu_qty = pWeighReadingsModels.get(i).getCumilativeQuantity();
//                        previous_cumu_qty = Double.parseDouble(strprevious_cumu_qty);
//                        totalexcessgiveaway += excessgiveaway;
//
//                        String strtotalexcessgiveaway = String.valueOf(totalexcessgiveaway);
//                        Double dbtotalexcessgiveaway = Double.valueOf(strtotalexcessgiveaway);
//
//                        String strexcessgiveaway = String.valueOf(excessgiveaway);
//                        Double dbexcess = Double.valueOf(strexcessgiveaway);
//
//                        txt_excess_give_Away.setText(decimalFormat.format(dbtotalexcessgiveaway));
//
//                        if (dbexcess > Double.parseDouble(gmperpouch)) {
//                            txt_excess_give_Away.setText(decimalFormat.format(dbexcess));
//                        }
//
//                        if (i != 0) {
//                            int pos = i - 1;
//                            String previous_cum = pWeighReadingsModels.get(pos).getCumilativeExcessGiveaway();
//                            cumulativeexcessfiveaway = excessgiveaway + Double.parseDouble(previous_cum);
//                        }
//                    }
//                }
//                packing_total_qty = Double.parseDouble(Total_qty) - 1;
//
//                String strtobepackedqty = String.valueOf(to_be_packed_quantity);
//                Double dtoobepackedqty = Double.valueOf(strtobepackedqty);
//
//                txt_to_be_packed_quantity.setText(decimalFormat.format(dtoobepackedqty));
//                txt_to_be_packed_pouches.setText(tobepackednoofpouches + "");
//
//                remaining_pouch_qty = Double.parseDouble(Total_qty) - sumoftotalweights;
//
//                //remaining_pouch_qty <=lclgm
//                //Integer.parseInt(gmperpouch)
//
//                for (int i = 0; i < pWeighReadingsModels.size(); i++) {
//                    String status = pWeighReadingsModels.get(i).getStatus();
//                    if (status.equals("Ignored")) {
//                        count++;
//                        if (count == 3) {
//                            handler.removeCallbacks(runnable);
//
//                            ViewGroup viewGroup = findViewById(android.R.id.content);
//                            View dialogView = LayoutInflater.from(PausedBatchWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
//                            AlertDialog.Builder builder = new AlertDialog.Builder(PausedBatchWeightReadingActivity.this);
//                            builder.setView(dialogView);
//                            ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
//                            //ImageButton btn_sync = dialogView.findViewById(R.id.btn_sync);
//                            TextView txt_status = dialogView.findViewById(R.id.txt_status);
//                            final AlertDialog alertDialog = builder.create();
//                            alertDialog.show();
//                            txt_status.setText("Batch Aborted");
//                            unfilled_quantity = String.valueOf(remaining_pouch_qty);
//                            actualQuantityFilled = String.valueOf(sumoftotalweights);
//                            Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
//                            String a = txt_to_be_packed_pouches.getText().toString();
//                            int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
//                            unfilled_pouches = String.valueOf(strunfilled_pouches);
//
//                            handler.removeCallbacks(runnable);
//
//                            btn_test_complete.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    alertDialog.dismiss();
//                                    Status = "Abort";
//                                    BatchStatus ="Abort";
//                                    helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
//                                    InsertIntoWeightReadingDb();
//                                    Calculation();
//
//                                    SQLiteDatabase db = helper.getWritableDatabase();
//                                    String currentDateandTime = dateformat.format(new Date());
//                                    ContentValues cv=new ContentValues();
//
//                                    Double dtotalquantity = Double.parseDouble(Total_qty);
//
//                                    cv.put(Constants.P_TAG_RESULT_ID,1);
//                                    cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
//                                    cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
//                                    cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
//                                    cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
//                                    cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
//                                    cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
//                                    cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
//                                    cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Abort");
//                                    cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
//                                    cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
//                                    cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
//                                    cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);
//
//
//
//
//                                    long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
//                                    Log.d("Success", String.valueOf(d));
//
//                                    Intent i = new Intent(PausedBatchWeightReadingActivity.this, BatchReportDetails.class);
//                                    i.putExtra("selected_date", str_selected_date);
//                                    i.putExtra("product", str_product);
//                                    i.putExtra("batch_id", str_batch_id);
//                                    startActivity(i);
//
//                                }
//                            });
//                        }
//                    } else {
//                        count--;
//                    }
//                }
//
//                if (sumoftotalweights >= packing_total_qty) {
//                    btnStop.setVisibility(View.GONE);
//                    handler.removeCallbacks(runnable);
//                    unfilled_quantity = String.valueOf(remaining_pouch_qty);
//                    actualQuantityFilled = String.valueOf(sumoftotalweights);
//                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
//                    String a = txt_to_be_packed_pouches.getText().toString();
//                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
//                    unfilled_pouches = String.valueOf(strunfilled_pouches);
//
//                    ViewGroup viewGroup = findViewById(android.R.id.content);
//                    View dialogView = LayoutInflater.from(PausedBatchWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
//                    AlertDialog.Builder builder = new AlertDialog.Builder(PausedBatchWeightReadingActivity.this);
//                    builder.setView(dialogView);
//                    ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
//                    TextView txt_status = dialogView.findViewById(R.id.txt_status);
//                    txt_status.setText("Batch Completed");
//
//
//                    final AlertDialog alertDialog = builder.create();
//                    alertDialog.show();
//
//                    btn_test_complete.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            alertDialog.dismiss();
//                            BatchStatus ="Complete";
//                            helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
//                            InsertIntoWeightReadingDb();
//
//                            Calculation();
//
////                            DataBaseHelper helper = new DataBaseHelper(PausedBatchWeightReadingActivity.this);
////                            SQLiteDatabase db = helper.getWritableDatabase();
//                            String currentDateandTime = sdf.format(new Date());
//                            ContentValues cv = new ContentValues();
//                            Double dtotalquantity = Double.parseDouble(Total_qty);
//
//                            cv.put(Constants.P_TAG_RESULT_ID,1);
//                            cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
//                            cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
//                            cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
//                            cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
//                            cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
//                            cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
//                            cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
//                            cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Complete");
//                            cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
//                            cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
//                            cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
//                            cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);
//
//
//                            long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
//                            Log.d("Success", String.valueOf(d));
//
//
//                            Intent i = new Intent(PausedBatchWeightReadingActivity.this, BatchReportDetails.class);
//                            i.putExtra("selected_date", str_selected_date);
//                            i.putExtra("product", str_product);
//                            i.putExtra("batch_id", str_batch_id);
//                            startActivity(i);
//
//                        }
//                    });
//
//                } else if (remaining_pouch_qty < lclgm) {
//                    btnStop.setVisibility(View.GONE);
//                    handler.removeCallbacks(runnable);
//                    unfilled_quantity = String.valueOf(remaining_pouch_qty);
//                    actualQuantityFilled = String.valueOf(sumoftotalweights);
//                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
//                    String a = txt_to_be_packed_pouches.getText().toString();
//                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
//                    unfilled_pouches = String.valueOf(strunfilled_pouches);
//
//                    ViewGroup viewGroup = findViewById(android.R.id.content);
//                    View dialogView = LayoutInflater.from(PausedBatchWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
//                    AlertDialog.Builder builder = new AlertDialog.Builder(PausedBatchWeightReadingActivity.this);
//                    builder.setView(dialogView);
//                    ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
//                    //ImageButton btn_sync = dialogView.findViewById(R.id.btn_sync);
//                    TextView txt_status = dialogView.findViewById(R.id.txt_status);
//                    final AlertDialog alertDialog = builder.create();
//                    alertDialog.show();
//                    txt_status.setText("Insufficient remaining quantity");
//
//                    btn_test_complete.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            alertDialog.dismiss();
//                            Status = "Abort";
//                            BatchStatus ="Paused";
//                            helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
//                            InsertIntoWeightReadingDb();
//                            Calculation();
//
////                            DataBaseHelper helper = new DataBaseHelper(PausedBatchWeightReadingActivity.this);
////                            SQLiteDatabase db = helper.getWritableDatabase();
//                            ContentValues cv=new ContentValues();
//                            Double dtotalquantity = 0.0;
//
//                            if(struom.equals("gm"))
//                            {
//                                dtotalquantity = Double.parseDouble(Total_qty);
//
//                            }
//                            else
//                            {
//                                dtotalquantity = Double.parseDouble(Total_qty) * dintkgvalue;
//
//                            }
//
//                            cv.put(Constants.P_TAG_RESULT_ID,1);
//                            cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
//                            cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
//                            cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
//                            cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
//                            cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
//                            cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
//                            cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
//                            cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Paused");
//                            cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
//                            cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
//                            cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
//                            cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);
//
//                            long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
//                            Log.d("Success", String.valueOf(d));
//
//                            Intent i = new Intent(PausedBatchWeightReadingActivity.this, BatchReportDetails.class);
//                            i.putExtra("selected_date", str_selected_date);
//                            i.putExtra("product", str_product);
//                            i.putExtra("batch_id", str_batch_id);
//                            startActivity(i);
//
//
//                        }
//                    });
//
//                } else {
//                    PWeighReadingsModel model = new PWeighReadingsModel();
//                    readingcount++;
//                    int size = pWeighReadingsModels.size();
//                    model.setBatchId(str_batch_id);
//                    model.setWeight(arrayList1.get(index));
//                    model.setShift(str_shifts);
//                    model.setModifiedDate(str_selected_date);
//                    model.setActualGmPerPouch(arrayList1.get(index));
//                    model.setTargetGmPerPouch(gmperpouch);
//                    //model.setPWeightReadingID(String.valueOf(index+1));
//                    model.setPWeightReadingID(String.valueOf(readingcount));
//                    model.setCumilativeExcessGiveaway(String.valueOf(cumulativeexcessfiveaway));
//                    String weight = arrayList1.get(index);
//                    Double excess_giveaway = Double.parseDouble(weight) - Double.parseDouble(gmperpouch);
//                    model.setExcessGiveawayPerPouch(String.valueOf(excess_giveaway));
//                    model.setCreatedDate(currentDateandTime);
//
//                    String arralistvalue = arrayList1.get(index);
//                    Double dvalue = Double.parseDouble(arralistvalue);
//
//                    Double cum_quantity_field = dvalue + previous_cumu_qty;
//                    model.setCumilativeQuantity(String.valueOf(cum_quantity_field));
//                            /*String pouches = txt_to_be_packed_pouches.getText().toString();
//                            model.setCumilativePouches(txt_to_be_packed_pouches.getText().toString());*/
//
//                    if (Integer.parseInt(arrayList1.get(index)) > uclgm || Integer.parseInt(arrayList1.get(index)) < lclgm) {
//                        model.setStatus("Ignored");
//                    } else {
//                        model.setStatus("Okay");
//                    }
//                    pWeighReadingsModels.add(model);
//
//                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PausedBatchWeightReadingActivity.this);
//                    rv_weight_reading.setLayoutManager(linearLayoutManager);
//                    rv_weight_reading.setAdapter(new PausedBatchListAdapter(PausedBatchWeightReadingActivity.this, pWeighReadingsModels));
//
//                    index++;
//
//                }
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    public void kgperpouchCalculation()
//    {
//        Double sumoftotalweights = 0.0;
//        Double packing_total_qty = 0.0;
//        int sumoftotalpackedpouches = 0;
//        Double sumoftotalpackedquantity =0.0;
//        Double previous_cumu_qty = 0.0;
//        Double totalexcessgiveaway = 0.0;
//        Double excessgiveawayperpouch = 0.0;
//        Double cumulativeexcessfiveaway = 0.0;
//        Double dgmperpouch = Double.parseDouble(gmperpouch)/Double.parseDouble(strkgvalue);
//
//        try {
//            if (pWeighReadingsModels.size()!=0)
//            {
//                int pouches_count = 1;
//
//                for(int i = 0 ; i < pWeighReadingsModels.size(); i++) {
//                    String status = pWeighReadingsModels.get(i).getStatus();
//
//                    if (status.equals("Okay")) {
//                        String weight =pWeighReadingsModels.get(i).getWeight();
//                        sumoftotalweights += Double.parseDouble(weight);
//
//                        sumoftotalpackedquantity += Double.parseDouble(pWeighReadingsModels.get(i).getWeight());
//                        sumoftotalpackedpouches = pouches_count++;
//                        excessgiveawayperpouch = Double.parseDouble(weight) - dgmperpouch;
//
//                        String strprevious_cumu_qty = pWeighReadingsModels.get(i).getCumilativeQuantity();
//                        previous_cumu_qty = Double.parseDouble(strprevious_cumu_qty);
//                        totalexcessgiveaway += excessgiveawayperpouch;
//
//                        String strtotalexcessgiveaway = String.valueOf(totalexcessgiveaway);
//                        Double dbtotalexcessgiveaway = Double.valueOf(strtotalexcessgiveaway);
//
//                        String strexcessgiveaway = String.valueOf(excessgiveawayperpouch);
//                        Double dbexcess = Double.valueOf(strexcessgiveaway);
//
//                        txt_excess_give_Away.setText(decimalFormat.format(dbtotalexcessgiveaway));
//
//                        if (excessgiveawayperpouch > Double.parseDouble(gmperpouch)) {
//                            txt_excess_give_Away.setText(decimalFormat.format(dbexcess));
//                        }
//
//                        if (i!=0) {
//                            int pos = i-1;
//                            String previous_cum= pWeighReadingsModels.get(pos).getCumilativeExcessGiveaway();
//                            cumulativeexcessfiveaway = excessgiveawayperpouch + Double.parseDouble(previous_cum);
//
//                        }
//                    }
//                }
//                Double dvalue = 1.0/dintkgvalue;
//                Double dtotalqty = Double.parseDouble(Total_qty)/dintkgvalue;
//                packing_total_qty = dtotalqty - dvalue;
//
//                String strtobepackedqty = String.valueOf(sumoftotalpackedquantity);
//                Double dtoobepackedqty = Double.valueOf(strtobepackedqty);
//
//                txt_to_be_packed_quantity.setText(decimalFormat.format(dtoobepackedqty));
//                txt_to_be_packed_pouches.setText(sumoftotalpackedpouches + "");
//
//                //this quantity used for checking the remaining quantity for packing
//                Double dqty = dtotalqty * dintkgvalue;
//                Double dsumoftotalweight = sumoftotalweights*dintkgvalue;
//                Double dremaingqty = dqty - dsumoftotalweight;
//                Double dlc = dlcl * dintkgvalue;
//                //remaining_pouch_qty = Double.parseDouble(Total_qty)- sumoftotalweights;
//
//                for (int i =0 ;i<pWeighReadingsModels.size();i++)
//                {
//                    String status = pWeighReadingsModels.get(i).getStatus();
//                    if (status.equals("Ignored"))
//                    {
//                        count++;
//                        if (count==3) {
//                            handler.removeCallbacks(runnable);
//                            ViewGroup viewGroup = findViewById(android.R.id.content);
//                            View dialogView = LayoutInflater.from(PausedBatchWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
//                            AlertDialog.Builder builder = new AlertDialog.Builder(PausedBatchWeightReadingActivity.this);
//                            builder.setView(dialogView);
//                            ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
//                            TextView txt_status = dialogView.findViewById(R.id.txt_status);
//                            final AlertDialog alertDialog = builder.create();
//                            alertDialog.show();
//                            txt_status.setText("Batch Aborted");
//                            unfilled_quantity = String.valueOf(dremaingqty);
//                            actualQuantityFilled = String.valueOf(sumoftotalweights);
//                            Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
//                            String a =  txt_to_be_packed_pouches.getText().toString();
//                            int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
//                            unfilled_pouches = String.valueOf(strunfilled_pouches);
//
//                            handler.removeCallbacks(runnable);
//
//                            btn_test_complete.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    alertDialog.dismiss();
//                                    Status ="Abort";
//                                    BatchStatus ="Abort";
//                                    helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
//
//                                    InsertIntoWeightReadingDb();
//                                    Calculation();
//                                    SQLiteDatabase db = helper.getWritableDatabase();
//                                    String currentDateandTime = dateformat.format(new Date());
//                                    ContentValues cv=new ContentValues();
//                                    Double dtotalquantity = Double.parseDouble(Total_qty) * dintkgvalue;
//
//                                    cv.put(Constants.P_TAG_RESULT_ID,1);
//                                    cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
//                                    cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
//                                    cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
//                                    cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
//                                    cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
//                                    cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
//                                    cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
//                                    cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Abort");
//                                    cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
//                                    cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
//                                    cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
//                                    cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);
//
//
//
//                                    long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
//                                    Log.d("Success", String.valueOf(d));
//
//                                    Intent i = new Intent(PausedBatchWeightReadingActivity.this, BatchReportDetails.class);
//                                    i.putExtra("selected_date",str_selected_date);
//                                    i.putExtra("product",str_product);
//                                    i.putExtra("batch_id",str_batch_id);
//                                    startActivity(i);
//
//                                }
//                            });
//                        }
//                    }
//                    else {
//                        count--;
//                    }
//                }
//
//                if (sumoftotalweights>=packing_total_qty)
//                {
//                    btnStop.setVisibility(View.GONE);
//                    handler.removeCallbacks(runnable);
//                    unfilled_quantity = String.valueOf(dremaingqty);
//                    actualQuantityFilled = String.valueOf(sumoftotalweights);
//                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
//                    String a =  txt_to_be_packed_pouches.getText().toString();
//                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
//                    unfilled_pouches = String.valueOf(strunfilled_pouches);
//
//                    ViewGroup viewGroup = findViewById(android.R.id.content);
//                    View dialogView = LayoutInflater.from(PausedBatchWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
//                    AlertDialog.Builder builder = new AlertDialog.Builder(PausedBatchWeightReadingActivity.this);
//                    builder.setView(dialogView);
//                    ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
//                    TextView txt_status = dialogView.findViewById(R.id.txt_status);
//                    txt_status.setText("Batch Completed");
//
//
//                    final AlertDialog alertDialog = builder.create();
//                    alertDialog.show();
//
//                    btn_test_complete.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            alertDialog.dismiss();
//                            BatchStatus ="Complete";
//                            helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
//                            InsertIntoWeightReadingDb();
//
//                            Calculation();
//                            SQLiteDatabase db = helper.getWritableDatabase();
//                            String currentDateandTime = dateformat.format(new Date());
//                            ContentValues cv=new ContentValues();
//                            cv.put(Constants.P_TAG_RESULT_ID,1);
//                            cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
//                            cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
//                            cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
//                            cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
//                            cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
//                            cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(Total_qty)));
//                            cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
//                            cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Complete");
//                            cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
//                            cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
//                            cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
//                            cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);
//
//
//
//                            Intent i = new Intent(PausedBatchWeightReadingActivity.this, BatchReportDetails.class);
//                            i.putExtra("selected_date",str_selected_date);
//                            i.putExtra("product",str_product);
//                            i.putExtra("batch_id",str_batch_id);
//                            startActivity(i);
//
//                        }
//                    });
//
//                }
//                else if (dremaingqty < dlc)
//                {
//                    btnStop.setVisibility(View.GONE);
//                    handler.removeCallbacks(runnable);
//                    unfilled_quantity = String.valueOf(dremaingqty);
//                    actualQuantityFilled = String.valueOf(sumoftotalweights);
//                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
//                    String a =  txt_to_be_packed_pouches.getText().toString();
//                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
//                    unfilled_pouches = String.valueOf(strunfilled_pouches);
//
//                    ViewGroup viewGroup = findViewById(android.R.id.content);
//                    View dialogView = LayoutInflater.from(PausedBatchWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
//                    AlertDialog.Builder builder = new AlertDialog.Builder(PausedBatchWeightReadingActivity.this);
//                    builder.setView(dialogView);
//                    ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
//                    TextView txt_status = dialogView.findViewById(R.id.txt_status);
//                    final AlertDialog alertDialog = builder.create();
//                    alertDialog.show();
//                    txt_status.setText("Insufficient remaining quantity");
//
//                    btn_test_complete.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            alertDialog.dismiss();
//                            Status = "Abort";
//                            BatchStatus ="Abort";
//                            helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
//                            InsertIntoWeightReadingDb();
//                            Calculation();
//                            SQLiteDatabase db = helper.getWritableDatabase();
//                            String currentDateandTime = dateformat.format(new Date());
//                            ContentValues cv=new ContentValues();
//                            Double dtotalquantity = Double.parseDouble(Total_qty) * dintkgvalue;
//
//                            cv.put(Constants.P_TAG_RESULT_ID,1);
//                            cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
//                            cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
//                            cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
//                            cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
//                            cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
//                            cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
//                            cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
//                            cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Abort");
//                            cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
//                            cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
//                            cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
//                            cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);
//                            Intent i = new Intent(PausedBatchWeightReadingActivity.this, BatchReportDetails.class);
//                            i.putExtra("selected_date",str_selected_date);
//                            i.putExtra("product",str_product);
//                            i.putExtra("batch_id",str_batch_id);
//                            startActivity(i);
//
//
//                        }
//                    });
//
//                }
//                else {
//                    PWeighReadingsModel model = new PWeighReadingsModel();
//                    String currentDateandTime = dateformat.format(new Date());
//                    readingcount++;
//                    model.setBatchId(str_batch_id);
//                    model.setWeight(arrayList1.get(index));
//                    model.setShift(str_shifts);
//                    model.setModifiedDate(str_selected_date);
//                    model.setActualGmPerPouch(arrayList1.get(index));
//                    model.setTargetGmPerPouch(String.valueOf(dgmperpouch));
//                    model.setPWeightReadingID(String.valueOf(readingcount));
//                    model.setCumilativeExcessGiveaway(String.valueOf(cumulativeexcessfiveaway));
//                    String weight = arrayList1.get(index);
//                    Double excess_giveaway = Double.parseDouble(weight)-dgmperpouch;
//                    model.setExcessGiveawayPerPouch(String.valueOf(decimalFormat.format(excess_giveaway)));
//                    model.setCreatedDate(currentDateandTime);
//
//                    Double cum_quantity_field = Double.parseDouble(arrayList1.get(index)) + previous_cumu_qty;
//                    model.setCumilativeQuantity(String.valueOf(cum_quantity_field));
//                    String pouches = txt_to_be_packed_pouches.getText().toString();
//                    model.setCumilativePouches(txt_to_be_packed_pouches.getText().toString());
//
//                    if (Double.parseDouble(arrayList1.get(index))>ducl || Double.parseDouble(arrayList1.get(index))<dlcl) {
//                        model.setStatus("Ignored");
//                    }
//                    else
//                    {
//                        model.setStatus("Okay");
//                    }
//                    pWeighReadingsModels.add(model);
//                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PausedBatchWeightReadingActivity.this);
//                    rv_weight_reading.setLayoutManager(linearLayoutManager);
//                    rv_weight_reading.setAdapter(new PausedBatchListAdapter(PausedBatchWeightReadingActivity.this, pWeighReadingsModels));
//
//
//                    index ++;
//                }
//            }
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//
//        }
//
//
//    }

    private static final String TAG = "weightREading";
    DecimalFormat decimalformat = new DecimalFormat("0.000");

    ImageButton btnStop, img_btn_test_complete;
    RecyclerView rv_weight_reading;
    MqttAndroidClient client = null;
    MqttAndroidClient clientObj = null;
    private boolean isDone;
    private int DATA_LOGGER_PORT = 8080;
    public static final String MQTT_BROKER = "tcp://192.168.10.95:1883";

    String current_tag_id;
    ArrayList<Production_Reason_Table> productionReason_list = new ArrayList<>();
    ArrayList<PWeighReadingsModel> pWeighReadingsModels = new ArrayList<>();
    ArrayList<String> productionReasonList = new ArrayList<>();
    TextView txtTagid;
    Handler handler = new Handler();
    Runnable runnable;
    int delay = 5000;
    String gmperpouch;
    double max_reading_value = 0.0;
    double min_reading_value = 0.0;
    double mean_of_readings = 0.0;
    double range = 0.0;
    double average = 0.0;
    double standard_deviation = 0.0;
    double totals_sum = 0.0;
    DataBaseHelper helper;
    String Tolal_no_of_pouches,Total_qty,str_shifts;
    String Status,actualQuantityFilled, Actual_filled_pouches, unfilled_quantity, unfilled_pouches;
    String str_batch_id, str_positive_tolerance, str_negative_tolerance, str_product, str_selected_date, str_cancel_Reason, str_cancel_remark,strkgvalue;
    ImageButton img_btn_test_paused;
    int count = 0;
    int uclgm = 103;
    int lclgm = 97;
    String struom,BatchStatus,dataloggerName,dataloggerMacId;
    private int readingcount=0;

    ArrayList<String> arrayList1 = new ArrayList<>();
    DecimalFormat decimalFormat = new DecimalFormat("0.000");
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String currentDateandTime = sdf.format(new Date());

    private int index = 0;
    Double ducl = 0.0;
    Double dlcl = 0.0;
    Double dintkgvalue = 0.0;
    TextView txt_to_be_packed_quantity, txt_total_quantity, txt_report_product, txt_report_pro_type, txt_excess_give_Away, txt_no_of_pouches_of_pouches_packed, txt_to_be_packed_pouches;
    private String strUnfilledPouches, strUnfilledQty, strFilledQty, strFilledPouches, strTotalexcessgiveaway;
    private String dbName;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paused_weight_reading);

        dbName = AppPreferences.getDatabseName(this);
        helper = new DataBaseHelper(this);
        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));

        initUi();

        getWeightReadingData(str_batch_id);
        getWeightReportData(str_batch_id);


        txt_to_be_packed_pouches.setText(strFilledPouches);
        txt_no_of_pouches_of_pouches_packed.setText(Tolal_no_of_pouches);

        if (struom.equals("Kg"))
        {
            Double dfilledqty = Double.parseDouble(strFilledQty) /dintkgvalue;
            Double dtotalqty = Double.parseDouble(Total_qty)/dintkgvalue;
            Double dexcess= Double.parseDouble(strTotalexcessgiveaway)/dintkgvalue;
            txt_to_be_packed_quantity.setText(decimalFormat.format(dfilledqty));
            txt_total_quantity.setText(decimalFormat.format(dtotalqty));
            txt_excess_give_Away.setText(decimalFormat.format(dexcess));

        }
        else
        {
            txt_total_quantity.setText(Total_qty);
            txt_to_be_packed_quantity.setText(strFilledQty);
            txt_excess_give_Away.setText(strTotalexcessgiveaway);


        }
        masterProductionREjectionReson();
    }

    private void initUi() {
        btnStop = findViewById(R.id.btnStop);
        txtTagid = findViewById(R.id.txtTagid);
        img_btn_test_complete = findViewById(R.id.img_btn_test_complete);
        img_btn_test_paused = findViewById(R.id.img_btn_test_paused);

        btnStop.setOnClickListener(this);
        img_btn_test_paused.setOnClickListener(this);
        img_btn_test_paused.setOnClickListener(this);

        current_tag_id = getIntent().getStringExtra("current_tag_id");
        Total_qty = getIntent().getStringExtra("total_qty");
        gmperpouch = getIntent().getStringExtra("gmperpouch");
        Tolal_no_of_pouches = getIntent().getStringExtra("no_of_pouches");
        str_shifts = getIntent().getStringExtra("shift");
        str_batch_id = getIntent().getStringExtra("batch_id");
        str_positive_tolerance = getIntent().getStringExtra("positive_tolerance");
        str_negative_tolerance = getIntent().getStringExtra("negative_tolerance");
        str_selected_date = getIntent().getStringExtra("select_date");
        str_product = getIntent().getStringExtra("product");
        String ucl = getIntent().getStringExtra("ducl");
        String lcl = getIntent().getStringExtra("dlcl");
        struom = getIntent().getStringExtra("uom");
        strkgvalue = getIntent().getStringExtra("intkgvalue");
        dataloggerMacId = getIntent().getStringExtra("dataloggerMacid");
        dataloggerName = getIntent().getStringExtra("dataloggername");

        if (struom.equals("Kg"))
        {
            if (strkgvalue != null && !strkgvalue.equals("null")) {
                dintkgvalue = Double.parseDouble(strkgvalue);

            }
        }

        if (struom.equals("gm")) {
            if (ucl != null) {
                uclgm = Integer.parseInt(ucl);
            }
            if (lcl != null) {
                lclgm = Integer.parseInt(lcl);
            }
        } else {
            if (ucl != null) {
                Double uc = Double.parseDouble(ucl);
                ducl = Double.parseDouble(decimalFormat.format(uc));
            }
            if (lcl != null) {
                Double lc = Double.parseDouble(lcl);
                dlcl = Double.parseDouble(decimalFormat.format(lc));
            }
        }


        txtTagid.setText(str_batch_id);
        rv_weight_reading = (RecyclerView) findViewById(R.id.rv_weight_reading);
        txt_to_be_packed_quantity = findViewById(R.id.txt_to_be_packed_quantity);
        txt_total_quantity = findViewById(R.id.txt_total_quantity);
        txt_report_product = findViewById(R.id.txt_report_product);
        txt_report_pro_type = findViewById(R.id.txt_report_pro_type);
        txt_excess_give_Away = findViewById(R.id.txt_excess_give_Away);
        txt_no_of_pouches_of_pouches_packed = findViewById(R.id.txt_no_of_pouches_of_pouches_packed);
        txt_to_be_packed_pouches = findViewById(R.id.txt_to_be_packed_pouches);

        txt_no_of_pouches_of_pouches_packed.setText(Tolal_no_of_pouches);
        Double dtotalqtty = Double.valueOf(Total_qty);
        txt_total_quantity.setText(decimalFormat.format(dtotalqtty));
        //txt_total_quantity.setText(Total_qty);


    }

    @Override
    protected void onResume() {

        handler.postDelayed(runnable = new Runnable() {
            public void run() {
                handler.postDelayed(runnable, delay);

                if (struom.equals("gm"))
                {
                    gmperpouchcalculation();
                }
                else
                {
                    kgperpouchCalculation();
                }
            }

        }, delay);

        super.onResume();
    }

    private void Calculation()
    {
        for (int cnt =1;cnt < pWeighReadingsModels.size(); cnt++) {
            String status = pWeighReadingsModels.get(cnt).getStatus();
            if (status.equals("Okay")) {
                if (Double.parseDouble(pWeighReadingsModels.get(cnt).getWeight()) > max_reading_value) {
                    max_reading_value = Double.parseDouble(pWeighReadingsModels.get(cnt).getWeight());
                }
            }
        }

        min_reading_value = Double.parseDouble(pWeighReadingsModels.get(0).getWeight());
        for (int cnt =1; cnt < pWeighReadingsModels.size(); cnt++)
        {
            String status = pWeighReadingsModels.get(cnt).getStatus();
            if (status.equals("Okay")) {
                if (Double.parseDouble(pWeighReadingsModels.get(cnt).getWeight()) < min_reading_value) {
                    min_reading_value = Double.parseDouble(pWeighReadingsModels.get(cnt).getWeight());
                }
            } }
        //mean calculation
        for (int cnt = 0 ; cnt< pWeighReadingsModels.size() ; cnt++)
        {
            String status = pWeighReadingsModels.get(cnt).getStatus();
            if (status.equals("Okay"))
            {
                totals_sum += Double.parseDouble(pWeighReadingsModels.get(cnt).getWeight());

            }

        }
        mean_of_readings = (max_reading_value - min_reading_value)/2 + min_reading_value;
        //range calculation
        range = max_reading_value - min_reading_value;

        //average_calculation
        average = mean_of_readings;


        double[] deviations = new double[pWeighReadingsModels.size()];
        //Taking the deviation of mean from each number
        for (int i = 0;i < deviations.length; i++)
        {
            String status = pWeighReadingsModels.get(i).getStatus();
            if (status.equals("Okay"))
            {
                deviations[i] = Double.parseDouble(pWeighReadingsModels.get(i).getWeight()) - average;
            }
        }
        //getting the squares of deviations
        double[] squares = new double[pWeighReadingsModels.size()];
        for (int i = 0;i<pWeighReadingsModels.size();i++)
        {
            String status = pWeighReadingsModels.get(i).getStatus();
            if (status.equals("Okay"))
            {
                for (int i1 = 0;i1 < squares.length; i1++)
                {
                    squares[i1] = deviations[i1] * deviations[i1];
                }
            }
        }
        //Adding all the squares
        double sum_of_squares = 0;
        for (int i =0;i<squares.length;i++)
        {
            sum_of_squares = sum_of_squares + squares[i];
        }
        double result = sum_of_squares / (pWeighReadingsModels.size() - 1);
        standard_deviation = Math.sqrt(result);

        SQLiteDatabase db = helper.getWritableDatabase();
        String currentDateandTime = dateformat.format(new Date());
        Double  totalexcessgiveaway = 0.000;

        if (struom.equals("Kg"))
        {
            try {
                for(int i=0;i<pWeighReadingsModels.size();i++)
                {
                    String totalexcess = pWeighReadingsModels.get(i).getExcessGiveawayPerPouch();
                    totalexcessgiveaway += Double.parseDouble(totalexcess);
                }
                totalexcessgiveaway = Double.parseDouble(txt_excess_give_Away.getText().toString());

                Double doubleunfilledqty = Double.parseDouble(unfilled_quantity) * dintkgvalue;
                Double doublefilledqty = Double.parseDouble(actualQuantityFilled) * dintkgvalue;
                Double dmin = min_reading_value * dintkgvalue;
                Double dmax = max_reading_value * dintkgvalue;
                Double dmean = mean_of_readings * dintkgvalue;
                Double daverage = average*dintkgvalue;
                Double drange = range*dintkgvalue;
                Double dsd = standard_deviation*dintkgvalue;
                Double dtotalexcess = totalexcessgiveaway*dintkgvalue;
                ContentValues cv=new ContentValues();
                cv.put("ReportID",1);

                cv.put("OEMID","1234");
                cv.put("TagID",current_tag_id);
                cv.put("BatchID",str_batch_id);
                cv.put("MinWeight",Float.parseFloat(String.valueOf(dmin)));
                cv.put("MaxWeight",Float.parseFloat(String.valueOf(dmax)));
                cv.put("MeanWeight",Float.parseFloat(String.valueOf(dmean)));
                cv.put("Average",Float.parseFloat(String.valueOf(daverage)));
                cv.put("Range",Float.parseFloat(String.valueOf(drange)));
                cv.put("SD",Float.parseFloat(String.valueOf(dsd)));
                cv.put("UnfilledQuantity",Float.parseFloat(String.valueOf(doubleunfilledqty)));
                cv.put("FilledQuantity", Float.parseFloat(String.valueOf(doublefilledqty)));
                cv.put("UnfilledPouches",unfilled_pouches);
                cv.put("FilledPouches",Integer.parseInt(Actual_filled_pouches));
                cv.put("ExcessGiveawayPrice","");
                cv.put("ExcessGiveawayGm",decimalformat.format(dtotalexcess));
                cv.put("Reason",str_cancel_Reason);
                cv.put("Remark",str_cancel_remark);
                cv.put("IsActive",1);
                cv.put("CreatedDate",currentDateandTime);
                cv.put("ModifiedDate",currentDateandTime);
                cv.put("IsSync",0);

                long d=db.insert(Constants.PRODUCTION_WEIGHT_REPORT,null,cv);
                Log.d("Success", String.valueOf(d));
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            try {
                for(int i=0;i<pWeighReadingsModels.size();i++)
                {
                    String totalexcess = pWeighReadingsModels.get(i).getExcessGiveawayPerPouch();
                    totalexcessgiveaway += Double.parseDouble(totalexcess);
                }
                totalexcessgiveaway = Double.parseDouble(txt_excess_give_Away.getText().toString());

                Double doubleunfilledqty = Double.valueOf(unfilled_quantity);
                Double doublefilledqty = Double.valueOf(actualQuantityFilled);
                ContentValues cv=new ContentValues();
                cv.put("ReportID",1);
                cv.put("OEMID",1234);
                cv.put("TagID",current_tag_id);
                cv.put("BatchID",str_batch_id);
                cv.put("MinWeight",Float.parseFloat(String.valueOf(min_reading_value)));
                cv.put("MaxWeight",Float.parseFloat(String.valueOf(max_reading_value)));
                cv.put("MeanWeight",Float.parseFloat(String.valueOf(mean_of_readings)));
                cv.put("Average",Float.parseFloat(String.valueOf(average)));
                cv.put("Range",Float.parseFloat(String.valueOf(range)));
                cv.put("SD",Float.parseFloat(String.valueOf(standard_deviation)));
                cv.put("UnfilledQuantity",Float.parseFloat(String.valueOf(doubleunfilledqty)));
                cv.put("FilledQuantity", Float.parseFloat(String.valueOf(doublefilledqty)));
                cv.put("UnfilledPouches",Integer.parseInt(unfilled_pouches));
                cv.put("FilledPouches",Integer.parseInt(Actual_filled_pouches));
                cv.put("ExcessGiveawayPrice","");
                cv.put("ExcessGiveawayGm",Float.parseFloat(String.valueOf(totalexcessgiveaway)));
                cv.put("Reason",str_cancel_Reason);
                cv.put("Remark",str_cancel_remark);
                cv.put("IsActive",1);
                cv.put("CreatedDate",currentDateandTime);
                cv.put("ModifiedDate",currentDateandTime);
                cv.put("IsSync",0);

                long d=db.insert(Constants.PRODUCTION_WEIGHT_REPORT,null,cv);
                Log.d("Success", String.valueOf(d));
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }



    }

    private void masterProductionREjectionReson() {
        Cursor masterRejectionReason = helper.getMasterProductionRejectionReason();
        while (masterRejectionReason.moveToNext()) {
            String reasonId = masterRejectionReason.getString(0);
            String reasonDesc = masterRejectionReason.getString(1);
            productionReasonList.add(reasonDesc);

            Production_Reason_Table model = new Production_Reason_Table();
            model.setReasonID(reasonId);
            model.setReasonDesc(reasonDesc);

            productionReason_list.add(model);
            Log.d("TAG", "getProductsUsedForTrial: ");
        }
    }

    public void InsertIntoWeightReadingDb() {
        SQLiteDatabase db = helper.getWritableDatabase();
        String currentDateandTime = dateformat.format(new Date());
        try {
            if (struom.equals("Kg"))
            {
                for (int i = 0; i < pWeighReadingsModels.size(); i++) {

                    String weight = pWeighReadingsModels.get(i).getWeight();
                    String status = pWeighReadingsModels.get(i).getStatus();
                    Double dweight = Double.parseDouble(weight) * dintkgvalue;



                    Double excess_giveaway = dweight-Double.parseDouble(gmperpouch) * dintkgvalue;
                    Double dbweight = Double.parseDouble(pWeighReadingsModels.get(i).getWeight()) * dintkgvalue;
                    Double dcumulativeexcess = Double.parseDouble(pWeighReadingsModels.get(i).getCumilativeExcessGiveaway()) * dintkgvalue;
                    Double dactualgmperpouch = Double.parseDouble(pWeighReadingsModels.get(i).getActualGmPerPouch()) * dintkgvalue;
                    Double dtargetgmperpouch = Double.parseDouble(pWeighReadingsModels.get(i).getTargetGmPerPouch()) * dintkgvalue;
                    Double dcumulativequantity = Double.parseDouble(pWeighReadingsModels.get(i).getCumilativeQuantity()) * dintkgvalue;

                    ContentValues cv = new ContentValues();
                    cv.put(Constants.P_WEIGHT_READING_ID,i+1);
                    cv.put(Constants.P_WEIGHT_READING_OEM_ID,1234);
                    cv.put(Constants.P_WEIGHT_READING_MAC_ID,dataloggerMacId);
                    cv.put(Constants.P_WEIGHT_READING_LOGGER_ID,dataloggerName);
                    cv.put(Constants.P_WEIGHT_READING_BATCHID, str_batch_id);

                    cv.put(Constants.P_WEIGHT_READING_WEIGHT_ID, i+1);
                    cv.put(Constants.P_WEIGHT_READING_PRODUCTION_TAG_NAME, current_tag_id);
                    cv.put(Constants.P_WEIGHT_READING_SHIFT, str_shifts);
                    cv.put(Constants.P_WEIGHT_READING_WEIGHT, Float.parseFloat(String.valueOf(dbweight)));
                    cv.put(Constants.P_WEIGHT_READING_TARGETGMPERPOUCH, Float.parseFloat(String.valueOf(dtargetgmperpouch)));
                    cv.put(Constants.P_WEIGHT_READING_ACTUALGMPERPOUCH, Float.parseFloat(String.valueOf(dactualgmperpouch)));
                    if (status.equals("Okay"))
                    {
                        cv.put(Constants.P_WEIGHT_READING_EXCESSGIVEAWAYPERPOUCH, Float.parseFloat(String.valueOf(excess_giveaway)));
                    }
                    else
                    {
                        cv.put(Constants.P_WEIGHT_READING_EXCESSGIVEAWAYPERPOUCH, 0.0);
                    }

                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVEEXCESSGIVEAWAY,Float.parseFloat(String.valueOf(dcumulativeexcess)));
                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVEQUANTITY,Float.parseFloat(String.valueOf(dcumulativequantity)));
                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVE_POUCHES,Integer.parseInt(pWeighReadingsModels.get(i).getCumilativePouches()));
                    cv.put(Constants.P_WEIGHT_READING_NEGATIVE_GIVEAWAY_FROM_TAG, 0.0);
                    cv.put(Constants.P_WEIGHT_READING_UCL, 0.0);
                    cv.put(Constants.P_WEIGHT__READING_LCL,0.0);
                    cv.put(Constants.P_WEIGHT_READING_STATUS, pWeighReadingsModels.get(i).getStatus());
                    cv.put(Constants.P_WEIGHT_READING_CREATED_DATE,currentDateandTime);
                    cv.put(Constants.P_WEIGHT_READING_CREATED_BY,"Pooja");
                    cv.put(Constants.P_WEIGHT_READING_MODIFIED_BY,"Pooja");
                    cv.put(Constants.P_WEIGHT_READING_MODIFIED_DATE, currentDateandTime);
                    cv.put(Constants.P_WEIGHT_READING_IS_SYNC,0);

                    long d = db.insert(Constants.P_WEIGHT_READING_TABLE, null, cv);
                    Log.d("Success", String.valueOf(d));
                    if (d == -1){
                        Toast.makeText(this, "Data not Inserted To Sqlite", Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(this, "Data Inserted To Sqlite", Toast.LENGTH_SHORT).show();
                        //wakeup();
                    } }
            }
            else
            {
                for (int i = 0; i < pWeighReadingsModels.size(); i++) {

                    String weight = pWeighReadingsModels.get(i).getWeight();
                    String status = pWeighReadingsModels.get(i).getStatus();
                    Double dweight = Double.parseDouble(weight);



                    Double excess_giveaway = dweight-Double.parseDouble(gmperpouch);
                    Double dbweight = Double.parseDouble(pWeighReadingsModels.get(i).getWeight());
                    Double dcumulativeexcess = Double.parseDouble(pWeighReadingsModels.get(i).getCumilativeExcessGiveaway());
                    Double dactualgmperpouch = Double.parseDouble(pWeighReadingsModels.get(i).getActualGmPerPouch());
                    Double dtargetgmperpouch = Double.parseDouble(pWeighReadingsModels.get(i).getTargetGmPerPouch());
                    Double dcumulativequantity = Double.parseDouble(pWeighReadingsModels.get(i).getCumilativeQuantity());

                    ContentValues cv = new ContentValues();
                    cv.put(Constants.P_WEIGHT_READING_ID,i+1);
                    cv.put(Constants.P_WEIGHT_READING_OEM_ID,1234);
                    cv.put(Constants.P_WEIGHT_READING_MAC_ID,dataloggerMacId);
                    cv.put(Constants.P_WEIGHT_READING_LOGGER_ID,dataloggerName);
                    cv.put(Constants.P_WEIGHT_READING_WEIGHT_ID, i+1);
                    cv.put(Constants.P_WEIGHT_READING_BATCHID, str_batch_id);
                    cv.put(Constants.P_WEIGHT_READING_PRODUCTION_TAG_NAME, current_tag_id);
                    cv.put(Constants.P_WEIGHT_READING_SHIFT, str_shifts);
                    cv.put(Constants.P_WEIGHT_READING_UCL, 0.0);
                    cv.put(Constants.P_WEIGHT__READING_LCL,0.0);
                    cv.put(Constants.P_WEIGHT_READING_WEIGHT, decimalformat.format(dbweight));
                    cv.put(Constants.P_WEIGHT_READING_TARGETGMPERPOUCH, Float.parseFloat(String.valueOf(dtargetgmperpouch)));
                    cv.put(Constants.P_WEIGHT_READING_ACTUALGMPERPOUCH, Float.parseFloat(String.valueOf(dactualgmperpouch)));
                    if (status.equals("Okay"))
                    {
                        cv.put(Constants.P_WEIGHT_READING_EXCESSGIVEAWAYPERPOUCH, Float.parseFloat(String.valueOf(excess_giveaway)));
                    }
                    else
                    {
                        cv.put(Constants.P_WEIGHT_READING_EXCESSGIVEAWAYPERPOUCH, 0.0);
                    }
                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVEEXCESSGIVEAWAY,Float.parseFloat(String.valueOf(dcumulativeexcess)));
                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVEQUANTITY,Float.parseFloat(String.valueOf(dcumulativequantity)));
                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVE_POUCHES,Integer.parseInt(pWeighReadingsModels.get(i).getCumilativePouches()));
                    cv.put(Constants.P_WEIGHT_READING_NEGATIVE_GIVEAWAY_FROM_TAG, 0.0);
                    cv.put(Constants.P_WEIGHT_READING_MODIFIED_DATE, currentDateandTime);
                    cv.put(Constants.P_WEIGHT_READING_CREATED_DATE,currentDateandTime);
                    cv.put(Constants.P_WEIGHT_READING_STATUS, pWeighReadingsModels.get(i).getStatus());
                    cv.put(Constants.P_WEIGHT_READING_MODIFIED_BY,"Pooja");
                    cv.put(Constants.P_WEIGHT_READING_CREATED_BY,"Pooja");
                    cv.put(Constants.P_WEIGHT_READING_IS_SYNC,0);

                    long d = db.insert(Constants.P_WEIGHT_READING_TABLE, null, cv);
                    Log.d("Success", String.valueOf(d));
                    if (d == -1){
                        Toast.makeText(this, "Data not Inserted To Sqlite", Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(this, "Data Inserted To Sqlite", Toast.LENGTH_SHORT).show();
                        //wakeup();
                    } }

            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == btnStop.getId()) {
            final ArrayAdapter<String> productionReasonlist = new ArrayAdapter<String>(this, R.layout.item_spinner_latyout, productionReasonList);

            handler.removeCallbacks(runnable);

            ViewGroup viewGroup = findViewById(android.R.id.content);
            View dialogView = LayoutInflater.from(PausedBatchWeightReadingActivity.this).inflate(R.layout.cancel_readings_layout, viewGroup, false);
            AlertDialog.Builder builder = new AlertDialog.Builder(PausedBatchWeightReadingActivity.this);
            builder.setView(dialogView);
            Button btn_cancel_qty = dialogView.findViewById(R.id.btn_cancel_quantity);
            Button btn_ok_qty = dialogView.findViewById(R.id.btn_ok_quantity);
            final EditText ed_cancel_reason = dialogView.findViewById(R.id.ed_reason);

            final Spinner spin_rejection_Reason = dialogView.findViewById(R.id.spin_reason);
            productionReasonlist.setDropDownViewResource(R.layout.item_spinner_latyout);
            spin_rejection_Reason.setAdapter(productionReasonlist);

            spin_rejection_Reason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    str_cancel_Reason = spin_rejection_Reason.getSelectedItem().toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            final AlertDialog alertDialog = builder.create();
            alertDialog.show();

            btn_ok_qty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    InsertIntoWeightReadingDb();
                    //InsertIntoWeightReportDb();
                    str_cancel_remark = ed_cancel_reason.getText().toString();
                    Calculation();

//                    DataBaseHelper helper = new DataBaseHelper(PausedBatchWeightReadingActivity.this);
//                    SQLiteDatabase db = helper.getWritableDatabase();
                    String currentDateandTime = sdf.format(new Date());
                    ContentValues cv = new ContentValues();
                    cv.put(Constants.P_TAG_RESULT_OEM_ID, "1234");
                    cv.put(Constants.P_TAG_RESULT_MAC_ID, "1234");
                    cv.put(Constants.P_TAG_RESULT_LOGGER_ID, "DiA0008");
                    cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID, "1");
                    cv.put(Constants.P_TAG_RESULT_TAG_ID, str_batch_id);
                    cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT, Total_qty);
                    cv.put(Constants.P_TAG_RESULT_HEAD, "Right");
                    cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED, "Abort");
                    cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE, currentDateandTime);
                    cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED, "Yes");
                    cv.put(Constants.P_TAG_RESULT_MODIFIED_BY, "Pooja");
                    cv.put(Constants.P_TAG_RESULT_IS_SYNC, "0");


                    long d = db.insert(Constants.P_TAG_RESULT_TABLE, null, cv);
                    Log.d("Success", String.valueOf(d));
                    Intent i = new Intent(PausedBatchWeightReadingActivity.this, BatchReportDetails.class);
                    i.putExtra("selected_date", str_selected_date);
                    i.putExtra("product", str_product);
                    i.putExtra("batch_id", str_batch_id);
                    startActivity(i);

                }
            });

            btn_cancel_qty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    InsertIntoWeightReadingDb();
                    str_cancel_remark = ed_cancel_reason.getText().toString();
                    Calculation();
//                    DataBaseHelper helper = new DataBaseHelper(PausedBatchWeightReadingActivity.this);
//                    SQLiteDatabase db = helper.getWritableDatabase();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss");
                    String currentDateandTime = sdf.format(new Date());
                    ContentValues cv = new ContentValues();
                    cv.put(Constants.P_TAG_RESULT_OEM_ID, "1234");
                    cv.put(Constants.P_TAG_RESULT_MAC_ID, "1234");
                    cv.put(Constants.P_TAG_RESULT_LOGGER_ID, "DiA0008");
                    cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID, "1");
                    cv.put(Constants.P_TAG_RESULT_TAG_ID, str_batch_id);
                    cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT, Total_qty);
                    cv.put(Constants.P_TAG_RESULT_HEAD, "Right");
                    cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED, "Abort");
                    cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE, currentDateandTime);
                    cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED, "Yes");
                    cv.put(Constants.P_TAG_RESULT_MODIFIED_BY, "Pooja");
                    cv.put(Constants.P_TAG_RESULT_IS_SYNC, "0");

                    long d = db.insert(Constants.P_TAG_RESULT_TABLE, null, cv);
                    Log.d("Success", String.valueOf(d));

                    Intent i = new Intent(PausedBatchWeightReadingActivity.this, BatchReportDetails.class);
                    i.putExtra("selected_date", str_selected_date);
                    i.putExtra("product", str_product);
                    i.putExtra("batch_id", str_batch_id);
                    startActivity(i);

                }
            });


        } else if (v.getId() == img_btn_test_paused.getId()) {
            handler.removeCallbacks(runnable);
            Toast.makeText(PausedBatchWeightReadingActivity.this, "Test has been paused", Toast.LENGTH_SHORT).show();

            ViewGroup viewGroup = findViewById(android.R.id.content);
            View dialogView = LayoutInflater.from(PausedBatchWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
            AlertDialog.Builder builder = new AlertDialog.Builder(PausedBatchWeightReadingActivity.this);
            builder.setView(dialogView);
            TextView txt_status = dialogView.findViewById(R.id.txt_status);
            txt_status.setText("Batch Paused");
            ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
            //ImageButton btn_sync = dialogView.findViewById(R.id.btn_sync);
            //btn_sync.setVisibility(View.INVISIBLE);
            final AlertDialog alertDialog = builder.create();
            alertDialog.show();

            btn_test_complete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    BatchStatus ="Paused";
                    helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
                    InsertIntoWeightReadingDb();
//                    DataBaseHelper helper = new DataBaseHelper(PausedBatchWeightReadingActivity.this);
//                    SQLiteDatabase db = helper.getWritableDatabase();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss");
                    String currentDateandTime = sdf.format(new Date());
                    ContentValues cv = new ContentValues();
                    cv.put(Constants.P_TAG_RESULT_OEM_ID, "1234");
                    cv.put(Constants.P_TAG_RESULT_MAC_ID, "1234");
                    cv.put(Constants.P_TAG_RESULT_LOGGER_ID, "DiA0008");
                    cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID, "1");
                    cv.put(Constants.P_TAG_RESULT_TAG_ID, str_batch_id);
                    cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT, Total_qty);
                    cv.put(Constants.P_TAG_RESULT_HEAD, "Right");
                    cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED, "Paused");
                    cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE, currentDateandTime);
                    cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED, "Yes");
                    cv.put(Constants.P_TAG_RESULT_MODIFIED_BY, "Pooja");
                    cv.put(Constants.P_TAG_RESULT_IS_SYNC, "0");

                    long d = db.insert(Constants.P_TAG_RESULT_TABLE, null, cv);
                    Log.d("Success", String.valueOf(d));

                    Intent i = new Intent(PausedBatchWeightReadingActivity.this, ProductionMenuActivity.class);
                    i.putExtra("selected_date", str_selected_date);
                    i.putExtra("product", str_product);
                    i.putExtra("batch_id", str_batch_id);
                    startActivity(i);
                }
            });
        }

    }
    private void getWeightReadingData(String Batchid) {
        Cursor reading = helper.getProductionweightReading(Batchid);
        Double totalexcessgiveaway = 0.0;
        while (reading.moveToNext()) {
            try {
                String pweightId = reading.getString(4);
                String batchid = reading.getString(5);
                String weight = reading.getString(7);
                String shift = reading.getString(8);
                String targetgmperpouch = reading.getString(9);
                String actualgmperpouch = reading.getString(10);
                String excess = reading.getString(11);
                String cumulativeexcessgiveaway = reading.getString(12);
                String cumulativeqty = reading.getString(13);
                String status = reading.getString(18);
                String time = reading.getString(19);
                Double intexcess = Double.parseDouble(excess);

                totalexcessgiveaway += intexcess;

                strTotalexcessgiveaway = String.valueOf(totalexcessgiveaway);

                if(struom.equals("Kg"))
                {
                    readingcount++;
                    double dweightReading = Double.parseDouble(weight)/dintkgvalue;
                    double dexcessgiveaway = Double.parseDouble(excess)/dintkgvalue;
                    PWeighReadingsModel model = new PWeighReadingsModel();
                    model.setWeight(String.valueOf(dweightReading));
                    model.setStatus(status);
                    model.setExcessGiveawayPerPouch(String.valueOf(dexcessgiveaway));
                    model.setCreatedDate(time);
                    model.setPWeightReadingID(pweightId);
                    model.setBatchId(batchid);
                    model.setCumilativeQuantity(cumulativeqty);
                    model.setActualGmPerPouch(actualgmperpouch);
                    model.setCumilativeExcessGiveaway(cumulativeexcessgiveaway);
                    model.setPWeightReadingID(pweightId);
                    model.setShift(shift);
                    model.setTargetGmPerPouch(targetgmperpouch);
                    pWeighReadingsModels.add(model);
                    Log.d("TAG", "baglengthReadingList: ");
                }
                else

                {
                    readingcount++;
                    PWeighReadingsModel model = new PWeighReadingsModel();
                    model.setWeight(weight);
                    model.setStatus(status);
                    model.setExcessGiveawayPerPouch(excess);
                    model.setCreatedDate(time);
                    model.setPWeightReadingID(pweightId);
                    model.setBatchId(batchid);
                    model.setCumilativeQuantity(cumulativeqty);
                    model.setActualGmPerPouch(actualgmperpouch);
                    model.setCumilativeExcessGiveaway(cumulativeexcessgiveaway);
                    model.setPWeightReadingID(pweightId);
                    model.setShift(shift);
                    model.setTargetGmPerPouch(targetgmperpouch);
                    pWeighReadingsModels.add(model);
                    Log.d("TAG", "baglengthReadingList: ");

                }




            } catch (Exception e) {
                e.printStackTrace();
            }
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PausedBatchWeightReadingActivity.this);
            rv_weight_reading.setLayoutManager(linearLayoutManager);
            rv_weight_reading.setAdapter(new PausedBatchListAdapter(PausedBatchWeightReadingActivity.this, pWeighReadingsModels));

        }


    }

    private void getWeightReportData(String Batchid) {
        Cursor report = helper.getProductionBatchWeightReport(Batchid);
        while (report.moveToNext()) {
            try {

                String max = report.getString(4);
                String average = report.getString(5);
                String range = report.getString(6);
                String sd = report.getString(7);
                String unfilled_qty = report.getString(8);
                String filled_qty = report.getString(9);
                String unfilled_pouches = report.getString(10);
                String filled_pouches = report.getString(11);
                String excess_give_away_gm = report.getString(13);
                String excess_give_away_price = report.getString(12);
                String min = report.getString(14);
                String mean = report.getString(15);
                String reason = report.getString(16);
                String remark = report.getString(17);

                strUnfilledPouches = unfilled_pouches;
                strUnfilledQty = unfilled_qty;
                strFilledQty = filled_qty;
                strFilledPouches = filled_pouches;

                Log.d("TAG", "p_tag: ");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public void gmperpouchcalculation() {
        Double sumoftotalweights = 0.0;
        Double packing_total_qty = 0.0;
        int tobepackednoofpouches = 0;
        Double to_be_packed_quantity = 0.0;
        Double previous_cumu_qty = 0.0;
        Double totalexcessgiveaway = 0.0;
        Double excessgiveaway = 0.0;
        int cumulativep_pouches = 0;
        Double remaining_pouch_qty = 0.0;
        Double cumulativeexcessfiveaway = 0.0;

        try {
            if (pWeighReadingsModels.size() != 0) {
                for (int i = 0; i < pWeighReadingsModels.size(); i++) {
                    int pouches_count = pWeighReadingsModels.size();

                    String status = pWeighReadingsModels.get(i).getStatus();

                    if (status.equals("Okay")) {
                        String value = pWeighReadingsModels.get(i).getWeight();
                        sumoftotalweights += Double.parseDouble(value);

                        to_be_packed_quantity += Double.parseDouble(pWeighReadingsModels.get(i).getWeight());
                        tobepackednoofpouches = pouches_count++;
                        excessgiveaway = Double.parseDouble(value) - Double.parseDouble(gmperpouch);

                        String strprevious_cumu_qty = pWeighReadingsModels.get(i).getCumilativeQuantity();
                        previous_cumu_qty = Double.parseDouble(strprevious_cumu_qty);
                        totalexcessgiveaway += excessgiveaway;

                        String strtotalexcessgiveaway = String.valueOf(totalexcessgiveaway);
                        Double dbtotalexcessgiveaway = Double.valueOf(strtotalexcessgiveaway);

                        String strexcessgiveaway = String.valueOf(excessgiveaway);
                        Double dbexcess = Double.valueOf(strexcessgiveaway);

                        txt_excess_give_Away.setText(decimalFormat.format(dbtotalexcessgiveaway));

                        if (dbexcess > Double.parseDouble(gmperpouch)) {
                            txt_excess_give_Away.setText(decimalFormat.format(dbexcess));
                        }

                        if (i != 0) {
                            int pos = i - 1;
                            String previous_cum = pWeighReadingsModels.get(pos).getCumilativeExcessGiveaway();
                            cumulativeexcessfiveaway = excessgiveaway + Double.parseDouble(previous_cum);
                        }
                    }
                }
                packing_total_qty = Double.parseDouble(Total_qty) - 1;

                String strtobepackedqty = String.valueOf(to_be_packed_quantity);
                Double dtoobepackedqty = Double.valueOf(strtobepackedqty);

                txt_to_be_packed_quantity.setText(decimalFormat.format(dtoobepackedqty));
                txt_to_be_packed_pouches.setText(tobepackednoofpouches + "");

                remaining_pouch_qty = Double.parseDouble(Total_qty) - sumoftotalweights;

                //remaining_pouch_qty <=lclgm
                //Integer.parseInt(gmperpouch)

                for (int i = 0; i < pWeighReadingsModels.size(); i++) {
                    String status = pWeighReadingsModels.get(i).getStatus();
                    if (status.equals("Ignored")) {
                        count++;
                        if (count == 3) {
                            handler.removeCallbacks(runnable);

                            ViewGroup viewGroup = findViewById(android.R.id.content);
                            View dialogView = LayoutInflater.from(PausedBatchWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
                            AlertDialog.Builder builder = new AlertDialog.Builder(PausedBatchWeightReadingActivity.this);
                            builder.setView(dialogView);
                            ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
                            //ImageButton btn_sync = dialogView.findViewById(R.id.btn_sync);
                            TextView txt_status = dialogView.findViewById(R.id.txt_status);
                            final AlertDialog alertDialog = builder.create();
                            alertDialog.show();
                            txt_status.setText("Batch Aborted");
                            unfilled_quantity = String.valueOf(remaining_pouch_qty);
                            actualQuantityFilled = String.valueOf(sumoftotalweights);
                            Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
                            String a = txt_to_be_packed_pouches.getText().toString();
                            int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
                            unfilled_pouches = String.valueOf(strunfilled_pouches);

                            handler.removeCallbacks(runnable);

                            btn_test_complete.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.dismiss();
                                    Status = "Abort";
                                    BatchStatus ="Abort";
                                    helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
                                    InsertIntoWeightReadingDb();
                                    Calculation();

                                    SQLiteDatabase db = helper.getWritableDatabase();
                                    String currentDateandTime = dateformat.format(new Date());
                                    ContentValues cv=new ContentValues();

                                    Double dtotalquantity = Double.parseDouble(Total_qty);

                                    cv.put(Constants.P_TAG_RESULT_ID,1);
                                    cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
                                    cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
                                    cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
                                    cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
                                    cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
                                    cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
                                    cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
                                    cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Abort");
                                    cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
                                    cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
                                    cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
                                    cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);




                                    long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
                                    Log.d("Success", String.valueOf(d));

                                    Intent i = new Intent(PausedBatchWeightReadingActivity.this, BatchReportDetails.class);
                                    i.putExtra("selected_date", str_selected_date);
                                    i.putExtra("product", str_product);
                                    i.putExtra("batch_id", str_batch_id);
                                    startActivity(i);

                                }
                            });
                        }
                    } else {
                        count--;
                    }
                }

                if (sumoftotalweights >= packing_total_qty) {
                    btnStop.setVisibility(View.GONE);
                    handler.removeCallbacks(runnable);
                    unfilled_quantity = String.valueOf(remaining_pouch_qty);
                    actualQuantityFilled = String.valueOf(sumoftotalweights);
                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
                    String a = txt_to_be_packed_pouches.getText().toString();
                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
                    unfilled_pouches = String.valueOf(strunfilled_pouches);

                    ViewGroup viewGroup = findViewById(android.R.id.content);
                    View dialogView = LayoutInflater.from(PausedBatchWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(PausedBatchWeightReadingActivity.this);
                    builder.setView(dialogView);
                    ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
                    TextView txt_status = dialogView.findViewById(R.id.txt_status);
                    txt_status.setText("Batch Completed");


                    final AlertDialog alertDialog = builder.create();
                    alertDialog.show();

                    btn_test_complete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            BatchStatus ="Complete";
                            helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
                            InsertIntoWeightReadingDb();

                            Calculation();

//                            DataBaseHelper helper = new DataBaseHelper(PausedBatchWeightReadingActivity.this);
//                            SQLiteDatabase db = helper.getWritableDatabase();
                            String currentDateandTime = sdf.format(new Date());
                            ContentValues cv = new ContentValues();
                            Double dtotalquantity = Double.parseDouble(Total_qty);

                            cv.put(Constants.P_TAG_RESULT_ID,1);
                            cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
                            cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
                            cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
                            cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
                            cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
                            cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
                            cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
                            cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Complete");
                            cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
                            cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
                            cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
                            cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);


                            long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
                            Log.d("Success", String.valueOf(d));


                            Intent i = new Intent(PausedBatchWeightReadingActivity.this, BatchReportDetails.class);
                            i.putExtra("selected_date", str_selected_date);
                            i.putExtra("product", str_product);
                            i.putExtra("batch_id", str_batch_id);
                            startActivity(i);

                        }
                    });

                } else if (remaining_pouch_qty < lclgm) {
                    btnStop.setVisibility(View.GONE);
                    handler.removeCallbacks(runnable);
                    unfilled_quantity = String.valueOf(remaining_pouch_qty);
                    actualQuantityFilled = String.valueOf(sumoftotalweights);
                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
                    String a = txt_to_be_packed_pouches.getText().toString();
                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
                    unfilled_pouches = String.valueOf(strunfilled_pouches);

                    ViewGroup viewGroup = findViewById(android.R.id.content);
                    View dialogView = LayoutInflater.from(PausedBatchWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(PausedBatchWeightReadingActivity.this);
                    builder.setView(dialogView);
                    ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
                    //ImageButton btn_sync = dialogView.findViewById(R.id.btn_sync);
                    TextView txt_status = dialogView.findViewById(R.id.txt_status);
                    final AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                    txt_status.setText("Insufficient remaining quantity");

                    btn_test_complete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            Status = "Abort";
                            BatchStatus ="Paused";
                            helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
                            InsertIntoWeightReadingDb();
                            Calculation();

//                            DataBaseHelper helper = new DataBaseHelper(PausedBatchWeightReadingActivity.this);
//                            SQLiteDatabase db = helper.getWritableDatabase();
                            ContentValues cv=new ContentValues();
                            Double dtotalquantity = 0.0;

                            if(struom.equals("gm"))
                            {
                                dtotalquantity = Double.parseDouble(Total_qty);

                            }
                            else
                            {
                                dtotalquantity = Double.parseDouble(Total_qty) * dintkgvalue;

                            }

                            cv.put(Constants.P_TAG_RESULT_ID,1);
                            cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
                            cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
                            cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
                            cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
                            cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
                            cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
                            cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
                            cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Paused");
                            cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
                            cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
                            cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
                            cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);

                            long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
                            Log.d("Success", String.valueOf(d));

                            Intent i = new Intent(PausedBatchWeightReadingActivity.this, BatchReportDetails.class);
                            i.putExtra("selected_date", str_selected_date);
                            i.putExtra("product", str_product);
                            i.putExtra("batch_id", str_batch_id);
                            startActivity(i);


                        }
                    });

                } else {
                    PWeighReadingsModel model = new PWeighReadingsModel();
                    readingcount++;
                    int size = pWeighReadingsModels.size();
                    model.setBatchId(str_batch_id);
                    model.setWeight(arrayList1.get(index));
                    model.setShift(str_shifts);
                    model.setModifiedDate(str_selected_date);
                    model.setActualGmPerPouch(arrayList1.get(index));
                    model.setTargetGmPerPouch(gmperpouch);
                    //model.setPWeightReadingID(String.valueOf(index+1));
                    model.setPWeightReadingID(String.valueOf(readingcount));
                    model.setCumilativeExcessGiveaway(String.valueOf(cumulativeexcessfiveaway));
                    String weight = arrayList1.get(index);
                    Double excess_giveaway = Double.parseDouble(weight) - Double.parseDouble(gmperpouch);
                    model.setExcessGiveawayPerPouch(String.valueOf(excess_giveaway));
                    model.setCreatedDate(currentDateandTime);

                    String arralistvalue = arrayList1.get(index);
                    Double dvalue = Double.parseDouble(arralistvalue);

                    Double cum_quantity_field = dvalue + previous_cumu_qty;
                    model.setCumilativeQuantity(String.valueOf(cum_quantity_field));
                            /*String pouches = txt_to_be_packed_pouches.getText().toString();
                            model.setCumilativePouches(txt_to_be_packed_pouches.getText().toString());*/

                    if (Integer.parseInt(arrayList1.get(index)) > uclgm || Integer.parseInt(arrayList1.get(index)) < lclgm) {
                        model.setStatus("Ignored");
                    } else {
                        model.setStatus("Okay");
                    }
                    pWeighReadingsModels.add(model);

                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PausedBatchWeightReadingActivity.this);
                    rv_weight_reading.setLayoutManager(linearLayoutManager);
                    rv_weight_reading.setAdapter(new PausedBatchListAdapter(PausedBatchWeightReadingActivity.this, pWeighReadingsModels));

                    index++;

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void kgperpouchCalculation()
    {
        Double sumoftotalweights = 0.0;
        Double packing_total_qty = 0.0;
        int sumoftotalpackedpouches = 0;
        Double sumoftotalpackedquantity =0.0;
        Double previous_cumu_qty = 0.0;
        Double totalexcessgiveaway = 0.0;
        Double excessgiveawayperpouch = 0.0;
        Double cumulativeexcessfiveaway = 0.0;
        Double dgmperpouch = Double.parseDouble(gmperpouch)/Double.parseDouble(strkgvalue);

        try {
            if (pWeighReadingsModels.size()!=0)
            {
                int pouches_count = 1;

                for(int i = 0 ; i < pWeighReadingsModels.size(); i++) {
                    String status = pWeighReadingsModels.get(i).getStatus();

                    if (status.equals("Okay")) {
                        String weight =pWeighReadingsModels.get(i).getWeight();
                        sumoftotalweights += Double.parseDouble(weight);

                        sumoftotalpackedquantity += Double.parseDouble(pWeighReadingsModels.get(i).getWeight());
                        sumoftotalpackedpouches = pouches_count++;
                        excessgiveawayperpouch = Double.parseDouble(weight) - dgmperpouch;

                        String strprevious_cumu_qty = pWeighReadingsModels.get(i).getCumilativeQuantity();
                        previous_cumu_qty = Double.parseDouble(strprevious_cumu_qty);
                        totalexcessgiveaway += excessgiveawayperpouch;

                        String strtotalexcessgiveaway = String.valueOf(totalexcessgiveaway);
                        Double dbtotalexcessgiveaway = Double.valueOf(strtotalexcessgiveaway);

                        String strexcessgiveaway = String.valueOf(excessgiveawayperpouch);
                        Double dbexcess = Double.valueOf(strexcessgiveaway);

                        txt_excess_give_Away.setText(decimalFormat.format(dbtotalexcessgiveaway));

                        if (excessgiveawayperpouch > Double.parseDouble(gmperpouch)) {
                            txt_excess_give_Away.setText(decimalFormat.format(dbexcess));
                        }

                        if (i!=0) {
                            int pos = i-1;
                            String previous_cum= pWeighReadingsModels.get(pos).getCumilativeExcessGiveaway();
                            cumulativeexcessfiveaway = excessgiveawayperpouch + Double.parseDouble(previous_cum);

                        }
                    }
                }
                Double dvalue = 1.0/dintkgvalue;
                Double dtotalqty = Double.parseDouble(Total_qty)/dintkgvalue;
                packing_total_qty = dtotalqty - dvalue;

                String strtobepackedqty = String.valueOf(sumoftotalpackedquantity);
                Double dtoobepackedqty = Double.valueOf(strtobepackedqty);

                txt_to_be_packed_quantity.setText(decimalFormat.format(dtoobepackedqty));
                txt_to_be_packed_pouches.setText(sumoftotalpackedpouches + "");

                //this quantity used for checking the remaining quantity for packing
                Double dqty = dtotalqty * dintkgvalue;
                Double dsumoftotalweight = sumoftotalweights*dintkgvalue;
                Double dremaingqty = dqty - dsumoftotalweight;
                Double dlc = dlcl * dintkgvalue;
                //remaining_pouch_qty = Double.parseDouble(Total_qty)- sumoftotalweights;

                for (int i =0 ;i<pWeighReadingsModels.size();i++)
                {
                    String status = pWeighReadingsModels.get(i).getStatus();
                    if (status.equals("Ignored"))
                    {
                        count++;
                        if (count==3) {
                            handler.removeCallbacks(runnable);
                            ViewGroup viewGroup = findViewById(android.R.id.content);
                            View dialogView = LayoutInflater.from(PausedBatchWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
                            AlertDialog.Builder builder = new AlertDialog.Builder(PausedBatchWeightReadingActivity.this);
                            builder.setView(dialogView);
                            ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
                            TextView txt_status = dialogView.findViewById(R.id.txt_status);
                            final AlertDialog alertDialog = builder.create();
                            alertDialog.show();
                            txt_status.setText("Batch Aborted");
                            unfilled_quantity = String.valueOf(dremaingqty);
                            actualQuantityFilled = String.valueOf(sumoftotalweights);
                            Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
                            String a =  txt_to_be_packed_pouches.getText().toString();
                            int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
                            unfilled_pouches = String.valueOf(strunfilled_pouches);

                            handler.removeCallbacks(runnable);

                            btn_test_complete.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.dismiss();
                                    Status ="Abort";
                                    BatchStatus ="Abort";
                                    helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);

                                    InsertIntoWeightReadingDb();
                                    Calculation();
                                    SQLiteDatabase db = helper.getWritableDatabase();
                                    String currentDateandTime = dateformat.format(new Date());
                                    ContentValues cv=new ContentValues();
                                    Double dtotalquantity = Double.parseDouble(Total_qty) * dintkgvalue;

                                    cv.put(Constants.P_TAG_RESULT_ID,1);
                                    cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
                                    cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
                                    cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
                                    cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
                                    cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
                                    cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
                                    cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
                                    cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Abort");
                                    cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
                                    cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
                                    cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
                                    cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);



                                    long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
                                    Log.d("Success", String.valueOf(d));

                                    Intent i = new Intent(PausedBatchWeightReadingActivity.this, BatchReportDetails.class);
                                    i.putExtra("selected_date",str_selected_date);
                                    i.putExtra("product",str_product);
                                    i.putExtra("batch_id",str_batch_id);
                                    startActivity(i);

                                }
                            });
                        }
                    }
                    else {
                        count--;
                    }
                }

                if (sumoftotalweights>=packing_total_qty)
                {
                    btnStop.setVisibility(View.GONE);
                    handler.removeCallbacks(runnable);
                    unfilled_quantity = String.valueOf(dremaingqty);
                    actualQuantityFilled = String.valueOf(sumoftotalweights);
                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
                    String a =  txt_to_be_packed_pouches.getText().toString();
                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
                    unfilled_pouches = String.valueOf(strunfilled_pouches);

                    ViewGroup viewGroup = findViewById(android.R.id.content);
                    View dialogView = LayoutInflater.from(PausedBatchWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(PausedBatchWeightReadingActivity.this);
                    builder.setView(dialogView);
                    ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
                    TextView txt_status = dialogView.findViewById(R.id.txt_status);
                    txt_status.setText("Batch Completed");


                    final AlertDialog alertDialog = builder.create();
                    alertDialog.show();

                    btn_test_complete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            BatchStatus ="Complete";
                            helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
                            InsertIntoWeightReadingDb();

                            Calculation();
                            SQLiteDatabase db = helper.getWritableDatabase();
                            String currentDateandTime = dateformat.format(new Date());
                            ContentValues cv=new ContentValues();
                            cv.put(Constants.P_TAG_RESULT_ID,1);
                            cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
                            cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
                            cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
                            cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
                            cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
                            cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(Total_qty)));
                            cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
                            cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Complete");
                            cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
                            cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
                            cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
                            cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);



                            Intent i = new Intent(PausedBatchWeightReadingActivity.this, BatchReportDetails.class);
                            i.putExtra("selected_date",str_selected_date);
                            i.putExtra("product",str_product);
                            i.putExtra("batch_id",str_batch_id);
                            startActivity(i);

                        }
                    });

                }
                else if (dremaingqty < dlc)
                {
                    btnStop.setVisibility(View.GONE);
                    handler.removeCallbacks(runnable);
                    unfilled_quantity = String.valueOf(dremaingqty);
                    actualQuantityFilled = String.valueOf(sumoftotalweights);
                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
                    String a =  txt_to_be_packed_pouches.getText().toString();
                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
                    unfilled_pouches = String.valueOf(strunfilled_pouches);

                    ViewGroup viewGroup = findViewById(android.R.id.content);
                    View dialogView = LayoutInflater.from(PausedBatchWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(PausedBatchWeightReadingActivity.this);
                    builder.setView(dialogView);
                    ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
                    TextView txt_status = dialogView.findViewById(R.id.txt_status);
                    final AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                    txt_status.setText("Insufficient remaining quantity");

                    btn_test_complete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            Status = "Abort";
                            BatchStatus ="Abort";
                            helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
                            InsertIntoWeightReadingDb();
                            Calculation();
                            SQLiteDatabase db = helper.getWritableDatabase();
                            String currentDateandTime = dateformat.format(new Date());
                            ContentValues cv=new ContentValues();
                            Double dtotalquantity = Double.parseDouble(Total_qty) * dintkgvalue;

                            cv.put(Constants.P_TAG_RESULT_ID,1);
                            cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
                            cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
                            cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
                            cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
                            cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
                            cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
                            cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
                            cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Abort");
                            cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
                            cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
                            cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
                            cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);
                            Intent i = new Intent(PausedBatchWeightReadingActivity.this, BatchReportDetails.class);
                            i.putExtra("selected_date",str_selected_date);
                            i.putExtra("product",str_product);
                            i.putExtra("batch_id",str_batch_id);
                            startActivity(i);


                        }
                    });

                }
                else {
                    PWeighReadingsModel model = new PWeighReadingsModel();
                    String currentDateandTime = dateformat.format(new Date());
                    readingcount++;
                    model.setBatchId(str_batch_id);
                    model.setWeight(arrayList1.get(index));
                    model.setShift(str_shifts);
                    model.setModifiedDate(str_selected_date);
                    model.setActualGmPerPouch(arrayList1.get(index));
                    model.setTargetGmPerPouch(String.valueOf(dgmperpouch));
                    model.setPWeightReadingID(String.valueOf(readingcount));
                    model.setCumilativeExcessGiveaway(String.valueOf(cumulativeexcessfiveaway));
                    String weight = arrayList1.get(index);
                    Double excess_giveaway = Double.parseDouble(weight)-dgmperpouch;
                    model.setExcessGiveawayPerPouch(String.valueOf(decimalFormat.format(excess_giveaway)));
                    model.setCreatedDate(currentDateandTime);

                    Double cum_quantity_field = Double.parseDouble(arrayList1.get(index)) + previous_cumu_qty;
                    model.setCumilativeQuantity(String.valueOf(cum_quantity_field));
                    String pouches = txt_to_be_packed_pouches.getText().toString();
                    model.setCumilativePouches(txt_to_be_packed_pouches.getText().toString());

                    if (Double.parseDouble(arrayList1.get(index))>ducl || Double.parseDouble(arrayList1.get(index))<dlcl) {
                        model.setStatus("Ignored");
                    }
                    else
                    {
                        model.setStatus("Okay");
                    }
                    pWeighReadingsModels.add(model);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PausedBatchWeightReadingActivity.this);
                    rv_weight_reading.setLayoutManager(linearLayoutManager);
                    rv_weight_reading.setAdapter(new PausedBatchListAdapter(PausedBatchWeightReadingActivity.this, pWeighReadingsModels));


                    index ++;
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();

        }
    }

    public String sendReadyToDas1(String tagId, String dataLoggerIP, String dataLoggerName) throws IOException {
        String tag = tagId;
        HttpClient httpClient = new DefaultHttpClient();

        String responseString="";

        String url = "http://"+dataLoggerIP+":"+DATA_LOGGER_PORT+"/start?tagid="+tagId+"&dataLoggerIP="+dataLoggerIP+"&dataLoggerName="+dataLoggerName;
        //String url = "http://"+dataLoggerIP+"/start?tagid="+tagId+"&dataLoggerIP="+dataLoggerIP+"&dataLoggerName="+dataLoggerName;
        HttpResponse response1 = httpClient.execute(new HttpGet(url));
        StatusLine statusLine = response1.getStatusLine();
        if(statusLine.getStatusCode() == HttpStatus.SC_OK){
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response1.getEntity().writeTo(out);
            responseString = out.toString();
            Toast.makeText(PausedBatchWeightReadingActivity.this, responseString, Toast.LENGTH_SHORT).show();
            out.close();
        } else{
            response1.getEntity().getContent().close();
            throw new IOException(statusLine.getReasonPhrase());
        }
        return responseString;
    }

    /*private String abort() throws IOException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String tag = current_tag_id;
        HttpClient httpClient = new DefaultHttpClient();

        String responseString="";

        String dataPort=Constants.DATA_LOGGER_PORT+"";
        String url = "http://"+dataloggerIp+":"+dataPort+"/abort?tagid=";
        HttpResponse response1 = httpClient.execute(new HttpGet(url));
        StatusLine statusLine = response1.getStatusLine();
        if(statusLine.getStatusCode() == HttpStatus.SC_OK){
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response1.getEntity().writeTo(out);
            responseString = out.toString();
            Toast.makeText(this, responseString, Toast.LENGTH_SHORT).show();
            out.close();
        } else{
            response1.getEntity().getContent().close();
            throw new IOException(statusLine.getReasonPhrase());
        }
        return responseString;

    }*/
}