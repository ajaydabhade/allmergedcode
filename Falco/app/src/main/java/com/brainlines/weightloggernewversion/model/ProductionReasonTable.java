package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ProductionReasonTable implements Parcelable {
    String ReasonID,ReasonDesc,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy;

    public ProductionReasonTable(String reasonID, String reasonDesc, String createdDate, String modifiedDate, String createdBy, String modifiedBy) {
        ReasonID = reasonID;
        ReasonDesc = reasonDesc;
        CreatedDate = createdDate;
        ModifiedDate = modifiedDate;
        CreatedBy = createdBy;
        ModifiedBy = modifiedBy;
    }

    protected ProductionReasonTable(Parcel in) {
        ReasonID = in.readString();
        ReasonDesc = in.readString();
        CreatedDate = in.readString();
        ModifiedDate = in.readString();
        CreatedBy = in.readString();
        ModifiedBy = in.readString();
    }

    public static final Creator<ProductionReasonTable> CREATOR = new Creator<ProductionReasonTable>() {
        @Override
        public ProductionReasonTable createFromParcel(Parcel in) {
            return new ProductionReasonTable(in);
        }

        @Override
        public ProductionReasonTable[] newArray(int size) {
            return new ProductionReasonTable[size];
        }
    };

    public String getReasonID() {
        return ReasonID;
    }

    public void setReasonID(String reasonID) {
        ReasonID = reasonID;
    }

    public String getReasonDesc() {
        return ReasonDesc;
    }

    public void setReasonDesc(String reasonDesc) {
        ReasonDesc = reasonDesc;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ReasonID);
        dest.writeString(ReasonDesc);
        dest.writeString(CreatedDate);
        dest.writeString(ModifiedDate);
        dest.writeString(CreatedBy);
        dest.writeString(ModifiedBy);
    }
}
