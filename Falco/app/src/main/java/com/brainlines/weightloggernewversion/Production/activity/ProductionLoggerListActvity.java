package com.brainlines.weightloggernewversion.Production.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.activity.LoggerFunctionActivity;
import com.brainlines.weightloggernewversion.adapter.LoggerListAdapter;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.listener.GoToNewTestTagFillAcitivity;
import com.brainlines.weightloggernewversion.model.LoggersFromDBModel;
import com.brainlines.weightloggernewversion.model.MasterFillerTypeModel;
import com.brainlines.weightloggernewversion.model.MasterHeadModel;
import com.brainlines.weightloggernewversion.model.MasterMachineTypeModel;
import com.brainlines.weightloggernewversion.model.MasterPouchSymmetryModel;
import com.brainlines.weightloggernewversion.model.MasterProduct;
import com.brainlines.weightloggernewversion.model.MasterSealTypeModel;
import com.brainlines.weightloggernewversion.model.MasterTargetAccuracyModel;
import com.brainlines.weightloggernewversion.model.MasterWireCupModel;
import com.brainlines.weightloggernewversion.utils.AsyncTasks;
import com.brainlines.weightloggernewversion.utils.Constants;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class ProductionLoggerListActvity extends AppCompatActivity implements GoToNewTestTagFillAcitivity {
    private static final String TAG = "NewTestActivity";

    RecyclerView rv_Logger_List;
    LoggerListAdapter adapter;
    ArrayList<LoggersFromDBModel> loggerList = new ArrayList<>();
    ArrayList<MasterFillerTypeModel> masterFillerTypeList = new ArrayList<>();
    ArrayList<MasterHeadModel> masterHeadModelList = new ArrayList<>();
    ArrayList<MasterTargetAccuracyModel> masterTargetAccuracyModels = new ArrayList<>();
    ArrayList<MasterProduct> masterProductTypeArrayList = new ArrayList<>();
    ArrayList<MasterMachineTypeModel> machineTypeModels = new ArrayList<>();
    ArrayList<MasterSealTypeModel> sealTypeModels = new ArrayList<>();
    ArrayList<MasterWireCupModel> masterWireCupModels = new ArrayList<>();
    DataBaseHelper helper;
    private String dataPort = Constants.DATA_LOGGER_PORT+"";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setContentView(R.layout.activity_new_test);

        try {
            initUI();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }
    private void initUI() throws ExecutionException, InterruptedException {
//        helper = new DataBaseHelper(this);
        rv_Logger_List = findViewById(R.id.rv_Logger_List);

        ArrayList<LoggersFromDBModel> loggers = getLoggerListFromDB();

        LinearLayoutManager manager = new LinearLayoutManager(this);
        rv_Logger_List.setLayoutManager(manager);
        adapter = new LoggerListAdapter(this,loggers,this);
        rv_Logger_List.setAdapter(adapter);

        for (int i = 0 ; i < loggers.size() ; i++){
            LoggersFromDBModel model = loggers.get(i);
            helper.addDataLoggersToLocalDB(i+1+"",model.getOem_id(),model.getSerial_num(),model.getLogger_name(),model.getIp_address(),model.getMac_id(),model.getLogger_location(),model.getCreate_timestamp(),model.getCreate_by_id(),model.getUpdate_timestamp(),model.getUpdate_by_id(),model.isActive(),model.getInuse());
            Log.d(TAG, "initUI: ");
        }

        Cursor data = helper.getDataLoggers();
        while (data.moveToNext()){
            String loggerId = data.getString(0);
            String oemId = data.getString(1);
            String srNum = data.getString(2);
            String loggerName = data.getString(3);
            String ip = data.getString(4);
            String mac = data.getString(5);
            String location = data.getString(6);
            String createTimeStamp = data.getString(7);
            String createdBy = data.getString(8);
            String uptimestamp = data.getString(9);
            String modifiedBy = data.getString(10);
            boolean isActive = data.getInt(11) > 0;
            boolean inUse = data.getInt(12) > 0;
            Log.d(TAG, "initUI: ");
            //usersListFromLocalDb.add(new UsersFromDBModel(loginID,userID,roleID,userName,password,isActive,isDeleted,createdDate,modifiedDate));
        }

//        getDataFromServerDb();
    }

    private ArrayList<LoggersFromDBModel> getLoggerListFromDB() throws ExecutionException, InterruptedException {
        loggerList.clear();
        loggerList = new AsyncTasks.GetLoggerListFromDB(this).execute().get();
        return loggerList;
    }

    private ArrayList<MasterFillerTypeModel> getMasterFillersfromDB() throws ExecutionException, InterruptedException {
        masterFillerTypeList.clear();
        masterFillerTypeList = new AsyncTasks.GetMasterFillerTypeFromDB(this).execute().get();
        return masterFillerTypeList;
    }

    private ArrayList<MasterHeadModel> getMasterHeadTable() throws ExecutionException, InterruptedException {
        masterHeadModelList.clear();
        masterHeadModelList = new AsyncTasks.GetMasterHeadTable(this).execute().get();
        return masterHeadModelList;
    }

    private ArrayList<MasterTargetAccuracyModel> getTargetAcuracyTable() throws ExecutionException, InterruptedException {
        masterTargetAccuracyModels.clear();
        masterTargetAccuracyModels = new AsyncTasks.GetTargetAccuracy(this).execute().get();
        return masterTargetAccuracyModels;
    }

    private ArrayList<MasterProduct> getProductType() throws ExecutionException, InterruptedException {
        masterProductTypeArrayList.clear();
        masterProductTypeArrayList = new AsyncTasks.GetProductMaster(this).execute().get();
        return masterProductTypeArrayList;
    }

    private ArrayList<MasterMachineTypeModel> getMachineTypeTable() throws ExecutionException, InterruptedException {
        machineTypeModels.clear();
        machineTypeModels = new AsyncTasks.GetMachineTypeMaster(this).execute().get();
        return machineTypeModels;
    }

    private ArrayList<MasterSealTypeModel> getSealTypeTable() throws ExecutionException, InterruptedException {
        sealTypeModels.clear();
        sealTypeModels = new AsyncTasks.GetSealTypeMaster(this).execute().get();
        return sealTypeModels;
    }

    private ArrayList<MasterWireCupModel> getWireCupFromDB() throws ExecutionException, InterruptedException {
        masterWireCupModels.clear();
        masterWireCupModels = new AsyncTasks.GetWireCupMaster(this).execute().get();
        return masterWireCupModels;
    }


    private String getDataLoggerStatus(String loggerName) {
//        Cursor loggerStatus = helper.getLoggerStatusFromLoggerTable(loggerName);
//        String loggerStatusYesOrNo = "";
//        while (loggerStatus.moveToNext()){
//            loggerStatusYesOrNo = loggerStatus.getString(0);
//            Log.d(TAG, "getFillerType: ");
//        }
        return "";
    }

    private String checkIfIsReady(String dataLoggerIP, String dataLoggerName) throws IOException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        HttpClient httpClient = new DefaultHttpClient();
        String responseString="";
        String url = "http://"+dataLoggerIP+":"+dataPort+"/hello?loggername="+dataLoggerName;
        //String url = "http://"+dataLoggerIP+":"+dataPort+"/hello?loggername=";
        HttpResponse response1 = httpClient.execute(new HttpGet(url));
        StatusLine statusLine = response1.getStatusLine();
        if(statusLine.getStatusCode() == HttpStatus.SC_OK){
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response1.getEntity().writeTo(out);
            responseString = out.toString();
            if (responseString.equals("ok")){
                changeDataLoggerStatus(dataLoggerName,"Yes");
                Intent intent = new Intent(this, LoggerFunctionActivity.class);
                intent.putExtra("LoggerName",dataLoggerName);
                intent.putExtra("LoggerIP",dataLoggerIP);
                startActivity(intent);
            }
            Toast.makeText(this, responseString, Toast.LENGTH_SHORT).show();
            out.close();
        } else{
            response1.getEntity().getContent().close();
            throw new IOException(statusLine.getReasonPhrase());
        }
        return responseString;
    }

    private void changeDataLoggerStatus(String dataLoggerName, String yes) {
        helper.updateMstLoggerTableForLoggerStatus(dataLoggerName);
    }

    @Override
    public void getLogger(String loggerName, String loggerIpAddress, String loggerMacId) throws IOException {
        try {
            String loggerStatus = getDataLoggerStatus(loggerName);
            if (loggerStatus.equals("No")){
                String ifReady = checkIfIsReady(loggerIpAddress,loggerName);
                String status = ifReady;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
