package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

import android.os.Parcel;
import android.os.Parcelable;

public class GetChecklistOfServiceCallModel implements Parcelable {
    String ServiceCallID;
    String Typeofservicecallid;

    public GetChecklistOfServiceCallModel(String serviceCallID, String typeofservicecallid) {
        ServiceCallID = serviceCallID;
        Typeofservicecallid = typeofservicecallid;
    }

    protected GetChecklistOfServiceCallModel(Parcel in) {
        ServiceCallID = in.readString();
        Typeofservicecallid = in.readString();
    }

    public static final Creator<GetChecklistOfServiceCallModel> CREATOR = new Creator<GetChecklistOfServiceCallModel>() {
        @Override
        public GetChecklistOfServiceCallModel createFromParcel(Parcel in) {
            return new GetChecklistOfServiceCallModel(in);
        }

        @Override
        public GetChecklistOfServiceCallModel[] newArray(int size) {
            return new GetChecklistOfServiceCallModel[size];
        }
    };

    public String getServiceCallID() {
        return ServiceCallID;
    }

    public void setServiceCallID(String serviceCallID) {
        ServiceCallID = serviceCallID;
    }

    public String getTypeofservicecallid() {
        return Typeofservicecallid;
    }

    public void setTypeofservicecallid(String typeofservicecallid) {
        Typeofservicecallid = typeofservicecallid;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ServiceCallID);
        dest.writeString(Typeofservicecallid);
    }
}
