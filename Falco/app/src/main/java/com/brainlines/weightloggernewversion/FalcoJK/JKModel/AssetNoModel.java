package com.brainlines.weightloggernewversion.FalcoJK.JKModel;

public class AssetNoModel {
    String assetno;

    public String getAssetno() {
        return assetno;
    }

    public void setAssetno(String assetno) {
        this.assetno = assetno;
    }
}
