package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

public class PartOrderByCustomerModel {

    String ServiceCallID,OEMID,CustomerID,MachineID,ITEMID,Quantity,NAME,PRICE,Applicable,Consumables,FinalPrice,ExcludeInclude,CreatedBy,CreatedDate;

//    public PartOrderByCustomerModel(String serviceCallID, String OEMID, String customerID, String machineID, String ITEMID, String quantity, String NAME, String PRICE, String applicable, String consumables, String finalPrice, String excludeInclude, String createdBy, String createdDate) {
//        ServiceCallID = serviceCallID;
//        this.OEMID = OEMID;
//        CustomerID = customerID;
//        MachineID = machineID;
//        this.ITEMID = ITEMID;
//        Quantity = quantity;
//        this.NAME = NAME;
//        this.PRICE = PRICE;
//        Applicable = applicable;
//        Consumables = consumables;
//        FinalPrice = finalPrice;
//        ExcludeInclude = excludeInclude;
//        CreatedBy = createdBy;
//        CreatedDate = createdDate;
//    }

    public String getServiceCallID() {
        return ServiceCallID;
    }

    public void setServiceCallID(String serviceCallID) {
        ServiceCallID = serviceCallID;
    }

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getMachineID() {
        return MachineID;
    }

    public void setMachineID(String machineID) {
        MachineID = machineID;
    }

    public String getITEMID() {
        return ITEMID;
    }

    public void setITEMID(String ITEMID) {
        this.ITEMID = ITEMID;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getPRICE() {
        return PRICE;
    }

    public void setPRICE(String PRICE) {
        this.PRICE = PRICE;
    }

    public String getApplicable() {
        return Applicable;
    }

    public void setApplicable(String applicable) {
        Applicable = applicable;
    }

    public String getConsumables() {
        return Consumables;
    }

    public void setConsumables(String consumables) {
        Consumables = consumables;
    }

    public String getFinalPrice() {
        return FinalPrice;
    }

    public void setFinalPrice(String finalPrice) {
        FinalPrice = finalPrice;
    }

    public String getExcludeInclude() {
        return ExcludeInclude;
    }

    public void setExcludeInclude(String excludeInclude) {
        ExcludeInclude = excludeInclude;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }
}
