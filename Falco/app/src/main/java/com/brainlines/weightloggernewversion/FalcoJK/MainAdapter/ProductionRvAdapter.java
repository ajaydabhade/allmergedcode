package com.brainlines.weightloggernewversion.FalcoJK.MainAdapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.ProdTicketModel;
import com.brainlines.weightloggernewversion.FalcoJK.UrlConstants.JK_URL_Constants;
import com.brainlines.weightloggernewversion.FalcoJK.activity.JK_ProductionTicket_Update_Actvity;
import com.brainlines.weightloggernewversion.FalcoJK.activity.JkViewProductionTicket;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class ProductionRvAdapter extends RecyclerView.Adapter<ProductionRvAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<ProdTicketModel> ticket_list;
    View.OnClickListener mClickListener;
    Context context;
    String str_tk_no;

    public ProductionRvAdapter(Context ctx, ArrayList<ProdTicketModel> ticket_list){
        inflater = LayoutInflater.from(ctx);
        this.ticket_list = ticket_list;
        this.context=ctx;
    }

    @Override
    public int getItemCount() {
        return ticket_list.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_production_list_details, parent, false);
        final MyViewHolder holder = new MyViewHolder(view);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onClick(view);

            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int i) {
        ProdTicketModel model=ticket_list.get(i);
        if (model!= null)
        {
            holder.tk_no.setText(ticket_list.get(i).getPtkd_TkNo());
            holder.tk_sku_8_d.setText(ticket_list.get(i).getPtkd_PSKU());
            holder.tk_plant.setText(ticket_list.get(i).getPtkd_Plant());

            holder.tk_value_stream.setText(ticket_list.get(i).getPtkd_ValueStreamCode());
            //holder.tk_prod_qty.setText();
            holder.tk_start_date.setText(ticket_list.get(i).getTk_Actual_Start_Date());
            holder.tk_end_date.setText(model.getTk_Actual_End_Date());


           /* if(ticket_list.get(i).getPtkd_IsRelease().equals("true"))
            {
                holder.tk_Release_date.setText("NA");
            }*/
            if (ticket_list.get(i).getPtkd_ReleaseDate().equals("null"))
            {
                holder.tk_Release_date.setText("NA");
            }
            else
            {
                holder.tk_Release_date.setText(ticket_list.get(i).getPtkd_ReleaseDate());
            }
            String total_qty = ticket_list.get(i).getPtkd_Qty();
            double constant = 12.0;
            try
            {
                double result = Double.parseDouble(total_qty) * constant;
                String qty = String.valueOf(result);
                holder.pro_txt_qty.setText(qty.concat(" no"));
            }
            catch (Exception e)
            {
                e.printStackTrace();
                holder.pro_txt_qty.setText(total_qty.concat(" no"));

            }
            String tkrelease=ticket_list.get(i).getPtkd_IsRelease();

            if (tkrelease.equals("true"))
            {
                holder.img_release.setVisibility(View.INVISIBLE);
                holder.img_icon_for_Release_ticket.setVisibility(View.INVISIBLE);
            }
            else if (holder.pro_details_layout.getVisibility()==View.VISIBLE)
            {
                holder.pro_img_plus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        holder.pro_details_layout.setVisibility(View.VISIBLE);
                        holder.pro_img_plus.setImageResource(R.drawable.minus16);
                    }
                });
            }
            else
            {
                holder.pro_img_plus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        holder.pro_details_layout.setVisibility(View.GONE);
                        holder.pro_img_plus.setImageResource(R.drawable.plus16size);
                    }
                });
            }
            String actend_date = model.getTk_act_End_Date();
            if (actend_date != null)
            {
                holder.rl_production.setBackgroundResource(R.drawable.shape_background_for_close_ticket);
            }
            if (tkrelease.equals("false"))
            {
                holder.img_btn_update.setEnabled(false);
                holder.img_btn_view.setEnabled(false);
                holder.img_btn_update.setClickable(false);
                holder.img_btn_view.setClickable(false);
            }
            else
            {
                holder.img_btn_update.setEnabled(true);
                holder.img_btn_view.setEnabled(true);
                holder.img_btn_update.setClickable(true);
                holder.img_btn_view.setClickable(true);
            }



            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.pro_details_layout.getVisibility()==View.VISIBLE)
                    {
                        holder.pro_img_plus.setImageResource(R.drawable.plus16size);
                    }
                    else
                    {
                        holder.pro_img_plus.setImageResource(R.drawable.minus16);
                    }
                    holder.pro_details_layout.setVisibility((holder.pro_details_layout.getVisibility() == View.VISIBLE)
                            ? View.GONE
                            : View.VISIBLE);
                }
            });
            holder.pro_details_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.pro_details_layout.setVisibility(View.GONE);
                    holder.pro_img_plus.setImageResource(R.drawable.plus16size);

                }
            });

            holder.img_release.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    str_tk_no=ticket_list.get(i).getPtkd_TkNo();
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
                    String str_tk_release_date = sdf.format(c.getTime());
                    String[] string =str_tk_release_date.split(" ");
                    String main_string_release_date=string[0];
                    new ReleaseProductionTkCall().execute();
                    holder.tk_Release_date.setText(main_string_release_date);
                    holder.img_release.setVisibility(View.GONE);
                    holder.img_btn_update.setEnabled(true);
                    holder.img_btn_view.setEnabled(true);
                    holder.img_btn_update.setClickable(true);
                    holder.img_btn_view.setClickable(true);
                    holder.img_icon_for_Release_ticket.setVisibility(View.GONE);


                }
            });

            holder.img_btn_update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent =new Intent(context, JK_ProductionTicket_Update_Actvity.class);
                    Bundle bun = new Bundle();
                    bun.putString("ptk_tk_id",ticket_list.get(i).getPtkd_PktId());
                    bun.putString("tk_no",ticket_list.get(i).getPtkd_TkNo());
                    bun.putString("tk_start_date",ticket_list.get(i).getTk_Actual_Start_Date());
                    bun.putString("tk_end_date",ticket_list.get(i).getTk_Actual_End_Date());
                    bun.putString("tk_sku",ticket_list.get(i).getPtkd_PSKU());
                    bun.putString("tk_plant",ticket_list.get(i).getPtkd_Plant());
                    bun.putString("tk_quantity",ticket_list.get(i).getPtkd_Qty());
                    bun.putString("ptyk_Active",ticket_list.get(i).getPtkd_Active());
                    intent.putExtras(bun);
                    context.startActivity(intent);
                }
            });
            holder.img_btn_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent =new Intent(context, JkViewProductionTicket.class);
                    Bundle bun = new Bundle();
                    bun.putString("ptk_tk_id",ticket_list.get(i).getPtkd_PktId());
                    bun.putString("ptyk_Active",ticket_list.get(i).getPtkd_Active());
                    bun.putString("tk_no",ticket_list.get(i).getPtkd_TkNo());
                    bun.putString("tk_start_date",ticket_list.get(i).getTk_Actual_Start_Date());
                    bun.putString("tk_end_date",ticket_list.get(i).getTk_Actual_End_Date());
                    bun.putString("tk_sku_8",ticket_list.get(i).getPtkd_PSKU());
                    bun.putString("tk_plant",ticket_list.get(i).getPtkd_Plant());
                    bun.putString("tk_quantity",ticket_list.get(i).getPtkd_Qty());
                    bun.putString("ptyk_Active",ticket_list.get(i).getPtkd_Active());
                    intent.putExtras(bun);
                    context.startActivity(intent);

                }
            });
        }

    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tk_no,tk_plant,tk_sku_8_d,tk_Release_date,tk_value_stream,tk_start_date,tk_end_date,pro_txt_qty,tk_prod_qty;
        ImageView img_btn_update,img_btn_view,img_release,pro_img_plus,img_icon_for_Release_ticket;
        RelativeLayout pro_details_layout;
        FrameLayout frame_list_card_item;
        RelativeLayout rl_production;

        public MyViewHolder(View itemView) {
            super(itemView);
            tk_no= itemView.findViewById(R.id.pro_txt_ticket_no);
            tk_plant= itemView.findViewById(R.id.pro_txt_up_plant);
            tk_sku_8_d= itemView.findViewById(R.id.txt_sku_8_digit);
            tk_Release_date= itemView.findViewById(R.id.pro_txt_release_date);
            tk_value_stream= itemView.findViewById(R.id.pro_txt_value_stream);
            tk_start_date= itemView.findViewById(R.id.txt_up_start_date);
            tk_end_date= itemView.findViewById(R.id.pro_txt_end_date);
            img_btn_update=(ImageButton)itemView.findViewById(R.id.pro_img_update);
            img_release=(ImageButton)itemView.findViewById(R.id.pro_img_release);
            img_btn_view=(ImageButton)itemView.findViewById(R.id.pro_img_view);
            pro_img_plus=(ImageButton)itemView.findViewById(R.id.pro_img_plus);
            pro_details_layout= itemView.findViewById(R.id.pro_details_list);
            pro_txt_qty=(TextView)itemView.findViewById(R.id.pro_txt_qty);
            //tk_prod_qty=(TextView)itemView.findViewById(R.id.pro_txt_produce_qty);
            img_icon_for_Release_ticket=(ImageView)itemView.findViewById(R.id.img_icon_for_Release_ticket);
            frame_list_card_item = (FrameLayout)itemView.findViewById(R.id.frame_list_card_item);
            rl_production = (RelativeLayout) itemView.findViewById(R.id.rl_production);

        }
    }

    public class ReleaseProductionTkCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(context);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String str_created_by="capacity@decintell.com";
            String url= JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.RELEASE_PROD_TICKETS + str_tk_no + "&tk_CreatedBy=" + str_created_by;
            response= URL_Constants.makeHttpPutRequest(url);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try
                { JSONObject object=new JSONObject(response);
                    String data=object.getString("data");
                    String status=object.getString("status");
                    String message=object.getString("message");

                    if (status.equals("200"))
                    {
                        Toast.makeText(context, "Successfully Released", Toast.LENGTH_SHORT).show();

                    }

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

        }
    }


}
