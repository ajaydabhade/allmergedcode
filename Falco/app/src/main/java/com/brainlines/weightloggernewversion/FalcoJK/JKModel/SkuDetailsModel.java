package com.brainlines.weightloggernewversion.FalcoJK.JKModel;

public class SkuDetailsModel {
    String SKU_No,dp_stamp_type,dp_stamp_chart,dp_tang_color,dp_blko_flg,QTY,ReleaseQty;

    public String getReleaseQty() {
        return ReleaseQty;
    }

    public void setReleaseQty(String releaseQty) {
        ReleaseQty = releaseQty;
    }

    public String getSKU_No() {
        return SKU_No;
    }

    public void setSKU_No(String SKU_No) {
        this.SKU_No = SKU_No;
    }

    public String getDp_stamp_type() {
        return dp_stamp_type;
    }

    public void setDp_stamp_type(String dp_stamp_type) {
        this.dp_stamp_type = dp_stamp_type;
    }

    public String getDp_stamp_chart() {
        return dp_stamp_chart;
    }

    public void setDp_stamp_chart(String dp_stamp_chart) {
        this.dp_stamp_chart = dp_stamp_chart;
    }

    public String getDp_tang_color() {
        return dp_tang_color;
    }

    public void setDp_tang_color(String dp_tang_color) {
        this.dp_tang_color = dp_tang_color;
    }

    public String getDp_blko_flg() {
        return dp_blko_flg;
    }

    public void setDp_blko_flg(String dp_blko_flg) {
        this.dp_blko_flg = dp_blko_flg;
    }

    public String getQTY() {
        return QTY;
    }

    public void setQTY(String QTY) {
        this.QTY = QTY;
    }
}
