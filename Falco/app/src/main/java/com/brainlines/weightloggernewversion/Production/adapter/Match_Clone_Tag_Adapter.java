package com.brainlines.weightloggernewversion.Production.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class Match_Clone_Tag_Adapter extends RecyclerView.Adapter<Match_Clone_Tag_Adapter.MyViewHolder> {

    Context context;
    ArrayList<MatchCloneTagModel> matchCloneTagModels = new ArrayList<>();
    MatchCloneTagListener listener;

    public Match_Clone_Tag_Adapter(Context context, ArrayList<MatchCloneTagModel> matchCloneTagModels, MatchCloneTagListener listener) {
        this.context = context;
        this.matchCloneTagModels = matchCloneTagModels;
        this.listener = listener;
    }

    @NonNull
    @Override
    public Match_Clone_Tag_Adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.iten_match_with_clone_tag,parent,false);
        return new Match_Clone_Tag_Adapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final MatchCloneTagModel model = matchCloneTagModels.get(position);
        if (model != null){
            holder.txt_Logger_name.setText(model.getProductionTag());
            holder.cv_LoggerName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.tagClick(model);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return matchCloneTagModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_Logger_name;
        CardView cv_LoggerName;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_Logger_name = (itemView).findViewById(R.id.txt_pro_Logger_name);
            cv_LoggerName = (itemView).findViewById(R.id.cv_LoggerName);
        }
    }
}
