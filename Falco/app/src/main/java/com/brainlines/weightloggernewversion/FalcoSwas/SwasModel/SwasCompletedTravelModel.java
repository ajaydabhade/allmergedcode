package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

public class SwasCompletedTravelModel {
    String TravellingExpensesHeadsID,ServiceCallID,OEMID,CustomerID,Service_Expense_Heads_id,Service_Expense_Heads_Name,VisitDates,VisitCharges,CreatedBy,CreatedDate;

    public String getTravellingExpensesHeadsID() {
        return TravellingExpensesHeadsID;
    }

    public void setTravellingExpensesHeadsID(String travellingExpensesHeadsID) {
        TravellingExpensesHeadsID = travellingExpensesHeadsID;
    }

    public String getServiceCallID() {
        return ServiceCallID;
    }

    public void setServiceCallID(String serviceCallID) {
        ServiceCallID = serviceCallID;
    }

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getService_Expense_Heads_id() {
        return Service_Expense_Heads_id;
    }

    public void setService_Expense_Heads_id(String service_Expense_Heads_id) {
        Service_Expense_Heads_id = service_Expense_Heads_id;
    }

    public String getService_Expense_Heads_Name() {
        return Service_Expense_Heads_Name;
    }

    public void setService_Expense_Heads_Name(String service_Expense_Heads_Name) {
        Service_Expense_Heads_Name = service_Expense_Heads_Name;
    }

    public String getVisitDates() {
        return VisitDates;
    }

    public void setVisitDates(String visitDates) {
        VisitDates = visitDates;
    }

    public String getVisitCharges() {
        return VisitCharges;
    }

    public void setVisitCharges(String visitCharges) {
        VisitCharges = visitCharges;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }
}
