package com.brainlines.weightloggernewversion.FalcoJK.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.PacketTicketTrackingModel;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.ViewTicketTableAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.UrlConstants.JK_URL_Constants;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class JKViewTicketActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    RecyclerView grid_table_details;
    ViewTicketTableAdapter adapter1;
    String str_tk_id,str_tk_no,str_tk_startdate,str_tk_end_date,str_tk_qty,str_plant,str_sku;
    TextView txt_up_ticket_no,txt_up_plant,txt_up_tk_qty,txt_sku_13,txt_up_startdate,txt_up_enddate,txt_up_act_strat_date,txt_up_act_end_date;
    ImageView img_back,img_userinfo,nav_view_img;
    DrawerLayout drawer;
    String user_email,user_role,user_token;
    private TextView profile_user,profile_role;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_packing_ticket_view_navigation);
        grid_table_details=(RecyclerView) findViewById(R.id.grid_table_contents);
        initUi();
        setData();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_swas).setVisible(false);
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img=(ImageView)findViewById(R.id.nav_view_img);
        Bundle bundle=getIntent().getExtras();
        str_tk_id=bundle.getString("tk_id");
        str_tk_no=bundle.getString("tk_no");
        str_tk_startdate=bundle.getString("tk_start_date");
        str_tk_end_date=bundle.getString("tk_end_date");
        str_tk_qty=bundle.getString("tk_quantity");
        str_sku=bundle.getString("tk_sku_13");
        str_plant=bundle.getString("tk_plant");

        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }
        });


        //str_plant=bundle.getString("tk_plant");
        txt_up_ticket_no.setText(str_tk_no);
        txt_up_plant.setText(str_plant);
        txt_up_tk_qty.setText(str_tk_qty.concat(" no"));
        txt_up_enddate.setText(str_tk_end_date);
        txt_up_startdate.setText(str_tk_startdate);
        txt_sku_13.setText(str_sku);
        txt_up_plant.setText(str_plant);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(JKViewTicketActivity.this, JKPackingTicketSelection.class);
                startActivity(i);
            }
        });
        img_userinfo.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        new GetTicketDetailsCall().execute();
    }

    public void initUi()
    {
        txt_up_startdate=(TextView)findViewById(R.id.txt_up_startdate);
        txt_up_ticket_no=(TextView)findViewById(R.id.txt_up_ticket_no);
        txt_up_enddate=(TextView)findViewById(R.id.txt_up_enddate);
        txt_up_tk_qty=(TextView)findViewById(R.id.txt_up_tk_qty);
        txt_up_plant=(TextView)findViewById(R.id.txt_up_plant);
        txt_sku_13=(TextView)findViewById(R.id.txt_sku_13);
        txt_up_act_strat_date=(TextView)findViewById(R.id.txt_up_act_strat_date);
        txt_up_act_end_date=(TextView)findViewById(R.id.txt_up_act_end_date);
        img_back=(ImageView)findViewById(R.id.img_back);
        img_userinfo=(ImageView)findViewById(R.id.img_userinfo);
    }

    public class GetTicketDetailsCall extends AsyncTask<String,String,String>
    {
        public  final String TAG = "URL_Call";
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JKViewTicketActivity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String url = null;
            url= JK_URL_Constants.MAIN_JK_URL+ JK_URL_Constants.GET_TICKET_DETAILS + str_tk_id;
            response= URL_Constants.makeHttpPutRequest(url);
            Log.d(TAG, "Whole data = " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray array=jsonObject.getJSONArray("Data");
                    if (array.length()==0){
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                    else {
                        ArrayList<PacketTicketTrackingModel> ticketmodel= new ArrayList<>();
                        for (int i=0;i<array.length();i++) {
                            JSONObject obj = new JSONObject(array.get(i).toString());
                            PacketTicketTrackingModel model = new PacketTicketTrackingModel();

                            model.setTt_Plant(obj.getString("tt_Plant"));
                            model.setTt_Date(obj.getString("tt_Date"));
                            model.setTt_TotalQuantity(obj.getString("tt_TotalQuantity"));
                            model.setTt_Qty(obj.getString("tt_Qty"));
                            model.setTt_PendingQty(obj.getString("tt_PendingQty"));
                            model.setTt_CreatedDate(obj.getString("tt_CreatedDate"));
                            model.setTt_CreatedBy(obj.getString("tt_CreatedBy"));
                            model.setTt_ApprovedDate(obj.getString("tt_ApprovedDate"));
                            model.setTt_ApprovedBy(obj.getString("tt_ApprovedBy"));
                            model.setTt_Active(obj.getString("tt_Active"));
                            model.setTt_Approve(obj.getString("tt_Approve"));
                            model.setFlag1(obj.getString("flag1"));
                            model.setTk_SKU(obj.getString("tk_SKU"));
                            model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                            model.setTk_End_Date(obj.getString("tk_End_Date"));
                            model.setActualstartdate(obj.getString("actualstartdate"));
                            model.setToday(obj.getString("today"));

                            if(obj.has("actualenddate"))
                            {
                                model.setActualenddate(obj.getString("actualenddate"));
                                String enddate=obj.getString("actualenddate");
                                if (enddate.equals("null"))
                                {
                                    txt_up_act_end_date.setText("NA");
                                }
                                else
                                {
                                    txt_up_act_end_date.setText(enddate);
                                }


                            }
                            else
                            {
                                model.setActualenddate("null");
                                String enddate="null";
                                if (enddate.equals("null"))
                                {
                                    txt_up_act_end_date.setText("NA");
                                }
                                else
                                {
                                    txt_up_act_end_date.setText(enddate);
                                }

                            }
                            model.setToday(obj.getString("today"));
                            txt_up_act_strat_date.setText(obj.getString("actualstartdate"));
                            txt_up_act_end_date.setText(obj.getString("actualenddate"));
                            ticketmodel.add(model);

                        }
                        adapter1=new ViewTicketTableAdapter(JKViewTicketActivity.this,ticketmodel);
                        grid_table_details.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL, false));
                        grid_table_details.setAdapter(adapter1);

                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(JKViewTicketActivity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);
        setData();
        // Set content width and height
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(JKViewTicketActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }


    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(JKViewTicketActivity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();
                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout()
    {
        finish();
        Toast toast= Toast.makeText(JKViewTicketActivity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(JKViewTicketActivity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        Intent i=new Intent(JKViewTicketActivity.this, JKPackingTicketSelection.class);
        startActivity(i);
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id== R.id.nav_planning)
        {
            Intent i=new Intent(JKViewTicketActivity.this, FalcoJKHomeActivity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }
}
