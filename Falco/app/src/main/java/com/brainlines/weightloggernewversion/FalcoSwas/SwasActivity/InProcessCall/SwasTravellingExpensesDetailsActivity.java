package com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.InProcessCall;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Feedback_Activity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Home_Actvity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter.SwasCompletedTravelAdapter;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter.SwasTravellingExpensesAdapter;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasListener.TravelExpensesListener;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompletedTravelModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasIdDetailsModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasTravellingExpensesModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.TravelExpensesKeyValueModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.android.material.navigation.NavigationView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants.convertStreamToString;

public class SwasTravellingExpensesDetailsActivity extends AppCompatActivity implements TravelExpensesListener, NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "SwasTravellingExpensesD";
    ImageButton btnSubTravel,btnFeedback,btn_travel_list_display;
    TextView txt_travel_service_call_id,travel_service_call_id;
    RecyclerView rv_travelling_expenses;
    SwasTravellingExpensesAdapter adapter;
    SwasCompletedTravelAdapter completedTravelAdapter;
    EditText ed_travel_date;
    String oem_id,visit_Date,service_call_id,created_by,customer_id,status_id,type_of_Service_Call_id;
    int yearr, month, dayofmonth;
    ArrayList<TravelExpensesKeyValueModel> keyValueModelArrayList = new ArrayList<>();
    ArrayList<SwasTravellingExpensesModel> model = new ArrayList<>();
    RecyclerView alert_rv_list;
    ImageView img_user_info,nav_view_img,img_back;
    String user_email,user_token,user_role;
    DrawerLayout drawer;
    TextView profile_user,profile_role;
    String Created_by="",service_eng_id;
    private DataBaseHelper helper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_swas_inprocess_travel_expenses_navigation);
        initUi();
        setData();
        img_back=(ImageView)findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SwasTravellingExpensesDetailsActivity.this, Swas_Deatails_of_Inprocess_Call_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_Service_Call_id);
                bundle.putString("customer_id",customer_id);
                i.putExtras(bundle);
                startActivity(i);
                finish();
            }
        });
        if (user_email.equals("yogesh@brainlines.in"))
        {
            created_by = "Yogesh Jadhav";
//            service_eng_id = "1";
        }
        else
        {
            created_by = "Deepak Kulkarni";
//            service_eng_id = "2";
        }
        img_user_info=(ImageView)findViewById(R.id.img_userinfo);
        img_user_info.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img=(ImageView)findViewById(R.id.nav_view_img);

        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_planning).setVisible(false);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
        {
            status_id = bundle.getString("status_id");
            service_call_id = bundle.getString("service_call_id");
            type_of_Service_Call_id = bundle.getString("Typeofservicecall_id");
            customer_id = bundle.getString("customer_id");
            String customerName = bundle.getString("customerName");
            travel_service_call_id.setText("SCID#"+service_call_id+"  ");
            txt_travel_service_call_id.setText("("+customer_id+") "+customerName);
        }
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
//        if(isConnected) {
//            Log.d("Network", "Connected");
//            Id_Details();
////            travelling_expenses_details();
//            showTravellingExpensesFromDB();
//        }
//        else{
//            checkNetworkConnection();
//            Log.d("Network","Not Connected");
//            showTravellingExpensesFromDB();
//        }
        showTravellingExpensesFromDB();

        ed_travel_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar myCalendar = Calendar.getInstance();
                DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        yearr = Calendar.YEAR;
                        month = Calendar.MONTH;
                        dayofmonth = Calendar.DAY_OF_MONTH;
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "yyyy-MM-dd"; // your format
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                        ed_travel_date.setText(sdf.format(myCalendar.getTime()));

                    }

                };
                DatePickerDialog datePickerDialog=new DatePickerDialog(SwasTravellingExpensesDetailsActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));

                //following line to restrict future date selection
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
/*
                new DatePickerDialog(SwasTravellingExpensesDetailsActivity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
*/
            }
        });
        visit_Date = ed_travel_date.getText().toString();

        btnSubTravel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(ed_travel_date.getText().toString()))
                {
                    ed_travel_date.setError("Please select the date");
                }
                else
                {
                    ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                    boolean isConnected = activeNetwork != null &&
                            activeNetwork.isConnected();
                    if(isConnected) {
                        new SaveTravellingExpenses().execute();
                    }else {
                        saveTravellingExpensesToLocalDB();
                    }
                }
            }
        });
        btnFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SwasTravellingExpensesDetailsActivity.this, Swas_Feedback_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_Service_Call_id);
                bundle.putString("customer_id",customer_id);
                i.putExtras(bundle);
                startActivity(i);
            }
        });

        btn_travel_list_display.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnected();
                if(isConnected) {
                    Log.d("Network", "Connected");
                    listDisplay();
                }
                else{
                    checkNetworkConnection();
                    Log.d("Network","Not Connected");
                    listDisplay1();

                }
            }
        });

    }

    private void showTravellingExpensesFromDB() {
        Cursor cursor = helper.getDataFromExpenseDetailsTableFromLocalDB(service_call_id,customer_id);
        while (cursor.moveToNext()){
            String expenseHeadsID = cursor.getString(0);
            String expenseHeadsName = cursor.getString(1);
            String customerId = cursor.getString(2);
            SwasTravellingExpensesModel swastravellingModel = new SwasTravellingExpensesModel();
            swastravellingModel.setTravelling_expenses_Id(expenseHeadsID);
            swastravellingModel.setTravelling_expenses_name(expenseHeadsName);
            swastravellingModel.setCustomerID(customer_id);
            swastravellingModel.setCreatedBy(created_by);
            swastravellingModel.setOEMID(oem_id);
            swastravellingModel.setVisitDates(visit_Date);
            swastravellingModel.setVisit_charge("0");
            swastravellingModel.setServiceCallID(service_call_id);
            model.add(swastravellingModel);
        }
        adapter = new SwasTravellingExpensesAdapter(SwasTravellingExpensesDetailsActivity.this, model,SwasTravellingExpensesDetailsActivity.this,created_by,visit_Date,oem_id,service_call_id,customer_id);
        rv_travelling_expenses.setLayoutManager(new LinearLayoutManager(SwasTravellingExpensesDetailsActivity.this, LinearLayoutManager.VERTICAL, false));
        rv_travelling_expenses.setAdapter(adapter);
    }

    private void saveTravellingExpensesToLocalDB() {
//        for (int i = 0 ; i < model.size() ; i++){
//            SwasTravellingExpensesModel swtModel = model.get(i);
//            SQLiteDatabase db1 = helper.getWritableDatabase();
//            ContentValues cv = new ContentValues();
//            cv.put(Constants.EXPENSE_DETAILS_TRAVELLING_EXPENSES_HEADS_ID,swtModel.getTravelling_expenses_Id());
//            cv.put(Constants.EXPENSE_DETAILS_SERVICE_CALL_ID,swtModel.getServiceCallID());
//            cv.put(Constants.EXPENSE_DETAILS_OEMID,swtModel.getOEMID());
//            cv.put(Constants.EXPENSE_DETAILS_CUSTOMER_ID,swtModel.getCustomerID());
//            cv.put(Constants.EXPENSE_DETAILS_SERVICE_EXPENSE_HEADS_ID,swtModel.getTravelling_expenses_Id());
//            cv.put(Constants.EXPENSE_DETAILS_SERVICE_EXPENSE_HEADS_NAME,swtModel.getTravelling_expenses_name());
//            cv.put(Constants.EXPENSE_DETAILS_VISIT_DATES,swtModel.getVisitDates());
//            cv.put(Constants.EXPENSE_DETAILS_VISIT_CHARGES_RS,swtModel.getVisit_charge());
//            cv.put(Constants.EXPENSE_DETAILS_CREATED_BY,created_by);
//            cv.put(Constants.EXPENSE_DETAILS_CREATED_DATE,"");
//            cv.put(Constants.EXPENSE_DETAILS_IS_SYNC,0);
//            long res = db1.insert(Constants.TABLE_EXPENSE_DETAILS,null,cv);
//            Log.d(TAG, "saveTravellingExpensesToLocalDB: ");
//
//        }
        for (int i = 0 ; i < model.size() ; i++){
            SwasTravellingExpensesModel swasTravellingExpensesModel = model.get(i);
            helper.updateTravellingExpenseTableSWAS(
                    swasTravellingExpensesModel.getServiceCallID(),
                    swasTravellingExpensesModel.getCustomerID(),
                    ed_travel_date.getText().toString(),
                    swasTravellingExpensesModel.getVisit_charge(),
                    swasTravellingExpensesModel.getCreatedBy(),
                    ed_travel_date.getText().toString()
            );
        }
    }

    public void initUi() {
        helper = new DataBaseHelper(this);
        btnSubTravel=findViewById(R.id.btn_travel_submit);
        btnFeedback=findViewById(R.id.btn_travel_feedback);
        rv_travelling_expenses=(RecyclerView)findViewById(R.id.rv_travelling_expenses);
        ed_travel_date = (EditText)findViewById(R.id.ed_travel_date);
        txt_travel_service_call_id = (TextView)findViewById(R.id.txt_travel_service_call_id);
        travel_service_call_id = (TextView)findViewById(R.id.travel_service_call_id);
        btn_travel_list_display = findViewById(R.id.btn_travel_list_display);
    }

    private void travelling_expenses_details() {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getexpenseheadslist1(customer_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {

                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");

                            if (array.length() != 0) {

                                for (int i = 0; i < array.length(); i++) {
                                    SwasTravellingExpensesModel swastravellingModel = new SwasTravellingExpensesModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    swastravellingModel.setTravelling_expenses_Id(object1.getString("Service_Expense_Heads_id"));
                                    swastravellingModel.setTravelling_expenses_name(object1.getString("Service_Expense_Heads_Name"));
                                    swastravellingModel.setCustomerID(customer_id);
                                    swastravellingModel.setCreatedBy(created_by);
                                    swastravellingModel.setOEMID(oem_id);
                                    swastravellingModel.setVisitDates(visit_Date);
                                    swastravellingModel.setVisit_charge("0");
                                    swastravellingModel.setServiceCallID(service_call_id);
                                    model.add(swastravellingModel);
                                }
                                ArrayList<SwasTravellingExpensesModel> list = model;
                                adapter = new SwasTravellingExpensesAdapter(SwasTravellingExpensesDetailsActivity.this, model,SwasTravellingExpensesDetailsActivity.this,created_by,visit_Date,oem_id,service_call_id,customer_id);
                                rv_travelling_expenses.setLayoutManager(new LinearLayoutManager(SwasTravellingExpensesDetailsActivity.this, LinearLayoutManager.VERTICAL, false));
                                rv_travelling_expenses.setAdapter(adapter);

                            } else if (array.length() == 0) {
                            }
                        }
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(SwasTravellingExpensesDetailsActivity.this, "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void listDisplay() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.swas_alert_travel_expenses_list, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        alert_rv_list = (RecyclerView)dialogView.findViewById(R.id.rv_alert_travel_details);
        alertlist_parameter();
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
    private void listDisplay1() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.swas_alert_travel_expenses_list, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        alert_rv_list = (RecyclerView)dialogView.findViewById(R.id.rv_alert_travel_details);
        //alertlist_parameter();
        getDataFromLocalDB();
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void getDataFromLocalDB() {
        ArrayList<SwasCompletedTravelModel> model = new ArrayList<>();
        Cursor cursor = new DataBaseHelper(this).getExpenseDetailsFromLocalDbAgainsterviceCallID(service_call_id);
        while (cursor.moveToNext()){
            String id = cursor.getString(1);
            String serviceCallID = cursor.getString(2);
            String oemid = cursor.getString(3);
            String custid = cursor.getString(4);
            String serviceExpenseHeadsid = cursor.getString(5);
            String serviceExpenseHeadsName = cursor.getString(6);
            String visitDates = cursor.getString(7);
            String vistCharges = cursor.getString(8);
            String createdBy = cursor.getString(9);
            String createdDate = cursor.getString(10);
            SwasCompletedTravelModel travelModel = new SwasCompletedTravelModel();
            travelModel.setTravellingExpensesHeadsID(id);
            travelModel.setServiceCallID(serviceCallID);
            travelModel.setOEMID(oemid);
            travelModel.setCustomerID(custid);
            travelModel.setService_Expense_Heads_id(serviceExpenseHeadsid);
            travelModel.setService_Expense_Heads_Name(serviceExpenseHeadsName);
            travelModel.setVisitDates(visitDates);
            travelModel.setVisitCharges(vistCharges);
            travelModel.setCreatedBy(createdBy);
            travelModel.setCreatedDate(createdDate);

            //txt_travel_service_call_id.setText(object1.getString("ServiceCallID"));
            //swasServiceModel .setServicecall_status_id(object1.getString("ServiceCallStatusID"));

            model.add(travelModel);
        }
        completedTravelAdapter = new SwasCompletedTravelAdapter(SwasTravellingExpensesDetailsActivity.this,model);
        alert_rv_list.setLayoutManager(new LinearLayoutManager(SwasTravellingExpensesDetailsActivity.this, LinearLayoutManager.VERTICAL, false));
        alert_rv_list.setAdapter(completedTravelAdapter);
    }

    @Override
    public void gettravel_expenses(TravelExpensesKeyValueModel model) {
        if (model!=null)
        {
            keyValueModelArrayList.add(model);

        }
    }


    private void Id_Details() {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getalldetailsofsingleservicecall(service_call_id,status_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {

                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            ArrayList<SwasIdDetailsModel> model = new ArrayList<>();
                            if (array.length() != 0) {

                                for (int i = 0; i < array.length(); i++) {
                                    SwasIdDetailsModel swasIdDetailsModel = new SwasIdDetailsModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    service_call_id=object1.getString("ServiceCallID");
                                    created_by = object1.getString("CreatedBy");
                                    customer_id = object1.getString("CustomerID");
                                    txt_travel_service_call_id.setText(service_call_id);

                                }
                            }
                            else if (array.length() == 0) {
                            }
                        }
                    }


                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public class SaveTravellingExpenses extends AsyncTask<String,String,String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SwasTravellingExpensesDetailsActivity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String getResponse="";
            visit_Date = ed_travel_date.getText().toString();

            String url= Retroconfig.BASEURL_SWAS + "insertravellingexpensedetails";

            try {
                HttpParams httpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(httpParams, 60000);
                HttpConnectionParams.setSoTimeout(httpParams, 60000);
                HttpClient client = new DefaultHttpClient(httpParams);
                HttpPost httpPost = new HttpPost(url);

                httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
                JSONArray array=new JSONArray();

                for (int i= 0;i<model.size();i++)
                {
                    try {
                        JSONObject object=new JSONObject();
                        object.put("ServiceCallID",model.get(i).getServiceCallID());
                        object.put("OEMID",model.get(i).getOEMID());
                        object.put("CustomerID",model.get(i).getCustomerID());
                        object.put("Service_Expense_Heads_id",model.get(i).getTravelling_expenses_Id());
                        object.put("VisitDates",visit_Date);
                        if (model.get(i).getTravelling_expenses_Id().equals("7"))
                        {
                            object.put("VisitCharges","300");
                        }
                        else
                        {
                            object.put("VisitCharges",model.get(i).getVisit_charge());
                        }

                        object.put("CreatedBy",created_by);
                        array.put(object);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }


                httpPost.setEntity(new StringEntity(array.toString(),"UTF-8"));
                httpPost.setParams(httpParams);

                HttpResponse response = client.execute(httpPost);
                StatusLine statusLine = response.getStatusLine();

                if (statusLine.getStatusCode() == HttpStatus.SC_OK || statusLine.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();

                    getResponse = convertStreamToString(inputStream);
                }
                else if (statusLine.getStatusCode()==HttpStatus.SC_UNAUTHORIZED)
                {
                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();

                    getResponse = convertStreamToString(inputStream);
                }
                else {
                    response.getEntity().getContent().close();
                }
            }

            catch (Exception e) {
                Log.i(URL_Constants.TAG, "Error Message in Exception :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_EXCEPTION;

            }
            return getResponse;
        }

        @Override
        protected void onPostExecute(String getResponse) {
            super.onPostExecute(getResponse);
            progressDialog.dismiss();
            if (!getResponse.equals("Error"))
            {
                Toast.makeText(SwasTravellingExpensesDetailsActivity.this, "Travelling expenses inserted successfully", Toast.LENGTH_SHORT).show();
                model.clear();
               /* model.clear();
                travelling_expenses_details();*/
                Intent i = new Intent( SwasTravellingExpensesDetailsActivity.this,SwasTravellingExpensesDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_Service_Call_id);
                bundle.putString("customer_id",customer_id);
                i.putExtras(bundle);
                startActivity(i);
            }
        }
    }

    private void alertlist_parameter() {
        API api = Retroconfig.swasretrofit().create(API.class);
        /*dateformat1 = "2020-01-20";*/
        Call<ResponseBody> call = api.gettravellingexpensesdetails(service_call_id,customer_id,oem_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {

                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");


                            ArrayList<SwasCompletedTravelModel> model = new ArrayList<>();

                            if (array.length() != 0) {

                                for (int i = 0; i < array.length(); i++) {
                                    SwasCompletedTravelModel travelModel = new SwasCompletedTravelModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    travelModel.setTravellingExpensesHeadsID(object1.getString("TravellingExpensesHeadsID"));
                                    travelModel.setServiceCallID(object1.getString("ServiceCallID"));
                                    travelModel.setOEMID(object1.getString("OEMID"));
                                    travelModel.setCustomerID(object1.getString("CustomerID"));
                                    travelModel.setService_Expense_Heads_id(object1.getString("Service_Expense_Heads_id"));
                                    travelModel.setService_Expense_Heads_Name(object1.getString("Service_Expense_Heads_Name"));
                                    travelModel.setVisitDates(object1.getString("VisitDates"));
                                    travelModel.setVisitCharges(object1.getString("VisitCharges"));
                                    travelModel.setCreatedBy(object1.getString("CreatedBy"));
                                    travelModel.setCreatedDate(object1.getString("CreatedDate"));

                                    model.add(travelModel);

                                }
                                completedTravelAdapter = new SwasCompletedTravelAdapter(SwasTravellingExpensesDetailsActivity.this,model);
                                alert_rv_list.setLayoutManager(new LinearLayoutManager(SwasTravellingExpensesDetailsActivity.this, LinearLayoutManager.VERTICAL, false));
                                alert_rv_list.setAdapter(completedTravelAdapter);

                            }
                            else if (array.length() == 0) {
                            }
                        }
                    }


                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(SwasTravellingExpensesDetailsActivity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);

        // Set content width and height
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(SwasTravellingExpensesDetailsActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }


    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(SwasTravellingExpensesDetailsActivity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();

                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    oem_id= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));
                    service_eng_id= cursor.getString(cursor.getColumnIndex(Constants.USER_LOGIN_ID));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));


                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout()
    {
        finish();
        Toast toast= Toast.makeText(SwasTravellingExpensesDetailsActivity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(SwasTravellingExpensesDetailsActivity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);

        //Preferences.Logout();

    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();


        if (id==R.id.nav_swas)
        {
            Intent i=new Intent(SwasTravellingExpensesDetailsActivity.this, Swas_Home_Actvity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    public void checkNetworkConnection(){
        AlertDialog.Builder builder =new AlertDialog.Builder(SwasTravellingExpensesDetailsActivity.this);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent i= new Intent(SwasTravellingExpensesDetailsActivity.this,Swas_Deatails_of_Inprocess_Call_Activity.class);
        Bundle bundle = new Bundle();
        bundle.putString("service_call_id",service_call_id);
        bundle.putString("status_id",status_id);
        bundle.putString("Typeofservicecall_id",type_of_Service_Call_id);
        bundle.putString("customer_id",customer_id);
        i.putExtras(bundle);
        startActivity(i);
        finish();
    }

}
