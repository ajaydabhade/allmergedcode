package com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasListener.TravelExpensesListener;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasTravellingExpensesModel;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class SwasTravellingExpensesAdapter extends RecyclerView.Adapter<SwasTravellingExpensesAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<SwasTravellingExpensesModel> travel_list;
    View.OnClickListener mClickListener;
    Context context;
    String stredexpensesvalue,id;
    TravelExpensesListener travelExpensesListener;
    String visit_date,created_by,oem_id,service_call_id,customer_id;



    public SwasTravellingExpensesAdapter(Context ctx, ArrayList< SwasTravellingExpensesModel> travel_list,TravelExpensesListener travelExpensesListener,String created_by,String visit_date,String oem_id,String service_call_id,String customer_id){
        inflater = LayoutInflater.from(ctx);
        this.travel_list = travel_list;
        this.context=ctx;
        this.travelExpensesListener = travelExpensesListener;
        this.visit_date = visit_date;
        this.created_by = created_by;
        this.oem_id = oem_id;
        this.service_call_id = service_call_id;
        this.customer_id = customer_id;
    }

    @Override
    public int getItemCount() {
        return travel_list.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_travelling_expenses_list, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int i) {
        final  SwasTravellingExpensesModel model = travel_list.get(i);
        if (model!=null) {

            holder.txt_travelling_expenses.setText(model. getTravelling_expenses_name());
            if (model.getTravelling_expenses_Id().equals("7"))
            {
                holder.edExpensesValue.setText("300");
                holder.edExpensesValue.setEnabled(false);
            }

            holder.edExpensesValue.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                }

                @Override
                public void afterTextChanged(Editable s) {
                    String stredexpensesvalue = s.toString();
                    if (s!=null){

                        if (model.getTravelling_expenses_Id().equals("7"))
                        {
                            travel_list.get(i).setVisit_charge("300");
                        }
                        else
                        {
                            travel_list.get(i).setVisit_charge(stredexpensesvalue);
                        }

                    }
                    else if (s==null){

                    }


                }

            });

        }




    }



    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txt_travelling_expenses;
        EditText edExpensesValue;


        public MyViewHolder(View itemView) {
            super(itemView);
            txt_travelling_expenses=itemView.findViewById(R.id.txt_travelling_Expenes);
            edExpensesValue = itemView.findViewById(R.id.edExpensesValue);


        }
    }


}