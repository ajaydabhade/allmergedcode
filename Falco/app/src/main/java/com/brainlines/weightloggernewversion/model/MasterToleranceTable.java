package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MasterToleranceTable implements Parcelable {

    String tolerance_id;
    int oemId;
    String productType;
    int targetWeight;
    int minTargetWeight;
    int maxTargetWeight;
    int minWeightSamples;
    int minPaperShiftSamples;
    int paperShiftTolerance;
    int minBagLengthSamples;
    int bagLengthTolerance;
    boolean isactive;
    boolean isdeleted;
    String createdDate;
    String modifiedDate;
    String createdBy;
    String modifiedBy;
    int loginId;
    int userId;
    int roleId;
    String userName;
    String password;
    String PositiveTolerance;
    String NegativeTolerance;

    protected MasterToleranceTable(Parcel in) {
        tolerance_id = in.readString();
        oemId = in.readInt();
        productType = in.readString();
        targetWeight = in.readInt();
        minTargetWeight = in.readInt();
        maxTargetWeight = in.readInt();
        minWeightSamples = in.readInt();
        minPaperShiftSamples = in.readInt();
        paperShiftTolerance = in.readInt();
        minBagLengthSamples = in.readInt();
        bagLengthTolerance = in.readInt();
        isactive = in.readByte() != 0;
        isdeleted = in.readByte() != 0;
        createdDate = in.readString();
        modifiedDate = in.readString();
        createdBy = in.readString();
        modifiedBy = in.readString();
        loginId = in.readInt();
        userId = in.readInt();
        roleId = in.readInt();
        userName = in.readString();
        password = in.readString();
        PositiveTolerance = in.readString();
        NegativeTolerance = in.readString();
    }

    public static final Creator<MasterToleranceTable> CREATOR = new Creator<MasterToleranceTable>() {
        @Override
        public MasterToleranceTable createFromParcel(Parcel in) {
            return new MasterToleranceTable(in);
        }

        @Override
        public MasterToleranceTable[] newArray(int size) {
            return new MasterToleranceTable[size];
        }
    };

    public String getTolerance_id() {
        return tolerance_id;
    }

    public void setTolerance_id(String tolerance_id) {
        this.tolerance_id = tolerance_id;
    }

    public int getOemId() {
        return oemId;
    }

    public void setOemId(int oemId) {
        this.oemId = oemId;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public int getTargetWeight() {
        return targetWeight;
    }

    public void setTargetWeight(int targetWeight) {
        this.targetWeight = targetWeight;
    }

    public int getMinTargetWeight() {
        return minTargetWeight;
    }

    public void setMinTargetWeight(int minTargetWeight) {
        this.minTargetWeight = minTargetWeight;
    }

    public int getMaxTargetWeight() {
        return maxTargetWeight;
    }

    public void setMaxTargetWeight(int maxTargetWeight) {
        this.maxTargetWeight = maxTargetWeight;
    }

    public int getMinWeightSamples() {
        return minWeightSamples;
    }

    public void setMinWeightSamples(int minWeightSamples) {
        this.minWeightSamples = minWeightSamples;
    }

    public int getMinPaperShiftSamples() {
        return minPaperShiftSamples;
    }

    public void setMinPaperShiftSamples(int minPaperShiftSamples) {
        this.minPaperShiftSamples = minPaperShiftSamples;
    }

    public int getPaperShiftTolerance() {
        return paperShiftTolerance;
    }

    public void setPaperShiftTolerance(int paperShiftTolerance) {
        this.paperShiftTolerance = paperShiftTolerance;
    }

    public int getMinBagLengthSamples() {
        return minBagLengthSamples;
    }

    public void setMinBagLengthSamples(int minBagLengthSamples) {
        this.minBagLengthSamples = minBagLengthSamples;
    }

    public int getBagLengthTolerance() {
        return bagLengthTolerance;
    }

    public void setBagLengthTolerance(int bagLengthTolerance) {
        this.bagLengthTolerance = bagLengthTolerance;
    }

    public boolean isIsactive() {
        return isactive;
    }

    public void setIsactive(boolean isactive) {
        this.isactive = isactive;
    }

    public boolean isIsdeleted() {
        return isdeleted;
    }

    public void setIsdeleted(boolean isdeleted) {
        this.isdeleted = isdeleted;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public int getLoginId() {
        return loginId;
    }

    public void setLoginId(int loginId) {
        this.loginId = loginId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPositiveTolerance() {
        return PositiveTolerance;
    }

    public void setPositiveTolerance(String positiveTolerance) {
        PositiveTolerance = positiveTolerance;
    }

    public String getNegativeTolerance() {
        return NegativeTolerance;
    }

    public void setNegativeTolerance(String negativeTolerance) {
        NegativeTolerance = negativeTolerance;
    }

    public MasterToleranceTable(String tolerance_id, int oemId, String productType, int targetWeight, int minTargetWeight, int maxTargetWeight, int minWeightSamples, int minPaperShiftSamples, int paperShiftTolerance, int minBagLengthSamples, int bagLengthTolerance, boolean isactive, boolean isdeleted, String createdDate, String modifiedDate, String createdBy, String modifiedBy, int loginId, int userId, int roleId, String userName, String password) {
        this.tolerance_id = tolerance_id;
        this.oemId = oemId;
        this.productType = productType;
        this.targetWeight = targetWeight;
        this.minTargetWeight = minTargetWeight;
        this.maxTargetWeight = maxTargetWeight;
        this.minWeightSamples = minWeightSamples;
        this.minPaperShiftSamples = minPaperShiftSamples;
        this.paperShiftTolerance = paperShiftTolerance;
        this.minBagLengthSamples = minBagLengthSamples;
        this.bagLengthTolerance = bagLengthTolerance;
        this.isactive = isactive;
        this.isdeleted = isdeleted;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.createdBy = createdBy;
        this.modifiedBy = modifiedBy;
        this.loginId = loginId;
        this.userId = userId;
        this.roleId = roleId;
        this.userName = userName;
        this.password = password;

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(tolerance_id);
        dest.writeInt(oemId);
        dest.writeString(productType);
        dest.writeInt(targetWeight);
        dest.writeInt(minTargetWeight);
        dest.writeInt(maxTargetWeight);
        dest.writeInt(minWeightSamples);
        dest.writeInt(minPaperShiftSamples);
        dest.writeInt(paperShiftTolerance);
        dest.writeInt(minBagLengthSamples);
        dest.writeInt(bagLengthTolerance);
        dest.writeByte((byte) (isactive ? 1 : 0));
        dest.writeByte((byte) (isdeleted ? 1 : 0));
        dest.writeString(createdDate);
        dest.writeString(modifiedDate);
        dest.writeString(createdBy);
        dest.writeString(modifiedBy);
        dest.writeInt(loginId);
        dest.writeInt(userId);
        dest.writeInt(roleId);
        dest.writeString(userName);
        dest.writeString(password);
        dest.writeString(PositiveTolerance);
        dest.writeString(NegativeTolerance);
    }
}
