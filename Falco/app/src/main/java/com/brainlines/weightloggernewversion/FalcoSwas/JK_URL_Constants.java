package com.brainlines.weightloggernewversion.FalcoSwas;

public class JK_URL_Constants {

    public static final String TAG = "URL_Call";
    public static final String MAIN_JK_URL = "http://apps.brainlines.net/JKTicketAPI/api";
    public static final String GET_TICKETS = "/GetPackingUnpackingTickets?TicketType=";
    public static final String START_PACK_TICKET = "/update?TicketIDNo=";
    public static final String GET_TICKET_DETAILS = "/GetPlanningDetails?TicketIDNo=";
    public static final String INSERT_PACKING_UPDATE_TICKET = "/InsertUpdate?tt_id=";
    public static final String RELEASE_PACK_TICKET = "/Release?tk_CreatedBy=";
    public static final String ADD_DAY = "/AddDay?tt_TicketID=";
    public static final String END_PACK_TICKET = "/ReleaseEndDate?tk_Id=";
    public static final String FIRST_TIME_SAVE_PACKUNPACKTK = "/save?tt_TicketID=";
    public static final String GET_PROD_TICKETS = "/GetProductionTickets?plantuser=";
    public static final String START_PROD_TICKET_ID_DETAILS = "/GetProdToday?TicketIDNo=";
    public static final String INSERT_PROD_UPDATE_TICKET = "/InsertUpdateProd?ptk_PtkdId=";
    public static final String RELEASE_PROD_TICKETS = "/ReleaseProductionDate?tk_TicketNo=";
    public static final String END_PROD_TICKET = "/GetEndticket?TicketIDNo=";
    public static final String VIEW_PROD_TICKET = "/GetProductionView?TicketIDNo=";
    public static final String TEST_UPDATE_PRODUCTION_SUBMIT = "/GetTestArray?Array=";
    public static final String GETSKUBLACKIDIZING = "/GetSKUBlackidizing?ticketIDNo=";
    public static final String GET_OPERATION_EQUIPMENT="/GetOperationEquipment?valueStram=";
    public static final String GET_EQUIPMENT_WISE_TYPE="/GetEquipmentWiseType?valueStram=";
    public static final String GET__EQUIPMENT_ASSET_NO="/GetEquipmentAssetNo?valueStram=";
    public static final String SAVE_DOWNTIME_OPERATION="/SaveDownTime?tt_Plant=";

    public static final String PLANT="Chiplun";
    //public static final String PLANT="vapi";
    public static final String PLANT_USER="capacity@decintell.com";



    public static final String MAIN_JK_SWAS_URL = "http://apps.brainlines.net/SWAS-ServiceCallWebAPI/api/servicecall";

    public static final String GET_CALL_STATUS = "/getservicecallstatus";



}