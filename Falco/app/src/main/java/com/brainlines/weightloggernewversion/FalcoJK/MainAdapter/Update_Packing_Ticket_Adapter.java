package com.brainlines.weightloggernewversion.FalcoJK.MainAdapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.StartTicketPackModel;
import com.brainlines.weightloggernewversion.FalcoJK.Listeners.UpdatePackingTicketeListener;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class Update_Packing_Ticket_Adapter extends RecyclerView.Adapter<Update_Packing_Ticket_Adapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<StartTicketPackModel> ticket_list;
    View.OnClickListener mClickListener;
    Context context;
    String str_coml_qty;
    UpdatePackingTicketeListener updatePackingTicketeListener;

    public Update_Packing_Ticket_Adapter(Context ctx, ArrayList<StartTicketPackModel> ticket_list,UpdatePackingTicketeListener updatePackingTicketeListener){
        inflater = LayoutInflater.from(ctx);
        this.ticket_list = ticket_list;
        this.updatePackingTicketeListener = updatePackingTicketeListener;
        this.context=ctx;

    }

    @Override
    public int getItemCount() {
        return ticket_list.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_update_packing_ticket_data, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onClick(view);
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int i) {
        final StartTicketPackModel model = ticket_list.get(i);
        holder.txt_date.setText(ticket_list.get(i).getDay());
        holder.txt_pending_qty.setText(ticket_list.get(i).getTk_Pending());


        holder.txt_compl_qty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                str_coml_qty = s.toString();
                if (s != null) {

                    if (!str_coml_qty.equals("")) {
                        ticket_list.get(i).setStr_compl_qty(str_coml_qty);
                        ticket_list.get(i).setStr_txt_pending_qty(holder.txt_pending_qty.getText().toString());
                    }
                }
            }
        });

        holder.img_btn_action_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                updatePackingTicketeListener.imgactionSubmitIsClicked(model);


            }
        });
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView txt_date,txt_compl_qty,txt_pending_qty;
        ImageButton img_btn_action_submit;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_date= itemView.findViewById(R.id.txtdate);
            txt_compl_qty= itemView.findViewById(R.id.txtcompletedqty1);
            txt_pending_qty= itemView.findViewById(R.id.txtpendingqty1);
            img_btn_action_submit=itemView.findViewById(R.id.img_action_pack_submit);


        }
    }
}
