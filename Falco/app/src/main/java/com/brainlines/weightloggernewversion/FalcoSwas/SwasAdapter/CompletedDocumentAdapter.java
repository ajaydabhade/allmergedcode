package com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasListener.UploadImageDisplayListener;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.DocumentModel;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class CompletedDocumentAdapter extends RecyclerView.Adapter<CompletedDocumentAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    private ArrayList<DocumentModel> documentModelArrayList;
    View.OnClickListener mClickListener;
    Context context;
    UploadImageDisplayListener uploadImageDisplayListener;

    public CompletedDocumentAdapter(Context ctx, ArrayList<DocumentModel> documentModelArrayList, UploadImageDisplayListener uploadImageDisplayListener) {
        inflater = LayoutInflater.from(ctx);
        this.documentModelArrayList = documentModelArrayList;
        this.uploadImageDisplayListener= uploadImageDisplayListener;
        this.context = ctx;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_document_display, parent, false);
       MyViewHolder holder = new MyViewHolder(view);

       return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        final DocumentModel model = documentModelArrayList.get(position);
        if (model!=null) {
            holder.txt_document_name.setText(model.getDocumentName());

            holder.img_view_doc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    uploadImageDisplayListener.displayImage(model);

                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return documentModelArrayList.size();
    }

    class MyViewHolder  extends RecyclerView.ViewHolder{
        TextView txt_document_name;
        ImageButton img_view_doc;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_document_name = itemView.findViewById(R.id.txt_document_name);
            img_view_doc = itemView.findViewById(R.id.img_view_doc);



        }
    }

}
