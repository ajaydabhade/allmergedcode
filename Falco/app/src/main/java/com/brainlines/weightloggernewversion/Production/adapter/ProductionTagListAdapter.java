package com.brainlines.weightloggernewversion.Production.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.Production.Model.ProductionTagHistoryListModel;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.listener.ProductionTagListListener;
import com.brainlines.weightloggernewversion.model.LoggersFromDBModel;

import java.io.IOException;
import java.util.ArrayList;

public class ProductionTagListAdapter extends RecyclerView.Adapter<ProductionTagListAdapter.MyViewHolder> {

    Context context;
    ArrayList<ProductionTagHistoryListModel> p_tag_list = new ArrayList<>();
    ProductionTagListListener listener;

    public ProductionTagListAdapter(Context context, ArrayList<ProductionTagHistoryListModel> p_tag_list, ProductionTagListListener listener) {
        this.context = context;
        this.p_tag_list = p_tag_list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_production_tag_list,parent,false);
        return new ProductionTagListAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final ProductionTagHistoryListModel model = p_tag_list.get(position);
        if (model != null)
        {
            final String tag_name = p_tag_list.get(position).toString();
            holder.txt_production_tag.setText(model.getBatch_id());
            holder.txt_history_gm_pouch.setText(model.getGm_pouch());
            holder.txt_history_no_of_pouches.setText(model.getNo_of_pouches());
            holder.txt_history_product.setText(model.getProduct());
            holder.txt_history_total_weight.setText(model.getTotal_weight());

            holder.cv_history_tag_details.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.isTagClicked(model);

                }
            });
            holder.txt_production_tag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.isTagClicked(model);
                }
            });

            holder.btn_view_details.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.isTagClicked(model);

                }
            });
        }





    }

    @Override
    public int getItemCount() {
        return p_tag_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_production_tag,txt_history_total_weight,txt_history_no_of_pouches,txt_history_gm_pouch,txt_history_product;
        CardView cv_history_tag_details;
        ImageButton btn_view_details;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_production_tag = (itemView).findViewById(R.id.txt_production_tag);
            txt_history_total_weight = (itemView).findViewById(R.id.txt_history_total_weight);
            txt_history_no_of_pouches = (itemView).findViewById(R.id.txt_history_no_of_pouches);
            txt_history_gm_pouch = (itemView).findViewById(R.id.txt_history_gm_pouch);
            txt_history_product = (itemView).findViewById(R.id.txt_history_product);
            cv_history_tag_details = (itemView).findViewById(R.id.cv_history_tag_details);
            btn_view_details = (itemView).findViewById(R.id.img_btn_view_details);
        }
    }
}