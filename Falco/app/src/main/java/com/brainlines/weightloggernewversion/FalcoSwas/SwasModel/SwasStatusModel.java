package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

public class SwasStatusModel {
    String servicecall_status_id,status_name;

    public String getServicecall_status_id() {
        return servicecall_status_id;
    }

    public void setServicecall_status_id(String servicecall_status_id) {
        this.servicecall_status_id = servicecall_status_id;
    }

    public String getStatus_name() {
        return status_name;
    }

    public void setStatus_name(String status_name) {
        this.status_name = status_name;
    }
}
