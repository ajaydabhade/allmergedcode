package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CommentsModel implements Parcelable{

    String CommentsID;
    String OEMID;
    String MacID;
    String LoggerID;
    String TagID;
    String Comments;
    String FunComment1;
    String FunComment2;
    String FunComment3;
    String FunComment4;
    String FunComment5;
    String ModifiedDate;

    public CommentsModel(String CommentsID,String OEMID, String macID, String loggerID, String tagID, String comment, String funComment1, String funComment2, String funComment3, String funComment4, String funComment5, String modifiedDate) {
        this.CommentsID = CommentsID;
        this.OEMID = OEMID;
        this.MacID = macID;
        this.LoggerID = loggerID;
        this.TagID = tagID;
        this.Comments = comment;
        this.FunComment1 = funComment1;
        this.FunComment2 = funComment2;
        this.FunComment3 = funComment3;
        this.FunComment4 = funComment4;
        this.FunComment5 = funComment5;
        this.ModifiedDate = modifiedDate;
    }

    protected CommentsModel(Parcel in) {
        CommentsID = in.readString();
        OEMID = in.readString();
        MacID = in.readString();
        LoggerID = in.readString();
        TagID = in.readString();
        Comments = in.readString();
        FunComment1 = in.readString();
        FunComment2 = in.readString();
        FunComment3 = in.readString();
        FunComment4 = in.readString();
        FunComment5 = in.readString();
        ModifiedDate = in.readString();
    }

    public static final Creator<CommentsModel> CREATOR = new Creator<CommentsModel>() {
        @Override
        public CommentsModel createFromParcel(Parcel in) {
            return new CommentsModel(in);
        }

        @Override
        public CommentsModel[] newArray(int size) {
            return new CommentsModel[size];
        }
    };

    public String getCommentsID() {
        return CommentsID;
    }

    public void setCommentsID(String commentsID) {
        CommentsID = commentsID;
    }

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getMacID() {
        return MacID;
    }

    public void setMacID(String macID) {
        MacID = macID;
    }

    public String getLoggerID() {
        return LoggerID;
    }

    public void setLoggerID(String loggerID) {
        LoggerID = loggerID;
    }

    public String getTagID() {
        return TagID;
    }

    public void setTagID(String tagID) {
        TagID = tagID;
    }

    public String getComment() {
        return Comments;
    }

    public void setComment(String comment) {
        Comments = comment;
    }

    public String getFunComment1() {
        return FunComment1;
    }

    public void setFunComment1(String funComment1) {
        FunComment1 = funComment1;
    }

    public String getFunComment2() {
        return FunComment2;
    }

    public void setFunComment2(String funComment2) {
        FunComment2 = funComment2;
    }

    public String getFunComment3() {
        return FunComment3;
    }

    public void setFunComment3(String funComment3) {
        FunComment3 = funComment3;
    }

    public String getFunComment4() {
        return FunComment4;
    }

    public void setFunComment4(String funComment4) {
        FunComment4 = funComment4;
    }

    public String getFunComment5() {
        return FunComment5;
    }

    public void setFunComment5(String funComment5) {
        FunComment5 = funComment5;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(CommentsID);
        dest.writeString(OEMID);
        dest.writeString(MacID);
        dest.writeString(LoggerID);
        dest.writeString(TagID);
        dest.writeString(Comments);
        dest.writeString(FunComment1);
        dest.writeString(FunComment2);
        dest.writeString(FunComment3);
        dest.writeString(FunComment4);
        dest.writeString(FunComment5);
        dest.writeString(ModifiedDate);
    }
}
