package com.brainlines.weightloggernewversion.FalcoJK.Listeners;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.EnterWIPDataModel;

public interface EnterWipListener {

    public void getWIPData(EnterWIPDataModel model);

}
