package com.brainlines.weightloggernewversion.listener;

import java.io.IOException;

public interface GoToNewTestTagFillAcitivity {
    void getLogger(String loggerName, String loggerIpAddress,String loggerMacId) throws IOException;
}
