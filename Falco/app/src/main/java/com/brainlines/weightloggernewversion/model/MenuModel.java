package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MenuModel implements Parcelable {
    String RoleName,OEMId,Module_ID,ModuleName;

    public MenuModel(String roleName, String OEMId, String module_ID, String moduleName) {
        RoleName = roleName;
        this.OEMId = OEMId;
        Module_ID = module_ID;
        ModuleName = moduleName;
    }

    public MenuModel(Parcel in) {
        RoleName = in.readString();
        OEMId = in.readString();
        Module_ID = in.readString();
        ModuleName = in.readString();
    }

    public static final Creator<MenuModel> CREATOR = new Creator<MenuModel>() {
        @Override
        public MenuModel createFromParcel(Parcel in) {
            return new MenuModel(in);
        }

        @Override
        public MenuModel[] newArray(int size) {
            return new MenuModel[size];
        }
    };

    public String getRoleName() {
        return RoleName;
    }

    public void setRoleName(String roleName) {
        RoleName = roleName;
    }

    public String getOEMId() {
        return OEMId;
    }

    public void setOEMId(String OEMId) {
        this.OEMId = OEMId;
    }

    public String getModule_ID() {
        return Module_ID;
    }

    public void setModule_ID(String module_ID) {
        Module_ID = module_ID;
    }

    public String getModuleName() {
        return ModuleName;
    }

    public void setModuleName(String moduleName) {
        ModuleName = moduleName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(RoleName);
        dest.writeString(OEMId);
        dest.writeString(Module_ID);
        dest.writeString(ModuleName);
    }
}
