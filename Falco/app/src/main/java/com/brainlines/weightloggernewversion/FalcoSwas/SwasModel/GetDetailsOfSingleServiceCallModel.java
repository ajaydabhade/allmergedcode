package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

import android.os.Parcel;
import android.os.Parcelable;

public class GetDetailsOfSingleServiceCallModel implements Parcelable {
    String ServiceCallID;
    String ServiceCallStatusID;

    public GetDetailsOfSingleServiceCallModel(String serviceCallID, String typeofservicecallid) {
        ServiceCallID = serviceCallID;
        ServiceCallStatusID = typeofservicecallid;
    }

    protected GetDetailsOfSingleServiceCallModel(Parcel in) {
        ServiceCallID = in.readString();
        ServiceCallStatusID = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ServiceCallID);
        dest.writeString(ServiceCallStatusID);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<GetDetailsOfSingleServiceCallModel> CREATOR = new Creator<GetDetailsOfSingleServiceCallModel>() {
        @Override
        public GetDetailsOfSingleServiceCallModel createFromParcel(Parcel in) {
            return new GetDetailsOfSingleServiceCallModel(in);
        }

        @Override
        public GetDetailsOfSingleServiceCallModel[] newArray(int size) {
            return new GetDetailsOfSingleServiceCallModel[size];
        }
    };

    public String getServiceCallID() {
        return ServiceCallID;
    }

    public void setServiceCallID(String serviceCallID) {
        ServiceCallID = serviceCallID;
    }

    public String getServiceCallStatusID() {
        return ServiceCallStatusID;
    }

    public void setServiceCallStatusID(String serviceCallStatusID) {
        ServiceCallStatusID = serviceCallStatusID;
    }
}
