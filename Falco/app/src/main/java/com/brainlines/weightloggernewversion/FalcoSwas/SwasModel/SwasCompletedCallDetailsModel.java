package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

import android.os.Parcel;
import android.os.Parcelable;

public class SwasCompletedCallDetailsModel implements Parcelable {

    int ServiceCallID;
    String CustomerID;
    String ITEMID;
    String CURRENCY;
    String LOCATIONNAME;
    String CreatedBy;
    String CreatedDate;
    String UpdatedBy;
    String UpdatedDate;
    String ClosedDateTime;
    String ClosedBy;
    int ShortTermResolutionID;
    int LongTermResolutionID;
    int RootCauseID;
    String SERemark;
    String QERemark;
    String RootCauseName;
    String Description;
    int DepartmentID;
    String Departments;
    String LongTermResolutionText;
    String ShortTermResolutionText;
    String TypeOfServiceCallText;
    int TypeOfServiceCallID;
    String NAME;
    String BriefComplaint;
    String TypeOfFaultText;
    String StatusText;
    String FaultyPart;
    String DocumentPath;
    int ServiceCallStatusID;
    String Comments;
    String ServiceEngineerName;
    int ServiceEngineerID;
    String User_Id;
    String CurrentlyAssignedToSE;
    int NumberOfVisits;

    public SwasCompletedCallDetailsModel(int serviceCallID, String customerID, String ITEMID, String CURRENCY, String LOCATIONNAME, String createdBy, String createdDate, String updatedBy, String updatedDate, String closedDateTime, String closedBy, int shortTermResolutionID, int longTermResolutionID, int rootCauseID, String SERemark, String QERemark, String rootCauseName, String description, int departmentID, String departments, String longTermResolutionText, String shortTermResolutionText, String typeOfServiceCallText, int typeOfServiceCallID, String NAME, String briefComplaint, String typeOfFaultText, String statusText, String faultyPart, String documentPath, int serviceCallStatusID, String comments, String serviceEngineerName, int serviceEngineerID, String user_Id, String currentlyAssignedToSE, int numberOfVisits) {
        ServiceCallID = serviceCallID;
        CustomerID = customerID;
        this.ITEMID = ITEMID;
        this.CURRENCY = CURRENCY;
        this.LOCATIONNAME = LOCATIONNAME;
        CreatedBy = createdBy;
        CreatedDate = createdDate;
        UpdatedBy = updatedBy;
        UpdatedDate = updatedDate;
        ClosedDateTime = closedDateTime;
        ClosedBy = closedBy;
        ShortTermResolutionID = shortTermResolutionID;
        LongTermResolutionID = longTermResolutionID;
        RootCauseID = rootCauseID;
        this.SERemark = SERemark;
        this.QERemark = QERemark;
        RootCauseName = rootCauseName;
        Description = description;
        DepartmentID = departmentID;
        Departments = departments;
        LongTermResolutionText = longTermResolutionText;
        ShortTermResolutionText = shortTermResolutionText;
        TypeOfServiceCallText = typeOfServiceCallText;
        TypeOfServiceCallID = typeOfServiceCallID;
        this.NAME = NAME;
        BriefComplaint = briefComplaint;
        TypeOfFaultText = typeOfFaultText;
        StatusText = statusText;
        FaultyPart = faultyPart;
        DocumentPath = documentPath;
        ServiceCallStatusID = serviceCallStatusID;
        Comments = comments;
        ServiceEngineerName = serviceEngineerName;
        ServiceEngineerID = serviceEngineerID;
        User_Id = user_Id;
        CurrentlyAssignedToSE = currentlyAssignedToSE;
        NumberOfVisits = numberOfVisits;
    }

    protected SwasCompletedCallDetailsModel(Parcel in) {
        ServiceCallID = in.readInt();
        CustomerID = in.readString();
        ITEMID = in.readString();
        CURRENCY = in.readString();
        LOCATIONNAME = in.readString();
        CreatedBy = in.readString();
        CreatedDate = in.readString();
        UpdatedBy = in.readString();
        UpdatedDate = in.readString();
        ClosedDateTime = in.readString();
        ClosedBy = in.readString();
        ShortTermResolutionID = in.readInt();
        LongTermResolutionID = in.readInt();
        RootCauseID = in.readInt();
        SERemark = in.readString();
        QERemark = in.readString();
        RootCauseName = in.readString();
        Description = in.readString();
        DepartmentID = in.readInt();
        Departments = in.readString();
        LongTermResolutionText = in.readString();
        ShortTermResolutionText = in.readString();
        TypeOfServiceCallText = in.readString();
        TypeOfServiceCallID = in.readInt();
        NAME = in.readString();
        BriefComplaint = in.readString();
        TypeOfFaultText = in.readString();
        StatusText = in.readString();
        FaultyPart = in.readString();
        DocumentPath = in.readString();
        ServiceCallStatusID = in.readInt();
        Comments = in.readString();
        ServiceEngineerName = in.readString();
        ServiceEngineerID = in.readInt();
        User_Id = in.readString();
        CurrentlyAssignedToSE = in.readString();
        NumberOfVisits = in.readInt();
    }

    public static final Creator<SwasCompletedCallDetailsModel> CREATOR = new Creator<SwasCompletedCallDetailsModel>() {
        @Override
        public SwasCompletedCallDetailsModel createFromParcel(Parcel in) {
            return new SwasCompletedCallDetailsModel(in);
        }

        @Override
        public SwasCompletedCallDetailsModel[] newArray(int size) {
            return new SwasCompletedCallDetailsModel[size];
        }
    };

    public int getServiceCallID() {
        return ServiceCallID;
    }

    public void setServiceCallID(int serviceCallID) {
        ServiceCallID = serviceCallID;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getITEMID() {
        return ITEMID;
    }

    public void setITEMID(String ITEMID) {
        this.ITEMID = ITEMID;
    }

    public String getCURRENCY() {
        return CURRENCY;
    }

    public void setCURRENCY(String CURRENCY) {
        this.CURRENCY = CURRENCY;
    }

    public String getLOCATIONNAME() {
        return LOCATIONNAME;
    }

    public void setLOCATIONNAME(String LOCATIONNAME) {
        this.LOCATIONNAME = LOCATIONNAME;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getUpdatedBy() {
        return UpdatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        UpdatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return UpdatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        UpdatedDate = updatedDate;
    }

    public String getClosedDateTime() {
        return ClosedDateTime;
    }

    public void setClosedDateTime(String closedDateTime) {
        ClosedDateTime = closedDateTime;
    }

    public String getClosedBy() {
        return ClosedBy;
    }

    public void setClosedBy(String closedBy) {
        ClosedBy = closedBy;
    }

    public int getShortTermResolutionID() {
        return ShortTermResolutionID;
    }

    public void setShortTermResolutionID(int shortTermResolutionID) {
        ShortTermResolutionID = shortTermResolutionID;
    }

    public int getLongTermResolutionID() {
        return LongTermResolutionID;
    }

    public void setLongTermResolutionID(int longTermResolutionID) {
        LongTermResolutionID = longTermResolutionID;
    }

    public int getRootCauseID() {
        return RootCauseID;
    }

    public void setRootCauseID(int rootCauseID) {
        RootCauseID = rootCauseID;
    }

    public String getSERemark() {
        return SERemark;
    }

    public void setSERemark(String SERemark) {
        this.SERemark = SERemark;
    }

    public String getQERemark() {
        return QERemark;
    }

    public void setQERemark(String QERemark) {
        this.QERemark = QERemark;
    }

    public String getRootCauseName() {
        return RootCauseName;
    }

    public void setRootCauseName(String rootCauseName) {
        RootCauseName = rootCauseName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getDepartmentID() {
        return DepartmentID;
    }

    public void setDepartmentID(int departmentID) {
        DepartmentID = departmentID;
    }

    public String getDepartments() {
        return Departments;
    }

    public void setDepartments(String departments) {
        Departments = departments;
    }

    public String getLongTermResolutionText() {
        return LongTermResolutionText;
    }

    public void setLongTermResolutionText(String longTermResolutionText) {
        LongTermResolutionText = longTermResolutionText;
    }

    public String getShortTermResolutionText() {
        return ShortTermResolutionText;
    }

    public void setShortTermResolutionText(String shortTermResolutionText) {
        ShortTermResolutionText = shortTermResolutionText;
    }

    public String getTypeOfServiceCallText() {
        return TypeOfServiceCallText;
    }

    public void setTypeOfServiceCallText(String typeOfServiceCallText) {
        TypeOfServiceCallText = typeOfServiceCallText;
    }

    public int getTypeOfServiceCallID() {
        return TypeOfServiceCallID;
    }

    public void setTypeOfServiceCallID(int typeOfServiceCallID) {
        TypeOfServiceCallID = typeOfServiceCallID;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getBriefComplaint() {
        return BriefComplaint;
    }

    public void setBriefComplaint(String briefComplaint) {
        BriefComplaint = briefComplaint;
    }

    public String getTypeOfFaultText() {
        return TypeOfFaultText;
    }

    public void setTypeOfFaultText(String typeOfFaultText) {
        TypeOfFaultText = typeOfFaultText;
    }

    public String getStatusText() {
        return StatusText;
    }

    public void setStatusText(String statusText) {
        StatusText = statusText;
    }

    public String getFaultyPart() {
        return FaultyPart;
    }

    public void setFaultyPart(String faultyPart) {
        FaultyPart = faultyPart;
    }

    public String getDocumentPath() {
        return DocumentPath;
    }

    public void setDocumentPath(String documentPath) {
        DocumentPath = documentPath;
    }

    public int getServiceCallStatusID() {
        return ServiceCallStatusID;
    }

    public void setServiceCallStatusID(int serviceCallStatusID) {
        ServiceCallStatusID = serviceCallStatusID;
    }

    public String getComments() {
        return Comments;
    }

    public void setComments(String comments) {
        Comments = comments;
    }

    public String getServiceEngineerName() {
        return ServiceEngineerName;
    }

    public void setServiceEngineerName(String serviceEngineerName) {
        ServiceEngineerName = serviceEngineerName;
    }

    public int getServiceEngineerID() {
        return ServiceEngineerID;
    }

    public void setServiceEngineerID(int serviceEngineerID) {
        ServiceEngineerID = serviceEngineerID;
    }

    public String getUser_Id() {
        return User_Id;
    }

    public void setUser_Id(String user_Id) {
        User_Id = user_Id;
    }

    public String getCurrentlyAssignedToSE() {
        return CurrentlyAssignedToSE;
    }

    public void setCurrentlyAssignedToSE(String currentlyAssignedToSE) {
        CurrentlyAssignedToSE = currentlyAssignedToSE;
    }

    public int getNumberOfVisits() {
        return NumberOfVisits;
    }

    public void setNumberOfVisits(int numberOfVisits) {
        NumberOfVisits = numberOfVisits;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ServiceCallID);
        dest.writeString(CustomerID);
        dest.writeString(ITEMID);
        dest.writeString(CURRENCY);
        dest.writeString(LOCATIONNAME);
        dest.writeString(CreatedBy);
        dest.writeString(CreatedDate);
        dest.writeString(UpdatedBy);
        dest.writeString(UpdatedDate);
        dest.writeString(ClosedDateTime);
        dest.writeString(ClosedBy);
        dest.writeInt(ShortTermResolutionID);
        dest.writeInt(LongTermResolutionID);
        dest.writeInt(RootCauseID);
        dest.writeString(SERemark);
        dest.writeString(QERemark);
        dest.writeString(RootCauseName);
        dest.writeString(Description);
        dest.writeInt(DepartmentID);
        dest.writeString(Departments);
        dest.writeString(LongTermResolutionText);
        dest.writeString(ShortTermResolutionText);
        dest.writeString(TypeOfServiceCallText);
        dest.writeInt(TypeOfServiceCallID);
        dest.writeString(NAME);
        dest.writeString(BriefComplaint);
        dest.writeString(TypeOfFaultText);
        dest.writeString(StatusText);
        dest.writeString(FaultyPart);
        dest.writeString(DocumentPath);
        dest.writeInt(ServiceCallStatusID);
        dest.writeString(Comments);
        dest.writeString(ServiceEngineerName);
        dest.writeInt(ServiceEngineerID);
        dest.writeString(User_Id);
        dest.writeString(CurrentlyAssignedToSE);
        dest.writeInt(NumberOfVisits);
    }
}
