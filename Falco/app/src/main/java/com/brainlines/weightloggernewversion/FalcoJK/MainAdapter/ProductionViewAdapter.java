package com.brainlines.weightloggernewversion.FalcoJK.MainAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.ProductionViewTrackingModel;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class ProductionViewAdapter extends RecyclerView.Adapter<ProductionViewAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<ProductionViewTrackingModel> ticket_list;
    View.OnClickListener mClickListener;
    Context context;
    String str_tk_no;

    public ProductionViewAdapter(Context ctx, ArrayList<ProductionViewTrackingModel> ticket_list) {
        inflater = LayoutInflater.from(ctx);
        this.ticket_list = ticket_list;
        this.context = ctx;
    }


    @Override
    public int getItemCount() {
        return ticket_list.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_view_updated_production_ticket_data, parent, false);
        final MyViewHolder holder = new MyViewHolder(view);

       /* holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onClick(view);

            }
        });*/


        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int i) {
        holder.viewopeartions.setText(ticket_list.get(i).getOpr_operation_name());
        holder.viewokay.setText(ticket_list.get(i).getPtk_OkQty());
        holder.viewreject.setText(ticket_list.get(i).getPtk_RejectQty());
        holder.viewrework.setText(ticket_list.get(i).getPtk_ReworkQty());
        holder.viewsecond.setText(ticket_list.get(i).getPtk_SecondQty());
        holder.viewwip.setText(ticket_list.get(i).getPtk_WIPQty());

    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView viewopeartions,view_current_route,viewokay,viewrework,viewreject,viewwip,viewsecond;

        public MyViewHolder(View itemView) {
            super(itemView);
            viewopeartions=(TextView)itemView.findViewById(R.id.viewopeartions);
            //view_current_route=(TextView)itemView.findViewById(R.id.view_current_route);
            viewokay=(TextView)itemView.findViewById(R.id.viewokay);
            viewrework=(TextView)itemView.findViewById(R.id.viewrework);
            viewreject=(TextView)itemView.findViewById(R.id.viewreject);
            viewwip=(TextView)itemView.findViewById(R.id.viewwip);
            viewsecond=(TextView)itemView.findViewById(R.id.viewsecond);
        }
    }
}
