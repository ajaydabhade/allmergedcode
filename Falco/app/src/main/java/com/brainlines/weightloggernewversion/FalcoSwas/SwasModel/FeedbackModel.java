package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

public class FeedbackModel {
    String FeedbackDetailsID,OEMID,ServiceCallID,UserID,UserFeedback,CreatedBy,CreatedDate;

    public String getFeedbackDetailsID() {
        return FeedbackDetailsID;
    }

    public void setFeedbackDetailsID(String feedbackDetailsID) {
        FeedbackDetailsID = feedbackDetailsID;
    }

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getServiceCallID() {
        return ServiceCallID;
    }

    public void setServiceCallID(String serviceCallID) {
        ServiceCallID = serviceCallID;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getUserFeedback() {
        return UserFeedback;
    }

    public void setUserFeedback(String userFeedback) {
        UserFeedback = userFeedback;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }
}
