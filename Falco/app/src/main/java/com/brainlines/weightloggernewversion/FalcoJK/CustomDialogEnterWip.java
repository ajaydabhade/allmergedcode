package com.brainlines.weightloggernewversion.FalcoJK;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.EnterWIPDataModel;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.EnterWipAdapter;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class CustomDialogEnterWip extends DialogFragment {

    private static final String TAG = "MyCustomDialog";
    RecyclerView rvSubPlanDetailsList;
    Button btnSubscribe;
    EnterWipAdapter alertDetailsAdapter;
    ArrayList<EnterWIPDataModel> enterWIPDataModelArrayList =new ArrayList<>();
    String str_tk_no;
    Context context;

    public CustomDialogEnterWip(String str_tk_no, Context context) {
        this.str_tk_no = str_tk_no;
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.custom_alert_enter_wip, container, false);
        rvSubPlanDetailsList = view.findViewById(R.id.rvSubPlanDetailsList);
        btnSubscribe = view.findViewById(R.id.btnSubscribe);

        getEnterWipData();

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

//        try{
//            listener = (SubPlanListener) getActivity();
//        }catch (ClassCastException e){
//            Log.e(TAG, "onAttach: ClassCastException: " + e.getMessage() );
//        }

    }

    private void getEnterWipData() {
        API api = Retroconfig.retrofit().create(API.class);
        Call<ResponseBody> call = api.getEnterWipData(str_tk_no);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if(response.body()!=null)
                    {}
                    String res = response.body().string();
                    if (res!=null){
                        JSONObject object = new JSONObject(res);
                        JSONArray array = object.getJSONArray("data");
                        enterWIPDataModelArrayList.clear();
                        if (array.length()!=0){

                            for (int i = 0 ; i < array.length() ; i++){
                                JSONObject obj = array.getJSONObject(i);
                                EnterWIPDataModel model = new EnterWIPDataModel();
                                model.setPtkd_Id(obj.getString("ptkd_Id"));
                                model.setPtkd_PktId(obj.getString("ptkd_PktId"));
                                model.setPtkd_TkNo(obj.getString("ptkd_TkNo"));
                                model.setPtkd_PSKU(obj.getString("ptkd_PSKU"));
                                model.setPtkd_Plant(obj.getString("ptkd_Plant"));
                                model.setPtkd_ValueStreamCode(obj.getString("ptkd_ValueStreamCode"));
                                model.setPtkd_Operation(obj.getString("ptkd_Operation"));
                                model.setPtkd_WIP(obj.getString("ptkd_WIP"));
                                model.setOpr_operation_name(obj.getString("opr_operation_name"));
                                model.setUni_unit_name(obj.getString("uni_unit_name"));
                                model.setPtkd_Qty(obj.getString("ptkd_Qty"));
                                enterWIPDataModelArrayList.add(model);
                            }
                        }

                        ArrayList<EnterWIPDataModel> list = enterWIPDataModelArrayList;
                        LinearLayoutManager manager = new LinearLayoutManager(context);
                       /* rvSubPlanDetailsList.setLayoutManager(manager);
                        alertDetailsAdapter = new EnterWipAdapter(context , enterWIPDataModelArrayList);
                        rvSubPlanDetailsList.setAdapter(alertDetailsAdapter);*/

                    }else {
                    }
                }
                catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}
