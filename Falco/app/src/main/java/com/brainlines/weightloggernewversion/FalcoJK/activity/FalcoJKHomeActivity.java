package com.brainlines.weightloggernewversion.FalcoJK.activity;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.PackingTicketModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.ProdTicketModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.ProdTkOperationModel;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewPackingTicket.JKNewPackingTicketSelectionListActivity;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.JkNewProductionTicketSelectionListActivity;
import com.brainlines.weightloggernewversion.FalcoJK.UrlConstants.JK_URL_Constants;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.AppPreferences;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.brainlines.weightloggernewversion.utils.GlobalClass;
import com.google.android.material.navigation.NavigationView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FalcoJKHomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "FalcoJKHomeActivity";
    String user_email,user_role,user_token,oem_id;
    ImageView img_user_profile,nav_view_img;
    DrawerLayout drawer;
    TextView profile_user,profile_role;
    private ProgressDialog progressDialog;
    ArrayList<ProdTkOperationModel> ticketmodel= new ArrayList<>();
    DataBaseHelper helper;
    SQLiteDatabase db;
    String strproductionTkID,strProductionTkNo,strpa;
    private String strpackingTkID,strReason,dbName;
    private ProgressDialog progressDialogToGetData;
    private String userEmail;
    ImageButton btn_sync;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jk_home_navigation);
        userEmail = AppPreferences.getUser(this);
        btn_sync = findViewById(R.id.btn_sync);
        com.brainlines.weightloggernewversion.model.UserModel model = getIntent().getParcelableExtra("UserDataModel");
        dbName = GlobalClass.db_global_name;
        if (model != null){
            helper = new DataBaseHelper(this);
            db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));
        }else {
            helper = new DataBaseHelper(this);
            db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));
            Log.d(TAG, "onCreate: ");
        }
        int c = helper.getTableCount();
        if (c == 0){
            Log.d(TAG, "onCreate: Table Count -> " + c);
            helper.createTablesForJKPlanning();
            new TempSwasLoginCall().execute();
        }else {
            Log.d(TAG, "onCreate: Table Count -> " + c );
        }

        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img= findViewById(R.id.nav_view_img);
        img_user_profile= findViewById(R.id.img_userinfo);
        img_user_profile.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        insertReasonsInMaster();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);

        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_swas).setVisible(false);
        CardView card_production=findViewById(R.id.card_production);
        CardView card_packing=findViewById(R.id.card_packing);

        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }
        });

        card_packing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(FalcoJKHomeActivity.this, JKNewPackingTicketSelectionListActivity.class);
//                Intent i=new Intent(FalcoJKHomeActivity.this, JKPackingTicketSelection.class);
                startActivity(i);
            }
        });
        card_production.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(FalcoJKHomeActivity.this, JkNewProductionTicketSelectionListActivity.class);
//                Intent i=new Intent(FalcoJKHomeActivity.this, JK_Production_Selection_list.class);
                startActivity(i);
            }
        });
        ImageView img_backe=findViewById(R.id.img_back);
        img_backe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i=new Intent(FalcoJKHomeActivity.this, Falco_Home_Main_Activity.class);
//                startActivity(i);
            }
        });
    }

    private void getData() {
        progressDialogToGetData = new ProgressDialog(FalcoJKHomeActivity.this);
        progressDialogToGetData.setMessage("Please wait while loading database");
        progressDialogToGetData.setCancelable(false);
        progressDialogToGetData.show();
        getPackingTicketlist();
        getProductionTicketlist();
        getAssetMaster();
        getEquipmentMaster();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Cursor getproductionDetails = helper.getPackingTicketIDandDetails();
                while (getproductionDetails.moveToNext())
                {
                    strpackingTkID = getproductionDetails.getString(1);
                    getPackingTicketsDatewiseQuanitity();
                }
            }
        },4000);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Cursor getproductionDetails = helper.getProductionTicketIDandDetails();
                while (getproductionDetails.moveToNext())
                {
                    strproductionTkID = getproductionDetails.getString(1);
                    strProductionTkNo = getproductionDetails.getString(2);
                    getSkuDetails(strProductionTkNo);
                    getWIPAgainstTickets(strProductionTkNo);

                }
            }
        },8000);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Cursor cursorreason = helper.getReasons();
                while (cursorreason.moveToNext())
                {
                    strReason = cursorreason.getString(1);
                    //strProductionTkNo = getproductionDetails.getString(2);
                    String string="Reason";
                    //strReason=string.concat(strReason);
                    //getEquipmentdetails(strReason);
                    // getWIPAgainstTickets(strProductionTkNo);
                    progressDialogToGetData.dismiss();
                }
            }
        },12000);
    }

    public class TempSwasLoginCall extends AsyncTask<String,String,String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(FalcoJKHomeActivity.this);
            progressDialog.setMessage("Please wait while loading database");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String getResponse="";
            try {
                HttpParams httpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(httpParams, 60000);
                HttpConnectionParams.setSoTimeout(httpParams, 60000);
                HttpClient client = new DefaultHttpClient(httpParams);
                HttpPost httpPost = new HttpPost(URL_Constants.LoginApi);
                httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
               /* try {
                    String password = "password";
                    String encryptedMsg = AESCrypt.encrypt(password, str_password);
                    str_password = encryptedMsg;
                }catch (GeneralSecurityException e){
                    //handle error
                }
                str_password = Base64.encodeToString( str_password.getBytes(), Base64.DEFAULT );*/
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("grant_type","password"));
                nameValuePairs.add(new BasicNameValuePair("username",userEmail));
                //nameValuePairs.add(new BasicNameValuePair("password",str_password));
                nameValuePairs.add(new BasicNameValuePair("password","UJkGMEMpSWXJnOKrd9bxYSVnXF0uQ7zKFzGlygZdKl8="));
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                httpPost.setParams(httpParams);

                HttpResponse response = client.execute(httpPost);
                StatusLine statusLine = response.getStatusLine();

                if (statusLine.getStatusCode() == HttpStatus.SC_OK || statusLine.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();

                    getResponse = URL_Constants.convertStreamToString(inputStream);
                }
                else {
                    response.getEntity().getContent().close();
                }
            }
            catch (UnknownHostException e) {
                Log.i(TAG, "Error Message in UnknownHostException :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_UNKNOWN_HOST_EXCEPTION;

            }
            catch (ConnectTimeoutException e) {
                Log.i(TAG, "Error Message in ConnectTimeoutException :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_CONNECTION_TIMEOUT_EXCEPTION;


            }
            catch (ClientProtocolException e) {
                Log.i(TAG, "Error Message in ClientProtocolException :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_CLIENT_PROTOCOL_EXCEPTION;

            }
            catch (IOException e) {
                Log.i(TAG, "Error Message in IOException :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_IO_EXCEPTION;

            }
            catch (Exception e) {
                Log.i(TAG, "Error Message in Exception :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_EXCEPTION;

            }
            return getResponse;
        }
        @Override
        protected void onPostExecute(String getResponse) {
            super.onPostExecute(getResponse);
            progressDialog.dismiss();
            if (getResponse.contains("error"))
            {
                Toast.makeText(FalcoJKHomeActivity.this, "Invalid username & password", Toast.LENGTH_SHORT).show();
//                ed_username.getText().clear();
//                ed_password.getText().clear();
            }
            else {
                if (!getResponse.equals("Error"))
                {
                    try {
                        JSONObject jsonObject=new JSONObject(getResponse);
//                        String token=jsonObject.getString("access_token");
//                        String token_type=jsonObject.getString("token_type");
//                        String user_emailid=jsonObject.getString("UserEmailId");
//                        String user_role=jsonObject.getString("RoleName");
//                        String oemid=jsonObject.getString("OEMID");
//                        UserModelToStoreInDB model = new UserModelToStoreInDB(jsonObject.getString("access_token"),
//                                jsonObject.getString("token_type"),jsonObject.getString("OEMID"),
//                                jsonObject.getString("UserEmailId"),jsonObject.getString("RoleName"));
                        ContentValues cv=new ContentValues();
                        cv.put("UserToken",jsonObject.getString("access_token"));
                        cv.put("UserEmailID",jsonObject.getString("UserEmailId"));
                        cv.put("UserTokenType",jsonObject.getString("token_type"));
                        cv.put("UserOEMID",jsonObject.getString("OEMID"));
                        cv.put("UserRoleName",jsonObject.getString("RoleName"));
                        cv.put("UserExpiresIn",jsonObject.getString("expires_in"));
                        cv.put("UserOEMName",jsonObject.getString("OEMName"));
                        cv.put("UserFirstName",jsonObject.getString("FirstName"));
                        cv.put("UserLastName",jsonObject.getString("LastName"));
                        cv.put("UserLoginID",jsonObject.getString("Login_Id"));
                        cv.put("UserIsIssued",jsonObject.getString(".issued"));
                        cv.put("UserExpires",jsonObject.getString(".expires"));
                        long d=db.insert(Constants.TABLE_USER_DETAILS,null,cv);
                        Log.d(TAG, "onCreate: ");
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    setData();
                    getData();
                }
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(FalcoJKHomeActivity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);
        setData();
        // Set content width and height
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(FalcoJKHomeActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }

    public void logout()
    {
        finish();
        Toast toast= Toast.makeText(FalcoJKHomeActivity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper DataBaseHelper = new DataBaseHelper(FalcoJKHomeActivity.this);
        SQLiteDatabase database = DataBaseHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);

        //Preferences.Logout();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id== R.id.nav_planning)
        {
            Intent i=new Intent(FalcoJKHomeActivity.this, FalcoJKHomeActivity.class);
            startActivity(i);
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;

    }
    public void setData() {
        DataBaseHelper DataBaseHelper = new DataBaseHelper(FalcoJKHomeActivity.this);
        SQLiteDatabase database = DataBaseHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();
                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    oem_id= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));


                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }


    private void getPackingTicketlist()
    {
        progressDialog=new ProgressDialog(FalcoJKHomeActivity.this);
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);
        progressDialog.show();
        API api = Retroconfig.retrofit().create(API.class);
        Call<ResponseBody> call = api.getPackingTickets("null", JK_URL_Constants.PLANT);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            progressDialog.dismiss();

                            JSONObject jsonObject=new JSONObject(res);
                            JSONArray array=jsonObject.getJSONArray("Data");
                            if (array.length()==0){
                                /*Toast toast = Toast.makeText(getApplicationContext(),
                                        "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                                toast.show();*/
                            }
                            else {
                                ArrayList<PackingTicketModel> ticketmodel= new ArrayList<>();

                                for (int i=0;i<array.length();i++)
                                {

                                    JSONObject obj=new JSONObject(array.get(i).toString());
                                    PackingTicketModel model=new PackingTicketModel();

                                    if (obj.has("tk_Release_Date")){
                                       /* model.setID(obj.getString("ID"));
                                        model.setTk_Id(obj.getString("tk_Id"));
                                        model.setTk_TicketNo(obj.getString("tk_TicketNo"));
                                        model.setTk_SONO(obj.getString("tk_SONO"));
                                        model.setTk_UploadRefNumber(obj.getString("tk_UploadRefNumber"));
                                        model.setTk_Plant(obj.getString("tk_Plant"));
                                        model.setTk_SKU(obj.getString("tk_SKU"));
                                        model.setTk_Prod_SKU(obj.getString("tk_Prod_SKU"));
                                        model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                                        model.setEDD(obj.getString("EDD"));
                                        model.setTk_End_Date(obj.getString("tk_End_Date"));
                                        model.setTk_Quantity(obj.getString("tk_Quantity"));
                                        model.setPln_Packing_Norms(obj.getString("pln_Packing_Norms"));
                                        //model.setTk_Unpaking_SKU(obj.getString("tk_Unpaking_SKU"));
                                        model.setTk_Unpacking(obj.getString("tk_Unpacking"));
                                        model.setMonthStart(obj.getString("MonthStart"));
                                        model.setMonthEnd(obj.getString("MonthEnd"));
                                        model.setTk_Active(obj.getString("tk_Active"));
                                        model.setTk_Release(obj.getString("tk_Release"));
                                        model.setTk_produce_qty(obj.getString("tk_totalQty"));
                                        // model.setTk_actual_end_Date(obj.getString("tk_Actual_End_Date"));
                                        model.setTk_Release_Date(obj.getString("tk_Release_Date"));
                                        ticketmodel.add(model);*/

                                        ContentValues cv=new ContentValues();
                                        cv.put("ID",obj.getString("ID"));
                                        cv.put("tk_Id",obj.getString("tk_Id"));
                                        cv.put("tk_TicketNo",obj.getString("tk_TicketNo"));
                                        cv.put("tk_SONO",obj.getString("tk_SONO"));
                                        cv.put("tk_Plant",obj.getString("tk_Plant"));
                                        cv.put("tk_SKU",obj.getString("tk_SKU"));
                                        cv.put("tk_Prod_SKU",obj.getString("tk_Prod_SKU"));
                                        cv.put("tk_Start_Date",obj.getString("tk_Start_Date"));
                                        cv.put("tk_End_Date",obj.getString("tk_End_Date"));
                                        cv.put("tk_Quantity",obj.getString("tk_Quantity"));
                                        cv.put("tk_Unpacking",obj.getString("tk_Unpacking"));
                                        if (obj.getString("tk_Unpacking").equals("N")) {
                                            cv.put("ticket_type","PackingTicket");
                                        }
                                        else {
                                            cv.put("ticket_type","RepackingTicket");

                                        }
                                        if (obj.has("tk_Unpaking_SKU"))
                                        {
                                            cv.put("tk_Unpaking_SKU",obj.has("tk_Unpaking_SKU"));
                                        }
                                        else {  cv.put("tk_Unpaking_SKU","null");
                                        }
                                        cv.put("MonthStart",obj.getString("MonthStart"));
                                        cv.put("MonthEnd",obj.getString("MonthEnd"));
                                        cv.put("tk_Active",obj.getString("tk_Active"));
                                        cv.put("tk_Release",obj.getString("tk_Release"));
                                        cv.put("tk_totalQty",obj.getString("tk_totalQty"));
                                        cv.put("tk_Release_Date",obj.getString("tk_Release_Date"));

                                        String ticketno = obj.getString("tk_TicketNo");
                                        String q = "SELECT * FROM "+Constants.PACKING_TICKET_TABLE+" where "+Constants.PACKING_TICKET_TICKET_NO+"='"+ticketno+"'";
                                        Cursor c = db.rawQuery(q,null);
                                        if(c.moveToFirst())
                                        {
                                            //showMessage("Error", "Record exist");
                                        }
                                        else
                                        {
                                            long d=db.insert(Constants.PACKING_TICKET_TABLE,null,cv);
                                            Log.d("user check", String.valueOf(d));
                                        }
                                    }
                                    else
                                    {
                                        /*model.setID(obj.getString("ID"));
                                        model.setTk_Id(obj.getString("tk_Id"));
                                        model.setTk_TicketNo(obj.getString("tk_TicketNo"));
                                        model.setTk_SONO(obj.getString("tk_SONO"));
                                        model.setTk_UploadRefNumber(obj.getString("tk_UploadRefNumber"));
                                        model.setTk_Plant(obj.getString("tk_Plant"));
                                        model.setTk_SKU(obj.getString("tk_SKU"));
                                        model.setTk_Prod_SKU(obj.getString("tk_Prod_SKU"));
                                        model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                                        model.setEDD(obj.getString("EDD"));
                                        model.setTk_End_Date(obj.getString("tk_End_Date"));
                                        model.setTk_Quantity(obj.getString("tk_Quantity"));
                                        model.setPln_Packing_Norms(obj.getString("pln_Packing_Norms"));
                                        //model.setTk_Unpaking_SKU(obj.getString("tk_Unpaking_SKU"));
                                        model.setTk_Unpacking(obj.getString("tk_Unpacking"));
                                        model.setMonthStart(obj.getString("MonthStart"));
                                        model.setMonthEnd(obj.getString("MonthEnd"));
                                        model.setTk_Active(obj.getString("tk_Active"));
                                        model.setTk_Release(obj.getString("tk_Release"));
                                        model.setTk_produce_qty(obj.getString("tk_totalQty"));
                                        //model.setTk_actual_end_Date(obj.getString("tk_Actual_End_Date"));
                                        model.setTk_Release_Date("null");
                                        ticketmodel.add(model);*/

                                        DataBaseHelper helper=new DataBaseHelper(FalcoJKHomeActivity.this);
                                        SQLiteDatabase db = helper.getWritableDatabase();
                                        ContentValues cv=new ContentValues();
                                        cv.put("ID",obj.getString("ID"));
                                        cv.put("tk_Id",obj.getString("tk_Id"));
                                        cv.put("tk_TicketNo",obj.getString("tk_TicketNo"));
                                        cv.put("tk_SONO",obj.getString("tk_SONO"));
                                        cv.put("tk_Plant",obj.getString("tk_Plant"));
                                        cv.put("tk_SKU",obj.getString("tk_SKU"));
                                        cv.put("tk_Prod_SKU",obj.getString("tk_Prod_SKU"));
                                        cv.put("tk_Start_Date",obj.getString("tk_Start_Date"));
                                        cv.put("tk_End_Date",obj.getString("tk_End_Date"));
                                        cv.put("tk_Quantity",obj.getString("tk_Quantity"));
                                        cv.put("tk_Unpacking",obj.getString("tk_Unpacking"));
                                        if (obj.getString("tk_Unpacking").equals("N")) {
                                            cv.put("ticket_type","PackingTicket");
                                        }
                                        else {
                                            cv.put("ticket_type","RepackingTicket");

                                        }
                                        if (obj.has("tk_Unpaking_SKU")) {
                                            cv.put("tk_Unpaking_SKU",obj.has("tk_Unpaking_SKU"));
                                        }
                                        else {  cv.put("tk_Unpaking_SKU","null");
                                        }
                                        cv.put("MonthStart",obj.getString("MonthStart"));
                                        cv.put("MonthEnd",obj.getString("MonthEnd"));
                                        cv.put("tk_Active",obj.getString("tk_Active"));
                                        cv.put("tk_Release",obj.getString("tk_Release"));
                                        cv.put("tk_totalQty",obj.getString("tk_totalQty"));
                                        cv.put("tk_Release_Date","null");

                                        String ticketno = obj.getString("tk_TicketNo");
                                        String q = "SELECT * FROM "+Constants.PACKING_TICKET_TABLE+" where "+Constants.PACKING_TICKET_TICKET_NO+"='"+ticketno+"'";
                                        Cursor c = db.rawQuery(q,null);
                                        if(c.moveToFirst())
                                        {
                                            //showMessage("Error", "Record exist");
                                        }
                                        else
                                        {
                                            long d=db.insert(Constants.PACKING_TICKET_TABLE,null,cv);
                                            Log.d("user check", String.valueOf(d));
                                        }
                                    }

                                }

                            }

                        }
                    }
                    else
                    {
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(FalcoJKHomeActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getProductionTicketlist()
    {
       /* progressDialog=new ProgressDialog(FalcoJKHomeActivity.this);
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);
        progressDialog.show();*/
        API api = Retroconfig.retrofit().create(API.class);
        Call<ResponseBody> call = api.getProductionTickets(JK_URL_Constants.PLANT);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            //progressDialog.dismiss();

                            JSONObject jsonObject=new JSONObject(res);
                            JSONArray array=jsonObject.getJSONArray("Data");
                            if (array.length()==0){
                                /*Toast toast = Toast.makeText(getApplicationContext(),
                                        "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                                toast.show();*/
                            }
                            else {
                                ArrayList<ProdTicketModel> ticketmodel= new ArrayList<>();

                                for (int i=0;i<array.length();i++)
                                {

                                    JSONObject obj=new JSONObject(array.get(i).toString());
                                    ProdTicketModel model =new ProdTicketModel();
                                    if (obj.has("ptkd_ReleaseDate"))
                                    {
                                        if(obj.has("tk_End_Date"))
                                        {
                                            model.setPtkd_PktId(obj.getString("ptkd_PktId"));
                                            model.setPtkd_TkNo(obj.getString("ptkd_TkNo"));
                                            model.setPtkd_PSKU(obj.getString("ptkd_PSKU"));
                                            model.setPtkd_Plant(obj.getString("ptkd_Plant"));
                                            model.setPtkd_ValueStreamCode(obj.getString("ptkd_ValueStreamCode"));
                                            model.setPtkd_Ftypecode(obj.getString("ptkd_Ftypecode"));

                                            model.setPtkd_IsRelease(obj.getString("ptkd_IsRelease"));
                                            model.setTk_Actual_Start_Date(obj.getString("tk_Actual_Start_Date"));
                                            model.setTk_Actual_End_Date(obj.getString("tk_Actual_End_Date"));
                                            model.setPtkd_IsRelease1(obj.getString("ptkd_IsRelease1"));
                                            model.setPtkd_ReleaseDate(obj.getString("ptkd_ReleaseDate"));
                                            model.setPtkd_Active(obj.getString("ptkd_Active"));
                                            model.setTk_act_End_Date(obj.getString("tk_End_Date"));
                                            ticketmodel.add(model);

                                            ContentValues cv=new ContentValues();
                                            cv.put("ptkd_PktId",obj.getString("ptkd_PktId"));
                                            cv.put("ptkd_TkNo",obj.getString("ptkd_TkNo"));
                                            cv.put("ptkd_PSKU",obj.getString("ptkd_PSKU"));
                                            cv.put("ptkd_Plant",obj.getString("ptkd_Plant"));
                                            cv.put("ptkd_ValueStreamCode",obj.getString("ptkd_ValueStreamCode"));
                                            cv.put("ptkd_Ftypecode",obj.getString("ptkd_Ftypecode"));
                                            cv.put("ptkd_Ftypecode",obj.getString("ptkd_Ftypecode"));
                                            String total_qty = obj.getString("ptkd_Qty");
                                            double constant = 12.0;
                                            try
                                            {
                                                double result = Double.parseDouble(total_qty) * constant;
                                                String qty = String.valueOf(result);
                                                cv.put("ptkd_Qty",qty);

                                            }
                                            catch (Exception e)
                                            {
                                                e.printStackTrace();
                                                cv.put("ptkd_Qty",total_qty);

                                            }
                                            cv.put("ptkd_IsRelease",obj.getString("ptkd_IsRelease"));
                                            cv.put("tk_Actual_Start_Date",obj.getString("tk_Actual_Start_Date"));
                                            cv.put("tk_Actual_End_Date",obj.getString("tk_Actual_End_Date"));
                                            cv.put("ptkd_IsRelease1",obj.getString("ptkd_IsRelease1"));
                                            cv.put("ptkd_ReleaseDate",obj.getString("ptkd_ReleaseDate"));
                                            cv.put("ptkd_Active",obj.getString("ptkd_Active"));
                                            cv.put("tk_End_Date",obj.getString("tk_End_Date"));

                                            String ticketno = obj.getString("ptkd_TkNo");
                                            String q = "SELECT * FROM "+Constants.PRODUCTION_TICKET_TABLE+" where "+Constants.PRODUCTION_TICKET_NO+"='"+ticketno+"'";
                                            Cursor c = db.rawQuery(q,null);
                                            if(c.moveToFirst())
                                            {
                                                //showMessage("Error", "Record exist");
                                            }
                                            else
                                            {
                                                long d=db.insert(Constants.PRODUCTION_TICKET_TABLE,null,cv);
                                                Log.d("user check", String.valueOf(d));
                                            }
                                        }
                                        else
                                        {

                                            DataBaseHelper helper=new DataBaseHelper(FalcoJKHomeActivity.this);
                                            SQLiteDatabase db = helper.getWritableDatabase();
                                            ContentValues cv=new ContentValues();
                                            cv.put("ptkd_PktId",obj.getString("ptkd_PktId"));
                                            cv.put("ptkd_TkNo",obj.getString("ptkd_TkNo"));
                                            cv.put("ptkd_PSKU",obj.getString("ptkd_PSKU"));
                                            cv.put("ptkd_Plant",obj.getString("ptkd_Plant"));
                                            cv.put("ptkd_ValueStreamCode",obj.getString("ptkd_ValueStreamCode"));
                                            cv.put("ptkd_Ftypecode",obj.getString("ptkd_Ftypecode"));
                                            cv.put("ptkd_Ftypecode",obj.getString("ptkd_Ftypecode"));
                                            String total_qty = obj.getString("ptkd_Qty");
                                            double constant = 12.0;
                                            try
                                            {
                                                double result = Double.parseDouble(total_qty) * constant;
                                                String qty = String.valueOf(result);
                                                cv.put("ptkd_Qty",qty);

                                            }
                                            catch (Exception e)
                                            {
                                                e.printStackTrace();
                                                cv.put("ptkd_Qty",total_qty);

                                            }
                                            cv.put("ptkd_IsRelease",obj.getString("ptkd_IsRelease"));
                                            cv.put("tk_Actual_Start_Date",obj.getString("tk_Actual_Start_Date"));
                                            cv.put("tk_Actual_End_Date",obj.getString("tk_Actual_End_Date"));
                                            cv.put("ptkd_IsRelease1",obj.getString("ptkd_IsRelease1"));
                                            cv.put("ptkd_ReleaseDate",obj.getString("ptkd_ReleaseDate"));
                                            cv.put("ptkd_Active",obj.getString("ptkd_Active"));
                                            cv.put("tk_End_Date","null");


                                            String ticketno = obj.getString("ptkd_TkNo");
                                            String q = "SELECT * FROM "+Constants.PRODUCTION_TICKET_TABLE+" where "+Constants.PRODUCTION_TICKET_NO+"='"+ticketno+"'";
                                            Cursor c = db.rawQuery(q,null);
                                            if(c.moveToFirst())
                                            {
                                                //showMessage("Error", "Record exist");
                                            }
                                            else
                                            {
                                                long d=db.insert(Constants.PRODUCTION_TICKET_TABLE,null,cv);
                                                Log.d("user check", String.valueOf(d));
                                            }
                                        }

                                    }
                                    else {
                                        if (obj.has("tk_End_Date"))
                                        {

                                            DataBaseHelper helper=new DataBaseHelper(FalcoJKHomeActivity.this);
                                            SQLiteDatabase db = helper.getWritableDatabase();
                                            ContentValues cv=new ContentValues();
                                            cv.put("ptkd_PktId",obj.getString("ptkd_PktId"));
                                            cv.put("ptkd_TkNo",obj.getString("ptkd_TkNo"));
                                            cv.put("ptkd_PSKU",obj.getString("ptkd_PSKU"));
                                            cv.put("ptkd_Plant",obj.getString("ptkd_Plant"));
                                            cv.put("ptkd_ValueStreamCode",obj.getString("ptkd_ValueStreamCode"));
                                            cv.put("ptkd_Ftypecode",obj.getString("ptkd_Ftypecode"));
                                            cv.put("ptkd_Ftypecode",obj.getString("ptkd_Ftypecode"));
                                            String total_qty = obj.getString("ptkd_Qty");
                                            double constant = 12.0;
                                            try
                                            {
                                                double result = Double.parseDouble(total_qty) * constant;
                                                String qty = String.valueOf(result);
                                                cv.put("ptkd_Qty",qty);

                                            }
                                            catch (Exception e)
                                            {
                                                e.printStackTrace();
                                                cv.put("ptkd_Qty",total_qty);


                                            }
                                            cv.put("ptkd_IsRelease",obj.getString("ptkd_IsRelease"));
                                            cv.put("tk_Actual_Start_Date",obj.getString("tk_Actual_Start_Date"));
                                            cv.put("tk_Actual_End_Date",obj.getString("tk_Actual_End_Date"));
                                            cv.put("ptkd_IsRelease1",obj.getString("ptkd_IsRelease1"));
                                            cv.put("ptkd_ReleaseDate","null");
                                            cv.put("ptkd_Active",obj.getString("ptkd_Active"));
                                            cv.put("tk_End_Date",obj.getString("tk_End_Date"));

                                            String ticketno = obj.getString("tk_TicketNo");
                                            String q = "SELECT * FROM "+Constants.PRODUCTION_TICKET_TABLE+" where "+Constants.PACKING_TICKET_TICKET_NO+"='"+ticketno+"'";
                                            Cursor c = db.rawQuery(q,null);
                                            if(c.moveToFirst())
                                            {
                                                //showMessage("Error", "Record exist");
                                            }
                                            else
                                            {
                                                long d=db.insert(Constants.PRODUCTION_TICKET_TABLE,null,cv);
                                                Log.d("user check", String.valueOf(d));
                                            }
                                        }
                                        else
                                        {

                                            DataBaseHelper helper=new DataBaseHelper(FalcoJKHomeActivity.this);
                                            SQLiteDatabase db = helper.getWritableDatabase();
                                            ContentValues cv=new ContentValues();
                                            cv.put("ptkd_PktId",obj.getString("ptkd_PktId"));
                                            cv.put("ptkd_TkNo",obj.getString("ptkd_TkNo"));
                                            cv.put("ptkd_PSKU",obj.getString("ptkd_PSKU"));
                                            cv.put("ptkd_Plant",obj.getString("ptkd_Plant"));
                                            cv.put("ptkd_ValueStreamCode",obj.getString("ptkd_ValueStreamCode"));
                                            cv.put("ptkd_Ftypecode",obj.getString("ptkd_Ftypecode"));
                                            cv.put("ptkd_Ftypecode",obj.getString("ptkd_Ftypecode"));
                                            String total_qty = obj.getString("ptkd_Qty");
                                            double constant = 12.0;
                                            try
                                            {
                                                double result = Double.parseDouble(total_qty) * constant;
                                                String qty = String.valueOf(result);
                                                cv.put("ptkd_Qty",qty);

                                            }
                                            catch (Exception e)
                                            {
                                                e.printStackTrace();
                                                cv.put("ptkd_Qty",total_qty);


                                            }
                                            cv.put("ptkd_IsRelease",obj.getString("ptkd_IsRelease"));
                                            cv.put("tk_Actual_Start_Date",obj.getString("tk_Actual_Start_Date"));
                                            cv.put("tk_Actual_End_Date",obj.getString("tk_Actual_End_Date"));
                                            cv.put("ptkd_IsRelease1",obj.getString("ptkd_IsRelease1"));
                                            cv.put("ptkd_ReleaseDate","null");
                                            cv.put("ptkd_Active",obj.getString("ptkd_Active"));
                                            cv.put("tk_End_Date","null");

                                            String ticketno = obj.getString("ptkd_TkNo");
                                            String q = "SELECT * FROM "+Constants.PRODUCTION_TICKET_TABLE+" where "+Constants.PRODUCTION_TICKET_NO+"='"+ticketno+"'";
                                            Cursor c = db.rawQuery(q,null);
                                            if(c.moveToFirst())
                                            {
                                                //showMessage("Error", "Record exist");
                                            }
                                            else
                                            {
                                                long d=db.insert(Constants.PRODUCTION_TICKET_TABLE,null,cv);
                                                Log.d("user check", String.valueOf(d));
                                            }
                                        }
                                    }
                                }

                            }

                        }
                    }
                    else
                    {
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(FalcoJKHomeActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getproductionTicketDetailsUsingStartButton(String strProductionTkNo)
    {
        /*progressDialog=new ProgressDialog(FalcoJKHomeActivity.this);
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);
        progressDialog.show();*/
        API api = Retroconfig.retrofit().create(API.class);
        // dateformat1 = "2020-02-07";
        //dateformat1 = sdf1.format(dateformat1);
        String str_tk_id= "";
        String flag = "Startbtn";
        Call<ResponseBody> call = api.getProductionTicketsDetailsonclickofstartbutton(str_tk_id,flag);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if(response.body()!=null) {
                        String res = response.body().string();
                        if (res!=null){
                            // progressDialog.dismiss();

                            try {
                                JSONObject jsonObject=new JSONObject(res);
                                JSONArray array=jsonObject.getJSONArray("Data");
                                ticketmodel.clear();
                                if (array.length()==0)
                                {
                                    /*Toast toast = Toast.makeText(getApplicationContext(),
                                            "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                                    toast.show();*/
                                }

                                else {

                                    for (int i=0;i<array.length();i++) {
                                        JSONObject obj = new JSONObject(array.get(i).toString());
                                        ProdTkOperationModel model = new ProdTkOperationModel();

                                        if (obj.has("tk_Actual_End_Date"))
                                        {
                                            model.setPtk_PtkdId(obj.getInt("ptkd_Id"));
                                            model.setPtkd_TkNo(obj.getString("ptkd_TkNo"));
                                            model.setPtkd_PSKU(obj.getString("ptkd_PSKU"));
                                            model.setPtkd_Plant(obj.getString("ptkd_Plant"));
                                            model.setPtkd_ValueStreamCode(obj.getString("ptkd_ValueStreamCode"));
                                            model.setPtkd_Ftypecode(obj.getString("ptkd_Ftypecode"));
                                            model.setPtkd_Operation(obj.getInt("ptkd_Operation"));
                                            model.setPtkd_Qty(obj.getInt("ptkd_Qty"));
                                            model.setPtkd_IsRelease(obj.getString("ptkd_IsRelease"));
                                            model.setUni_unit_name(obj.getString("uni_unit_name"));
                                            model.setOpr_operation_name(obj.getString("opr_operation_name"));
                                            model.setVal_valuestream_name(obj.getString("val_valuestream_name"));
                                            model.setFt_ftype_desc(obj.getString("ft_ftype_desc"));
                                            model.setTk_Actual_Start_Date(obj.getString("tk_Actual_Start_Date"));
                                            model.setTk_Actual_End_Date(obj.getString("tk_Actual_End_Date"));
                                            model.setPtkd_ReleaseDate(obj.getString("ptkd_ReleaseDate"));
                                            model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                                            model.setTk_End_Date(obj.getString("tk_End_Date"));
                                            model.setFlag(obj.getString("flag"));
                                            model.setVsCode(obj.getString("ptkd_ValueStreamCode"));
                                            model.setPtk_ChangeRoute_Flag("C");

                                            ContentValues cv = new ContentValues();
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_PACKET_ID,obj.getInt("ptkd_Id"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_TICKET_NO,obj.getString("ptkd_TkNo"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_PLANT,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_VALUE_STREAM_CODE,obj.getString("ptkd_ValueStreamCode"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_OPERATION,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_UPDATEDATE,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_OK_QTY,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_REJECT_QTY,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_REWORK_QTY,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_WIP_QTY,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_SECOND_QTY,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_OPERATION_NAME,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_QTY,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_UNIT_NAME,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_VALUESTREAM_NAME,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_TICKET_ACTUAL_START_DATE,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_TICKET_ACTUAL_END_DATE,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_TICKET_START_DATE,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_TICKET_END_DATE,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_CHANGE_ROUTE_FLAG,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_CREATED_BY,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_CREATED_DATE,obj.getString("ptkd_Plant"));

                                            long d=db.insert(Constants.PRODUCTION_TICKET_DETAILS_TABLE,null,cv);
                                            Log.d("user check", String.valueOf(d));

                                            ticketmodel.add(model);
                                        }

                                        else
                                        {
                                            model.setPtk_PtkdId(obj.getInt("ptkd_Id"));
                                            model.setPtkd_TkNo(obj.getString("ptkd_TkNo"));
                                            model.setPtkd_PSKU(obj.getString("ptkd_PSKU"));
                                            model.setPtkd_Plant(obj.getString("ptkd_Plant"));
                                            model.setPtkd_ValueStreamCode(obj.getString("ptkd_ValueStreamCode"));
                                            model.setPtkd_Ftypecode(obj.getString("ptkd_Ftypecode"));
                                            model.setPtkd_Operation(obj.getInt("ptkd_Operation"));
                                            model.setPtkd_Qty(obj.getInt("ptkd_Qty"));
                                            model.setPtkd_IsRelease(obj.getString("ptkd_IsRelease"));
                                            model.setUni_unit_name(obj.getString("uni_unit_name"));
                                            model.setOpr_operation_name(obj.getString("opr_operation_name"));
                                            model.setVal_valuestream_name(obj.getString("val_valuestream_name"));
                                            model.setFt_ftype_desc(obj.getString("ft_ftype_desc"));
                                            model.setTk_Actual_Start_Date(obj.getString("tk_Actual_Start_Date"));
                                            model.setTk_Actual_End_Date("null");
                                            model.setPtkd_ReleaseDate(obj.getString("ptkd_ReleaseDate"));
                                            model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                                            model.setTk_End_Date(obj.getString("tk_End_Date"));
                                            model.setFlag(obj.getString("flag"));
                                            model.setVsCode(obj.getString("ptkd_ValueStreamCode"));
                                            model.setPtk_ChangeRoute_Flag("C");
                                            ticketmodel.add(model);
                                            ContentValues cv = new ContentValues();
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_PACKET_ID,obj.getInt("ptkd_Id"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_TICKET_NO,obj.getString("ptkd_TkNo"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_PLANT,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_VALUE_STREAM_CODE,obj.getString("ptkd_ValueStreamCode"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_OPERATION,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_UPDATEDATE,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_OK_QTY,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_REJECT_QTY,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_REWORK_QTY,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_WIP_QTY,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_SECOND_QTY,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_OPERATION_NAME,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_QTY,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_UNIT_NAME,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_VALUESTREAM_NAME,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_TICKET_ACTUAL_START_DATE,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_TICKET_ACTUAL_END_DATE,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_TICKET_START_DATE,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_TICKET_END_DATE,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_CHANGE_ROUTE_FLAG,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_CREATED_BY,obj.getString("ptkd_Plant"));
                                            cv.put(Constants.PRODUCTION_TICKET_DETAILS_CREATED_DATE,obj.getString("ptkd_Plant"));


                                            long d=db.insert(Constants.PRODUCTION_TICKET_DETAILS_TABLE,null,cv);
                                            Log.d("user check", String.valueOf(d));
                                        }



                                    }


                                }
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    private void getSkuDetails(final String tkno)
    {
        /*progressDialog=new ProgressDialog(FalcoJKHomeActivity.this);
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);
        progressDialog.show();*/
        API api = Retroconfig.retrofit().create(API.class);
        Call<ResponseBody> call = api.getSkuDetailsAccordingtoticket(strproductionTkID);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if(response.body()!=null) {
                        String res = response.body().string();
                        if (res!=null){
                            //progressDialog.dismiss();

                            try {
                                JSONObject jsonObject=new JSONObject(res);
                                JSONArray array=jsonObject.getJSONArray("Data");

                                if (array.length()!=0){
                                    for (int i=0;i<array.length();i++)
                                    {
                                        JSONObject obj=new JSONObject(array.get(i).toString());
                                        ContentValues cv=new ContentValues();
                                        if(obj.has("SKU_No"))
                                        {
                                            cv.put("SKU_No",obj.getString("SKU_No"));
                                        }
                                        else
                                        {
                                            cv.put("SKU_No","-");
                                        }
                                        if (obj.has("dp_stamp_type")){
                                            cv.put("dp_stamp_type",obj.getString("dp_stamp_type"));
                                        }
                                        else {   cv.put("dp_stamp_type","-");
                                        }
                                        if (obj.has("dp_stamp_type")){
                                            cv.put("dp_stamp_chart",obj.getString("dp_stamp_chart"));
                                        }
                                        else {  cv.put("dp_stamp_chart","-");

                                        }
                                        if (obj.has("dp_blko_flg")){
                                            cv.put("dp_blko_flg",obj.getString("dp_blko_flg"));
                                        }
                                        else {  cv.put("dp_blko_flg","-");

                                        }
                                        if (obj.has("dp_tang_color")){
                                            cv.put("dp_tang_color",obj.getString("dp_tang_color"));
                                        }
                                        else {  cv.put("dp_tang_color","-");

                                        }
                                        if (obj.has("QTY")){
                                            cv.put("QTY",obj.getString("QTY"));
                                        }
                                        else {  cv.put("QTY","-");

                                        }
                                        if (obj.has("OrgQTY")){
                                            cv.put("OrgQTY",obj.getString("OrgQTY"));
                                        }
                                        else {  cv.put("OrgQTY","-");

                                        }

                                        cv.put("ptkd_TkNo",tkno);
                                        String SKU_No = obj.getString("SKU_No");
                                        long d=db.insert(Constants.PRODUCTION_TICKET_MULTIPLE_SKU_TABLE,null,cv);
                                        Log.d("user check", String.valueOf(d));

                                           /* String q = "SELECT * FROM "+Constants.PRODUCTION_TICKET_MULTIPLE_SKU_TABLE+" where "+Constants.PRODUCTION_TICKET_MULTIPLE_SKU_PTKD_SKU_NO+"='"+SKU_No+"'";
                                            Cursor c = db.rawQuery(q,null);
                                            if(c.moveToFirst())
                                            {
                                                //showMessage("Error", "Record exist");
                                            }
                                            else
                                            {
                                                long d=db.insert(Constants.PRODUCTION_TICKET_MULTIPLE_SKU_TABLE,null,cv);
                                                Log.d("user check", String.valueOf(d));
                                            }*/
                                    }

                                }
                                else
                                {


                                }

                            }


                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    private void getWIPAgainstTickets(String tkno)
    {
       /* progressDialog=new ProgressDialog(FalcoJKHomeActivity.this);
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);
        progressDialog.show();*/
        API api = Retroconfig.retrofit().create(API.class);
        Call<ResponseBody> call = api.getEnterWipData(tkno);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if(response.body()!=null)
                    {
                        String res = response.body().string();
                        if (res!=null){
                            //progressDialog.dismiss();
                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("Data");
                            if (array.length()!=0){
                                for (int i = 0 ; i < array.length() ; i++){
                                    JSONObject obj = array.getJSONObject(i);

                                    ContentValues cv = new ContentValues();
                                    cv.put("ptkd_Id",obj.getString("ptkd_Id"));
                                    cv.put("ptkd_PktId",obj.getString("ptkd_PktId"));
                                    cv.put("ptkd_TkNo",obj.getString("ptkd_TkNo"));
                                    cv.put("ptkd_PSKU",obj.getString("ptkd_PSKU"));
                                    cv.put("ptkd_Plant",obj.getString("ptkd_Plant"));
                                    cv.put("ptkd_ValueStreamCode",obj.getString("ptkd_ValueStreamCode"));
                                    cv.put("ptkd_Operation",obj.getString("ptkd_Operation"));
                                    cv.put("opr_operation_name",obj.getString("opr_operation_name"));
                                    cv.put("uni_unit_name",obj.getString("uni_unit_name"));
                                    cv.put("ptkd_Qty",obj.getString("ptkd_Qty"));
                                    if (obj.has("ptkd_WIP"))
                                    {
                                        cv.put("ptkd_WIP",obj.getString("ptkd_WIP"));

                                    }
                                    else
                                    {
                                        cv.put("ptkd_WIP","0");
                                    }
                                   /* long d=db.insert(Constants.WIP_TABLE,null,cv);
                                    Log.d("user check", String.valueOf(d));*/
                                    String ticketno = obj.getString("ptkd_TkNo");
                                    String ptkd_Id = obj.getString("ptkd_Id");
                                    String q = "SELECT * FROM "+Constants.WIP_TABLE+" where "+Constants.WIP_PTKD_TICKET_NO+"='"+ticketno+"'"+" and "+ Constants.WIP_PTKD_ID + "='" + ptkd_Id + "'";
                                    Cursor c = db.rawQuery(q,null);
                                    if(c.moveToFirst())
                                    {
                                        //showMessage("Error", "Record exist");
                                    }
                                    else
                                    {
                                        long d=db.insert(Constants.WIP_TABLE,null,cv);
                                        Log.d("user check", String.valueOf(d));
                                    }

                                }
                            }


                        }
                        else {
                        }
                    }

                }
                catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    private void getPackingTicketsDatewiseQuanitity()
    {
        API api = Retroconfig.retrofit().create(API.class);
        Call<ResponseBody> call = api.getPackingticketDatewiseQuantity(strpackingTkID);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            // progressDialog.dismiss();

                            JSONObject jsonObject=new JSONObject(res);
                            JSONArray array=jsonObject.getJSONArray("Data");
                            if (array.length()==0){
                                /*Toast toast = Toast.makeText(getApplicationContext(),
                                        "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                                toast.show();*/
                            }
                            else {
                                ArrayList<PackingTicketModel> ticketmodel= new ArrayList<>();

                                for (int i=0;i<array.length();i++)
                                {

                                    JSONObject obj=new JSONObject(array.get(i).toString());
                                    ContentValues cv=new ContentValues();
                                    cv.put("tt_id",obj.getString("tt_id"));
                                    cv.put("tt_TicketID",obj.getString("tt_TicketID"));
                                    cv.put("tt_TicketNo",obj.getString("tt_TicketNo"));
                                    cv.put("tt_Plant",obj.getString("tt_Plant"));
                                    cv.put("tt_Date",obj.getString("tt_Date"));
                                    cv.put("tt_TotalQuantity",obj.getString("tt_TotalQuantity"));
                                    cv.put("tt_Qty",obj.getString("tt_Qty"));
                                    cv.put("tt_PendingQty",obj.getString("tt_PendingQty"));
                                    cv.put("tt_CreatedDate",obj.getString("tt_CreatedDate"));
                                    cv.put("tt_CreatedBy",obj.getString("tt_CreatedBy"));
                                    cv.put("tt_ApprovedDate",obj.getString("tt_ApprovedDate"));
                                    cv.put("tt_ApprovedBy",obj.getString("tt_ApprovedBy"));
                                    cv.put("tt_Active",obj.getString("tt_Active"));
                                    cv.put("tt_Approve",obj.getString("tt_Approve"));
                                    cv.put("flag1",obj.getString("flag1"));
                                    cv.put("tk_SKU",obj.getString("tk_SKU"));
                                    cv.put("tk_Start_Date",obj.getString("tk_Start_Date"));
                                    cv.put("tk_End_Date",obj.getString("tk_End_Date"));
                                    cv.put("actualstartdate",obj.getString("actualstartdate"));
                                    cv.put("today",obj.getString("today"));
                                    cv.put("actualenddate",obj.getString("actualenddate"));


                                    String tt_id = obj.getString("tt_id");

                                    String q = "SELECT * FROM "+Constants.DATE_WISE_QUANTITY_PACKING_TABLE+" where "+Constants.DATE_WISE_QUANTITY_PACKING_TICKET_ID+"='"+tt_id+"'";
                                    Cursor c = db.rawQuery(q,null);
                                    if(c.moveToFirst())
                                    {
                                        //showMessage("Error", "Record exist");
                                    }
                                    else
                                    {
                                        long d=db.insert(Constants.DATE_WISE_QUANTITY_PACKING_TABLE,null,cv);
                                        Log.d("user check", String.valueOf(d));
                                    }



                                }

                            }

                        }
                    }
                    else
                    {
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(FalcoJKHomeActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getEquipmentdetails()
    {
        API api = Retroconfig.retrofit().create(API.class);
        Call<ResponseBody> call = api.getPackingticketDatewiseQuantity(strpackingTkID);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            // progressDialog.dismiss();

                            JSONObject jsonObject=new JSONObject(res);
                            JSONArray array=jsonObject.getJSONArray("Data");
                            if (array.length()==0){
                                /*Toast toast = Toast.makeText(getApplicationContext(),
                                        "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                                toast.show();*/
                            }
                            else {
                                ArrayList<PackingTicketModel> ticketmodel= new ArrayList<>();

                                for (int i=0;i<array.length();i++)
                                {

                                    JSONObject obj=new JSONObject(array.get(i).toString());
                                    ContentValues cv=new ContentValues();
                                    cv.put("tt_id",obj.getString("tt_id"));
                                    cv.put("tt_TicketID",obj.getString("tt_TicketID"));
                                    cv.put("tt_TicketNo",obj.getString("tt_TicketNo"));
                                    cv.put("tt_Plant",obj.getString("tt_Plant"));
                                    cv.put("tt_Date",obj.getString("tt_Date"));
                                    cv.put("tt_TotalQuantity",obj.getString("tt_TotalQuantity"));
                                    cv.put("tt_Qty",obj.getString("tt_Qty"));
                                    cv.put("tt_PendingQty",obj.getString("tt_PendingQty"));
                                    cv.put("tt_CreatedDate",obj.getString("tt_CreatedDate"));
                                    cv.put("tt_CreatedBy",obj.getString("tt_CreatedBy"));
                                    cv.put("tt_ApprovedDate",obj.getString("tt_ApprovedDate"));
                                    cv.put("tt_ApprovedBy",obj.getString("tt_ApprovedBy"));
                                    cv.put("tt_Active",obj.getString("tt_Active"));
                                    cv.put("tt_Approve",obj.getString("tt_Approve"));
                                    cv.put("flag1",obj.getString("flag1"));
                                    cv.put("tk_SKU",obj.getString("tk_SKU"));
                                    cv.put("tk_Start_Date",obj.getString("tk_Start_Date"));
                                    cv.put("tk_End_Date",obj.getString("tk_End_Date"));
                                    cv.put("actualstartdate",obj.getString("actualstartdate"));
                                    cv.put("today",obj.getString("today"));
                                    cv.put("actualenddate",obj.getString("actualenddate"));


                                    String tt_id = obj.getString("tt_id");

                                    String q = "SELECT * FROM "+Constants.DATE_WISE_QUANTITY_PACKING_TABLE+" where "+Constants.DATE_WISE_QUANTITY_PACKING_TICKET_ID+"='"+tt_id+"'";
                                    Cursor c = db.rawQuery(q,null);
                                    if(c.moveToFirst())
                                    {
                                        //showMessage("Error", "Record exist");
                                    }
                                    else
                                    {
                                        long d=db.insert(Constants.DATE_WISE_QUANTITY_PACKING_TABLE,null,cv);
                                        Log.d("user check", String.valueOf(d));
                                    }



                                }

                            }

                        }
                    }
                    else
                    {
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(FalcoJKHomeActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void insertReasonsInMaster()
    {
        ArrayList<String> reasonarray_list = new ArrayList<>();
        reasonarray_list.add("Breakdown");
        reasonarray_list.add("Abseentism");
        reasonarray_list.add("Quality");
        reasonarray_list.add("Quality");
        reasonarray_list.add("Setting time");
        reasonarray_list.add("Feed not available");
        reasonarray_list.add("Other");
        for (int i =0;i<reasonarray_list.size();i++)
        {
            ContentValues cv = new ContentValues();
            cv.put(Constants.REASON_MASTER_NAME,reasonarray_list.get(i));
            cv.put(Constants.REASON_MASTER_ID,i);
            long d=db.insert(Constants.REASON_MASTER_TABLE,null,cv);
            Log.d("user check", String.valueOf(d));

        }

    }

    private void getEquipmentMaster()
    {
        API api = Retroconfig.retrofit().create(API.class);
        Call<ResponseBody> call = api.getEquipmentMaster();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            // progressDialog.dismiss();

                            JSONObject jsonObject=new JSONObject(res);
                            JSONArray array=jsonObject.getJSONArray("Data");
                            if (array.length()==0){
                                /*Toast toast = Toast.makeText(getApplicationContext(),
                                        "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                                toast.show();*/
                            }
                            else {
                                ArrayList<PackingTicketModel> ticketmodel= new ArrayList<>();

                                for (int i=0;i<array.length();i++)
                                {

                                    JSONObject obj=new JSONObject(array.get(i).toString());
                                    ContentValues cv=new ContentValues();
                                    cv.put("eq_id",obj.getString("eq_id"));
                                    cv.put("eq_name",obj.getString("eq_name"));
                                    cv.put("eq_type",obj.getString("eq_type"));
                                    cv.put("eq_remarks",obj.getString("eq_remarks"));
                                    cv.put("eq_isApproved",obj.getString("eq_isApproved"));
                                    cv.put("eq_isActive",obj.getString("eq_isActive"));
                                    cv.put("eq_createdate",obj.getString("eq_createdate"));
                                    cv.put("eq_createby",obj.getString("eq_createby"));
                                    cv.put("eq_approvedate",obj.getString("eq_approvedate"));
                                    cv.put("eq_approveby",obj.getString("eq_approveby"));



                                    String eq_id = obj.getString("eq_id");

                                    String q = "SELECT * FROM "+Constants.EQUPMENT_MASTER_TABLE+" where "+Constants.EQUPMENT_ID+"='"+eq_id+"'";
                                    Cursor c = db.rawQuery(q,null);
                                    if(c.moveToFirst())
                                    {
                                        //showMessage("Error", "Record exist");
                                    }
                                    else
                                    {
                                        long d=db.insert(Constants.EQUPMENT_MASTER_TABLE,null,cv);
                                        Log.d("user check", String.valueOf(d));
                                    }



                                }

                            }

                        }
                    }
                    else
                    {
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(FalcoJKHomeActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getAssetMaster()
    {
        API api = Retroconfig.retrofit().create(API.class);
        Call<ResponseBody> call = api.getAssetMaster();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            // progressDialog.dismiss();

                            JSONObject jsonObject=new JSONObject(res);
                            JSONArray array=jsonObject.getJSONArray("Data");
                            if (array.length()==0){
                                /*Toast toast = Toast.makeText(getApplicationContext(),
                                        "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                                toast.show();*/
                            }
                            else {
                                ArrayList<PackingTicketModel> ticketmodel= new ArrayList<>();

                                for (int i=0;i<array.length();i++)
                                {

                                    JSONObject obj=new JSONObject(array.get(i).toString());
                                    ContentValues cv=new ContentValues();
                                    cv.put("ea_asset_no",obj.getString("ea_asset_no"));
                                    cv.put("ea_equip_id",obj.getString("ea_equip_id"));
                                    cv.put("ea_createby",obj.getString("ea_createby"));
                                    cv.put("ea_createdate",obj.getString("ea_createdate"));
                                    cv.put("ea_isActive",obj.getString("ea_isActive"));
                                    cv.put("ea_isApproved",obj.getString("ea_isApproved"));
                                    cv.put("ea_approvedby",obj.getString("ea_approvedby"));
                                    cv.put("ea_approvedate",obj.getString("ea_approvedate"));

                                    String ea_asset_no = obj.getString("ea_asset_no");

                                    String q = "SELECT * FROM "+Constants.ASSET_NO_MASTER_TABLE+" where "+Constants.ASSET_NO+"='"+ea_asset_no+"'";
                                    Cursor c = db.rawQuery(q,null);
                                    if(c.moveToFirst())
                                    {
                                        //showMessage("Error", "Record exist");
                                    }
                                    else
                                    {
                                        long d=db.insert(Constants.ASSET_NO_MASTER_TABLE,null,cv);
                                        Log.d("user check", String.valueOf(d));
                                    }



                                }

                            }

                        }
                    }
                    else
                    {
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(FalcoJKHomeActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }
   
   
    /*private void getOperationListAgainstTicket(String TicketNo)
    {
        API api = Retroconfig.retrofit().create(API.class);
        Call<ResponseBody> call = api.getOperationListAgainstTicket(TicketNo);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            // progressDialog.dismiss();

                            JSONObject jsonObject=new JSONObject(res);
                            JSONArray array=jsonObject.getJSONArray("Data");
                            if (array.length()==0){
                                *//*Toast toast = Toast.makeText(getApplicationContext(),
                                        "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                                toast.show();*//*
                            }
                            else {
                                ArrayList<PackingTicketModel> ticketmodel= new ArrayList<>();

                                for (int i=0;i<array.length();i++)
                                {

                                    JSONObject obj=new JSONObject(array.get(i).toString());
                                    ContentValues cv=new ContentValues();
                                    cv.put("ptkd_Id",obj.getString("ptkd_Id"));
                                    cv.put("ptkd_PktId",obj.getString("ptkd_PktId"));
                                    cv.put("ptkd_TkNo",obj.getString("ptkd_TkNo"));
                                    cv.put("ptkd_PSKU",obj.getString("ptkd_PSKU"));
                                    cv.put("ptkd_Plant",obj.getString("ptkd_Plant"));
                                    cv.put("ptkd_ValueStreamCode",obj.getString("ptkd_ValueStreamCode"));
                                    cv.put("ptkd_Ftypecode",obj.getString("ptkd_Operation"));
                                    cv.put("ptkd_Qty",obj.getString("ptkd_Qty"));
                                    cv.put("ptkd_CreatedBy",obj.getString("ptkd_CreatedBy"));
                                    cv.put("ptkd_CreatedDate",obj.getString("ptkd_CreatedDate"));
                                    cv.put("ptkd_ApprovedBy",obj.getString("ptkd_ApprovedBy"));
                                    cv.put("ptkd_ApproveDate",obj.getString("ptkd_ApproveDate"));
                                    cv.put("ptkd_ReleaseDate",obj.getString("ptkd_ReleaseDate"));
                                    cv.put("ptkd_IsRelease",obj.getString("ptkd_IsRelease"));
                                    cv.put("ptkd_Active",obj.getString("ptkd_Active"));

                                    String ptkd_Id = obj.getString("ptkd_Id");

                                    String q = "SELECT * FROM "+Constants.TICKET_WISE_OPERATION_LIST+" where "+Constants.TICKET_WISE_OPERATION_PTKD_ID+"='"+ptkd_Id+"'";
                                    Cursor c = db.rawQuery(q,null);
                                    if(c.moveToFirst())
                                    {
                                        //showMessage("Error", "Record exist");
                                    }
                                    else
                                    {
                                        long d=db.insert(Constants.TICKET_WISE_OPERATION_LIST,null,cv);
                                        Log.d("user check", String.valueOf(d));
                                    }



                                }

                            }

                        }
                    }
                    else
                    {
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(FalcoJKHomeActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }*/




}
