package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

public class UserModel {
    String user_token;
    String user_id;
    String user_email;
    String user_role;

    public String getUser_oem_id() {
        return user_oem_id;
    }

    public void setUser_oem_id(String user_oem_id) {
        this.user_oem_id = user_oem_id;
    }

    String user_oem_id;

    public String getUser_token() {
        return user_token;
    }

    public void setUser_token(String user_token) {
        this.user_token = user_token;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_role() {
        return user_role;
    }

    public void setUser_role(String user_role) {
        this.user_role = user_role;
    }
}
