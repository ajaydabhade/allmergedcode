package com.brainlines.weightloggernewversion.Production.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Production_Reason_Table implements Parcelable {
    String ReasonID,ReasonDesc,CreatedDate,ModifiedDate;

    public Production_Reason_Table(String reasonID, String reasonDesc, String createdDate, String modifiedDate) {
        ReasonID = reasonID;
        ReasonDesc = reasonDesc;
        CreatedDate = createdDate;
        ModifiedDate = modifiedDate;
    }
    public Production_Reason_Table() {

    }

    public String getReasonID() {
        return ReasonID;
    }

    public void setReasonID(String reasonID) {
        ReasonID = reasonID;
    }

    public String getReasonDesc() {
        return ReasonDesc;
    }

    public void setReasonDesc(String reasonDesc) {
        ReasonDesc = reasonDesc;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public static Creator<Production_Reason_Table> getCREATOR() {
        return CREATOR;
    }

    protected Production_Reason_Table(Parcel in) {
        ReasonID = in.readString();
        ReasonDesc = in.readString();
        CreatedDate = in.readString();
        ModifiedDate = in.readString();
    }

    public static final Creator<Production_Reason_Table> CREATOR = new Creator<Production_Reason_Table>() {
        @Override
        public Production_Reason_Table createFromParcel(Parcel in) {
            return new Production_Reason_Table(in);
        }

        @Override
        public Production_Reason_Table[] newArray(int size) {
            return new Production_Reason_Table[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ReasonID);
        dest.writeString(ReasonDesc);
        dest.writeString(CreatedDate);
        dest.writeString(ModifiedDate);
    }
}
