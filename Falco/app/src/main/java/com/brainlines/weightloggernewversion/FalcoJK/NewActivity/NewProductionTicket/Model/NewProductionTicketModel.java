package com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class NewProductionTicketModel implements Parcelable {
    String ptkd_PktId1,ptkd_PktId,ptkd_TkNo,ptkd_PSKU,ptkd_Plant,ptkd_ValueStreamCode,ptkd_Ftypecode,ptkd_Qty,ptkd_IsRelease,tk_Actual_Start_Date;
    String tk_Actual_End_Date,ptkd_IsRelease1,ptkd_ReleaseDate,ptkd_Active,tk_End_Date;

    protected NewProductionTicketModel(Parcel in) {
        ptkd_PktId1 = in.readString();
        ptkd_PktId = in.readString();
        ptkd_TkNo = in.readString();
        ptkd_PSKU = in.readString();
        ptkd_Plant = in.readString();
        ptkd_ValueStreamCode = in.readString();
        ptkd_Ftypecode = in.readString();
        ptkd_Qty = in.readString();
        ptkd_IsRelease = in.readString();
        tk_Actual_Start_Date = in.readString();
        tk_Actual_End_Date = in.readString();
        ptkd_IsRelease1 = in.readString();
        ptkd_ReleaseDate = in.readString();
        ptkd_Active = in.readString();
        tk_End_Date = in.readString();
    }

    public static final Creator<NewProductionTicketModel> CREATOR = new Creator<NewProductionTicketModel>() {
        @Override
        public NewProductionTicketModel createFromParcel(Parcel in) {
            return new NewProductionTicketModel(in);
        }

        @Override
        public NewProductionTicketModel[] newArray(int size) {
            return new NewProductionTicketModel[size];
        }
    };

    public NewProductionTicketModel() {

    }

    public String getPtkd_PktId1() {
        return ptkd_PktId1;
    }

    public void setPtkd_PktId1(String ptkd_PktId1) {
        this.ptkd_PktId1 = ptkd_PktId1;
    }

    public String getPtkd_PktId() {
        return ptkd_PktId;
    }

    public void setPtkd_PktId(String ptkd_PktId) {
        this.ptkd_PktId = ptkd_PktId;
    }

    public String getPtkd_TkNo() {
        return ptkd_TkNo;
    }

    public void setPtkd_TkNo(String ptkd_TkNo) {
        this.ptkd_TkNo = ptkd_TkNo;
    }

    public String getPtkd_PSKU() {
        return ptkd_PSKU;
    }

    public void setPtkd_PSKU(String ptkd_PSKU) {
        this.ptkd_PSKU = ptkd_PSKU;
    }

    public String getPtkd_Plant() {
        return ptkd_Plant;
    }

    public void setPtkd_Plant(String ptkd_Plant) {
        this.ptkd_Plant = ptkd_Plant;
    }

    public String getPtkd_ValueStreamCode() {
        return ptkd_ValueStreamCode;
    }

    public void setPtkd_ValueStreamCode(String ptkd_ValueStreamCode) {
        this.ptkd_ValueStreamCode = ptkd_ValueStreamCode;
    }

    public String getPtkd_Ftypecode() {
        return ptkd_Ftypecode;
    }

    public void setPtkd_Ftypecode(String ptkd_Ftypecode) {
        this.ptkd_Ftypecode = ptkd_Ftypecode;
    }

    public String getPtkd_Qty() {
        return ptkd_Qty;
    }

    public void setPtkd_Qty(String ptkd_Qty) {
        this.ptkd_Qty = ptkd_Qty;
    }

    public String getPtkd_IsRelease() {
        return ptkd_IsRelease;
    }

    public void setPtkd_IsRelease(String ptkd_IsRelease) {
        this.ptkd_IsRelease = ptkd_IsRelease;
    }

    public String getTk_Actual_Start_Date() {
        return tk_Actual_Start_Date;
    }

    public void setTk_Actual_Start_Date(String tk_Actual_Start_Date) {
        this.tk_Actual_Start_Date = tk_Actual_Start_Date;
    }

    public String getTk_Actual_End_Date() {
        return tk_Actual_End_Date;
    }

    public void setTk_Actual_End_Date(String tk_Actual_End_Date) {
        this.tk_Actual_End_Date = tk_Actual_End_Date;
    }

    public String getPtkd_IsRelease1() {
        return ptkd_IsRelease1;
    }

    public void setPtkd_IsRelease1(String ptkd_IsRelease1) {
        this.ptkd_IsRelease1 = ptkd_IsRelease1;
    }

    public String getPtkd_ReleaseDate() {
        return ptkd_ReleaseDate;
    }

    public void setPtkd_ReleaseDate(String ptkd_ReleaseDate) {
        this.ptkd_ReleaseDate = ptkd_ReleaseDate;
    }

    public String getPtkd_Active() {
        return ptkd_Active;
    }

    public void setPtkd_Active(String ptkd_Active) {
        this.ptkd_Active = ptkd_Active;
    }

    public String getTk_End_Date() {
        return tk_End_Date;
    }

    public void setTk_End_Date(String tk_End_Date) {
        this.tk_End_Date = tk_End_Date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ptkd_PktId1);
        dest.writeString(ptkd_PktId);
        dest.writeString(ptkd_TkNo);
        dest.writeString(ptkd_PSKU);
        dest.writeString(ptkd_Plant);
        dest.writeString(ptkd_ValueStreamCode);
        dest.writeString(ptkd_Ftypecode);
        dest.writeString(ptkd_Qty);
        dest.writeString(ptkd_IsRelease);
        dest.writeString(tk_Actual_Start_Date);
        dest.writeString(tk_Actual_End_Date);
        dest.writeString(ptkd_IsRelease1);
        dest.writeString(ptkd_ReleaseDate);
        dest.writeString(ptkd_Active);
        dest.writeString(tk_End_Date);
    }
}
