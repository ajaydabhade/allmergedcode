package com.brainlines.weightloggernewversion.FalcoSwas.SwasListener;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.TravelExpensesKeyValueModel;

public interface TravelExpensesListener {

    void gettravel_expenses(TravelExpensesKeyValueModel model);
}
