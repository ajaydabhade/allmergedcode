package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

import android.os.Parcel;
import android.os.Parcelable;

public class ExpenseDetailsTableModel implements Parcelable {

    String customerID;
    String serviceCallID;

    public ExpenseDetailsTableModel(String customerID, String serviceCallID) {
        this.customerID = customerID;
        this.serviceCallID = serviceCallID;
    }

    protected ExpenseDetailsTableModel(Parcel in) {
        customerID = in.readString();
        serviceCallID = in.readString();
    }

    public static final Creator<ExpenseDetailsTableModel> CREATOR = new Creator<ExpenseDetailsTableModel>() {
        @Override
        public ExpenseDetailsTableModel createFromParcel(Parcel in) {
            return new ExpenseDetailsTableModel(in);
        }

        @Override
        public ExpenseDetailsTableModel[] newArray(int size) {
            return new ExpenseDetailsTableModel[size];
        }
    };

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getServiceCallID() {
        return serviceCallID;
    }

    public void setServiceCallID(String serviceCallID) {
        this.serviceCallID = serviceCallID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(customerID);
        dest.writeString(serviceCallID);
    }
}
