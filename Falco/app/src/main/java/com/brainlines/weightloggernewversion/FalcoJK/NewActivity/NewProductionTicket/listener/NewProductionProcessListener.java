package com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.listener;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.Model.NewOperationModel;

import java.util.ArrayList;

public interface NewProductionProcessListener {

    void imgRouteIsClicked(boolean flag, NewOperationModel model);

    void imgSubmitButtonIsClicked(ArrayList<NewOperationModel> ticketList);


    void getOkValue(String str_ok, String rejectqty, String secondqty, String str_wip);

    void checktotalokqty(String totalqty, String str_ok, String rejectqty, String secondqty, String str_wip);
}
