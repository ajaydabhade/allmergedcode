package com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Service_Call_List_Activity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasStatusModel;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class ServiceCallStausAdapter extends RecyclerView.Adapter<ServiceCallStausAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<SwasStatusModel> staus_list;
    View.OnClickListener mClickListener;
    Context context;
    String str_start_date,str_end_Date,str_tk_id,str_approved_by,str_tk_release_date,main_string_release_date;
    String str_tkid;
    RecyclerView rv_packingtickets;
    TicketListRvAdapter adapter;

    public ServiceCallStausAdapter(Context ctx, ArrayList<SwasStatusModel> staus_list){
        inflater = LayoutInflater.from(ctx);
        this.staus_list = staus_list;
        this.context=ctx;
    }

    @Override
    public int getItemCount() {
        return staus_list.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_service_call_status, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int i) {
        final SwasStatusModel model = staus_list.get(i);
        if (model!=null) {

            holder.txt_status.setText(model.getStatus_name());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String status_name = model.getStatus_name();
                    Intent i = new Intent(context, Swas_Service_Call_List_Activity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("status_id",model.getServicecall_status_id());
                    bundle.putString("status_text",model.getStatus_name());
                    i.putExtras(bundle);
                    context.startActivity(i);
                }
            });

        }
    }



    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txt_status;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_status=itemView.findViewById(R.id.item_txt_call_status);


        }
    }


}
