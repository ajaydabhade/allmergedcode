package com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasPartOrderedByModel;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class SwasPartOrderedByAdapter extends RecyclerView.Adapter<SwasPartOrderedByAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<SwasPartOrderedByModel> list2;
    View.OnClickListener mClickListener;
    Context context;
    String str_start_date,str_end_Date,str_tk_id,str_approved_by,str_tk_release_date,main_string_release_date;
    String str_tkid;
    RecyclerView rv_completed_customer_list;
    SwasPartOrderedByAdapter adapter2;

    public SwasPartOrderedByAdapter(Context ctx, ArrayList<SwasPartOrderedByModel> list2){
        inflater = LayoutInflater.from(ctx);
        this.list2 = list2;
        this.context=ctx;
    }

    @Override
    public int getItemCount() {
        return list2.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_part_ordered_by, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int i) {
        final SwasPartOrderedByModel model = list2.get(i);
        if (model!=null) {

            holder.txt1.setText(model.getITEMID());
            holder.txt2.setText(model.getNAME());
            holder.txt3.setText(model.getPRICE());
            holder.txt4.setText(model.getQuantity());


        }




    }



    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txt1,txt2,txt3,txt4;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt1=itemView.findViewById(R.id.txt1);
            txt2=itemView.findViewById(R.id.txt2);
            txt3=itemView.findViewById(R.id.txt3);
            txt4=itemView.findViewById(R.id.txt4);





        }
    }


}

