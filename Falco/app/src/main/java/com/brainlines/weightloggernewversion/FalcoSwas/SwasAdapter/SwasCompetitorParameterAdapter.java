package com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasListener.CompetitorParameterListener;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompetitorParameterModel;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class SwasCompetitorParameterAdapter  extends RecyclerView.Adapter<SwasCompetitorParameterAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<SwasCompetitorParameterModel>  competitive_parameter;
    View.OnClickListener mClickListener;
    Context context;
    String str_start_date,str_end_Date,str_tk_id,str_approved_by,str_tk_release_date,main_string_release_date;
    String str_tkid;
    RecyclerView rv_packingtickets;
    TicketListRvAdapter adapter;
    CompetitorParameterListener competitorParameterListener;

    public  SwasCompetitorParameterAdapter(Context ctx, ArrayList<SwasCompetitorParameterModel>competitive_parameter,CompetitorParameterListener competitorParameterListener){
        inflater = LayoutInflater.from(ctx);
        this.competitive_parameter =   competitive_parameter;
        this.context=ctx;
        this.competitorParameterListener = competitorParameterListener;
    }

    @Override
    public int getItemCount() {
        return   competitive_parameter.size();
    }

    @NonNull
    @Override
    public  MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_swas_competitor_parameters, parent, false);
        MyViewHolder holder = new  MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final SwasCompetitorParameterModel model = competitive_parameter.get(position);
        if (model!=null) {

            holder.txtCompetitorParameter.setText(model.getCompetitorsParameters());
            holder.txt_unit.setText(model.getUnit());

            holder.edCompetitorValue.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String stredexpensesvalue = s.toString();
                    if (s!=null){

                        competitive_parameter.get(position).setMachineParameterValues(stredexpensesvalue);
                    }
                    else if (s==null){
                        competitive_parameter.get(position).setMachineParameterValues("0");
                    }
                }
            });
            holder.ed_remark.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String remark = s.toString();
                    if (s!=null){

                        competitive_parameter.get(position).setRemark(remark);
                    }
                    else if (s==null){
                        competitive_parameter.get(position).setRemark("NA");
                    }
                }
            });
        }
    }



    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txtCompetitorParameter,txt_unit;
        EditText edCompetitorValue,ed_remark;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtCompetitorParameter=itemView.findViewById(R.id.txtCompetitorParameter);
            edCompetitorValue=itemView.findViewById(R.id.edCompetitorValue);
            ed_remark = itemView.findViewById(R.id.edCompetitorremark);
            txt_unit = itemView.findViewById(R.id.txt_unit);


        }
    }


}

