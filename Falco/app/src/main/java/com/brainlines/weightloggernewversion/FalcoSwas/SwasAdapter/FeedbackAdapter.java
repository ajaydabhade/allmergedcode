package com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.FeedbackModel;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class FeedbackAdapter extends RecyclerView.Adapter< FeedbackAdapter.MyViewHolder> {

private LayoutInflater inflater;
private ArrayList<FeedbackModel> feedback;
        View.OnClickListener mClickListener;
        Context context;
        String str_start_date,str_end_Date,str_tk_id,str_approved_by,str_tk_release_date,main_string_release_date;
        String str_tkid;
        RecyclerView rv_Feedback;
        FeedbackAdapter adapter;

public FeedbackAdapter(Context ctx, ArrayList<FeedbackModel> feedback){
        inflater = LayoutInflater.from(ctx);
        this.feedback = feedback;
        this.context=ctx;
        }




@Override
public int getItemCount() {
        return feedback.size();
        }

@NonNull
@Override
public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_swas_feedback, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
        }

@Override
public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
final  FeedbackModel model = feedback.get(position);
        if (model!=null) {

        holder.txtCustomer.setText(model.getCreatedBy());
        holder.txtCustomerFeedback.setText(model.getUserFeedback());
        holder.txtDate.setText(model.getCreatedDate());



        }
        }


public class MyViewHolder extends RecyclerView.ViewHolder{
    TextView txtCustomer,txtCustomerFeedback,txtDate;

    public MyViewHolder(View itemView) {
        super(itemView);
        txtCustomer=itemView.findViewById(R.id.txtCustomer);
        txtCustomerFeedback=itemView.findViewById(R.id.txtCustomerFeedback);
        txtDate=itemView.findViewById(R.id.txtDate);




    }

}
}


