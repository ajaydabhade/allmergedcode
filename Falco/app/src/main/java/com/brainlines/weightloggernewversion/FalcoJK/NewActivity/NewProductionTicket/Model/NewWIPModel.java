package com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.Model;

public class NewWIPModel {
    String ptkd_Id,ptkd_PktId,ptkd_TkNo,ptkd_PSKU,ptkd_Plant,ptkd_ValueStreamCode,ptkd_Operation,ptkd_WIP,opr_operation_name,uni_unit_name,ptkd_Qty,createdby,createddate;

    public String getPtkd_Id() {
        return ptkd_Id;
    }

    public void setPtkd_Id(String ptkd_Id) {
        this.ptkd_Id = ptkd_Id;
    }

    public String getPtkd_PktId() {
        return ptkd_PktId;
    }

    public void setPtkd_PktId(String ptkd_PktId) {
        this.ptkd_PktId = ptkd_PktId;
    }

    public String getPtkd_TkNo() {
        return ptkd_TkNo;
    }

    public void setPtkd_TkNo(String ptkd_TkNo) {
        this.ptkd_TkNo = ptkd_TkNo;
    }

    public String getPtkd_PSKU() {
        return ptkd_PSKU;
    }

    public void setPtkd_PSKU(String ptkd_PSKU) {
        this.ptkd_PSKU = ptkd_PSKU;
    }

    public String getPtkd_Plant() {
        return ptkd_Plant;
    }

    public void setPtkd_Plant(String ptkd_Plant) {
        this.ptkd_Plant = ptkd_Plant;
    }

    public String getPtkd_ValueStreamCode() {
        return ptkd_ValueStreamCode;
    }

    public void setPtkd_ValueStreamCode(String ptkd_ValueStreamCode) {
        this.ptkd_ValueStreamCode = ptkd_ValueStreamCode;
    }

    public String getPtkd_Operation() {
        return ptkd_Operation;
    }

    public void setPtkd_Operation(String ptkd_Operation) {
        this.ptkd_Operation = ptkd_Operation;
    }

    public String getPtkd_WIP() {
        return ptkd_WIP;
    }

    public void setPtkd_WIP(String ptkd_WIP) {
        this.ptkd_WIP = ptkd_WIP;
    }

    public String getOpr_operation_name() {
        return opr_operation_name;
    }

    public void setOpr_operation_name(String opr_operation_name) {
        this.opr_operation_name = opr_operation_name;
    }

    public String getUni_unit_name() {
        return uni_unit_name;
    }

    public void setUni_unit_name(String uni_unit_name) {
        this.uni_unit_name = uni_unit_name;
    }

    public String getPtkd_Qty() {
        return ptkd_Qty;
    }

    public void setPtkd_Qty(String ptkd_Qty) {
        this.ptkd_Qty = ptkd_Qty;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }
}
