package com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.FaultyCharactristics_Model;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class FaultyCharacteristicsAdapter extends RecyclerView.Adapter<FaultyCharacteristicsAdapter.MyViewHolder> {


    private LayoutInflater inflater;
    private ArrayList<FaultyCharactristics_Model> faultyCharactristics_models;
    View.OnClickListener mClickListener;
    Context context;

    public FaultyCharacteristicsAdapter(Context ctx, ArrayList<FaultyCharactristics_Model> faultyCharactristics_models) {
        inflater = LayoutInflater.from(ctx);
        this.faultyCharactristics_models = faultyCharactristics_models;
        this.context = ctx;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_faulty_characteristics, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final FaultyCharactristics_Model model = faultyCharactristics_models.get(position);
        if (model!=null) {

            holder.item_check_name.setText(model.getFaultCharacteristicsText());

            // holder.item_txt_customer_name.setText(model.getLOCATIONNAME());


        }
    }

    @Override
    public int getItemCount() {
        return faultyCharactristics_models.size();
    }

    class MyViewHolder  extends RecyclerView.ViewHolder{
        TextView item_check_name;
        CheckBox item_check_box;

        public MyViewHolder(View itemView) {
            super(itemView);
            item_check_name = itemView.findViewById(R.id.txt_faulty_char_name);



        }
    }
}
