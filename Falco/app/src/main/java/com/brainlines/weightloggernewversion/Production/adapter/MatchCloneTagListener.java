package com.brainlines.weightloggernewversion.Production.adapter;

public interface MatchCloneTagListener {

    void tagClick(MatchCloneTagModel model);
}
