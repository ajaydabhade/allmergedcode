package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

import android.os.Parcel;
import android.os.Parcelable;

public class ServiceCallCheckListModel implements Parcelable {

    String checkListForSiteReadynessId;
    String checkListForSiteReadynesstext;
    String chcked;

    public ServiceCallCheckListModel(String checkListForSiteReadynessId, String checkListForSiteReadynesstext, String chcked) {
        this.checkListForSiteReadynessId = checkListForSiteReadynessId;
        this.checkListForSiteReadynesstext = checkListForSiteReadynesstext;
        this.chcked = chcked;
    }

    protected ServiceCallCheckListModel(Parcel in) {
        checkListForSiteReadynessId = in.readString();
        checkListForSiteReadynesstext = in.readString();
        chcked = in.readString();
    }

    public static final Creator<ServiceCallCheckListModel> CREATOR = new Creator<ServiceCallCheckListModel>() {
        @Override
        public ServiceCallCheckListModel createFromParcel(Parcel in) {
            return new ServiceCallCheckListModel(in);
        }

        @Override
        public ServiceCallCheckListModel[] newArray(int size) {
            return new ServiceCallCheckListModel[size];
        }
    };

    public String getCheckListForSiteReadynessId() {
        return checkListForSiteReadynessId;
    }

    public void setCheckListForSiteReadynessId(String checkListForSiteReadynessId) {
        this.checkListForSiteReadynessId = checkListForSiteReadynessId;
    }

    public String getCheckListForSiteReadynesstext() {
        return checkListForSiteReadynesstext;
    }

    public void setCheckListForSiteReadynesstext(String checkListForSiteReadynesstext) {
        this.checkListForSiteReadynesstext = checkListForSiteReadynesstext;
    }

    public String getChcked() {
        return chcked;
    }

    public void setChcked(String chcked) {
        this.chcked = chcked;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(checkListForSiteReadynessId);
        dest.writeString(checkListForSiteReadynesstext);
        dest.writeString(chcked);
    }
}
