package com.brainlines.weightloggernewversion.utils;

public class Constants {

    //local
    public static String dbIpAddress = "192.168.10.67";
    public static String dbUserName = "test2";
    public static String dbPassword = "test12345";
    public static String dbDatabaseName = "decitalpool";
    public static String serverport = "1433";
    public static String dbUrl = "jdbc:jtds:sqlserver://"+dbIpAddress+":"+serverport+"/"+dbDatabaseName;
    public static int DATA_LOGGER_PORT = 8080;
    public static final String MQTT_BROKER = "tcp://"+dbIpAddress;

    //Azure
//    public static String dbIpAddress = "192.227.69.198";
//    public static String dbUserName = "weightloggeruser";
//    public static String dbPassword = "weightlogger123";
//    public static String dbDatabaseName = "WeightLoggerDev";
//    public static String serverport = "1433";
//    public static String dbUrl = "jdbc:jtds:sqlserver://"+dbIpAddress+":"+serverport+"/"+dbDatabaseName;
//    public static int DATA_LOGGER_PORT = 8080;
//    public static final String MQTT_BROKER = "tcp://"+dbIpAddress;


    public static final String TEST_ALLOWED = "TESTALLOWED";
    public static final String TEST_APPEAL = "APPEALREQUIRED";
    public static final String TEST_NOT_ALLOWED = "TESTNOTALLOWED";
    public static final String RUN_TEST_INPROGRESS_STATUS = "INPROGRESS";
    public static final String GM = "+/- GM";
    public static final String GMPERCENT = "+/- %GM";
    public static final String GMSD = "GM SD";
    public static final String PERCENTSD = "% SD";
    public static final String AVGGIVEAWAY= "AvgGiveAway";

    /*
    * WAC TABLES CONSTANTS
    *
    * */

    //C-Tags Table

    public static final String C_TAGS_TABLE = "CalibrationTags";
    public static final String C_TAG_ID = "CalTagsID";
    public static final String C_TAG_OEM_ID = "OEMID";
    public static final String C_TAG_MACID = "MacID";
    public static final String C_TAG_LOGGER_ID = "LoggerID";
    public static final String C_TAG_TAGS_ID = "TagID";
    public static final String C_TAG_OEM_NUMBER = "OINumber";
    public static final String C_TAG_TARGET_WIGHT = "TargetWeight";
    public static final String C_TAG_CUSTOMER_NAME = "CustomerName";
    public static final String C_TAG_MACHINE_TYPE_ID = "MachineTypeID";
    public static final String C_TAG_PRODUCT_TYPE_ID = "ProductTypeID";
    public static final String C_TAG_MACHINE_TYPE_NAME = "MachineTypeName";
    public static final String C_TAG_PRODUCT_TYPE_NAME = "ProductTypeName";
    public static final String C_TAG_FILLER_TYPE = "FillerType";
    public static final String C_TAG_PRODUCT = "ProductName";
    public static final String C_TAG_OPERATOR = "Operator";
    public static final String C_TAG_TARGET_WEIGHT_UNIT = "TargetWeightUnit";
    public static final String C_TAG_INSPECTOR = "Inspector";
    public static final String C_TAG_FILEM_TYPE = "FilmType";
    public static final String C_TAG_PRODUCT_FEED = "ProductFeed";
    public static final String C_TAG_AGITATOR_MOTOR_FREQ = "AgitatorMotorFrequency";
    public static final String C_TAG_AGITATOR_MODE = "AgitatorMode";
    public static final String C_TAG_AGITATOR_OD = "AgitatorOD";
    public static final String C_TAG_AGITATOR_ID = "AgitatorID";
    public static final String C_TAG_AGITATOR_PITCH = "AgitatorPitch";
    public static final String C_TAG_TARGET_ACCURACY_TYPE = "TargetAccuracyType";
    public static final String C_TAG_TARGET_ACCURACY_VALUE = "TargetAccuracyValue";
    public static final String C_TAG_MACHINE_SERIAL_NUMBER = "MachineSerialNumber";
    public static final String C_TAG_PRODUCT_BULK_MAIN = "PrBulkMain";
    public static final String C_TAG_TARGET_SPEED = "TargetSpeed";
    public static final String C_TAG_HEAD = "Head";
    public static final String C_TAG_AIR_PRESSURE = "AirPressure";
    public static final String C_TAG_QUANTITY_SETTING = "QuantitySetting";
    public static final String C_TAG_BAG_LENGTH_SETTING = "BaglengthSetting";
    public static final String C_TAG_FILLVALVE_GAP = "FillValveGap";
    public static final String C_TAG_FILL_VFD_FREQUENCY = "FillVfdFreq";
    public static final String C_TAG_HORIZONTAL_CUT_SEAL_SETTING = "HorizCutSealSetting";
    public static final String C_TAG_HORIZONTAL_BAND_SEAL_SETTING = "HorizBandSealSetting";
    public static final String C_TAG_VERTICAL_SEAL_SETTING = "VertSealSetting";
    public static final String C_TAG_AUGER_AGITATOR_FREQUENCY = "AugerAgitatorFreq";
    public static final String C_TAG_AUGER_FILL_VALVE = "AugerFillValve";
    public static final String C_TAG_AUGER_PITCH = "AugurPitch";
    public static final String C_TAG_POUCH_WIDTH = "PouchWidth";
    public static final String C_TAG_POUCH_LENGTH = "PouchLength";
    public static final String C_TAG_POUCH_SEAL_TYPE = "PouchSealType";
    public static final String C_TAG_POUCH_TEMP_HORIZONTAL_FRONT = "PouchTempHorizFront";
    public static final String C_TAG_POUCH_TEMP_HORIZONTAL_BACK = "PouchTempHorizBack";
    public static final String C_TAG_POUCH_TEMP_VERTICAL = "PouchTempVert";
    public static final String C_TAG_POUCH_TEMP_VERT_FRONT_LEFT = "PouchTempVertFrontLeft";
    public static final String C_TAG_POUCH_TEMP_VERTICAL_FRONT_RIGHT = "PouchTempVertFrontRight";
    public static final String C_TAG_POUCH_TEMP_VERT_BACK_LEFT = "PouchTempVertBackLeft";
    public static final String C_TAG_POUCH_TEMP_VERT_BACK_RIGHT = "PouchTempVertBackRight";
    public static final String C_TAG_POUCH_GUSSET = "PouchGusset";
    public static final String C_TAG_POUCH_FILM_THICKNESS = "PouchFilmThickness";
    public static final String C_TAG_PUCH_PRODUCT_BULK = "PouchProductBulk";
    public static final String C_TAG_POUCH_EQ_HORIZ_SERRATIONS = "PouchEqHorizSerrations";
    public static final String C_TAG_POUCH_FILL_eVENT = "PouchFillEvent";
    public static final String C_TAG_OVERALL_STATUS = "OverallStatus";
    public static final String C_TAG_REMARK = "Remarks";
    public static final String C_TAG_SEAL_STATUS = "SealStatus";
    public static final String C_TAG_DROP_STATUS = "DropStatus";
    public static final String C_TAG_TARGET_SPEED_STATUS = "TargetSpeedStatus";
    public static final String C_TAG_WEIGHT_TEST_STATUS = "WeightTestStatus";
    public static final String C_TAG_BAG_STATUS = "BagStatus";
    public static final String C_TAG_PAPER_STATUS = "PaperStatus";
    public static final String C_TAG_FY_YEAR = "FinancialYear";
    public static final String C_TAG_SEAL_REJECTION_REASON = "SealRejectionReason";
    public static final String C_TAG_CUSTOMER_PRODUCT = "IsCustomerProduct";
    public static final String C_TAG_CUSTOMER_FILM = "IsCustomerFilm";
    public static final String C_TAG_NEGATIVE_WEIGHT = "IsNegativeWeight";
    public static final String C_TAG_LOGGER_CREATE_TIMESTAMP = "LoggerCreateTimestamp";
    public static final String C_TAG_CREATED_DATE = "CreatedDate";
    public static final String C_TAG_MODIFIED_DATE = "ModifiedDate";
    public static final String C_TAGS_IS_SYNC = "IsSync";


    //master Seal rejection reason Table
    public static final String SEAL_REJECTION_TABLE = "mstSealRejectionReasons";
    public static final String SEAL_REJECTION_REASON_ID = "rsn_id";
    public static final String SEAL_REJECTION_OEM_ID = "OEMID";
    public static final String SEAL_REJECTION_PRODUCT_TYPE = "product_type";
    public static final String SEAL_REJECTION_REASON_DESCIPTION = "reason_desc";
    public static final String SEAL_REJECTION_IS_ACTIVE = "IsActive";
    public static final String SEAL_REJECTION_CREATED_DATE = "CreatedDate";
    public static final String SEAL_REJECTION_MODIFIED_DATE = "ModifiedDate";
    public static final String SEAL_REJECTION_CREATED_BY = "CreatedBy";
    public static final String SEAL_REJECTION_MODIFIED_BY = "ModifiedBy";
    public static final String SEAL_REJECTION_IS_SYNC = "IsSync";

    //Tolerance Table
    public static final String TOLERANCE_TABLE = "mstCalibrationTolerance";
    public static final String TOLERANCE_ID = "CalibrationToleranceID";
    public static final String TOLERANCE_OEM_ID = "OEMID";
    public static final String TOLERANCE_PRODUCT_TYPE = "ProductType";
    public static final String TOLERANCE_TARGET_WEIGHT = "TargetWeight";
    public static final String TOLERANCE_MIN_TARGET_WEIGHT = "MinTargetWeight";
    public static final String TOLERANCE_MAX_TARGET_WEIGHT = "MaxTargetWeight";
    public static final String TOLERANCE_MIN_WEIGHT_SAMPLES = "MinWeightSamples";
    public static final String TOLERANCE_MIN_PAPER_SHIFT_SAMPLES = "MinPaperShiftSamples";
    public static final String TOLERANCE_PAPER_SHIFT_TOLERANCE = "PaperShiftTolerance";
    public static final String TOLERANCE_MIN_BAG_LENGTH_SAMPLES = "MinBagLengthSamples";
    public static final String TOLERANCE_BAG_LENGTH_TOLERANCE = "BagLengthTolerance";
    public static final String TOLERANCE_CREATED_DATE = "CreatedDate";
    public static final String TOLERANCE_MODIFIED_DATE = "ModifiedDate";
    public static final String TOLERANCE_CREATED_BY = "CreatedBy";
    public static final String TOLERANCE_MODIFIED_BY = "ModifiedBy";
    public static final String TOLERANCE_IS_ACTIVE = "IsActive";


    //PRODUCT Table
    public static final String PRODUCT_TYPE_TABLE = "mstProductType";
    public static final String PRODUCT_TYPE_ID = "ProductTypeID";
    public static final String PRODUCT_OEM_ID = "OEMID";
    public static final String PRODUCT_PRODUCT_TYPE = "ProductType";
    public static final String PRODUCT_CREATED_BY = "CreatedBy";
    public static final String PRODUCT_MODIFIED_BY = "ModifiedBy";
    public static final String PRODUCT_UNIT_OF_MEASUREMENT = "UOM";
    public static final String PRODUCT_UNIT_OF_MEASUREMENT_ID = "UOMID";
    public static final String PRODUCT_CREATED_DATE = "CreatedDate";
    public static final String PRODUCT_MODIFIED_DATE = "ModifiedDate";
    public static final String PRODUCT_IS_ACTVIE = "IsActive";


    //Master WireCup
    public static final String MASTER_WIRE_CUP_TABLE = "mstWireCup";
    public static final String  MASTER_WIRE_CUP_ID = "WireCupID";
    public static final String  MASTER_WIRE_CUP_OEM_ID = "OEMID";
    public static final String  MASTER_WIRE_CUP = "WireCup";
    public static final String  MASTER_WIRE_CUP_CREATED_DATE = "CreatedDate";
    public static final String  MASTER_WIRE_CUP_MODIFIED_DATE = "ModifiedDate";
    public static final String  MASTER_WIRE_CUP_CREATED_BY = "CreatedBy";
    public static final String  MASTER_WIRE_CUP_MODIFIED_BY = "ModifiedBy";
    public static final String  MASTER_WIRE_CUP_IS_ACTIVE = "IsActive";



    //Master target accuracy Table
    public static final String TARGET_ACCURACY_TABLE = "mstTargetAccuracy";
    public static final String TARGET_ACCURACY_ID = "TargetAccuracyID";
    public static final String TARGET_ACCURACY_OEM_ID = "OEMID";
    public static final String TARGET_ACCURACY_TARGET_ID = "TargetID";
    public static final String TARGET_ACCURACY = "TargetAccuracy";
    public static final String TARGET_ACCURACY_DESCRIPTION = "TargetAccuracy_Desc";
    public static final String TARGET_ACCURACY_CREATED_DATE = "CreatedDate";
    public static final String TARGET_ACCURACY_MODIFIED_DATE = "ModifiedDate";
    public static final String TARGET_ACCURACY_CREATED_BY = "CreatedBy";
    public static final String TARGET_ACCURACY_MODIFIED_BY = "ModifiedBy";
    public static final String TARGET_ACCURACY_IS_ACTIVE = "IsActive";


    //Master Machine Type Table
    public static final String MACHINE_TYPE_TABLE = "mstMachineType";
    public static final String MACHINE_TYPE_ID = "MachineTypeID";
    public static final String MACHINE_TYPE_OEM_ID = "OEMID";
    public static final String MACHINE_TYPE_MACHINE_ID = "MachineID";
    public static final String MACHINE_TYPE_PRODUCT_TYPE_ID = "ProductTypeID";
    public static final String MACHINE_TYPE_MACHINE_NAME = "MachineName";
    public static final String MACHINE_TYPE_MACHINE_DESCRIPTION = "MachineDescription";
    public static final String MACHINE_TYPE_MACHINE_PRODUCT_TYPE = "ProductType";
    public static final String MACHINE_TYPE_CREATED_DATE = "CreatedDate";
    public static final String MACHINE_TYPE_MODIFIED_DATE = "ModifiedDate";
    public static final String MACHINE_TYPE_CREATED_BY = "CreatedBy";
    public static final String MACHINE_TYPE_MODIFIED_BY = "ModifiedBy";
    public static final String MACHINE_TYPE_IS_ACTIVE = "IsActive";

    //Master POUCH ATTRIBUTES Table
    public static final String POUCH_ATTRIBUTES_TABLE = "mstPouchAttributes";
    public static final String POUCH_ATTRIBUTES_ID = "PouchAttributesID";
    public static final String POUCH_ATTRIBUTES_OEM_ID = "OEMID";
    public static final String POUCH_ATTRIBUTES_PRODUCT_TYPE_ID = "ProdutTypeID";
    public static final String POUCH_ATTRIBUTES_DESCRIPTION = "PouchAttributeDescription";
    public static final String POUCH_ATTRIBUTES_CREATED_DATE = "CreatedDate";
    public static final String POUCH_ATTRIBUTES_MODIFIED_DATE = "ModifiedDate";
    public static final String POUCH_ATTRIBUTES_CREATED_BY = "CreatedBy";
    public static final String POUCH_ATTRIBUTES_MODIFIED_BY = "ModifiedBy";
    public static final String POUCH_ATTRIBUTES_IS_ACTVIE = "IsActive";


    //Master SEAL_TYPE Table
    public static final String SEAL_TYPE_TABLE = "mstSealType";
    public static final String SEAL_TYPE = "SealType";
    public static final String SEAL_TYPE_OEM_ID = "OEMID";
    public static final String SEAL_TYPE_SEAL_ID = "SealID";
    public static final String SEAL_TYPE_PRODUCT_TYPE_ID = "ProductTypeID";
    public static final String SEAL_TYPE_IS_ACTIVE = "IsActive";
    public static final String SEAL_TYPE_CREATED_DATE = "CreatedDate";
    public static final String SEAL_TYPE_MODIFIED_DATE = "ModifiedDate";
    public static final String SEAL_TYPE_CREATED_BY = "CreatedBy";
    public static final String SEAL_TYPE_MODIFIED_BY = "ModifiedBy";


    //Master ProductionTag Table
    public static final String PRODUCTION_TAG_TABLE = "ProductionTag";
    public static final String PRODUCTION_TAG_ID = "ProductionTagID";
    public static final String PRODUCTION_TAG = "ProductionTagName";
    public static final String PRODUCTION_TAG_OEM_ID = "OEMID";
    public static final String PRODUCTION_TAG_MAC_ID = "MacID";
    public static final String PRODUCTION_TAG_BATCH_ID = "BatchID";
    public static final String PRODUCTION_TAG_PRODUCT_ID= "ProductName";
    public static final String PRODUCTION_TAG_GM_PER_POUCH = "GmPerPouch";
    public static final String PRODUCTION_TAG_UNIT = "Unit";
    public static final String PRODUCTION_TAG_TOTAL_QTY = "TotalQuantity";
    public static final String PRODUCTION_TAG_TOTAL_QTY_UOM = "TotalQuantityUOM";
    public static final String PRODUCTION_TAG_IS_NEGATIVE_ALLOWED = "IsNegAllowed";
    public static final String PRODUCTION_TAG_MODIFIED_DATE = "ModifiedDate";
    public static final String PRODUCTION_TAG_SHIFT = "TagShift";
    public static final String PRODUCTION_TAG_NO_OF_POUCHES = "TagNumberOfPouches";
    public static final String PRODUCTION_TAG_SESSION_POSITVIE_TOLERANCE = "SessionPositiveTolerance";
    public static final String PRODUCTION_TAG_SESSION_NEAGTIVE_TOLERANCE = "SessionNegativeTolerance";
    public static final String PRODUCTION_TAG_MASTER_TOLERANCE_POSITIVE = "MasterTolerancePositive";
    public static final String PRODUCTION_TAG_MASTER_TOLERANCE_NEGATIVE = "MasterToleranceNegative";
    public static final String PRODUCTION_TAG_UCL_GM = "UCLgm";
    public static final String PRODUCTION_TAG_LCL_GM = "LCLgm";
    public static final String PRODUCTION_TAG_OPERATOR_NAME = "Operator";
    public static final String PRODUCTION_TAG_TARGET_ACCURACY_TYPE = "TargetAccurancyType";
    public static final String PRODUCTION_TAG_TARGET_ACCURACY_VALUE = "TargetAccurancyValue";
    public static final String PRODUCTION_TAG_BATCH_STATUS = "BatchStatus";
    public static final String PRODUCTION_TAG_IS_SYNC = "IsSync";

    //User Login B_devicedid_
    public static final String USER_LOGIN_TABLE = "UserLogin";
    public static final String LOGIN_ID = "LoginID";
    public static final String USER_ID = "UserID";
    public static final String ROLE_ID = "RoleID";
    public static final String USER_NAME = "UserName";
    public static final String PASSWORD = "Password";
    public static final String IS_ACTIVE = "IsActive";
    public static final String IS_DELETED = "IsDeleted";
    public static final String CREATED_DATE = "CreatedDate";
    public static final String MODIFIED_DATE = "ModifiedDate";

    //Data Logger Table
    public static final String DATA_LOGGER_TABLE = "mstLogger";
    public static final String LOGGER_ID = "LoggerID";
    public static final String OEMID = "OEMID";
    public static final String LOGGER_SERAIL_NUM = "SerialNumber";
    public static final String LOGGER_NAME = "LoggerName";
    public static final String LOGGER_IP_ADDRESS = "LocalIpAddress";
    public static final String LOGGER_MAC_ID = "logger_mac_id";//**************************Check
    public static final String LOGGER_LOCATION = "LoggerLocation";
    public static final String LOGGER_CREATE_TIMESTAMP = "CreatedDate";
    public static final String LOGGER_CREATED_BY = "CreatedBy";//************************Check
    public static final String LOGGER_LOGGERLOCATION = "LoggerLocation";
    public static final String  LOGGER_MODIFIED_DATE= "ModifiedDate";
    public static final String LOGGER_IS_ACTIVE = "IsActive";
    public static final String LOGGER_IN_USE = "IsInUse";

    //Master Filler Type Table
    public static final String MASTER_FILLER_TYPE_TABLE = "mstFillerType";
    public static final String FILLER_TYPE_ID = "FillerTypeID";
    public static final String FILLER_TYPE_OEMID = "OEMID";
    public static final String FILLER_TYPE_PRODUCT_TYPE_ID = "ProductTypeID";
    public static final String FILLER_TYPE_FILTER_TYPE = "FillerType";
    public static final String FILLER_TYPE_CREATED_DATE = "CreatedDate";
    public static final String FILLER_TYPE_MODIFIED_DATE = "ModifiedDate";
    public static final String FILLER_TYPE_CREATED_BY = "CreatedBy";
    public static final String FILLER_TYPE_MODIFIED_BY = "ModifiedBy";
    public static final String FILLER_TYPE_IS_ACTIVE = "IsActive";
    public static final String FILLER_TYPE_IS_DELETED = "IsDeleted";
    public static final String FILLER_TYPE_FILTER_ID = "FillerID";//************************Check

    //Master Head Table
    public static final String MASTER_HEAD_TABLE = "mstHead";
    public static final String MASTER_HEAD_HEAD_ID = "HeadID";
    public static final String MASTER_HEAD_OEM_ID = "OEMID";
    public static final String MASTER_HEAD_HEAD_ID_ = "Head_ID";//************************Check
    public static final String MASTER_HEAD_HEAD_TYPE_ = "HeadType";
    public static final String MASTER_HEAD_IS_ACTIVE = "IsActive";
    public static final String MASTER_HEAD_CREATED_DATE = "CreatedDate";
    public static final String MASTER_HEAD_MODIFIED_DATE = "ModifiedDate";
    public static final String MASTER_HEAD_CREATED_BY = "CreatedBy";
    public static final String MASTER_HEAD_MODIFIED_BY = "ModifiedBy";





    //Master Products
    public static final String MASTER_PRODUCTS_TABLE = "mstProducts";
    public static final String MASTER_PRODUCTS_PRODUCTS_ID = "ProductID";
    public static final String MASTER_PRODUCTS_OEM_ID = "OEMID";
    public static final String MASTER_PRODUCTS_PRODUCTS = "Product";
    public static final String MASTER_PRODUCTS_PRICEPERKG = "PricePerKG";
    public static final String MASTER_PRODUCTS_CURRENCY = "Currency";
    public static final String MASTER_PRODUCTS_TYPE_ID = "ProductTypeID";
    public static final String MASTER_PRODUCTS_IS_ACTIVE = "IsActive";
    public static final String MASTER_PRODUCTS_CREATED_DATE = "CreatedDate";
    public static final String MASTER_PRODUCTS_MODIFIED_DATE = "ModifiedDate";
    public static final String MASTER_PRODUCTS_CREATED_BY = "CreatedBy";
    public static final String MASTER_PRODUCTS_MODIFIED_BY = "ModifiedBy";


    //Master Pouch Symmetry
    public static final String MASTER_POUCH_SYMMETRY_TABLE = "mstPouchSymmetry";
    public static final String MASTER_POUCH_SYMMETRY_ID = "PouchSymmetryID";
    public static final String MASTER_POUCH_OEM_ID = "OEMID";
    public static final String MASTER_POUCH_ID = "PouchID";
    public static final String MASTER_POUCH_TYPE_ID = "PouchTypeID";
    public static final String MASTER_POUCH_DESC = "PouchDesc";
    public static final String MASTER_POUCH_IS_ACTIVE = "IsActive";
    public static final String MASTER_POUCH_IS_DELETED = "IsDeleted";
    public static final String MASTER_POUCH_CREATED_DATE = "CreatedDate";
    public static final String MASTER_POUCH_MODIFIED_DATE = "ModifiedDate";
    public static final String MASTER_POUCH_CREATED_BY = "CreatedBy";
    public static final String MASTER_POUCH_MODIFIED_BY = "ModifiedBy";






    //Seal Rejection Reason Master TAble
    public static final String SEAL_REJECTION_REASON_MASTER_TABLE = "mstSealRejectionReasons";
    public static final String SEAL_REJECTION_REASON_OEM_ID = "OEMID";
    public static final String SEAL_REJECTION_REASON_MAC_ID = "mac_id";
    public static final String SEAL_REJECTION_REASON_LOGGER_ID = "LoggerID";
    public static final String SEAL_REJECTION_REASON_REASON_ID = "rsn_id";
    public static final String SEAL_REJECTION_REASON_PRODUCT_TYPE = "product_type";
    public static final String SEAL_REJECTION_REASON_REASON_DESC = "reason_desc";
    public static final String SEAL_REJECTION_REASON_IS_ACTIVE = "IsActive";
    public static final String SEAL_REJECTION_REASON_IS_DELETED = "IsDeleted";
    public static final String SEAL_REJECTION_REASON_CREATED_DATE = "CreatedDate";
    public static final String SEAL_REJECTION_REASON_MODIFIED_DATE = "ModifiedDate";
    public static final String SEAL_REJECTION_REASON_CREATED_BY = "CreatedBy";
    public static final String SEAL_REJECTION_REASON_MODIFIED_BY = "ModifiedBy";

    //Master UOM Table
    public static final String MASTER_UOM_TABLE = "mstUomTable";
    public static final String UOM_ID = "UOMId";
    public static final String UOM_TABLE_OEMID = "OEMID";
    public static final String UOM_PRODUCT_TYPE = "ProductType";
    public static final String UOM_PRODUCT_UOM = "ProductUOM";
    public static final String UOM_PRODUCT_IS_ACTIVE = "isActive";
    public static final String UOM_PRODUCT_IS_DELETED = "isDeleted";
    public static final String UOM_PRODUCT_CREATED_DATE = "CreatedDate";
    public static final String UOM_PRODUCT_MODIFIED_DATE = "ModifiedDate";
    public static final String UOM_PRODUCT_CREATED_BY = "CreatedBy";
    public static final String UOM_PRODUCT_MODIFIED_BY = "ModifiedBy";




    //Interface to start and stop service
    public interface ACTION {
        public static String STARTFORGROUND_ACTION = "foregroundservice.action.startforeground";
        public static String STOPFORGROUND_ACTION = "foregroundservice.action.stopforeground";
    }

    //Production Reason Master
    public static final String PRODUCTION_REASON_MASTER = "mstProductionReasonMaster";
    public static final String PRODUCTION_REASON_REASONID = "ReasonID";
    public static final String PRODUCTION_REASON_REASONDESC = "ReasonDesc";
    public static final String PRODUCTION_REASON_CREATED_DATE = "CreatedDate";
    public static final String PRODUCTION_REASON_MODIFIED_DATE = "ModifiedDate";

    //Production TOLERANCE Master
    public static final String PRODUCTION_TOLERANCE_MASTER = "mstProductionTolerance";
    public static final String PRODUCTION_TOLERANCE_TOLERANCEID = "ProductionToleranceID";
    public static final String PRODUCTION_TOLERANCE_OEMID = "OEMID";
    public static final String PRODUCTION_TOLERANCE_GRAMMAGE = "Grammage";
    public static final String PRODUCTION_TOLERANCE_TOLERANCE_POSITIVE = "TolerancePositive";
    public static final String PRODUCTION_TOLERANCE_TOLERANCE_NEGATIVE = "ToleranceNegetive";
    public static final String PRODUCTION_TOLERANCE_ISACTIVE = "Isactive";
    public static final String PRODUCTION_TOLERANCE_ISDELETED = "IsDeleted";
    public static final String PRODUCTION_TOLERANCE_CREATED_DATE = "CreatedDate";
    public static final String PRODUCTION_TOLERANCE_MODIFIED_DATE= "ModifiedDate";

    //PRODUCTIION WEIGHT BATCH RESULT
    public static final String PRODUCTION_WEIGHT_REPORT = "ProductionWeightReport";
    public static final String PRODUCTION_WEIGHT_REPORT_REPORTID = "ReportID";
    public static final String PRODUCTION_WEIGHT_REPORT_OEMID = "OEMID";
    public static final String PRODUCTION_WEIGHT_REPORT_TAGID = "TagID";
    public static final String PRODUCTION_WEIGHT_REPORT_BATCH_ID = "BatchID";
    public static final String PRODUCTION_WEIGHT_REPORT_MIN_WEIGHT = "MinWeight";
    public static final String PRODUCTION_WEIGHT_REPORT_MAX_WEIGHT = "MaxWeight";
    public static final String PRODUCTION_WEIGHT_REPORT_MEAN_WEIGHT = "MeanWeight";
    public static final String PRODUCTION_WEIGHT_REPORT_AVERAGE_WEIGHT = "Average";
    public static final String PRODUCTION_WEIGHT_REPORT_RANGE = "Range";
    public static final String PRODUCTION_WEIGHT_REPORT_SD = "SD";
    public static final String PRODUCTION_WEIGHT_REPORT_UNFILLED_QUANTITY = "UnfilledQuantity";
    public static final String PRODUCTION_WEIGHT_REPORT_FILLED_QUANTITY = "FilledQuantity";
    public static final String PRODUCTION_WEIGHT_REPORT_UNFILLED_POUCHES = "UnfilledPouches";
    public static final String PRODUCTION_WEIGHT_REPORT_FILLED_POUCHES = "FilledPouches";
    public static final String PRODUCTION_WEIGHT_REPORT_EXCESS_GIVEAWAY_PRICE = "ExcessGiveawayPrice";
    public static final String PRODUCTION_WEIGHT_REPORT_EXCESS_GIVEAWAY_GM = "ExcessGiveawayGm";
    public static final String PRODUCTION_WEIGHT_REPORT_REASON = "Reason";
    public static final String PRODUCTION_WEIGHT_REPORT_REMARK = "Remark";
    public static final String PRODUCTION_WEIGHT_REPORT_ISACTIVE = "IsActive";
    public static final String PRODUCTION_WEIGHT_REPORT_CREATED_DATE = "CreatedDate";
    public static final String PRODUCTION_WEIGHT_REPORT_MODIFIED_DATE = "ModifiedDate";
    public static final String PRODUCTION_WEIGHT_REPORT_IS_SYNC = "IsSync";


    //Transaction P_WEIGHT_READING Table
    public static final String P_WEIGHT_READING_TABLE = "ProductionWeightReadings";
    public static final String P_WEIGHT_READING_ID = "PWeightReadingID";
    public static final String P_WEIGHT_READING_OEM_ID = "OEMID";
    public static final String P_WEIGHT_READING_MAC_ID = "MacID";
    public static final String P_WEIGHT_READING_LOGGER_ID = "LoggerID";
    public static final String P_WEIGHT_READING_WEIGHT_ID = "PWeightID";
    public static final String P_WEIGHT_READING_BATCHID = "BatchID";
    public static final String P_WEIGHT_READING_PRODUCTION_TAG_NAME = "ProductionTagName";
    public static final String P_WEIGHT_READING_WEIGHT = "Weight";
    public static final String P_WEIGHT_READING_SHIFT = "Shift";
    public static final String P_WEIGHT_READING_TARGETGMPERPOUCH = "TargetGmPerPouch";
    public static final String P_WEIGHT_READING_ACTUALGMPERPOUCH = "ActualGmPerPouch";
    public static final String P_WEIGHT_READING_EXCESSGIVEAWAYPERPOUCH = "ExcessGiveawayPerPouch";
    public static final String P_WEIGHT_READING_CUMILATIVEEXCESSGIVEAWAY = "CumilativeExcessGiveaway";
    public static final String P_WEIGHT_READING_CUMILATIVEQUANTITY = "CumilativeQuantity";
    public static final String P_WEIGHT_READING_CUMILATIVE_POUCHES = "CumilativePouches";
    public static final String P_WEIGHT_READING_NEGATIVE_GIVEAWAY_FROM_TAG = "NegetiveGigiveawayFromTag";
    public static final String P_WEIGHT_READING_UCL = "UCLgm";
    public static final String P_WEIGHT__READING_LCL = "LCLgm";
    public static final String P_WEIGHT_READING_STATUS = "Status";
    public static final String P_WEIGHT_READING_CREATED_DATE = "CreatedDate";
    public static final String P_WEIGHT_READING_CREATED_BY = "CreatedBy";
    public static final String P_WEIGHT_READING_MODIFIED_DATE = "ModifiedDate";
    public static final String P_WEIGHT_READING_MODIFIED_BY = "ModifiedBy";
    public static final String P_WEIGHT_READING_IS_SYNC = "IsSync";

    // P_TAG_RESULT Table
    public static final String P_TAG_RESULT_TABLE = "ProductionTagResults";
    public static final String P_TAG_RESULT_ID = "PTagResultsID";
    public static final String P_TAG_RESULT_OEM_ID = "OEMID";
    public static final String P_TAG_RESULT_MAC_ID = "MacID";
    public static final String P_TAG_RESULT_LOGGER_ID = "LoggerID";
    public static final String P_TAG_RESULT_RUN_STATUS_ID = "RunStatusID";
    public static final String P_TAG_RESULT_TAG_ID = "ProductionTagName";
    public static final String P_TAG_RESULT_TARGET_WEIGHT = "TargetWeight";
    public static final String P_TAG_RESULT_HEAD = "Head";
    public static final String P_TAG_RESULT_STATUS_ABORTED = "StatusAborted";
    public static final String P_TAG_RESULT_MODIFIED_DATE = "ModifiedDate";
    public static final String P_TAG_RESULT_IS_AUTHORIZED = "IsAuthorised";
    public static final String P_TAG_RESULT_MODIFIED_BY = "ModifiedBy";
    public static final String P_TAG_RESULT_IS_SYNC = "IsSync";


    //List Of Master Tables
    public static final String LIST_OF_MASTER_TABLES = "mstMasterTables";
    public static final String MASTER_TABLES_ID = "MasterTableId";
    public static final String MASTER_TABLES_NAME = "MasterTableName";
    public static final String MASTER_TABLES_DISLAY_NAME = "MasterTableDisplayName";

    //kg values table
    public static final String PRODUCTION_KG_VALUES_MASTER_TABLES = "mstProductionGrammageMaster";
    public static final String PRODUCTION_GRAMMAGE_ID = "grammage_id";
    public static final String PRODUCTION_GRAMMAGE = "grammage_name";
    public static final String PRODUCTION_KG_VALUES = "grammage_value";

    //Bag length variation RESULT table
    public static final String BAG_LENGTH_VARIATION_RESULT = "CalibrationBagLengthVariationResults";
    public static final String BAG_LENGTH_VARIATION_BAG_LENGTH_ID = "BagLengthID";
    public static final String BAG_LENGTH_VARIATION_OEM_ID = "OEMID";
    public static final String BAG_LENGTH_VARIATION_MAC_ID = "MacID";
    public static final String BAG_LENGTH_VARIATION_LOGGER_ID = "LoggerID";
    public static final String BAG_LENGTH_VARIATION_TAG_ID = "TagID";
    public static final String BAG_LENGTH_VARIATION_OINUM = "OINumber";
    public static final String BAG_LENGTH_VARIATION_LENGTH = "BagLength";
    public static final String BAG_LENGTH_VARIATION_MODIFIED_DATE = "ModifiedDate";
    public static final String BAG_LENGTH_VARIATION_IS_SYNC = "IsSync";

    //Bag Length Analytics
    public static final String BAG_LENGTH_ANALYTICS_TABLE = "CalibrationBagLengthAnalytics";
    public static final String BAG_LEN_ANA_ID = "BagLengthAnalyticsID";
    public static final String BAG_LEN_ANA_OEM_ID = "OEMID";
    public static final String BAG_LEN_ANA_LOGGER_ID = "LoggerID";
    public static final String BAG_LEN_ANA_TAG_ID = "TagID";
    public static final String BAG_LEN_ANA_MIN_READING = "MinReading";
    public static final String BAG_LEN_ANA_MAX_READING = "MaxReading";
    public static final String BAG_LEN_ANA_RANGE_READING = "RangeReading";
    public static final String BAG_LEN_ANA_RUN_STATUS = "RunStatus";
    public static final String BAG_LEN_ANA_MODIFIED_DATE = "ModifiedDate";
    public static final String BAG_LEN_ANA_MEAN_READING = "MeanReading";
    public static final String BAG_LEN_ANA_IS_SYNC = "IsSync";

    //PAPER SHIFT RESULT TABLE
    public static final String PAPER_SHIFT_RESULTS = "CalibrationPaperShiftResults";
    public static final String PAPER_SHIFT_RESULTS_PAPER_SHIFT_ID = "PaperShiftID";
    public static final String PAPER_SHIFT_RESULTS_OEM_ID = "OEMID";
    public static final String PAPER_SHIFT_RESULTS_MAC_ID = "MacID";
    public static final String PAPER_SHIFT_RESULTS_LOGGER_ID = "LoggerID";
    public static final String PAPER_SHIFT_RESULTS_TAG_ID = "TagID";
    public static final String PAPER_SHIFT_RESULTS_SHIFT = "Shift";
    public static final String PAPER_SHIFT_RESULTS_OINUM = "OINumber";
    public static final String PAPER_SHIFT_RESULTS_MODIFIED_DATE = "ModifiedDate";
    public static final String PAPER_SHIFT_RESULTS_IS_SYNC = "IsSync";

    //Paper Shift Analytics
    public static final String PAPER_SHIFT_ANALYTICS_TABLE = "CalibrationPaperShiftAnalytics";
    public static final String PAPER_SHIFT_ANALYTICS_ID = "PaperShiftAnalyticsID";
    public static final String PAPER_SHIFT_ANALYTICS_OEM_ID = "OEMID";
    public static final String PAPER_SHIFT_ANALYTICS_LOGGER_ID = "LoggerID";
    public static final String PAPER_SHIFT_ANALYTICS_TAG_ID = "TagID";
    public static final String PAPER_SHIFT_ANALYTICS_MIN_READING = "MinReading";
    public static final String PAPER_SHIFT_ANALYTICS_MAX_READING = "MaxReading";
    public static final String PAPER_SHIFT_ANALYTICS_RANGE_READING = "RangeReading";
    public static final String PAPER_SHIFT_ANALYTICS_RUN_STATUS = "RunStatus";
    public static final String PAPER_SHIFT_ANALYTICS_MEAN_READING = "MeanReading";
    public static final String PAPER_SHIFT_ANALYTICS_MODIFIED_DATE = "ModifiedDate";
    public static final String PAPER_SHIFT_ANALYTICS_IS_SYNC = "IsSync";

    //Weight Results Analytics.
    public static final String WEIGHT_RESULTS_ANALYTICS_TABLE = "CalibrationWeightTestResultstAnalytics";
    public static final String WEIGHT_RESULTS_ANALYTICS_ID = "WeightResultsAnalyticsID";
    public static final String WEIGHT_RESULTS_ANALYTICS_OEM_ID = "OEMID";
    public static final String WEIGHT_RESULTS_ANALYTICS_LOGGER_ID = "LoggerID";
    public static final String WEIGHT_RESULTS_ANALYTICS_TAG_ID = "TagID";
    public static final String WEIGHT_RESULTS_ANALYTICS_MIN_READING = "MinReading";
    public static final String WEIGHT_RESULTS_ANALYTICS_MAX_READING = "MaxReading";
    public static final String WEIGHT_RESULTS_ANALYTICS_RANGE_READING = "RangeReading";
    public static final String WEIGHT_RESULTS_ANALYTICS_MEAN_READING = "MeanReading";
    public static final String WEIGHT_RESULTS_ANALYTICS_SD_READING = "SDReading";
    public static final String WEIGHT_RESULTS_ANALYTICS_REMARKS = "Remarks";
    public static final String WEIGHT_RESULTS_ANALYTICS_RUN_STATUS = "RunStatus";
    public static final String WEIGHT_RESULTS_ANALYTICS_GM = "GM";
    public static final String WEIGHT_RESULTS_ANALYTICS_GM_PERCENT = "GMPercent";
    public static final String WEIGHT_RESULTS_ANALYTICS_GM_SD = "GMSD";
    public static final String WEIGHT_RESULTS_ANALYTICS_GM_SD_PERCENT = "GMSDPercent";
    public static final String WEIGHT_RESULTS_ANALYTICS_AVERAGE_GIVE_AWAY = "AverageGiveAway";
    public static final String WEIGHT_RESULTS_ANALYTICS_MODIFIED_DATE = "ModifiedDate";
    public static final String WEIGHT_RESULTS_ANALYTICS_IS_SYNC = "IsSync";

    //Weight Test Result
    public static final String WEIGHT_TEST_RESULT = "CalibrationWeightTestResults";
    public static final String WEIGHT_TEST_RESULT_TEST_WEIGHT_ID = "TestWeightID";
    public static final String WEIGHT_TEST_RESULT_OEMID = "OEMID";
    public static final String WEIGHT_TEST_RESULT_MAC_ID = "MacID";
    public static final String WEIGHT_TEST_RESULT_LOGGER_ID = "LoggerID";
    public static final String WEIGHT_TEST_RESULT_TAG_ID = "TagID";
    public static final String WEIGHT_TEST_RESULT_WEIGHT = "Weight";
    public static final String WEIGHT_TEST_RESULT_OINUM = "OINumber";
    public static final String WEIGHT_TEST_RESULT_MODIFIED_DATE = "ModifiedDate";
    public static final String WEIGHT_TEST_RESULTS_IS_SYNC = "IsSync";

    //Comments Table
    public static final String COMMENTS_TABLE = "CalibrationComments";
    public static final String COMMENTS_ID = "CommentsID";
    public static final String COMMENTS_OEM_ID = "OEMID";
    public static final String COMMENTS_MAC_ID = "MacID";
    public static final String COMMENTS_LOGGER_ID = "LoggerID";
    public static final String COMMENTS_TAG_ID = "TagID";
    public static final String COMMENTS_COMMENTS_COLUMN = "Comments";
    public static final String COMMENTS_FUN_COMMENTS_1 = "FunComment1";
    public static final String COMMENTS_FUN_COMMENTS_2 = "FunComment2";
    public static final String COMMENTS_FUN_COMMENTS_3 = "FunComment3";
    public static final String COMMENTS_FUN_COMMENTS_4 = "FunComment4";
    public static final String COMMENTS_FUN_COMMENTS_5 = "FunComment5";
    public static final String COMMENTS_MODIFIED_DATE = "ModifiedDate";
    public static final String COMMENTS_IS_SYNC = "IsSync";




    //Observations Table
    public static final String OBSERVATIONS_TABLE = "CalibrationObservations";
    public static final String OBSERVATIONS_ID = "ObservationsID";
    public static final String OBSERVATIONS_OEM_ID = "OEMID";
    public static final String OBSERVATIONS_MAC_ID = "MacID";
    public static final String OBSERVATIONS_LOGGER_ID = "LoggerID";
    public static final String OBSERVATIONS_TAG_ID = "TagID";
    public static final String OBSERVATIONS_AUGER_COUNT = "AugerCount";
    public static final String OBSERVATIONS_AUGER_SPEED = "AugerSpeed";
    public static final String OBSERVATIONS_AUGER_PRE_AUGER = "AugerPreUsed";
    public static final String OBSERVATIONS_AUGER_NO_OF_AGITATORS = "AugerNumberOfAgitators";
    public static final String OBSERVATIONS_MACHINE_VERTICAL_TEFLON = "McVerticalTeflon";
    public static final String OBSERVATIONS_MACHINE_HORIZONTAL_LAW_OPENING = "McHorizontalJawOpeneing";
    public static final String OBSERVATIONS_MACHINE_HORIZONTAL_LAW_MOVEMENT = "McHorizontalJawMovement";
    public static final String OBSERVATIONS_MACHINE_VERTICAL_ALIGN_CENTER = "McVerticalAlignCenter";
    public static final String OBSERVATIONS_MACHINE_VERTICAL_LAW_MOVEMENT = "McVerticalJawMovement";
    public static final String OBSERVATIONS_PACKAGE_WIRE_CUP = "PkgWireCup";
    public static final String OBSERVATIONS_PACKAGE_EMPTY_POUCHES = "PkgEmptyPouches";
    public static final String OBSERVATIONS_PACKAGE_BAG_LENGTH_VARIATION = "PkgBaglengthVariation";
    public static final String OBSERVATIONS_PACKAGE_PAPER_SHIFTING = "PkgPaperShifting";
    public static final String OBSERVATIONS_PACKAGE_VERTICAL_OVERLAP = "PkgVerticalOverlap";
    public static final String OBSERVATIONS_PACKAGE_POUCH_SYMMETRY = "PkgPouchSymmetry";
    public static final String OBSERVATIONS_PACKAGE_OBSERV_SPEED = "ObservSpeed";
    public static final String OBSERVATIONS_SEAL_REJECTION_REASON = "SealRejectionReason";
    public static final String OBSERVATIONS_PACKAGE_IS_SYNC = "IsSync";

    /*
     * WAC TABLES CONSTANTS ENDS
     *
     * */




    public static final String QUERY_INSERT_TAGTBL = "INSERT INTO tags " +
            "(tagid,oinum,targetwt,fillertype,customer,machinetype," +
            "producttype,product,operator,targetwtunit,inspector," +
            "filmtype,productfeed,pouchwidth,pouchlength,agitatormotorfreq," +
            "agitatormode,agitatorod,agitatorid,agitatorpitch,targetacctype," +
            "targetaccval,mcserialnum,prbulkmain,targetspeed,customerproduct," +
            "head,airpressure,qtysetting,bglensetting,fillvalvegap,fillvfdfreq," +
            "horizcutsealsetting,horizbandsealsetting,vertsealsetting,augeragitatorfreq," +
            "augerfillvalve,augurpitch,pouchsealtype,pouchtemphzf,pouchtemphzb,pouchtempvert," +
            "pouchtempvertfl,pouchtempvertfr,pouchtempvertbl,pouchtempvertbr,pouchgusset," +
            "customerfilm,pouchfilmthickness,pouchprbulk,poucheqhorizserrations,pouchfillevent,negWt,fyyear,createddate)" +
            "values (?,?,?,?,?,?,?,?,?," +
            "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )";



    /*
     * SWAS TABLES CONSTANTS
     *
     * */

    //User Login Table
    public static final String TABLE_USER_DETAILS = "UserLoginDetails";
    public static final String SWAS_USER_ID="UserID";
    public static final String USER_TOKEN="UserToken";
    public static final String USER_EMAILID="UserEmailID";
    public static final String USER_TOKEN_TYPE="UserTokenType";
    public static final String USER_OEMID="UserOEMID";
    public static final String USER_ROLE_NAME="UserRoleName";
    public static final String USER_EXPIRES_IN="UserExpiresIn";
    public static final String USER_OEM_NAME="UserOEMName";
    public static final String USER_FIRST_NAME="UserFirstName";
    public static final String USER_LAST_NAME="UserLastName";
    public static final String USER_LOGIN_ID="UserLoginID";
    public static final String USER_IS_ISSUED="UserIsIssued";
    public static final String USER_EXPIRES="UserExpires";

    //List of services by status
    public static final String TABLE_LIST_OF_SERVICES_BY_STATUS = "ListOfServicesByStatus";
    public static final String LIST_OF_SERVICES_BY_ID="ID";
    public static final String LIST_OF_SERVICES_BY_STATUS_SERVICE_CALL_ID="ServiceCallID";
    public static final String LIST_OF_SERVICES_BY_STATUS_CUSTOMER_ID="CustomerID";
    public static final String LIST_OF_SERVICES_BY_STATUS_LOCATION_NAME="LOCATIONNAME";
    public static final String LIST_OF_SERVICES_BY_STATUS_ITEM_ID="ITEMID";
    public static final String LIST_OF_SERVICES_BY_STATUS_SERVICE_ENGINEER_NAME="ServiceEngineerName";
    public static final String LIST_OF_SERVICES_BY_STATUS_STATUS_TEXT="StatusText";
    public static final String LIST_OF_SERVICES_BY_STATUS_TYPE_OF_SERVICE_CALL_TEXT="TypeOfServiceCallText";
    public static final String LIST_OF_SERVICES_BY_STATUS_CREATED_BY="CreatedBy";
    public static final String LIST_OF_SERVICES_BY_STATUS_CREATED_DATE="CreatedDate";
    public static final String LIST_OF_SERVICES_BY_STATUS_UPDATED_BY="UpdatedBy";
    public static final String LIST_OF_SERVICES_BY_STATUS_UPDATED_DATE="UpdatedDate";
    public static final String LIST_OF_SERVICES_BY_STATUS_SERVICE_CALL_STATUS_ID="ServiceCallStatusID";
    public static final String LIST_OF_SERVICES_BY_STATUS_NAME="NAME";
    public static final String LIST_OF_SERVICES_BY_STATUS_BRIEF_COMPLAINT="BriefComplaint";
    public static final String LIST_OF_SERVICES_BY_STATUS_TYPE_OF_FAULT_TEXT="TypeOfFaultText";
    public static final String LIST_OF_SERVICES_BY_STATUS_FAULTY_PART="FaultyPart";
    public static final String LIST_OF_SERVICES_BY_STATUS_CURRENCY="CURRENCY";
    public static final String LIST_OF_SERVICES_BY_STATUS_DOCUMENTS_PATH="DocumentPath";
    public static final String LIST_OF_SERVICES_BY_STATUS_TYPE_OF_SERVICE_CALL_ID="TypeOfServiceCallID";
    public static final String LIST_OF_SERVICES_BY_STATUS_CURRENTLY_ASSIGNED_TO_SE="CurrentlyAssignedToSE";
    public static final String LIST_OF_SERVICES_BY_STATUS_CLOSED_DATE_TIME="ClosedDateTime";
    public static final String LIST_OF_SERVICES_BY_STATUS_CLOSED_BY="ClosedBy";
    public static final String LIST_OF_SERVICES_BY_STATUS_COMMENTS="Comments";
    public static final String LIST_OF_SERVICES_BY_STATUS_SERVICE_ENGINEER_ID="ServiceEngineerID";
    public static final String LIST_OF_SERVICES_BY_STATUS_NUMBER_OF_VISITS="NumberOfVisits";
    public static final String LIST_OF_SERVICES_BY_STATUS_SHORT_TERM="ShortTermResolutionText";
    public static final String LIST_OF_SERVICES_BY_STATUS_LONG_TERM="LongTermResolutionText";
    public static final String LIST_OF_SERVICES_BY_STATUS_SHORT_TERM_ID="ShortTermResolutionID";
    public static final String LIST_OF_SERVICES_BY_STATUS_LONG_TERM_ID="LongTermResolutionID";
    public static final String LIST_OF_SERVICES_BY_STATUS_ROOT_CAUSE_ID="RootCauseID";
    public static final String LIST_OF_SERVICES_BY_STATUS_SE_REMARK="SERemark";
    public static final String LIST_OF_SERVICES_BY_STATUS_QE_REMARK="QERemark";
    public static final String LIST_OF_SERVICES_BY_STATUS_ROOT_CAUSE_NAME="RootCauseName";
    public static final String LIST_OF_SERVICES_BY_STATUS_DESCRIPTION="Description";
    public static final String LIST_OF_SERVICES_BY_STATUS_DEPARTMENT_ID="DepartmentID";
    public static final String LIST_OF_SERVICES_BY_STATUS_DEPARTMENTS="Departments";
    public static final String LIST_OF_SERVICES_BY_STATUS_USER_ID="User_Id";

    public static final String TABLE_OPERATION_DETAILS="ticket_operation_details";
    public static final String ticketid="ptkd_PktId";
    public static final String ticketno="ptkd_TkNo";
    public static final String tkpsku="ptkd_PSKU";
    public static final String tkplant="ptkd_Plant";
    public static final String valuestreamcode="ptkd_ValueStreamCode";
    public static final String fttypecode="ptkd_Ftypecode";
    public static final String tkoperation="ptkd_Operation";
    public static final String tkqty="ptkd_Qty";
    public static final String isrelease="ptkd_IsRelease";
    public static final String unitname="uni_unit_name";
    public static final String operationname="opr_operation_name";
    public static final String valuestream="val_valuestream_name";
    public static final String fttypedesc="ft_ftype_desc";
    public static final String actualstartdate="tk_Actual_Start_Date";
    public static final String actualenddate="tk_Actual_End_Date";
    public static final String produceqty="produceqty";

    public static final String TABLE_PART_ORDERS="PartOrders";
    public static final String PART_ORDERS_ID="PartOrderID";
    public static final String PART_ORDERS_SERVICE_CALL_ID="ServiceCallID";
    public static final String PART_ORDERS_OEMID="OEMID";
    public static final String PART_ORDERS_CUSTOMER_ID="CustomerID";
    public static final String PART_ORDERS_ITEMID="ITEMID";
    public static final String PART_ORDERS_QUANTITY="Quantity";
    public static final String PART_ORDERS_NAME="NAME";
    public static final String PART_ORDERS_PRICE="PRICE";
    public static final String PART_ORDERS_APPLICABLE="Applicable";
    public static final String PART_ORDERS_FINAL_PRICE="FinalPrice";
    public static final String PART_ORDERS_EXCLUDE_INCLUDE="ExcludeInclude";
    public static final String PART_ORDERS_CREATED_BY="CreatedBy";
    public static final String PART_ORDERS_CREATED_DATE="CreatedDate";

    public static final String TABLE_SERVICE_CALL_FEEDBACK="ServiceCallFeedback";
    public static final String TABLE_SERVICE_CALL_FEEDBACK_ID="ServiceCallFeedbackID";
    public static final String SERVICE_CALL_FEEDBACK_FEEDBACK_DETAILS_ID="FeedbackDetailsID";
    public static final String SERVICE_CALL_FEEDBACK_OEM_ID="OEMID";
    public static final String SERVICE_CALL_FEEDBACK_SERVICE_CALL_ID="ServiceCallID";
    public static final String SERVICE_CALL_FEEDBACK_USER_ID="UserID";
    public static final String SERVICE_CALL_FEEDBACK_USER_FEEDBACK="UserFeedBack";
    public static final String SERVICE_CALL_FEEDBACK_CREATED_BY="CreatedBy";
    public static final String SERVICE_CALL_FEEDBACK_CREATED_DATE="CreatedDate";
    public static final String SERVICE_CALL_FEEDBACK_IS_SYNC="IsSync";

    public static final String TABLE_EXPENSE_DETAILS="ExpenseDetails";
    public static final String EXPENSE_DETAILS_TRAVELLING_EXPENSES_ID="ExpensesID";
    public static final String EXPENSE_DETAILS_TRAVELLING_EXPENSES_HEADS_ID="TravellingExpensesHeadsID";
    public static final String EXPENSE_DETAILS_SERVICE_CALL_ID="ServiceCallID";
    public static final String EXPENSE_DETAILS_OEMID ="OEMID";
    public static final String EXPENSE_DETAILS_CUSTOMER_ID ="CustomerID";
    public static final String EXPENSE_DETAILS_SERVICE_EXPENSE_HEADS_ID ="Service_Expense_Heads_id";
    public static final String EXPENSE_DETAILS_SERVICE_EXPENSE_HEADS_NAME ="Service_Expense_Heads_Name";
    public static final String EXPENSE_DETAILS_VISIT_DATES ="VisitDates";
    public static final String EXPENSE_DETAILS_VISIT_CHARGES_RS ="VisitCharges";
    public static final String EXPENSE_DETAILS_CREATED_BY ="CreatedBy";
    public static final String EXPENSE_DETAILS_CREATED_DATE ="CreatedDate";
    public static final String EXPENSE_DETAILS_IS_SYNC ="IsSync";

    public static final String TABLE_SERVICE_CALL_MC_PARAMETERS="ServiceCallMCParameters";
    public static final String SERVICE_CALL_MC_PARAMETERS_ID="ID";
    public static final String SERVICE_CALL_MC_PARAMETERS_MACHINE_PARAMETER_LOG_ID="MachineParameterLogID";
    public static final String SERVICE_CALL_MC_PARAMETERS_OEM_ID="OEMID";
    public static final String SERVICE_CALL_MC_PARAMETERS_CUSTOMER_ID="CustomerID";
    public static final String SERVICE_CALL_MC_PARAMETERS_SERVICE_CALL_ID="ServiceCallID";
    public static final String SERVICE_CALL_MC_PARAMETERS_MACHINE_PARAMETERS="MachineParameters";
    public static final String SERVICE_CALL_MC_PARAMETERS_SERVICE_CALL_START_DATE="ServiceCallStartDate";
    public static final String SERVICE_CALL_MC_PARAMETERS_SERVICE_CALL_END_DATE="ServiceCallEndDate";
    public static final String SERVICE_CALL_MC_PARAMETERS_PARAMETER_VALUE="ParameterValue";
    public static final String SERVICE_CALL_MC_PARAMETERS_UNIT="Unit";
    public static final String SERVICE_CALL_MC_PARAMETERS_REMARK="Remark";
    public static final String SERVICE_CALL_MC_PARAMETERS_CREATED_BY="CreatedBy";
    public static final String SERVICE_CALL_MC_PARAMETERS_CREATED_DATE="CreatedDate";
    public static final String SERVICE_CALL_MC_PARAMETERS_IS_SYNC="IsSync";

    public static final String TABLE_SERVICE_CALL_COMP_PARAMETERS="ServiceCallCompPara";
    public static final String SERVICE_CALL_COMP_PARAMETERS_ID="ID";
    public static final String SERVICE_CALL_COMP_PARAMETERS_COMPITITORS_PARMETER_ID="CompetitorsParameterID";
    public static final String SERVICE_CALL_COMP_PARAMETERS_OEM_ID="OEMID";
    public static final String SERVICE_CALL_COMP_PARAMETERS_CUSTOMER_ID="CustomerID";
    public static final String SERVICE_CALL_COMP_PARAMETERS_SERVICE_CALL_ID="ServiceCallID";
    public static final String SERVICE_CALL_COMP_PARAMETERS_COMPITITORS_PARMETER="CompetitorsParameter";
    public static final String SERVICE_CALL_COMP_PARAMETERS_SERVICE_CALL_START_DATE="ServiceCallStartDate";
    public static final String SERVICE_CALL_COMP_PARAMETERS_MACHINE_PARA_VALUE="MachineParameterValues";
    public static final String SERVICE_CALL_COMP_PARAMETERS_REMARK="Remark";
    public static final String SERVICE_CALL_COMP_PARAMETERS_CREATED_BY="CreatedBy";
    public static final String SERVICE_CALL_COMP_PARAMETERS_CREATED_DATE="CreatedDate";
    public static final String SERVICE_CALL_COMP_PARAMETERS_UNIT="Unit";
    public static final String SERVICE_CALL_COMP_PARAMETERS_IS_SYNC="IsSync";

    public static final String TABLE_VISIT_LOGS="VisitLog";
    public static final String VISIT_LOGS_ID="VisitLogID";
    public static final String VISIT_LOGS_SERVICE_LOG_ID="ServiceLogID";
    public static final String VISIT_LOGS_OEM_ID="OEMID";
    public static final String VISIT_LOGS_SERVICE_CALL_ID="ServiceCallID";
    public static final String VISIT_LOGS_CUSTOMER_ID="CustomerID";
    public static final String VISIT_LOGS_SERVICE_ENGINEER_ID="ServiceEngineerID";
    public static final String VISIT_LOGS_PART_STATUS="PartStatus";
    public static final String VISIT_LOGS_SERVICE_CALL_START_DATE="ServiceCallStartDate";
    public static final String VISIT_LOGS_SERVICE_CALL_END_DATE="ServiceCallEndDate";
    public static final String VISIT_LOGS_SERVICE_CALL_ENTRY_TIME="ServiceCallEntryTime";
    public static final String VISIT_LOGS_SERVICE_CALL_EXIT_TIME="ServiceCallExitTime";
    public static final String VISIT_LOGS_GATE_PASS_IMAGE_PATH="GatePassImagePath";
    public static final String VISIT_LOGS_REMARK="REMARK";
    public static final String VISIT_LOGS_CREATED_BY="CreatedBy";
    public static final String VISIT_LOGS_CREATED_DATE="CreatedDate";
    public static final String VISIT_LOGS_IS_SYNC="IsSync";

    public static final String TABLE_SCHEDULED_DATES="ScheduledDates";
    public static final String SCHEDULED_DATES_ID="ScheduledDatesID";
    public static final String SCHEDULED_DATES_SERVICE_ENGINEER_DETAILS_ID="ServiceEngineerDetailsID";
    public static final String SCHEDULED_DATES_OEM_ID="OEMID";
    public static final String SCHEDULED_DATES_CUSTOMER_ID="CustomerID";
    public static final String SCHEDULED_DATES_SERVICE_CALL_ID="Service_Call_ID";
    public static final String SCHEDULED_DATES_SERVICE_ENGINEER_ID="ServiceEngineerID";
    public static final String SCHEDULED_DATES_IS_ASSIGNED="IsAssigned";
    public static final String SCHEDULED_DATES_IS_ACCEPTED="IsAccepted";
    public static final String SCHEDULED_DATES_ASSIGNED_DATE="AssignedDate";
    public static final String SCHEDULED_DATES_ASSIGNED_TIME="AssignedTime";
    public static final String SCHEDULED_DATES_NOTE="Note";
    public static final String SCHEDULED_DATES_CREATED_BY="CreatedBy";
    public static final String SCHEDULED_DATES_CREATED_DATE="CreatedDate";
    public static final String SCHEDULED_DATES_UPDATE_BY="UpdatedBy";
    public static final String SCHEDULED_DATES_IS_ACTIVE="IsActive";
    public static final String SCHEDULED_DATES_UPDATE_DATE="UpdatedDate";
    public static final String SCHEDULED_DATES_LOCATION_NAME="LOCATIONNAME";
    public static final String SCHEDULED_DATES_COMMON_NAME="CommonName";
    public static final String SCHEDULED_DATES_IS_SYNC="IsSync";

    public static final String TABLE_SERVICE_CALL_CHECKLIST="ServiceCallCheckList";
    public static final String SERVICE_CALL_CHECKLIST_ID="ServiceCallCheckListID";
    public static final String SERVICE_CALL_CHECKLIST_SERVICE_CALL_ID="ServiceCallID";
    public static final String SERVICE_CALL_CHECKLIST_CHECKLIST_FOR_SITE_READYNESS_ID="ChecklistForSiteReadynessID";
    public static final String SERVICE_CALL_CHECKLIST_CHECKLIST_FOR_SITE_READYNESS_TEXT="ChecklistForSiteReadynessText";
    public static final String SERVICE_CALL_CHECKLIST_CHECKED="Checked";

    public static final String TABLE_SERVICE_CALL_FAULT_CHAR="ServiceCallFaultChar";
    public static final String SERVICE_CALL_FAULT_CHAR_ID="ServiceCallFaultCharID";
    public static final String SERVICE_CALL_FAULT_CHAR_CUSTOMER_ID="CustomerID";
    public static final String SERVICE_CALL_FAULT_CHAR_SERVICE_CALL_ID="ServiceCallID";
    public static final String SERVICE_CALL_FAULT_CHAR_FAULT_CHAR_TEXT="FaultCharacteristicsText";

    public static final String TABLE_EXPENSE_HEAD_MASTER="ExpenseHeadmaster";
    public static final String EXPENSE_HEAD_MASTER_ID="ExpenseHeadmasterId";
    public static final String EXPENSE_HEAD_MASTER_SERVICE_EXPENSE_HEADS_ID="Service_Expense_Heads_id";
    public static final String EXPENSE_HEAD_MASTER_SERVICE_EXPENSE_HEADS_NAME="Service_Expense_Heads_Name";

    public static final String TABLE_DOCUMENTS="Documents";
    public static final String DOCUMENTS_ID="DocumentID";
    public static final String DOCUMENTS_REPORT_DOCUMENTS_ID="ReportDocumentID";
    public static final String DOCUMENTS_REPORT_OEM_ID="OEMID";
    public static final String DOCUMENTS_REPORT_CUSTOMER_ID="CustomerID";
    public static final String DOCUMENTS_REPORT_SERVICE_CALL_ID="ServiceCallID";
    public static final String DOCUMENTS_REPORT_DOCUMENT_PATH="DocumentPath";
    public static final String DOCUMENTS_REPORT_DOCUMENT_NAME="DocumentName";
    public static final String DOCUMENTS_REPORT_IS_ACTIVE="IsActive";
    public static final String DOCUMENTS_REPORT_CREATED_BY="CreatedBy";
    public static final String DOCUMENTS_REPORT_CREATED_DATE="CreatedDate";
    public static final String DOCUMENTS_REPORT_UPDATED_BY="UpdatedBy";
    public static final String DOCUMENTS_REPORT_UPDATED_DATE="UpdatedDate";
    public static final String DOCUMENTS_REPORT_IS_SYNC="IsSync";

    public static final String TABLE_MACHINE_PARAMETER_INPROCESS_CALL="MachineParametreInprocessCall";
    public static final String MACHINE_PARAMETER_ID="ID";
    public static final String MACHINE_PARAMETER_INPROCESS_CALL_ID="MachineParameterID";
    public static final String MACHINE_PARAMETER_INPROCESS_CALL_NAME="MachineParameters";
    public static final String MACHINE_PARAMETER_INPROCESS_CALL_UNIT="UNIT";

    public static final String TABLE_MACHINE_PARAMETER_COMPLTEDC_CALLS="MachineParametreCompletedCall";
    public static final String MACHINE_PARAMETER_COMPLTEDC_ID="ID";
    public static final String MACHINE_PARAMETER_COMPLTEDC_CALLS_MACHINE_PARAMETER_LOG_ID="MachineParameterLogID";
    public static final String MACHINE_PARAMETER_COMPLTEDC_CALLS_OEM_ID="OEMID";
    public static final String MACHINE_PARAMETER_COMPLTEDC_CALLS_CUSTOMER_ID="CustomerID";
    public static final String MACHINE_PARAMETER_COMPLTEDC_CALLS_SERVICE_CALL_ID="ServiceCallID";
    public static final String MACHINE_PARAMETER_COMPLTEDC_CALLS_MACHINE_PARAMETERS="MachineParameters";
    public static final String MACHINE_PARAMETER_COMPLTEDC_CALLS_SERVICE_CALL_START_DATE="ServiceCallStartDate";
    public static final String MACHINE_PARAMETER_COMPLTEDC_CALLS_SERVICE_CALL_END_DATE="ServiceCallEndDate";
    public static final String MACHINE_PARAMETER_COMPLTEDC_CALLS_PARAMETER_VALUE="ParameterValue";
    public static final String MACHINE_PARAMETER_COMPLTEDC_CALLS_UNIT="Unit";
    public static final String MACHINE_PARAMETER_COMPLTEDC_CALLS_REMARK="Remark";
    public static final String MACHINE_PARAMETER_COMPLTEDC_CALLS_CREATED_BY="CreatedBy";
    public static final String MACHINE_PARAMETER_COMPLTEDC_CALLS_CREATED_DATE="CreatedDate";

    public static final String TABLE_COMPITITIVE_MACHINE_PARAMS_IN_PROCESS_CALL="CompititiveMachineParamsInProcessCall";
    public static final String COMPITITIVE_MACHINE_PARAMS_IN_PROCESS_CALL_ID="ID";
    public static final String COMPITITIVE_MACHINE_PARAMS_IN_PROCESS_CALL_COMP_PARAMS_ID="CompetitorsParameterID";
    public static final String COMPITITIVE_MACHINE_PARAMS_IN_PROCESS_CALL__COMP_PARAMS="CompetitorsParameters";
    public static final String COMPITITIVE_MACHINE_PARAMS_IN_PROCESS_CALL_UNIT="UNIT";

    public static final String TABLE_SHORT_TERM_LIST_TABLE="ShortTermLisTable";
    public static final String TABLE_SHORT_TERM_RESOLUTION_ID="ShortTermResolutionID";
    public static final String TABLE_SHORT_TERM_RESOLUTION_TEXT="ShortTermResolutionText";

    public static final String TABLE_STATUS_CALL="StatusCAllTable";
    public static final String TABLE_STATUS_CALL_SERVICE_CALL_STATUS_ID="ServiceCallStatusID";
    public static final String TABLE_STATUS_CALL_STATUS_TEXT="StatusText";
    /*
     * SWAS TABLES CONSTANTS ENDS
     *
     * */


    /*
    * FalcoJK Table Constants
    * */
    public static  final String PACKING_TICKET_TABLE ="PackingTicketTable";
    public static  final String PACKING_TICKET_ID ="ID";
    public static  final String PACKING_TICKET_TK_ID ="tk_Id";
    public static  final String PACKING_TICKET_TICKET_NO ="tk_TicketNo";
    public static  final String PACKING_TICKET_TICKET_TYPE ="ticket_type";
    public static  final String PACKING_TICKET_TK_SONO ="tk_SONO";
    public static  final String PACKING_TICKET_TK_PLANT ="tk_Plant";
    public static  final String PACKING_TICKET_TK_SKU ="tk_SKU";
    public static  final String PACKING_TICKET_TK_PROD_SKU ="tk_Prod_SKU";
    public static  final String PACKING_TICKET_TK_RELEASE_DATE ="tk_Release_Date";
    public static  final String PACKING_TICKET_TK_START_DATE ="tk_Start_Date";
    public static  final String PACKING_TICKET_TK_END_DATE ="tk_End_Date";
    public static  final String PACKING_TICKET_TK_QUANTITY ="tk_Quantity";
    public static  final String PACKING_TICKET_TK_UNPACKING_SKU ="tk_Unpaking_SKU";
    public static  final String PACKING_TICKET_TK_UNPACKING ="tk_Unpacking";
    public static  final String PACKING_TICKET_TK_ACTIVE ="tk_Active";
    public static  final String PACKING_TICKET_TK_RELEAE ="tk_Release";
    public static  final String PACKING_TICKET_MONTH_START ="MonthStart";
    public static  final String PACKING_TICKET_MONTH_END ="MonthEnd";
    public static  final String PACKING_TICKET_TOTAL_QTY ="tk_totalQty";
    public static  final String PACKING_TICKET_CRETAED_DATE ="tt_CreatedDate";
    public static  final String PACKING_TICKET_CREATED_BY ="tt_CreatedBy";
    public static  final String PACKING_TICKET_APPROVED_DATE ="tt_ApprovedDate";
    public static  final String PACKING_TICKET_APPROVED_BY ="tt_ApprovedBy";
    public static  final String PACKING_TICKET_TK_ACTUAL_END_DATE ="tk_Actual_End_Date";
    public static  final String PACKING_TICKET_TK_ACTUAL_START_DATE ="tk_Actual_Start_Date";

    public static  final String PRODUCTION_TICKET_TABLE ="ProductionTicketTable";
    public static  final String PRODUCTION_TICKET_ID ="ptkd_PktId1";
    public static  final String PRODUCTION_TICKET_TK_ID ="ptkd_PktId";
    public static  final String PRODUCTION_TICKET_NO ="ptkd_TkNo";
    public static  final String PRODUCTION_TICKET_TK_PSKU ="ptkd_PSKU";
    public static  final String PRODUCTION_TICKET_TK_PLANT ="ptkd_Plant";
    public static  final String PRODUCTION_TICKET_TK_VALUESTREAM_CODE ="ptkd_ValueStreamCode";
    public static  final String PRODUCTION_TICKET_TK_FTYPE_CODE ="ptkd_Ftypecode";
    public static  final String PRODUCTION_TICKET_TK_QTY ="ptkd_Qty";
    public static  final String PRODUCTION_TICKET_TK_ISRELEASE ="ptkd_IsRelease";
    public static  final String PRODUCTION_TICKET_TK_START_DATE ="tk_Actual_Start_Date";
    public static  final String PRODUCTION_TICKET_TK_END_DATE ="tk_Actual_End_Date";
    public static  final String PRODUCTION_TICKET_TK_IS_RELEASE1 ="ptkd_IsRelease1";
    public static  final String PRODUCTION_TICKET_TK_RELEASE_DATE ="ptkd_ReleaseDate";
    public static  final String PRODUCTION_TICKET_TK_ACTIVE ="ptkd_Active";
    public static  final String PRODUCTION_TICKET_TK_ACTUAL_END_DATE ="tk_End_Date";
    public static  final String PRODUCTION_TICKET_TK_ACTUAL_START_DATE ="tk_start_date";
    public static  final String PRODUCTION_TICKET_TK_IS_WIP_ENTER ="IsWIP";



    public static  final String DATE_WISE_QUANTITY_PACKING_TABLE ="DatewiseQuantitytable";
    public static  final String DATE_WISE_QUANTITY_PACKING_TICKET_ID ="tt_id";
    public static  final String DATE_WISE_QUANTITY_PACKING_TICKET_TT_ID ="tt_TicketID";
    public static  final String DATE_WISE_QUANTITY_PACKING_TICKET_NO ="tt_TicketNo";
    public static  final String DATE_WISE_QUANTITY_PACKING_TICKET_PLANT ="tt_Plant";
    public static  final String DATE_WISE_QUANTITY_PACKING_TICKET_DATE ="tt_Date";
    public static  final String DATE_WISE_QUANTITY_PACKING_TICKET_TOTAL_QTY ="tt_TotalQuantity";
    public static  final String DATE_WISE_QUANTITY_PACKING_TICKET_DAYWISE_QTY ="tt_Qty";
    public static  final String DATE_WISE_QUANTITY_PACKING_TICKET_PENDING ="tt_PendingQty";
    public static  final String DATE_WISE_QUANTITY_PACKING_TICKET_DAY ="today";
    public static  final String DATE_WISE_QUANTITY_PACKING_TICKET_CREATED_BY ="tt_CreatedBy";
    public static  final String DATE_WISE_QUANTITY_PACKING_TICKET_CREATED_DATE ="tt_CreatedDate";
    public static  final String DATE_WISE_QUANTITY_PACKING_TICKET_APPROVED_DATE ="tt_ApprovedDate";
    public static  final String DATE_WISE_QUANTITY_PACKING_TICKET_APPROVED_BY ="tt_ApprovedBy";
    public static  final String DATE_WISE_QUANTITY_PACKING_TICKET_ACTIVE ="tt_Active";
    public static  final String DATE_WISE_QUANTITY_PACKING_TICKET_APPROVE ="tt_Approve";
    public static  final String DATE_WISE_QUANTITY_PACKING_TICKET_FLAG1 ="flag1";
    public static  final String DATE_WISE_QUANTITY_PACKING_TICKET_SKU ="tk_SKU";
    public static  final String DATE_WISE_QUANTITY_PACKING_TICKET_START_DATE ="tk_Start_Date";
    public static  final String DATE_WISE_QUANTITY_PACKING_TICKET_END_DATE ="tk_End_Date";
    public static  final String DATE_WISE_QUANTITY_PACKING_TICKET_ACTUAL_START_DATE ="actualstartdate";
    public static  final String DATE_WISE_QUANTITY_PACKING_TICKET_ACTUAL_END_DATE ="actualenddate";


    public static  final String PRODUCTION_TICKET_DETAILS_TABLE ="ProdutionTicketDetailsTable";
    public static  final String PRODUCTION_TICKET_DETAILS_PTKD_PACKET_ID ="ptk_PtkdId";
    public static  final String PRODUCTION_TICKET_DETAILS_PTKD_TICKET_NO ="ptk_TkNo";
    public static  final String PRODUCTION_TICKET_DETAILS_PTKD_PLANT ="ptkd_Plant";
    public static  final String PRODUCTION_TICKET_DETAILS_PTKD_VALUE_STREAM_CODE ="ptk_ValueStream";
    public static  final String PRODUCTION_TICKET_DETAILS_PTKD_OPERATION ="ptk_Operation";
    public static  final String PRODUCTION_TICKET_DETAILS_PTKD_UPDATEDATE ="ptk_UpdateDate";
    public static  final String PRODUCTION_TICKET_DETAILS_PTKD_OK_QTY ="ptk_OkQty";
    public static  final String PRODUCTION_TICKET_DETAILS_PTKD_REJECT_QTY ="ptk_RejectQty";
    public static  final String PRODUCTION_TICKET_DETAILS_PTKD_REWORK_QTY ="ptk_ReworkQty";
    public static  final String PRODUCTION_TICKET_DETAILS_PTKD_WIP_QTY ="ptk_WIPQty";
    public static  final String PRODUCTION_TICKET_DETAILS_PTKD_SECOND_QTY ="ptk_SecondQty";
    public static  final String PRODUCTION_TICKET_DETAILS_OPERATION_NAME ="opr_operation_name";
    public static  final String PRODUCTION_TICKET_DETAILS_PTKD_QTY ="ptkd_Qty";
    public static  final String PRODUCTION_TICKET_DETAILS_UNIT_NAME ="uni_unit_name";
    public static  final String PRODUCTION_TICKET_DETAILS_VALUESTREAM_NAME ="val_valuestream_name";
    public static  final String PRODUCTION_TICKET_DETAILS_TICKET_ACTUAL_START_DATE ="tk_Actual_Start_Date";
    public static  final String PRODUCTION_TICKET_DETAILS_TICKET_ACTUAL_END_DATE ="tk_Actual_End_Date";
    public static  final String PRODUCTION_TICKET_DETAILS_TICKET_START_DATE ="tk_Start_Date";
    public static  final String PRODUCTION_TICKET_DETAILS_TICKET_END_DATE ="tk_End_Date";
    public static  final String PRODUCTION_TICKET_DETAILS_PTKD_CHANGE_ROUTE_FLAG ="ptk_ChangeRoute_Flag";
    public static  final String PRODUCTION_TICKET_DETAILS_CREATED_BY ="createdby";
    public static  final String PRODUCTION_TICKET_DETAILS_CREATED_DATE ="createddate";

    public static  final String PRODUCTION_TICKET_MULTIPLE_SKU_TABLE ="ProductionTicketMultiplesku";
    public static  final String PRODUCTION_TICKET_MULTIPLE_SKU_PTKD_TICKET_NO ="ptkd_TkNo";
    public static  final String PRODUCTION_TICKET_MULTIPLE_SKU_PTKD_SKU_NO ="SKU_No";
    public static  final String PRODUCTION_TICKET_MULTIPLE_SKU_DP_STAMP_TYPE ="dp_stamp_type";
    public static  final String PRODUCTION_TICKET_MULTIPLE_SKU_DP_STAMP_CHART ="dp_stamp_chart";
    public static  final String PRODUCTION_TICKET_MULTIPLE_SKU_DP_BLKO_FLAG ="dp_blko_flg";
    public static  final String PRODUCTION_TICKET_MULTIPLE_SKU_DP_TANG_COLOR ="dp_tang_color";
    public static  final String PRODUCTION_TICKET_MULTIPLE_SKU_QTY ="QTY";
    public static  final String PRODUCTION_TICKET_MULTIPLE_SKU_ORG_QTY ="OrgQTY";
    public static  final String PRODUCTION_TICKET_MULTIPLE_SKU_CREATED_BY ="createdby";
    public static  final String PRODUCTION_TICKET_MULTIPLE_SKU_CREATED_DATE ="createddate";

    public static  final String WIP_TABLE ="WIPTable";
    public static  final String WIP_PTKD_ID ="ptkd_Id";
    public static  final String WIP_PTKD_PKTID ="ptkd_PktId";
    public static  final String WIP_PTKD_TICKET_NO ="ptkd_TkNo";
    public static  final String WIP_PTKD_PSKU ="ptkd_PSKU";
    public static  final String WIP_PTKD_PLANT ="ptkd_Plant";
    public static  final String WIP_PTKD_VALUE_STREAM_CODE ="ptkd_ValueStreamCode";
    public static  final String WIP_PTKD_OPERATION ="ptkd_Operation";
    public static  final String WIP_PTKD_WIP ="ptkd_WIP";
    public static  final String WIP_OPR_OPEARTION_NAME ="opr_operation_name";
    public static  final String WIP_UNI_UNIT_NAME ="uni_unit_name";
    public static  final String WIP_PTKD_QTY ="ptkd_Qty";
    public static  final String WIP_CREATED_BY ="createdby";
    public static  final String WIP_CREATED_DATE ="createddate";

    public static  final String TICKET_EQUIPMENT_TABLE ="TicketEquipmentTable ";
    public static  final String TICKET_EQUIPMENT_PTKD_PACKAT_ID ="ptkd_PktId ";
    public static  final String TICKET_EQUIPMENT_PTKD_TICKET_NO ="ptkd_TkNo ";
    public static  final String TICKET_EQUIPMENT_PTKD_VALUE_STRAEM ="ptk_ValueStream ";
    public static  final String TICKET_EQUIPMENT_PTKD_OPERATION ="ptk_Operation ";
    public static  final String TICKET_EQUIPMENT_PTKD_PLANT ="ptkd_Plant ";
    public static  final String TICKET_EQUIPMENT_EQUIPMENT_ID ="Eq_id ";
    public static  final String TICKET_EQUIPMENT_EQUIPMENT_NAME ="eq_name ";
    public static  final String TICKET_EQUIPMENT_EQUIPMENT_TYPE ="eq_type ";
    public static  final String TICKET_EQUIPMENT_EQUIPMENT_ASSET_NO ="eq_asset_no ";
    public static  final String TICKET_EQUIPMENT_CREATED_BY ="createdby ";
    public static  final String TICKET_EQUIPMENT_CREATED_DATE ="createddate ";

    public static  final String SAVE_DOWN_TIME_TABLE ="SaveDownTimeTable ";
    public static  final String SAVE_DOWN_TIME_PTKD_TICKET_NO ="ptkd_TkNo ";
    public static  final String SAVE_DOWN_TIME_PTKD_VALUE_STRAEM_CODE ="ptkd_ValueStreamCode ";
    public static  final String SAVE_DOWN_TIME_TT_PLANT ="tt_Plant ";
    public static  final String SAVE_DOWN_TIME_OPERATION_NO ="OperationNo ";
    public static  final String SAVE_DOWN_TIME_TE_EQUIPMENT_ID ="te_EquiId ";
    public static  final String SAVE_DOWN_TIME_TICKET_ID_NO ="TicketIDNo ";
    public static  final String SAVE_DOWN_TIME_TE_RESEASENDT ="te_ReseasenDt ";
    public static  final String SAVE_DOWN_TIME_TE_NUMBER_OF_SHIP_T ="te_NoofShipt ";
    public static  final String SAVE_DOWN_TIME_TE_AB_BR_QT ="te_Ab_Br_Qt ";
    public static  final String SAVE_DOWN_TIME_TE_MOVED_QUANTITY ="te_MovedQuntity ";
    public static  final String SAVE_DOWN_TIME_TE_ASSET_NO ="te_AssetNo ";
    public static  final String SAVE_DOWN_TIME_USER_ID ="userId ";
    public static  final String SAVE_DOWN_CR_REMARK ="cRRemark ";
    public static  final String SAVE_DOWN_CREATED_BY ="createdby ";
    public static  final String SAVE_DOWN_CREATED_DATE ="createddate ";

    public static  final String REASON_MASTER_TABLE ="ReasonMaster";
    public static  final String REASON_MASTER_ID ="Reason_id";
    public static  final String REASON_MASTER_NAME ="Reasoon_Name";
    public static  final String REASON_MASTER_CREATED_BY ="createdby";
    public static  final String REASON_MASTER_CREATED_DATE ="createddate";



    public static  final String EQUPMENT_MASTER_TABLE ="EquipmentMaster";
    public static  final String EQUPMENT_ID ="eq_id";
    public static  final String EQUPMENT_NAME ="eq_name";
    public static  final String EQUPMENT_TYPE ="eq_type";
    public static  final String EQUPMENT_REMARKS ="eq_remarks";
    public static  final String EQUPMENT_IS_APPROVED ="eq_isApproved";
    public static  final String EQUPMENT_IS_ACTIVE ="eq_isActive";
    public static  final String EQUPMENT_CREATED_DATE ="eq_createdate";
    public static  final String EQUPMENT_CREATED_BY ="eq_createby";
    public static  final String EQUPMENT_APPROVED_DATE ="eq_approvedate";
    public static  final String EQUPMENT_APPROVED_BY ="eq_approveby";

    public static  final String ASSET_NO_MASTER_TABLE ="AssetMaster";
    public static  final String ASSET_NO ="ea_asset_no";
    public static  final String ASSET_EQUIPMENT_ID ="ea_equip_id";
    public static  final String ASSET_NO_CREATED_BY ="ea_createby";
    public static  final String ASSET_NO_CREATED_DATE ="ea_createdate";
    public static  final String ASSET_NO_IS_ACTIVE ="ea_isActive";
    public static  final String ASSET_IS_APPROVED ="ea_isApproved";
    public static  final String ASSET_APPROVED_BY ="ea_approvedby";
    public static  final String ASSET_APPROVED_DATE ="ea_approvedate";

    public static final String  TICKET_WISE_OPERATION_LIST="TicketWiseOperationList";
    public static final String  TICKET_WISE_OPERATION_PTKD_ID="ptkd_Id";
    public static final String  TICKET_WISE_OPERATION_PTKID="ptkd_PktId";
    public static final String  TICKET_WISE_OPERATION_TICKET_NO="ptkd_TkNo";
    public static final String  TICKET_WISE_OPERATION_PSKU="ptkd_PSKU";
    public static final String  TICKET_WISE_OPERATION_PLANT="ptkd_Plant";
    public static final String  TICKET_WISE_OPERATION_QTY="ptkd_Qty";
    public static final String  TICKET_WISE_OPERATION_VALUE_STREAM_CODE="ptkd_ValueStreamCode";
    public static final String  TICKET_WISE_OPERATION_FTYPE_CODE="ptkd_Ftypecode";
    public static final String  TICKET_WISE_OPERATION="ptkd_Operation";
    public static final String  TICKET_WISE_OPERATION_CREATED_BY="ptkd_CreatedBy";
    public static final String  TICKET_WISE_OPERATION_CREATED_DATE="ptkd_CreatedDate";
    public static final String  TICKET_WISE_OPERATION_APPROVED_BY="ptkd_ApprovedBy";
    public static final String  TICKET_WISE_OPERATION_APPROVE_DATE="ptkd_ApproveDate";
    public static final String  TICKET_WISE_OPERATION_RELEASE_DATE="ptkd_ReleaseDate";
    public static final String  TICKET_WISE_OPERATION_IS_RELEASE="ptkd_IsRelease";
    public static final String  TICKET_WISE_OPERATION_ACTIVE="ptkd_Active";



    /*
    * FalcoJK Tables End Here
    * */
}
