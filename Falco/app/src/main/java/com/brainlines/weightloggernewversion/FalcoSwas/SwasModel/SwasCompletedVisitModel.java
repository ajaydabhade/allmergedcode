package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

public class SwasCompletedVisitModel {
    String ServiceLogID,OEMID,CustomerID,ServiceCallID,ServiceEngineerID,PartStatus,ServiceCallStartDate,ServiceCallEndDate,ServiceCallEntryTime,ServiceCallExitTime,GatePassImagePath,Remark,CreatedBy,CreatedDate;

    public String getServiceLogID() {
        return ServiceLogID;
    }

    public void setServiceLogID(String serviceLogID) {
        ServiceLogID = serviceLogID;
    }

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getServiceCallID() {
        return ServiceCallID;
    }

    public void setServiceCallID(String serviceCallID) {
        ServiceCallID = serviceCallID;
    }

    public String getServiceEngineerID() {
        return ServiceEngineerID;
    }

    public void setServiceEngineerID(String serviceEngineerID) {
        ServiceEngineerID = serviceEngineerID;
    }

    public String getPartStatus() {
        return PartStatus;
    }

    public void setPartStatus(String partStatus) {
        PartStatus = partStatus;
    }

    public String getServiceCallStartDate() {
        return ServiceCallStartDate;
    }

    public void setServiceCallStartDate(String serviceCallStartDate) {
        ServiceCallStartDate = serviceCallStartDate;
    }

    public String getServiceCallEndDate() {
        return ServiceCallEndDate;
    }

    public void setServiceCallEndDate(String serviceCallEndDate) {
        ServiceCallEndDate = serviceCallEndDate;
    }

    public String getServiceCallEntryTime() {
        return ServiceCallEntryTime;
    }

    public void setServiceCallEntryTime(String serviceCallEntryTime) {
        ServiceCallEntryTime = serviceCallEntryTime;
    }

    public String getServiceCallExitTime() {
        return ServiceCallExitTime;
    }

    public void setServiceCallExitTime(String serviceCallExitTime) {
        ServiceCallExitTime = serviceCallExitTime;
    }

    public String getGatePassImagePath() {
        return GatePassImagePath;
    }

    public void setGatePassImagePath(String gatePassImagePath) {
        GatePassImagePath = gatePassImagePath;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }
}
