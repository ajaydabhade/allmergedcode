package com.brainlines.weightloggernewversion.Production.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.Production.Model.PausedTestListModel;
import com.brainlines.weightloggernewversion.Production.ProductionPausedBatchListener;
import com.brainlines.weightloggernewversion.Production.adapter.Paused_Test_List_Adapter;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.AppPreferences;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class PauseTestListActivity extends AppCompatActivity implements ProductionPausedBatchListener {

    CardView cv_new_batch;
    ImageButton img_btn_view_details;
    DataBaseHelper helper;
    ArrayList<PausedTestListModel> pausedTestListModels = new ArrayList<>();
    ArrayList<String> batchidarrayList = new ArrayList<>();
    ArrayList<String> grammage_values_list = new ArrayList<>();
    RecyclerView rv_paused_test_list;
    String str_batch_id;
    Paused_Test_List_Adapter adapter;
    String str_shifts,dataloggername,dataloggermacId;
    private String str_uom;
    private String dbName;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pause_test_main_screen);
        //cv_new_batch = findViewById(R.id.cv_new_batch);
        dataloggername = getIntent().getStringExtra("LoggerName");
        dataloggermacId = getIntent().getStringExtra("mac_id");
        img_btn_view_details = findViewById(R.id.img_btn_view_details);
        rv_paused_test_list = findViewById(R.id.rv_paused_test_list);
        dbName = AppPreferences.getDatabseName(this);
//        helper = new DataBaseHelper(this,dbName);
        helper = new DataBaseHelper(this);
        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));
        getProductionGrammageValues();
        getPausedtestBatchid();


        /*cv_new_batch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PauseTestListActivity.this, ProductionWeightReadingActivity.class);
                startActivity(i);
            }
        });*/

    }

    private void getPausedtestBatchid() {
        String staus = "Paused";
        Cursor p_tag = helper.getPausedTestBatchId(staus);
        while (p_tag.moveToNext()) {
            try {

                String p_tag_string = p_tag.getString(0);
                String batchid = p_tag.getString(4);
                String product = p_tag.getString(5);
                String gm_pouch = p_tag.getString(6);
                String total_weight = p_tag.getString(8);
                String no_of_pouches = p_tag.getString(14);
                String uom = p_tag.getString(7);
                String ucl = p_tag.getString(19);
                String lcl = p_tag.getString(20);
                String strkgvalue="";

                if (uom.equals("Kg"))
                {
                    strkgvalue = grammage_values_list.get(0);
                    Double dlcl = Double.parseDouble(lcl)/ Double.parseDouble(strkgvalue);
                    Double ducl = Double.parseDouble(ucl)/Double.parseDouble(strkgvalue);
                    PausedTestListModel model = new PausedTestListModel();
                    model.setGmPerPouch(gm_pouch);
                    model.setTagNumberOfPouches(no_of_pouches);
                    model.setStrUom(uom);
                    model.setProductID(product);
                    model.setTotalQTY(total_weight);
                    model.setBatchid(batchid);
                    model.setStrkgvalue(strkgvalue);
                    model.setStrlcl(String.valueOf(dlcl));
                    model.setStrucl(String.valueOf(ducl));
                    pausedTestListModels.add(model);

                }
                else if(uom.equals("gm")){
                    strkgvalue = "";
                    PausedTestListModel model = new PausedTestListModel();
                    model.setGmPerPouch(gm_pouch);
                    model.setTagNumberOfPouches(no_of_pouches);
                    model.setStrUom(uom);
                    model.setProductID(product);
                    model.setTotalQTY(total_weight);
                    model.setBatchid(batchid);
                    model.setStrkgvalue(strkgvalue);
                    model.setStrucl(ucl);
                    model.setStrlcl(lcl);
                    pausedTestListModels.add(model);

                }

            }
            catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (pausedTestListModels.size()!=0)
        {
            LinearLayoutManager manager = new LinearLayoutManager(this);
            rv_paused_test_list.setLayoutManager(manager);
            adapter = new Paused_Test_List_Adapter(this,pausedTestListModels,this);
            rv_paused_test_list.setAdapter(adapter);
        }

    }


    private void getProductionGrammageValues() {
        Cursor grammageValues = helper.getProductionGrammageValues();
        while (grammageValues.moveToNext()){
            String kgid = grammageValues.getString(0);
            String kgname = grammageValues.getString(1);
            String kgvalues = grammageValues.getString(2);

            grammage_values_list.add(kgvalues);
            Log.d("TAG", "getProductsUsedForTrial: ");
        }

    }

    @Override
    public void isTagClicked(final PausedTestListModel model) {

        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(PauseTestListActivity.this).inflate(R.layout.activity_pause_testselection_starttest, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(PauseTestListActivity.this);
        builder.setView(dialogView);
        final EditText ed_oprator = dialogView.findViewById(R.id.ed_pause_operator);
        final Spinner spin_pause_shift = dialogView.findViewById(R.id.spin_pause_shift);
        TextView txtpausebatchid = dialogView.findViewById(R.id.txtpausebatchid);
        txtpausebatchid.setText(model.getBatchid());
        ImageButton btn_pause_ok = dialogView.findViewById(R.id.btn_pause_ok);
        ArrayList<String> shiftlist = new ArrayList<>();
        str_uom = model.getStrUom();

        shiftlist.add("1");
        shiftlist.add("2");
        shiftlist.add("3");

        ArrayAdapter<String> shift_arraylist = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, shiftlist);
        shift_arraylist.setDropDownViewResource(R.layout.item_spinner_latyout);
        spin_pause_shift.setAdapter(shift_arraylist);

        spin_pause_shift.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                str_shifts = spin_pause_shift.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        btn_pause_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());
                Intent i = new Intent(PauseTestListActivity.this,PausedBatchWeightReadingActivity.class);
                i.putExtra("gmperpouch",model.getGmPerPouch());
                i.putExtra("total_qty",model.getTotalQTY());
                i.putExtra("negative_tolerance",model.getTagPositiveTolerance());
                i.putExtra("positive_tolerance",model.getTagPositiveTolerance());
                i.putExtra("no_of_pouches",model.getTagNumberOfPouches());
                i.putExtra("batch_id",model.getBatchid());
                i.putExtra("select_date",currentDateandTime);
                i.putExtra("product",model.getProductID());
                i.putExtra("operator",ed_oprator.getText().toString());
                i.putExtra("shift",str_shifts);
                i.putExtra("uom",str_uom);
                i.putExtra("intkgvalue",model.getStrkgvalue());
                i.putExtra("dlcl",model.getStrlcl());
                i.putExtra("ducl",model.getStrucl());
                i.putExtra("dataloggername",dataloggername);
                i.putExtra("dataloggerMacid",dataloggermacId);
                startActivity(i);


            }
        });


    }
}



