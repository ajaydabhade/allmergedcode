package com.brainlines.weightloggernewversion.FalcoJK.MainAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.EquipTypeModel;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class EquipTypeSpinAdapter extends BaseAdapter {
    Context context;
    ArrayList<EquipTypeModel> arrayList;


    public EquipTypeSpinAdapter(Context context, ArrayList<EquipTypeModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    class ViewHolder {

        TextView Heading;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
       ViewHolder viewHolder;
        /* typeface = Typeface.createFromAsset(context.getAssets(), Constants.FONT);*/

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = ((LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.adapter_spinner, viewGroup, false);
            viewHolder.Heading = convertView.findViewById(R.id.tv_spinner_head);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.Heading.setText(arrayList.get(position).getEq_type());


        return convertView;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
