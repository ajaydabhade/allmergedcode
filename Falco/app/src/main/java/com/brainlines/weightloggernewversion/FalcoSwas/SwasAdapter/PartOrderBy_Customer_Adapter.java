package com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.PartOrderByCustomerModel;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class PartOrderBy_Customer_Adapter extends RecyclerView.Adapter<PartOrderBy_Customer_Adapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<PartOrderByCustomerModel> partOrderByCustomerModelArrayList;
    View.OnClickListener mClickListener;
    Context context;

    public PartOrderBy_Customer_Adapter(Context ctx, ArrayList<PartOrderByCustomerModel> partOrderByCustomerModelArrayList) {
        inflater = LayoutInflater.from(ctx);
        this.partOrderByCustomerModelArrayList = partOrderByCustomerModelArrayList;
        this.context = ctx;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_part_order_by_customer, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final PartOrderByCustomerModel model = partOrderByCustomerModelArrayList.get(position);
        if (model!=null) {

            holder.txt_item_name.setText(model.getITEMID());
            holder.txt_item_id.setText(model.getNAME());
            holder.txt_item_price.setText(model.getFinalPrice());
            holder.txt_item_qty.setText(model.getQuantity());

           /* holder.txt_item_qty.setEnabled(false);
            holder.txt_item_id.setEnabled(false);
            holder.txt_item_name.setEnabled(false);
            holder.txt_item_price.setEnabled(false);
            holder.itemView.setEnabled(false);
*/
        }
    }

    @Override
    public int getItemCount() {
        return partOrderByCustomerModelArrayList.size();
    }

    class MyViewHolder  extends RecyclerView.ViewHolder{
        TextView txt_item_name,txt_item_id,txt_item_qty,txt_item_price;


        public MyViewHolder(View itemView) {
            super(itemView);
            txt_item_name = itemView.findViewById(R.id.txt_item_name);
            txt_item_id = itemView.findViewById(R.id.txt_item_id);
            txt_item_qty = itemView.findViewById(R.id.txt_item_qty);
            txt_item_price = itemView.findViewById(R.id.txt_item_price);



        }
    }
}
