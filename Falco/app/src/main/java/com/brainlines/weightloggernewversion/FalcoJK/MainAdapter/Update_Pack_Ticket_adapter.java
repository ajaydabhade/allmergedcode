package com.brainlines.weightloggernewversion.FalcoJK.MainAdapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.PacketTicketTrackingModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.StartTicketPackModel;
import com.brainlines.weightloggernewversion.FalcoJK.Listeners.UpdatePackingTicketeListener;
import com.brainlines.weightloggernewversion.FalcoJK.UrlConstants.JK_URL_Constants;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class Update_Pack_Ticket_adapter extends RecyclerView.Adapter<Update_Pack_Ticket_adapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<StartTicketPackModel> ticket_list;
    View.OnClickListener mClickListener;
    Context context;
    String str_tk_no,str_coml_qty,pending_qty,str_tk_id;
    UpdatePackTrackingTicketRvAdapter updatePackTrackingTicketRvAdapter;
    RecyclerView recyclerView;
    UpdatePackingTicketeListener updatePackingTicketeListener;
    String str_plant,str_date,str_total_qty,str_qty,str_pendingqty,strcreatedby,str_approved_by,str_approve,adddaypendingqty;

    public Update_Pack_Ticket_adapter(Context ctx, ArrayList<StartTicketPackModel> ticket_list,UpdatePackingTicketeListener updatePackingTicketeListener){
        inflater = LayoutInflater.from(ctx);
        this.ticket_list = ticket_list;
       this.updatePackingTicketeListener=updatePackingTicketeListener;
        this.context=ctx;
        this.str_approved_by=str_approved_by;
        this.strcreatedby=strcreatedby;
        this.str_approve=str_approve;
    }

    @Override
    public int getItemCount() {
        return ticket_list.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_update_packing_ticket_data, parent, false);
       MyViewHolder holder = new MyViewHolder(view);

       holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onClick(view);
            }
        });
        return holder;
    }
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int i) {
        final StartTicketPackModel model = ticket_list.get(i);
        holder.txt_date.setText(ticket_list.get(i).getDay());
        holder.txt_pending_qty.setText(ticket_list.get(i).getTk_Pending());

        holder.txt_compl_qty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                str_coml_qty = s.toString();
                if (s != null) {

                    if (!str_coml_qty.equals("")) {
                        ticket_list.get(i).setStr_compl_qty(str_coml_qty);
                        ticket_list.get(i).setStr_txt_pending_qty(holder.txt_pending_qty.getText().toString());
                    }
                }
            }
        });

        holder.img_btn_action_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                updatePackingTicketeListener.imgactionSubmitIsClicked(model);


            }
        });




    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView txt_date,txt_compl_qty,txt_pending_qty;
        ImageButton img_btn_action_submit;


        public MyViewHolder(View itemView) {
            super(itemView);
            txt_date= itemView.findViewById(R.id.txtdate);
            txt_compl_qty= itemView.findViewById(R.id.txtcompletedqty1);
            txt_pending_qty= itemView.findViewById(R.id.txtpendingqty1);
            img_btn_action_submit=itemView.findViewById(R.id.img_action_pack_submit);


        }
    }
    public class GetTicketDetailsCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(context);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String url = null;
            url= JK_URL_Constants.MAIN_JK_URL+JK_URL_Constants.GET_TICKET_DETAILS + str_tk_id;

            response= URL_Constants.makeHttpPutRequest(url);
            Log.d(URL_Constants.TAG, "Whole data = " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray array=jsonObject.getJSONArray("data");
                    if (array.length()==0){

                        new SavePackingTicketTrackingCall().execute();
                    }
                    else {
                        new UpdatePackingTicketTrackingCall().execute();


                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

        }
    }

    public class UpdatePackingTicketTrackingCall  extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(context);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String str_user_id="capacity@decintell.com";
            String url1= JK_URL_Constants.MAIN_JK_URL;
            String url2=url1 + JK_URL_Constants.INSERT_PACKING_UPDATE_TICKET + str_tk_id +"&tt_CompletedQty=" + str_coml_qty + "&tt_PendingQty=" + str_pendingqty + "&UserId=" + str_user_id
                    + "&TicketIDNo=" + str_tk_no;
            response= URL_Constants.makeHttpPostRequest(url2);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try {
                    JSONObject object=new JSONObject(response);
                    String status=object.getString("data");


                    if (status.equals("1"))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);

                        //builder.setTitle("Confirm");
                        builder.setMessage("You successfully update the record");

                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {

                                // Do nothing, but close the dialog
                                dialog.dismiss();
                                new DisplayGetTicketDetailsCall().execute();
                            }
                        });




                        Toast.makeText(context,"You successfully update the record",Toast.LENGTH_SHORT).show();

                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }

            }
        }
    }



    public class DisplayGetTicketDetailsCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(context);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String url = null;
            url= JK_URL_Constants.MAIN_JK_URL+JK_URL_Constants.GET_TICKET_DETAILS + str_tk_id;

            response= URL_Constants.makeHttpPutRequest(url);
            Log.d(URL_Constants.TAG, "Whole data = " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray array=jsonObject.getJSONArray("data");

                    if (array.length()==0)
                    {
                        Toast toast = Toast.makeText(context,
                                "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                    else
                        {
                        ArrayList<PacketTicketTrackingModel> ticketmodel= new ArrayList<>();
                        for (int i=0;i<array.length();i++) {
                            JSONObject obj = new JSONObject(array.get(i).toString());
                            PacketTicketTrackingModel model = new PacketTicketTrackingModel();
                            model.setTt_id(obj.getString("tt_id"));
                            model.setTt_TicketID(obj.getString("tt_TicketID"));
                            model.setTt_TicketNo(obj.getString("tt_TicketNo"));
                            model.setTt_Plant(obj.getString("tt_Plant"));
                            model.setTt_Date(obj.getString("tt_Date"));
                            model.setTt_TotalQuantity(obj.getString("tt_TotalQuantity"));
                            model.setTt_Qty(obj.getString("tt_Qty"));
                            model.setTt_PendingQty(obj.getString("tt_PendingQty"));
                            model.setTt_CreatedDate(obj.getString("tt_CreatedDate"));
                            model.setTt_CreatedBy(obj.getString("tt_CreatedBy"));
                            model.setTt_ApprovedDate(obj.getString("tt_ApprovedDate"));
                            model.setTt_ApprovedBy(obj.getString("tt_ApprovedBy"));
                            model.setTt_Active(obj.getString("tt_Active"));
                            model.setTt_Approve(obj.getString("tt_Approve"));
                            model.setFlag1(obj.getString("flag1"));
                            model.setTk_SKU(obj.getString("tk_SKU"));
                            model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                            model.setTk_End_Date(obj.getString("tk_End_Date"));
                            model.setActualstartdate(obj.getString("actualstartdate"));
                            model.setActualenddate(obj.getString("actualenddate"));
                            model.setToday(obj.getString("today"));
                            ticketmodel.add(model);

                        }
                        /*UpdatePackTrackingTicketRvAdapter updatePackTrackingTicketRvAdapter=new UpdatePackTrackingTicketRvAdapter(context,ticketmodel);
                        recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL, false));
                        recyclerView.setAdapter(updatePackTrackingTicketRvAdapter);*/
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

        }
    }

    public class SavePackingTicketTrackingCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(context);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String str_user_id="capacity@decintell.com";
            str_pendingqty=URL_Constants.substraction(str_coml_qty,pending_qty);
            String url1= JK_URL_Constants.MAIN_JK_URL;
            String url2=url1 + JK_URL_Constants.FIRST_TIME_SAVE_PACKUNPACKTK + str_tk_id + "&tt_TicketNo=" + str_tk_no + "&tt_Plant=" + "CHNM" + "&tt_Date=" + str_date
                    + "&tt_TotalQuantity=" + str_total_qty + "&tt_Qty=" + str_coml_qty + "&tt_PendingQty=" + str_pendingqty + "&tt_CreatedBy="
                    + str_user_id + "&tt_ApprovedBy=" + str_user_id + "&tt_Approve=" + "N";

            response= URL_Constants.makeHttpPostRequest(url2);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try {
                    JSONObject object=new JSONObject(response);
                    String status=object.getString("data");
                    String abc=object.getString("status");
                    if (abc.equals("true"))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);

                        //builder.setTitle("Confirm");
                        builder.setMessage("You successfully insert the record");

                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {

                                // Do nothing, but close the dialog
                                dialog.dismiss();
                                new DisplayGetTicketDetailsCall().execute();
                            }
                        });

                    }

                    /*AlertDialog.Builder builder = new AlertDialog.Builder(context);

                    //builder.setTitle("Confirm");
                    builder.setMessage("You successfully update the record");

                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {

                            // Do nothing, but close the dialog
                            dialog.dismiss();
                            new DisplayGetTicketDetailsCall().execute();
                        }
                    });*/

                    if (status.equals("1")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);

                        //builder.setTitle("Confirm");
                        builder.setMessage("You successfully update the record");

                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {

                                // Do nothing, but close the dialog
                                dialog.dismiss();
                                new DisplayGetTicketDetailsCall().execute();
                            }
                        });
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }

            }
        }
    }
}
