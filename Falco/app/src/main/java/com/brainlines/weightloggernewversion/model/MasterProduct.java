package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MasterProduct implements Parcelable {
    String productId;
    int oemId;
    String productTypeId;
    String product;
    boolean isActive;
    boolean IsDeleted;
    String createdDate;
    String modifiedDate;
    String createdBy;
    String modifiedBy;

    public MasterProduct(String productId, int oemId, String productTypeId, String product, boolean isActive, boolean isDeleted, String createdDate, String modifiedDate, String createdBy, String modifiedBy) {
        this.productId = productId;
        this.oemId = oemId;
        this.productTypeId = productTypeId;
        this.product = product;
        this.isActive = isActive;
        IsDeleted = isDeleted;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.createdBy = createdBy;
        this.modifiedBy = modifiedBy;
    }

    protected MasterProduct(Parcel in) {
        productId = in.readString();
        oemId = in.readInt();
        productTypeId = in.readString();
        product = in.readString();
        isActive = in.readByte() != 0;
        IsDeleted = in.readByte() != 0;
        createdDate = in.readString();
        modifiedDate = in.readString();
        createdBy = in.readString();
        modifiedBy = in.readString();
    }

    public static final Creator<MasterProduct> CREATOR = new Creator<MasterProduct>() {
        @Override
        public MasterProduct createFromParcel(Parcel in) {
            return new MasterProduct(in);
        }

        @Override
        public MasterProduct[] newArray(int size) {
            return new MasterProduct[size];
        }
    };

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getOemId() {
        return oemId;
    }

    public void setOemId(int oemId) {
        this.oemId = oemId;
    }

    public String getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(String productTypeId) {
        this.productTypeId = productTypeId;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isDeleted() {
        return IsDeleted;
    }

    public void setDeleted(boolean deleted) {
        IsDeleted = deleted;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(productId);
        dest.writeInt(oemId);
        dest.writeString(productTypeId);
        dest.writeString(product);
        dest.writeByte((byte) (isActive ? 1 : 0));
        dest.writeByte((byte) (IsDeleted ? 1 : 0));
        dest.writeString(createdDate);
        dest.writeString(modifiedDate);
        dest.writeString(createdBy);
        dest.writeString(modifiedBy);
    }
}

