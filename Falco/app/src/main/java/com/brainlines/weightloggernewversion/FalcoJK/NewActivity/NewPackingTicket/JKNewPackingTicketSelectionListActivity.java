package com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewPackingTicket;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.SpinnerAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewPackingTicket.Listener.NewPackingTicketListener;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewPackingTicket.Model.NewPackingTicketModel;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewPackingTicket.adapter.NewPackingTicketAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.UrlConstants.JK_URL_Constants;
import com.brainlines.weightloggernewversion.FalcoJK.activity.FalcoJKHomeActivity;
import com.brainlines.weightloggernewversion.FalcoJK.activity.JKUpdateTicketActivity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

@SuppressLint("Registered")
public class JKNewPackingTicketSelectionListActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, NewPackingTicketListener {
    DataBaseHelper helper;
    SQLiteDatabase db;
    RecyclerView rv_packing_ticket_list;
    NewPackingTicketAdapter adapter;
    Spinner spin_ticket_type,spin_pack_release,spin_ticket;
    String ticket_type,ticket_release,ticket_active;
    ImageView img_back,img_user_info,nav_view_img;
    DrawerLayout drawer;
    String user_email,user_role,user_token;
    SwipeRefreshLayout swipeto_refresh;
    TextView profile_user,profile_role;
    final ArrayList<String> arrayrelese_type = new ArrayList<>();
    final ArrayList<String> arrayticket_type = new ArrayList<>();
    final ArrayList<String> arrayactiveticket_type = new ArrayList<>();
    String str_ticket_type;
    ArrayList<NewPackingTicketModel> packingTicketModelArrayList= new ArrayList<>();
    String str_end_Date,str_tk_id,str_approved_by,str_start_date,str_release_date;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_packing_ticket_selection_navigation);
        helper = new DataBaseHelper(JKNewPackingTicketSelectionListActivity.this);
        db = helper.getWritableDatabase();
        initUi();

        spin_ticket_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i!=0)
                {
                    ticket_type=arrayticket_type.get(i).toString();
                    packingTicketModelArrayList.clear();
                    Cursor tickettypecursor = helper.getTickets(ticket_type);
                    while (tickettypecursor.moveToNext()) {
                        NewPackingTicketModel model=new NewPackingTicketModel();
                        model.setID(tickettypecursor.getString(0));
                        model.setTk_Id(tickettypecursor.getString(1));
                        model.setTk_TicketNo(tickettypecursor.getString(2));
                        model.setTk_type(tickettypecursor.getString(3));
                        model.setTk_SONO(tickettypecursor.getString(4));
                        model.setTk_Plant(tickettypecursor.getString(5));
                        model.setTk_SKU(tickettypecursor.getString(6));
                        model.setTk_Prod_SKU(tickettypecursor.getString(7));
                        model.setTk_Release_Date(tickettypecursor.getString(8));
                        model.setTk_Start_Date(tickettypecursor.getString(9));
                        model.setTk_End_Date(tickettypecursor.getString(10));
                        model.setTk_Quantity(tickettypecursor.getString(11));
                        model.setTk_Unpaking_SKU(tickettypecursor.getString(12));
                        model.setTk_Active(tickettypecursor.getString(13));
                        model.setTk_Unpacking(tickettypecursor.getString(14));
                        model.setTk_Release(tickettypecursor.getString(15));
                        model.setMonthStart(tickettypecursor.getString(16));
                        model.setMonthEnd(tickettypecursor.getString(17));
                        model.setTotalqty(tickettypecursor.getString(18));
                        model.setTk_Created_date(tickettypecursor.getString(19));
                        model.setTk_Created_by(tickettypecursor.getString(20));
                        model.setTk_Approved_date(tickettypecursor.getString(21));
                        model.setTk_Approved_by(tickettypecursor.getString(22));
                        model.setTk_Actual_start_date(tickettypecursor.getString(21));
                        model.setTk_Actual_end_date(tickettypecursor.getString(22));
                        packingTicketModelArrayList.add(model);
                    }

                    adapter=new NewPackingTicketAdapter(JKNewPackingTicketSelectionListActivity.this,packingTicketModelArrayList,rv_packing_ticket_list,adapter,JKNewPackingTicketSelectionListActivity.this);
                    rv_packing_ticket_list.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL, false));
                    rv_packing_ticket_list.setAdapter(adapter);

                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spin_pack_release.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i!=0)
                {
                    ticket_release=arrayrelese_type.get(i).toString();
                    ticket_type=arrayticket_type.get(i).toString();

                    if (ticket_release.equals("ReleasedTicket"))
                    {
                        ticket_release="true";
                    }
                    else
                    {
                        ticket_release="false";
                    }
                    packingTicketModelArrayList.clear();
                    Cursor tickettypecursor = helper.getReleaseUnreleaseTickets(ticket_type,ticket_release);
                    while (tickettypecursor.moveToNext()) {
                        NewPackingTicketModel model=new NewPackingTicketModel();
                        model.setID(tickettypecursor.getString(0));
                        model.setTk_Id(tickettypecursor.getString(1));
                        model.setTk_TicketNo(tickettypecursor.getString(2));
                        model.setTk_type(tickettypecursor.getString(3));
                        model.setTk_SONO(tickettypecursor.getString(4));
                        model.setTk_Plant(tickettypecursor.getString(5));
                        model.setTk_SKU(tickettypecursor.getString(6));
                        model.setTk_Prod_SKU(tickettypecursor.getString(7));
                        model.setTk_Release_Date(tickettypecursor.getString(8));
                        model.setTk_Start_Date(tickettypecursor.getString(9));
                        model.setTk_End_Date(tickettypecursor.getString(10));
                        model.setTk_Quantity(tickettypecursor.getString(11));
                        model.setTk_Unpaking_SKU(tickettypecursor.getString(12));
                        model.setTk_Active(tickettypecursor.getString(13));
                        model.setTk_Unpacking(tickettypecursor.getString(14));
                        model.setTk_Release(tickettypecursor.getString(15));
                        model.setMonthStart(tickettypecursor.getString(16));
                        model.setMonthEnd(tickettypecursor.getString(17));
                        model.setTotalqty(tickettypecursor.getString(18));
                        model.setTk_Created_date(tickettypecursor.getString(19));
                        model.setTk_Created_by(tickettypecursor.getString(20));
                        model.setTk_Approved_date(tickettypecursor.getString(21));
                        model.setTk_Approved_by(tickettypecursor.getString(22));
                        model.setTk_Actual_start_date(tickettypecursor.getString(21));
                        model.setTk_Actual_end_date(tickettypecursor.getString(22));
                        packingTicketModelArrayList.add(model);
                    }

                    adapter=new NewPackingTicketAdapter(JKNewPackingTicketSelectionListActivity.this,packingTicketModelArrayList,rv_packing_ticket_list,adapter,JKNewPackingTicketSelectionListActivity.this);
                    rv_packing_ticket_list.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL, false));
                    rv_packing_ticket_list.setAdapter(adapter);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        spin_ticket.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i!=0)
                {
                    ticket_active=arrayactiveticket_type.get(i).toString();

                    if (ticket_active.equals("ActiveTicket"))
                    {
                        ticket_active = "true";
                    }
                    else
                    {
                        ticket_active = "false";

                    }
                    packingTicketModelArrayList.clear();
                    Cursor tickettypecursor = helper.getActiveInActiveTickets(ticket_active);
                    while (tickettypecursor.moveToNext()) {
                        NewPackingTicketModel model=new NewPackingTicketModel();
                        model.setID(tickettypecursor.getString(0));
                        model.setTk_Id(tickettypecursor.getString(1));
                        model.setTk_TicketNo(tickettypecursor.getString(2));
                        model.setTk_type(tickettypecursor.getString(3));
                        model.setTk_SONO(tickettypecursor.getString(4));
                        model.setTk_Plant(tickettypecursor.getString(5));
                        model.setTk_SKU(tickettypecursor.getString(6));
                        model.setTk_Prod_SKU(tickettypecursor.getString(7));
                        model.setTk_Release_Date(tickettypecursor.getString(8));
                        model.setTk_Start_Date(tickettypecursor.getString(9));
                        model.setTk_End_Date(tickettypecursor.getString(10));
                        model.setTk_Quantity(tickettypecursor.getString(11));
                        model.setTk_Unpaking_SKU(tickettypecursor.getString(12));
                        model.setTk_Active(tickettypecursor.getString(13));
                        model.setTk_Unpacking(tickettypecursor.getString(14));
                        model.setTk_Release(tickettypecursor.getString(15));
                        model.setMonthStart(tickettypecursor.getString(16));
                        model.setMonthEnd(tickettypecursor.getString(17));
                        model.setTotalqty(tickettypecursor.getString(18));
                        model.setTk_Created_date(tickettypecursor.getString(19));
                        model.setTk_Created_by(tickettypecursor.getString(20));
                        model.setTk_Approved_date(tickettypecursor.getString(21));
                        model.setTk_Approved_by(tickettypecursor.getString(22));
                        model.setTk_Actual_start_date(tickettypecursor.getString(21));
                        model.setTk_Actual_end_date(tickettypecursor.getString(22));
                        packingTicketModelArrayList.add(model);
                    }

                    adapter=new NewPackingTicketAdapter(JKNewPackingTicketSelectionListActivity.this,packingTicketModelArrayList,rv_packing_ticket_list,adapter,JKNewPackingTicketSelectionListActivity.this);
                    rv_packing_ticket_list.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL, false));
                    rv_packing_ticket_list.setAdapter(adapter);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        swipeto_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeto_refresh.setRefreshing(false);
            }
        });
    }

    public void initUi()
    {
        rv_packing_ticket_list = findViewById(R.id.rv_packing_ticket_list);
        swipeto_refresh = findViewById(R.id.swipeto_refresh);
        spin_pack_release = findViewById(R.id.spin_pack_release);
        spin_ticket = findViewById(R.id.spin_ticket);
        spin_ticket_type = findViewById(R.id.spin_pack_ticket);

        arrayrelese_type.add("Select Released");
        arrayrelese_type.add("ReleasedTicket");
        arrayrelese_type.add("UnreleasedTicket");
        SpinnerAdapter arrayAdapter = new SpinnerAdapter(this,arrayrelese_type);
        spin_pack_release.setAdapter(arrayAdapter);

        arrayticket_type.add("Select Ticket Type");
        arrayticket_type.add("PackingTicket");
        arrayticket_type.add("RepackingTicket");
        final SpinnerAdapter arrayAdapter1 = new SpinnerAdapter(this,arrayticket_type);
        spin_ticket_type.setAdapter(arrayAdapter1);

        arrayactiveticket_type.add("Select Ticket");
        arrayactiveticket_type.add("ActiveTicket");
        arrayactiveticket_type.add("ClosedTicket");
        SpinnerAdapter arrayAdapter2 = new SpinnerAdapter(this,arrayactiveticket_type);
        spin_ticket.setAdapter(arrayAdapter2);

        img_back = findViewById(R.id.img_back);
        img_user_info = findViewById(R.id.img_userinfo);
        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        drawer =  findViewById(R.id.drawer_layout);
        nav_view_img = findViewById(R.id.nav_view_img);
        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });
        profile_user = headerView.findViewById(R.id.profile_user);
        profile_role = headerView.findViewById(R.id.profile_role);

        profile_user.setText(user_email);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_swas).setVisible(false);




    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id==R.id.nav_planning) {
            Intent i=new Intent(JKNewPackingTicketSelectionListActivity.this, FalcoJKHomeActivity.class);
            startActivity(i);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(JKNewPackingTicketSelectionListActivity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);
        setData();
        // Set content width and height
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(JKNewPackingTicketSelectionListActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }

    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(JKNewPackingTicketSelectionListActivity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);
        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();
                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout()
    {
        finish();
        Toast toast= Toast.makeText(JKNewPackingTicketSelectionListActivity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(JKNewPackingTicketSelectionListActivity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);

        //Preferences.Logout();
    }

    @Override
    public void isupdateClicked(NewPackingTicketModel model) {
        if (model!=null)
        {
            Intent i = new Intent(JKNewPackingTicketSelectionListActivity.this, JKUpdateTicketActivity.class);
            // i.putExtra("model",model);
            Bundle bun = new Bundle();
            bun.putString("tk_id",model.getTk_Id());
            bun.putString("tk_no",model.getTk_TicketNo());
            bun.putString("tk_start_date", URL_Constants.DateConversion(model.getTk_Start_Date()));
            bun.putString("tk_end_date",URL_Constants.DateConversion(model.getTk_End_Date()));
            bun.putString("tk_sku_13",model.getTk_SKU());
            bun.putString("tk_plant",model.getTk_Plant());
            bun.putString("tk_quantity",model.getTk_Quantity());
            bun.putString("tk_release_date",URL_Constants.DateConversion(model.getTk_Release_Date()));
            bun.putString("tk_pending_qty",model.getTk_Quantity());
            i.putExtras(bun);
            startActivity(i);
        }

    }

    @Override
    public void isreleaseClicked(NewPackingTicketModel model, TextView txtreleasedate, ImageButton btnrelease, ImageButton btnupdate, ImageButton btnview, ImageView imagerelease) {
        if (model!=null)
        {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
            String str_tk_release_date = sdf.format(c.getTime());
            String[] string =str_tk_release_date.split(" ");
            String main_string_release_date=string[0];
            txtreleasedate.setText(main_string_release_date);
            btnrelease.setVisibility(View.GONE);
            btnupdate.setEnabled(true);
            btnview.setEnabled(true);
            imagerelease.setVisibility(View.GONE);

            str_start_date  = model.getTk_Start_Date();
            str_end_Date = model.getTk_End_Date();
            str_release_date = main_string_release_date;
            String str_created_by="capacity@decintell.com";
            str_approved_by="capacity@decintell.com";
            str_tk_id = model.getTk_Id();



            helper.ReleaseDatePackingDatabase(model.getTk_TicketNo(),main_string_release_date,"Releasedate");
            new ReleasePackingTicketCall().execute();

        }
    }

    @Override
    public void isviewClicked(NewPackingTicketModel model) {
        if (model!=null)
        {
            Intent i = new Intent(JKNewPackingTicketSelectionListActivity.this,JKNewViewPackingTicketActivity.class);
            //i.putExtra("model",model);
            Bundle bun = new Bundle();
            bun.putString("tk_id",model.getTk_Id());
            bun.putString("tk_no",model.getTk_TicketNo());
            bun.putString("tk_start_date", URL_Constants.DateConversion(model.getTk_Start_Date()));
            bun.putString("tk_end_date",URL_Constants.DateConversion(model.getTk_End_Date()));
            bun.putString("tk_sku_13",model.getTk_SKU());
            bun.putString("tk_plant",model.getTk_Plant());
            bun.putString("tk_quantity",model.getTk_Quantity());
            bun.putString("tk_release_date",URL_Constants.DateConversion(model.getTk_Release_Date()));
            bun.putString("tk_pending_qty",model.getTk_Quantity());
            i.putExtras(bun);
            startActivity(i);
        }

    }

    public class ReleasePackingTicketCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JKNewPackingTicketSelectionListActivity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String str_created_by="capacity@decintell.com";
            String url= JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.RELEASE_PACK_TICKET + str_created_by + "&tk_Start_Date=" + str_start_date
                    + "&tk_End_Date=" + str_end_Date + "&tk_Id=" + str_tk_id + "&tk_ApprovedBy=" + str_approved_by + "&tk_Release_Date=" + str_release_date;
            response= URL_Constants.makeHttpPostRequest(url);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            progressDialog.dismiss();
            if (!response.equals("Error"))
            { try
            {
                JSONObject object=new JSONObject(response);
                String data=object.getString("data");
                String status=object.getString("status");
                String message=object.getString("message");

                if (message.equals("Success"))
                {
                    Toast.makeText(JKNewPackingTicketSelectionListActivity.this, "Successfully Released", Toast.LENGTH_SHORT).show();
                    //new GetTicketsCall().execute();

                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            }

        }
    }
}
