package com.brainlines.weightloggernewversion.activity.history;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class HistoryWeigthtCalculationsAdapter extends RecyclerView.Adapter<HistoryWeigthtCalculationsAdapter.MyViewHolder> {

    Context context;
    ArrayList<String> weightReadingsList = new ArrayList<>();

    public HistoryWeigthtCalculationsAdapter(Context context, ArrayList<String> weightReadingsList) {
        this.context = context;
        this.weightReadingsList = weightReadingsList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_display_calculation_show_readings_value,parent,false);
        return new HistoryWeigthtCalculationsAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        String readingValue = weightReadingsList.get(position);
        if (!readingValue.equals("")){
            holder.txt_weight_reading.setText(readingValue);
        }
    }

    @Override
    public int getItemCount() {
        return weightReadingsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_weight_reading;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_weight_reading = (itemView).findViewById(R.id.txt_weight_reading);
        }
    }
}
