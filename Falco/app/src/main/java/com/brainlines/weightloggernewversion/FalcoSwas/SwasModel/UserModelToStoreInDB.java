package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

import android.os.Parcel;
import android.os.Parcelable;

public class UserModelToStoreInDB implements Parcelable {

    String access_token;
    String token_type;
    String OEMID;
    String UserEmailId;
    String RoleName;

    public UserModelToStoreInDB(String access_token, String token_type, String OEMID, String userEmailId, String roleName) {
        this.access_token = access_token;
        this.token_type = token_type;
        this.OEMID = OEMID;
        UserEmailId = userEmailId;
        RoleName = roleName;
    }

    protected UserModelToStoreInDB(Parcel in) {
        access_token = in.readString();
        token_type = in.readString();
        OEMID = in.readString();
        UserEmailId = in.readString();
        RoleName = in.readString();
    }

    public static final Creator<UserModelToStoreInDB> CREATOR = new Creator<UserModelToStoreInDB>() {
        @Override
        public UserModelToStoreInDB createFromParcel(Parcel in) {
            return new UserModelToStoreInDB(in);
        }

        @Override
        public UserModelToStoreInDB[] newArray(int size) {
            return new UserModelToStoreInDB[size];
        }
    };

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getUserEmailId() {
        return UserEmailId;
    }

    public void setUserEmailId(String userEmailId) {
        UserEmailId = userEmailId;
    }

    public String getRoleName() {
        return RoleName;
    }

    public void setRoleName(String roleName) {
        RoleName = roleName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(access_token);
        dest.writeString(token_type);
        dest.writeString(OEMID);
        dest.writeString(UserEmailId);
        dest.writeString(RoleName);
    }
}
