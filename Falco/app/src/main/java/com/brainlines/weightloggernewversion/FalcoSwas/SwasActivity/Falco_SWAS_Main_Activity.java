package com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.FalcoSwas.AppPreferenceManager;
import com.brainlines.weightloggernewversion.FalcoJK.activity.FalcoJKHomeActivity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.Checklist_Model;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.DocumentModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.ExpenseHeadMasterModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.FaultyCharactristics_Model;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.FeedBackModelForSyncing;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.FeedbackModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.GetChecklistOfServiceCallModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.GetDetailsOfSingleServiceCallModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.GetPartOrderModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.PartOrderByCustomerModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompetitorParameterModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompletedCompetitorModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompletedMachineModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompletedTravelModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompletedVisitModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasIdDetailsModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompletedCallMachineParameterModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasServiceCallIdModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.AppPreferences;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.brainlines.weightloggernewversion.utils.GlobalClass;
import com.google.android.material.navigation.NavigationView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants.convertStreamToString;

public class Falco_SWAS_Main_Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener  {

    private static final String TAG = "Falco_Home_Main_Activit";
    ImageView img_user_info,nav_view_img,img_back;
    String user_email,user_token,user_role,oemid;
    DrawerLayout drawer;
    TextView profile_user,profile_role;
    ImageView imageView2;
    Button btn_sync;
    DataBaseHelper helper;
    private ArrayList<String> listOfServiceCallIds = new ArrayList<>();
    private ArrayList<GetDetailsOfSingleServiceCallModel> detailsOfSingleServiceCallModelArrayList = new ArrayList<>();
    private ArrayList<GetChecklistOfServiceCallModel> checklistOfServiceCallModels = new ArrayList<>();
    private ArrayList<GetPartOrderModel> partOrderModels = new ArrayList<>();
    private ArrayList<String> getCompParamsList = new ArrayList<>();
    private ArrayList<String> getExpenseList = new ArrayList<>();
    private ArrayList<String> getFeedBackList = new ArrayList<>();
    private ArrayList<String> getservicecallvisitlogList = new ArrayList<>();
    private ArrayList<String> getservicecallmachineparametersList = new ArrayList<>();
    private ArrayList<DocumentModel> documentModelArrayList = new ArrayList<>();
    private ArrayList<FeedBackModelForSyncing> feedBackModelForSyncingArrayList = new ArrayList<>();
    private ProgressDialog progressDialog;
    private String oem_id;

    private String dbName;
    private SQLiteDatabase db;
    private GlobalClass globalClass;
    private com.brainlines.weightloggernewversion.model.UserModel model;
    private String userEmail;
    private String service_eng_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_main_home_navigation);
        
//        MenuModel model = getIntent().getParcelableExtra("MenuModel");
        model = getIntent().getParcelableExtra("UserDataModel");
        userEmail = AppPreferences.getUser(this);
        
        dbName = GlobalClass.db_global_name;
        if (model != null){
            helper = new DataBaseHelper(this);
            db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));
            //SQLiteDatabase db = helper.getWritableDatabase();
//            ContentValues cv=new ContentValues();
//            cv.put("UserToken",userModelToStoreInDB.getAccess_token());
//            cv.put("UserTokenType",userModelToStoreInDB.getToken_type());
//            cv.put("UserEmailID",userModelToStoreInDB.getUserEmailId());
//            cv.put("UserOEMID",userModelToStoreInDB.getOEMID());
//            cv.put("UserRoleName",userModelToStoreInDB.getRoleName());
//            long d=db.insert(Constants.TABLE_USER_DETAILS,null,cv);
//            Log.d(TAG, "onCreate: ");
        }else {
            helper = new DataBaseHelper(this);
            db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));
        }
        globalClass = new GlobalClass(this,helper);

        int c = helper.getTableCount();
        if (c == 0){
            Log.d(TAG, "onCreate: Table Count -> " + c);
            helper.createTableForSWAS();
            new TempSwasLoginCall().execute();
        }else {
            Log.d(TAG, "onCreate: Table Count -> " + c );

        }
        setData();
        img_back=(ImageView)findViewById(R.id.img_back);
        img_back.setVisibility(View.INVISIBLE);
        img_user_info=(ImageView)findViewById(R.id.img_userinfo);
        btn_sync=findViewById(R.id.btn_sync);
        img_user_info.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img=(ImageView)findViewById(R.id.nav_view_img);
        imageView2 = (ImageView)findViewById(R.id.imageView2);
        imageView2.setVisibility(View.INVISIBLE);
        imageView2.setVisibility(View.INVISIBLE);

//        Bundle bundle = getIntent().getExtras();
//        user_email = bundle.getString("email_id");
//        user_token = bundle.getString("token");
//        user_role = bundle.getString("user_role");
//        oemid = bundle.getString("oemid");

        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        final View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_planning).setVisible(false);
        CardView card_jk_planning=findViewById(R.id.card_jk_planning);
        card_jk_planning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(Falco_SWAS_Main_Activity.this, FalcoJKHomeActivity.class);
                startActivity(i);
            }
        });
        CardView card_swas=findViewById(R.id.card_swas);
        card_swas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(Falco_SWAS_Main_Activity.this, Swas_Home_Actvity.class);
                startActivity(i);
            }
        });

        //user_email = "yogesh@brainlines.in";

        if (user_email.equals("yogesh@brainlines.in"))
        {
            card_jk_planning.setVisibility(View.GONE);
        }
        else if (user_email.equals("brainlinesweb@gmail.com"))
        {
            card_jk_planning.setVisibility(View.GONE);

        }
        else if (user_email.equals("chiplun@decintell.com"))
        {
            card_swas.setVisibility(View.GONE);
        }

        String isSaved = AppPreferenceManager.getIsDataSaved(this);
        if (!isSaved.equals("")){

        }else {
            getAssignedCalls();
            getCompletedCalls();
            getInProcessCalls();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    Cursor getServiceCallIDAndStatusID = helper.getServiceCallIdAndStatusIDFromListOfServicesByStatus();
                    while (getServiceCallIDAndStatusID.moveToNext()){
                        String serviceCallID = getServiceCallIDAndStatusID.getString(0);
                        String statusId = getServiceCallIDAndStatusID.getString(1);
                        detailsOfSingleServiceCallModelArrayList.add(new GetDetailsOfSingleServiceCallModel(serviceCallID,statusId));
                    }
                    for (GetDetailsOfSingleServiceCallModel model : detailsOfSingleServiceCallModelArrayList){
                        String serviceCallID = model.getServiceCallID();
                        String statusID = model.getServiceCallStatusID();
                        getDetailsOFSingleServiceCall(serviceCallID,statusID);
                    }

                    Cursor getServiceCallID = helper.getServiceCallIdFromListOfServicesByStatus();
                    while (getServiceCallID.moveToNext()){
                        String serviceCallID = getServiceCallID.getString(0);
                        listOfServiceCallIds.add(serviceCallID);
                    }
                    for (String id : listOfServiceCallIds){
                        getfaultcharacteristicslist(id);
                    }

                }
            },4000);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    Cursor getchecklistofservicecall = helper.getChecklistOfServiceIdAndTypeOfServiceCallID();
                    while (getchecklistofservicecall.moveToNext()){
                        String serviceCallID = getchecklistofservicecall.getString(0);
                        String typeOfServiceCallID = getchecklistofservicecall.getString(1);
                        checklistOfServiceCallModels.add(new GetChecklistOfServiceCallModel(serviceCallID,typeOfServiceCallID));
                    }
                    for (GetChecklistOfServiceCallModel model : checklistOfServiceCallModels){
                        String serviceCallID = model.getServiceCallID();
                        String typeOfServiceCallId = model.getTypeofservicecallid();
                        getCheckListOfServiceCall(serviceCallID,typeOfServiceCallId);
                    }

                    Cursor getPartOrders = helper.getPartOrderOfServiceIdAndCutsomerID();
                    while (getPartOrders.moveToNext()){
                        String serviceCallID = getPartOrders.getString(0);
                        String customerId = getPartOrders.getString(1);
                        partOrderModels.add(new GetPartOrderModel(serviceCallID,customerId));
                    }
                    for (GetPartOrderModel model : partOrderModels){
                        String serviceCallID = model.getServiceCallID();
                        String customerId = model.getCustomerID();
                        getPartOrdersServiceCall(serviceCallID,customerId);
                    }

                }
            },8000);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    Cursor getservicecallcompetitorsparameters = helper.getCompetitorParams();
                    while (getservicecallcompetitorsparameters.moveToNext()){
                        String serviceCallID = getservicecallcompetitorsparameters.getString(0);
                        getCompParamsList.add(serviceCallID);
                    }
                    for (String s : getCompParamsList){
                        String serviceCallID = s;
                        getCompPara(serviceCallID);
                    }

                    Cursor getexpenseheadslist = helper.getXpenseHeadListParams();
                    while (getexpenseheadslist.moveToNext()){
                        String customerID = getexpenseheadslist.getString(0);
                        getExpenseList.add(customerID);
                    }
//                    for (String s : getExpenseList){
//                        String customerID = s;
//
//                    }
                    getexpenseheadsList("C000003");

                }
            },12000);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Cursor getfeedbackdetails = helper.getFeedbackDetails();
                    while (getfeedbackdetails.moveToNext()){
                        String serviceCallID = getfeedbackdetails.getString(0);
                        getFeedBackList.add(serviceCallID);
                    }
                    for (String s : getFeedBackList){
                        String serviceCallID = s;
                        getFeedBackList(serviceCallID);
                    }

                    Cursor getservicecallvisitlog = helper.getFeedbackDetails();
                    while (getservicecallvisitlog.moveToNext()){
                        String serviceCallID = getservicecallvisitlog.getString(0);
                        getservicecallvisitlogList.add(serviceCallID);
                    }
                    for (String s : getservicecallvisitlogList){
                        String serviceCallID = s;
                        getservicecallvisitLOG(serviceCallID);
                    }

                }
            },16000);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Cursor getfeedbackdetails = helper.getFeedbackDetails();
                    while (getfeedbackdetails.moveToNext()){
                        String serviceCallID = getfeedbackdetails.getString(0);
                        getservicecallmachineparametersList.add(serviceCallID);
                    }
                    for (String s : getservicecallmachineparametersList){
                        String serviceCallID = s;
                        getservicecallmachineparameters(serviceCallID);
                    }

                    MachineParameterDetailsInProcessCall();
                    Log.d(TAG, "run: "+"after 20 Seconds");
                }
            },20000);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Cursor getfeedbackdetails = helper.getFeedbackDetails();
                    while (getfeedbackdetails.moveToNext()){
                        String serviceCallID = getfeedbackdetails.getString(0);
                        getservicecallmachineparametersList.add(serviceCallID);
                    }
                    for (String s : getservicecallmachineparametersList){
                        String serviceCallID = s;
                        MachineParameterDetailsCompletedCall(serviceCallID);
                    }
                    CompetitorParameterDetailsInProcess();
                    Cursor cursor = helper.getFeedbackDetails();
                    while (cursor.moveToNext()){
                        String serviceCallID = cursor.getString(0);
                        getservicecallmachineparametersList.add(serviceCallID);
                    }
                    for (String s : getservicecallmachineparametersList){
                        String serviceCallID = s;
                        GetDocuments(serviceCallID);
                    }

                }
            },24000);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Cursor getPartOrders = helper.getPartOrderOfServiceIdAndCutsomerID();
                    while (getPartOrders.moveToNext()){
                        String serviceCallID = getPartOrders.getString(0);
                        String customerId = getPartOrders.getString(1);
                        partOrderModels.add(new GetPartOrderModel(serviceCallID,customerId));
                    }
                    for (GetPartOrderModel model : partOrderModels){
                        String serviceCallID = model.getServiceCallID();
                        String customerId = model.getCustomerID();
                        getTravelExpenses(serviceCallID,customerId);
                    }
                    Cursor cursor = helper.getFeedbackDetails();
                    while (cursor.moveToNext()){
                        String serviceCallID = cursor.getString(0);
                        getservicecallmachineparametersList.add(serviceCallID);
                    }
                    for (String s : getservicecallmachineparametersList){
                        String serviceCallID = s;
                        getFeedbacks(serviceCallID);
                    }
                    AppPreferenceManager.setIsDataSaved(Falco_SWAS_Main_Activity.this,"Data Is Saved");
                    progressDialog.dismiss();
                }
            },28000);
        }

        btn_sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    syncAllSwasData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public class TempSwasLoginCall extends AsyncTask<String,String,String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(Falco_SWAS_Main_Activity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String getResponse="";
            try {
                HttpParams httpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(httpParams, 60000);
                HttpConnectionParams.setSoTimeout(httpParams, 60000);
                HttpClient client = new DefaultHttpClient(httpParams);
                HttpPost httpPost = new HttpPost(URL_Constants.LoginApi);
                httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
               /* try {
                    String password = "password";
                    String encryptedMsg = AESCrypt.encrypt(password, str_password);
                    str_password = encryptedMsg;
                }catch (GeneralSecurityException e){
                    //handle error
                }
                str_password = Base64.encodeToString( str_password.getBytes(), Base64.DEFAULT );*/

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("grant_type","password"));
                nameValuePairs.add(new BasicNameValuePair("username",userEmail));
                //nameValuePairs.add(new BasicNameValuePair("password",str_password));
                nameValuePairs.add(new BasicNameValuePair("password","UJkGMEMpSWXJnOKrd9bxYSVnXF0uQ7zKFzGlygZdKl8="));
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                httpPost.setParams(httpParams);

                HttpResponse response = client.execute(httpPost);
                StatusLine statusLine = response.getStatusLine();

                if (statusLine.getStatusCode() == HttpStatus.SC_OK || statusLine.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();

                    getResponse = URL_Constants.convertStreamToString(inputStream);
                }
                else {
                    response.getEntity().getContent().close();
                }
            }
            catch (UnknownHostException e) {
                Log.i(TAG, "Error Message in UnknownHostException :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_UNKNOWN_HOST_EXCEPTION;

            }
            catch (ConnectTimeoutException e) {
                Log.i(TAG, "Error Message in ConnectTimeoutException :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_CONNECTION_TIMEOUT_EXCEPTION;


            }
            catch (ClientProtocolException e) {
                Log.i(TAG, "Error Message in ClientProtocolException :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_CLIENT_PROTOCOL_EXCEPTION;

            }
            catch (IOException e) {
                Log.i(TAG, "Error Message in IOException :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_IO_EXCEPTION;

            }
            catch (Exception e) {
                Log.i(TAG, "Error Message in Exception :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_EXCEPTION;

            }
            return getResponse;
        }
        @Override
        protected void onPostExecute(String getResponse) {
            super.onPostExecute(getResponse);
            progressDialog.dismiss();
            if (getResponse.contains("error"))
            {
                Toast.makeText(Falco_SWAS_Main_Activity.this, "Invalid username & password", Toast.LENGTH_SHORT).show();
//                ed_username.getText().clear();
//                ed_password.getText().clear();
            }
            else {
                if (!getResponse.equals("Error"))
                {
                    try {
                        JSONObject jsonObject=new JSONObject(getResponse);
//                        String token=jsonObject.getString("access_token");
//                        String token_type=jsonObject.getString("token_type");
//                        String user_emailid=jsonObject.getString("UserEmailId");
//                        String user_role=jsonObject.getString("RoleName");
//                        String oemid=jsonObject.getString("OEMID");
//                        UserModelToStoreInDB model = new UserModelToStoreInDB(jsonObject.getString("access_token"),
//                                jsonObject.getString("token_type"),jsonObject.getString("OEMID"),
//                                jsonObject.getString("UserEmailId"),jsonObject.getString("RoleName"));

                        ContentValues cv=new ContentValues();
                        cv.put("UserToken",jsonObject.getString("access_token"));
                        cv.put("UserTokenType",jsonObject.getString("token_type"));
                        cv.put("UserEmailID",jsonObject.getString("UserEmailId"));
                        cv.put("UserOEMID",jsonObject.getString("OEMID"));
                        cv.put("UserRoleName",jsonObject.getString("RoleName"));
                        long d=db.insert(Constants.TABLE_USER_DETAILS,null,cv);
                        Log.d(TAG, "onCreate: ");
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

    private void syncAllSwasData() throws JSONException {
        getDataFromFeedBackTableFromLocaDBAndSyncItToTheServer();
        getDataFromVisitLogsTableToSendToTheServer();
        getDataFromScheduledDatesLocalDbAndSenditToTheServer();
    }

    private void getDataFromScheduledDatesLocalDbAndSenditToTheServer() {
        Cursor cursor = helper.getDataFromScheduledDatedFromLocalDb();
        while (cursor.moveToNext()){

        }
    }

    private void getDataFromVisitLogsTableToSendToTheServer() throws JSONException {
        Cursor cursor = helper.getDataFromVisitLogsLocalDb();
        JSONArray array = new JSONArray();
        while (cursor.moveToNext()){
            String oemId = cursor.getString(2);
            String CustomerID = cursor.getString(4);
            String ServiceCallID = cursor.getString(3);
            String ServiceEngineerID = cursor.getString(5);
            String PartStatus = cursor.getString(6);
            String ServiceCallStartDate = cursor.getString(7);
            String ServiceCallEndDate = cursor.getString(8);
            String ServiceCallEntryTime = cursor.getString(9);
            String ServiceCallExitTime = cursor.getString(10);
            String GatePassImagePath = cursor.getString(11);
            GatePassImagePath = GatePassImagePath.replaceAll("\\n","");
            String Remark = cursor.getString(12);
            String IsActive = "1";
            String CreatedBy = cursor.getString(13);
            JSONObject object = new JSONObject();
            object.put("OEMID", oemId);
            object.put("CustomerID", CustomerID);
            object.put("ServiceCallID", ServiceCallID);
            object.put("ServiceEngineerID", ServiceEngineerID);
            object.put("PartStatus", PartStatus);
            object.put("ServiceCallStartDate", ServiceCallStartDate);
            object.put("ServiceCallEndDate", ServiceCallEndDate);
            object.put("ServiceCallEntryTime", ServiceCallEntryTime);
            object.put("ServiceCallExitTime", ServiceCallExitTime);
            object.put("GatePassImagePath", GatePassImagePath);
            object.put("Remark", Remark);
            object.put("IsActive", IsActive);
            object.put("CreatedBy", CreatedBy);
            array.put(object);
        }

        new SaveVisitLogDetails(array).execute();

//        API api = Retroconfig.swasretrofit().create(API.class);
//        Call<ResponseBody> call = api.saveVisitLogs(array);
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                if (response.body() != null) {
//                    try {
//                        String res = response.body().string();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//
//            }
//        });
    }

    private void getDataFromFeedBackTableFromLocaDBAndSyncItToTheServer() {
        feedBackModelForSyncingArrayList.clear();
        Cursor cursor = helper.getDataFromFeedbackTableFromLocalDB();
        while (cursor.moveToNext()){
            String feedbackId = cursor.getString(0);
            String serviceCallId = cursor.getString(1);
            String oemId = cursor.getString(2);
            String userId = cursor.getString(3);
            String userFeedBack = cursor.getString(4);
            String createdBy = cursor.getString(5);
            feedBackModelForSyncingArrayList.add(new FeedBackModelForSyncing(serviceCallId,oemId,userId,userFeedBack,createdBy,feedbackId));
        }
        for (FeedBackModelForSyncing model : feedBackModelForSyncingArrayList){
            String serviceCallId = model.getServiceCallId();
            String oemId = model.getOemId();
            String userId = model.getUserId();
            String userFeedBack = model.getFeedBack();
            String createdBy = model.getCreatedBy();
            API api = Retroconfig.swasretrofit().create(API.class);
            Call<ResponseBody> call = api.savefeedbackdetails(serviceCallId,oemId,userId,userFeedBack,createdBy);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (response.body() != null) {
                            String res = response.body().string();
                            if (res != null) {
                                JSONObject object = new JSONObject(res);
                                String statusCode = object.getString("statusCode");
                                if (statusCode.equals("200")){
                                    for (FeedBackModelForSyncing model : feedBackModelForSyncingArrayList){
                                        String feedbackid = model.getFeedbackId();
                                        helper.updateServiceFeedBackTable(feedbackid);
                                    }
                                }
//                            JSONObject object = new JSONObject(res);
//                            JSONArray array = object.getJSONArray("data");
//                            ArrayList<SwasIdDetailsModel> model = new ArrayList<>();
//                            if (array.length() != 0) {
//                            }
//                            else if (array.length() == 0) {
//                            }
                            }
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }
    }

    private void getFeedbacks(String serviceCallID) {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getfeedbackdetails(serviceCallID);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            ArrayList<FeedbackModel> model = new ArrayList<>();
                            if (array.length() != 0) {
                                for (int i = 0; i < array.length(); i++) {
                                    FeedbackModel feedbackModel = new FeedbackModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    feedbackModel.setFeedbackDetailsID(object1.getString("FeedbackDetailsID"));
                                    feedbackModel.setOEMID(object1.getString("OEMID"));
                                    feedbackModel.setServiceCallID(object1.getString("ServiceCallID"));
                                    feedbackModel.setUserID(object1.getString("UserID"));
                                    feedbackModel.setUserFeedback(object1.getString("UserFeedback"));
                                    feedbackModel.setCreatedBy(object1.getString("CreatedBy"));
                                    feedbackModel.setCreatedDate(object1.getString("CreatedDate"));
                                    model.add(feedbackModel);
                                    //DataBaseHelper helper=new DataBaseHelper(Falco_SWAS_Main_Activity.this);
                                    SQLiteDatabase db = helper.getWritableDatabase();
                                    ContentValues cv=new ContentValues();
                                    cv.put("FeedbackDetailsID",object1.getString("FeedbackDetailsID"));
                                    cv.put("OEMID",object1.getString("OEMID"));
                                    cv.put("ServiceCallID",object1.getString("ServiceCallID"));
                                    cv.put("UserID",object1.getString("UserID"));
                                    cv.put("UserFeedback",object1.getString("UserFeedback"));
                                    cv.put("CreatedBy",object1.getString("CreatedBy"));
                                    cv.put("CreatedDate",object1.getString("CreatedDate"));
                                    String FeedbackDetailsID = object1.getString("FeedbackDetailsID");
                                    String q = "SELECT * FROM "+ Constants.TABLE_SERVICE_CALL_FEEDBACK+" where "+Constants.SERVICE_CALL_FEEDBACK_FEEDBACK_DETAILS_ID+"='"+FeedbackDetailsID+"'";
                                    Cursor c = db.rawQuery(q,null);
                                    if(c.moveToFirst())
                                    {
                                        //showMessage("Error", "Record exist");
                                    }
                                    else
                                    {
                                        long d=db.insert(Constants.TABLE_SERVICE_CALL_FEEDBACK,null,cv);
                                        Log.d("user check", String.valueOf(d));
                                        if (d==-1){
                                            Log.d(TAG, "onResponse: "+"Data not Inserted");
                                        }else {
                                            Log.d(TAG, "onResponse: "+"Data Inserted");
                                        }
                                    }
                                }
                            } else if (array.length() == 0) {

                            }
                        }
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void getTravelExpenses(String serviceCallID, String customerId) {
        String oemId = getOEMIDofUser();
        API api = Retroconfig.swasretrofit().create(API.class);
        /*dateformat1 = "2020-01-20";*/
        Call<ResponseBody> call = api.gettravellingexpensesdetails(serviceCallID,customerId,oemId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            ArrayList<SwasCompletedTravelModel> model = new ArrayList<>();
                            if (array.length() != 0) {
                                for (int i = 0; i < array.length(); i++) {
                                    SwasCompletedTravelModel travelModel = new SwasCompletedTravelModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    travelModel.setTravellingExpensesHeadsID(object1.getString("TravellingExpensesHeadsID"));
                                    travelModel.setServiceCallID(object1.getString("ServiceCallID"));
                                    travelModel.setOEMID(object1.getString("OEMID"));
                                    travelModel.setCustomerID(object1.getString("CustomerID"));
                                    travelModel.setService_Expense_Heads_id(object1.getString("Service_Expense_Heads_id"));
                                    travelModel.setService_Expense_Heads_Name(object1.getString("Service_Expense_Heads_Name"));
                                    travelModel.setVisitDates(object1.getString("VisitDates"));
                                    travelModel.setVisitCharges(object1.getString("VisitCharges"));
                                    travelModel.setCreatedBy(object1.getString("CreatedBy"));
                                    travelModel.setCreatedDate(object1.getString("CreatedDate"));
                                    //txt_travel_service_call_id.setText(object1.getString("ServiceCallID"));
                                    //swasServiceModel .setServicecall_status_id(object1.getString("ServiceCallStatusID"));
                                    model.add(travelModel);
                                    //DataBaseHelper helper=new DataBaseHelper(Falco_SWAS_Main_Activity.this);
                                    SQLiteDatabase db = helper.getWritableDatabase();
                                    ContentValues cv=new ContentValues();
                                    cv.put("TravellingExpensesHeadsID",object1.getString("TravellingExpensesHeadsID"));
                                    cv.put("ServiceCallID",object1.getString("ServiceCallID"));
                                    cv.put("OEMID",object1.getString("OEMID"));
                                    cv.put("CustomerID",object1.getString("CustomerID"));
                                    cv.put("Service_Expense_Heads_id",object1.getString("Service_Expense_Heads_id"));
                                    cv.put("Service_Expense_Heads_Name",object1.getString("Service_Expense_Heads_Name"));
                                    cv.put("VisitDates",object1.getString("VisitDates"));
                                    cv.put("VisitCharges",object1.getString("VisitCharges"));
                                    cv.put("CreatedBy",object1.getString("CreatedBy"));
                                    cv.put("CreatedDate",object1.getString("CreatedDate"));

                                    String TravellingExpensesHeadsID = object1.getString("TravellingExpensesHeadsID");
                                    String q = "SELECT * FROM "+Constants.TABLE_EXPENSE_DETAILS+" where "+Constants.EXPENSE_DETAILS_TRAVELLING_EXPENSES_HEADS_ID+"='"+TravellingExpensesHeadsID+"'";
                                    Cursor c = db.rawQuery(q,null);
                                    if(c.moveToFirst())
                                    {
                                        //showMessage("Error", "Record exist");
                                    }
                                    else
                                    {
                                        long d=db.insert(Constants.TABLE_EXPENSE_DETAILS,null,cv);
                                        Log.d("user check", String.valueOf(d));
                                        if (d==-1){
                                            Log.d(TAG, "onResponse: "+"Data not Inserted");
                                        }else {
                                            Log.d(TAG, "onResponse: "+"Data Inserted");
                                        }
                                    }

                                }


                            } else if (array.length() == 0) {

                            }
                        }
                    }


                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void getservicecallmachineparameters(String serviceCallID) {
        API api = Retroconfig.swasretrofit().create(API.class);
        /*dateformat1 = "2020-01-20";*/
        Call<ResponseBody> call = api.getservicecallmachineparameters(serviceCallID);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            ArrayList<SwasCompletedMachineModel> model = new ArrayList<>();

                            if (array.length() != 0) {

                                for (int i = 0; i < array.length(); i++) {
                                    SwasCompletedMachineModel machineModel = new SwasCompletedMachineModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    machineModel.setMachineParameterLogID(object1.getString("MachineParameterLogID"));
                                    machineModel.setOEMID(object1.getString("OEMID"));
                                    machineModel.setCustomerID(object1.getString("CustomerID"));
                                    machineModel.setServiceCallID(object1.getString("ServiceCallID"));
                                    machineModel.setMachineParameters(object1.getString("MachineParameters"));
                                    machineModel.setServiceCallStartDate(object1.getString("ServiceCallStartDate"));
                                    machineModel.setServiceCallEndDate(object1.getString("ServiceCallEndDate"));
                                    machineModel.setParameterValue(object1.getString("ParameterValue"));
                                    machineModel.setRemark(object1.getString("Remark"));
                                    machineModel. setCreatedBy(object1.getString("CreatedBy"));
                                    machineModel.setCreatedDate(object1.getString("CreatedDate"));
                                    model.add(machineModel);

                                    //DataBaseHelper helper=new DataBaseHelper(Falco_SWAS_Main_Activity.this);
                                    SQLiteDatabase db = helper.getWritableDatabase();
                                    ContentValues cv=new ContentValues();
                                    cv.put("MachineParameterLogID",object1.getString("MachineParameterLogID"));
                                    cv.put("OEMID",object1.getString("OEMID"));
                                    cv.put("ServiceCallID",object1.getString("ServiceCallID"));
                                    cv.put("CustomerID",object1.getString("CustomerID"));
                                    cv.put("ServiceCallStartDate",object1.getString("ServiceCallStartDate"));
                                    cv.put("ServiceCallEndDate",object1.getString("ServiceCallEndDate"));
                                    cv.put("MachineParameters",object1.getString("MachineParameters"));
                                    cv.put("ParameterValue",object1.getString("ParameterValue"));
                                    cv.put("Remark",object1.getString("Remark"));
                                    cv.put("CreatedBy",object1.getString("CreatedBy"));
                                    cv.put("CreatedDate",object1.getString("CreatedDate"));
                                    long d=db.insert(Constants.TABLE_SERVICE_CALL_MC_PARAMETERS,null,cv);
                                    Log.d("user check", String.valueOf(d));
                                    if (d==-1){
                                        Log.d(TAG, "onResponse: "+"Data not Inserted");
                                    }else {
                                        Log.d(TAG, "onResponse: "+"Data Inserted");
                                    }
                                }

                            }
                            else if (array.length() == 0) {
//                                Toast.makeText(Swas_Completed_Machine_Parameters_Activity.this,"Sorry there is no data",Toast.LENGTH_SHORT).show();

                            }
                        }
                    }


                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void getservicecallvisitLOG(String serviceCallID) {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getservicecallvisitlog(serviceCallID);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {

                        String res = response.body().string();
                        if (res != null) {

                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            ArrayList<SwasCompletedVisitModel> model = new ArrayList<>();

                            if (array.length() != 0) {

                                for (int i = 0; i < array.length(); i++) {
                                    SwasCompletedVisitModel visitModel = new SwasCompletedVisitModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    visitModel.setServiceLogID(object1.getString("ServiceLogID"));
                                    visitModel.setOEMID(object1.getString("OEMID"));
                                    visitModel.setCustomerID(object1.getString("CustomerID"));
                                    visitModel.setServiceCallID(object1.getString("ServiceCallID"));
                                    visitModel.setServiceEngineerID(object1.getString("ServiceEngineerID"));
                                    visitModel.setPartStatus(object1.getString("PartStatus"));
                                    visitModel.setServiceCallStartDate(object1.getString("ServiceCallStartDate"));
                                    visitModel.setServiceCallEndDate(object1.getString("ServiceCallEndDate"));
                                    visitModel.setServiceCallEntryTime(object1.getString("ServiceCallEntryTime"));
                                    visitModel.setServiceCallExitTime(object1.getString("ServiceCallExitTime"));
                                    visitModel.setGatePassImagePath(object1.getString("GatePassImagePath"));
                                    visitModel.setRemark(object1.getString("Remark"));
                                    visitModel.setCreatedBy(object1.getString("CreatedBy"));
                                    visitModel.setCreatedDate(object1.getString("CreatedDate"));

                                    model.add(visitModel);

                                    //DataBaseHelper helper=new DataBaseHelper(Falco_SWAS_Main_Activity.this);
                                    SQLiteDatabase db = helper.getWritableDatabase();
                                    ContentValues cv=new ContentValues();
                                    cv.put("ServiceLogID",object1.getString("ServiceLogID"));
                                    cv.put("OEMID",object1.getString("OEMID"));
                                    cv.put("ServiceCallID",object1.getString("ServiceCallID"));
                                    cv.put("CustomerID",object1.getString("CustomerID"));
                                    cv.put("ServiceEngineerID",object1.getString("ServiceEngineerID"));
                                    cv.put("PartStatus",object1.getString("PartStatus"));
                                    cv.put("ServiceCallStartDate",object1.getString("ServiceCallStartDate"));
                                    cv.put("ServiceCallEndDate",object1.getString("ServiceCallEndDate"));
                                    cv.put("ServiceCallEntryTime",object1.getString("ServiceCallEntryTime"));
                                    cv.put("ServiceCallExitTime",object1.getString("ServiceCallExitTime"));
                                    cv.put("GatePassImagePath",object1.getString("GatePassImagePath"));
                                    cv.put("REMARK",object1.getString("Remark"));
                                    cv.put("CreatedBy",object1.getString("CreatedBy"));
                                    cv.put("CreatedDate",object1.getString("CreatedDate"));
                                    cv.put("IsSync","1");
                                    long d=db.insert(Constants.TABLE_VISIT_LOGS,null,cv);
                                    Log.d("user check", String.valueOf(d));
                                    if (d==-1){
                                        Log.d(TAG, "onResponse: "+"Data not Inserted");
                                    }else {
                                        Log.d(TAG, "onResponse: "+"Data Inserted");
                                    }
                                }


                            }
                            else if (array.length() == 0) {
                                //Toast.makeText(Swas_Completed_Visit_Log_Activity.this,"Sorry there is no data",Toast.LENGTH_SHORT).show();
                            }
                        }
                    }


                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void getFeedBackList(String serviceCallID) {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getfeedbackdetails(serviceCallID);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            //textView.setText(res);
                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");

                            ArrayList<FeedbackModel> model = new ArrayList<>();
                            if (array.length() != 0) {
                                for (int i = 0; i < array.length(); i++) {

                                    FeedbackModel feedbackModel = new FeedbackModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    feedbackModel.setFeedbackDetailsID(object1.getString("FeedbackDetailsID"));
                                    feedbackModel.setOEMID(object1.getString("OEMID"));
                                    feedbackModel.setServiceCallID(object1.getString("ServiceCallID"));
                                    feedbackModel.setUserID(object1.getString("UserID"));
                                    feedbackModel.setUserFeedback(object1.getString("UserFeedback"));
                                    feedbackModel.setCreatedBy(object1.getString("CreatedBy"));
                                    feedbackModel.setCreatedDate(object1.getString("CreatedDate"));
                                    model.add(feedbackModel);

                                    //DataBaseHelper helper=new DataBaseHelper(Falco_SWAS_Main_Activity.this);
                                    SQLiteDatabase db = helper.getWritableDatabase();
                                    ContentValues cv=new ContentValues();
                                    cv.put("FeedbackDetailsID",object1.getString("FeedbackDetailsID"));
                                    cv.put("OEMID",object1.getString("OEMID"));
                                    cv.put("ServiceCallID",object1.getString("ServiceCallID"));
                                    cv.put("UserID",object1.getString("UserID"));
                                    cv.put("UserFeedback",object1.getString("UserFeedback"));
                                    cv.put("CreatedBy",object1.getString("CreatedBy"));
                                    cv.put("CreatedDate",object1.getString("CreatedDate"));
                                    long d=db.insert(Constants.TABLE_SERVICE_CALL_FEEDBACK,null,cv);
                                    Log.d("user check", String.valueOf(d));
                                    if (d==-1){
                                        Log.d(TAG, "onResponse: "+"Data not Inserted");
                                    }else {
                                        Log.d(TAG, "onResponse: "+"Data Inserted");
                                    }
                                }


                            } else if (array.length() == 0) {

                            }
                        }

                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void getexpenseheadsList(String customerID) {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getexpenseheadslist1(customerID);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body() != null) {
                    try {
                        String res = response.body().string();
                        if (res != null) {
                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            ArrayList<ExpenseHeadMasterModel> model = new ArrayList<>();
                            if (array.length() != 0){
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object1 = array.getJSONObject(i);
                                    String Service_Expense_Heads_id = object1.getString("Service_Expense_Heads_id");
                                    String Service_Expense_Heads_Name = object1.getString("Service_Expense_Heads_Name");
                                    model.add(new ExpenseHeadMasterModel(Service_Expense_Heads_id,Service_Expense_Heads_Name));

                                    //DataBaseHelper helper=new DataBaseHelper(Falco_SWAS_Main_Activity.this);
                                    SQLiteDatabase db = helper.getWritableDatabase();
                                    ContentValues cv=new ContentValues();
                                    cv.put("Service_Expense_Heads_id",object1.getString("Service_Expense_Heads_id"));
                                    cv.put("Service_Expense_Heads_Name",object1.getString("Service_Expense_Heads_Name"));
                                    long d=db.insert(Constants.TABLE_EXPENSE_HEAD_MASTER,null,cv);
                                    Log.d("user check", String.valueOf(d));
                                    if (d==-1){
                                        Log.d(TAG, "onResponse: "+"Data not Inserted");
                                    }else {
                                        Log.d(TAG, "onResponse: "+"Data Inserted");
                                    }
                                }
                            }
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(Falco_SWAS_Main_Activity.this, "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getCompPara(String serviceCallID) {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getservicecallcompetitorsparameters(serviceCallID);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {

                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");

                            ArrayList<SwasCompletedCompetitorModel> model = new ArrayList<>();
                            if (array.length() != 0) {

                                for (int i = 0; i < array.length(); i++) {
                                    SwasCompletedCompetitorModel compModel = new SwasCompletedCompetitorModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    compModel.setCompetitorsParameterID(object1.getString("CompetitorsParameterID"));
                                    compModel.setOEMID(object1.getString("OEMID"));
                                    compModel.setCustomerID(object1.getString("CustomerID"));
                                    compModel.setServiceCallID(object1.getString("ServiceCallID"));
                                    compModel.setCompetitorsParameters(object1.getString("CompetitorsParameters"));
                                    compModel.setServiceCallStartDate(object1.getString("ServiceCallStartDate"));
                                    compModel.setMachineParameterValues(object1.getString("MachineParameterValues"));
                                    compModel.setRemark(object1.getString("Remark"));
                                    compModel.setCreatedBy(object1.getString("CreatedBy"));
                                    compModel.setCreatedDate(object1.getString("CreatedDate"));
                                    compModel.setCreatedDate(object1.getString("UNIT"));

                                    model.add(compModel);

                                    //DataBaseHelper helper=new DataBaseHelper(Falco_SWAS_Main_Activity.this);
                                    SQLiteDatabase db = helper.getWritableDatabase();
                                    ContentValues cv=new ContentValues();
                                    cv.put("CompetitorsParameterID",object1.getString("CompetitorsParameterID"));
                                    cv.put("OEMID",object1.getString("OEMID"));
                                    cv.put("CustomerID",object1.getString("CustomerID"));
                                    cv.put("ServiceCallID",object1.getString("ServiceCallID"));
                                    cv.put("CompetitorsParameter",object1.getString("CompetitorsParameters"));
                                    cv.put("ServiceCallStartDate",object1.getString("ServiceCallStartDate"));
                                    cv.put("MachineParameterValues",object1.getString("MachineParameterValues"));
                                    cv.put("Remark",object1.getString("Remark"));
                                    cv.put("CreatedBy",object1.getString("CreatedBy"));
                                    cv.put("CreatedDate",object1.getString("CreatedDate"));
                                    cv.put("UNIT",object1.getString("UNIT"));

                                    String CompetitorsParameterID = object1.getString("CompetitorsParameterID");
                                    String q = "SELECT * FROM "+Constants.TABLE_SERVICE_CALL_COMP_PARAMETERS+" where "+Constants.SERVICE_CALL_COMP_PARAMETERS_COMPITITORS_PARMETER_ID+"='"+CompetitorsParameterID+"'";
                                    Cursor c = db.rawQuery(q,null);
                                    if(c.moveToFirst())
                                    {
                                        //showMessage("Error", "Record exist");
                                    }
                                    else
                                    {
                                        long d=db.insert(Constants.TABLE_SERVICE_CALL_COMP_PARAMETERS,null,cv);
                                        Log.d("user check", String.valueOf(d));
                                        if (d==-1){
                                            Log.d(TAG, "onResponse: "+"Data not Inserted");
                                        }else {
                                            Log.d(TAG, "onResponse: "+"Data Inserted");
                                        }
                                    }

                                }


                            } else if (array.length() == 0) {
//                                Toast.makeText(Falco_SWAS_Main_Activity.this,"Sorry there is no data",Toast.LENGTH_SHORT).show();

                            }
                        }
                    }


                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void getPartOrdersServiceCall(String serviceCallID, String customerId) {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getpartordered(serviceCallID,customerId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {

                            JSONArray array = new JSONArray(res);
                            ArrayList<PartOrderByCustomerModel> partOrderByCustomerModelArrayList = new ArrayList<>();
                            if (array.length() != 0) {

                                for (int i = 0; i < array.length(); i++) {
                                    PartOrderByCustomerModel model = new PartOrderByCustomerModel();
                                    JSONObject obj = array.getJSONObject(i);
                                    model.setApplicable(obj.getString("Applicable"));
                                    model.setConsumables(obj.getString("Consumables"));
                                    model.setCreatedBy(obj.getString("CreatedBy"));
                                    model.setCreatedDate(obj.getString("CreatedDate"));
                                    model.setCustomerID(obj.getString("CustomerID"));
                                    model.setExcludeInclude(obj.getString("ExcludeInclude"));
                                    model.setFinalPrice(obj.getString("FinalPrice"));
                                    model.setITEMID(obj.getString("ITEMID"));
                                    model.setMachineID(obj.getString("MachineID"));
                                    model.setNAME(obj.getString("NAME"));
                                    model.setPRICE(obj.getString("PRICE"));
                                    model.setOEMID(obj.getString("OEMID"));
                                    model.setQuantity(obj.getString("Quantity"));
                                    model.setQuantity(obj.getString("ServiceCallID"));

                                    partOrderByCustomerModelArrayList.add(model);
                                    //DataBaseHelper helper=new DataBaseHelper(Falco_SWAS_Main_Activity.this);
                                    SQLiteDatabase db = helper.getWritableDatabase();
                                    ContentValues cv=new ContentValues();
                                    cv.put("ServiceCallID",obj.getString("ServiceCallID"));
                                    cv.put("Applicable",obj.getString("Applicable"));
                                    //cv.put("Consumables",obj.getString("Consumables"));//--
                                    cv.put("CreatedBy",obj.getString("CreatedBy"));
                                    cv.put("CreatedDate",obj.getString("CreatedDate"));
                                    cv.put("CustomerID",obj.getString("CustomerID"));
                                    cv.put("ExcludeInclude",obj.getString("ExcludeInclude"));
                                    cv.put("FinalPrice",obj.getString("FinalPrice"));
                                    cv.put("ITEMID",obj.getString("ITEMID"));
                                    //cv.put("MachineID",obj.getString("MachineID"));//--
                                    cv.put("NAME",obj.getString("NAME"));
                                    cv.put("PRICE",obj.getString("PRICE"));
                                    cv.put("OEMID",obj.getString("OEMID"));
                                    cv.put("Quantity",obj.getString("Quantity"));

                                    long d=db.insert(Constants.TABLE_PART_ORDERS,null,cv);
                                    Log.d("user check", String.valueOf(d));
                                    if (d==-1){
                                        Log.d(TAG, "onResponse: "+"Data not Inserted");
                                    }else {
                                        Log.d(TAG, "onResponse: "+"Data Inserted");
                                    }
                                }


                            } else if (array.length() == 0) {
                                //relativeLayout1.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void getCheckListOfServiceCall(final String serviceCallID, String typeOfServiceCallId) {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getchecklistofservicecall(serviceCallID,typeOfServiceCallId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {

                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            ArrayList<Checklist_Model> checklist_models = new ArrayList<>();
                            if (array.length() != 0) {

                                for (int i = 0; i < array.length(); i++) {
                                    Checklist_Model model = new Checklist_Model();
                                    JSONObject obj = array.getJSONObject(i);
                                    model.setCheck_id(obj.getString("ChecklistForSiteReadynessID"));
                                    model.setCheck_name(obj.getString("ChecklistForSiteReadynessText"));
                                    model.setChecked(obj.getString("Checked"));
                                    checklist_models.add(model);

                                    //DataBaseHelper helper=new DataBaseHelper(Falco_SWAS_Main_Activity.this);
                                    SQLiteDatabase db = helper.getWritableDatabase();
                                    ContentValues cv=new ContentValues();
                                    cv.put("ServiceCallID",serviceCallID);
                                    cv.put("ChecklistForSiteReadynessID",obj.getString("ChecklistForSiteReadynessID"));
                                    cv.put("ChecklistForSiteReadynessText",obj.getString("ChecklistForSiteReadynessText"));
                                    cv.put("Checked",obj.getString("Checked"));

                                    long d=db.insert(Constants.TABLE_SERVICE_CALL_CHECKLIST,null,cv);
                                    Log.d("user check", String.valueOf(d));
                                    if (d==-1){
                                        Log.d(TAG, "onResponse: "+"Data not Inserted");
                                    }else {
                                        Log.d(TAG, "onResponse: "+"Data Inserted");
                                    }

                                }


                            } else if (array.length() == 0) {
                            }
                        }
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

//    private void gtAllServiceCall() {
//
//        API api = Retroconfig.swasretrofit().create(API.class);
//        List<Observable<?>> requests = new ArrayList<>();
//        requests.add(api.getservicecalllistbystatus("Assigned Call","1"));
//        requests.add(api.getservicecalllistbystatus("Completed Call","1"));
//        requests.add(api.getservicecalllistbystatus("In process call","1"));
//
//        Observable.zip(
//                requests,
//                new Function<Object[], Object>() {
//                    @Override
//                    public Object apply(Object[] objects) throws Exception {
//                        // Objects[] is an array of combined results of completed requests
//
//                        // do something with those results and emit new event
//                        for (int i = 0 ; i < objects.length ; i++){
//                            String res = objects.toString();
//                            Log.d(TAG, "apply: "+res);
//                        }
//                        return new Object();
//                    }
//                }).subscribe(
//                // Will be triggered if all requests will end successfully (4xx and 5xx also are successful requests too)
//                new Consumer<Object>() {
//                    @Override
//                    public void accept(Object o) throws Exception {
//                        //Do something on successful completion of all requests
//                    }
//                },
//
//                // Will be triggered if any error during requests will happen
//                new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable e) throws Exception {
//                        //Do something on error completion of requests
//                    }
//                }
//        );
//    }

    private void getDetailsOFSingleServiceCall(final String serviceCallID, String statusID) {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getalldetailsofsingleservicecall(serviceCallID,statusID);
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String res = null;
                try {
                    res = response.body().string();
                    if (res != null) {
                        JSONObject object = new JSONObject(res);
                        JSONArray array = object.getJSONArray("data");
                        ArrayList<SwasIdDetailsModel> model = new ArrayList<>();
                        if (array.length() != 0) {
                            for (int i = 0; i < array.length(); i++) {
                                SwasIdDetailsModel swasIdDetailsModel = new SwasIdDetailsModel();
                                JSONObject object1 = array.getJSONObject(i);
                                swasIdDetailsModel.setService_Id(object1.getString("ServiceCallID"));
                                swasIdDetailsModel.setLOCATIONNAME(object1.getString("LOCATIONNAME"));
                                swasIdDetailsModel.setCreatedBy(object1.getString("CreatedBy"));
                                swasIdDetailsModel.setCreatedDate(object1.getString("CreatedDate"));
                                swasIdDetailsModel.setUpdatedBy(object1.getString("UpdatedBy"));
                                swasIdDetailsModel.setUpdatedDate(object1.getString("UpdatedDate"));
                                swasIdDetailsModel.setTypeOfServiceCallText(object1.getString("TypeOfServiceCallText"));
                                swasIdDetailsModel.setNAME(object1.getString("NAME"));
                                swasIdDetailsModel.setBriefComplaint(object1.getString("BriefComplaint"));
                                swasIdDetailsModel.setTypeOfFaultText(object1.getString("TypeOfFaultText"));
                                swasIdDetailsModel.setStatusText(object1.getString("StatusText"));
                                swasIdDetailsModel.setFaultyPart(object1.getString("FaultyPart"));
                                swasIdDetailsModel.setCustomerID(object1.getString("CustomerID"));
                                swasIdDetailsModel.setITEMID(object1.getString("ITEMID"));
                                swasIdDetailsModel.setCURRENCY(object1.getString("CURRENCY"));
                                swasIdDetailsModel.setDocumentPath(object1.getString("DocumentPath"));
                                swasIdDetailsModel.setTypeOfServiceCallID(object1.getString("TypeOfServiceCallID"));
                                swasIdDetailsModel.setCurrentlyAssignedToSE(object1.getString("CurrentlyAssignedToSE"));
                                swasIdDetailsModel.setServiceEngineerName(object1.getString("ServiceEngineerName"));
                                swasIdDetailsModel.setClosedDateTime(object1.getString("ClosedDateTime"));
                                swasIdDetailsModel.setClosedBy(object1.getString("ClosedBy"));
                                swasIdDetailsModel.setServiceCallStatusID(object1.getString("ServiceCallStatusID"));
                                swasIdDetailsModel.setComments(object1.getString("Comments"));
                                swasIdDetailsModel.setServiceEngineerID(object1.getString("ServiceEngineerID"));
                                swasIdDetailsModel.setNumberOfVisits(object1.getString("NumberOfVisits"));

                                //Write here the code of update
                                helper.updateListOfServicesByStatus(serviceCallID,object1.getString("NAME"),object1.getString("BriefComplaint"),
                                        object1.getString("TypeOfFaultText"),object1.getString("FaultyPart"),object1.getString("CURRENCY"),
                                        object1.getString("DocumentPath"),object1.getString("TypeOfServiceCallID"),object1.getString("CurrentlyAssignedToSE"),
                                        object1.getString("ClosedDateTime"),object1.getString("ClosedBy"),object1.getString("Comments"),
                                        object1.getString("ServiceEngineerID"),object1.getString("NumberOfVisits"));

//                                txt_assign_sr_id.setText(object1.getString("ServiceCallID"));
//                                txt_assign_custom_name.setText(object1.getString("LOCATIONNAME"));
//                                txt_assign_call_created_by.setText(object1.getString("CreatedBy"));
//                                txt_assign_create_on.setText(object1.getString("CreatedDate"));
//                                txt_type_service_call.setText(object1.getString("TypeOfServiceCallText"));
//                                txt_service_call_status_text.setText(object1.getString("StatusText"));
//                                txt_assign_product.setText(object1.getString("NAME"));
//                                txt_last_modified.setText(object1.getString("UpdatedDate"));

//                                if(!object1.getString("Comments").equals("null"))
//                                {
//                                    txt_note_for_service_engineer.setText(object1.getString("Comments"));
//                                }

//                                txt_type_of_fault_text.setText(object1.getString("TypeOfFaultText"));
//                                complaint = object1.getString("BriefComplaint");
//                                customer_id = object1.getString("CustomerID");
//                                status_id = object1.getString("ServiceCallStatusID");
//                                service_call_id = object1.getString("ServiceCallID");
//                                //type_of_service_status_id = object1.getString("TypeOfServiceCallID");
//                                type_of_service_call_id = object1.getString("TypeOfServiceCallID");
                                model.add(swasIdDetailsModel);
                            }
                        }
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void getfaultcharacteristicslist(String id) {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getfaultcharacteristicslist(id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            ArrayList<FaultyCharactristics_Model> faultyCharactristics_modelArrayList = new ArrayList<>();
                            if (array.length() != 0) {
                                for (int i = 0; i < array.length(); i++) {
                                    FaultyCharactristics_Model model = new FaultyCharactristics_Model();
                                    JSONObject obj = array.getJSONObject(i);
                                    model.setCustomerID(obj.getString("CustomerID"));
                                    model.setServiceCallID(obj.getString("ServiceCallID"));
                                    model.setFaultCharacteristicsText(obj.getString("FaultCharacteristicsText"));
                                    faultyCharactristics_modelArrayList.add(model);

                                    //DataBaseHelper helper=new DataBaseHelper(Falco_SWAS_Main_Activity.this);
                                    SQLiteDatabase db = helper.getWritableDatabase();
                                    ContentValues cv=new ContentValues();
                                    cv.put("CustomerID",obj.getString("CustomerID"));
                                    cv.put("ServiceCallID",obj.getString("ServiceCallID"));
                                    cv.put("FaultCharacteristicsText",obj.getString("FaultCharacteristicsText"));

                                    long d=db.insert(Constants.TABLE_SERVICE_CALL_FAULT_CHAR,null,cv);
                                    Log.d("user check", String.valueOf(d));
                                    if (d==-1){
                                        Log.d(TAG, "onResponse: "+"Data not Inserted");
                                    }else {
                                        Log.d(TAG, "onResponse: "+"Data Inserted");
                                    }
                                }

                            } else if (array.length() == 0) {
                                //relativeLayout1.setVisibility(View.VISIBLE);
                            }
                        }
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void getAssignedCalls() {
        progressDialog=new ProgressDialog(Falco_SWAS_Main_Activity.this);
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);
        progressDialog.show();
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getservicecalllistbystatus("Assigned Call",service_eng_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            ArrayList<SwasServiceCallIdModel> model = new ArrayList<>();

                            if (array.length() != 0) {
                                for (int i = 0; i < array.length(); i++) {
                                    SwasServiceCallIdModel swasServiceModel = new SwasServiceCallIdModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    swasServiceModel.setServiceCallID(object1.getString("ServiceCallID"));
                                    swasServiceModel.setCustomerID(object1.getString("CustomerID"));
                                    swasServiceModel.setLOCATIONNAME(object1.getString("LOCATIONNAME"));
                                    swasServiceModel.setITEMID(object1.getString("ITEMID"));
                                    swasServiceModel.setServiceEngineerName(object1.getString("ServiceEngineerName"));
                                    swasServiceModel.setStatusText(object1.getString("StatusText"));
                                    swasServiceModel.setTypeOfServiceCallText(object1.getString("TypeOfServiceCallText"));
                                    swasServiceModel.setCreatedBy(object1.getString("CreatedBy"));
                                    swasServiceModel.setCreatedDate(object1.getString("CreatedDate"));
                                    swasServiceModel.setUpdatedBy(object1.getString("UpdatedBy"));
                                    swasServiceModel.setUpdatedDate(object1.getString("UpdatedDate"));
                                    swasServiceModel.setServiceCallStatusID(object1.getString("ServiceCallStatusID"));

                                    model.add(swasServiceModel);

                                    //DataBaseHelper helper=new DataBaseHelper(Falco_SWAS_Main_Activity.this);
                                    SQLiteDatabase db = helper.getWritableDatabase();
                                    ContentValues cv=new ContentValues();
                                    cv.put("ServiceCallID",object1.getString("ServiceCallID"));
                                    cv.put("CustomerID",object1.getString("CustomerID"));
                                    cv.put("LOCATIONNAME",object1.getString("LOCATIONNAME"));
                                    cv.put("ITEMID",object1.getString("ITEMID"));
                                    cv.put("ServiceEngineerName",object1.getString("ServiceEngineerName"));
                                    cv.put("StatusText",object1.getString("StatusText"));
                                    cv.put("TypeOfServiceCallText",object1.getString("TypeOfServiceCallText"));
                                    cv.put("CreatedBy",object1.getString("CreatedBy"));
                                    cv.put("CreatedDate",object1.getString("CreatedDate"));
                                    cv.put("UpdatedBy",object1.getString("UpdatedBy"));
                                    cv.put("UpdatedDate",object1.getString("UpdatedDate"));
                                    cv.put("ServiceCallStatusID",object1.getString("ServiceCallStatusID"));

                                    String serviceCallID = object1.getString("ServiceCallID");
                                    String q = "SELECT * FROM "+Constants.TABLE_LIST_OF_SERVICES_BY_STATUS+" where "+Constants.LIST_OF_SERVICES_BY_STATUS_SERVICE_CALL_ID+"='"+serviceCallID+"'";
                                    Cursor c = db.rawQuery(q,null);
                                    if(c.moveToFirst())
                                    {
                                        //showMessage("Error", "Record exist");
                                    }
                                    else
                                    {
                                        long d=db.insert(Constants.TABLE_LIST_OF_SERVICES_BY_STATUS,null,cv);
                                        Log.d("user check", String.valueOf(d));
                                    }
                                }
//                                adapter = new ServiceIdListAdapter(Swas_Service_Call_List_Activity.this, model);
//                                rv_service_id_details.setLayoutManager(new LinearLayoutManager(Swas_Service_Call_List_Activity.this, LinearLayoutManager.VERTICAL, false));
//                                rv_service_id_details.setAdapter(adapter);

                            } else if (array.length() == 0) {
//                                Toast.makeText(Falco_SWAS_Main_Activity.this, "Sorry there is no data", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else
                    {
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(Falco_SWAS_Main_Activity.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getCompletedCalls() {
        API api = Retroconfig.swasretrofit().create(API.class);
        // String service_eng_id = "1";
        Call<ResponseBody> call = api.getservicecalllistbystatus("Completed Call",service_eng_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            ArrayList<SwasServiceCallIdModel> model = new ArrayList<>();

                            if (array.length() != 0) {
                                for (int i = 0; i < array.length(); i++) {
                                    SwasServiceCallIdModel swasServiceModel = new SwasServiceCallIdModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    swasServiceModel.setServiceCallID(object1.getString("ServiceCallID"));
                                    swasServiceModel.setCustomerID(object1.getString("CustomerID"));
                                    swasServiceModel.setLOCATIONNAME(object1.getString("LOCATIONNAME"));
                                    swasServiceModel.setITEMID(object1.getString("ITEMID"));
                                    swasServiceModel.setServiceEngineerName(object1.getString("ServiceEngineerName"));
                                    swasServiceModel.setStatusText(object1.getString("StatusText"));
                                    swasServiceModel.setTypeOfServiceCallText(object1.getString("TypeOfServiceCallText"));
                                    swasServiceModel.setCreatedBy(object1.getString("CreatedBy"));
                                    swasServiceModel.setCreatedDate(object1.getString("CreatedDate"));
                                    swasServiceModel.setUpdatedBy(object1.getString("UpdatedBy"));
                                    swasServiceModel.setUpdatedDate(object1.getString("UpdatedDate"));
                                    swasServiceModel.setServiceCallStatusID(object1.getString("ServiceCallStatusID"));

                                    model.add(swasServiceModel);

                                    //DataBaseHelper helper=new DataBaseHelper(Falco_SWAS_Main_Activity.this);
                                    SQLiteDatabase db = helper.getWritableDatabase();
                                    ContentValues cv=new ContentValues();
                                    cv.put("ServiceCallID",object1.getString("ServiceCallID"));
                                    cv.put("CustomerID",object1.getString("CustomerID"));
                                    cv.put("LOCATIONNAME",object1.getString("LOCATIONNAME"));
                                    cv.put("ITEMID",object1.getString("ITEMID"));
                                    cv.put("ServiceEngineerName",object1.getString("ServiceEngineerName"));
                                    cv.put("StatusText",object1.getString("StatusText"));
                                    cv.put("TypeOfServiceCallText",object1.getString("TypeOfServiceCallText"));
                                    cv.put("CreatedBy",object1.getString("CreatedBy"));
                                    cv.put("CreatedDate",object1.getString("CreatedDate"));
                                    cv.put("UpdatedBy",object1.getString("UpdatedBy"));
                                    cv.put("UpdatedDate",object1.getString("UpdatedDate"));
                                    cv.put("ServiceCallStatusID",object1.getString("ServiceCallStatusID"));
                                    String serviceCallID = object1.getString("ServiceCallID");
                                    String q = "SELECT * FROM "+Constants.TABLE_LIST_OF_SERVICES_BY_STATUS+" where "+Constants.LIST_OF_SERVICES_BY_STATUS_SERVICE_CALL_ID+"='"+serviceCallID+"'";
                                    Cursor c = db.rawQuery(q,null);
                                    if(c.moveToFirst())
                                    {
                                        //showMessage("Error", "Record exist");
                                    }
                                    else
                                    {
                                        long d=db.insert(Constants.TABLE_LIST_OF_SERVICES_BY_STATUS,null,cv);
                                        Log.d("user check", String.valueOf(d));
                                    }
                                }
//                                adapter = new ServiceIdListAdapter(Swas_Service_Call_List_Activity.this, model);
//                                rv_service_id_details.setLayoutManager(new LinearLayoutManager(Swas_Service_Call_List_Activity.this, LinearLayoutManager.VERTICAL, false));
//                                rv_service_id_details.setAdapter(adapter);


                            } else if (array.length() == 0) {
//                                Toast.makeText(Falco_SWAS_Main_Activity.this, "Sorry there is no data", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else
                    {
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(Falco_SWAS_Main_Activity.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getInProcessCalls() {
        API api = Retroconfig.swasretrofit().create(API.class);
        // String service_eng_id = "1";
        Call<ResponseBody> call = api.getservicecalllistbystatus("In process call",service_eng_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            ArrayList<SwasServiceCallIdModel> model = new ArrayList<>();

                            if (array.length() != 0) {
                                for (int i = 0; i < array.length(); i++) {
                                    SwasServiceCallIdModel swasServiceModel = new SwasServiceCallIdModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    swasServiceModel.setServiceCallID(object1.getString("ServiceCallID"));
                                    swasServiceModel.setCustomerID(object1.getString("CustomerID"));
                                    swasServiceModel.setLOCATIONNAME(object1.getString("LOCATIONNAME"));
                                    swasServiceModel.setITEMID(object1.getString("ITEMID"));
                                    swasServiceModel.setServiceEngineerName(object1.getString("ServiceEngineerName"));
                                    swasServiceModel.setStatusText(object1.getString("StatusText"));
                                    swasServiceModel.setTypeOfServiceCallText(object1.getString("TypeOfServiceCallText"));
                                    swasServiceModel.setCreatedBy(object1.getString("CreatedBy"));
                                    swasServiceModel.setCreatedDate(object1.getString("CreatedDate"));
                                    swasServiceModel.setUpdatedBy(object1.getString("UpdatedBy"));
                                    swasServiceModel.setUpdatedDate(object1.getString("UpdatedDate"));
                                    swasServiceModel.setServiceCallStatusID(object1.getString("ServiceCallStatusID"));

                                    model.add(swasServiceModel);

                                    //DataBaseHelper helper=new DataBaseHelper(Falco_SWAS_Main_Activity.this);
                                    SQLiteDatabase db = helper.getWritableDatabase();
                                    ContentValues cv=new ContentValues();
                                    cv.put("ServiceCallID",object1.getString("ServiceCallID"));
                                    cv.put("CustomerID",object1.getString("CustomerID"));
                                    cv.put("LOCATIONNAME",object1.getString("LOCATIONNAME"));
                                    cv.put("ITEMID",object1.getString("ITEMID"));
                                    cv.put("ServiceEngineerName",object1.getString("ServiceEngineerName"));
                                    cv.put("StatusText",object1.getString("StatusText"));
                                    cv.put("TypeOfServiceCallText",object1.getString("TypeOfServiceCallText"));
                                    cv.put("CreatedBy",object1.getString("CreatedBy"));
                                    cv.put("CreatedDate",object1.getString("CreatedDate"));
                                    cv.put("UpdatedBy",object1.getString("UpdatedBy"));
                                    cv.put("UpdatedDate",object1.getString("UpdatedDate"));
                                    cv.put("ServiceCallStatusID",object1.getString("ServiceCallStatusID"));

                                    String serviceCallID = object1.getString("ServiceCallID");
                                    String q = "SELECT * FROM "+Constants.TABLE_LIST_OF_SERVICES_BY_STATUS+" where "+Constants.LIST_OF_SERVICES_BY_STATUS_SERVICE_CALL_ID+"='"+serviceCallID+"'";
                                    Cursor c = db.rawQuery(q,null);
                                    if(c.moveToFirst())
                                    {
                                        //showMessage("Error", "Record exist");
                                    }
                                    else
                                    {
                                        long d=db.insert(Constants.TABLE_LIST_OF_SERVICES_BY_STATUS,null,cv);
                                        Log.d("user check", String.valueOf(d));
                                    }
                                }
//                                adapter = new ServiceIdListAdapter(Swas_Service_Call_List_Activity.this, model);
//                                rv_service_id_details.setLayoutManager(new LinearLayoutManager(Swas_Service_Call_List_Activity.this, LinearLayoutManager.VERTICAL, false));
//                                rv_service_id_details.setAdapter(adapter);


                            } else if (array.length() == 0) {
//                                Toast.makeText(Falco_SWAS_Main_Activity.this, "Sorry there is no data", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else
                    {
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(Falco_SWAS_Main_Activity.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void MachineParameterDetailsInProcessCall() {
        final ArrayList<SwasCompletedCallMachineParameterModel> model = new ArrayList<>();
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getmachineparameterslist();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                    }
                    String res = response.body().string();
                    if (res != null) {

                        JSONObject object = new JSONObject(res);
                        JSONArray array = object.getJSONArray("data");

                        if (array.length() != 0) {

                            for (int i = 0; i < array.length(); i++) {
                                SwasCompletedCallMachineParameterModel swasCompletedCallMachineParameterModel = new SwasCompletedCallMachineParameterModel();
                                JSONObject object1 = array.getJSONObject(i);
                                swasCompletedCallMachineParameterModel.setMachineParameterLogID(object1.getString("MachineParameterID"));
                                swasCompletedCallMachineParameterModel.setMachineParameters(object1.getString("MachineParameters"));
                                swasCompletedCallMachineParameterModel.setUnit(object1.getString("UNIT"));
                                swasCompletedCallMachineParameterModel.setRemark("NA");
                                model.add(swasCompletedCallMachineParameterModel);

                                //DataBaseHelper helper=new DataBaseHelper(Falco_SWAS_Main_Activity.this);
                                SQLiteDatabase db = helper.getWritableDatabase();
                                ContentValues cv=new ContentValues();
                                cv.put("MachineParameterID",object1.getString("MachineParameterID"));
                                cv.put("MachineParameters",object1.getString("MachineParameters"));
                                cv.put("UNIT",object1.getString("UNIT"));
//                                long d=db.insert(Constants.TABLE_MACHINE_PARAMETER_INPROCESS_CALL,null,cv);
//                                Log.d("user check", String.valueOf(d));
//                                if (d==-1){
//                                    Log.d(TAG, "onResponse: "+"Data not Inserted");
//                                }else {
//                                    Log.d(TAG, "onResponse: "+"Data Inserted");
//                                }
                                String q = "SELECT * FROM "+Constants.TABLE_MACHINE_PARAMETER_INPROCESS_CALL+" where "+Constants.MACHINE_PARAMETER_INPROCESS_CALL_ID+"='"+object1.getString("MachineParameterLogID")+"'";
                                Cursor c = db.rawQuery(q,null);
                                if(c.moveToFirst())
                                {
                                    //showMessage("Error", "Record exist");
                                }
                                else
                                {
                                    long d=db.insert(Constants.TABLE_MACHINE_PARAMETER_INPROCESS_CALL,null,cv);
                                    Log.d("user check", String.valueOf(d));
                                    if (d==-1){
                                        Log.d(TAG, "onResponse: "+"Data not Inserted");
                                    }else {
                                        Log.d(TAG, "onResponse: "+"Data Inserted");
                                    }
                                }
                            }

                        } else if (array.length() == 0) {
                        }
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void MachineParameterDetailsCompletedCall(final String serviceCallID) {
        API api = Retroconfig.swasretrofit().create(API.class);
        /*dateformat1 = "2020-01-20";*/
        Call<ResponseBody> call = api.getservicecallmachineparameters(serviceCallID);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            ArrayList<SwasCompletedMachineModel> model = new ArrayList<>();

                            if (array.length() != 0) {

                                for (int i = 0; i < array.length(); i++) {
                                    SwasCompletedMachineModel machineModel = new SwasCompletedMachineModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    machineModel.setMachineParameterLogID(object1.getString("MachineParameterLogID"));
                                    machineModel.setOEMID(object1.getString("OEMID"));
                                    machineModel.setCustomerID(object1.getString("CustomerID"));
                                    machineModel.setServiceCallID(object1.getString("ServiceCallID"));
                                    machineModel.setMachineParameters(object1.getString("MachineParameters"));
                                    machineModel.setServiceCallStartDate(object1.getString("ServiceCallStartDate"));
                                    machineModel.setServiceCallEndDate(object1.getString("ServiceCallEndDate"));
                                    machineModel.setParameterValue(object1.getString("ParameterValue"));
                                    machineModel.setRemark(object1.getString("Remark"));
                                    machineModel. setCreatedBy(object1.getString("CreatedBy"));
                                    machineModel.setCreatedDate(object1.getString("CreatedDate"));
                                    model.add(machineModel);

                                    //DataBaseHelper helper=new DataBaseHelper(Falco_SWAS_Main_Activity.this);
                                    SQLiteDatabase db = helper.getWritableDatabase();
                                    ContentValues cv=new ContentValues();
                                    cv.put("MachineParameterLogID",object1.getString("MachineParameterLogID"));
                                    cv.put("OEMID",object1.getString("OEMID"));
                                    cv.put("CustomerID",object1.getString("CustomerID"));
                                    cv.put("ServiceCallID",object1.getString("ServiceCallID"));
                                    cv.put("ServiceCallStartDate",object1.getString("ServiceCallStartDate"));
                                    cv.put("ServiceCallEndDate",object1.getString("ServiceCallEndDate"));
                                    cv.put("MachineParameters",object1.getString("MachineParameters"));
                                    cv.put("ParameterValue",object1.getString("ParameterValue"));
                                    cv.put("Remark",object1.getString("Remark"));
                                    cv.put("CreatedBy",object1.getString("CreatedBy"));
                                    cv.put("CreatedDate",object1.getString("CreatedDate"));
                                    String q = "SELECT * FROM "+Constants.TABLE_MACHINE_PARAMETER_COMPLTEDC_CALLS+" where "+Constants.MACHINE_PARAMETER_COMPLTEDC_CALLS_MACHINE_PARAMETER_LOG_ID+"='"+object1.getString("MachineParameterLogID")+"'";
                                    Cursor c = db.rawQuery(q,null);
                                    if(c.moveToFirst())
                                    {
                                        //showMessage("Error", "Record exist");
                                    }
                                    else
                                    {
                                        long d=db.insert(Constants.TABLE_MACHINE_PARAMETER_COMPLTEDC_CALLS,null,cv);
                                        Log.d("user check", String.valueOf(d));
                                        if (d==-1){
                                            Log.d(TAG, "onResponse: "+"Data not Inserted");
                                        }else {
                                            Log.d(TAG, "onResponse: "+"Data Inserted");
                                        }
                                    }
                                }
                            }
                            else if (array.length() == 0) {

                            }
                        }
                    }


                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void CompetitorParameterDetailsInProcess() {
        API api = Retroconfig.swasretrofit().create(API.class);
        /*dateformat1 = "2020-01-20";*/
        Call<ResponseBody> call = api.getcompetitorsparameterslist();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            if (array.length() != 0) {
                                for (int i = 0; i < array.length(); i++) {
                                    SwasCompetitorParameterModel swasCompetitorParameterModel = new SwasCompetitorParameterModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    swasCompetitorParameterModel.setCompetitorsParameters(object1.getString("CompetitorsParameters"));
                                    swasCompetitorParameterModel.setCompetitorsParameterID(object1.getString("CompetitorsParameterID"));
                                    swasCompetitorParameterModel.setUnit(object1.getString("UNIT"));
                                    swasCompetitorParameterModel.setRemark("NA");

                                    //DataBaseHelper helper=new DataBaseHelper(Falco_SWAS_Main_Activity.this);
                                    SQLiteDatabase db = helper.getWritableDatabase();
                                    ContentValues cv=new ContentValues();
                                    cv.put("CompetitorsParameterID",object1.getString("CompetitorsParameterID"));
                                    cv.put("CompetitorsParameters",object1.getString("CompetitorsParameters"));
                                    cv.put("UNIT",object1.getString("UNIT"));
                                    String id = object1.getString("CompetitorsParameterID");
                                    String q = "SELECT * FROM "+Constants.TABLE_COMPITITIVE_MACHINE_PARAMS_IN_PROCESS_CALL+" where "+Constants.COMPITITIVE_MACHINE_PARAMS_IN_PROCESS_CALL_COMP_PARAMS_ID+"='"+id+"'";
                                    Cursor c = db.rawQuery(q,null);
                                    if(c.moveToFirst())
                                    {
                                        //showMessage("Error", "Record exist");
                                    }
                                    else
                                    {
                                        long d=db.insert(Constants.TABLE_COMPITITIVE_MACHINE_PARAMS_IN_PROCESS_CALL,null,cv);
                                        Log.d("user check", String.valueOf(d));
                                        if (d==-1){
                                            Log.d(TAG, "onResponse: "+"Data not Inserted");
                                        }else {
                                            Log.d(TAG, "onResponse: "+"Data Inserted");
                                        }
                                    }
                                }
                            } else if (array.length() == 0) {
                            }
                        }
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    private void GetDocuments(String service_call_id) {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getreportdocumentdetails(service_call_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {

                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            documentModelArrayList.clear();
                            if (array.length() != 0) {
                                for (int i = 0;i<array.length();i++)
                                {
                                    DocumentModel model1 = new DocumentModel();
                                    JSONObject obj = array.getJSONObject(i);
                                    model1.setReportDocumentID(obj.getString("ReportDocumentID"));
                                    model1.setOEMID(obj.getString("OEMID"));
                                    model1.setCustomerID(obj.getString("CustomerID"));
                                    model1.setServiceCallID(obj.getString("ServiceCallID"));
                                    model1.setDocumentPath(obj.getString("DocumentPath"));
                                    model1.setDocumentName(obj.getString("DocumentName"));
                                    model1.setIsActive(obj.getString("IsActive"));
                                    model1.setCreatedBy(obj.getString("CreatedBy"));
                                    model1.setCreatedDate(obj.getString("CreatedDate"));
                                    documentModelArrayList.add(model1);

                                    //DataBaseHelper helper=new DataBaseHelper(Falco_SWAS_Main_Activity.this);
                                    SQLiteDatabase db = helper.getWritableDatabase();
                                    ContentValues cv=new ContentValues();
                                    cv.put("ReportDocumentID",obj.getString("ReportDocumentID"));
                                    cv.put("OEMID",obj.getString("OEMID"));
                                    cv.put("CustomerID",obj.getString("CustomerID"));
                                    cv.put("ServiceCallID",obj.getString("ServiceCallID"));
                                    cv.put("DocumentPath",obj.getString("DocumentPath"));
                                    cv.put("DocumentName",obj.getString("DocumentName"));
                                    cv.put("IsActive",obj.getString("IsActive"));
                                    cv.put("CreatedBy",obj.getString("CreatedBy"));
                                    cv.put("CreatedDate",obj.getString("CreatedDate"));
                                    cv.put("IsSync",1);
                                    String id = obj.getString("ReportDocumentID");
                                    String q = "SELECT * FROM "+Constants.TABLE_DOCUMENTS+" where "+Constants.DOCUMENTS_REPORT_DOCUMENTS_ID+"='"+id+"'";
                                    Cursor c = db.rawQuery(q,null);
                                    if(c.moveToFirst())
                                    {
                                        //showMessage("Error", "Record exist");
                                    }
                                    else
                                    {
                                        long d=db.insert(Constants.TABLE_DOCUMENTS,null,cv);
                                        Log.d("user check", String.valueOf(d));
                                        if (d==-1){
                                            Log.d(TAG, "onResponse: "+"Data not Inserted");
                                        }else {
                                            Log.d(TAG, "onResponse: "+"Data Inserted");
                                        }
                                    }
                                }

                            }
                            else if (array.length() == 0) {
                            }
                        }

                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void setData() {
        //DataBaseHelper dbHelper = new DataBaseHelper(Falco_SWAS_Main_Activity.this);
        SQLiteDatabase database = helper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();
                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    service_eng_id = cursor.getString(cursor.getColumnIndex(Constants.USER_LOGIN_ID));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout() {
        finish();
        Toast toast= Toast.makeText(Falco_SWAS_Main_Activity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        //DataBaseHelper dbHelper = new DataBaseHelper(Falco_SWAS_Main_Activity.this);
        SQLiteDatabase database = helper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);

        //Preferences.Logout();

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(Falco_SWAS_Main_Activity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);
        setData();
        // Set content width and height
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(Falco_SWAS_Main_Activity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id==R.id.nav_planning)
        {
            Intent i=new Intent(Falco_SWAS_Main_Activity.this, FalcoJKHomeActivity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    public String getOEMIDofUser(){
        //DataBaseHelper dbHelper = new DataBaseHelper(Falco_SWAS_Main_Activity.this);
        SQLiteDatabase database = helper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);
        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    oem_id= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
        return oem_id;
    }

    public class SaveVisitLogDetails extends AsyncTask<String,String,String> {

        JSONArray jsonArray;

        public SaveVisitLogDetails(JSONArray jsonArray) {
            this.jsonArray = jsonArray;
        }

        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Falco_SWAS_Main_Activity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String getResponse="";
            String url= Retroconfig.BASEURL_SWAS + "insertvisitlogdetails";
            String jsonarraystring = jsonArray.toString();
            Log.d(TAG, "doInBackground: ");
            try {
                HttpParams httpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(httpParams, 60000);
                HttpConnectionParams.setSoTimeout(httpParams, 60000);
                HttpClient client = new DefaultHttpClient(httpParams);
                HttpPost httpPost = new HttpPost(url);

                httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");

                httpPost.setEntity(new StringEntity(jsonArray.toString(), "UTF-8"));
                httpPost.setParams(httpParams);

                HttpResponse response = client.execute(httpPost);
                StatusLine statusLine = response.getStatusLine();

                if (statusLine.getStatusCode() == HttpStatus.SC_OK || statusLine.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();

                    getResponse = convertStreamToString(inputStream);
                } else if (statusLine.getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();

                    getResponse = convertStreamToString(inputStream);
                } else {
                    response.getEntity().getContent().close();
                }
            }
            catch (Exception e) {
                Log.i(URL_Constants.TAG, "Error Message in Exception :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_EXCEPTION;

            }
            return getResponse;
        }


        @Override
        protected void onPostExecute(String getResponse) {
            super.onPostExecute(getResponse);
            progressDialog.dismiss();
            if (!getResponse.equals("Error"))
            {
                Toast.makeText(Falco_SWAS_Main_Activity.this, "Visit log details inserted successfully", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
