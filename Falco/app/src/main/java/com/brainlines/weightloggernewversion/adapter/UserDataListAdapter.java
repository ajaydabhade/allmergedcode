package com.brainlines.weightloggernewversion.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.listener.UserDataListener;
import com.brainlines.weightloggernewversion.model.UserModel;

import java.util.ArrayList;

public class UserDataListAdapter extends RecyclerView.Adapter<UserDataListAdapter.MyViewHolder> {

    Context context;
    private ArrayList<UserModel> userModelList = new ArrayList<>();
    UserDataListener listener;

    public UserDataListAdapter(Context context, ArrayList<UserModel> userModelList, UserDataListener listener) {
        this.context = context;
        this.userModelList = userModelList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public UserDataListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.rv_user_data_model_list,parent,false);
        return new UserDataListAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserDataListAdapter.MyViewHolder holder, int position) {
        if (userModelList.size() != 0){
            final UserModel model = userModelList.get(position);
            if (model != null){

                holder.txtModuleName.setText(model.getModuleName());

                holder.rl_UserData.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.sendUserData(model);
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return userModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtModuleName;
        RelativeLayout rl_UserData;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtModuleName = (itemView).findViewById(R.id.txt_moduleName);
            rl_UserData = (itemView).findViewById(R.id.rl_UserData);
        }
    }
}
