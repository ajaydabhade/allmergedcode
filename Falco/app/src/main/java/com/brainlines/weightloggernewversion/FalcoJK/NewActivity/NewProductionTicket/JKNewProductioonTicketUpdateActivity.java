package com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.AssetNoModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.EnterWIPDataModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.EquipTypeModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.OperationEquipModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.ProdTkOperationModel;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.AssetNoAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.EquipTypeSpinAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.OpearionEqupSpinAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.Model.NewMultipleSkuModel;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.Model.NewOperationAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.Model.NewOperationModel;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.Model.NewProductionTicketModel;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.Model.NewWIPModel;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.adapter.NewDisplayWipAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.adapter.NewMultipleSkuAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.listener.NewProductionProcessListener;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.listener.NewWipDisplayListener;
import com.brainlines.weightloggernewversion.FalcoJK.UrlConstants.JK_URL_Constants;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JKNewProductioonTicketUpdateActivity extends AppCompatActivity implements NewProductionProcessListener, NewWipDisplayListener, View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    TextView txt_up_ticket_no,txt_up_plant,txt_up_valuestream,txt_up_tk_filetype,txt_up_tk_qty,txt_sku_8,txt_sku_13,txt_up_startdate;
    TextView txt_up_enddate,txt_up_act_strat_date,txt_up_act_end_date,txt_up_Release_qty;
    ImageButton img_start_ticket,img_end_ticket,previous_nav,next_nav,view_sku_details,img_enter_wip;
    TextView txt_img_up_end_ticket,txt_up_img_starttk,txt_enter_wip;
    RecyclerView rv_update_pro_ticket;
    String str_tk_id,user_email,user_role,user_token,str_tk_no;
    ImageView img_back,img_userinfo,nav_view_img;
    DrawerLayout drawer;
    TextView txt_updated_griddate;
    RelativeLayout relativelayout_sku;

    RecyclerView rv_enterwipdetails;
    ArrayList<ProdTkOperationModel> ticketmodel= new ArrayList<>();
    ArrayList<EnterWIPDataModel> enterWIPDataModelArrayList = new ArrayList<>();
    AlertDialog alertDialog;

    //NewProductionProcessesAdapter newTicketListAdapter;
    NewOperationAdapter newTicketListAdapter;
    NewDisplayWipAdapter enterWipAdapter;
    ArrayList<OperationEquipModel> models=new ArrayList<>();
    ArrayList<EquipTypeModel>equipTypelist=new ArrayList<>();
    ArrayList<AssetNoModel>assetNoModels=new ArrayList<>();
    ArrayList<ProdTkOperationModel> getTicketList = new ArrayList<>();
    OpearionEqupSpinAdapter opearionEqupSpinAdapter;
    EquipTypeSpinAdapter equipTypeSpinAdapter;
    AssetNoAdapter assetNoAdapter;
    Spinner spin_equipment, spin_equip_type,spin_select_operation,spin_spin_asset_no;
    String xmlData,jsonstring;
    String date1,date,psku ,start_date,end_date;
    BottomSheetDialog mBottomSheetDialog;
    private String str_valuestream_code,str_ticketIDNo,str_plant,te_NoofShipt,cRRemark;
    int str_operation_no;
    private String te_ReseasenDt,str_equip_name;
    private String str_plant_code;
    private String str_eqip_id;
    private String te_AssetNo;
    //SwipeRefreshLayout swipeRefreshLayout;
    String lastitemokqty,okqty,lastitemtotalqty;
    ImageButton img_btn_refresh;
    String flag="IsFirst";
    String dateformat1 = "";
    Calendar c = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    private TextView profile_user,profile_role;
    String str_validate_wip,str_validate_okqty,str_validate_rejectqty,str_validate_secondqty,str_validate_total_qty;
    RecyclerView rv_sku_details;
    TextView txt_first_opr_qty;
    NewMultipleSkuAdapter sku_adapter;
    NewProductionTicketModel model;
    ArrayList<NewMultipleSkuModel> newMultipleSkuModelArrayList = new ArrayList<>();
    ArrayList<NewWIPModel> newWIPModelArrayList = new ArrayList<>();
    //ArrayList<NewDateWiseProductionTicketProcessList> newDateWiseProductionTicketProcessListArrayList = new ArrayList<>();
    ArrayList<NewOperationModel> newDateWiseProductionTicketProcessListArrayList = new ArrayList<>();
    DataBaseHelper helper;
    SQLiteDatabase db;
    String strticketNo;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_production_ticket_update_navigation);
        helper = new DataBaseHelper(JKNewProductioonTicketUpdateActivity.this);
        db = helper.getWritableDatabase();
        initUi();
        setData();
        txt_sku_8.setText(psku);
        date = sdf.format(c.getTime());
        txt_updated_griddate.setText(date);
        dateformat1 = sdf.format(c.getTime());

    }

    public void initUi()
    {
        model = getIntent().getParcelableExtra("model");
        assert model != null;
        txt_up_ticket_no = findViewById(R.id.txt_up_ticket_no);
        txt_up_plant = findViewById(R.id.txt_up_plant);
        txt_up_valuestream = findViewById(R.id.txt_up_valuestream);
        txt_up_tk_filetype = findViewById(R.id.txt_up_tk_filetype);
        txt_up_tk_qty = findViewById(R.id.txt_up_tk_qty);
        txt_sku_8 = findViewById(R.id.txt_sku_8);
        txt_up_startdate = findViewById(R.id.txt_up_startdate);
        txt_up_enddate = findViewById(R.id.txt_up_enddate);
        txt_up_act_strat_date = findViewById(R.id.txt_up_act_strat_date);
        txt_up_act_end_date = findViewById(R.id.txt_up_act_end_date);
        txt_img_up_end_ticket = findViewById(R.id.txt_img_up_end_ticket);
        txt_up_img_starttk = findViewById(R.id.txt_up_img_starttk);
        txt_enter_wip = findViewById(R.id.txt_enter_wip);
        txt_up_Release_qty = findViewById(R.id.txt_up_Release_qty);

        txt_up_ticket_no.setText(model.getPtkd_TkNo());
        txt_up_plant.setText(model.getPtkd_Plant());
        txt_up_valuestream.setText(model.getPtkd_ValueStreamCode());
        txt_up_tk_filetype.setText(model.getPtkd_Ftypecode());
        txt_up_tk_qty.setText(model.getPtkd_Qty());
        txt_sku_8.setText(model.getPtkd_PSKU());
        txt_up_startdate.setText(model.getTk_Actual_Start_Date());
        txt_up_enddate.setText(model.getTk_Actual_End_Date());

        strticketNo = model.getPtkd_TkNo();

        img_start_ticket = findViewById(R.id.img_start_ticket);
        img_end_ticket = findViewById(R.id.img_end_ticket);
        img_enter_wip = findViewById(R.id.img_enter_wip);

        rv_update_pro_ticket = findViewById(R.id.rv_prod_view_ticket);
        rv_sku_details = findViewById(R.id.rv_sku_details);

        relativelayout_sku=findViewById(R.id.relativelayout_sku);
        previous_nav = findViewById(R.id.previous_nav);
        next_nav = findViewById(R.id.next_nav);
        txt_first_opr_qty =  findViewById(R.id.txt_first_opr_qty);

        txt_updated_griddate = findViewById(R.id.txt_updated_griddate);
        img_btn_refresh = findViewById(R.id.img_btn_refresh);

        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img = findViewById(R.id.nav_view_img);

        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i=new Intent(JKNewProductioonTicketUpdateActivity.this, Falco_Home_Main_Activity.class);
//                startActivity(i);
            }
        });

        img_userinfo = findViewById(R.id.img_userinfo);
        img_userinfo.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_swas).setVisible(false);
        view_sku_details = (ImageButton)findViewById(R.id.view_sku_details);


        txt_enter_wip.setOnClickListener(this);
        img_enter_wip.setOnClickListener(this);
        img_start_ticket.setOnClickListener(this);
        txt_img_up_end_ticket.setOnClickListener(this);
        img_end_ticket.setOnClickListener(this);
        txt_img_up_end_ticket.setOnClickListener(this);
        view_sku_details.setOnClickListener(this);
        GetMultipleSku(strticketNo);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(JKNewProductioonTicketUpdateActivity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);
        setData();
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });
        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(JKNewProductioonTicketUpdateActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }

    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(JKNewProductioonTicketUpdateActivity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();

                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));


                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout()
    {
        finish();
        Toast toast= Toast.makeText(JKNewProductioonTicketUpdateActivity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(JKNewProductioonTicketUpdateActivity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);

        //Preferences.Logout();

    }

    @Override
    public void onClick(View v) {
        if (v.getId()==img_enter_wip.getId())
        {
            displayEnterWIp();
        }
        else if (v.getId()==txt_enter_wip.getId())
        {
            displayEnterWIp();
        }
        else if (v.getId()==view_sku_details.getId())
        {
            if(relativelayout_sku.getVisibility()==View.GONE) {
                relativelayout_sku.setVisibility(View.VISIBLE);
            }
            else
            {
                relativelayout_sku.setVisibility(View.GONE);
            }
        }
        else if (v.getId()==img_start_ticket.getId())
        {
            String str_tk_release_date = sdf.format(c.getTime());
            String[] string =str_tk_release_date.split(" ");
            String main_string_start_date=string[0];
            txt_up_act_strat_date.setText(main_string_start_date);
            helper.updateStartdateinDatabase(str_tk_no,main_string_start_date);
            img_start_ticket.setVisibility(View.INVISIBLE);
            txt_up_img_starttk.setVisibility(View.INVISIBLE);
            alloperationgridlist();

        }
        else if (v.getId()==img_end_ticket.getId())
        {
            String str_tk_release_date = sdf.format(c.getTime());
            String[] string =str_tk_release_date.split(" ");
            String main_string_start_date=string[0];
            txt_up_act_strat_date.setText(main_string_start_date);
            helper.updateEnddateinDatabase(str_tk_no,main_string_start_date);
            img_end_ticket.setVisibility(View.INVISIBLE);
            txt_img_up_end_ticket.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    private void GetMultipleSku(String strtkno)
    {
        Cursor cursormultiplesku = helper.getMultipleSku(strtkno);
        while (cursormultiplesku.moveToNext())
        {
            NewMultipleSkuModel model = new NewMultipleSkuModel();
            model.setPtkd_TkNo(cursormultiplesku.getString(0));
            model.setSKU_No(cursormultiplesku.getString(1));
            model.setDp_stamp_type(cursormultiplesku.getString(2));
            model.setDp_stamp_chart(cursormultiplesku.getString(3));
            model.setDp_blko_flg(cursormultiplesku.getString(4));
            model.setDp_tang_color(cursormultiplesku.getString(5));
            model.setQTY(cursormultiplesku.getString(6));
            model.setOrgQTY(cursormultiplesku.getString(7));
            model.setCreatedby(cursormultiplesku.getString(8));
            model.setCreateddate(cursormultiplesku.getString(9));
            newMultipleSkuModelArrayList.add(model);
        }
        LinearLayoutManager manager = new LinearLayoutManager(JKNewProductioonTicketUpdateActivity.this);
        rv_sku_details.setLayoutManager(manager);
        sku_adapter = new NewMultipleSkuAdapter(JKNewProductioonTicketUpdateActivity.this,newMultipleSkuModelArrayList);
        rv_sku_details.setAdapter(sku_adapter);

    }

    private  void DisplayEnterWip()
    {
        Cursor cursorwip = helper.displayenterWIP(strticketNo);
        while(cursorwip.moveToNext())
        {
            NewWIPModel model = new NewWIPModel();
            model.setPtkd_Id(cursorwip.getString(0));
            model.setPtkd_PktId(cursorwip.getString(1));
            model.setPtkd_TkNo(cursorwip.getString(2));
            model.setPtkd_PSKU(cursorwip.getString(3));
            model.setPtkd_Plant(cursorwip.getString(4));
            model.setPtkd_ValueStreamCode(cursorwip.getString(5));
            model.setPtkd_Operation(cursorwip.getString(6));
            model.setPtkd_WIP(cursorwip.getString(7));
            model.setOpr_operation_name(cursorwip.getString(8));
            model.setUni_unit_name(cursorwip.getString(9));
            model.setPtkd_Qty(cursorwip.getString(10));
            model.setCreatedby(cursorwip.getString(11));
            model.setCreateddate(cursorwip.getString(12));
            newWIPModelArrayList.add(model);
        }
        LinearLayoutManager manager = new LinearLayoutManager(JKNewProductioonTicketUpdateActivity.this);
        rv_enterwipdetails.setLayoutManager(manager);
        enterWipAdapter = new NewDisplayWipAdapter(JKNewProductioonTicketUpdateActivity.this , newWIPModelArrayList,JKNewProductioonTicketUpdateActivity.this);
        rv_enterwipdetails.setAdapter(enterWipAdapter);

    }
    private void displayEnterWIp() {

        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_enter_wip_data_layout, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        ImageButton alert_button= dialogView.findViewById(R.id.btn_wip_save);
        ImageButton alert_cancel= dialogView.findViewById(R.id.btn_wip_cancel);
        rv_enterwipdetails= dialogView.findViewById(R.id.rv_wip_details_Data);

        DisplayEnterWip();

        alert_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContentValues cv = new ContentValues();
                for (int i = 0;i<newWIPModelArrayList.size();i++)
                {
                    cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_PACKET_ID,newWIPModelArrayList.get(i).getPtkd_PktId());
                    cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_TICKET_NO,newWIPModelArrayList.get(i).getPtkd_TkNo());
                    cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_PLANT,newWIPModelArrayList.get(i).getPtkd_Plant());
                    cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_VALUE_STREAM_CODE,newWIPModelArrayList.get(i).getPtkd_ValueStreamCode());
                    cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_OPERATION,newWIPModelArrayList.get(i).getPtkd_Operation());
                    cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_UPDATEDATE,"");
                    cv.put(Constants.PRODUCTION_TICKET_DETAILS_OPERATION_NAME,newWIPModelArrayList.get(i).getOpr_operation_name());
                    cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_QTY,newWIPModelArrayList.get(i).getPtkd_Qty());
                    cv.put(Constants.PRODUCTION_TICKET_DETAILS_UNIT_NAME,newWIPModelArrayList.get(i).getUni_unit_name());
                    cv.put(Constants.PRODUCTION_TICKET_DETAILS_VALUESTREAM_NAME,newWIPModelArrayList.get(i).getPtkd_ValueStreamCode());
                    cv.put(Constants.PRODUCTION_TICKET_DETAILS_TICKET_ACTUAL_START_DATE,"");
                    cv.put(Constants.PRODUCTION_TICKET_DETAILS_TICKET_ACTUAL_END_DATE,"");
                    cv.put(Constants.PRODUCTION_TICKET_DETAILS_TICKET_START_DATE,model.getTk_Actual_Start_Date());
                    cv.put(Constants.PRODUCTION_TICKET_DETAILS_TICKET_END_DATE,model.getTk_Actual_End_Date());
                    cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_CHANGE_ROUTE_FLAG,"");
                    cv.put(Constants.PRODUCTION_TICKET_DETAILS_CREATED_BY,"");
                    cv.put(Constants.PRODUCTION_TICKET_DETAILS_CREATED_DATE,"");
                    cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_WIP_QTY,newWIPModelArrayList.get(i).getPtkd_WIP());

                    long d=db.insert(Constants.PRODUCTION_TICKET_DETAILS_TABLE,null,cv);
                    Log.d("user check", String.valueOf(d));
                    alertDialog.dismiss();
                    txt_enter_wip.setVisibility(View.INVISIBLE);
                    img_enter_wip.setVisibility(View.INVISIBLE);
                }

                //new JK_ProductionTicket_Update_Actvity.SaveEnterWipDataCall().execute();
            }
        });
        alertDialog = builder.create();

        alertDialog.show();
        alertDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

        alert_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    private void alloperationgridlist()
    {
        Cursor cursorwip = helper.processessListasPerTicketNo(strticketNo);
        newDateWiseProductionTicketProcessListArrayList.clear();
        while(cursorwip.moveToNext())
        {
            //NewDateWiseProductionTicketProcessList model = new NewDateWiseProductionTicketProcessList();
            NewOperationModel model = new NewOperationModel();
            model.setPtk_PtkdId(cursorwip.getInt(0));
            model.setPtk_TkNo(cursorwip.getString(1));
            model.setPtkd_Plant(cursorwip.getString(2));
            model.setPtkd_ValueStreamCode(cursorwip.getString(3));
            model.setPtk_Operation(cursorwip.getInt(4));
            model.setPtk_UpdateDate(cursorwip.getString(5));
            model.setPtk_OkQty(cursorwip.getInt(6));
            model.setPtk_RejectQty(cursorwip.getInt(7));
            model.setPtk_ReworkQty(cursorwip.getInt(8));
            model.setPtk_WIPQty(cursorwip.getInt(9));
            model.setPtk_SecondQty(cursorwip.getInt(10));
            model.setOpr_operation_name(cursorwip.getString(11));
            model.setPtkd_Qty(cursorwip.getInt(12));
            model.setUni_unit_name(cursorwip.getString(13));
            model.setVal_valuestream_name(cursorwip.getString(14));
            model.setTk_Actual_Start_Date(cursorwip.getString(15));
            model.setTk_Actual_End_Date(cursorwip.getString(16));
            model.setTk_Start_Date(cursorwip.getString(17));
            model.setTk_End_Date(cursorwip.getString(18));
            model.setPtk_ChangeRoute_Flag(cursorwip.getString(19));
            model.setCreatedby(cursorwip.getString(20));
            model.setCreateddate(cursorwip.getString(21));
            newDateWiseProductionTicketProcessListArrayList.add(model);
        }
        LinearLayoutManager manager = new LinearLayoutManager(JKNewProductioonTicketUpdateActivity.this);
        rv_update_pro_ticket.setLayoutManager(manager);
        newTicketListAdapter = new NewOperationAdapter(JKNewProductioonTicketUpdateActivity.this,newDateWiseProductionTicketProcessListArrayList,JKNewProductioonTicketUpdateActivity.this,str_plant_code);
        rv_update_pro_ticket.setAdapter(newTicketListAdapter);
    }


    @Override
    public void getWIPData(NewWIPModel model) {

    }


    @Override
    public void imgRouteIsClicked(boolean flag, NewOperationModel model) {

    }

    @Override
    public void imgSubmitButtonIsClicked(ArrayList<NewOperationModel> ticketList) {

        ArrayList<NewOperationModel> list = ticketList;
        String s = list.toString();
        Log.d("List", "imgSubmitButtonIsClicked: "+list.toString());

        helper.deletedatafromProductionProcesses(strticketNo);

        for (int i = 0;i<ticketList.size();i++)
        {
            ContentValues cv = new ContentValues();
            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_PACKET_ID,ticketList.get(i).getPtk_PtkdId());
            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_TICKET_NO,ticketList.get(i).getPtk_TkNo());
            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_PLANT,ticketList.get(i).getPtkd_Plant());
            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_VALUE_STREAM_CODE,ticketList.get(i).getPtkd_ValueStreamCode());
            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_OPERATION,ticketList.get(i).getPtk_Operation());
            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_UPDATEDATE,sdf.format(c.getTime()));
            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_OK_QTY,ticketList.get(i).getPtk_OkQty_ans());
            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_REJECT_QTY,ticketList.get(i).getPtk_RejectQty_ans());
            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_REWORK_QTY,ticketList.get(i).getPtk_ReworkQty_ans());
            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_WIP_QTY,ticketList.get(i).getPtk_WIPQty_ans());
            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_SECOND_QTY,ticketList.get(i).getPtk_SecondQty_ans());
            cv.put(Constants.PRODUCTION_TICKET_DETAILS_OPERATION_NAME,ticketList.get(i).getOpr_operation_name());
            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_QTY,ticketList.get(i).getPtkd_Qty());
            cv.put(Constants.PRODUCTION_TICKET_DETAILS_UNIT_NAME,ticketList.get(i).getUni_unit_name());
            cv.put(Constants.PRODUCTION_TICKET_DETAILS_VALUESTREAM_NAME,ticketList.get(i).getVal_valuestream_name());
            cv.put(Constants.PRODUCTION_TICKET_DETAILS_TICKET_ACTUAL_START_DATE,ticketList.get(i).getTk_Actual_Start_Date());
            cv.put(Constants.PRODUCTION_TICKET_DETAILS_TICKET_ACTUAL_END_DATE,ticketList.get(i).getTk_Actual_End_Date());
            cv.put(Constants.PRODUCTION_TICKET_DETAILS_TICKET_START_DATE,ticketList.get(i).getTk_Start_Date());
            cv.put(Constants.PRODUCTION_TICKET_DETAILS_TICKET_END_DATE,ticketList.get(i).getTk_End_Date());
            cv.put(Constants.PRODUCTION_TICKET_DETAILS_PTKD_CHANGE_ROUTE_FLAG,ticketList.get(i).getPtk_ChangeRoute_Flag());
            cv.put(Constants.PRODUCTION_TICKET_DETAILS_CREATED_BY,ticketList.get(i).getCreatedby());
            cv.put(Constants.PRODUCTION_TICKET_DETAILS_CREATED_DATE,ticketList.get(i).getCreateddate());

            long d=db.insert(Constants.PRODUCTION_TICKET_DETAILS_TABLE,null,cv);
            Log.d("user check", String.valueOf(d));
        }
        alloperationgridlist();


        Log.d("List", "imgSubmitButtonIsClicked: "+list.toString());
        StringBuilder stringBuilder=new StringBuilder();
        stringBuilder.append('[');
        for (int i = 0 ; i < list.size() ; i++){

            stringBuilder.append('{' + "ptk_PtkdId" + ':' + '"').append(list.get(i).getPtk_PtkdId()).append('"').append(',');
            stringBuilder.append("ptk_TkNo" + ':' + '"').append(list.get(i).getPtkd_TkNo()).append('"').append(',');
            stringBuilder.append("ptk_Plant" + ':' + '"').append(list.get(i).getPtkd_Plant()).append('"').append(',');
            stringBuilder.append("ptk_Operation" + ':' + '"').append(list.get(i).getPtkd_Operation()).append('"').append(',');
            stringBuilder.append("ptk_UpdateDate"+':'+'"' + date +'"'+',');
            if (list.get(i).getPtk_OkQty_ans()==null){
                stringBuilder.append("ptk_OkQty" + ':' + '"').append(list.get(i).getPtk_OkQty()).append('"').append(',');
            }else {
                stringBuilder.append("ptk_OkQty" + ':' + '"').append(list.get(i).getPtk_OkQty_ans()).append('"').append(',');
            }
            stringBuilder.append("ptk_ActualPlant" + ':' + '"').append(list.get(i).getPtkd_Plant()).append('"').append(',');
            if (list.get(i).getPtk_ReworkQty_ans()==null){

                stringBuilder.append("ptk_ReworkQty" + ':' + '"').append(list.get(i).getPtk_ReworkQty()).append('"').append(',');
            }else {
                stringBuilder.append("ptk_ReworkQty" + ':' + '"').append(list.get(i).getPtk_ReworkQty_ans()).append('"').append(',');
            }

            if (list.get(i).getPtk_RejectQty_ans()==null){

                stringBuilder.append("ptk_RejectQty" + ':' + '"').append(list.get(i).getPtk_RejectQty()).append('"').append(',');
            }else {
                stringBuilder.append("ptk_RejectQty" + ':' + '"').append(list.get(i).getPtk_RejectQty_ans()).append('"').append(',');
            }
            if (list.get(i).getPtk_WIPQty_ans()==null){

                stringBuilder.append("ptk_WIPQty" + ':' + '"').append(list.get(i).getPtk_WIPQty()).append('"').append(',');
            }
            else {
                stringBuilder.append("ptk_WIPQty" + ':' + '"').append(list.get(i).getPtk_WIPQty_ans()).append('"').append(',');
            }
            if (list.get(i).getPtk_SecondQty_ans()==null){
                stringBuilder.append("ptk_SecondQty" + ':' + '"').append(list.get(i).getPtk_SecondQty()).append('"').append(',');
            }else {
                stringBuilder.append("ptk_SecondQty" + ':' + '"').append(list.get(i).getPtk_SecondQty_ans()).append('"').append(',');
            }
            stringBuilder.append("ptk_TotalQty" + ':' + '"').append(list.get(i).getPtkd_Qty()).append('"').append(',');
            stringBuilder.append("ptk_CreatedBy"+':'+'"' + JK_URL_Constants.PLANT_USER +'"'+',' );
            stringBuilder.append("ptk_CreatedDate"+':'+'"' + date + '"'+',');
            stringBuilder.append("ptk_ApprovedBy"+':'+'"' + JK_URL_Constants.PLANT_USER +'"'+',');
            stringBuilder.append("ptk_ApprovedDate"+':'+'"' + date +'"'+',');
            stringBuilder.append("ptk_Approve"+':'+'"' + "N" + '"'+',');
            stringBuilder.append("ptk_ValueStream" + ':' + '"').append(list.get(i).getVsCode()).append('"').append(',');
            stringBuilder.append("ptk_Active"+':'+ "1" +'}'+',');

            if (i==list.size() - 1)
            {
                lastitemokqty=list.get(i).getPtk_OkQty_ans();
                lastitemtotalqty = String.valueOf(list.get(i).getPtkd_Qty());

            }

        }

        stringBuilder.append(']');
        xmlData = stringBuilder.toString().replace("&", "&amp;").replace(" <", "<").replace("> ", ">");
        jsonstring=xmlData.substring(0, xmlData.length() - 2)+ ']';
        updateTicketDetails();

    }

    @Override
    public void getOkValue(String str_ok, String rejectqty, String secondqty, String str_wip) {

    }

    @Override
    public void checktotalokqty(String totalqty, String str_ok, String rejectqty, String secondqty, String str_wip) {

    }

    private void updateTicketDetails() {

        API api = Retroconfig.retrofit().create(API.class);
        Call<ResponseBody> call = api.updateData(jsonstring);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body().string()!=null)
                    {
                        String res = response.body().string();
                        Log.d("", "onResponse: "+res);
                        if (res!=null){
                            if (res.contains("")){
                                Toast.makeText(JKNewProductioonTicketUpdateActivity.this, "SuccessFully Inserted", Toast.LENGTH_SHORT).show();

                                String str=txt_up_tk_qty.getText().toString();
                                String str1[]=str.split(" ");
                                String qty=str1[0];
                                if (lastitemokqty!= null)
                                {
                                    if (lastitemokqty.concat(".0").equals(qty) || lastitemokqty.equals(lastitemtotalqty))
                                    {
                                        //new JK_ProductionTicket_Update_Actvity.EndProductionTicketCall().execute();
                                    }
                                    else if (lastitemokqty.concat(".0").equals(lastitemtotalqty)|| Integer.parseInt(lastitemokqty)>Integer.parseInt(lastitemtotalqty)) {
                                        //new JK_ProductionTicket_Update_Actvity.EndProductionTicketCall().execute();
                                    }
                                }
                                alloperationgridlist();
                            }
                        }else{

                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(JKNewProductioonTicketUpdateActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    public class EndProductionTicketCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JKNewProductioonTicketUpdateActivity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String url= JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.END_PROD_TICKET + str_tk_id ;
            response= URL_Constants.makeHttpPutRequest(url);
            Log.d(JK_URL_Constants.TAG, "Whole data = " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if(!response.equals("Error"))
            {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    String message=jsonObject.getString("message");
                    String status=jsonObject.getString("status");

                    if (message.equals("Success"))
                    {
                       /* Intent i=new Intent(JK_ProductionTicket_Update_Actvity.this,JK_Production_Selection_list.class);
                        startActivity(i);*/
                        alloperationgridlist();
                    }
                    else
                    {
                        Toast.makeText(JKNewProductioonTicketUpdateActivity.this, "This ticket is not Ended", Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }


            }

        }
    }

    public class StartProductionTicketDetails extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JKNewProductioonTicketUpdateActivity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String url= JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.START_PROD_TICKET_ID_DETAILS + str_tk_id + "&flag=" + "Startbtn";
            response= URL_Constants.makeHttpPostRequest(url);
            Log.d(JK_URL_Constants.TAG, "Whole data = " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();

            if (!response.equals("Error"))
            {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray array=jsonObject.getJSONArray("Data");
                    ticketmodel.clear();
                    if (array.length()==0)
                    {
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }

                    else {

                        for (int i=0;i<array.length();i++) {
                            JSONObject obj = new JSONObject(array.get(i).toString());
                            ProdTkOperationModel model = new ProdTkOperationModel();

                            if (obj.has("tk_Actual_End_Date"))
                            {
                                model.setPtk_PtkdId(obj.getInt("ptkd_Id"));
                                model.setPtkd_TkNo(obj.getString("ptkd_TkNo"));
                                model.setPtkd_PSKU(obj.getString("ptkd_PSKU"));
                                model.setPtkd_Plant(obj.getString("ptkd_Plant"));
                                model.setPtkd_ValueStreamCode(obj.getString("ptkd_ValueStreamCode"));
                                model.setPtkd_Ftypecode(obj.getString("ptkd_Ftypecode"));
                                model.setPtkd_Operation(obj.getInt("ptkd_Operation"));
                                model.setPtkd_Qty(obj.getInt("ptkd_Qty"));
                                model.setPtkd_IsRelease(obj.getString("ptkd_IsRelease"));
                                model.setUni_unit_name(obj.getString("uni_unit_name"));
                                model.setOpr_operation_name(obj.getString("opr_operation_name"));
                                model.setVal_valuestream_name(obj.getString("val_valuestream_name"));
                                model.setFt_ftype_desc(obj.getString("ft_ftype_desc"));
                                model.setTk_Actual_Start_Date(obj.getString("tk_Actual_Start_Date"));
                                model.setTk_Actual_End_Date(obj.getString("tk_Actual_End_Date"));
                                model.setPtkd_ReleaseDate(obj.getString("ptkd_ReleaseDate"));
                                model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                                model.setTk_End_Date(obj.getString("tk_End_Date"));
                                model.setFlag(obj.getString("flag"));
                                model.setVsCode(obj.getString("ptkd_ValueStreamCode"));
                                model.setPtk_ChangeRoute_Flag("C");

                                if (obj.getString("tk_Actual_Start_Date") != null && !obj.getString("tk_Actual_Start_Date").equals("null"))
                                {
                                    img_start_ticket.setVisibility(View.INVISIBLE);
                                    txt_up_img_starttk.setVisibility(View.INVISIBLE);
                                    start_date = obj.getString("tk_Actual_Start_Date");
                                    img_enter_wip.setVisibility(View.INVISIBLE);
                                    txt_up_img_starttk.setVisibility(View.INVISIBLE);
                                    txt_enter_wip.setVisibility(View.INVISIBLE);
                                }
                                else {
                                    img_start_ticket.setVisibility(View.VISIBLE);
                                    txt_up_img_starttk.setVisibility(View.VISIBLE);
                                }

                                if (obj.getString("tk_Actual_End_Date") != null && !obj.getString("tk_Actual_End_Date").equals("null"))
                                {
                                    img_end_ticket.setVisibility(View.INVISIBLE);
                                    txt_img_up_end_ticket.setVisibility(View.INVISIBLE);
                                    end_date = obj.getString("tk_Actual_End_Date");
                                }
                                else {
                                    img_end_ticket.setVisibility(View.VISIBLE);
                                    txt_img_up_end_ticket.setVisibility(View.VISIBLE);
                                }

                                if(obj.getString("tk_Actual_Start_Date") != null &&!obj.getString("tk_Actual_Start_Date").equals("null"))
                                {
                                    txt_up_act_strat_date.setText(obj.getString("tk_Actual_Start_Date"));
                                }
                                else
                                {
                                    txt_up_act_strat_date.setText("NA");

                                }
                                if(!obj.getString("tk_Actual_End_Date").equals("null"))
                                {
                                    txt_up_act_end_date.setText(obj.getString("tk_Actual_End_Date"));
                                }
                                else
                                {
                                    txt_up_act_end_date.setText("NA");

                                }
                                txt_up_ticket_no.setText(obj.getString("ptkd_TkNo"));
                                txt_up_plant.setText(obj.getString("ptkd_Plant"));
                                txt_up_valuestream.setText(obj.getString("val_valuestream_name"));
                                txt_up_tk_filetype.setText(obj.getString("ft_ftype_desc"));
                                txt_sku_8.setText(obj.getString("ptkd_PSKU"));
                                txt_up_startdate.setText(obj.getString("tk_Start_Date"));
                                txt_up_enddate.setText(obj.getString("tk_End_Date"));
                                txt_up_tk_qty.setText(obj.getString("ptkd_Qty").concat(" no"));
                                txt_updated_griddate.setText(obj.getString("tk_Actual_Start_Date"));
                                ticketmodel.add(model);
                            }

                            else
                            {
                                model.setPtk_PtkdId(obj.getInt("ptkd_Id"));
                                model.setPtkd_TkNo(obj.getString("ptkd_TkNo"));
                                model.setPtkd_PSKU(obj.getString("ptkd_PSKU"));
                                model.setPtkd_Plant(obj.getString("ptkd_Plant"));
                                model.setPtkd_ValueStreamCode(obj.getString("ptkd_ValueStreamCode"));
                                model.setPtkd_Ftypecode(obj.getString("ptkd_Ftypecode"));
                                model.setPtkd_Operation(obj.getInt("ptkd_Operation"));
                                model.setPtkd_Qty(obj.getInt("ptkd_Qty"));
                                model.setPtkd_IsRelease(obj.getString("ptkd_IsRelease"));
                                model.setUni_unit_name(obj.getString("uni_unit_name"));
                                model.setOpr_operation_name(obj.getString("opr_operation_name"));
                                model.setVal_valuestream_name(obj.getString("val_valuestream_name"));
                                model.setFt_ftype_desc(obj.getString("ft_ftype_desc"));
                                model.setTk_Actual_Start_Date(obj.getString("tk_Actual_Start_Date"));
                                model.setTk_Actual_End_Date("null");
                                model.setPtkd_ReleaseDate(obj.getString("ptkd_ReleaseDate"));
                                model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                                model.setTk_End_Date(obj.getString("tk_End_Date"));
                                model.setFlag(obj.getString("flag"));
                                model.setVsCode(obj.getString("ptkd_ValueStreamCode"));
                                model.setPtk_ChangeRoute_Flag("C");


                                if (obj.getString("tk_Actual_Start_Date") != null && !obj.getString("tk_Actual_Start_Date").equals("null"))
                                {
                                    img_start_ticket.setVisibility(View.INVISIBLE);
                                    txt_up_img_starttk.setVisibility(View.INVISIBLE);
                                    start_date = obj.getString("tk_Actual_Start_Date");
                                    img_enter_wip.setVisibility(View.INVISIBLE);
                                    txt_up_img_starttk.setVisibility(View.INVISIBLE);
                                    txt_enter_wip.setVisibility(View.INVISIBLE);
                                }
                                else {
                                    img_start_ticket.setVisibility(View.VISIBLE);
                                    txt_up_img_starttk.setVisibility(View.VISIBLE);
                                }
                                if(obj.getString("tk_Actual_Start_Date") != null &&!obj.getString("tk_Actual_Start_Date").equals("null"))
                                {
                                    txt_up_act_strat_date.setText(obj.getString("tk_Actual_Start_Date"));
                                }
                                else
                                {
                                    txt_up_act_strat_date.setText("NA");

                                }
                                txt_up_act_end_date.setText("NA");

                                txt_up_ticket_no.setText(obj.getString("ptkd_TkNo"));
                                txt_up_plant.setText(obj.getString("ptkd_Plant"));
                                txt_up_valuestream.setText(obj.getString("val_valuestream_name"));
                                txt_up_tk_filetype.setText(obj.getString("ft_ftype_desc"));
                                txt_sku_8.setText(obj.getString("ptkd_PSKU"));
                                txt_up_startdate.setText(obj.getString("tk_Start_Date"));
                                txt_up_enddate.setText(obj.getString("tk_End_Date"));
                                txt_up_tk_qty.setText(obj.getString("ptkd_Qty").concat(" no"));
                                txt_updated_griddate.setText(obj.getString("tk_Actual_Start_Date"));
                                ticketmodel.add(model);
                            }


                            //getViewData();
                            if (getTicketList.size()==0)
                            {
                               /* ArrayList<ProdTkOperationModel> list = ticketmodel;
                                LinearLayoutManager manager = new LinearLayoutManager(JKNewProductioonTicketUpdateActivity.this);
                                rv_update_pro_ticket.setLayoutManager(manager);
                                newTicketListAdapter = new NewTicketListAdapter(JKNewProductioonTicketUpdateActivity.this,ticketmodel,JKNewProductioonTicketUpdateActivity.this,str_plant_code);
                                rv_update_pro_ticket.setAdapter(newTicketListAdapter);*/
                            }
                        }


                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }}
    }
}
