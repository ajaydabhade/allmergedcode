package com.brainlines.weightloggernewversion.adapter.newtestadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.adapter.recalltestadapter.Record_paper_shift_reading_Adapter;

import java.util.ArrayList;

public class NewTestPaperShiftReadingAdapter extends RecyclerView.Adapter<NewTestPaperShiftReadingAdapter.MyViewHolder> {

    public String TAG = "WorkDetailsListener";
    Context context;
    ArrayList<String> paper_shif_reading_list = new ArrayList<>();


    public NewTestPaperShiftReadingAdapter(Context context, ArrayList<String> paper_shif_reading_list) {
        this.context = context;
        this.paper_shif_reading_list = paper_shif_reading_list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_paper_shift_reading, parent, false);
        return new NewTestPaperShiftReadingAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        String reading = paper_shif_reading_list.get(position);

        holder.txt_degree.setText(reading);


    }

    @Override
    public int getItemCount() {
        return paper_shif_reading_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_degree,txt_edu_details;
        ImageView img_remove_School;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_degree = (itemView).findViewById(R.id.txt_paper_shift_reading);

        }

    }
}
