package com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.listener;

import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.Model.NewProductionTicketModel;

public interface NewProductionTicketListener {
    void isupdateClicked(NewProductionTicketModel model);
    void isreleaseClicked(NewProductionTicketModel model, TextView txtreleasedate, ImageButton btnrelease, ImageButton btnupdate, ImageButton btnview, ImageView imagerelease);
    void isviewClicked(NewProductionTicketModel model);
}
