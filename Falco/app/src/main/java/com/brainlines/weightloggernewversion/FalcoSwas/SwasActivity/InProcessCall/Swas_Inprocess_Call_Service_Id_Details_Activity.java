package com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.InProcessCall;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Feedback_Activity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Home_Actvity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter.Chekc_List_Adapter;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter.FaultyCharacteristicsAdapter;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter.PartOrderBy_Customer_Adapter;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.Checklist_Model;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.FaultyCharactristics_Model;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.PartOrderByCustomerModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasIdDetailsModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Swas_Inprocess_Call_Service_Id_Details_Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    ImageButton btnFeedback;
    String status_id,service_call_id,type_of_service_status_id,customer_id,type_of_Service_call_id;
    Chekc_List_Adapter chekc_list_adapter;
    PartOrderBy_Customer_Adapter partOrderBy_customer_adapter;
    RecyclerView rv_faulty_characteristics;
    FaultyCharacteristicsAdapter faultyCharacteristicsAdapter;
    TextView txt_type_of_fault_text;
    String complaint= "";
    TextView txt_in_complaint,txt_type_in_service_call,txt_in_product,txt_dtls_call_created_by,txt_dtls_created_on,txt_dtls_modified_on;
    RecyclerView rv_details_part_order_by,rv_details_checklist;
    TextView txt_dtls_service_call_id;
    ImageView img_user_info,nav_view_img,img_back;
    String user_email,user_token,user_role;
    DrawerLayout drawer;
    TextView profile_user,profile_role;
    private String oem_id;
    DataBaseHelper helper;
    private ArrayList<Checklist_Model> serviceCallCheckListModels = new ArrayList<>();
    private ArrayList<PartOrderByCustomerModel> partOrderModels = new ArrayList<>();
    private ArrayList<FaultyCharactristics_Model> faultCharTextList = new ArrayList<>();
    private String service_eng_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swas_inprocess_id_details_navigation);
        initUi();
        setData();
        img_user_info=(ImageView)findViewById(R.id.img_userinfo);
        img_user_info.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img=(ImageView)findViewById(R.id.nav_view_img);
        img_back=(ImageView)findViewById(R.id.img_back);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_user.setText(user_email);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_planning).setVisible(false);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            status_id = bundle.getString("status_id");
            service_call_id = bundle.getString("service_call_id");
            type_of_service_status_id = bundle.getString("Typeofservicecall_id");
            customer_id = bundle.getString("customer_id");
        }

        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
//        if(isConnected) {
//            Log.d("Network", "Connected");
//            Id_Details();
//
//        }
//        else{
//            checkNetworkConnection();
//            Log.d("Network","Not Connected");
//            getDataFromListOfServiceCallByStatus();
//            getServiceCallCheckList();
//            getPartOrders();
//            getFaultCharas();
//        }
        getDataFromListOfServiceCallByStatus();
        getServiceCallCheckList();
        getPartOrders();
        getFaultCharas();

        btnFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Swas_Inprocess_Call_Service_Id_Details_Activity.this, Swas_Feedback_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_service_status_id);
                bundle.putString("customer_id",customer_id);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        txt_in_complaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view_detail_complaint(complaint);
            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Swas_Inprocess_Call_Service_Id_Details_Activity.this, Swas_Deatails_of_Inprocess_Call_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_service_status_id);
                bundle.putString("customer_id",customer_id);
                i.putExtras(bundle);
                startActivity(i);
                finish();
            }
        });

        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });
    }
    public void initUi() {
        helper = new DataBaseHelper(this);
        txt_type_in_service_call = (TextView)findViewById(R.id.txt_type_in_service_call);
        txt_in_product = (TextView)findViewById(R.id.txt_in_product);
        txt_in_complaint = (TextView)findViewById(R.id.txt_in_complaint);

        txt_dtls_service_call_id = (TextView)findViewById(R.id.txt_dtls_service_call_id);
        rv_details_part_order_by = (RecyclerView)findViewById(R.id.rv_details_part_order_by);
        rv_details_checklist = (RecyclerView)findViewById(R.id.rv_details_checklist);
        rv_faulty_characteristics = (RecyclerView)findViewById(R.id.rv_faulty_characteristics);
        txt_type_of_fault_text = (TextView)findViewById(R.id.txt_type_of_fault_text);
        //txt_dtls_modified_on = (TextView)findViewById(R.id.txt_dtls_modified_on);
        rv_details_checklist.setEnabled(false);
        rv_details_part_order_by.setEnabled(false);

        btnFeedback=findViewById(R.id.btn_dtls_feedback);
    }

    private void Id_Details() {
        API api = Retroconfig.swasretrofit().create(API.class);

        Call<ResponseBody> call = api.getalldetailsofsingleservicecall(service_call_id,status_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {

                        String res = response.body().string();
                        if (res != null) {

                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            ArrayList<SwasIdDetailsModel> model = new ArrayList<>();
                            if (array.length() != 0) {

                                for (int i = 0; i < array.length(); i++) {
                                    SwasIdDetailsModel swasIdDetailsModel = new SwasIdDetailsModel();
                                    JSONObject object1 = array.getJSONObject(i);

                                    swasIdDetailsModel.setService_Id(object1.getString("ServiceCallID"));
                                    swasIdDetailsModel.setLOCATIONNAME(object1.getString("LOCATIONNAME"));
                                    swasIdDetailsModel.setCreatedBy(object1.getString("CreatedBy"));
                                    swasIdDetailsModel.setCreatedDate(object1.getString("CreatedDate"));
                                    swasIdDetailsModel.setUpdatedBy(object1.getString("UpdatedBy"));
                                    swasIdDetailsModel.setUpdatedDate(object1.getString("UpdatedDate"));
                                    swasIdDetailsModel.setTypeOfServiceCallText(object1.getString("TypeOfServiceCallText"));
                                    swasIdDetailsModel.setNAME(object1.getString("NAME"));
                                    swasIdDetailsModel.setBriefComplaint(object1.getString("BriefComplaint"));
                                    swasIdDetailsModel.setTypeOfFaultText(object1.getString("TypeOfFaultText"));
                                    swasIdDetailsModel.setStatusText(object1.getString("StatusText"));
                                    swasIdDetailsModel.setCustomerID(object1.getString("CustomerID"));
                                    swasIdDetailsModel.setITEMID(object1.getString("ITEMID"));
                                    swasIdDetailsModel.setCURRENCY(object1.getString("CURRENCY"));
                                    swasIdDetailsModel.setTypeOfServiceCallID(object1.getString("TypeOfServiceCallID"));
                                    swasIdDetailsModel.setServiceEngineerName(object1.getString("ServiceEngineerName"));
                                    swasIdDetailsModel.setClosedDateTime(object1.getString("ClosedDateTime"));
                                    swasIdDetailsModel.setClosedBy(object1.getString("ClosedBy"));
                                    swasIdDetailsModel.setService_call_status_Id(object1.getString("ServiceCallStatusID"));
                                    swasIdDetailsModel.setComments(object1.getString("Comments"));
                                    swasIdDetailsModel.setServiceEngineerID(object1.getString("ServiceEngineerID"));
                                    swasIdDetailsModel.setNumberOfVisits(object1.getString("NumberOfVisits"));
                                    txt_type_of_fault_text.setText(object1.getString("TypeOfFaultText"));
                                    type_of_Service_call_id = object1.getString("TypeOfServiceCallID");

                                    txt_type_in_service_call.setText(object1.getString("TypeOfServiceCallText"));
                                    txt_in_product.setText(object1.getString("NAME"));
                                    service_call_id=object1.getString("ServiceCallID");
                                    complaint = object1.getString("BriefComplaint");
                                    customer_id = object1.getString("CustomerID");
                                    txt_dtls_service_call_id.setText(service_call_id);

                                }
                                GetChecklist();
                                GetPartOrderBY();
                                FaultyCharacteristicsList();
                            }
                            else if (array.length() == 0) {
                            }
                        }
                    }


                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void GetChecklist() {
        API api = Retroconfig.swasretrofit().create(API.class);
        //String status_id = "2";
        // service_call_id = "1";
        Call<ResponseBody> call = api.getchecklistofservicecall(service_call_id,type_of_Service_call_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                    }
                    String res = response.body().string();
                    if (res != null) {

                        JSONObject object = new JSONObject(res);
                        JSONArray array = object.getJSONArray("data");
                        ArrayList<Checklist_Model> checklist_models = new ArrayList<>();
                        if (array.length() != 0) {

                            for (int i = 0; i < array.length(); i++) {
                                Checklist_Model swasIdDetailsModel = new Checklist_Model();
                                Checklist_Model model = new Checklist_Model();
                                JSONObject obj = array.getJSONObject(i);
                                model.setCheck_id(obj.getString("ChecklistForSiteReadynessID"));
                                model.setCheck_name(obj.getString("ChecklistForSiteReadynessText"));
                                model.setChecked(obj.getString("Checked"));
                                checklist_models.add(model);

                            }
                            chekc_list_adapter = new Chekc_List_Adapter(Swas_Inprocess_Call_Service_Id_Details_Activity.this,checklist_models);
                            rv_details_checklist.setLayoutManager(new LinearLayoutManager(Swas_Inprocess_Call_Service_Id_Details_Activity.this,LinearLayoutManager.VERTICAL, false));
                            rv_details_checklist.setAdapter(chekc_list_adapter);


                        } else if (array.length() == 0) {
                        }
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void GetPartOrderBY(){
        API api = Retroconfig.swasretrofit().create(API.class);
      /*  String status_id = "33";
        String Customer_id = "C000698";*/
        Call<ResponseBody> call = api.getpartordered(service_call_id,customer_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                    }
                    String res = response.body().string();
                    if (res != null) {

                        JSONArray array = new JSONArray(res);
                        ArrayList<PartOrderByCustomerModel> partOrderByCustomerModelArrayList = new ArrayList<>();
                        if (array.length() != 0) {

                            for (int i = 0; i < array.length(); i++) {
                                PartOrderByCustomerModel model = new PartOrderByCustomerModel();
                                JSONObject obj = array.getJSONObject(i);
                                model.setApplicable(obj.getString("Applicable"));
                                model.setConsumables(obj.getString("Consumables"));
                                model.setCreatedBy(obj.getString("CreatedBy"));
                                model.setCreatedDate(obj.getString("CreatedDate"));
                                model.setCustomerID(obj.getString("CustomerID"));
                                model.setExcludeInclude(obj.getString("ExcludeInclude"));
                                model.setFinalPrice(obj.getString("FinalPrice"));
                                model.setITEMID(obj.getString("ITEMID"));
                                model.setMachineID(obj.getString("MachineID"));
                                model.setNAME(obj.getString("NAME"));
                                model.setPRICE(obj.getString("PRICE"));
                                model.setOEMID(obj.getString("OEMID"));
                                model.setQuantity(obj.getString("Quantity"));

                                partOrderByCustomerModelArrayList.add(model);
                            }
                            partOrderBy_customer_adapter = new PartOrderBy_Customer_Adapter(Swas_Inprocess_Call_Service_Id_Details_Activity.this,partOrderByCustomerModelArrayList);
                            rv_details_part_order_by.setLayoutManager(new LinearLayoutManager(Swas_Inprocess_Call_Service_Id_Details_Activity.this,LinearLayoutManager.VERTICAL, false));
                            rv_details_part_order_by.setAdapter(partOrderBy_customer_adapter);


                        } else if (array.length() == 0) {
                        }
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void FaultyCharacteristicsList() {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getfaultcharacteristicslist(service_call_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                    }
                    String res = response.body().string();
                    if (res != null) {

                        JSONObject object =  new JSONObject(res);
                        JSONArray array = object.getJSONArray("data");
                        ArrayList<FaultyCharactristics_Model> faultyCharactristics_modelArrayList = new ArrayList<>();
                        if (array.length() != 0) {

                            for (int i = 0; i < array.length(); i++) {
                                FaultyCharactristics_Model model = new FaultyCharactristics_Model();
                                JSONObject obj = array.getJSONObject(i);
                                model.setCustomerID(obj.getString("CustomerID"));
                                model.setServiceCallID(obj.getString("ServiceCallID"));
                                model.setFaultCharacteristicsText(obj.getString("FaultCharacteristicsText"));

                                faultyCharactristics_modelArrayList.add(model);
                            }
                            //relativeLayout1.setVisibility(View.VISIBLE);
                            faultyCharacteristicsAdapter = new FaultyCharacteristicsAdapter(Swas_Inprocess_Call_Service_Id_Details_Activity.this,faultyCharactristics_modelArrayList);
                            rv_faulty_characteristics.setLayoutManager(new LinearLayoutManager(Swas_Inprocess_Call_Service_Id_Details_Activity.this,LinearLayoutManager.VERTICAL, false));
                            rv_faulty_characteristics.setAdapter(faultyCharacteristicsAdapter);


                        } else if (array.length() == 0) {
                            //relativeLayout1.setVisibility(View.VISIBLE);
                        }
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void view_detail_complaint(String complaint) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.alert_view_detailed_complaint, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        TextView textView = (TextView)dialogView.findViewById(R.id.txt_details_complaint);
        textView.setText(complaint);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(Swas_Inprocess_Call_Service_Id_Details_Activity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);
        setData();
        // Set content width and height
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(Swas_Inprocess_Call_Service_Id_Details_Activity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }

    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(Swas_Inprocess_Call_Service_Id_Details_Activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();

                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    oem_id= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));
                    service_eng_id = cursor.getString(cursor.getColumnIndex(Constants.USER_LOGIN_ID));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));


                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout() {
        finish();
        Toast toast= Toast.makeText(Swas_Inprocess_Call_Service_Id_Details_Activity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(Swas_Inprocess_Call_Service_Id_Details_Activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);

        //Preferences.Logout();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();


        if (id==R.id.nav_swas)
        {
            Intent i=new Intent(Swas_Inprocess_Call_Service_Id_Details_Activity.this, Swas_Home_Actvity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    public void checkNetworkConnection(){
        AlertDialog.Builder builder =new AlertDialog.Builder(Swas_Inprocess_Call_Service_Id_Details_Activity.this);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i= new Intent(Swas_Inprocess_Call_Service_Id_Details_Activity.this,Swas_Deatails_of_Inprocess_Call_Activity.class);
        Bundle bundle = new Bundle();
        bundle.putString("service_call_id",service_call_id);
        bundle.putString("status_id",status_id);
        bundle.putString("Typeofservicecall_id",type_of_Service_call_id);
        bundle.putString("customer_id",customer_id);
        i.putExtras(bundle);
        startActivity(i);
        finish();
    }

    private void getDataFromListOfServiceCallByStatus() {
        Cursor cursor = helper.getDetailsDataFromServiceCallStatusTable(service_call_id);
        while (cursor.moveToNext()){
            String customer_name = cursor.getString(0);
            String created_By = cursor.getString(1);
            String created_on = cursor.getString(2);
            String last_modified_on = cursor.getString(3);
            String product = cursor.getString(4);
            complaint = cursor.getString(5);
            String type_of_fault_text= cursor.getString(7);
            String typeOfServiceCallText= cursor.getString(6);
//            String statusText= cursor.getString(8);
            txt_dtls_service_call_id.setText(service_call_id);
            txt_type_in_service_call.setText(typeOfServiceCallText);
            txt_in_product.setText(product);
            txt_type_of_fault_text.setText(type_of_fault_text);
//            txt_service_call_status_text.setText(statusText);
//            view_detail_complaint(complaint);

        }
    }
    private void getServiceCallCheckList() {
        serviceCallCheckListModels.clear();
        Cursor cursor = helper.getServiceCallCheckListAgainstServiceCallIDForDetails(service_call_id);
        while (cursor.moveToNext()){
            String checkListForSiteReadynessId = cursor.getString(2);
            String checkListForSiteReadynesstext = cursor.getString(3);
            String chcked = cursor.getString(4);
            Checklist_Model model = new Checklist_Model();
            model.setCheck_id(checkListForSiteReadynessId);
            model.setCheck_name(checkListForSiteReadynesstext);
            model.setChecked(chcked);
            serviceCallCheckListModels.add(model);
        }
        chekc_list_adapter = new Chekc_List_Adapter(Swas_Inprocess_Call_Service_Id_Details_Activity.this,serviceCallCheckListModels);
        rv_details_checklist.setLayoutManager(new LinearLayoutManager(Swas_Inprocess_Call_Service_Id_Details_Activity.this,LinearLayoutManager.VERTICAL, false));
        rv_details_checklist.setAdapter(chekc_list_adapter);
    }
    private void getPartOrders() {
        partOrderModels.clear();
        Cursor cursor = helper.getPartOrdersAgainstServiceCallIDForDetails(service_call_id);
        while(cursor.moveToNext()){
            String ServiceCallID = cursor.getString(1);
            String OEMID = cursor.getString(2);
            String customerId = cursor.getString(3);
            String itemId = cursor.getString(4);
            String quantity = cursor.getString(5);
            String name = cursor.getString(6);
            String price = cursor.getString(7);
            String applicable = cursor.getString(8);
            String finalPrice = cursor.getString(9);
            String excludeInclude = cursor.getString(10);
            String createdBy = cursor.getString(11);
            String createdDate = cursor.getString(12);
            PartOrderByCustomerModel model= new PartOrderByCustomerModel();
            model.setServiceCallID(ServiceCallID);
            model.setOEMID(OEMID);
            model.setCustomerID(customerId);
            model.setITEMID(itemId);
            model.setQuantity(quantity);
            model.setNAME(name);
            model.setPRICE(price);
            model.setApplicable(applicable);
            model.setFinalPrice(finalPrice);
            model.setExcludeInclude(excludeInclude);
            model.setCreatedBy(createdBy);
            model.setCreatedBy(createdDate);
            partOrderModels.add(model);

        }
        partOrderBy_customer_adapter = new PartOrderBy_Customer_Adapter(Swas_Inprocess_Call_Service_Id_Details_Activity.this,partOrderModels);
        rv_details_part_order_by.setLayoutManager(new LinearLayoutManager(Swas_Inprocess_Call_Service_Id_Details_Activity.this,LinearLayoutManager.VERTICAL, false));
        rv_details_part_order_by.setAdapter(partOrderBy_customer_adapter);
    }
    private void getFaultCharas() {
        faultCharTextList.clear();
        Cursor cursor = helper.getFaultCharList(service_call_id);
        while (cursor.moveToNext()){
            String customerId = cursor.getString(1);
            String serviceCallID = cursor.getString(2);
            String faultCharText = cursor.getString(3);
            FaultyCharactristics_Model model = new FaultyCharactristics_Model();
            model.setCustomerID(customerId);;
            model.setServiceCallID(serviceCallID);
            model.setFaultCharacteristicsText(faultCharText);
            faultCharTextList.add(model);
        }
        faultyCharacteristicsAdapter = new FaultyCharacteristicsAdapter(Swas_Inprocess_Call_Service_Id_Details_Activity.this,faultCharTextList);
        rv_faulty_characteristics.setLayoutManager(new LinearLayoutManager(Swas_Inprocess_Call_Service_Id_Details_Activity.this,LinearLayoutManager.VERTICAL, false));
        rv_faulty_characteristics.setAdapter(faultyCharacteristicsAdapter);
    }
}