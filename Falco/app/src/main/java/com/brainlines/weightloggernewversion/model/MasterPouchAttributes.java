package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MasterPouchAttributes implements Parcelable {

    int PouchAttributesID;
    int OEMID;
    int ProdutTypeID;
    String AttributeDesc;
    boolean IsActive;
    boolean IsDeleted;
    String CreatedDate;
    String ModifiedDate;
    String CreatedBy;
    String ModifiedBy;

    public MasterPouchAttributes(int pouchAttributesID, int OEMID, int produtTypeID, String attributeDesc, boolean isActive, boolean isDeleted, String createdDate, String modifiedDate, String createdBy, String modifiedBy) {
        PouchAttributesID = pouchAttributesID;
        this.OEMID = OEMID;
        ProdutTypeID = produtTypeID;
        AttributeDesc = attributeDesc;
        IsActive = isActive;
        IsDeleted = isDeleted;
        CreatedDate = createdDate;
        ModifiedDate = modifiedDate;
        CreatedBy = createdBy;
        ModifiedBy = modifiedBy;
    }

    protected MasterPouchAttributes(Parcel in) {
        PouchAttributesID = in.readInt();
        OEMID = in.readInt();
        ProdutTypeID = in.readInt();
        AttributeDesc = in.readString();
        IsActive = in.readByte() != 0;
        IsDeleted = in.readByte() != 0;
        CreatedDate = in.readString();
        ModifiedDate = in.readString();
        CreatedBy = in.readString();
        ModifiedBy = in.readString();
    }

    public static final Creator<MasterPouchAttributes> CREATOR = new Creator<MasterPouchAttributes>() {
        @Override
        public MasterPouchAttributes createFromParcel(Parcel in) {
            return new MasterPouchAttributes(in);
        }

        @Override
        public MasterPouchAttributes[] newArray(int size) {
            return new MasterPouchAttributes[size];
        }
    };

    public int getPouchAttributesID() {
        return PouchAttributesID;
    }

    public void setPouchAttributesID(int pouchAttributesID) {
        PouchAttributesID = pouchAttributesID;
    }

    public int getOEMID() {
        return OEMID;
    }

    public void setOEMID(int OEMID) {
        this.OEMID = OEMID;
    }

    public int getProdutTypeID() {
        return ProdutTypeID;
    }

    public void setProdutTypeID(int produtTypeID) {
        ProdutTypeID = produtTypeID;
    }

    public String getAttributeDesc() {
        return AttributeDesc;
    }

    public void setAttributeDesc(String attributeDesc) {
        AttributeDesc = attributeDesc;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public boolean isDeleted() {
        return IsDeleted;
    }

    public void setDeleted(boolean deleted) {
        IsDeleted = deleted;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(PouchAttributesID);
        dest.writeInt(OEMID);
        dest.writeInt(ProdutTypeID);
        dest.writeString(AttributeDesc);
        dest.writeByte((byte) (IsActive ? 1 : 0));
        dest.writeByte((byte) (IsDeleted ? 1 : 0));
        dest.writeString(CreatedDate);
        dest.writeString(ModifiedDate);
        dest.writeString(CreatedBy);
        dest.writeString(ModifiedBy);
    }
}
