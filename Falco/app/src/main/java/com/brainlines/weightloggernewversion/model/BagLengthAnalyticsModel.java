package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class BagLengthAnalyticsModel implements Parcelable {

    String BagLengthAnalyticsID;
    String OEMID;
    String LoggerID;
    String TagID;
    String MinReading;
    String MaxReading;
    String RangeReading;
    String RunStatus;
    String ModifiedDate;
    String MeanReading;

    public BagLengthAnalyticsModel(String bagLengthAnalyticsID, String oemID, String loggerID, String tagID, String minReading, String maxReading, String rangeReading, String runStatus, String modifiedDate, String meanReading) {
        this.BagLengthAnalyticsID = bagLengthAnalyticsID;
        this.OEMID = oemID;
        this.LoggerID = loggerID;
        this.TagID = tagID;
        this.MinReading = minReading;
        this.MaxReading = maxReading;
        this.RangeReading = rangeReading;
        this.RunStatus = runStatus;
        this.ModifiedDate = modifiedDate;
        this.MeanReading = meanReading;
    }

    protected BagLengthAnalyticsModel(Parcel in) {
        BagLengthAnalyticsID = in.readString();
        OEMID = in.readString();
        LoggerID = in.readString();
        TagID = in.readString();
        MinReading = in.readString();
        MaxReading = in.readString();
        RangeReading = in.readString();
        RunStatus = in.readString();
        ModifiedDate = in.readString();
        MeanReading = in.readString();
    }

    public static final Creator<BagLengthAnalyticsModel> CREATOR = new Creator<BagLengthAnalyticsModel>() {
        @Override
        public BagLengthAnalyticsModel createFromParcel(Parcel in) {
            return new BagLengthAnalyticsModel(in);
        }

        @Override
        public BagLengthAnalyticsModel[] newArray(int size) {
            return new BagLengthAnalyticsModel[size];
        }
    };

    public String getBagLengthAnalyticsID() {
        return BagLengthAnalyticsID;
    }

    public void setBagLengthAnalyticsID(String bagLengthAnalyticsID) {
        this.BagLengthAnalyticsID = bagLengthAnalyticsID;
    }

    public String getOemID() {
        return OEMID;
    }

    public void setOemID(String oemID) {
        this.OEMID = oemID;
    }

    public String getLoggerID() {
        return LoggerID;
    }

    public void setLoggerID(String loggerID) {
        this.LoggerID = loggerID;
    }

    public String getTagID() {
        return TagID;
    }

    public void setTagID(String tagID) {
        this.TagID = tagID;
    }

    public String getMinReading() {
        return MinReading;
    }

    public void setMinReading(String minReading) {
        this.MinReading = minReading;
    }

    public String getMaxReading() {
        return MaxReading;
    }

    public void setMaxReading(String maxReading) {
        this.MaxReading = maxReading;
    }

    public String getRangeReading() {
        return RangeReading;
    }

    public void setRangeReading(String rangeReading) {
        this.RangeReading = rangeReading;
    }

    public String getRunStatus() {
        return RunStatus;
    }

    public void setRunStatus(String runStatus) {
        this.RunStatus = runStatus;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.ModifiedDate = modifiedDate;
    }

    public String getMeanReading() {
        return MeanReading;
    }

    public void setMeanReading(String meanReading) {
        this.MeanReading = meanReading;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(BagLengthAnalyticsID);
        dest.writeString(OEMID);
        dest.writeString(LoggerID);
        dest.writeString(TagID);
        dest.writeString(MinReading);
        dest.writeString(MaxReading);
        dest.writeString(RangeReading);
        dest.writeString(RunStatus);
        dest.writeString(ModifiedDate);
        dest.writeString(MeanReading);
    }
}
