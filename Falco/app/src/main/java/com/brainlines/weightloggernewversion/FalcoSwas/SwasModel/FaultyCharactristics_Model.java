package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

public class FaultyCharactristics_Model  {
    String CustomerID,ServiceCallID,FaultCharacteristicsText;

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getServiceCallID() {
        return ServiceCallID;
    }

    public void setServiceCallID(String serviceCallID) {
        ServiceCallID = serviceCallID;
    }

    public String getFaultCharacteristicsText() {
        return FaultCharacteristicsText;
    }

    public void setFaultCharacteristicsText(String faultCharacteristicsText) {
        FaultCharacteristicsText = faultCharacteristicsText;
    }
}
