package com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.Model;

public class NewMultipleSkuModel {
    String ptkd_TkNo,SKU_No,dp_stamp_type,dp_stamp_chart,dp_blko_flg,dp_tang_color,QTY,OrgQTY,createdby,createddate;

    public String getPtkd_TkNo() {
        return ptkd_TkNo;
    }

    public void setPtkd_TkNo(String ptkd_TkNo) {
        this.ptkd_TkNo = ptkd_TkNo;
    }

    public String getSKU_No() {
        return SKU_No;
    }

    public void setSKU_No(String SKU_No) {
        this.SKU_No = SKU_No;
    }

    public String getDp_stamp_type() {
        return dp_stamp_type;
    }

    public void setDp_stamp_type(String dp_stamp_type) {
        this.dp_stamp_type = dp_stamp_type;
    }

    public String getDp_stamp_chart() {
        return dp_stamp_chart;
    }

    public void setDp_stamp_chart(String dp_stamp_chart) {
        this.dp_stamp_chart = dp_stamp_chart;
    }

    public String getDp_blko_flg() {
        return dp_blko_flg;
    }

    public void setDp_blko_flg(String dp_blko_flg) {
        this.dp_blko_flg = dp_blko_flg;
    }

    public String getDp_tang_color() {
        return dp_tang_color;
    }

    public void setDp_tang_color(String dp_tang_color) {
        this.dp_tang_color = dp_tang_color;
    }

    public String getQTY() {
        return QTY;
    }

    public void setQTY(String QTY) {
        this.QTY = QTY;
    }

    public String getOrgQTY() {
        return OrgQTY;
    }

    public void setOrgQTY(String orgQTY) {
        OrgQTY = orgQTY;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }
}
