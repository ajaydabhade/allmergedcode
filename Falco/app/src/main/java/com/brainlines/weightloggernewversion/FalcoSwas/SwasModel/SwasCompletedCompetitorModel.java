package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

public class SwasCompletedCompetitorModel {
    String CompetitorsParameterID,OEMID,CustomerID,ServiceCallID,CompetitorsParameters,ServiceCallStartDate,MachineParameterValues,Remark,CreatedBy,CreatedDate;

    public String getCompetitorsParameterID() {
        return CompetitorsParameterID;
    }

    public void setCompetitorsParameterID(String competitorsParameterID) {
        CompetitorsParameterID = competitorsParameterID;
    }

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getServiceCallID() {
        return ServiceCallID;
    }

    public void setServiceCallID(String serviceCallID) {
        ServiceCallID = serviceCallID;
    }

    public String getCompetitorsParameters() {
        return CompetitorsParameters;
    }

    public void setCompetitorsParameters(String competitorsParameters) {
        CompetitorsParameters = competitorsParameters;
    }

    public String getServiceCallStartDate() {
        return ServiceCallStartDate;
    }

    public void setServiceCallStartDate(String serviceCallStartDate) {
        ServiceCallStartDate = serviceCallStartDate;
    }

    public String getMachineParameterValues() {
        return MachineParameterValues;
    }

    public void setMachineParameterValues(String machineParameterValues) {
        MachineParameterValues = machineParameterValues;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }
}
