package com.brainlines.weightloggernewversion.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.listener.MenuClickListenener;
import com.brainlines.weightloggernewversion.model.MenuModel;

import java.util.ArrayList;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MyViewHolder> {

    Context context;
    ArrayList<MenuModel> menuModelArrayList;
    MenuClickListenener menuClickListenener;

    public MenuAdapter(Context context, ArrayList<MenuModel> menuModelArrayList, MenuClickListenener menuClickListenener) {
        this.context = context;
        this.menuModelArrayList = menuModelArrayList;
        this.menuClickListenener = menuClickListenener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_main_menu_layout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final MenuModel model = menuModelArrayList.get(position);
        if (model!=null)
        {
            holder.txt_modulename.setText(model.getModuleName());
            holder.card_menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    menuClickListenener.clickonmenu(model);
                }
            });


        }

    }
    @Override
    public int getItemCount() {
        return menuModelArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_modulename,txtCurrentRoute,txtVsCode,txtQty;
        EditText edtWIP;
        CardView card_menu;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_modulename = (itemView).findViewById(R.id.txt_modulename);
            card_menu = (itemView).findViewById(R.id.card_menu);


        }
    }
}
