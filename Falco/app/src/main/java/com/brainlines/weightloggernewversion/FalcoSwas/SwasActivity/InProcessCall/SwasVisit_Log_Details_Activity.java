package com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.InProcessCall;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Feedback_Activity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Home_Actvity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter.SwasCompletedVisitAdapter;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasListener.VisitlogImagdisplay;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompletedVisitModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasIdDetailsModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.android.material.navigation.NavigationView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants.convertStreamToString;

public class SwasVisit_Log_Details_Activity extends AppCompatActivity  implements View.OnClickListener, VisitlogImagdisplay, NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "SwasVisit_Log_Details_A";
    ImageButton btnSubVisit, btnFeedback,btn_visit_list_display;
    TextView visit_get_pass_upload,txt_visit_service_call_id;
    EditText ed_visit_date,ed_visit_entry_time,ed_visit_exit_time,ed_visit_part_status,ed_visit_remark;
    int yearr, month, dayofmonth;
    private static final int MY_CAMERA_REQUEST_CODE = 100;
    private static final int MY_PERMISSION = 21;
    Bitmap photo;
    String picturePath;
    byte[] imageBytes;
    String oem_id,service_call_id,created_by,customer_id,status_id,type_of_Service_Call_id;
    static final int TIME_DIALOG_ID = 1111;
    private int hr;
    private int min;
    RecyclerView rv_alert_visit_log;
    SwasCompletedVisitAdapter adapter;
    ImageButton img_btn_viewimage;
    private SimpleDateFormat startTimeFormat;
    ImageView img_user_info,nav_view_img,img_back;
    String user_email,user_token,user_role;
    DrawerLayout drawer;
    TextView profile_user,profile_role;
    String Created_by="",service_eng_id;
    String currentDate;
    DateFormat dateFormat = new SimpleDateFormat("E, MMMM dd, yyyy HH:mm:ss.SSS aa");
    private String imageName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swas_inprocess_visit_log_details_navigation);
        initUi();
        setData();
        currentDate = dateFormat.format(new Date());
        img_back = (ImageView) findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SwasVisit_Log_Details_Activity.this, Swas_Deatails_of_Inprocess_Call_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("service_call_id", service_call_id);
                bundle.putString("status_id", status_id);
                bundle.putString("Typeofservicecall_id", type_of_Service_Call_id);
                bundle.putString("customer_id", customer_id);
                i.putExtras(bundle);
                startActivity(i);
                finish();
            }
        });
        if (user_email.equals("yogesh@brainlines.in")) {
            created_by = "Yogesh Jadhav";
//            service_eng_id = "1";
        } else {
            created_by = "Deepak Kulkarni";
//            service_eng_id = "2";
        }
        img_user_info = (ImageView) findViewById(R.id.img_userinfo);
        img_user_info.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img = (ImageView) findViewById(R.id.nav_view_img);

        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView) headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_planning).setVisible(false);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            status_id = bundle.getString("status_id");
            service_call_id = bundle.getString("service_call_id");
            type_of_Service_Call_id = bundle.getString("Typeofservicecall_id");
            customer_id = bundle.getString("customer_id");
        }
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
        if (isConnected) {
            Log.d("Network", "Connected");
            Id_Details();

        } else {
            checkNetworkConnection();
            Log.d("Network", "Not Connected");
        }
    }

    public void initUi() {
        img_btn_viewimage = (ImageButton)findViewById(R.id.img_btn_viewimage);
        visit_get_pass_upload = (TextView)findViewById(R.id.visit_get_pass_upload);
        ed_visit_date = (EditText)findViewById(R.id.ed_visit_date);
        ed_visit_entry_time = (EditText)findViewById(R.id.ed_visit_entry_time);
        ed_visit_exit_time = (EditText)findViewById(R.id.ed_visit_exit_time);
        ed_visit_part_status = (EditText)findViewById(R.id.ed_visit_part_status);
        btnSubVisit=findViewById(R.id.btn_visit_submit);
        btnFeedback=findViewById(R.id.btn_visit_feedback);
        ed_visit_remark = (EditText)findViewById(R.id.ed_visit_remark);
        btn_visit_list_display = findViewById(R.id.btn_visit_list_display);

        txt_visit_service_call_id = (TextView)findViewById(R.id.txt_visit_service_call_id);
        visit_get_pass_upload.setOnClickListener(this);
        ed_visit_date.setOnClickListener(this);
        ed_visit_entry_time.setOnClickListener(this);
        ed_visit_exit_time.setOnClickListener(this);
        btnFeedback.setOnClickListener(this);
        btnSubVisit.setOnClickListener(this);
        btn_visit_list_display.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == visit_get_pass_upload.getId())
        {
            selectImage(SwasVisit_Log_Details_Activity.this);
        }
        else if (view.getId() == btnFeedback.getId())
        {
            Intent i = new Intent(SwasVisit_Log_Details_Activity.this, Swas_Feedback_Activity.class);
            Bundle bundle = new Bundle();
            bundle.putString("service_call_id",service_call_id);
            bundle.putString("status_id",status_id);
            bundle.putString("Typeofservicecall_id",type_of_Service_Call_id);
            bundle.putString("customer_id",customer_id);
            i.putExtras(bundle);

            startActivity(i);
        }
        else if (view.getId() == btn_visit_list_display.getId())
        {

            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null &&
                    activeNetwork.isConnected();
//            if (isConnected) {
//                listDisplay();
//            }else {
//                listDisplay1();
//            }
            listDisplay1();
        }
        else if (view.getId() == ed_visit_date.getId())
        {
            final Calendar myCalendar = Calendar.getInstance();
            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    // TODO Auto-generated method stub

                    myCalendar.set(Calendar.YEAR, year);
                    yearr = Calendar.YEAR;
                    month = Calendar.MONTH;
                    dayofmonth = Calendar.DAY_OF_MONTH;
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    String myFormat = "yyyy/MM/dd"; // your format
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                    ed_visit_date.setText(sdf.format(myCalendar.getTime()));

                }
            };
            DatePickerDialog datePickerDialog=new DatePickerDialog(SwasVisit_Log_Details_Activity.this, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));

            //following line to restrict future date selection
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();

/*
            new DatePickerDialog(SwasVisit_Log_Details_Activity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
*/
        }

        else if (view.getId() == ed_visit_entry_time.getId())
        {
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(SwasVisit_Log_Details_Activity.this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.HOUR_OF_DAY,selectedHour);
                    calendar.set(Calendar.MINUTE,selectedMinute);
                    //calendar.set(Calendar.SECOND,second);

                    ed_visit_entry_time.setText(startTimeFormat().format(calendar.getTime()));
                }
            }, hour, minute, false);//Yes 24 hour time

            mTimePicker.setTitle("Select Time");
            mTimePicker.show();

        }

        else if (view.getId() == ed_visit_exit_time.getId())
        {
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(SwasVisit_Log_Details_Activity.this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.HOUR_OF_DAY,selectedHour);
                    calendar.set(Calendar.MINUTE,selectedMinute);
                    //calendar.set(Calendar.SECOND,second);

                    ed_visit_exit_time.setText(startTimeFormat().format(calendar.getTime()));
                }
            }, hour, minute, false);//Yes 24 hour time
            mTimePicker.setTitle("Select Time");
            mTimePicker.show();
        }

        else if (view.getId() == btnSubVisit.getId())
        {
            if (TextUtils.isEmpty(ed_visit_date.getText().toString()))
            {
                ed_visit_date.setError("please select visit date");
            }
            else if (TextUtils.isEmpty(ed_visit_entry_time.getText().toString()))
            {
                ed_visit_date.setError("please select entry time");
            }
            else if (TextUtils.isEmpty(ed_visit_exit_time.getText().toString()))
            {
                ed_visit_exit_time.setError("please select exit time");

            }
            else if (TextUtils.isEmpty(ed_visit_part_status.getText().toString()))
            {
                ed_visit_part_status.setError("please enter part status");

            }
            else if (TextUtils.isEmpty(ed_visit_remark.getText().toString()))
            {
                ed_visit_remark.setError("please enter note");

            }
            else
            {
                ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnected();
                if (isConnected) {
                    new SaveVisitLogDetails().execute();
                }else {
                    saveVisitLogsToLocalDB();
                }
            }
        }

    }



    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK && data != null) {
                        photo = (Bitmap) data.getExtras().get("data");
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        photo.compress(Bitmap.CompressFormat.PNG, 100, baos);
                        imageBytes = baos.toByteArray();
                        String byte_string = imageBytes.toString();
                        ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
                        getImageUri(SwasVisit_Log_Details_Activity.this, photo);

                        //visit_get_pass_upload.setText(data);
                        //img_profile_upload.setImageBitmap(photo);

                    }

                    break;
                case 1:
                    if (resultCode == RESULT_OK && data != null) {

//                        Uri selectedImage = data.getData();
//                        picturePath = getPath(selectedImage);
//                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
//                        Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
//                        cursor.moveToFirst();
//                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//                        String picturePath = cursor.getString(columnIndex);
//                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                        FileInputStream fis;
//                        try {
//                            fis = new FileInputStream(new File(selectedImage.getPath()));
//                            byte[] buf = new byte[1024];
//                            int n;
//                            while (-1 != (n = fis.read(buf)))
//                                baos.write(buf, 0, n);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                        //img_btn_viewimage.setVisibility(View.VISIBLE);
//                        imageBytes = baos.toByteArray();
//                        photo = BitmapFactory.decodeFile(picturePath);
//                        cursor.close();
                        Uri selectedImage = data == null ? null : data.getData();
                        picturePath = getPath(selectedImage);
                        File f = new File(picturePath);
                        imageName = f.getName();
                        visit_get_pass_upload.setText(imageName);
                        Log.d("ImageURI", selectedImage.getLastPathSegment());
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 8;
                        try {
                            InputStream input = getContentResolver().openInputStream(selectedImage);
                            Bitmap photo1 = BitmapFactory.decodeStream(input);
                            photo = Bitmap.createScaledBitmap(photo1,250,250,false);
                            Log.d("ImageURI", input.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                    break;
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(),
                inImage, "Title", null);
        //document_name.setText(path);
        File f = new File(path);
        visit_get_pass_upload.setText(f.getName().concat(".png"));
        return Uri.parse(path);
    }

    private void selectImage(Context context) {
        final CharSequence[] options = {"camera", "gallary", "cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("camera")) {
                    if (checkSelfPermission(Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                MY_CAMERA_REQUEST_CODE);
                    }
                    else
                    {
                        Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(takePicture, 0);
                    }
                }
                else if (options[item].equals("gallary")) {
                    if (ContextCompat.checkSelfPermission(SwasVisit_Log_Details_Activity.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(SwasVisit_Log_Details_Activity.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED )
                    {
                        ActivityCompat.requestPermissions(SwasVisit_Log_Details_Activity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSION);

                    }
                    else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_PICK);
                        startActivityForResult(intent, 1);
                    }

                } else if (options[item].equals("cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private String getPath(Uri uri) {

        String result;
        Cursor cursor = SwasVisit_Log_Details_Activity.this.getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
            result = uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            visit_get_pass_upload.setText(result);
            photo= BitmapFactory.decodeFile(result);
            cursor.close();
        }
        return result;

    }

    private void Id_Details() {
        API api = Retroconfig.swasretrofit().create(API.class);

        Call<ResponseBody> call = api.getalldetailsofsingleservicecall(service_call_id,status_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {

                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            ArrayList<SwasIdDetailsModel> model = new ArrayList<>();
                            if (array.length() != 0) {

                                for (int i = 0; i < array.length(); i++) {
                                    SwasIdDetailsModel swasIdDetailsModel = new SwasIdDetailsModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    service_call_id=object1.getString("ServiceCallID");
                                    //created_by = object1.getString("CreatedBy");
                                    txt_visit_service_call_id.setText(object1.getString("ServiceCallID"));


                                }


                            } else if (array.length() == 0) {
                            }
                        }
                    }


                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void listDisplay() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.swas_alert_visit_log_list, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        rv_alert_visit_log = (RecyclerView)dialogView.findViewById(R.id.rv_alert_visit_log);
        visit_list_display();
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }
    private void listDisplay1() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.swas_alert_visit_log_list, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        rv_alert_visit_log = (RecyclerView)dialogView.findViewById(R.id.rv_alert_visit_log);
        getVisitLogsFromLocalDB();
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void getVisitLogsFromLocalDB() {
        Cursor cursor = new DataBaseHelper(this).getVisitLogs(service_call_id);
        ArrayList<SwasCompletedVisitModel> model = new ArrayList<>();
        model.clear();
        while (cursor.moveToNext()){
            String ServiceLogID = cursor.getString(1);
            String OEMID = cursor.getString(2);
            String ServiceCallID = cursor.getString(3);
            String CustomerID = cursor.getString(4);
            String ServiceEngineerID = cursor.getString(5);
            String PartStatus = cursor.getString(6);
            String ServiceCallStartDate = cursor.getString(7);
            String ServiceCallEndDate = cursor.getString(8);
            String ServiceCallEntryTime = cursor.getString(9);
            String ServiceCallExitTime = cursor.getString(10);
            String GatePassImagePath = cursor.getString(11);
            String Remark = cursor.getString(12);
            String CreatedBy = cursor.getString(13);
            String CreatedDate = cursor.getString(14);
            SwasCompletedVisitModel visitModel = new SwasCompletedVisitModel();
            visitModel.setServiceLogID(ServiceLogID);
            visitModel.setOEMID(OEMID);
            visitModel.setCustomerID(CustomerID);
            visitModel.setServiceCallID(ServiceCallID);
            visitModel.setServiceEngineerID(ServiceEngineerID);
            visitModel.setPartStatus(PartStatus);
            visitModel.setServiceCallStartDate(ServiceCallStartDate);
            visitModel.setServiceCallEndDate(ServiceCallEndDate);
            visitModel.setServiceCallEntryTime(ServiceCallEntryTime);
            visitModel.setServiceCallExitTime(ServiceCallExitTime);
            visitModel.setGatePassImagePath(GatePassImagePath);
            visitModel.setRemark(Remark);
            visitModel.setCreatedBy(CreatedBy);
            visitModel.setCreatedDate(CreatedDate);
            model.add(visitModel);
        }
        adapter = new SwasCompletedVisitAdapter(SwasVisit_Log_Details_Activity.this,model,SwasVisit_Log_Details_Activity.this);
        rv_alert_visit_log.setLayoutManager(new LinearLayoutManager(SwasVisit_Log_Details_Activity.this, LinearLayoutManager.VERTICAL, false));
        rv_alert_visit_log.setAdapter(adapter);
    }

    private void visit_list_display(){
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getservicecallvisitlog(service_call_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                    }
                    String res = response.body().string();
                    if (res != null) {

                        JSONObject object = new JSONObject(res);
                        JSONArray array = object.getJSONArray("data");
                        ArrayList<SwasCompletedVisitModel> model = new ArrayList<>();

                        if (array.length() != 0) {

                            for (int i = 0; i < array.length(); i++) {
                                SwasCompletedVisitModel visitModel = new SwasCompletedVisitModel();
                                JSONObject object1 = array.getJSONObject(i);
                                visitModel.setServiceLogID(object1.getString("ServiceLogID"));
                                visitModel.setOEMID(object1.getString("OEMID"));
                                visitModel.setCustomerID(object1.getString("CustomerID"));
                                visitModel.setServiceCallID(object1.getString("ServiceCallID"));
                                visitModel.setServiceEngineerID(object1.getString("ServiceEngineerID"));
                                visitModel.setPartStatus(object1.getString("PartStatus"));
                                visitModel.setServiceCallStartDate(object1.getString("ServiceCallStartDate"));
                                visitModel.setServiceCallEndDate(object1.getString("ServiceCallEndDate"));
                                visitModel.setServiceCallEntryTime(object1.getString("ServiceCallEntryTime"));
                                visitModel.setServiceCallExitTime(object1.getString("ServiceCallExitTime"));
                                visitModel.setGatePassImagePath(object1.getString("GatePassImagePath"));
                                visitModel.setRemark(object1.getString("Remark"));
                                visitModel.setCreatedBy(object1.getString("CreatedBy"));
                                visitModel.setCreatedDate(object1.getString("CreatedDate"));

                                //swasServiceModel .setServicecall_status_id(object1.getString("ServiceCallStatusID"));

                                model.add(visitModel);

                            }
                            adapter = new SwasCompletedVisitAdapter(SwasVisit_Log_Details_Activity.this,model,SwasVisit_Log_Details_Activity.this);
                            rv_alert_visit_log.setLayoutManager(new LinearLayoutManager(SwasVisit_Log_Details_Activity.this, LinearLayoutManager.VERTICAL, false));
                            rv_alert_visit_log.setAdapter(adapter);


                        } else if (array.length() == 0) {
                        }
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void displayvisitlogdetailsImage(SwasCompletedVisitModel model) {
        if (model != null)
        {
            String img_string = model.getGatePassImagePath();
            byte [] encodeByte = Base64.decode(img_string,Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            //Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
            img_display(bitmap);
        }
    }

    public void img_display(Bitmap img) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.item_img_upload_display, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        ImageView pop_img_profile = (ImageView)dialogView.findViewById(R.id.pop_img_profile);

        pop_img_profile.setImageBitmap(img);


        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public class SaveVisitLogDetails extends AsyncTask<String,String,String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SwasVisit_Log_Details_Activity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String getResponse="";
            String visitdate= ed_visit_date.getText().toString();
            String visitpartstatus=ed_visit_part_status.getText().toString();
            String visitentrytime=ed_visit_entry_time.getText().toString();
            String visitexittime=ed_visit_exit_time.getText().toString();
            String visitremark=ed_visit_remark.getText().toString();

            String url= Retroconfig.BASEURL_SWAS + "insertvisitlogdetails";
            String base64 = getEncoded64ImageStringFromBitmap(photo);


            try {
                HttpParams httpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(httpParams, 60000);
                HttpConnectionParams.setSoTimeout(httpParams, 60000);
                HttpClient client = new DefaultHttpClient(httpParams);
                HttpPost httpPost = new HttpPost(url);

                httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");

                JSONArray array = new JSONArray();
                JSONObject object = new JSONObject();
                object.put("OEMID", oem_id);
                object.put("CustomerID", customer_id);
                object.put("ServiceCallID", service_call_id);
                object.put("ServiceEngineerID", service_eng_id);
                object.put("PartStatus", visitpartstatus);
                object.put("ServiceCallStartDate", visitdate);
                object.put("ServiceCallEndDate", visitdate);
                object.put("ServiceCallEntryTime", visitentrytime);
                object.put("ServiceCallExitTime", visitexittime);
                object.put("GatePassImagePath", base64);
                object.put("Remark", visitremark);
                object.put("IsActive", "1");
                object.put("CreatedBy", created_by);
                array.put(object);

                httpPost.setEntity(new StringEntity(array.toString(), "UTF-8"));
                httpPost.setParams(httpParams);

                HttpResponse response = client.execute(httpPost);
                StatusLine statusLine = response.getStatusLine();

                if (statusLine.getStatusCode() == HttpStatus.SC_OK || statusLine.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();

                    getResponse = convertStreamToString(inputStream);
                } else if (statusLine.getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();

                    getResponse = convertStreamToString(inputStream);
                } else {
                    response.getEntity().getContent().close();
                }
            }
            catch (Exception e) {
                Log.i(URL_Constants.TAG, "Error Message in Exception :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_EXCEPTION;

            }
            return getResponse;
        }

        @Override
        protected void onPostExecute(String getResponse) {
            super.onPostExecute(getResponse);
            progressDialog.dismiss();
            if (!getResponse.equals("Error"))
            {
                Toast.makeText(SwasVisit_Log_Details_Activity.this, "Visit log details inserted successfully", Toast.LENGTH_SHORT).show();

                Intent i = new Intent( SwasVisit_Log_Details_Activity.this,Swas_Deatails_of_Inprocess_Call_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_Service_Call_id);
                bundle.putString("customer_id",customer_id);
                i.putExtras(bundle);

                startActivity(i);
            }
        }
    }

    private void saveVisitLogsToLocalDB() {
        String visitdate= ed_visit_date.getText().toString();
        String visitpartstatus=ed_visit_part_status.getText().toString();
        String visitentrytime=ed_visit_entry_time.getText().toString();
        String visitexittime=ed_visit_exit_time.getText().toString();
        String visitremark=ed_visit_remark.getText().toString();
        String base64 = getEncoded64ImageStringFromBitmap(photo);
        byte[] data = Base64.decode(base64, Base64.DEFAULT);
        DataBaseHelper helper=new DataBaseHelper(SwasVisit_Log_Details_Activity.this);
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put("ServiceLogID","");
        cv.put("OEMID",oem_id);
        cv.put("ServiceCallID",service_call_id);
        cv.put("CustomerID",customer_id);
        cv.put("ServiceEngineerID",service_eng_id);
        cv.put("PartStatus",visitpartstatus);
        cv.put("ServiceCallStartDate",visitdate);
        cv.put("ServiceCallEndDate",visitdate);
        cv.put("ServiceCallEntryTime",visitentrytime);
        cv.put("ServiceCallExitTime",visitexittime);
        cv.put("GatePassImagePath",data);
        cv.put("REMARK",visitremark);
        cv.put("CreatedBy",created_by);
        cv.put("CreatedDate",currentDate);
        cv.put("IsSync","0");
        long d=db.insert(Constants.TABLE_VISIT_LOGS,null,cv);
        Log.d("user check", String.valueOf(d));
        if (d==-1){
            Log.d(TAG, "onResponse: "+"Data not Inserted");
        }else {
            Log.d(TAG, "onResponse: "+"Data Inserted");
        }
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }

    public SimpleDateFormat startTimeFormat(){
        return this.startTimeFormat = new SimpleDateFormat("hh:mm a");
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(SwasVisit_Log_Details_Activity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);

        // Set content width and height
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(SwasVisit_Log_Details_Activity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }

    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(SwasVisit_Log_Details_Activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();

                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    oem_id= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));
                    service_eng_id= cursor.getString(cursor.getColumnIndex(Constants.USER_LOGIN_ID));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));


                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout() {
        finish();
        Toast toast= Toast.makeText(SwasVisit_Log_Details_Activity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(SwasVisit_Log_Details_Activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);

        //Preferences.Logout();

    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id==R.id.nav_swas)
        {
            Intent i=new Intent(SwasVisit_Log_Details_Activity.this, Swas_Home_Actvity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    public void checkNetworkConnection(){
        AlertDialog.Builder builder =new AlertDialog.Builder(SwasVisit_Log_Details_Activity.this);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i= new Intent(SwasVisit_Log_Details_Activity.this,Swas_Deatails_of_Inprocess_Call_Activity.class);
        Bundle bundle = new Bundle();
        bundle.putString("service_call_id",service_call_id);
        bundle.putString("status_id",status_id);
        bundle.putString("Typeofservicecall_id",type_of_Service_Call_id);
        bundle.putString("customer_id",customer_id);
        i.putExtras(bundle);
        startActivity(i);
        finish();
    }
}
