package com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoJK.Listeners.EnterWipListener;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.Model.NewMultipleSkuModel;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class NewMultipleSkuAdapter extends RecyclerView.Adapter<NewMultipleSkuAdapter.MyViewHolder> {

    Context context;
    ArrayList<NewMultipleSkuModel> skuDetailsModelArrayList;
    EnterWipListener enterWipListener;

    public NewMultipleSkuAdapter(Context context, ArrayList<NewMultipleSkuModel> skuDetailsModelArrayList) {
        this.context = context;
        this.skuDetailsModelArrayList = skuDetailsModelArrayList;
        this.enterWipListener = enterWipListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_sku_details,parent,false);
        return new NewMultipleSkuAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final NewMultipleSkuModel model = skuDetailsModelArrayList.get(position);
        if (model!=null)
        {
            holder.sku_name.setText(model.getSKU_No());
            holder.sku_stamp_type.setText(model.getDp_stamp_type());
            holder.sku_stamp_chart.setText(model.getDp_stamp_chart());
            holder.sku_blackodizing.setText(model.getDp_blko_flg());
            holder.sku_tang_color.setText(model.getDp_tang_color());
            holder.sku_qty.setText(model.getQTY());
            holder.sku_releaseqty.setText(model.getOrgQTY());

        }

    }
    @Override
    public int getItemCount() {
        return skuDetailsModelArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView sku_name,sku_stamp_type,sku_stamp_chart,sku_blackodizing,sku_tang_color,sku_qty,sku_releaseqty;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            sku_name = (itemView).findViewById(R.id.sku_name);
            sku_stamp_type = (itemView).findViewById(R.id.sku_stamp_type);
            sku_stamp_chart = (itemView).findViewById(R.id.sku_stamp_chart);
            sku_blackodizing = (itemView).findViewById(R.id.sku_blackodizing);
            sku_tang_color = (itemView).findViewById(R.id.sku_tang_color);
            sku_qty = (itemView).findViewById(R.id.sku_qty);
            sku_releaseqty = (itemView).findViewById(R.id.sku_releaseqty);




        }
    }
}
