package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

import android.os.Parcel;
import android.os.Parcelable;

public class SwasInprocessCallMachineParameterModel implements Parcelable {

    int MachineParameterID;
    String MachineParameters;
    String UNIT;

    public SwasInprocessCallMachineParameterModel(int machineParameterID, String machineParameters, String UNIT) {
        MachineParameterID = machineParameterID;
        MachineParameters = machineParameters;
        this.UNIT = UNIT;
    }

    protected SwasInprocessCallMachineParameterModel(Parcel in) {
        MachineParameterID = in.readInt();
        MachineParameters = in.readString();
        UNIT = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(MachineParameterID);
        dest.writeString(MachineParameters);
        dest.writeString(UNIT);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SwasInprocessCallMachineParameterModel> CREATOR = new Creator<SwasInprocessCallMachineParameterModel>() {
        @Override
        public SwasInprocessCallMachineParameterModel createFromParcel(Parcel in) {
            return new SwasInprocessCallMachineParameterModel(in);
        }

        @Override
        public SwasInprocessCallMachineParameterModel[] newArray(int size) {
            return new SwasInprocessCallMachineParameterModel[size];
        }
    };

    public int getMachineParameterID() {
        return MachineParameterID;
    }

    public void setMachineParameterID(int machineParameterID) {
        MachineParameterID = machineParameterID;
    }

    public String getMachineParameters() {
        return MachineParameters;
    }

    public void setMachineParameters(String machineParameters) {
        MachineParameters = machineParameters;
    }

    public String getUNIT() {
        return UNIT;
    }

    public void setUNIT(String UNIT) {
        this.UNIT = UNIT;
    }
}
