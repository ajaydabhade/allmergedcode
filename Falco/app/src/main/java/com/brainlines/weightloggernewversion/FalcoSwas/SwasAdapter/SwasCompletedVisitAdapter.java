package com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasListener.VisitlogImagdisplay;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompletedVisitModel;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class SwasCompletedVisitAdapter extends RecyclerView.Adapter< SwasCompletedVisitAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<SwasCompletedVisitModel> visit_list;
    View.OnClickListener mClickListener;
    Context context;
    VisitlogImagdisplay visitlogImagdisplay;

    public SwasCompletedVisitAdapter(Context ctx, ArrayList<SwasCompletedVisitModel> visit_list, VisitlogImagdisplay visitlogImagdisplay){
        inflater = LayoutInflater.from(ctx);
        this.visit_list = visit_list;
        this.context=ctx;
        this.visitlogImagdisplay=visitlogImagdisplay;
    }

    @Override
    public int getItemCount() {
        return visit_list.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_swas_completed_visit_log, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final  SwasCompletedVisitModel model = visit_list.get(position);
        if (model!=null) {

            holder.txt_VisitDate.setText(model.getServiceCallStartDate());
            holder.txt_EntryTime.setText(model.getServiceCallEntryTime());
            holder.txt_ExitTime.setText(model.getServiceCallExitTime());
            holder.txt_VisitRemark.setText(model.getRemark());
            //holder.txt_VisitGetPass.setText(model.getGatePassImagePath());

            holder.img_VisitGetPass.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    visitlogImagdisplay.displayvisitlogdetailsImage(model);
                }
            });


        }
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txt_VisitDate,txt_EntryTime,txt_ExitTime,txt_VisitRemark;
        ImageButton img_VisitGetPass;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_VisitDate=itemView.findViewById(R.id.txt_VisitDate);
            txt_EntryTime=itemView.findViewById(R.id.txt_EntryTime);
            txt_ExitTime=itemView.findViewById(R.id.txt_ExitTime);
            txt_VisitRemark=itemView.findViewById(R.id.txt_VisitRemark);
            img_VisitGetPass = itemView.findViewById(R.id.img_VisitGetPass);




        }

    }
}

