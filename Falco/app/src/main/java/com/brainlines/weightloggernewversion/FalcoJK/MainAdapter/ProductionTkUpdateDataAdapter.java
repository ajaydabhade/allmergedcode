package com.brainlines.weightloggernewversion.FalcoJK.MainAdapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.AssetNoModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.EquipTypeModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.OperationEquipModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.ProdTkOperationModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.ProductionViewTrackingModel;
import com.brainlines.weightloggernewversion.FalcoJK.UrlConstants.JK_URL_Constants;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class ProductionTkUpdateDataAdapter extends RecyclerView.Adapter<ProductionTkUpdateDataAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    Spinner spin_equipment, spin_equip_type,spin_select_operation,spin_spin_asset_no;

    ArrayList<OperationEquipModel> models=new ArrayList<>();
    ArrayList<EquipTypeModel>equipTypelist=new ArrayList<>();
    ArrayList<AssetNoModel>assetNoModels=new ArrayList<>();
    EquipTypeSpinAdapter equipTypeSpinAdapter;
    private ArrayList<ProdTkOperationModel> ticket_list;
    OpearionEqupSpinAdapter opearionEqupSpinAdapter;
    String xmlData;
    Context context;
    AssetNoAdapter assetNoAdapter;
    String jsonstring;
    RecyclerView recyclerView;
    String ptk_no,ptk_plant,ptk_Operation,ptk_UpdateDate,ptk_OkQty,ptk_ActualPlant,ptk_ReworkQty,ptk_RejectQty,str_operation_id;
    String ptk_WIPQty,ptk_SecondQty,ptk_TotalQty,ptk_qty,ptk_CreatedBy,ptk_CreatedDate,ptk_ApprovedBy,ptk_ApprovedDate,ptk_Approve,ptk_Active;

    public ProductionTkUpdateDataAdapter(Context ctx, ArrayList<ProdTkOperationModel> ticket_list, RecyclerView rv_update_view_data) {
        inflater = LayoutInflater.from(ctx);
        this.ticket_list = ticket_list;
        this.context = ctx;
        this.recyclerView=rv_update_view_data;
    }

    @Override
    public int getItemCount() {
        return ticket_list.size();
    }

    @NonNull

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_update_production_ticket_data, parent, false);
        final MyViewHolder holder = new MyViewHolder(view);

       /* holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onClick(view);

           }
        });*/
       return holder;
    }
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int i) {
       holder.upopeartions.setText(ticket_list.get(i).getOpr_operation_name());
       holder.up_current_route.setText(ticket_list.get(i).getUni_unit_name());

       holder.img_route.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               //str_operation_id=ticket_list.get(i).getPtkd_Operation();
               showbottomsheetdialog();
           }
       });
       holder.img_submit.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

              // String operation=ticket_list.get(i).getPtkd_Operation();
               ptk_no=ticket_list.get(i).getPtkd_TkNo();
               ptk_OkQty=holder.upedokay.getText().toString();
               ptk_ReworkQty=holder.upedrework.getText().toString();
               ptk_RejectQty=holder.upedrework.getText().toString();
               ptk_SecondQty=holder.upedsecond.getText().toString();
               ptk_WIPQty=holder.upedwip.getText().toString();

               Calendar c = Calendar.getInstance();
               SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
               String date = sdf.format(c.getTime());
               String[] string =date.split(" ");
               String date1=string[0];
               //txt_updated_griddate.setText(date);


                  /* if (!operation.equals(ticket_list.get(i1).getPtkd_Operation()))
                   {
                       try {

                           StringBuilder stringBuilder=new StringBuilder();
                           stringBuilder.append('[');
                           // stringBuilder.append(+'"'+'{');
                           stringBuilder.append('{' + "ptk_PtkdId" + ':' + '"').append(ticket_list.get(i).getPtkd_PktId()).append('"').append(',');
                           stringBuilder.append("ptk_TkNo" + ':' + '"').append(ticket_list.get(i).getPtkd_TkNo()).append('"').append(',');
                           stringBuilder.append("ptk_Plant" + ':' + '"').append(ticket_list.get(i).getPtkd_Plant()).append('"').append(',');
                           stringBuilder.append("ptk_Operation" + ':' + '"').append(ticket_list.get(i).getPtkd_Operation()).append('"').append(',');
                           stringBuilder.append("ptk_UpdateDate"+':'+'"' + date +'"'+',');
                           stringBuilder.append("ptk_OkQty" + ':' + '"').append(ptk_OkQty).append('"').append(',');
                           stringBuilder.append("ptk_ActualPlant" + ':' + '"').append(ticket_list.get(i).getPtkd_Plant()).append('"').append(',');
                           stringBuilder.append("ptk_ReworkQty" + ':' + '"').append(ptk_ReworkQty).append('"').append(',');
                           stringBuilder.append("ptk_RejectQty" + ':' + '"').append(ptk_RejectQty).append('"').append(',');
                           stringBuilder.append("ptk_WIPQty" + ':' + '"').append(ptk_WIPQty).append('"').append(',');
                           stringBuilder.append("ptk_SecondQty" + ':' + '"').append(ptk_SecondQty).append('"').append(',');
                           stringBuilder.append("ptk_TotalQty" + ':' + '"').append(ticket_list.get(i).getPtkd_Qty()).append('"').append(',');
                           stringBuilder.append("ptk_CreatedBy"+':'+'"' + JK_URL_Constants.PLANT_USER +'"'+',' );
                           stringBuilder.append("ptk_CreatedDate"+':'+'"' + date + '"'+',');
                           stringBuilder.append("ptk_ApprovedBy"+':'+'"' + JK_URL_Constants.PLANT_USER +'"'+',');
                           stringBuilder.append("ptk_ApprovedDate"+':'+'"' + date +'"'+',');
                           stringBuilder.append("ptk_Approve"+':'+'"' + "N" + '"'+',');
                           stringBuilder.append("ptk_Active"+':'+ "1" +'}'+',');

                           stringBuilder.append(']');
                           xmlData = stringBuilder.toString().replace("&", "&amp;").replace(" <", "<").replace("> ", ">");

                       }

                       catch (Exception e)
                       {
                           e.printStackTrace();
                       }
                   }


              else
                   {*/
                       try {
                           if (!holder.upedokay.getText().toString().equals("null"))
                           {
                               ptk_OkQty=holder.upedokay.getText().toString();
                           }
                           else {ptk_OkQty="0";}

                           if (!holder.upedrework.getText().toString().equals("null"))
                           {
                               ptk_ReworkQty=holder.upedrework.getText().toString();
                           }
                           else {ptk_ReworkQty="0";}
                           if (!holder.upedreject.getText().toString().equals("null"))
                           {
                               ptk_RejectQty=holder.upedrework.getText().toString();
                           }
                           else {ptk_RejectQty="0";}

                           if (!holder.upedsecond.getText().toString().equals("null"))
                           {
                               ptk_SecondQty=holder.upedsecond.getText().toString();
                           }
                           else {ptk_SecondQty="0";}
                           if (!holder.upedwip.getText().toString().equals("null"))
                           {
                               ptk_WIPQty=holder.upedwip.getText().toString();
                           }
                           else
                           {ptk_WIPQty="0";}
                           //  String ptk_PtkdId="ptk_PtkdId";
                           StringBuilder stringBuilder=new StringBuilder();
                           stringBuilder.append('[');
                           for (int i=0;i<ticket_list.size();i++)
                           {
                                   stringBuilder.append('{' + "ptk_PtkdId" + ':' + '"').append(ticket_list.get(i).getPtkd_PktId()).append('"').append(',');
                                   stringBuilder.append("ptk_TkNo" + ':' + '"').append(ticket_list.get(i).getPtkd_TkNo()).append('"').append(',');
                                   stringBuilder.append("ptk_Plant" + ':' + '"').append(ticket_list.get(i).getPtkd_Plant()).append('"').append(',');
                                   stringBuilder.append("ptk_Operation" + ':' + '"').append(ticket_list.get(i).getPtkd_Operation()).append('"').append(',');
                                   stringBuilder.append("ptk_UpdateDate"+':'+'"' + date +'"'+',');
                                   stringBuilder.append("ptk_OkQty" + ':' + '"').append(ptk_OkQty).append('"').append(',');
                                   stringBuilder.append("ptk_ActualPlant" + ':' + '"').append(ticket_list.get(i).getPtkd_Plant()).append('"').append(',');
                                   stringBuilder.append("ptk_ReworkQty" + ':' + '"').append(ptk_ReworkQty).append('"').append(',');
                                   stringBuilder.append("ptk_RejectQty" + ':' + '"').append(ptk_RejectQty).append('"').append(',');
                                   stringBuilder.append("ptk_WIPQty" + ':' + '"').append(ptk_WIPQty).append('"').append(',');
                                   stringBuilder.append("ptk_SecondQty" + ':' + '"').append(ptk_SecondQty).append('"').append(',');
                                   stringBuilder.append("ptk_TotalQty" + ':' + '"').append(ticket_list.get(i).getPtkd_Qty()).append('"').append(',');
                                   stringBuilder.append("ptk_CreatedBy"+':'+'"' + JK_URL_Constants.PLANT_USER +'"'+',' );
                                   stringBuilder.append("ptk_CreatedDate"+':'+'"' + date + '"'+',');
                                   stringBuilder.append("ptk_ApprovedBy"+':'+'"' + JK_URL_Constants.PLANT_USER +'"'+',');
                                   stringBuilder.append("ptk_ApprovedDate"+':'+'"' + date +'"'+',');
                                   stringBuilder.append("ptk_Approve"+':'+'"' + "N" + '"'+',');
                                   stringBuilder.append("ptk_Active"+':'+ "1" +'}'+',');
                                   // stringBuilder.append(+'"'+'{');
                           }
                           stringBuilder.append(']');
                           xmlData = stringBuilder.toString().replace("&", "&amp;").replace(" <", "<").replace("> ", ">");

                       }

                       catch (Exception e)
                       {
                           e.printStackTrace();
                       }


                       String str=xmlData.substring(0, xmlData.length() - 2)+ ']';
               jsonstring=str;
               new UpdateProdTicketCall().execute();
           }
       });

    }
    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView upopeartions,up_current_route;
        ImageButton img_route,img_submit;
        EditText upedokay,upedrework,upedreject,upedwip,upedsecond;

        public MyViewHolder(View itemView) {
            super(itemView);
            upopeartions=(TextView)itemView.findViewById(R.id.upopeartions);
            up_current_route=(TextView)itemView.findViewById(R.id.up_current_route);
            upedrework=(EditText) itemView.findViewById(R.id.upedrework);
            upedokay=(EditText)itemView.findViewById(R.id.upedokay);
            upedreject=(EditText)itemView.findViewById(R.id.upedreject);
            upedwip=(EditText)itemView.findViewById(R.id.upedwip);
            upedsecond=(EditText)itemView.findViewById(R.id.upedsecond);
            img_route=(ImageButton)itemView.findViewById(R.id.img_route);
            img_submit=(ImageButton)itemView.findViewById(R.id.img_submit);
        }


    }

    public void showbottomsheetdialog()
    {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(context);
        View modelBottomSheet = LayoutInflater.from(context).inflate(R.layout.layout_downtime_prodution, null);


        mBottomSheetDialog.setContentView(modelBottomSheet);
        spin_equipment=(Spinner)mBottomSheetDialog.findViewById(R.id.spin_equip_name);
        spin_equip_type=(Spinner)mBottomSheetDialog.findViewById(R.id.spin_equi_type);
        spin_select_operation=(Spinner)mBottomSheetDialog.findViewById(R.id.spin_opearation);
        spin_spin_asset_no=(Spinner)mBottomSheetDialog.findViewById(R.id.spin_asset_no);

        ImageButton img_cancel_btn=(ImageButton)mBottomSheetDialog.findViewById(R.id.img_downtime_cancel);
        ImageButton img_save_btn=(ImageButton)mBottomSheetDialog.findViewById(R.id.img_downtime_submit);

        img_save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DowntimeCall().execute();
            }
        });
        img_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });


        final ArrayList<String> operation = new ArrayList<>();
        operation.add("Select Operation");
        operation.add("Breakdown");
        operation.add("Abseentism");
        operation.add("Quality");
        operation.add("Other");
        SpinnerAdapter arrayAdapter = new SpinnerAdapter(context, operation);
        spin_select_operation.setAdapter(arrayAdapter);

        spin_select_operation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
               models.clear();
                new GetOperationEquipment().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spin_equipment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
               // String eqip_id=models.get(i).getEq_id();
                equipTypelist.clear();

                new GetQuipmentType().execute();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spin_equip_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                assetNoModels.clear();
                new GetAssetNo().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mBottomSheetDialog.show();

    }

    public class UpdateProdTicketCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(context);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String str="";
            String url2= JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.TEST_UPDATE_PRODUCTION_SUBMIT + URLEncoder.encode(jsonstring) ;
            response= URL_Constants.httppost(url2);
            return response;
        }
        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                String str=response.replace("\"", "");
                //String str1=response.substring(0,response.length()-1);
                try {
                    if(str.equals("\n"))
                    {
                        Toast.makeText(context, "Successfully inserted", Toast.LENGTH_SHORT).show();
                        new GetProductionViewData().execute();
                    }
                    //JSONObject object = new JSONObject(response);
                    /*String status = object.getString("message");
                    if (status.equals("Success")) {
                        Toast.makeText(context, "Successfully inserted", Toast.LENGTH_SHORT).show();
                    } else
                    {
                        Toast.makeText(context, "Please try again", Toast.LENGTH_SHORT).show();
                    }*/
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        }
    }

    public class GetProductionViewData extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(context);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            // String user="CHNM_10RDF1GN_1593";
            //String user="Pithampur";
            String url= JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.VIEW_PROD_TICKET + ptk_no + "&ptk_UpdateDate=" + "2019-12-03";
            response= URL_Constants.makeHttpPostRequest(url);
            Log.d(JK_URL_Constants.TAG, "Whole data = " + response);
            return response;
        }
        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try {
                    //JSONObject jsonObject=new JSONObject(response);
                    JSONArray array=new JSONArray(response);
                    if (array.length()==0){
                        Toast toast = Toast.makeText(context,
                                "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                    else {
                        ArrayList<ProductionViewTrackingModel> ticketmodel= new ArrayList<>();
                        for (int i=0;i<array.length();i++) {
                            JSONObject obj = array.getJSONObject(i);
                            ProductionViewTrackingModel model = new ProductionViewTrackingModel();

                            model.setPtk_Id(obj.getString("ptk_Id"));
                            model.setPtk_PtkdId(obj.getString("ptk_PtkdId"));
                            model.setPtk_TkNo(obj.getString("ptk_TkNo"));
                            model.setPtk_Plant(obj.getString("ptk_Plant"));
                            model.setPtk_Operation(obj.getString("ptk_Operation"));
                            model.setPtk_UpdateDate(obj.getString("ptk_UpdateDate"));
                            model.setPtk_OkQty(obj.getString("ptk_OkQty"));
                            model.setPtk_RejectQty(obj.getString("ptk_ReworkQty"));
                            model.setPtk_ReworkQty(obj.getString("ptk_RejectQty"));
                            model.setPtk_WIPQty(obj.getString("ptk_WIPQty"));
                            model.setPtk_SecondQty(obj.getString("ptk_SecondQty"));
                            model.setPtk_TotalQty(obj.getString("ptk_TotalQty"));
                            model.setOpr_operation_name(obj.getString("opr_operation_name"));
                            model.setPtkd_Qty(obj.getString("ptkd_Qty"));
                            model.setUni_unit_name(obj.getString("uni_unit_name"));
                            model.setVal_valuestream_name(obj.getString("val_valuestream_name"));
                            model.setFt_ftype_desc(obj.getString("ft_ftype_desc"));
                            model.setTk_Actual_Start_Date(obj.getString("tk_Actual_Start_Date"));
                            model.setTk_Actual_End_Date(obj.getString("tk_Actual_End_Date"));
                            model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                            model.setTk_End_Date(obj.getString("tk_End_Date"));

                            /*txt_view_ticket_no.setText(obj.getString("ptk_TkNo"));
                            txt_view_act_strat_date.setText(obj.getString("tk_Actual_Start_Date"));
                            txt_view_act_end_date.setText(obj.getString("tk_Actual_End_Date"));
                            //txt_view_sku_13.setText(obj.getString(""));
                            txt_view_plant.setText(obj.getString("ptk_Plant"));
                            txt_view_tk_filetype.setText(obj.getString("ft_ftype_desc"));
                            txt_view_startdate.setText(obj.getString("tk_Start_Date"));
                            txt_view_tk_qty.setText(obj.getString("ptk_TotalQty"));
                            txt_view_enddate.setText(obj.getString("tk_End_Date"));*/

                            ticketmodel.add(model);
                        }
                        ProductionUpdatedViewAdapter adapter1=new ProductionUpdatedViewAdapter(context,ticketmodel);
                        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                        recyclerView.setAdapter(adapter1);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    public class DowntimeCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(context);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            String response="";
            String url= JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.SAVE_DOWNTIME_OPERATION + "CHNMRAJ" + "&operationNo=" + "10"
                    + "&te_Equid=" + "171" + "&ticketIDNo=" + "CHNM_04HAF1GN_104" + "&te_ReseasenDt=" + "Reason4" + "&te_NoofShipt=" + "2222" + "&te_Ab_Br_Qt=" + "0"
                    + "&te_MovedQuntity=" + "0" + "&te_AssetNo=" + "2108" + "&userId=" + JK_URL_Constants.PLANT_USER + "&cRRemark=" + "Ok" + "&te_TE_Flag=" + "Downtime";
            response= URL_Constants.makeHttpPostRequest(url);
            Log.d(JK_URL_Constants.TAG, "Whole data = " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (response.equals(""))
            {
                Toast.makeText(context, "Successfully insert", Toast.LENGTH_SHORT).show();
            }

        }
    }

    public class GetOperationEquipment extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(context);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String user="Chiplun";
            //String user="Pithampur";
                String url= JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.GET_OPERATION_EQUIPMENT + "RD" + "&plantCode=" + "CHNM" + "&operationNo=" + "10";
            response= URL_Constants.makeHttpPostRequest(url);
            Log.d(JK_URL_Constants.TAG, "Whole data = " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray array=jsonObject.getJSONArray("data");
                    if (array.length()==0)
                    {
                        Toast toast = Toast.makeText(context,
                                "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();

                    }
                    else {
                        if (array.length() > 0) {
                            OperationEquipModel model=new OperationEquipModel();
                            model.setEq_name("Select Equipment Name");
                            models.add(model);
                        }


                        for (int i=0;i<array.length();i++)
                        {
                            OperationEquipModel model=new OperationEquipModel();
                            JSONObject object=array.getJSONObject(i);

                            model.setEq_id(object.getString("Eq_id"));
                            model.setEq_name(object.getString("eq_name"));
                            models.add(model);
                        }
                        opearionEqupSpinAdapter=new OpearionEqupSpinAdapter(context,models);
                        spin_equipment.setAdapter(opearionEqupSpinAdapter);

                    }



                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    public class GetQuipmentType extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(context);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";

            String url= JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.GET_EQUIPMENT_WISE_TYPE + "RD" + "&plantCode=" + "CHNM" + "&OpretionNo=" + "377" + "&equipmentName=" + "Cutting Machine";
            String flag1 = url.replaceAll(" ", "%20");
            response= URL_Constants.makeHttpPostRequest(flag1);
            Log.d(JK_URL_Constants.TAG, "Whole data = " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray array= jsonObject.getJSONArray("data");
                    if (array.length()==0)
                    {
                        Toast.makeText(context, "Sorry no data available", Toast.LENGTH_SHORT).show();
                    }
                    else {

                        if (array.length() > 0) {
                            EquipTypeModel model=new EquipTypeModel();
                            model.setEq_type("Select Equipment Type");
                            equipTypelist.add(model);
                        }


                        for (int i=0;i<array.length();i++)
                        {
                            EquipTypeModel model=new EquipTypeModel();
                            JSONObject obj=array.getJSONObject(i);
                            model.setEq_id(obj.getString("Eq_id"));
                            model.setEq_type(obj.getString("eq_type"));

                            equipTypelist.add(model);
                        }
                        equipTypeSpinAdapter=new EquipTypeSpinAdapter(context,equipTypelist);
                        spin_equip_type.setAdapter(equipTypeSpinAdapter);


                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }


            }
        }
    }

    public class GetAssetNo extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(context);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String url= JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.GET__EQUIPMENT_ASSET_NO + "RD" + "&plantCode=" + "CHNM" + "&Euid=" + "182";
            //String flag1 = url.replaceAll(" ", "%20");
            response= URL_Constants.makeHttpPostRequest(url);
            Log.d(JK_URL_Constants.TAG, "Whole data = " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray array= jsonObject.getJSONArray("data");
                    if (array.length()==0)
                    {
                        Toast.makeText(context, "Sorry no data available", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        if (array.length() > 0) {
                            AssetNoModel model=new AssetNoModel();
                            model.setAssetno("Select Asset No");
                            assetNoModels.add(model);
                        }
                        for (int i=0;i<array.length();i++)
                        {
                            AssetNoModel model=new AssetNoModel();
                            model.setAssetno(array.getString(i));
                            assetNoModels.add(model);
                        }
                        assetNoAdapter=new AssetNoAdapter(context,assetNoModels);
                        spin_spin_asset_no.setAdapter(assetNoAdapter);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }


            }
        }
    }

}
