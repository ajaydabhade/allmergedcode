package com.brainlines.weightloggernewversion.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.listener.GoToNewTestTagFillAcitivity;
import com.brainlines.weightloggernewversion.model.LoggersFromDBModel;

import java.io.IOException;
import java.util.ArrayList;

public class LoggerListAdapter extends RecyclerView.Adapter<LoggerListAdapter.MyViewHolder> {

    Context context;
    ArrayList<LoggersFromDBModel> loggerList = new ArrayList<>();
    GoToNewTestTagFillAcitivity listener;

    public LoggerListAdapter(Context context, ArrayList<LoggersFromDBModel> loggerList, GoToNewTestTagFillAcitivity listener) {
        this.context = context;
        this.loggerList = loggerList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.rv_logger_list_item,parent,false);
        return new MyViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final LoggersFromDBModel model = loggerList.get(position);
        if (model != null){
            String inuse = model.getInuse();
            holder.txt_Logger_name.setText(model.getLogger_name());

            if (inuse.equals("true"))
            {
                holder.txt_Logger_name.setTextColor(context.getResources().getColor(R.color.red));
                holder.txt_Logger_name.setClickable(false);
                holder.cv_LoggerName.setClickable(false);
            }
            else
            {
                holder.cv_LoggerName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            listener.getLogger(model.getLogger_name(),model.getIp_address(),model.getMac_id());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }

        }
    }

    @Override
    public int getItemCount() {
        return loggerList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_Logger_name;
        CardView cv_LoggerName;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_Logger_name = (itemView).findViewById(R.id.txt_Logger_name);
            cv_LoggerName = (itemView).findViewById(R.id.cv_LoggerName);
        }
    }
}
