package com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter.FeedbackAdapter;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.FeedbackModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Swas_Feedback_Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "Swas_Feedback_Activity";
    ImageButton SubmitBtn;
    RecyclerView rv_feedback;
    FeedbackAdapter adapter;
    EditText ChatBox;
    String Feedback;
    String oem_id;
    String status_id,service_call_id,type_of_service_status_id,customer_id;
    ImageView img_user_info,nav_view_img,img_back;
    String user_email,user_token,user_role;
    DrawerLayout drawer;
    TextView profile_user,profile_role;
    String created_by="",service_eng_id;
    String currentDate;
    //    DateFormat dateFormat = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss.SSS");
    DateFormat dateFormat = new SimpleDateFormat("E, MMMM dd, yyyy HH:mm:ss.SSS aa");
    ArrayList<FeedbackModel> feedBackModelFromLocalDB = new ArrayList<>();
    ArrayList<FeedbackModel> feedBackModelFromAPi = new ArrayList<>();
    int randomNum;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_swas_feedback_navigation);
        initUi();
        setData();
        currentDate = dateFormat.format(new Date());
        img_back=(ImageView)findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Swas_Feedback_Activity.this, Swas_Home_Actvity.class);
                startActivity(i);
                finish();
            }
        });
        img_user_info=(ImageView)findViewById(R.id.img_userinfo);
        img_user_info.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img=(ImageView)findViewById(R.id.nav_view_img);

        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_planning).setVisible(false);

        Random random = new Random();
        randomNum = Integer.parseInt(String.format("%09d", random.nextInt(1000000000)));

        if (user_email.equals("yogesh@brainlines.in"))
        {
            created_by = "Yogesh Jadhav";
//            service_eng_id = "1";
        }
        else
        {
            created_by = "Deepak Kulkarni";
//            service_eng_id = "2";
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
        {
            status_id = bundle.getString("status_id");
            service_call_id = bundle.getString("service_call_id");
            type_of_service_status_id = bundle.getString("Typeofservicecall_id");
            customer_id = bundle.getString("customer_id");
        }

        if (status_id.equals("5"))
        {
            ChatBox.setEnabled(false);
        }
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
        if(isConnected) {
            Log.d("Network", "Connected");
            getFeedback();

        }
        else{
            checkNetworkConnection();
            Log.d("Network","Not Connected");
            getFeedBackFromLocalDB();

        }

        setData();
        SubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(ChatBox.getText().toString()))
                {
                    Toast.makeText(Swas_Feedback_Activity.this, "enter a message", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

                    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                    boolean isConnected = activeNetwork != null &&
                            activeNetwork.isConnected();
                    if (isConnected){
                        SaveFeedback();
                        Random random = new Random();
                        randomNum = Integer.parseInt(String.format("%09d", random.nextInt(1000000000)));
                    }else {
                        insertFeedbackIntoLocalDB();
                    }

                }
            }
        });
    }

    private void insertFeedbackIntoLocalDB() {
        SQLiteDatabase sqLiteDatabase = new DataBaseHelper(Swas_Feedback_Activity.this).getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(com.brainlines.weightloggernewversion.utils.Constants.SERVICE_CALL_FEEDBACK_FEEDBACK_DETAILS_ID,randomNum);
        contentValues.put(com.brainlines.weightloggernewversion.utils.Constants.SERVICE_CALL_FEEDBACK_OEM_ID,oem_id);
        contentValues.put(com.brainlines.weightloggernewversion.utils.Constants.SERVICE_CALL_FEEDBACK_SERVICE_CALL_ID,service_call_id);
        contentValues.put(com.brainlines.weightloggernewversion.utils.Constants.SERVICE_CALL_FEEDBACK_USER_ID,service_eng_id);
        contentValues.put(com.brainlines.weightloggernewversion.utils.Constants.SERVICE_CALL_FEEDBACK_USER_FEEDBACK,ChatBox.getText().toString());
        contentValues.put(com.brainlines.weightloggernewversion.utils.Constants.SERVICE_CALL_FEEDBACK_CREATED_BY,created_by);
        contentValues.put(com.brainlines.weightloggernewversion.utils.Constants.SERVICE_CALL_FEEDBACK_CREATED_DATE,currentDate);
        contentValues.put(com.brainlines.weightloggernewversion.utils.Constants.SERVICE_CALL_FEEDBACK_IS_SYNC,"0");
        String q = "SELECT * FROM "+com.brainlines.weightloggernewversion.utils.Constants.TABLE_SERVICE_CALL_FEEDBACK+" where "+com.brainlines.weightloggernewversion.utils.Constants.SERVICE_CALL_FEEDBACK_FEEDBACK_DETAILS_ID+"='"+randomNum+"'";
        Cursor c = sqLiteDatabase.rawQuery(q,null);
        if(c.moveToFirst())
        {
            //showMessage("Error", "Record exist");
        }
        else
        {
            long result = sqLiteDatabase.insert(com.brainlines.weightloggernewversion.utils.Constants.TABLE_SERVICE_CALL_FEEDBACK,null,contentValues);
            if (result == -1){
                Log.d(TAG, "onFailure: Data Inserted");
            }else {
                Log.d(TAG, "onFailure: Not Inserted");
            }
        }
    }

    public void initUi() {
        SubmitBtn=(ImageButton)findViewById(R.id.SubmitBtn);
        rv_feedback=(RecyclerView)findViewById(R.id.rv_feedback);
        ChatBox=(EditText)findViewById(R.id.ChatBox);
    }

    private void getFeedBackFromLocalDB() {
        feedBackModelFromLocalDB.clear();
        Cursor cursor = new DataBaseHelper(this).getFeedBackListFromLocalDB(service_call_id);
        while (cursor.moveToNext()){
            String feedBckDetailsId = cursor.getString(1);
            String oemId = cursor.getString(2);
            String serviceCallId = cursor.getString(3);
            String userId = cursor.getString(4);
            String userFeedBack = cursor.getString(5);
            String createdBy = cursor.getString(6);
            String createdDate = cursor.getString(7);
            FeedbackModel feedbackModel = new FeedbackModel();
            feedbackModel.setFeedbackDetailsID(feedBckDetailsId);
            feedbackModel.setOEMID(oemId);
            feedbackModel.setServiceCallID(serviceCallId);
            feedbackModel.setUserID(userId);
            feedbackModel.setUserFeedback(userFeedBack);
            feedbackModel.setCreatedBy(createdBy);
            feedbackModel.setCreatedDate(createdDate);
            feedBackModelFromLocalDB.add(feedbackModel);
        }
        adapter = new FeedbackAdapter(Swas_Feedback_Activity.this,feedBackModelFromLocalDB);
        rv_feedback.setLayoutManager(new LinearLayoutManager(Swas_Feedback_Activity.this, LinearLayoutManager.VERTICAL, false));
        rv_feedback.setAdapter(adapter);
    }

    private void getFeedback() {
        feedBackModelFromAPi.clear();
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getfeedbackdetails(service_call_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");

                            if (array.length() != 0) {
                                for (int i = 0; i < array.length(); i++) {
                                    FeedbackModel feedbackModel = new FeedbackModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    feedbackModel.setFeedbackDetailsID(object1.getString("FeedbackDetailsID"));
                                    feedbackModel.setOEMID(object1.getString("OEMID"));
                                    feedbackModel.setServiceCallID(object1.getString("ServiceCallID"));
                                    feedbackModel.setUserID(object1.getString("UserID"));
                                    feedbackModel.setUserFeedback(object1.getString("UserFeedback"));
                                    feedbackModel.setCreatedBy(object1.getString("CreatedBy"));
                                    feedbackModel.setCreatedDate(object1.getString("CreatedDate"));
                                    feedBackModelFromAPi.add(feedbackModel);
                                }
                                adapter = new FeedbackAdapter(Swas_Feedback_Activity.this,feedBackModelFromAPi);
                                rv_feedback.setLayoutManager(new LinearLayoutManager(Swas_Feedback_Activity.this, LinearLayoutManager.VERTICAL, false));
                                rv_feedback.setAdapter(adapter);


                            } else if (array.length() == 0) {

                            }
                        }
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
    private void SaveFeedback(){ {
        API api = Retroconfig.swasretrofit().create(API.class);
        Feedback=ChatBox.getText().toString();
        Call<ResponseBody> call = api.savefeedbackdetails(service_call_id,oem_id,service_eng_id,Feedback,created_by);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                    }
                    String res = response.body().string();
                    if (res != null) {
                        ChatBox.getText().clear();
                        getFeedback();
//                            JSONObject object = new JSONObject(res);
//                            JSONArray array = object.getJSONArray("data");
//                            ArrayList<SwasIdDetailsModel> model = new ArrayList<>();
//                            if (array.length() != 0) {
//                            }
//                            else if (array.length() == 0) {
//                            }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                SQLiteDatabase sqLiteDatabase = new DataBaseHelper(Swas_Feedback_Activity.this).getWritableDatabase();
                ContentValues contentValues = new ContentValues();
                contentValues.put(com.brainlines.weightloggernewversion.utils.Constants.SERVICE_CALL_FEEDBACK_FEEDBACK_DETAILS_ID,randomNum);
                contentValues.put(com.brainlines.weightloggernewversion.utils.Constants.SERVICE_CALL_FEEDBACK_OEM_ID,oem_id);
                contentValues.put(com.brainlines.weightloggernewversion.utils.Constants.SERVICE_CALL_FEEDBACK_SERVICE_CALL_ID,service_call_id);
                contentValues.put(com.brainlines.weightloggernewversion.utils.Constants.SERVICE_CALL_FEEDBACK_USER_ID,service_eng_id);
                contentValues.put(com.brainlines.weightloggernewversion.utils.Constants.SERVICE_CALL_FEEDBACK_USER_FEEDBACK,Feedback);
                contentValues.put(com.brainlines.weightloggernewversion.utils.Constants.SERVICE_CALL_FEEDBACK_CREATED_BY,created_by);
                contentValues.put(com.brainlines.weightloggernewversion.utils.Constants.SERVICE_CALL_FEEDBACK_CREATED_DATE,currentDate);
                contentValues.put(com.brainlines.weightloggernewversion.utils.Constants.SERVICE_CALL_FEEDBACK_IS_SYNC,"0");
                String q = "SELECT * FROM "+com.brainlines.weightloggernewversion.utils.Constants.TABLE_SERVICE_CALL_FEEDBACK+" where "+com.brainlines.weightloggernewversion.utils.Constants.SERVICE_CALL_FEEDBACK_FEEDBACK_DETAILS_ID+"='"+randomNum+"'";
                Cursor c = sqLiteDatabase.rawQuery(q,null);
                if(c.moveToFirst())
                {
                    //showMessage("Error", "Record exist");
                }
                else
                {
                    long result = sqLiteDatabase.insert(com.brainlines.weightloggernewversion.utils.Constants.TABLE_SERVICE_CALL_FEEDBACK,null,contentValues);
                    if (result == -1){
                        Log.d(TAG, "onFailure: Data Inserted");
                    }else {
                        Log.d(TAG, "onFailure: Not Inserted");
                    }
                }
            }
        });
    }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(Swas_Feedback_Activity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);

        // Set content width and height
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(Swas_Feedback_Activity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }


    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(Swas_Feedback_Activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(com.brainlines.weightloggernewversion.utils.Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();

                    user_email = cursor.getString(cursor.getColumnIndex(com.brainlines.weightloggernewversion.utils.Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(com.brainlines.weightloggernewversion.utils.Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(com.brainlines.weightloggernewversion.utils.Constants.USER_ROLE_NAME));
                    oem_id= cursor.getString(cursor.getColumnIndex(com.brainlines.weightloggernewversion.utils.Constants.USER_OEMID));
                    service_eng_id= cursor.getString(cursor.getColumnIndex(com.brainlines.weightloggernewversion.utils.Constants.USER_LOGIN_ID));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(com.brainlines.weightloggernewversion.utils.Constants.USER_OEMID));
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout()
    {
        finish();
        Toast toast= Toast.makeText(Swas_Feedback_Activity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(Swas_Feedback_Activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(com.brainlines.weightloggernewversion.utils.Constants.TABLE_USER_DETAILS,null,null);

        //Preferences.Logout();

    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id==R.id.nav_swas)
        {
            Intent i=new Intent(Swas_Feedback_Activity.this, Swas_Home_Actvity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    public void checkNetworkConnection(){
        AlertDialog.Builder builder =new AlertDialog.Builder(Swas_Feedback_Activity.this);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
