package com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewPackingTicket.Model;

public class NewPackingTicketDetailsModel {
    String tk_TicketNo,tk_Id,tk_Quantity,tk_SKU,tk_Start_Date,tk_End_Date,tk_Pending,actualstartdate1,actualenddate1,day,createdby,createddate;

    public String getTk_TicketNo() {
        return tk_TicketNo;
    }

    public void setTk_TicketNo(String tk_TicketNo) {
        this.tk_TicketNo = tk_TicketNo;
    }

    public String getTk_Id() {
        return tk_Id;
    }

    public void setTk_Id(String tk_Id) {
        this.tk_Id = tk_Id;
    }

    public String getTk_Quantity() {
        return tk_Quantity;
    }

    public void setTk_Quantity(String tk_Quantity) {
        this.tk_Quantity = tk_Quantity;
    }

    public String getTk_SKU() {
        return tk_SKU;
    }

    public void setTk_SKU(String tk_SKU) {
        this.tk_SKU = tk_SKU;
    }

    public String getTk_Start_Date() {
        return tk_Start_Date;
    }

    public void setTk_Start_Date(String tk_Start_Date) {
        this.tk_Start_Date = tk_Start_Date;
    }

    public String getTk_End_Date() {
        return tk_End_Date;
    }

    public void setTk_End_Date(String tk_End_Date) {
        this.tk_End_Date = tk_End_Date;
    }

    public String getTk_Pending() {
        return tk_Pending;
    }

    public void setTk_Pending(String tk_Pending) {
        this.tk_Pending = tk_Pending;
    }

    public String getActualstartdate1() {
        return actualstartdate1;
    }

    public void setActualstartdate1(String actualstartdate1) {
        this.actualstartdate1 = actualstartdate1;
    }

    public String getActualenddate1() {
        return actualenddate1;
    }

    public void setActualenddate1(String actualenddate1) {
        this.actualenddate1 = actualenddate1;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }
}
