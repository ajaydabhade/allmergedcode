package com.brainlines.weightloggernewversion.FalcoJK.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.PacketTicketTrackingModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.StartTicketPackModel;
import com.brainlines.weightloggernewversion.FalcoJK.Listeners.UpdatePackingTicketeListener;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.UpdatePackTrackingTicketRvAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.Update_Pack_Ticket_adapter;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.Update_Packing_Ticket_Adapter;
import com.brainlines.weightloggernewversion.FalcoJK.UrlConstants.JK_URL_Constants;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class JK_Packing_Tk_Update_Activity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener, UpdatePackingTicketeListener {
    String str_tk_id,str_tk_no,str_tk_startdate,str_tk_end_date,str_tk_qty,str_plant,str_sku;
    TextView txt_up_ticket_no,txt_up_plant,txt_up_tk_qty,txt_sku_13,txt_up_startdate,txt_up_enddate,txt_up_act_strat_date,txt_up_act_end_date;
    RecyclerView rv_update_pack_Data,rv_add_day_data;
    ImageButton img_start_ticket,img_end_ticket,img_add_day;
    TextView txt_up_starttk,txt_up_end_ticket,txt_up_add_Day;
    UpdatePackTrackingTicketRvAdapter updatePackTicketRvAdapter;
    Update_Pack_Ticket_adapter update_pack_ticket_adapter;
    ImageView img_back,img_user_info,nav_view_img;
    DrawerLayout drawer;
    String user_email,user_role,user_token,str_approved_by,str_created_by,str_approve,total_enter_qty;


    ArrayList<StartTicketPackModel> ticketmodel= new ArrayList<>();
    SwipeRefreshLayout swipeto_refresh;
    String dateformat;
    Update_Packing_Ticket_Adapter update_packing_ticket_adapter;
    String add_day_date,str_total_qty,str_pendingqty,str_coml_qty,cal_pendingqty;
    private TextView profile_user,profile_role;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_packing_ticket_update_navigation);
        intiUi();
        setData();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_swas).setVisible(false);
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img=(ImageView)findViewById(R.id.nav_view_img);
        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });

        Bundle bundle=getIntent().getExtras();
        str_tk_id=bundle.getString("tk_id");
        str_tk_no=bundle.getString("tk_no");
        str_tk_startdate=bundle.getString("tk_start_date");
        str_tk_end_date=bundle.getString("tk_end_date");
        str_tk_qty=bundle.getString("tk_quantity");
        str_plant=bundle.getString("tk_plant");
        str_sku=bundle.getString("tk_sku_13");
        img_back=(ImageView)findViewById(R.id.img_back);
        img_user_info=(ImageView)findViewById(R.id.img_userinfo);

        txt_up_add_Day=(TextView)findViewById(R.id.txt_up_add_Day);
        txt_up_end_ticket=(TextView)findViewById(R.id.txt_up_end_ticket);
        txt_up_starttk=(TextView)findViewById(R.id.txt_up_starttk);

        txt_up_ticket_no.setText(str_tk_no);
        txt_up_startdate.setText(str_tk_startdate);
        txt_up_enddate.setText(str_tk_end_date);
        txt_sku_13.setText(str_sku);
        txt_up_tk_qty.setText(str_tk_qty .concat(" no"));
        txt_up_plant.setText(str_plant);
        img_add_day.setOnClickListener(this);
        img_start_ticket.setOnClickListener(this);
        img_end_ticket.setOnClickListener(this);
        img_back.setOnClickListener(this);
        img_user_info.setOnClickListener(this);

        new GetTicketDetailsCall().execute();
        swipeto_refresh=(SwipeRefreshLayout)findViewById(R.id.swipeto_refresh);
        swipeto_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                ticketmodel.clear();
                new GetTicketDetailsCall().execute();
                swipeto_refresh.setRefreshing(false);
            }
        });

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        dateformat = sdf.format(c.getTime());
    }
    public void intiUi()
    {
        txt_up_ticket_no=(TextView)findViewById(R.id.txt_up_ticket_no);
        txt_up_plant=(TextView)findViewById(R.id.txt_up_plant);
        txt_up_tk_qty=(TextView)findViewById(R.id.txt_up_tk_qty);
        txt_sku_13=(TextView)findViewById(R.id.txt_sku_13);
        txt_up_startdate=(TextView)findViewById(R.id.txt_up_startdate);
        txt_up_enddate=(TextView)findViewById(R.id.txt_up_enddate);
        txt_up_act_strat_date=(TextView)findViewById(R.id.txt_up_act_strat_date);
        txt_up_act_end_date=(TextView)findViewById(R.id.txt_up_act_end_date);
        rv_update_pack_Data=(RecyclerView)findViewById(R.id.rv_update_pack_data);
        txt_up_starttk=(TextView)findViewById(R.id.txt_up_starttk);
        rv_add_day_data=(RecyclerView)findViewById(R.id.rv_add_day_data);

        img_start_ticket=(ImageButton)findViewById(R.id.img_start_ticket);
        img_end_ticket=(ImageButton)findViewById(R.id.img_end_ticket);
        img_add_day=(ImageButton)findViewById(R.id.img_add_day);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {
        if (view.getId()==img_start_ticket.getId())
        {
            new StartTicketApiCall().execute();
        }
        if (view.getId()==img_back.getId())
        {
            Intent i=new Intent(JK_Packing_Tk_Update_Activity.this, JKPackingTicketSelection.class);
            startActivity(i);
        }
        if (view.getId()==img_end_ticket.getId())
        {
            new EndPackingTicketCall().execute();
        }
        if (view.getId()==img_user_info.getId())
        {
            displayPopupWindow(view);
        }
        if (view.getId()==img_add_day.getId())
        {
            try {
                rv_add_day_data.setVisibility(View.VISIBLE);
                PacketTicketTrackingModel model=new PacketTicketTrackingModel();
                ArrayList<PacketTicketTrackingModel> models=new ArrayList<>();
                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                String str_date = df.format(Calendar.getInstance().getTime());
                model.setTt_Date(str_date);
                model.setTk_End_Date(model.getTk_End_Date());
                models.add(model);
                updatePackTicketRvAdapter.refreshData(models,str_tk_id,str_tk_no,rv_update_pack_Data);
            }

            catch (Exception e)
            {
                e.printStackTrace();
                Toast.makeText(JK_Packing_Tk_Update_Activity.this,"Please check internet connection",Toast.LENGTH_SHORT).show();
                new GetTicketDetailsCall().execute();
            }


        }
    }
    public class StartTicketApiCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JK_Packing_Tk_Update_Activity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String url = null;
            url= JK_URL_Constants.MAIN_JK_URL+ JK_URL_Constants.START_PACK_TICKET + str_tk_id;

            response= URL_Constants.makeHttpPostRequest(url);
            Log.d(URL_Constants.TAG, "Whole data = " + response);
            return response;

        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray array=jsonObject.getJSONArray("data");
                    if (array.length()==0){
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                    else {
                        ArrayList<StartTicketPackModel> ticketmodel= new ArrayList<>();
                        for (int i=0;i<array.length();i++) {
                            JSONObject obj = new JSONObject(array.get(i).toString());
                            StartTicketPackModel model = new StartTicketPackModel();


                            model.setTk_TicketNo(obj.getString("tk_TicketNo"));
                            model.setTk_Id(obj.getString("tk_Id"));
                            model.setTk_Plant(obj.getString("tk_TicketNo"));
                            model.setTk_Quantity(obj.getString("tk_Quantity"));
                            model.setTk_SKU(obj.getString("tk_SKU"));
                            model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                            model.setTk_End_Date(obj.getString("tk_End_Date"));
                            model.setTk_Pending(obj.getString("tk_Pending"));
                            model.setActualstartdate1(obj.getString("actualstartdate1"));
                            model.setActualenddate1(obj.getString("actualenddate1"));
                            model.setDay(URL_Constants.DateConversion(obj.getString("day")));

                            String ticket_no=obj.getString("tk_TicketNo");
                            String tk_plant=obj.getString("tk_Plant");
                            String tk_quantity=obj.getString("tk_Quantity");
                            String sku_13=obj.getString("tk_SKU");
                            String startdate=obj.getString("tk_Start_Date");
                            String end_date=obj.getString("tk_End_Date");
                            String pendng_qty=obj.getString("tk_Pending");
                            String act_start_date=obj.getString("actualstartdate1");
                            String act_end_date=obj.getString("actualenddate1");
                            String day=obj.getString("day");
                            txt_up_tk_qty.setText(tk_quantity);

                            String enddate=obj.getString("actualenddate1");
                            String actstartdate=obj.getString("actualstartdate1");
                            if (actstartdate.equals("null"))
                            {
                                txt_up_act_strat_date.setText("NA");
                            }
                            else
                            {
                                txt_up_act_strat_date.setText(URL_Constants.DateConversion(act_start_date));
                            }

                            if (enddate.equals("null"))
                            {
                                txt_up_act_end_date.setText("NA");
                            }
                            else
                            {
                                txt_up_act_end_date.setText(URL_Constants.DateConversion(act_end_date));
                            }

                            txt_up_ticket_no.setText(ticket_no);
                            txt_up_plant.setText(tk_plant);
                            txt_sku_13.setText(sku_13);
                            txt_up_tk_qty.setText(pendng_qty);

                            img_start_ticket.setVisibility(View.INVISIBLE);
                            txt_up_starttk.setVisibility(View.INVISIBLE);
                            String str[]=day.split("T");
                            String date=str[0];
                            if (dateformat.equals(date))
                            {
                                ticketmodel.add(model);
                            }


                        }
                        update_packing_ticket_adapter=new Update_Packing_Ticket_Adapter(JK_Packing_Tk_Update_Activity.this,ticketmodel, JK_Packing_Tk_Update_Activity.this);
                        rv_update_pack_Data.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                        rv_update_pack_Data.setAdapter(update_pack_ticket_adapter);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

        }
    }

    public class GetTicketDetailsCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JK_Packing_Tk_Update_Activity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String url = null;
            url= JK_URL_Constants.MAIN_JK_URL+ JK_URL_Constants.GET_TICKET_DETAILS + str_tk_id;
            response= URL_Constants.makeHttpPutRequest(url);
            Log.d(URL_Constants.TAG, "Whole data = " + response);
            return response;
        }
        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray array=jsonObject.getJSONArray("data");
                    if (array.length()==0){
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();

                    }
                    else {
                        ArrayList<PacketTicketTrackingModel> ticketlist = new ArrayList<>();

                        for (int i=0;i<array.length();i++) {
                            JSONObject obj = new JSONObject(array.get(i).toString());
                            PacketTicketTrackingModel model = new PacketTicketTrackingModel();
                            model.setTt_id(obj.getString("tt_id"));
                            model.setTt_TicketID(obj.getString("tt_TicketID"));
                            model.setTt_TicketNo(obj.getString("tt_TicketNo"));
                            model.setTt_Plant(obj.getString("tt_Plant"));
                            model.setTt_Date(obj.getString("tt_Date"));
                            model.setTt_TotalQuantity(obj.getString("tt_TotalQuantity"));
                            model.setTt_Qty(obj.getString("tt_Qty"));
                            model.setTt_PendingQty(obj.getString("tt_PendingQty"));
                            model.setTt_CreatedDate(obj.getString("tt_CreatedDate"));
                            model.setTt_CreatedBy(obj.getString("tt_CreatedBy"));
                            model.setTt_ApprovedDate(obj.getString("tt_ApprovedDate"));
                            model.setTt_ApprovedBy(obj.getString("tt_ApprovedBy"));
                            model.setTt_Active(obj.getString("tt_Active"));
                            model.setTt_Approve(obj.getString("tt_Approve"));
                            model.setFlag1(obj.getString("flag1"));
                            model.setTk_SKU(obj.getString("tk_SKU"));
                            model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                            model.setTk_End_Date(obj.getString("tk_End_Date"));
                            model.setActualstartdate(obj.getString("actualstartdate"));
                            model.setActualenddate(obj.getString("actualenddate"));
                            model.setToday(obj.getString("today"));
                            ticketlist.add(model);

                            String active=obj.getString                                                                                                                                                                                                                                                                        ("tt_Active");
                            String enddate=obj.getString("actualenddate");
                            String actstartdate=obj.getString("actualstartdate");
                            if (actstartdate.equals("null"))
                            {
                                txt_up_act_strat_date.setText("NA");
                            }
                            else
                            {
                                txt_up_act_strat_date.setText(obj.getString("actualstartdate"));
                            }

                            if (enddate.equals("null"))
                            {
                                txt_up_act_end_date.setText("NA");
                            }
                            else
                            {
                                txt_up_act_end_date.setText(enddate);
                            }

                            str_approve=obj.getString("tt_Approve");
                            str_approved_by=obj.getString("tt_ApprovedBy");
                            str_created_by=obj.getString("tt_CreatedBy");

                            if (active.equals("true"))
                            {
                                img_start_ticket.setVisibility(View.INVISIBLE);
                                txt_up_starttk.setVisibility(View.INVISIBLE);
                            }
                            if (!enddate.equals("null"))
                            {
                                img_end_ticket.setVisibility(View.INVISIBLE);
                                img_add_day.setVisibility(View.INVISIBLE);
                                txt_up_end_ticket.setVisibility(View.INVISIBLE);
                                txt_up_add_Day.setVisibility(View.INVISIBLE);
                            }
                            if (obj.getString("tt_PendingQty").equals("0"))
                            {
                                img_add_day.setVisibility(View.INVISIBLE);
                                txt_up_add_Day.setVisibility(View.INVISIBLE);
                            }
                        }

                        updatePackTicketRvAdapter=new UpdatePackTrackingTicketRvAdapter(JK_Packing_Tk_Update_Activity.this,ticketlist, JK_Packing_Tk_Update_Activity.this);
                        rv_update_pack_Data.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                        rv_update_pack_Data.setAdapter(updatePackTicketRvAdapter);
                    }
                    for (int i=0;i<ticketmodel.size();i++)
                    {
                        String pendingqty = "" ,str_act_endate="";
                        pendingqty = ticketmodel.get(i).getTk_Pending();
                        str_act_endate = ticketmodel.get(i).getActual_end_date();

                        if (pendingqty.equals("0") && str_act_endate.equals("null"))
                        {
                            new EndPackingTicketCall().execute();
                        }
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

        }
    }
    public class DisplayGetTicketDetailsCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JK_Packing_Tk_Update_Activity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String url = null;
            url= JK_URL_Constants.MAIN_JK_URL+ JK_URL_Constants.GET_TICKET_DETAILS + str_tk_id;
            response= URL_Constants.makeHttpPutRequest(url);
            Log.d(URL_Constants.TAG, "Whole data = " + response);
            return response;
        }
        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray array=jsonObject.getJSONArray("data");
                    if (array.length()==0){
                        /*Toast toast = Toast.makeText(getApplicationContext(),
                                "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();*/


                        new SavePackingTicketTrackingCall().execute();


                    }
                    else {
                        new UpdatePackingTicketTrackingCall().execute();


                    }

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

        }
    }

    public class EndPackingTicketCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JK_Packing_Tk_Update_Activity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String url = null;
            url= JK_URL_Constants.MAIN_JK_URL+ JK_URL_Constants.END_PACK_TICKET + str_tk_id + "&tk_CreatedBy=" + JK_URL_Constants.PLANT_USER;
            response= URL_Constants.makeHttpPutRequest(url);
            Log.d(URL_Constants.TAG, "Whole data = " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try
                {
                    JSONObject object=new JSONObject(response);
                    String str=object.getString("data");
                    if (str.equals("1"))
                    {
                        Toast.makeText(JK_Packing_Tk_Update_Activity.this, "This ticket is ended", Toast.LENGTH_SHORT).show();
                        /*Intent i=new Intent(JKUpdateTicketActivity.this, FalcoJKHomeActivity.class);
                        startActivity(i);*/
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    public class UpdatePackingTicketTrackingCall  extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JK_Packing_Tk_Update_Activity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String str_user_id="capacity@decintell.com";
            String url1= JK_URL_Constants.MAIN_JK_URL;

            String url2=url1 + JK_URL_Constants.INSERT_PACKING_UPDATE_TICKET + str_tk_id +"&tt_CompletedQty=" + str_coml_qty + "&tt_PendingQty=" + cal_pendingqty + "&UserId=" + str_user_id
                    + "&TicketIDNo=" + str_tk_no;
            response= URL_Constants.makeHttpPostRequest(url2);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try {
                    JSONObject object=new JSONObject(response);
                    String status=object.getString("data");

                    if (status.equals("1"))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(JK_Packing_Tk_Update_Activity.this);
                        //builder.setTitle("Confirm");
                        builder.setMessage("You successfully updated the record");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {

                                // Do nothing, but close the dialog
                                dialog.dismiss();
                                new GetTicketDetailsCall().execute();
                            }
                        });

                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }

            }
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(JK_Packing_Tk_Update_Activity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);
        setData();

        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(JK_Packing_Tk_Update_Activity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }
    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(JK_Packing_Tk_Update_Activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();

                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout()
    {
        finish();
        Toast toast= Toast.makeText(JK_Packing_Tk_Update_Activity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(JK_Packing_Tk_Update_Activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);
    }

    @Override
    public void imgactionSubmitIsClicked(StartTicketPackModel model) {

        String str = model.toString();
        if (model != null) {
            {
                str_tk_id = model.getTk_Id();
                str_tk_no = model.getTk_TicketNo();
                str_plant=model.getTk_Plant();
                add_day_date=model.getDay();
                str_total_qty=model.getTk_Quantity();
                str_pendingqty = model.getStr_txt_pending_qty();

                new DisplayGetTicketDetailsCall().execute();
            }
        }

    }

    @Override
    public void imgactintrackingSubmit(PacketTicketTrackingModel model) {
        String str = model.toString();
        if (model != null) {
            {
                str_tk_id = model.getTt_id();
                str_tk_no = model.getTt_TicketNo();
                str_plant=model.getTt_Plant();
                add_day_date=model.getTt_Date();
                str_total_qty=model.getTt_TotalQuantity();
                cal_pendingqty = model.getStr_pendingqty();
                str_coml_qty = model.getStr_txt_compl_qty();
                cal_pendingqty= URL_Constants.packsubstraction(cal_pendingqty,str_coml_qty);

                new DisplayGetTicketDetailsCall().execute();
            }
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id== R.id.nav_planning)
        {
            Intent i=new Intent(JK_Packing_Tk_Update_Activity.this, FalcoJKHomeActivity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    public class SavePackingTicketTrackingCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JK_Packing_Tk_Update_Activity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String str_user_id="capacity@decintell.com";

            str_pendingqty= URL_Constants.packsubstraction(str_coml_qty,cal_pendingqty);
            String url1= JK_URL_Constants.MAIN_JK_URL;
            String url2=url1 + JK_URL_Constants.FIRST_TIME_SAVE_PACKUNPACKTK + str_tk_id + "&tt_TicketNo=" + str_tk_no + "&tt_Plant=" + "CHNM" + "&tt_Date=" + add_day_date
                    + "&tt_TotalQuantity=" + str_total_qty + "&tt_Qty=" + str_coml_qty + "&tt_PendingQty=" + str_pendingqty + "&tt_CreatedBy="
                    + str_user_id + "&tt_ApprovedBy=" + str_user_id + "&tt_Approve=" + "N";

            response= URL_Constants.makeHttpPostRequest(url2);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try {
                    JSONObject object=new JSONObject(response);
                    String status=object.getString("data");
                    String abc=object.getString("status");
                    if (abc.equals("true"))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(JK_Packing_Tk_Update_Activity.this);

                        //builder.setTitle("Confirm");
                        builder.setMessage("You successfully inserted the quantity");

                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {

                                // Do nothing, but close the dialog
                                dialog.dismiss();
                                new GetTicketDetailsCall().execute();
                            }
                        });

                    }



                    if (status.equals("1")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(JK_Packing_Tk_Update_Activity.this);

                        //builder.setTitle("Confirm");
                        builder.setMessage("You successfully update the record");

                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {

                                // Do nothing, but close the dialog
                                dialog.dismiss();
                                new GetTicketDetailsCall().execute();
                            }
                        });
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }

            }
        }
    }
}
