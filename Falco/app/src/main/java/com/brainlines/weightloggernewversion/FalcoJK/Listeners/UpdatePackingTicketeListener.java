package com.brainlines.weightloggernewversion.FalcoJK.Listeners;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.PacketTicketTrackingModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.StartTicketPackModel;

public interface UpdatePackingTicketeListener {

    void imgactionSubmitIsClicked(StartTicketPackModel model);

    void imgactintrackingSubmit(PacketTicketTrackingModel model);
}
