package com.brainlines.weightloggernewversion.FalcoJK.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.ProdTicketModel;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.ProductionRvAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.ProductionTicketistWithoutSelection_Adapter;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.SpinnerAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.UrlConstants.JK_URL_Constants;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class JK_Production_Selection_list extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    Spinner spin_pro_Release,spin_ticket;
    RecyclerView rv_produtcion_list,rv_pro_all_tk_list;
    ProductionRvAdapter adapter;
    ImageView img_back,img_use_info,nav_view_img;
    DrawerLayout drawer;
    String user_email,user_role,user_token;
    String tickettype,str_active_close_ticket;
    ProductionTicketistWithoutSelection_Adapter productionTicketistWithoutSelection_adapter;
    SwipeRefreshLayout swipeto_refresh;
    private TextView profile_user,profile_role;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_production_ticket_selection_navigation);
        setData();
        rv_produtcion_list = findViewById(R.id.rv_production_ticket_list);
        swipeto_refresh = findViewById(R.id.swipeto_refresh);
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img = findViewById(R.id.nav_view_img);
        initUi();
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i=new Intent(JK_Production_Selection_list.this,Falco_Home_Main_Activity.class);
//                startActivity(i);
            }
        });

        img_use_info = findViewById(R.id.img_userinfo);
        img_use_info.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_swas).setVisible(false);

        //new GetActiveTicketsCall().execute();
        new GetProductionTicketList().execute();

        final ArrayList<String> arrayticket_type = new ArrayList<>();
        arrayticket_type.add("Select Released");
        arrayticket_type.add("ReleasedTicket");
        arrayticket_type.add("UnReleasedTicket");
        SpinnerAdapter arrayAdapter = new SpinnerAdapter(this, arrayticket_type);
        spin_pro_Release.setAdapter(arrayAdapter);

        final ArrayList<String> arrayticket = new ArrayList<>();
        arrayticket.add("Select Ticket");
        arrayticket.add("ActiveTicket");
        arrayticket.add("ClosedTicket");
        SpinnerAdapter arrayAdapter1 = new SpinnerAdapter(this, arrayticket);
        spin_ticket.setAdapter(arrayAdapter1);

        spin_pro_Release.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                tickettype=arrayticket_type.get(i).toString();
                //new GetProReleasdTicketCall().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        spin_ticket.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                str_active_close_ticket=arrayticket.get(i).toString();
                //new GetActiveTicketsCall().execute();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        swipeto_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new GetProductionTicketList().execute();
                swipeto_refresh.setRefreshing(false);
            }
        });
    }
    public void initUi()
    {
        spin_pro_Release=(Spinner)findViewById(R.id.spin_pro_release);
        spin_ticket=(Spinner)findViewById(R.id.spin_pro_ticket);
    }


    public class GetProductionTicketList extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JK_Production_Selection_list.this);
            progressDialog.setMessage("Please Wait");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setProgress(0);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String user= JK_URL_Constants.PLANT;
            String url= JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.GET_PROD_TICKETS + user ;
            response= URL_Constants.makeHttpGetRequest(url);
            Log.d(JK_URL_Constants.TAG, "Whole data = " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray array=jsonObject.getJSONArray("Data");
                    if (array.length()==0){
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                    else {
                        ArrayList<ProdTicketModel> ticketmodel= new ArrayList<>();

                        for (int i=0;i<array.length();i++)
                        {

                            JSONObject obj=new JSONObject(array.get(i).toString());

                            ProdTicketModel model =new ProdTicketModel();
                            if (obj.has("ptkd_ReleaseDate"))
                            {
                                if(obj.has("tk_End_Date"))
                                {
                                    model.setPtkd_PktId(obj.getString("ptkd_PktId"));
                                    model.setPtkd_TkNo(obj.getString("ptkd_TkNo"));
                                    model.setPtkd_PSKU(obj.getString("ptkd_PSKU"));
                                    model.setPtkd_Plant(obj.getString("ptkd_Plant"));
                                    model.setPtkd_ValueStreamCode(obj.getString("ptkd_ValueStreamCode"));
                                    model.setPtkd_Ftypecode(obj.getString("ptkd_Ftypecode"));
                                    model.setPtkd_Qty(obj.getString("ptkd_Qty"));
                                    model.setPtkd_IsRelease(obj.getString("ptkd_IsRelease"));
                                    model.setTk_Actual_Start_Date(obj.getString("tk_Actual_Start_Date"));
                                    model.setTk_Actual_End_Date(obj.getString("tk_Actual_End_Date"));
                                    model.setPtkd_IsRelease1(obj.getString("ptkd_IsRelease1"));
                                    model.setPtkd_ReleaseDate(obj.getString("ptkd_ReleaseDate"));
                                    model.setPtkd_Active(obj.getString("ptkd_Active"));
                                    model.setTk_act_End_Date(obj.getString("tk_End_Date"));
                                    ticketmodel.add(model);
                                }
                                else
                                {
                                    model.setPtkd_PktId(obj.getString("ptkd_PktId"));
                                    model.setPtkd_TkNo(obj.getString("ptkd_TkNo"));
                                    model.setPtkd_PSKU(obj.getString("ptkd_PSKU"));
                                    model.setPtkd_Plant(obj.getString("ptkd_Plant"));
                                    model.setPtkd_ValueStreamCode(obj.getString("ptkd_ValueStreamCode"));
                                    model.setPtkd_Ftypecode(obj.getString("ptkd_Ftypecode"));
                                    model.setPtkd_Qty(obj.getString("ptkd_Qty"));
                                    model.setPtkd_IsRelease(obj.getString("ptkd_IsRelease"));
                                    model.setTk_Actual_Start_Date(obj.getString("tk_Actual_Start_Date"));
                                    model.setTk_Actual_End_Date(obj.getString("tk_Actual_End_Date"));
                                    model.setPtkd_IsRelease1(obj.getString("ptkd_IsRelease1"));
                                    model.setPtkd_ReleaseDate(obj.getString("ptkd_ReleaseDate"));
                                    model.setPtkd_Active(obj.getString("ptkd_Active"));
                                    model.setTk_act_End_Date(null);
                                    ticketmodel.add(model);
                                }

                            }
                            else {
                                if (obj.has("tk_End_Date"))
                                {
                                    model.setPtkd_PktId(obj.getString("ptkd_PktId"));
                                    model.setPtkd_TkNo(obj.getString("ptkd_TkNo"));
                                    model.setPtkd_PSKU(obj.getString("ptkd_PSKU"));
                                    model.setPtkd_Plant(obj.getString("ptkd_Plant"));
                                    model.setPtkd_ValueStreamCode(obj.getString("ptkd_ValueStreamCode"));
                                    model.setPtkd_Ftypecode(obj.getString("ptkd_Ftypecode"));
                                    model.setPtkd_Qty(obj.getString("ptkd_Qty"));
                                    model.setPtkd_IsRelease(obj.getString("ptkd_IsRelease"));
                                    model.setTk_Actual_Start_Date(obj.getString("tk_Actual_Start_Date"));
                                    model.setTk_Actual_End_Date(obj.getString("tk_Actual_End_Date"));
                                    model.setPtkd_IsRelease1(obj.getString("ptkd_IsRelease1"));
                                    model.setPtkd_ReleaseDate("null");
                                    model.setPtkd_Active(obj.getString("ptkd_Active"));
                                    model.setTk_act_End_Date(obj.getString("tk_End_Date"));
                                    ticketmodel.add(model);
                                }
                                else
                                {
                                    model.setPtkd_PktId(obj.getString("ptkd_PktId"));
                                    model.setPtkd_TkNo(obj.getString("ptkd_TkNo"));
                                    model.setPtkd_PSKU(obj.getString("ptkd_PSKU"));
                                    model.setPtkd_Plant(obj.getString("ptkd_Plant"));
                                    model.setPtkd_ValueStreamCode(obj.getString("ptkd_ValueStreamCode"));
                                    model.setPtkd_Ftypecode(obj.getString("ptkd_Ftypecode"));
                                    model.setPtkd_Qty(obj.getString("ptkd_Qty"));
                                    model.setPtkd_IsRelease(obj.getString("ptkd_IsRelease"));
                                    model.setTk_Actual_Start_Date(obj.getString("tk_Actual_Start_Date"));
                                    model.setTk_Actual_End_Date(obj.getString("tk_Actual_End_Date"));
                                    model.setPtkd_IsRelease1(obj.getString("ptkd_IsRelease1"));
                                    model.setPtkd_ReleaseDate("null");
                                    model.setPtkd_Active(obj.getString("ptkd_Active"));
                                    model.setTk_act_End_Date(null);
                                    ticketmodel.add(model);
                                }
                            }
                        }
                        adapter=new ProductionRvAdapter(JK_Production_Selection_list.this,ticketmodel);
                        rv_produtcion_list.setLayoutManager(new LinearLayoutManager(JK_Production_Selection_list.this,LinearLayoutManager.VERTICAL, false));
                        rv_produtcion_list.setAdapter(adapter);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(JK_Production_Selection_list.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);
        setData();
        // Set content width and height
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(JK_Production_Selection_list.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }

    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(JK_Production_Selection_list.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);
        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();
                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout()
    {
        finish();
        Toast toast= Toast.makeText(JK_Production_Selection_list.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(JK_Production_Selection_list.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);

        //Preferences.Logout();

    }

    public class GetProReleasdTicketCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JK_Production_Selection_list.this);
            progressDialog.setMessage("Please Wait");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setProgress(0);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String user="Chiplun";
            //String user="Pithampur";
            String url= JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.GET_PROD_TICKETS + user ;
            response= URL_Constants.makeHttpGetRequest(url);
            Log.d(JK_URL_Constants.TAG, "Whole data = " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray array=jsonObject.getJSONArray("Data");
                    if (array.length()==0){
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                    else {
                        ArrayList<ProdTicketModel> ticketmodel= new ArrayList<>();

                        for (int i=0;i<array.length();i++)
                        {


                            JSONObject obj=new JSONObject(array.get(i).toString());
                            ProdTicketModel model =new ProdTicketModel();
                            if(tickettype.equals("ReleasedTicket"))
                            {

                                if (obj.getString("ptkd_IsRelease1").equals("true"))
                                {
                                    model.setPtkd_PktId(obj.getString("ptkd_PktId"));
                                    model.setPtkd_TkNo(obj.getString("ptkd_TkNo"));
                                    model.setPtkd_PSKU(obj.getString("ptkd_PSKU"));
                                    model.setPtkd_Plant(obj.getString("ptkd_Plant"));
                                    model.setPtkd_ValueStreamCode(obj.getString("ptkd_ValueStreamCode"));
                                    model.setPtkd_Ftypecode(obj.getString("ptkd_Ftypecode"));
                                    model.setPtkd_Qty(obj.getString("ptkd_Qty"));
                                    model.setPtkd_IsRelease(obj.getString("ptkd_IsRelease"));
                                    model.setTk_Actual_Start_Date(obj.getString("tk_Actual_Start_Date"));
                                    model.setTk_Actual_End_Date(obj.getString("tk_Actual_End_Date"));
                                    model.setPtkd_IsRelease1(obj.getString("ptkd_IsRelease1"));
                                    model.setPtkd_ReleaseDate(obj.getString("ptkd_ReleaseDate"));
                                    model.setPtkd_Active(obj.getString("ptkd_Active"));
                                    //model.setTk_act_End_Date(obj.getString("tk_End_Date"));
                                    ticketmodel.add(model);
                                }
                            }

                            if(tickettype.equals("UnReleasedTicket"))
                            {
                                if (obj.getString("ptkd_IsRelease1").equals("false"))
                                {
                                    model.setPtkd_PktId(obj.getString("ptkd_PktId"));
                                    model.setPtkd_TkNo(obj.getString("ptkd_TkNo"));
                                    model.setPtkd_PSKU(obj.getString("ptkd_PSKU"));
                                    model.setPtkd_Plant(obj.getString("ptkd_Plant"));
                                    model.setPtkd_ValueStreamCode(obj.getString("ptkd_ValueStreamCode"));
                                    model.setPtkd_Ftypecode(obj.getString("ptkd_Ftypecode"));
                                    model.setPtkd_Qty(obj.getString("ptkd_Qty"));
                                    model.setPtkd_IsRelease(obj.getString("ptkd_IsRelease"));
                                    model.setTk_Actual_Start_Date(obj.getString("tk_Actual_Start_Date"));
                                    model.setTk_Actual_End_Date(obj.getString("tk_Actual_End_Date"));
                                    model.setPtkd_IsRelease1(obj.getString("ptkd_IsRelease1"));
                                    model.setPtkd_ReleaseDate(obj.getString("ptkd_ReleaseDate"));
                                    model.setPtkd_Active(obj.getString("ptkd_Active"));
                                    //model.setTk_act_End_Date(obj.getString("tk_End_Date"));
                                    ticketmodel.add(model);
                                }
                            }
                        }
                        adapter=new ProductionRvAdapter(JK_Production_Selection_list.this,ticketmodel);
                        rv_produtcion_list.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL, false));
                        rv_produtcion_list.setAdapter(adapter);
                    }
                }
                catch (Exception e){e.printStackTrace();
                }
            }
        }

    }
    public class GetActiveTicketsCall extends AsyncTask<String,String,String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(JK_Production_Selection_list.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response = "";
            String user = "Chiplun";
            //String user="Pithampur";
            String url = JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.GET_PROD_TICKETS + user;
            response = URL_Constants.makeHttpGetRequest(url);
            Log.d(JK_URL_Constants.TAG, "Whole data = " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            if (!response.equals("Error")) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray array = jsonObject.getJSONArray("Data");
                    if (array.length() == 0) {
                        progressDialog.dismiss();
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    } else {
                        ArrayList<ProdTicketModel> ticketmodel = new ArrayList<>();
                        int limit = array.length();
                        if (limit > 200)
                            limit = 200;

                        for (int i = 0; i < limit; i++) {
                            JSONObject obj = new JSONObject(array.get(i).toString());
                            ProdTicketModel model = new ProdTicketModel();
                            if (str_active_close_ticket.equals("ActiveTicket")) {

                                if (obj.getString("ptkd_Active").equals("true")) {
                                    model.setPtkd_PktId(obj.getString("ptkd_PktId"));
                                    model.setPtkd_TkNo(obj.getString("ptkd_TkNo"));
                                    model.setPtkd_PSKU(obj.getString("ptkd_PSKU"));
                                    model.setPtkd_Plant(obj.getString("ptkd_Plant"));
                                    model.setPtkd_ValueStreamCode(obj.getString("ptkd_ValueStreamCode"));
                                    model.setPtkd_Ftypecode(obj.getString("ptkd_Ftypecode"));
                                    model.setPtkd_Qty(obj.getString("ptkd_Qty"));
                                    model.setPtkd_IsRelease(obj.getString("ptkd_IsRelease"));
                                    model.setTk_Actual_Start_Date(obj.getString("tk_Actual_Start_Date"));
                                    model.setTk_Actual_End_Date(obj.getString("tk_Actual_End_Date"));
                                    model.setPtkd_IsRelease1(obj.getString("ptkd_IsRelease1"));
                                    model.setPtkd_ReleaseDate(obj.getString("ptkd_ReleaseDate"));
                                    model.setPtkd_Active(obj.getString("ptkd_Active"));
                                    //model.setTk_act_End_Date(obj.getString("tk_End_Date"));
                                    ticketmodel.add(model);
                                }
                            }


                            if (str_active_close_ticket.equals("ClosedTicket")) {

                                if (obj.getString("ptkd_Active").equals("false")) {
                                    model.setPtkd_PktId(obj.getString("ptkd_PktId"));
                                    model.setPtkd_TkNo(obj.getString("ptkd_TkNo"));
                                    model.setPtkd_PSKU(obj.getString("ptkd_PSKU"));
                                    model.setPtkd_Plant(obj.getString("ptkd_Plant"));
                                    model.setPtkd_ValueStreamCode(obj.getString("ptkd_ValueStreamCode"));
                                    model.setPtkd_Ftypecode(obj.getString("ptkd_Ftypecode"));
                                    model.setPtkd_Qty(obj.getString("ptkd_Qty"));
                                    model.setPtkd_IsRelease(obj.getString("ptkd_IsRelease"));
                                    model.setTk_Actual_Start_Date(obj.getString("tk_Actual_Start_Date"));
                                    model.setTk_Actual_End_Date(obj.getString("tk_Actual_End_Date"));
                                    model.setPtkd_IsRelease1(obj.getString("ptkd_IsRelease1"));
                                    model.setPtkd_ReleaseDate(obj.getString("ptkd_ReleaseDate"));
                                    model.setPtkd_Active(obj.getString("ptkd_Active"));
                                    //model.setTk_act_End_Date(obj.getString("tk_End_Date"));
                                    ticketmodel.add(model);
                                }
                            }
                        }
                        adapter = new ProductionRvAdapter(JK_Production_Selection_list.this, ticketmodel);
                        rv_produtcion_list.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                        rv_produtcion_list.setAdapter(adapter);
                        progressDialog.dismiss();


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id==R.id.nav_planning)
        {
            Intent i=new Intent(JK_Production_Selection_list.this,FalcoJKHomeActivity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }
}
