package com.brainlines.weightloggernewversion;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.brainlines.weightloggernewversion.FalcoJK.activity.FalcoJKHomeActivity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Falco_SWAS_Main_Activity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Home_Actvity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModelToStoreInDB;
import com.brainlines.weightloggernewversion.adapter.MenuAdapter;
import com.brainlines.weightloggernewversion.adapter.UserDataListAdapter;
import com.brainlines.weightloggernewversion.listener.UserDataListener;
import com.brainlines.weightloggernewversion.model.MenuModel;
import com.brainlines.weightloggernewversion.model.UserModel;
import com.brainlines.weightloggernewversion.utils.GlobalClass;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class ModuleSelectionActivity extends AppCompatActivity implements  UserDataListener {
    private RecyclerView rvUserDataList;
    private UserDataListAdapter adapter;
    private MenuAdapter menuAdapter;
    private String user_email,user_token,user_role,user_oem_id;
    private UserModelToStoreInDB modelToStoreInDB;
    private ArrayList<UserModel> userModelList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_module_selection);

        rvUserDataList = findViewById(R.id.rvUserDataList);

        userModelList = getIntent().getParcelableArrayListExtra("UserDataList");
        ArrayList<UserModel> uniqueItemList = unique(userModelList);
        modelToStoreInDB = getIntent().getParcelableExtra("UserModel");
        user_role = getIntent().getStringExtra("user_role");
        user_oem_id = getIntent().getStringExtra("oemid");

        LinearLayoutManager manager = new LinearLayoutManager(this);
        rvUserDataList.setLayoutManager(manager);
        adapter = new UserDataListAdapter(this,uniqueItemList,this);
        rvUserDataList.setAdapter(adapter);
    }

    @Override
    public void sendUserData(UserModel model) {
        if (model != null){
            GlobalClass.getDbNameBasedOnModuleSelected(model,this);
            switch (model.getModuleID()){
                case 1:
                    //SWAS Module Flow
                    Intent moduleIdOne = new Intent(this, Swas_Home_Actvity.class);
                    moduleIdOne.putExtra("UserDataModel",model);
                    startActivity(moduleIdOne);
                    break;
                case 2:
                    //DOIT Module Flow
                    break;
                case 3:
                    //WAC Module Flow.
                    Intent moduleIdThree = new Intent(this, WAC_CalibrationOrProductionActivity.class);
                    moduleIdThree.putExtra("UserDataModel",model);
                    startActivity(moduleIdThree);
                    break;
                case 4:
                    //MyKRA Module Flow
                    break;
                case 9:
                    //Admin Settings Module Flow
                    break;
                case 13:
                    //Planning Module
                    Intent moduleIdThirteen = new Intent(this, FalcoJKHomeActivity.class);
                    moduleIdThirteen.putExtra("UserDataModel",model);
                    startActivity(moduleIdThirteen);
                    break;
            }
        }
    }

    private ArrayList<UserModel> unique(ArrayList<UserModel> list) {
        ArrayList<UserModel> uniqueList = new ArrayList<>();
        Set<UserModel> uniqueSet = new HashSet<>();
        for (UserModel obj : list) {
            if (uniqueSet.add(obj)) {
                uniqueList.add(obj);
            }
        }
        return uniqueList;
    }

    private ArrayList<MenuModel> uniqueMenuList(ArrayList<MenuModel> list) {
        ArrayList<MenuModel> uniqueList = new ArrayList<>();
        Set<MenuModel> uniqueSet = new HashSet<>();
        for (MenuModel obj : list) {
            if (uniqueSet.add(obj)) {
                uniqueList.add(obj);
            }
        }
        return uniqueList;
    }
//http://fgmobileapi.decintell.co.in/SWAS_Stg1/api/providers/Menus
//    private void GetMenus() {
//        API api = Retroconfig.MenuRetrofit().create(API.class);
//        Call<ResponseBody> call = api.getMenue(user_oem_id,user_role);
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                try {
//                    if(response.body()!=null)
//                    {
//                        String res = response.body().string();
//                        if (res!=null)
//                        { try {
//                            JSONObject object = new JSONObject(res);
//                            JSONArray array = object.getJSONArray("data");
//                            ArrayList<MenuModel> menuModels = new ArrayList<>();
//                            for(int i = 0;i<array.length();i++)
//                            {
//                                JSONObject obj = array.getJSONObject(i);
//                                MenuModel model = new MenuModel(obj.getString("RoleName"),obj.getString("OEMId"),
//                                        obj.getString("Module_ID"),obj.getString("ModuleName"));
////                                model.setModule_ID(obj.getString("Module_ID"));
////                                model.setModuleName(obj.getString("ModuleName"));
////                                model.setOEMId(obj.getString("OEMId"));
////                                model.setRoleName(obj.getString("RoleName"));
//                                menuModels.add(model);
//                            }
//                            ArrayList<MenuModel> uniqueItemList = uniqueMenuList(menuModels);
//                            menuAdapter=new com.brainlines.weightloggernewversion.adapter.MenuAdapter(ModuleSelectionActivity.this,uniqueItemList,ModuleSelectionActivity.this);
//                            rvUserDataList.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL, false));
//                            rvUserDataList.setAdapter(menuAdapter);
//                        }
//                        catch (Exception e)
//                        {
//                            e.printStackTrace();
//                        }
//                        }
//                    }
//                }
//                catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//
//            }
//        });
//    }
//
//    @Override
//    public void clickonmenu(MenuModel model) {
//
//        GlobalClass.getDbNameBasedOnModuleSelected(model,this);
//        switch (model.getModule_ID()){
//            case "1":
//                //WAC Module Flow
//                Intent moduleIdOne = new Intent(this, Falco_Home_Main_Activity.class);
//                moduleIdOne.putExtra("MenuModel",model);
//                moduleIdOne.putExtra("UserDataModel",modelToStoreInDB);
//                startActivity(moduleIdOne);
//                break;
//            case "2":
//                //SWAS Module Flow
//                Intent moduleIdTwo = new Intent(this, Falco_Home_Main_Activity.class);
//                moduleIdTwo.putExtra("MenuModel",model);
//                moduleIdTwo.putExtra("UserDataModel",modelToStoreInDB);
//                startActivity(moduleIdTwo);
//                break;
//            case "3":
//                //JK Planning Module Flow
//                break;
//        }
////        if(model!=null)
////        {
////            Intent i = new Intent(ModuleSelectionActivity.this, Falco_Home_Main_Activity.class);
////            startActivity(i);
////        }
//    }
}
