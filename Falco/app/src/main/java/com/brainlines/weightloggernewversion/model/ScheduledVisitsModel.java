package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ScheduledVisitsModel implements Parcelable {

    int ServiceEngineerDetailsID;
    String OEMID;
    int Service_Call_ID;
    String CustomerID;
    String ServiceEngineerID;
    String IsAssigned;
    String IsAccepted;
    String AssignedDate;
    String AssignedTime;
    String Note;
    String CreatedBy;
    String CreatedDate;
    String UpdatedBy;
    String IsActive;
    String UpdatedDate;
    String LOCATIONNAME;
    String CommonName;

    public ScheduledVisitsModel(int serviceEngineerDetailsID, String OEMID, int service_Call_ID, String customerID, String serviceEngineerID, String isAssigned, String isAccepted, String assignedDate, String assignedTime, String note, String createdBy, String createdDate, String updatedBy, String isActive, String updatedDate, String LOCATIONNAME, String commonName) {
        ServiceEngineerDetailsID = serviceEngineerDetailsID;
        this.OEMID = OEMID;
        Service_Call_ID = service_Call_ID;
        CustomerID = customerID;
        ServiceEngineerID = serviceEngineerID;
        IsAssigned = isAssigned;
        IsAccepted = isAccepted;
        AssignedDate = assignedDate;
        AssignedTime = assignedTime;
        Note = note;
        CreatedBy = createdBy;
        CreatedDate = createdDate;
        UpdatedBy = updatedBy;
        IsActive = isActive;
        UpdatedDate = updatedDate;
        this.LOCATIONNAME = LOCATIONNAME;
        CommonName = commonName;
    }

    protected ScheduledVisitsModel(Parcel in) {
        ServiceEngineerDetailsID = in.readInt();
        OEMID = in.readString();
        Service_Call_ID = in.readInt();
        CustomerID = in.readString();
        ServiceEngineerID = in.readString();
        IsAssigned = in.readString();
        IsAccepted = in.readString();
        AssignedDate = in.readString();
        AssignedTime = in.readString();
        Note = in.readString();
        CreatedBy = in.readString();
        CreatedDate = in.readString();
        UpdatedBy = in.readString();
        IsActive = in.readString();
        UpdatedDate = in.readString();
        LOCATIONNAME = in.readString();
        CommonName = in.readString();
    }

    public static final Creator<ScheduledVisitsModel> CREATOR = new Creator<ScheduledVisitsModel>() {
        @Override
        public ScheduledVisitsModel createFromParcel(Parcel in) {
            return new ScheduledVisitsModel(in);
        }

        @Override
        public ScheduledVisitsModel[] newArray(int size) {
            return new ScheduledVisitsModel[size];
        }
    };

    public int getServiceEngineerDetailsID() {
        return ServiceEngineerDetailsID;
    }

    public String getOEMID() {
        return OEMID;
    }

    public int getService_Call_ID() {
        return Service_Call_ID;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public String getServiceEngineerID() {
        return ServiceEngineerID;
    }

    public String getIsAssigned() {
        return IsAssigned;
    }

    public String getIsAccepted() {
        return IsAccepted;
    }

    public String getAssignedDate() {
        return AssignedDate;
    }

    public String getAssignedTime() {
        return AssignedTime;
    }

    public String getNote() {
        return Note;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public String getUpdatedBy() {
        return UpdatedBy;
    }

    public String getIsActive() {
        return IsActive;
    }

    public String getUpdatedDate() {
        return UpdatedDate;
    }

    public String getLOCATIONNAME() {
        return LOCATIONNAME;
    }

    public String getCommonName() {
        return CommonName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ServiceEngineerDetailsID);
        dest.writeString(OEMID);
        dest.writeInt(Service_Call_ID);
        dest.writeString(CustomerID);
        dest.writeString(ServiceEngineerID);
        dest.writeString(IsAssigned);
        dest.writeString(IsAccepted);
        dest.writeString(AssignedDate);
        dest.writeString(AssignedTime);
        dest.writeString(Note);
        dest.writeString(CreatedBy);
        dest.writeString(CreatedDate);
        dest.writeString(UpdatedBy);
        dest.writeString(IsActive);
        dest.writeString(UpdatedDate);
        dest.writeString(LOCATIONNAME);
        dest.writeString(CommonName);
    }

    @Override
    public String toString() {
        return "SampleModel{" +
                "ServiceEngineerDetailsID=" + ServiceEngineerDetailsID +
                ", OEMID='" + OEMID + '\'' +
                ", Service_Call_ID=" + Service_Call_ID +
                ", CustomerID='" + CustomerID + '\'' +
                ", ServiceEngineerID='" + ServiceEngineerID + '\'' +
                ", IsAssigned='" + IsAssigned + '\'' +
                ", IsAccepted='" + IsAccepted + '\'' +
                ", AssignedDate='" + AssignedDate + '\'' +
                ", AssignedTime='" + AssignedTime + '\'' +
                ", Note='" + Note + '\'' +
                ", CreatedBy='" + CreatedBy + '\'' +
                ", CreatedDate='" + CreatedDate + '\'' +
                ", UpdatedBy='" + UpdatedBy + '\'' +
                ", IsActive='" + IsActive + '\'' +
                ", UpdatedDate='" + UpdatedDate + '\'' +
                ", LOCATIONNAME='" + LOCATIONNAME + '\'' +
                ", CommonName='" + CommonName + '\'' +
                '}';
    }
}
