package com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.AssignedCall.Swas_Assigned_Call_Activity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.CompletedCall.Swas_Details_Of_Completed_Call;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.InProcessCall.Swas_Deatails_of_Inprocess_Call_Activity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter.ServiceIdListAdapter;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasListener.SendSingleServiceBundleToSwasServiceCallActivity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasServiceCallIdModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.AppPreferences;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.brainlines.weightloggernewversion.utils.GlobalClass;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Swas_Service_Call_List_Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, SendSingleServiceBundleToSwasServiceCallActivity {

    RecyclerView rv_service_id_details;
    ServiceIdListAdapter adapter;
    String status_id = "",status_name = "";
    ImageView img_user_info,nav_view_img,img_back;
    String user_email,user_token,user_role,created_by,service_eng_id;
    DrawerLayout drawer;
    TextView profile_user,profile_role;
    DataBaseHelper helper;

    private String dbName;
    private SQLiteDatabase db;

    private ArrayList<SwasServiceCallIdModel> model = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swas_assigned_call_selection_list_navigation);

        dbName = AppPreferences.getDatabseName(this);
//        helper = new DataBaseHelper(this,dbName);
        helper = new DataBaseHelper(this);
        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));
        setData();
        user_email = "yogesh@brainlines.in";
        if (user_email.equals("yogesh@brainlines.in")) {
            created_by = "Yogesh Jadhav";
//            service_eng_id = "1";
        }
        else {
            created_by = "Yogesh Jadhav";
//            service_eng_id = "1";
            //TODO:Uncomment 91 92 lines and comment line 88 and 89

//            created_by = "Deepak Kulkarni";
//            service_eng_id = "2";
        }
        img_user_info=(ImageView)findViewById(R.id.img_userinfo);
        img_user_info.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img=(ImageView)findViewById(R.id.nav_view_img);
        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_planning).setVisible(false);
        img_back=(ImageView)findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Swas_Service_Call_List_Activity.this, Swas_Home_Actvity.class);
                startActivity(i);
                finish();
            }
        });
        rv_service_id_details = (RecyclerView)findViewById(R.id.rv_service_id_details);
        Bundle bundle=getIntent().getExtras();
        if (bundle!= null) {
            status_id=bundle.getString("status_id");
            status_name =bundle.getString("status_text");

        }
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnected();
//        if(isConnected) {
//            Log.d("Network", "Connected");
//            serviceId();
////            if (status_name.equalsIgnoreCase("Completed Call")){
////                ArrayList<SwasServiceCallIdModel> models = new ArrayList<>();
////                Cursor completedCallsCursor = helper.getCompletedCallServices();
////                while (completedCallsCursor.moveToNext()){
////                    SwasServiceCallIdModel swasServiceModel = new SwasServiceCallIdModel();
////                    String ServiceCallID = completedCallsCursor.getString(0);
////                    String CustomerID = completedCallsCursor.getString(1);
////                    String LOCATIONNAME = completedCallsCursor.getString(2);
////                    String ITEMID = completedCallsCursor.getString(3);
////                    String ServiceEngineerName = completedCallsCursor.getString(4);
////                    String StatusText = completedCallsCursor.getString(5);
////                    String TypeOfServiceCallText = completedCallsCursor.getString(6);
////                    String CreatedBy = completedCallsCursor.getString(7);
////                    String CreatedDate = completedCallsCursor.getString(8);
////                    String UpdatedBy = completedCallsCursor.getString(9);
////                    String UpdatedDate = completedCallsCursor.getString(10);
////                    String ServiceCallStatusID = completedCallsCursor.getString(11);
////                    swasServiceModel.setServiceCallID(ServiceCallID);
////                    swasServiceModel.setCustomerID(CustomerID);
////                    swasServiceModel.setLOCATIONNAME(LOCATIONNAME);
////                    swasServiceModel.setITEMID(ITEMID);
////                    swasServiceModel.setServiceEngineerName(ServiceEngineerName);
////                    swasServiceModel.setStatusText(StatusText);
////                    swasServiceModel.setTypeOfServiceCallText(TypeOfServiceCallText);
////                    swasServiceModel.setCreatedBy(CreatedBy);
////                    swasServiceModel.setCreatedDate(CreatedDate);
////                    swasServiceModel.setUpdatedBy(UpdatedBy);
////                    swasServiceModel.setUpdatedDate(UpdatedDate);
////                    swasServiceModel.setServiceCallStatusID(ServiceCallStatusID);
////                    models.add(swasServiceModel);
////                    adapter = new ServiceIdListAdapter(Swas_Service_Call_List_Activity.this, models,this);
////                    rv_service_id_details.setLayoutManager(new LinearLayoutManager(Swas_Service_Call_List_Activity.this, LinearLayoutManager.VERTICAL, false));
////                    rv_service_id_details.setAdapter(adapter);
////                }
////            }
////            else if (status_name.equalsIgnoreCase("In Process Call")){
////                ArrayList<SwasServiceCallIdModel> models = new ArrayList<>();
////                Cursor inProcessCallsCursor = helper.getInProcessServices();
////                while (inProcessCallsCursor.moveToNext()){
////                    SwasServiceCallIdModel swasServiceModel = new SwasServiceCallIdModel();
////                    String ServiceCallID = inProcessCallsCursor.getString(0);
////                    String CustomerID = inProcessCallsCursor.getString(1);
////                    String LOCATIONNAME = inProcessCallsCursor.getString(2);
////                    String ITEMID = inProcessCallsCursor.getString(3);
////                    String ServiceEngineerName = inProcessCallsCursor.getString(4);
////                    String StatusText = inProcessCallsCursor.getString(5);
////                    String TypeOfServiceCallText = inProcessCallsCursor.getString(6);
////                    String CreatedBy = inProcessCallsCursor.getString(7);
////                    String CreatedDate = inProcessCallsCursor.getString(8);
////                    String UpdatedBy = inProcessCallsCursor.getString(9);
////                    String UpdatedDate = inProcessCallsCursor.getString(10);
////                    String ServiceCallStatusID = inProcessCallsCursor.getString(11);
////                    swasServiceModel.setServiceCallID(ServiceCallID);
////                    swasServiceModel.setCustomerID(CustomerID);
////                    swasServiceModel.setLOCATIONNAME(LOCATIONNAME);
////                    swasServiceModel.setITEMID(ITEMID);
////                    swasServiceModel.setServiceEngineerName(ServiceEngineerName);
////                    swasServiceModel.setStatusText(StatusText);
////                    swasServiceModel.setTypeOfServiceCallText(TypeOfServiceCallText);
////                    swasServiceModel.setCreatedBy(CreatedBy);
////                    swasServiceModel.setCreatedDate(CreatedDate);
////                    swasServiceModel.setUpdatedBy(UpdatedBy);
////                    swasServiceModel.setUpdatedDate(UpdatedDate);
////                    swasServiceModel.setServiceCallStatusID(ServiceCallStatusID);
////                    models.add(swasServiceModel);
////                    adapter = new ServiceIdListAdapter(Swas_Service_Call_List_Activity.this, models,this);
////                    rv_service_id_details.setLayoutManager(new LinearLayoutManager(Swas_Service_Call_List_Activity.this, LinearLayoutManager.VERTICAL, false));
////                    rv_service_id_details.setAdapter(adapter);
////                }
////            }
////            else if (status_name.equalsIgnoreCase("Assigned Call")){
////                ArrayList<SwasServiceCallIdModel> models = new ArrayList<>();
////                Cursor inProcessCallsCursor = helper.getAssignedCallsServices();
////                while (inProcessCallsCursor.moveToNext()){
////                    SwasServiceCallIdModel swasServiceModel = new SwasServiceCallIdModel();
////                    String ServiceCallID = inProcessCallsCursor.getString(0);
////                    String CustomerID = inProcessCallsCursor.getString(1);
////                    String LOCATIONNAME = inProcessCallsCursor.getString(2);
////                    String ITEMID = inProcessCallsCursor.getString(3);
////                    String ServiceEngineerName = inProcessCallsCursor.getString(4);
////                    String StatusText = inProcessCallsCursor.getString(5);
////                    String TypeOfServiceCallText = inProcessCallsCursor.getString(6);
////                    String CreatedBy = inProcessCallsCursor.getString(7);
////                    String CreatedDate = inProcessCallsCursor.getString(8);
////                    String UpdatedBy = inProcessCallsCursor.getString(9);
////                    String UpdatedDate = inProcessCallsCursor.getString(10);
////                    String ServiceCallStatusID = inProcessCallsCursor.getString(11);
////                    swasServiceModel.setServiceCallID(ServiceCallID);
////                    swasServiceModel.setCustomerID(CustomerID);
////                    swasServiceModel.setLOCATIONNAME(LOCATIONNAME);
////                    swasServiceModel.setITEMID(ITEMID);
////                    swasServiceModel.setServiceEngineerName(ServiceEngineerName);
////                    swasServiceModel.setStatusText(StatusText);
////                    swasServiceModel.setTypeOfServiceCallText(TypeOfServiceCallText);
////                    swasServiceModel.setCreatedBy(CreatedBy);
////                    swasServiceModel.setCreatedDate(CreatedDate);
////                    swasServiceModel.setUpdatedBy(UpdatedBy);
////                    swasServiceModel.setUpdatedDate(UpdatedDate);
////                    swasServiceModel.setServiceCallStatusID(ServiceCallStatusID);
////                    models.add(swasServiceModel);
////                    adapter = new ServiceIdListAdapter(Swas_Service_Call_List_Activity.this, models,this);
////                    rv_service_id_details.setLayoutManager(new LinearLayoutManager(Swas_Service_Call_List_Activity.this, LinearLayoutManager.VERTICAL, false));
////                    rv_service_id_details.setAdapter(adapter);
////                }
////            }
//        }
//        else{
//            checkNetworkConnection();
//            Log.d("Network","Not Connected");
//            if (status_name.equalsIgnoreCase("Completed Call")){
////                ArrayList<SwasServiceCallIdModel> models = new ArrayList<>();
//                model.clear();
//                Cursor completedCallsCursor = helper.getCompletedCallServices();
//                while (completedCallsCursor.moveToNext()){
//                    SwasServiceCallIdModel swasServiceModel = new SwasServiceCallIdModel();
//                    String ServiceCallID = completedCallsCursor.getString(1);
//                    String CustomerID = completedCallsCursor.getString(2);
//                    String LOCATIONNAME = completedCallsCursor.getString(3);
//                    String ITEMID = completedCallsCursor.getString(4);
//                    String ServiceEngineerName = completedCallsCursor.getString(5);
//                    String StatusText = completedCallsCursor.getString(6);
//                    String TypeOfServiceCallText = completedCallsCursor.getString(7);
//                    String CreatedBy = completedCallsCursor.getString(8);
//                    String CreatedDate = completedCallsCursor.getString(9);
//                    String UpdatedBy = completedCallsCursor.getString(10);
//                    String UpdatedDate = completedCallsCursor.getString(11);
//                    String ServiceCallStatusID = completedCallsCursor.getString(12);
//                    swasServiceModel.setServiceCallID(ServiceCallID);
//                    swasServiceModel.setCustomerID(CustomerID);
//                    swasServiceModel.setLOCATIONNAME(LOCATIONNAME);
//                    swasServiceModel.setITEMID(ITEMID);
//                    swasServiceModel.setServiceEngineerName(ServiceEngineerName);
//                    swasServiceModel.setStatusText(StatusText);
//                    swasServiceModel.setTypeOfServiceCallText(TypeOfServiceCallText);
//                    swasServiceModel.setCreatedBy(CreatedBy);
//                    swasServiceModel.setCreatedDate(CreatedDate);
//                    swasServiceModel.setUpdatedBy(UpdatedBy);
//                    swasServiceModel.setUpdatedDate(UpdatedDate);
//                    swasServiceModel.setServiceCallStatusID(ServiceCallStatusID);
//                    model.add(swasServiceModel);
//                    adapter = new ServiceIdListAdapter(Swas_Service_Call_List_Activity.this, model,this);
//                    rv_service_id_details.setLayoutManager(new LinearLayoutManager(Swas_Service_Call_List_Activity.this, LinearLayoutManager.VERTICAL, false));
//                    rv_service_id_details.setAdapter(adapter);
//                }
//            }
//            else if (status_name.equalsIgnoreCase("In Process Call")){
////                ArrayList<SwasServiceCallIdModel> models = new ArrayList<>();
//                model.clear();
//                Cursor inProcessCallsCursor = helper.getInProcessServices();
//                while (inProcessCallsCursor.moveToNext()){
//                    SwasServiceCallIdModel swasServiceModel = new SwasServiceCallIdModel();
//                    String ServiceCallID = inProcessCallsCursor.getString(1);
//                    String CustomerID = inProcessCallsCursor.getString(2);
//                    String LOCATIONNAME = inProcessCallsCursor.getString(3);
//                    String ITEMID = inProcessCallsCursor.getString(4);
//                    String ServiceEngineerName = inProcessCallsCursor.getString(5);
//                    String StatusText = inProcessCallsCursor.getString(6);
//                    String TypeOfServiceCallText = inProcessCallsCursor.getString(7);
//                    String CreatedBy = inProcessCallsCursor.getString(8);
//                    String CreatedDate = inProcessCallsCursor.getString(9);
//                    String UpdatedBy = inProcessCallsCursor.getString(10);
//                    String UpdatedDate = inProcessCallsCursor.getString(11);
//                    String ServiceCallStatusID = inProcessCallsCursor.getString(12);
//                    swasServiceModel.setServiceCallID(ServiceCallID);
//                    swasServiceModel.setCustomerID(CustomerID);
//                    swasServiceModel.setLOCATIONNAME(LOCATIONNAME);
//                    swasServiceModel.setITEMID(ITEMID);
//                    swasServiceModel.setServiceEngineerName(ServiceEngineerName);
//                    swasServiceModel.setStatusText(StatusText);
//                    swasServiceModel.setTypeOfServiceCallText(TypeOfServiceCallText);
//                    swasServiceModel.setCreatedBy(CreatedBy);
//                    swasServiceModel.setCreatedDate(CreatedDate);
//                    swasServiceModel.setUpdatedBy(UpdatedBy);
//                    swasServiceModel.setUpdatedDate(UpdatedDate);
//                    swasServiceModel.setServiceCallStatusID(ServiceCallStatusID);
//                    model.add(swasServiceModel);
//                    adapter = new ServiceIdListAdapter(Swas_Service_Call_List_Activity.this, model,this);
//                    rv_service_id_details.setLayoutManager(new LinearLayoutManager(Swas_Service_Call_List_Activity.this, LinearLayoutManager.VERTICAL, false));
//                    rv_service_id_details.setAdapter(adapter);
//                }
//            }
//            else if (status_name.equalsIgnoreCase("Assigned Call")){
////                ArrayList<SwasServiceCallIdModel> models = new ArrayList<>();
//                model.clear();
//                Cursor inProcessCallsCursor = helper.getAssignedCallsServices();
//                while (inProcessCallsCursor.moveToNext()){
//                    SwasServiceCallIdModel swasServiceModel = new SwasServiceCallIdModel();
//                    String ServiceCallID = inProcessCallsCursor.getString(1);
//                    String CustomerID = inProcessCallsCursor.getString(2);
//                    String LOCATIONNAME = inProcessCallsCursor.getString(3);
//                    String ITEMID = inProcessCallsCursor.getString(4);
//                    String ServiceEngineerName = inProcessCallsCursor.getString(5);
//                    String StatusText = inProcessCallsCursor.getString(6);
//                    String TypeOfServiceCallText = inProcessCallsCursor.getString(7);
//                    String CreatedBy = inProcessCallsCursor.getString(8);
//                    String CreatedDate = inProcessCallsCursor.getString(9);
//                    String UpdatedBy = inProcessCallsCursor.getString(10);
//                    String UpdatedDate = inProcessCallsCursor.getString(11);
//                    String ServiceCallStatusID = inProcessCallsCursor.getString(12);
//                    swasServiceModel.setServiceCallID(ServiceCallID);
//                    swasServiceModel.setCustomerID(CustomerID);
//                    swasServiceModel.setLOCATIONNAME(LOCATIONNAME);
//                    swasServiceModel.setITEMID(ITEMID);
//                    swasServiceModel.setServiceEngineerName(ServiceEngineerName);
//                    swasServiceModel.setStatusText(StatusText);
//                    swasServiceModel.setTypeOfServiceCallText(TypeOfServiceCallText);
//                    swasServiceModel.setCreatedBy(CreatedBy);
//                    swasServiceModel.setCreatedDate(CreatedDate);
//                    swasServiceModel.setUpdatedBy(UpdatedBy);
//                    swasServiceModel.setUpdatedDate(UpdatedDate);
//                    swasServiceModel.setServiceCallStatusID(ServiceCallStatusID);
//                    model.add(swasServiceModel);
//                    adapter = new ServiceIdListAdapter(Swas_Service_Call_List_Activity.this, model,this);
//                    rv_service_id_details.setLayoutManager(new LinearLayoutManager(Swas_Service_Call_List_Activity.this, LinearLayoutManager.VERTICAL, false));
//                    rv_service_id_details.setAdapter(adapter);
//                }
//            }
//        }
        if (status_name.equalsIgnoreCase("Completed Call")){
//                ArrayList<SwasServiceCallIdModel> models = new ArrayList<>();
            model.clear();
            Cursor completedCallsCursor = helper.getCompletedCallServices();
            while (completedCallsCursor.moveToNext()){
                SwasServiceCallIdModel swasServiceModel = new SwasServiceCallIdModel();
                String ServiceCallID = completedCallsCursor.getString(1);
                String CustomerID = completedCallsCursor.getString(2);
                String LOCATIONNAME = completedCallsCursor.getString(3);
                String ITEMID = completedCallsCursor.getString(4);
                String ServiceEngineerName = completedCallsCursor.getString(5);
                String StatusText = completedCallsCursor.getString(6);
                String TypeOfServiceCallText = completedCallsCursor.getString(7);
                String CreatedBy = completedCallsCursor.getString(8);
                String CreatedDate = completedCallsCursor.getString(9);
                String UpdatedBy = completedCallsCursor.getString(10);
                String UpdatedDate = completedCallsCursor.getString(11);
                String ServiceCallStatusID = completedCallsCursor.getString(12);
                swasServiceModel.setServiceCallID(ServiceCallID);
                swasServiceModel.setCustomerID(CustomerID);
                swasServiceModel.setLOCATIONNAME(LOCATIONNAME);
                swasServiceModel.setITEMID(ITEMID);
                swasServiceModel.setServiceEngineerName(ServiceEngineerName);
                swasServiceModel.setStatusText(StatusText);
                swasServiceModel.setTypeOfServiceCallText(TypeOfServiceCallText);
                swasServiceModel.setCreatedBy(CreatedBy);
                swasServiceModel.setCreatedDate(CreatedDate);
                swasServiceModel.setUpdatedBy(UpdatedBy);
                swasServiceModel.setUpdatedDate(UpdatedDate);
                swasServiceModel.setServiceCallStatusID(ServiceCallStatusID);
                model.add(swasServiceModel);
                adapter = new ServiceIdListAdapter(Swas_Service_Call_List_Activity.this, model,this);
                rv_service_id_details.setLayoutManager(new LinearLayoutManager(Swas_Service_Call_List_Activity.this, LinearLayoutManager.VERTICAL, false));
                rv_service_id_details.setAdapter(adapter);
            }
        }
        else if (status_name.equalsIgnoreCase("In Process Call")){
//                ArrayList<SwasServiceCallIdModel> models = new ArrayList<>();
            model.clear();
            Cursor inProcessCallsCursor = helper.getInProcessServices();
            while (inProcessCallsCursor.moveToNext()){
                SwasServiceCallIdModel swasServiceModel = new SwasServiceCallIdModel();
                String ServiceCallID = inProcessCallsCursor.getString(1);
                String CustomerID = inProcessCallsCursor.getString(2);
                String LOCATIONNAME = inProcessCallsCursor.getString(3);
                String ITEMID = inProcessCallsCursor.getString(4);
                String ServiceEngineerName = inProcessCallsCursor.getString(5);
                String StatusText = inProcessCallsCursor.getString(6);
                String TypeOfServiceCallText = inProcessCallsCursor.getString(7);
                String CreatedBy = inProcessCallsCursor.getString(8);
                String CreatedDate = inProcessCallsCursor.getString(9);
                String UpdatedBy = inProcessCallsCursor.getString(10);
                String UpdatedDate = inProcessCallsCursor.getString(11);
                String ServiceCallStatusID = inProcessCallsCursor.getString(12);
                swasServiceModel.setServiceCallID(ServiceCallID);
                swasServiceModel.setCustomerID(CustomerID);
                swasServiceModel.setLOCATIONNAME(LOCATIONNAME);
                swasServiceModel.setITEMID(ITEMID);
                swasServiceModel.setServiceEngineerName(ServiceEngineerName);
                swasServiceModel.setStatusText(StatusText);
                swasServiceModel.setTypeOfServiceCallText(TypeOfServiceCallText);
                swasServiceModel.setCreatedBy(CreatedBy);
                swasServiceModel.setCreatedDate(CreatedDate);
                swasServiceModel.setUpdatedBy(UpdatedBy);
                swasServiceModel.setUpdatedDate(UpdatedDate);
                swasServiceModel.setServiceCallStatusID(ServiceCallStatusID);
                model.add(swasServiceModel);
                adapter = new ServiceIdListAdapter(Swas_Service_Call_List_Activity.this, model,this);
                rv_service_id_details.setLayoutManager(new LinearLayoutManager(Swas_Service_Call_List_Activity.this, LinearLayoutManager.VERTICAL, false));
                rv_service_id_details.setAdapter(adapter);
            }
        }
        else if (status_name.equalsIgnoreCase("Assigned Call")){
//                ArrayList<SwasServiceCallIdModel> models = new ArrayList<>();
            model.clear();
            Cursor inProcessCallsCursor = helper.getAssignedCallsServices();
            while (inProcessCallsCursor.moveToNext()){
                SwasServiceCallIdModel swasServiceModel = new SwasServiceCallIdModel();
                String ServiceCallID = inProcessCallsCursor.getString(1);
                String CustomerID = inProcessCallsCursor.getString(2);
                String LOCATIONNAME = inProcessCallsCursor.getString(3);
                String ITEMID = inProcessCallsCursor.getString(4);
                String ServiceEngineerName = inProcessCallsCursor.getString(5);
                String StatusText = inProcessCallsCursor.getString(6);
                String TypeOfServiceCallText = inProcessCallsCursor.getString(7);
                String CreatedBy = inProcessCallsCursor.getString(8);
                String CreatedDate = inProcessCallsCursor.getString(9);
                String UpdatedBy = inProcessCallsCursor.getString(10);
                String UpdatedDate = inProcessCallsCursor.getString(11);
                String ServiceCallStatusID = inProcessCallsCursor.getString(12);
                swasServiceModel.setServiceCallID(ServiceCallID);
                swasServiceModel.setCustomerID(CustomerID);
                swasServiceModel.setLOCATIONNAME(LOCATIONNAME);
                swasServiceModel.setITEMID(ITEMID);
                swasServiceModel.setServiceEngineerName(ServiceEngineerName);
                swasServiceModel.setStatusText(StatusText);
                swasServiceModel.setTypeOfServiceCallText(TypeOfServiceCallText);
                swasServiceModel.setCreatedBy(CreatedBy);
                swasServiceModel.setCreatedDate(CreatedDate);
                swasServiceModel.setUpdatedBy(UpdatedBy);
                swasServiceModel.setUpdatedDate(UpdatedDate);
                swasServiceModel.setServiceCallStatusID(ServiceCallStatusID);
                model.add(swasServiceModel);
                adapter = new ServiceIdListAdapter(Swas_Service_Call_List_Activity.this, model,this);
                rv_service_id_details.setLayoutManager(new LinearLayoutManager(Swas_Service_Call_List_Activity.this, LinearLayoutManager.VERTICAL, false));
                rv_service_id_details.setAdapter(adapter);
            }
        }
    }

    private void serviceId() {
        model.clear();
        API api = Retroconfig.swasretrofit().create(API.class);
        // String service_eng_id = "1";
        Call<ResponseBody> call = api.getservicecalllistbystatus(status_name,service_eng_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
//                            ArrayList<SwasServiceCallIdModel> model = new ArrayList<>();

                            if (array.length() != 0) {
                                for (int i = 0; i < array.length(); i++) {
                                    SwasServiceCallIdModel swasServiceModel = new SwasServiceCallIdModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    swasServiceModel.setServiceCallID(object1.getString("ServiceCallID"));
                                    swasServiceModel.setCustomerID(object1.getString("CustomerID"));
                                    swasServiceModel.setLOCATIONNAME(object1.getString("LOCATIONNAME"));
                                    swasServiceModel.setITEMID(object1.getString("ITEMID"));
                                    swasServiceModel.setServiceEngineerName(object1.getString("ServiceEngineerName"));
                                    swasServiceModel.setStatusText(object1.getString("StatusText"));
                                    swasServiceModel.setTypeOfServiceCallText(object1.getString("TypeOfServiceCallText"));
                                    swasServiceModel.setCreatedBy(object1.getString("CreatedBy"));
                                    swasServiceModel.setCreatedDate(object1.getString("CreatedDate"));
                                    swasServiceModel.setUpdatedBy(object1.getString("UpdatedBy"));
                                    swasServiceModel.setUpdatedDate(object1.getString("UpdatedDate"));
                                    swasServiceModel.setServiceCallStatusID(object1.getString("ServiceCallStatusID"));

                                    model.add(swasServiceModel);

//                                    DBHelper helper=new DBHelper(Swas_Service_Call_List_Activity.this);
//                                    SQLiteDatabase db = helper.getWritableDatabase();
//                                    ContentValues cv=new ContentValues();
//                                    cv.put("ServiceCallID",object1.getString("ServiceCallID"));
//                                    cv.put("CustomerID",object1.getString("CustomerID"));
//                                    cv.put("LOCATIONNAME",object1.getString("LOCATIONNAME"));
//                                    cv.put("ITEMID",object1.getString("ITEMID"));
//                                    cv.put("ServiceEngineerName",object1.getString("ServiceEngineerName"));
//                                    cv.put("StatusText",object1.getString("StatusText"));
//                                    cv.put("TypeOfServiceCallText",object1.getString("TypeOfServiceCallText"));
//                                    cv.put("CreatedBy",object1.getString("CreatedBy"));
//                                    cv.put("CreatedDate",object1.getString("CreatedDate"));
//                                    cv.put("UpdatedBy",object1.getString("UpdatedBy"));
//                                    cv.put("UpdatedDate",object1.getString("UpdatedDate"));
//                                    cv.put("ServiceCallStatusID",object1.getString("ServiceCallStatusID"));
//
//                                    long d=db.insert(Constants.TABLE_LIST_OF_SERVICES_BY_STATUS,null,cv);
//                                    Log.d("user check", String.valueOf(d));
                                }
                                adapter = new ServiceIdListAdapter(Swas_Service_Call_List_Activity.this, model,Swas_Service_Call_List_Activity.this);
                                rv_service_id_details.setLayoutManager(new LinearLayoutManager(Swas_Service_Call_List_Activity.this, LinearLayoutManager.VERTICAL, false));
                                rv_service_id_details.setAdapter(adapter);


                            } else if (array.length() == 0) {
//                                Toast.makeText(Swas_Service_Call_List_Activity.this, "Sorry there is no data", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else
                    {
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void serviceId1() {
        model.clear();
        API api = Retroconfig.swasretrofit().create(API.class);
        // String service_eng_id = "1";
        Call<ResponseBody> call = api.getcompletedservicecalls(30,5,2);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
//                            ArrayList<SwasServiceCallIdModel> model = new ArrayList<>();

                            if (array.length() != 0) {
                                for (int i = 0; i < array.length(); i++) {
                                    SwasServiceCallIdModel swasServiceModel = new SwasServiceCallIdModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    swasServiceModel.setServiceCallID(object1.getString("ServiceCallID"));
                                    swasServiceModel.setCustomerID(object1.getString("CustomerID"));
                                    swasServiceModel.setLOCATIONNAME(object1.getString("LOCATIONNAME"));
                                    swasServiceModel.setITEMID(object1.getString("ITEMID"));
                                    swasServiceModel.setServiceEngineerName(object1.getString("ServiceEngineerName"));
                                    swasServiceModel.setStatusText(object1.getString("StatusText"));
                                    swasServiceModel.setTypeOfServiceCallText(object1.getString("TypeOfServiceCallText"));
                                    swasServiceModel.setCreatedBy(object1.getString("CreatedBy"));
                                    swasServiceModel.setCreatedDate(object1.getString("CreatedDate"));
                                    swasServiceModel.setUpdatedBy(object1.getString("UpdatedBy"));
                                    swasServiceModel.setUpdatedDate(object1.getString("UpdatedDate"));
                                    swasServiceModel.setServiceCallStatusID(object1.getString("ServiceCallStatusID"));

                                    model.add(swasServiceModel);

//                                    DBHelper helper=new DBHelper(Swas_Service_Call_List_Activity.this);
//                                    SQLiteDatabase db = helper.getWritableDatabase();
//                                    ContentValues cv=new ContentValues();
//                                    cv.put("ServiceCallID",object1.getString("ServiceCallID"));
//                                    cv.put("CustomerID",object1.getString("CustomerID"));
//                                    cv.put("LOCATIONNAME",object1.getString("LOCATIONNAME"));
//                                    cv.put("ITEMID",object1.getString("ITEMID"));
//                                    cv.put("ServiceEngineerName",object1.getString("ServiceEngineerName"));
//                                    cv.put("StatusText",object1.getString("StatusText"));
//                                    cv.put("TypeOfServiceCallText",object1.getString("TypeOfServiceCallText"));
//                                    cv.put("CreatedBy",object1.getString("CreatedBy"));
//                                    cv.put("CreatedDate",object1.getString("CreatedDate"));
//                                    cv.put("UpdatedBy",object1.getString("UpdatedBy"));
//                                    cv.put("UpdatedDate",object1.getString("UpdatedDate"));
//                                    cv.put("ServiceCallStatusID",object1.getString("ServiceCallStatusID"));
//
//                                    long d=db.insert(Constants.TABLE_LIST_OF_SERVICES_BY_STATUS,null,cv);
//                                    Log.d("user check", String.valueOf(d));
                                }
                                adapter = new ServiceIdListAdapter(Swas_Service_Call_List_Activity.this, model,Swas_Service_Call_List_Activity.this);
                                rv_service_id_details.setLayoutManager(new LinearLayoutManager(Swas_Service_Call_List_Activity.this, LinearLayoutManager.VERTICAL, false));
                                rv_service_id_details.setAdapter(adapter);


                            } else if (array.length() == 0) {
//                                Toast.makeText(Swas_Service_Call_List_Activity.this, "Sorry there is no data", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else
                    {
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(Swas_Service_Call_List_Activity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);
        setData();
        // Set content width and height
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(Swas_Service_Call_List_Activity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }

    public void setData() {
        //GlobalClass.db_global_name = AppPreferences.getDatabseName(this);
        DataBaseHelper dbHelper = new DataBaseHelper(Swas_Service_Call_List_Activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();

                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    service_eng_id = cursor.getString(cursor.getColumnIndex(Constants.USER_LOGIN_ID));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout() {
        finish();
        Toast toast= Toast.makeText(Swas_Service_Call_List_Activity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(Swas_Service_Call_List_Activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);
        //Preferences.Logout();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();


        if (id==R.id.nav_swas)
        {
            Intent i=new Intent(Swas_Service_Call_List_Activity.this, Swas_Home_Actvity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    public void checkNetworkConnection(){
        AlertDialog.Builder builder =new AlertDialog.Builder(Swas_Service_Call_List_Activity.this);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(Swas_Service_Call_List_Activity.this,Swas_Home_Actvity.class);
        startActivity(i);
    }

    @Override
    public void sendBundle(Bundle bundle,String callType) {
        if (bundle!=null&&!callType.equals("")){
            if (callType.equals("Assigned Call")){
                Intent i = new Intent(this, Swas_Assigned_Call_Activity.class);
                i.putExtras(bundle);
                startActivity(i);
            }else if (callType.equals("In process call")){
                Intent i = new Intent(this, Swas_Deatails_of_Inprocess_Call_Activity.class);
                i.putExtras(bundle);
                startActivity(i);
            }else if (callType.equals("Completed Call")){
                Intent i = new Intent(this, Swas_Details_Of_Completed_Call.class);
                i.putExtras(bundle);
                startActivity(i);
            }
        }
    }
}

