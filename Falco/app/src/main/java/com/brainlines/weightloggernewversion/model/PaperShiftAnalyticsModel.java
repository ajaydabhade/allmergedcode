package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PaperShiftAnalyticsModel implements Parcelable {
    String PaperShiftAnalyticsID;
    String OEMID;
    String LoggerID;
    String TagID;
    String MinReading;
    String MaxReading;
    String RangeReading;
    String RunStatus;
    String ModifiedDate;
    String  MeanReading;

    public PaperShiftAnalyticsModel(String paperShiftAnalyticsID, String OEMID, String loggerID, String tagID, String minReading, String maxReading, String rangeReading, String runStatus, String modifiedDate, String meanReading) {
        PaperShiftAnalyticsID = paperShiftAnalyticsID;
        this.OEMID = OEMID;
        LoggerID = loggerID;
        TagID = tagID;
        MinReading = minReading;
        MaxReading = maxReading;
        RangeReading = rangeReading;
        RunStatus = runStatus;
        ModifiedDate = modifiedDate;
        MeanReading = meanReading;
    }

    protected PaperShiftAnalyticsModel(Parcel in) {
        PaperShiftAnalyticsID = in.readString();
        OEMID = in.readString();
        LoggerID = in.readString();
        TagID = in.readString();
        MinReading = in.readString();
        MaxReading = in.readString();
        RangeReading = in.readString();
        RunStatus = in.readString();
        ModifiedDate = in.readString();
        MeanReading = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(PaperShiftAnalyticsID);
        dest.writeString(OEMID);
        dest.writeString(LoggerID);
        dest.writeString(TagID);
        dest.writeString(MinReading);
        dest.writeString(MaxReading);
        dest.writeString(RangeReading);
        dest.writeString(RunStatus);
        dest.writeString(ModifiedDate);
        dest.writeString(MeanReading);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PaperShiftAnalyticsModel> CREATOR = new Creator<PaperShiftAnalyticsModel>() {
        @Override
        public PaperShiftAnalyticsModel createFromParcel(Parcel in) {
            return new PaperShiftAnalyticsModel(in);
        }

        @Override
        public PaperShiftAnalyticsModel[] newArray(int size) {
            return new PaperShiftAnalyticsModel[size];
        }
    };

    public String getPaperShiftAnalyticsID() {
        return PaperShiftAnalyticsID;
    }

    public void setPaperShiftAnalyticsID(String paperShiftAnalyticsID) {
        PaperShiftAnalyticsID = paperShiftAnalyticsID;
    }

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getLoggerID() {
        return LoggerID;
    }

    public void setLoggerID(String loggerID) {
        LoggerID = loggerID;
    }

    public String getTagID() {
        return TagID;
    }

    public void setTagID(String tagID) {
        TagID = tagID;
    }

    public String getMinReading() {
        return MinReading;
    }

    public void setMinReading(String minReading) {
        MinReading = minReading;
    }

    public String getMaxReading() {
        return MaxReading;
    }

    public void setMaxReading(String maxReading) {
        MaxReading = maxReading;
    }

    public String getRangeReading() {
        return RangeReading;
    }

    public void setRangeReading(String rangeReading) {
        RangeReading = rangeReading;
    }

    public String getRunStatus() {
        return RunStatus;
    }

    public void setRunStatus(String runStatus) {
        RunStatus = runStatus;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public String getMeanReading() {
        return MeanReading;
    }

    public void setMeanReading(String meanReading) {
        MeanReading = meanReading;
    }
}
