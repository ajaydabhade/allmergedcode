package com.brainlines.weightloggernewversion.FalcoJK.MainAdapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.EnterWIPDataModel;
import com.brainlines.weightloggernewversion.FalcoJK.Listeners.EnterWipListener;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class EnterWipAdapter extends RecyclerView.Adapter<EnterWipAdapter.MyViewHolder> {

    Context context;
    ArrayList<EnterWIPDataModel> enterWIPDataModelArrayList;
    EnterWipListener enterWipListener;

    public EnterWipAdapter(Context context, ArrayList<EnterWIPDataModel> enterWIPDataModelArrayList, EnterWipListener enterWipListener) {
        this.context = context;
        this.enterWIPDataModelArrayList = enterWIPDataModelArrayList;
        this.enterWipListener = enterWipListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_rv_wip_details,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final EnterWIPDataModel model = enterWIPDataModelArrayList.get(position);
        if (model!=null)
        {
            holder.txtOperations.setText(model.getOpr_operation_name());
            holder.txtCurrentRoute.setText(model.getPtkd_Plant());
            holder.txtQty.setText(model.getPtkd_Qty());
            holder.txtVsCode.setText(model.getPtkd_ValueStreamCode());

            holder.edtWIP.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String value = editable.toString();
                    if (value != null)
                    {
                        model.setPtkd_WIP(value);
                        enterWipListener.getWIPData(model);
                    }


                }
            });
        }

    }
    @Override
    public int getItemCount() {
        return enterWIPDataModelArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
     TextView txtOperations,txtCurrentRoute,txtVsCode,txtQty;
     EditText edtWIP;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            txtOperations = (itemView).findViewById(R.id.txtoperations);
            txtCurrentRoute = (itemView).findViewById(R.id.txtrouteplant);
            txtVsCode = (itemView).findViewById(R.id.txtvscode);
            txtQty = (itemView).findViewById(R.id.txtQty);
            edtWIP = (itemView).findViewById(R.id.ed_enter_wip);

        }
    }
}
