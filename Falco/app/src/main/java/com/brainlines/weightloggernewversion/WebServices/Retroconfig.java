package com.brainlines.weightloggernewversion.WebServices;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Retroconfig {

    static String SYNC_DATA_BASEURL = "http://apps.brainlines.net/WeightLoggerApi/api/WeightLogger/";
    static String MASTER_DATA_BASEURL = "http://apps.brainlines.net/WeightLoggerMasterSyncAPIPublish/api/WeightLogger/";
    static String TESTING_DATA_BASEURL = "https://fgmobileapi.decintell.co.in/api/FGMobileAPI/";
    static String BASE_URL = "https://fgmobileapi.decintell.co.in/WAC/api/FGMobileAPI/";
    //Azure falco global UAT url FOR SWAS
    public static String BASEURL_SWAS = "https://www.decintell.co.in/FG_Nichrome_SWAS_API/api/servicecall/";
    static String BASEURL_JKPlanning = "https://fgmobileapi.decintell.co.in/PLN/api/";
    //static String MASTER_DATA_BASEURL1 = "https://fgmobileapi.decintell.co.in/api/FGMobileAPI/";
//    public static String BASEURL_SWAS = "http://fgmobileapi.decintell.co.in/SWAS/api/servicecall/";
    public static final String MENUAPI="http://fgmobileapi.decintell.co.in/SWAS_Stg1/api/providers/";

    public static Retrofit baseMethod(){
        OkHttpClient.Builder b = new OkHttpClient.Builder();
        b.readTimeout(5000, TimeUnit.SECONDS);
        b.writeTimeout(5000, TimeUnit.SECONDS);
        OkHttpClient client = b.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return retrofit;
    }

    public static Retrofit swasretrofit(){
        OkHttpClient.Builder b = new OkHttpClient.Builder();
        b.readTimeout(5000, TimeUnit.SECONDS);
        b.writeTimeout(5000, TimeUnit.SECONDS);
        OkHttpClient client = b.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASEURL_SWAS)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return retrofit;
    }

    //JKPlanning RetroConfig Method
    public static Retrofit retrofit(){
        OkHttpClient.Builder b = new OkHttpClient.Builder();
        b.readTimeout(5000, TimeUnit.SECONDS);
        b.writeTimeout(5000, TimeUnit.SECONDS);
        OkHttpClient client = b.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASEURL_JKPlanning)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return retrofit;
    }

    public static Retrofit MenuRetrofit(){
        OkHttpClient.Builder b = new OkHttpClient.Builder();
        b.readTimeout(5000, TimeUnit.SECONDS);
        b.writeTimeout(5000, TimeUnit.SECONDS);
        OkHttpClient client = b.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MENUAPI)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return retrofit;
    }
}
