package com.brainlines.weightloggernewversion.Production.adapter;

import java.io.IOException;

public interface ProductionLoggerListListener
{
    void getLogger(String loggerName, String loggerIpAddress,String loggerMacId) throws IOException;

}
