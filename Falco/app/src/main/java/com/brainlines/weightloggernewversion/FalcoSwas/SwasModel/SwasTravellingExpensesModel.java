package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

public class SwasTravellingExpensesModel {
    String travelling_expenses_name,travelling_expenses_Id;
    String visit_charge,ServiceCallID,OEMID,CustomerID,VisitDates,CreatedBy;

    public String getTravelling_expenses_name() {
        return travelling_expenses_name;
    }

    public void setTravelling_expenses_name(String travelling_expenses_name) {
        this.travelling_expenses_name = travelling_expenses_name;
    }

    public String getTravelling_expenses_Id() {
        return travelling_expenses_Id;
    }

    public void setTravelling_expenses_Id(String travelling_expenses_Id) {
        this.travelling_expenses_Id = travelling_expenses_Id;
    }

    public String getVisit_charge() {
        return visit_charge;
    }

    public void setVisit_charge(String visit_charge) {
        this.visit_charge = visit_charge;
    }

    public String getServiceCallID() {
        return ServiceCallID;
    }

    public void setServiceCallID(String serviceCallID) {
        ServiceCallID = serviceCallID;
    }

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getVisitDates() {
        return VisitDates;
    }

    public void setVisitDates(String visitDates) {
        VisitDates = visitDates;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }
}
