package com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewPackingTicket.Listener;

import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewPackingTicket.Model.NewPackingTicketModel;

public interface NewPackingTicketListener {

    void isupdateClicked(NewPackingTicketModel model);
    void isreleaseClicked(NewPackingTicketModel model, TextView txtreleasedate, ImageButton btnrelease, ImageButton btnupdate, ImageButton btnview, ImageView imagerelease);
    void isviewClicked(NewPackingTicketModel model);
}
