package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

import android.os.Parcel;
import android.os.Parcelable;

public class TravelExpensesKeyValueModel implements Parcelable {

    String expenses_id,visit_charge,ServiceCallID,OEMID,CustomerID,VisitDates,CreatedBy;

    public TravelExpensesKeyValueModel(String expenses_id, String visit_charge, String serviceCallID, String OEMID, String customerID, String visitDates, String createdBy) {
        this.expenses_id = expenses_id;
        this.visit_charge = visit_charge;
        ServiceCallID = serviceCallID;
        this.OEMID = OEMID;
        CustomerID = customerID;
        VisitDates = visitDates;
        CreatedBy = createdBy;
    }

    protected TravelExpensesKeyValueModel(Parcel in) {
        expenses_id = in.readString();
        visit_charge = in.readString();
        ServiceCallID = in.readString();
        OEMID = in.readString();
        CustomerID = in.readString();
        VisitDates = in.readString();
        CreatedBy = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(expenses_id);
        dest.writeString(visit_charge);
        dest.writeString(ServiceCallID);
        dest.writeString(OEMID);
        dest.writeString(CustomerID);
        dest.writeString(VisitDates);
        dest.writeString(CreatedBy);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TravelExpensesKeyValueModel> CREATOR = new Creator<TravelExpensesKeyValueModel>() {
        @Override
        public TravelExpensesKeyValueModel createFromParcel(Parcel in) {
            return new TravelExpensesKeyValueModel(in);
        }

        @Override
        public TravelExpensesKeyValueModel[] newArray(int size) {
            return new TravelExpensesKeyValueModel[size];
        }
    };

    public String getExpenses_id() {
        return expenses_id;
    }

    public void setExpenses_id(String expenses_id) {
        this.expenses_id = expenses_id;
    }

    public String getVisit_charge() {
        return visit_charge;
    }

    public void setVisit_charge(String visit_charge) {
        this.visit_charge = visit_charge;
    }

    public String getServiceCallID() {
        return ServiceCallID;
    }

    public void setServiceCallID(String serviceCallID) {
        ServiceCallID = serviceCallID;
    }

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getVisitDates() {
        return VisitDates;
    }

    public void setVisitDates(String visitDates) {
        VisitDates = visitDates;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

}
