package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class BagLengthVariationResultsModel implements Parcelable {

    String OEMID;
    String MacID;
    String LoggerID;
    int Bag_lenght_ID;
    String TagID;
    String BagLength;
    String OINumber;
    String ModifiedDate;

    public BagLengthVariationResultsModel(String oemId, String macId, String loggerId, int bagLengthId, String tagId, String length, String oiNum, String modifiedDate) {
        this.OEMID = oemId;
        this.MacID = macId;
        this.LoggerID = loggerId;
        this.Bag_lenght_ID = bagLengthId;
        this.TagID = tagId;
        this.BagLength = length;
        this.OINumber = oiNum;
        this.ModifiedDate = modifiedDate;
    }

    protected BagLengthVariationResultsModel(Parcel in) {
        OEMID = in.readString();
        MacID = in.readString();
        LoggerID = in.readString();
        Bag_lenght_ID = in.readInt();
        TagID = in.readString();
        BagLength = in.readString();
        OINumber = in.readString();
        ModifiedDate = in.readString();
    }

    public static final Creator<BagLengthVariationResultsModel> CREATOR = new Creator<BagLengthVariationResultsModel>() {
        @Override
        public BagLengthVariationResultsModel createFromParcel(Parcel in) {
            return new BagLengthVariationResultsModel(in);
        }

        @Override
        public BagLengthVariationResultsModel[] newArray(int size) {
            return new BagLengthVariationResultsModel[size];
        }
    };

    public String getOemId() {
        return OEMID;
    }

    public void setOemId(String oemId) {
        this.OEMID = oemId;
    }

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getMacID() {
        return MacID;
    }

    public void setMacID(String macID) {
        MacID = macID;
    }

    public String getLoggerID() {
        return LoggerID;
    }

    public void setLoggerID(String loggerID) {
        LoggerID = loggerID;
    }

    public int getBag_lenght_ID() {
        return Bag_lenght_ID;
    }

    public void setBag_lenght_ID(int bag_lenght_ID) {
        Bag_lenght_ID = bag_lenght_ID;
    }

    public String getTagID() {
        return TagID;
    }

    public void setTagID(String tagID) {
        TagID = tagID;
    }

    public String getBagLength() {
        return BagLength;
    }

    public void setBagLength(String bagLength) {
        BagLength = bagLength;
    }

    public String getOINumber() {
        return OINumber;
    }

    public void setOINumber(String OINumber) {
        this.OINumber = OINumber;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public static Creator<BagLengthVariationResultsModel> getCREATOR() {
        return CREATOR;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(OEMID);
        dest.writeString(MacID);
        dest.writeString(LoggerID);
        dest.writeInt(Bag_lenght_ID);
        dest.writeString(TagID);
        dest.writeString(BagLength);
        dest.writeString(OINumber);
        dest.writeString(ModifiedDate);
    }
}
