package com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.CompletedCall;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.FalcoJK.activity.FalcoJKHomeActivity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Feedback_Activity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter.FaultyCharacteristicsAdapter;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter.SwasCheckboxCompletedIdDetailsAdapter;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter.SwasPartOrderedByAdapter;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.FaultyCharactristics_Model;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompletedCheckListModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompletedModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasPartOrderedByModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Swas_Completed_Call_Service_Id_Details_Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    TextView txt_faulty_part_number,txt_complaint,txt_product,txt_type_of_service_call,txt_create_on,txt_modified,txt_call_created_by,txt_note,txt_assign_service_id,txt_assign_customer_name;
    RecyclerView rv_completed_customer_list,rv_comp_check_list;
    SwasCheckboxCompletedIdDetailsAdapter adapter1;
    SwasPartOrderedByAdapter adapter2;
    String status_id,service_call_id,type_of_service_status_id,customer_id;
    TextView txt_service_call_id;
    LinearLayout relativeLayout1;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
    String complaint = "";
    RecyclerView rv_faulty_characteristics;
    FaultyCharacteristicsAdapter faultyCharacteristicsAdapter;
    TextView txt_type_of_fault_text;
    ImageButton btn_feedback;
    String user_email,user_role,user_token;
    ImageView img_user_profile,nav_view_img,img_back;
    DrawerLayout drawer;
    TextView profile_user,profile_role;
    DataBaseHelper helper;
    private ArrayList<SwasCompletedCheckListModel> serviceCallCheckListModels = new ArrayList<>();
    private ArrayList<SwasPartOrderedByModel> partOrderModels = new ArrayList<>();
    private ArrayList<FaultyCharactristics_Model> faultCharTextList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swas_completed_call_id_details_navigation);
        setData();
        helper = new DataBaseHelper(this);
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img=(ImageView)findViewById(R.id.nav_view_img);
        img_user_profile=(ImageView)findViewById(R.id.img_userinfo);
        img_user_profile.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_planning).setVisible(false);
        img_back=(ImageView)findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Swas_Completed_Call_Service_Id_Details_Activity.this, Swas_Details_Of_Completed_Call.class);
                Bundle bundle = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_service_status_id);
                bundle.putString("customer_id",customer_id);
                i.putExtras(bundle);
                startActivity(i);
                finish();
            }
        });

        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });


        initUi();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
        {
            status_id = bundle.getString("status_id");
            service_call_id = bundle.getString("service_call_id");
            type_of_service_status_id = bundle.getString("Typeofservicecall_id");
            customer_id = bundle.getString("customer_id");
        }


        txt_complaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view_detail_complaint(complaint);
            }
        });
        btn_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Swas_Completed_Call_Service_Id_Details_Activity.this, Swas_Feedback_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_service_status_id);
                bundle.putString("customer_id",customer_id);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
//        if(isConnected) {
//            Log.d("Network", "Connected");
//            Details();
//
//        }
//        else{
//            checkNetworkConnection();
//            Log.d("Network","Not Connected");
//            serviceCallCheckListModels.clear();
//            partOrderModels.clear();
//            faultCharTextList.clear();
//            getDataFromListOfServiceCallByStatus();
//            getServiceCallCheckList();
//            getPartOrders();
//            getFaultCharas();
//        }
        serviceCallCheckListModels.clear();
        partOrderModels.clear();
        faultCharTextList.clear();
        getDataFromListOfServiceCallByStatus();
        getServiceCallCheckList();
        getPartOrders();
        getFaultCharas();
    }



    public void initUi() {
        relativeLayout1 = (LinearLayout)findViewById(R.id.relativeLayout1);
        txt_create_on=(TextView)findViewById(R.id.txt_create_on);
        txt_modified=(TextView)findViewById(R.id.txt_modified);
        txt_type_of_service_call=(TextView)findViewById(R.id.txt_type_of_service_call);
        txt_product=(TextView)findViewById(R.id.txt_product);
        txt_complaint=(TextView)findViewById(R.id.txt_complaint);
        //txt_faulty_part_number=(TextView)findViewById(R.id.txt_faulty_part_number);
        txt_call_created_by=(TextView)findViewById(R.id.txt_call_created_by);
        rv_comp_check_list=(RecyclerView)findViewById(R.id.rv_comp_check_list);
        rv_completed_customer_list=(RecyclerView)findViewById(R.id.rv_completed_customer_list);
        txt_note=(TextView)findViewById(R.id.txt_note);
        txt_assign_service_id=(TextView)findViewById(R.id.txt_assign_service_id);
        txt_assign_customer_name=(TextView)findViewById(R.id.txt_assign_customer_name);
        rv_faulty_characteristics = (RecyclerView)findViewById(R.id.rv_faulty_characteristics);
        txt_type_of_fault_text = (TextView)findViewById(R.id.txt_type_of_fault_text);
        btn_feedback = findViewById(R.id.btn_feedback);

    }

    private void Details() {
        API api = Retroconfig.swasretrofit().create(API.class);
        /*dateformat1 = "2020-01-20";*/
        Call<ResponseBody> call = api.getalldetailsofsingleservicecall(service_call_id,status_id);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {

                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");

                            ArrayList<SwasCompletedModel> model = new ArrayList<>();
                            if (array.length() != 0) {

                                for (int i = 0; i < array.length(); i++) {
                                    SwasCompletedModel swasCompModel = new SwasCompletedModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    swasCompModel.setService_Id(object1.getString("ServiceCallID"));
                                    swasCompModel.setLOCATIONNAME(object1.getString("LOCATIONNAME"));
                                    swasCompModel.setCreatedBy(object1.getString("CreatedBy"));
                                    swasCompModel.setCreatedDate(object1.getString("CreatedDate"));
                                    swasCompModel.setUpdatedBy(object1.getString("UpdatedBy"));
                                    swasCompModel.setUpdatedDate(object1.getString("UpdatedDate"));
                                    swasCompModel.setTypeOfServiceCallText(object1.getString("TypeOfServiceCallText"));
                                    swasCompModel.setNAME(object1.getString("NAME"));
                                    swasCompModel.setBriefComplaint(object1.getString("BriefComplaint"));
                                    swasCompModel.setTypeOfFaultText(object1.getString("TypeOfFaultText"));
                                    swasCompModel.setStatusText(object1.getString("StatusText"));
                                    swasCompModel.setFaultyPart(object1.getString("FaultyPart"));
                                    swasCompModel.setCustomerID(object1.getString("CustomerID"));
                                    swasCompModel.setITEMID(object1.getString("ITEMID"));
                                    swasCompModel.setCURRENCY(object1.getString("CURRENCY"));
                                    swasCompModel.setDocumentPath(object1.getString("DocumentPath"));
                                    swasCompModel.setTypeOfServiceCallID(object1.getString("TypeOfServiceCallID"));
                                    swasCompModel.setCurrentlyAssignedToSE(object1.getString("CurrentlyAssignedToSE"));
                                    swasCompModel.setServiceEngineerName(object1.getString("ServiceEngineerName"));
                                    swasCompModel.setClosedDateTime(object1.getString("ClosedDateTime"));
                                    swasCompModel.setClosedBy(object1.getString("ClosedBy"));
                                    swasCompModel.setServiceCallStatusID(object1.getString("ServiceCallStatusID"));
                                    swasCompModel. setComments(object1.getString("Comments"));
                                    swasCompModel.setServiceEngineerID(object1.getString("ServiceEngineerID"));
                                    swasCompModel.setNumberOfVisits(object1.getString("NumberOfVisits"));

                                    txt_create_on.setText(object1.getString("CreatedDate"));
                                    txt_product.setText(object1.getString("NAME"));
                                    txt_modified.setText(object1.getString("UpdatedDate"));
                                    txt_assign_service_id.setText(object1.getString("ServiceCallID"));
                                    txt_assign_customer_name.setText(object1.getString("LOCATIONNAME"));
                                    txt_call_created_by.setText(object1.getString("CreatedBy"));
                                    txt_type_of_service_call.setText(object1.getString("TypeOfServiceCallText"));
                                    complaint = object1.getString("BriefComplaint");
                                    model.add(swasCompModel);

                                }
                                CompletedChecklistDetails();
                                partOrderDetails();
                                FaultyCharacteristicsList();

                            } else if (array.length() == 0) {
                            }
                        }
                    }


                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void CompletedChecklistDetails() {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getchecklistofservicecall(service_call_id,type_of_service_status_id);


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {

                        String res = response.body().string();
                        if (res != null) {
                            //textView.setText(res);
                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            //JSONArray array=new JSONArray(res);
                            ArrayList<SwasCompletedCheckListModel> model1 = new ArrayList<>();

                            if (array.length() != 0) {

                                for (int i = 0; i < array.length(); i++) {
                                    SwasCompletedCheckListModel checkModel = new SwasCompletedCheckListModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    checkModel.setChecklistForSiteReadynessText(object1.getString("ChecklistForSiteReadynessText"));
                                    checkModel.setChecklistForSiteReadynessID(object1.getString("ChecklistForSiteReadynessID"));
                                    checkModel.setChecked(object1.getString("Checked"));

                                    model1.add(checkModel);

                                }
                                adapter1 = new SwasCheckboxCompletedIdDetailsAdapter(Swas_Completed_Call_Service_Id_Details_Activity.this, model1);
                                rv_comp_check_list.setLayoutManager(new LinearLayoutManager(Swas_Completed_Call_Service_Id_Details_Activity.this, LinearLayoutManager.VERTICAL, false));
                                rv_comp_check_list.setAdapter(adapter1);



                            } else if (array.length() == 0) {
                            }
                        }
                    }


                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
    private void partOrderDetails() {
        API api = Retroconfig.swasretrofit().create(API.class);
        /*dateformat1 = "2020-01-20";*/

        Call<ResponseBody> call = api.getpartordered(service_call_id,customer_id);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                    }
                    String res = response.body().string();
                    if (res != null) {
                        //textView.setText(res);
                        JSONArray array = new JSONArray(res);
                        //JSONArray array=new JSONArray(res);
                        ArrayList<SwasPartOrderedByModel> model2 = new ArrayList<>();

                        //JSONArray array = new JSONArray(res);

                        if (array.length() != 0) {

                            for (int i = 0; i < array.length(); i++) {
                                SwasPartOrderedByModel swapartbyorderModel = new SwasPartOrderedByModel();
                                JSONObject object1 = array.getJSONObject(i);
                                swapartbyorderModel.setServiceCallID(object1.getString("ServiceCallID"));
                                swapartbyorderModel.setOEMID(object1.getString("OEMID"));
                                swapartbyorderModel.setCustomerID(object1.getString("CustomerID"));
                                swapartbyorderModel.setMachineID(object1.getString("MachineID"));
                                swapartbyorderModel.setITEMID(object1.getString("ITEMID"));
                                swapartbyorderModel.setQuantity(object1.getString("Quantity"));
                                swapartbyorderModel.setNAME(object1.getString("NAME"));
                                swapartbyorderModel.setPRICE(object1.getString("PRICE"));
                                swapartbyorderModel.setApplicable(object1.getString("Applicable"));
                                swapartbyorderModel.setConsumables(object1.getString("Consumables"));
                                swapartbyorderModel.setFinalPrice(object1.getString("FinalPrice"));
                                swapartbyorderModel.setExcludeInclude(object1.getString("ExcludeInclude"));
                                swapartbyorderModel.setCreatedBy(object1.getString("CreatedBy"));
                                swapartbyorderModel.setCreatedDate(object1.getString("CreatedDate"));


                                model2.add(swapartbyorderModel);

                            }
                            relativeLayout1.setVisibility(View.VISIBLE);
                            adapter2 = new SwasPartOrderedByAdapter(Swas_Completed_Call_Service_Id_Details_Activity.this, model2);
                            rv_completed_customer_list.setLayoutManager(new LinearLayoutManager(Swas_Completed_Call_Service_Id_Details_Activity.this, LinearLayoutManager.VERTICAL, false));
                            rv_completed_customer_list.setAdapter(adapter2);



                        } else if (array.length() == 0) {
                            relativeLayout1.setVisibility(View.GONE);
                        }
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
    private void FaultyCharacteristicsList() {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getfaultcharacteristicslist(service_call_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                    }
                    String res = response.body().string();
                    if (res != null) {

                        JSONObject object =  new JSONObject(res);
                        JSONArray array = object.getJSONArray("data");
                        ArrayList<FaultyCharactristics_Model> faultyCharactristics_modelArrayList = new ArrayList<>();
                        if (array.length() != 0) {

                            for (int i = 0; i < array.length(); i++) {
                                FaultyCharactristics_Model model = new FaultyCharactristics_Model();
                                JSONObject obj = array.getJSONObject(i);
                                model.setCustomerID(obj.getString("CustomerID"));
                                model.setServiceCallID(obj.getString("ServiceCallID"));
                                model.setFaultCharacteristicsText(obj.getString("FaultCharacteristicsText"));

                                faultyCharactristics_modelArrayList.add(model);
                            }
                            //relativeLayout1.setVisibility(View.VISIBLE);
                            faultyCharacteristicsAdapter = new FaultyCharacteristicsAdapter(Swas_Completed_Call_Service_Id_Details_Activity.this,faultyCharactristics_modelArrayList);
                            rv_faulty_characteristics.setLayoutManager(new LinearLayoutManager(Swas_Completed_Call_Service_Id_Details_Activity.this,LinearLayoutManager.VERTICAL, false));
                            rv_faulty_characteristics.setAdapter(faultyCharacteristicsAdapter);


                        } else if (array.length() == 0) {
                            //relativeLayout1.setVisibility(View.VISIBLE);
                        }
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
    private void view_detail_complaint(String complaint) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.alert_view_detailed_complaint, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        TextView textView = (TextView)dialogView.findViewById(R.id.txt_details_complaint);
        textView.setText(complaint);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(Swas_Completed_Call_Service_Id_Details_Activity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);
        setData();
        // Set content width and height
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(Swas_Completed_Call_Service_Id_Details_Activity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }
    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(Swas_Completed_Call_Service_Id_Details_Activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();

                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));


                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }
    public void logout() {
        finish();
        Toast toast= Toast.makeText(Swas_Completed_Call_Service_Id_Details_Activity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(Swas_Completed_Call_Service_Id_Details_Activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);

        //Preferences.Logout();

    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id==R.id.nav_planning)
        {
            Intent i=new Intent(Swas_Completed_Call_Service_Id_Details_Activity.this, FalcoJKHomeActivity.class);
            startActivity(i);
        }


        drawer.closeDrawer(GravityCompat.START);
        return true;

    }
    public void checkNetworkConnection(){
        AlertDialog.Builder builder =new AlertDialog.Builder(Swas_Completed_Call_Service_Id_Details_Activity.this);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(Swas_Completed_Call_Service_Id_Details_Activity.this,Swas_Details_Of_Completed_Call.class);
        Bundle bundle = new Bundle();
        bundle.putString("service_call_id",service_call_id);
        bundle.putString("status_id",status_id);
        bundle.putString("Typeofservicecall_id",type_of_service_status_id);
        bundle.putString("customer_id",customer_id);
        i.putExtras(bundle);
        startActivity(i);
        finish();
    }

    private void getDataFromListOfServiceCallByStatus() {
        Cursor cursor = helper.getDetailsDataFromServiceCallStatusTable(service_call_id);
        while (cursor.moveToNext()){
            String customer_name = cursor.getString(0);
            String created_By = cursor.getString(1);
            String created_on = cursor.getString(2);
            String last_modified_on = cursor.getString(3);
            String product = cursor.getString(4);
            complaint = cursor.getString(5);
            String type_of_fault_text= cursor.getString(7);
            String typeOfServiceCallText= cursor.getString(6);
//            String statusText= cursor.getString(8);
            txt_assign_service_id.setText(service_call_id);
            txt_assign_customer_name.setText(customer_name);
            txt_call_created_by.setText(created_By);
            txt_create_on.setText(created_on);
            txt_modified.setText(last_modified_on);
            txt_type_of_service_call.setText(typeOfServiceCallText);
            //txt_type_of_service_call.setText(product);
            txt_product.setText(product);
            txt_type_of_fault_text.setText(type_of_fault_text);
//            txt_service_call_status_text.setText(statusText);
//            view_detail_complaint(complaint);

        }
    }
    private void getServiceCallCheckList() {
        serviceCallCheckListModels.clear();
        Cursor cursor = helper.getServiceCallCheckListAgainstServiceCallIDForDetails(service_call_id);
        while (cursor.moveToNext()){
            String checkListForSiteReadynessId = cursor.getString(2);
            String checkListForSiteReadynesstext = cursor.getString(3);
            String chcked = cursor.getString(4);
            SwasCompletedCheckListModel model = new SwasCompletedCheckListModel();
            model.setChecklistForSiteReadynessID(checkListForSiteReadynessId);
            model.setChecklistForSiteReadynessText(checkListForSiteReadynesstext);
            model.setChecked(chcked);
            serviceCallCheckListModels.add(model);
        }
        adapter1 = new SwasCheckboxCompletedIdDetailsAdapter(Swas_Completed_Call_Service_Id_Details_Activity.this, serviceCallCheckListModels);
        rv_comp_check_list.setLayoutManager(new LinearLayoutManager(Swas_Completed_Call_Service_Id_Details_Activity.this, LinearLayoutManager.VERTICAL, false));
        rv_comp_check_list.setAdapter(adapter1);
    }
    private void getPartOrders() {
        partOrderModels.clear();
        Cursor cursor = helper.getPartOrdersAgainstServiceCallIDForDetails(service_call_id);
        while(cursor.moveToNext()){
            String ServiceCallID = cursor.getString(1);
            String OEMID = cursor.getString(2);
            String customerId = cursor.getString(3);
            String itemId = cursor.getString(4);
            String quantity = cursor.getString(5);
            String name = cursor.getString(6);
            String price = cursor.getString(7);
            String applicable = cursor.getString(8);
            String finalPrice = cursor.getString(9);
            String excludeInclude = cursor.getString(10);
            String createdBy = cursor.getString(11);
            String createdDate = cursor.getString(12);
            SwasPartOrderedByModel model= new SwasPartOrderedByModel();
            model.setServiceCallID(ServiceCallID);
            model.setOEMID(OEMID);
            model.setCustomerID(customerId);
            model.setITEMID(itemId);
            model.setQuantity(quantity);
            model.setNAME(name);
            model.setPRICE(price);
            model.setApplicable(applicable);
            model.setFinalPrice(finalPrice);
            model.setExcludeInclude(excludeInclude);
            model.setCreatedBy(createdBy);
            model.setCreatedBy(createdDate);
            partOrderModels.add(model);

        }
        relativeLayout1.setVisibility(View.VISIBLE);
        adapter2 = new SwasPartOrderedByAdapter(Swas_Completed_Call_Service_Id_Details_Activity.this, partOrderModels);
        rv_completed_customer_list.setLayoutManager(new LinearLayoutManager(Swas_Completed_Call_Service_Id_Details_Activity.this, LinearLayoutManager.VERTICAL, false));
        rv_completed_customer_list.setAdapter(adapter2);
    }
    private void getFaultCharas() {
        faultCharTextList.clear();
        Cursor cursor = helper.getFaultCharList(service_call_id);
        while (cursor.moveToNext()){
            String customerId = cursor.getString(1);
            String serviceCallID = cursor.getString(2);
            String faultCharText = cursor.getString(3);
            FaultyCharactristics_Model model = new FaultyCharactristics_Model();
            model.setCustomerID(customerId);;
            model.setServiceCallID(serviceCallID);
            model.setFaultCharacteristicsText(faultCharText);
            faultCharTextList.add(model);
        }
        faultyCharacteristicsAdapter = new FaultyCharacteristicsAdapter(Swas_Completed_Call_Service_Id_Details_Activity.this,faultCharTextList);
        rv_faulty_characteristics.setLayoutManager(new LinearLayoutManager(Swas_Completed_Call_Service_Id_Details_Activity.this,LinearLayoutManager.VERTICAL, false));
        rv_faulty_characteristics.setAdapter(faultyCharacteristicsAdapter);
    }
}

