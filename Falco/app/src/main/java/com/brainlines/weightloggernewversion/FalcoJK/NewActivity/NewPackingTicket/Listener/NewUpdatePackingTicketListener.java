package com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewPackingTicket.Listener;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.PackingTicketDatewiseQuantityModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.StartTicketPackModel;

public interface NewUpdatePackingTicketListener {
    void imgactionSubmitIsClicked(StartTicketPackModel model);

    void imgactintrackingSubmit(PackingTicketDatewiseQuantityModel model);
}
