package com.brainlines.weightloggernewversion.Production.Model;

public class P_TagResultSyncModel {

    String OEMID,MacID,LoggerID,RunStatusID,ProductionTagName,TargetWeight,Head,IsAuthorised,StatusAborted,ModifiedDate,ModifiedBy;

    public P_TagResultSyncModel(String OEMID, String macID, String loggerID, String runStatusID, String productionTagName, String targetWeight, String head, String isAuthorised, String statusAborted, String modifiedDate, String modifiedBy) {
        this.OEMID = OEMID;
        MacID = macID;
        LoggerID = loggerID;
        RunStatusID = runStatusID;
        ProductionTagName = productionTagName;
        TargetWeight = targetWeight;
        Head = head;
        IsAuthorised = isAuthorised;
        StatusAborted = statusAborted;
        ModifiedDate = modifiedDate;
        ModifiedBy = modifiedBy;
    }

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getMacID() {
        return MacID;
    }

    public void setMacID(String macID) {
        MacID = macID;
    }

    public String getLoggerID() {
        return LoggerID;
    }

    public void setLoggerID(String loggerID) {
        LoggerID = loggerID;
    }

    public String getRunStatusID() {
        return RunStatusID;
    }

    public void setRunStatusID(String runStatusID) {
        RunStatusID = runStatusID;
    }

    public String getProductionTagName() {
        return ProductionTagName;
    }

    public void setProductionTagName(String productionTagName) {
        ProductionTagName = productionTagName;
    }

    public String getTargetWeight() {
        return TargetWeight;
    }

    public void setTargetWeight(String targetWeight) {
        TargetWeight = targetWeight;
    }

    public String getHead() {
        return Head;
    }

    public void setHead(String head) {
        Head = head;
    }

    public String getIsAuthorised() {
        return IsAuthorised;
    }

    public void setIsAuthorised(String isAuthorised) {
        IsAuthorised = isAuthorised;
    }

    public String getStatusAborted() {
        return StatusAborted;
    }

    public void setStatusAborted(String statusAborted) {
        StatusAborted = statusAborted;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }
}
