package com.brainlines.weightloggernewversion.Production.Model;

public class PWeightReportSyncModel {
    String MaxWeight,Average,Range,SD,UnfilledQuantity,FilledQuantity,UnfilledPouches,FilledPouches,ExcessGiveawayPrice,ExcessGiveawayGm,IsActive;
    String CreatedDate,ModifiedDate,MinWeight,MeanWeight,BatchID,OEMID,TagID,Reason,Remark;

    public PWeightReportSyncModel(String maxWeight, String average, String range, String SD, String unfilledQuantity, String filledQuantity, String unfilledPouches, String filledPouches, String excessGiveawayPrice, String excessGiveawayGm, String isActive, String createdDate, String modifiedDate, String minWeight, String meanWeight, String batchID, String OEMID, String tagID,String reason,String Remark) {
        this.MaxWeight = maxWeight;
        this.Average = average;
        this.Range = range;
        this.SD = SD;
        this.UnfilledQuantity = unfilledQuantity;
        this.FilledQuantity = filledQuantity;
        this.UnfilledPouches = unfilledPouches;
        this.FilledPouches = filledPouches;
        this.ExcessGiveawayPrice = excessGiveawayPrice;
        this.ExcessGiveawayGm = excessGiveawayGm;
        this.IsActive = isActive;
        this.CreatedDate = createdDate;
        this.ModifiedDate = modifiedDate;
        this.MinWeight = minWeight;
        this.MeanWeight = meanWeight;
        this.BatchID = batchID;
        this.OEMID = OEMID;
        this.TagID = tagID;
        this.Reason = reason;
        this.Remark = Remark;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }

    public String getMaxWeight() {
        return MaxWeight;
    }

    public void setMaxWeight(String maxWeight) {
        MaxWeight = maxWeight;
    }

    public String getAverage() {
        return Average;
    }

    public void setAverage(String average) {
        Average = average;
    }

    public String getRange() {
        return Range;
    }

    public void setRange(String range) {
        Range = range;
    }

    public String getSD() {
        return SD;
    }

    public void setSD(String SD) {
        this.SD = SD;
    }

    public String getUnfilledQuantity() {
        return UnfilledQuantity;
    }

    public void setUnfilledQuantity(String unfilledQuantity) {
        UnfilledQuantity = unfilledQuantity;
    }

    public String getFilledQuantity() {
        return FilledQuantity;
    }

    public void setFilledQuantity(String filledQuantity) {
        FilledQuantity = filledQuantity;
    }

    public String getUnfilledPouches() {
        return UnfilledPouches;
    }

    public void setUnfilledPouches(String unfilledPouches) {
        UnfilledPouches = unfilledPouches;
    }

    public String getFilledPouches() {
        return FilledPouches;
    }

    public void setFilledPouches(String filledPouches) {
        FilledPouches = filledPouches;
    }

    public String getExcessGiveawayPrice() {
        return ExcessGiveawayPrice;
    }

    public void setExcessGiveawayPrice(String excessGiveawayPrice) {
        ExcessGiveawayPrice = excessGiveawayPrice;
    }

    public String getExcessGiveawayGm() {
        return ExcessGiveawayGm;
    }

    public void setExcessGiveawayGm(String excessGiveawayGm) {
        ExcessGiveawayGm = excessGiveawayGm;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public String getMinWeight() {
        return MinWeight;
    }

    public void setMinWeight(String minWeight) {
        MinWeight = minWeight;
    }

    public String getMeanWeight() {
        return MeanWeight;
    }

    public void setMeanWeight(String meanWeight) {
        MeanWeight = meanWeight;
    }

    public String getBatchID() {
        return BatchID;
    }

    public void setBatchID(String batchID) {
        BatchID = batchID;
    }

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getTagID() {
        return TagID;
    }

    public void setTagID(String tagID) {
        TagID = tagID;
    }
}
