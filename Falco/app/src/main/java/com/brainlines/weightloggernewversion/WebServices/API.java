package com.brainlines.weightloggernewversion.WebServices;

import com.google.gson.JsonObject;

import org.json.JSONArray;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface API {

    /*@POST("InsertBagLengthAnalyticsData")
    Call<ResponseBody> syncBagLengthAnalyticsData(@Query("bagLengthAnalyticsStr") String response);*/

    @Headers({"Content-Type: application/json","Accept: application/json"})
    @POST("InsertTXNDataIntoTable")
    Call<ResponseBody> SyncTXNDataForThisTable(@Body JsonObject object,
                                               @Query("oemId") String oemId,
                                               @Query("tableName") String tableName,
                                               @Query("moduleId") String moduleId);

   /* @Headers({"Content-Type: application/json","Accept: application/json"})
    @POST("InsertTXNDataIntoTable")
    Call<ResponseBody> SyncTXNDataForThisTable(@Body JsonObject object,
                                               @Query("oemId") String oemId,
                                               @Query("tableName") String tableName,
                                               @Query("moduleId") String moduleId);*/

    @POST("InsertBagLengthVariationResultsDvcata")
    Call<ResponseBody> syncBagLengthVariationResultsData(@Query("bagLengthVariationResultsStr") String response);

    @POST("InsertPaperShiftAnalyticsData")
    Call<ResponseBody> syncPaperShiftAnalyticsData(@Query("paperShiftAnalyticsStr") String response);

    @POST("InsertPaperShiftResultsData")
    Call<ResponseBody> syncPaperShiftResultsData(@Query("paperShiftResultsStr") String response);

    @POST("InsertWeightTestResultsData")
    Call<ResponseBody> syncWeightTestResultsData(@Query("weightTestResultsStr") String response);

    @POST("InsertWeightResultstAnalyticsData")
    Call<ResponseBody> syncWeightResultstAnalyticsData(@Query("weightResultstAnalyticsStr") String response);

    @POST("InsertObservationsData")
    Call<ResponseBody> syncObservationsData(@Query("observationsStr") String response);

    @POST("InsertCommentsData")
    Call<ResponseBody> syncCommentsData(@Query("commentsStr") String response);

    @POST("InsertCTagsData")
    Call<ResponseBody> syncCTagsData(@Query("cTagResultsStr") String response);

    @POST("InsertProductionTagData")
    Call<ResponseBody> syncProductionTagData(@Query("productionTagStr") String response);

    @POST("InsertPWeightReadingsData")
    Call<ResponseBody> syncInsertPWeightReadingsData(@Query("pWeightReadingsStr") String response);

    @POST("InsertWeightReportTagData")
    Call<ResponseBody> syncInsertWeightReportData(@Query("weightResultStr") String response);

    @POST("InsertPTagResultsTagData")
    Call<ResponseBody> syncInsertPTagResultsTagData(@Query("pTagResultstr") String response);


    //Syncing Master Data to SQLITE DB
    @POST("SyncUOMDetails")
    Call<ResponseBody> SyncUOMDetails();

    @POST("SyncFillerTypeDetails")
    Call<ResponseBody> SyncFillerTypeDetails();

    @POST("SyncHeadDetails")
    Call<ResponseBody> SyncHeadDetails();

    @POST("SyncLoggerDetails")
    Call<ResponseBody> SyncLoggerDetails();

    @POST("SyncMachineTypeDetails")
    Call<ResponseBody> SyncMachineTypeDetails();

    @POST("SyncPouchSymmetryDetails")
    Call<ResponseBody> SyncPouchSymmetryDetails ();

    @POST("SyncProductionreasonDetails")
    Call<ResponseBody> SyncProductionreasonDetails ();

    @POST("SyncProductionToleranceDetails")
    Call<ResponseBody> SyncProductionToleranceDetails ();

    @POST("SyncProductionTypeDetails")
    Call<ResponseBody> SyncProductionTypeDetails ();

    @POST("SyncSealTypeDetails")
    Call<ResponseBody> SyncSealTypeDetails ();

    @POST("SyncTargetAccuracyDetails")
    Call<ResponseBody> SyncTargetAccuracyDetails ();

    @POST("SyncWireCupDetails")
    Call<ResponseBody> SyncWireCupDetails ();

    @POST("SyncProductDetails")
    Call<ResponseBody> SyncProductDetails ();

    @POST("SyncPouchAttributesDetails")
    Call<ResponseBody> SyncPouchAttributesDetails ();

    @POST("SealRejectionReasonMasterDetails")
    Call<ResponseBody> SealRejectionReasonMasterDetails ();

    @POST("GetMasterDataFromSQLTable")
    Call<ResponseBody> syncThisMasterTable(@Query("oemId") int oemId,@Query("tableName") String tableName);

    @GET("GetScriptForCreateTables")
    Call<ResponseBody> createTableForModule(@Query("oemId") int oemId , @Query("moduleId") int moduleId);

    @GET("GetUserEntitlement")
    Call<ResponseBody> syncUserLoginData(@Query("userName") String userName,@Query("password") String password);

    //SWAS API CALLS :

    @POST("GetTestArray")
    Call<ResponseBody> updateData(@Query("Array") String body);

    @GET("GetProdTicketTracking1")
    Call<ResponseBody> getViewData(@Query("TicketIDNo") String tktId,
                                   @Query("ptk_UpdateDate") String ptk_UpdateDate,
                                   @Query("CurrentProdSKU") String currentProdSKU);

    @POST("GetWIPTicketDetails")
    Call<ResponseBody> getEnterWipData(@Query("ticketNo") String ticketNo);

    @POST("save?tt_TicketID=")
    Call<ResponseBody> firstsavePackTicket(@Query("tt_TicketID") String tt_TicketID,
                                           @Query("tt_TicketNo") String tt_TicketNo,
                                           @Query("tt_Plant") String tt_Plant,
                                           @Query("tt_Date") String tt_Date,
                                           @Query("tt_TotalQuantity") String tt_TotalQuantity,
                                           @Query("tt_Qty") String tt_Qty,
                                           @Query("tt_PendingQty") String tt_PendingQty,
                                           @Query("tt_CreatedBy") String tt_CreatedBy,
                                           @Query("tt_ApprovedBy") String tt_ApprovedBy,
                                           @Query("tt_Approve") String tt_Approve,
                                           @Query("tt_TicketNo") String tt_TicketNo1,
                                           @Query("tt_Plant") String tt_Plant1,
                                           @Query("tt_Date") String tt_Date1);



    //Swas Api 06/02/2020

    @POST("getservicecallstatus")
    Call<ResponseBody> getSwasStatus();

    //service call accoring to status
    @POST("getservicecalllistbystatus")
    Call<ResponseBody> getservicecalllistbystatus(@Query("ServiceCallStatus") String ServiceCallStatusID,
                                                  @Query("ServiceEngineerID") String ServiceEngineerID);

    //Get Completed CallList
    @POST("getcompletedservicecalls")
    Call<ResponseBody> getcompletedservicecalls(@Query("ServiceCallID") int ServiceCallID,
                                                @Query("ServiceCallStatusID") int ServiceCallStatusID,
                                                @Query("QEStatusID") int QEStatusID);

    //single service call details
    @POST("getalldetailsofsingleservicecall")
    Call<ResponseBody> getalldetailsofsingleservicecall(@Query("ServiceCallID") String ServiceCallID,
                                                        @Query("ServiceCallStatusID") String ServiceCallStatusID);

    @Headers("Content-Type: application/json")
    @POST("saveacceptservicecallstatus")
    Call<ResponseBody> saveAcceptCall(@Query("ServiceCallID") int ServiceCallID,
                                      @Query("ServiceCallStatusID") int ServiceCallStatusID,
                                      @Query("IsCurrent") int IsCurrent,
                                      @Query("CreatedBy") String CreatedBy,
                                      @Query("QEStatusID") int QEStatusID,
                                      @Query("IsMobile") String IsMobile);

    @POST("getchecklistofservicecall")
    Call<ResponseBody> getchecklistofservicecall(@Query("ServiceCallID") String ServiceCallID,
                                                 @Query("Typeofservicecallid") String Typeofservicecallid);
    @POST("getpartordered")
    Call<ResponseBody> getpartordered(@Query("ServiceCallID") String ServiceCallID,
                                      @Query("CustomerID") String CustomerID);

    @POST("getassigneddatelisttoserviceengineer")
    Call<ResponseBody> getassigneddatelisttoserviceengineer(@Query("ServiceEngineerID") String ServiceEngineerID,
                                                            @Query("AssignedDateStartDate") String AssignedDateStartDate,
                                                            @Query("EndDate") String endDate);

    //travelling_expenses
    @POST("getexpenseheadslist")
    Call<ResponseBody> getexpenseheadslist();

    //travelling_expenses
    @POST("getexpenseheadslist")
    Call<ResponseBody> getexpenseheadslist1(@Query("CustomerID") String CustomerID);

    //competitor_parameter
    @POST("getservicecallcompetitorsparameters")
    Call<ResponseBody> getservicecallcompetitorsparameters(@Query("ServiceCallID") String ServiceCallID);


    //machine_parameter
    @POST("getservicecallmachineparameters")
    Call<ResponseBody> getservicecallmachineparameters(@Query("ServiceCallID") String ServiceCallID);




    @Headers("Content-Type: application/json")
    @POST("saverejectedservicecallstatus")
    Call<ResponseBody> saverejectedservicecallstatus(@Query("ServiceCallID") String ServiceCallID,
                                                     @Query("ServiceCallStatusID") String ServiceCallStatusID,
                                                     @Query("IsCurrent") String IsCurrent,
                                                     @Query("Comments") String Comments,
                                                     @Query("CreatedBy") String CreatedBy,
                                                     @Query("IsMobile") String IsMobile);


    //Completed_Travelling_Expenses
    @POST("gettravellingexpensesdetails")
    Call<ResponseBody> gettravellingexpensesdetails(@Query("ServiceCallID") String ServiceCallID,
                                                    @Query("CustomerID") String CustomerID,
                                                    @Query("OEMID") String OEMID);


    //Completed_Visit_Log
    @POST("getservicecallvisitlog")
    Call<ResponseBody> getservicecallvisitlog(@Query("ServiceCallID") String ServiceCallID);
    //PartOrderBy

    @POST("savecloseservicecallstatus")
    Call<ResponseBody> savecloseservicecallstatus(@Query("ServiceCallID") String ServiceCallID,
                                                  @Query("ServiceCallStatusID") String ServiceCallStatusID,
                                                  @Query("IsCurrent") String IsCurrent,
                                                  @Query("shortTermResolution") String shortTermResolution,
                                                  @Query("LogTermResolution") String LogTermResolution,
                                                  @Query("CreatedBy") String CreatedBy,
                                                  @Query("IsMobile") String IsMobile);

    @POST("savependingshorttermresolutionbymobile")
    Call<ResponseBody> savependingshorttermresolutionbymobile(@Query("ServiceCallID") int ServiceCallID,
                                                              @Query("ShortTermResolutionID") int ShortTermResolutionID,
                                                              @Query("SERemark") String SERemark,
                                                              @Query("ServiceCallStatusID") int ServiceCallStatusID,
                                                              @Query("QEStatusID") int QEStatusID,
                                                              @Query("IsCurrent") int IsCurrent,
                                                              @Query("CreatedBy") String CreatedBy);


    @FormUrlEncoded
    @POST("updateassigneddatesbyserviceengineer")
    Call<ResponseBody> updateassigneddatesbyserviceengineer(@Field("UpDateDates") String body);



    //travelling_expenses
    @POST("getfaultcharacteristicslist")
    Call<ResponseBody> getfaultcharacteristicslist(@Query("ServiceCallID") String ServiceCallID);

    @POST("getmachineparameterslist")
    Call<ResponseBody> getmachineparameterslist();

    @POST("getcompetitorsparameterslist")
    Call<ResponseBody> getcompetitorsparameterslist();

    //getFeedbackDetails
    @POST("getfeedbackdetails")
    Call<ResponseBody> getfeedbackdetails(@Query("ServiceCallID") String ServiceCallID);

    //SaveFeedback
    @POST("savefeedbackdetails")
    Call<ResponseBody> savefeedbackdetails(@Query("ServiceCallID") String ServiceCallID,
                                           @Query("OEMID") String ServiceCallStatusID,
                                           @Query("UserID") String IsCurrent,
                                           @Query("UserFeedback") String CreatedBy,
                                           @Query("CreatedBy") String IsMobile);

    //SaveFeedback
    @POST("savereportsdocumentdetails")
    Call<ResponseBody> savereportsdocumentdetails(@Query("OEMID") String OEMID,
                                                  @Query("CustomerID") String CustomerID,
                                                  @Query("ServiceCallID") String ServiceCallID,
                                                  @Query("DocumentPath") String DocumentPath,
                                                  @Query("DocumentName") String DocumentName,
                                                  @Query("CreatedBy") String CreatedBy);

    @POST("getreportdocumentdetails")
    Call<ResponseBody> getreportdocumentdetails(@Query("ServiceCallID") String ServiceCallID);

    @POST("insertvisitlogdetails")
    Call<ResponseBody> saveVisitLogs(@Body JSONArray visitLogs);

    @POST("Menus")
    Call<ResponseBody> getMenue(@Query("OEMID") String user_oem_id,
                                @Query("RoleName") String user_role);

    @POST("getshorttermresolutionlist")
    Call<ResponseBody> getshorttermresolutionlist();

    //getpermanantcorrectiveaction?ServiceCallID
    @POST("getpermanantcorrectiveaction")
    Call<ResponseBody> getpermanantcorrectiveaction(@Query("ServiceCallID") int serviceCallID);


    //Falco JK Planning

    @POST("getassigneddatelisttoserviceengineer")
    Call<ResponseBody> getassigneddatelisttoserviceengineer(@Query("ServiceEngineerID") String ServiceEngineerID,
                                                            @Query("AssignedDateStartDate") String AssignedDateStartDate);

    //travelling_expenses
    @POST("getexpenseheadslist")
    Call<ResponseBody> getexpenseheadslist(@Query("CustomerID") String CustomerID);


    //Planning Details
    @GET("GetPackingUnpackingTickets")
    Call<ResponseBody> getPackingTickets(@Query("TicketType") String TicketType,
                                         @Query("plantuser") String plantuser);

    @PUT("GetPlanningDetails")
    Call<ResponseBody> getPackingticketDatewiseQuantity(@Query("TicketIDNo") String str_tk_id);

    @GET("GetProductionTickets")
    Call<ResponseBody> getProductionTickets(@Query("plantuser") String plantuser);


    //This api is used for when we click the start button for starting the ticket
    @POST("GetProdToday")
    Call<ResponseBody> getProductionTicketsDetailsonclickofstartbutton(@Query("TicketIDNo") String TicketIDNo,
                                                                       @Query("flag") String flag);

    //This api is used  for the getting multiple sku according to ticket
    @POST("GetSKUBlackidizing")
    Call<ResponseBody> getSkuDetailsAccordingtoticket(@Query("ticketIDNo") String ticketIDNo);

    //This api is used  for the getting multiple sku according to ticket
    @POST("GetSKUBlackidizing")
    Call<ResponseBody> getequipmentdetails(@Query("ticketIDNo") String ticketIDNo);



    @GET("GetEquipment_Master")
    Call<ResponseBody> getEquipmentMaster();

    @GET("GetAssetMaster")
    Call<ResponseBody> getAssetMaster();

    @GET("GetTicketOperation")
    Call<ResponseBody> getOperationListAgainstTicket(@Query("TicketNo") String TicketNo);



}
