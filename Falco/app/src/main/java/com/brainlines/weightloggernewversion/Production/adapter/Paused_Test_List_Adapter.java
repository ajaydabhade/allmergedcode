package com.brainlines.weightloggernewversion.Production.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.Production.Model.PausedTestListModel;
import com.brainlines.weightloggernewversion.Production.ProductionPausedBatchListener;
import com.brainlines.weightloggernewversion.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Paused_Test_List_Adapter extends RecyclerView.Adapter<Paused_Test_List_Adapter.MyViewHolder> {

    Context context;
    ArrayList<PausedTestListModel> p_tag_list = new ArrayList<>();
    ProductionPausedBatchListener listener;

    public Paused_Test_List_Adapter(Context context, ArrayList<PausedTestListModel> p_tag_list, ProductionPausedBatchListener listener) {
        this.context = context;
        this.p_tag_list = p_tag_list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_paused_test_tag,parent,false);
        return new Paused_Test_List_Adapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final PausedTestListModel model = p_tag_list.get(position);
        DecimalFormat decimalFormat = new DecimalFormat("0.000");
        if (model != null)
        {
            String uom = model.getStrUom();
            String kgvalue = model.getStrkgvalue();

            if (uom.equals("Kg"))
            {
                Double dgmpouch = Double.parseDouble(model.getGmPerPouch())/Double.parseDouble(kgvalue);
                Double dtotalqty = Double.parseDouble(model.getTotalQTY())/Double.parseDouble(kgvalue);
                holder.txt_history_gm_pouch.setText(decimalFormat.format(dgmpouch));
                holder.txt_history_total_weight.setText(decimalFormat.format(dtotalqty) + " " + uom);
                holder.txt_gm_head.setText("Kg/Pouch");

            }
            else
            {
                holder.txt_history_gm_pouch.setText(model.getGmPerPouch());
                holder.txt_history_total_weight.setText(model.getTotalQTY() + " " + uom);
                holder.txt_gm_head.setText("Gm/Pouch");

            }

            holder.txt_production_tag.setText(model.getBatchid());
            holder.txt_history_no_of_pouches.setText(model.getTagNumberOfPouches());
            holder.txt_history_product.setText(model.getProductID());


            holder.cv_history_tag_details.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.isTagClicked(model);

                }
            });
            holder.txt_production_tag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.isTagClicked(model);
                }
            });

            holder.btn_view_details.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.isTagClicked(model);

                }
            });
        }





    }

    @Override
    public int getItemCount() {
        return p_tag_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_production_tag,txt_history_total_weight,txt_history_no_of_pouches,txt_history_gm_pouch,txt_history_product,txt_gm_head;
        CardView cv_history_tag_details;
        ImageButton btn_view_details;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_production_tag = (itemView).findViewById(R.id.txt_production_tag);
            txt_history_total_weight = (itemView).findViewById(R.id.txt_history_total_weight);
            txt_history_no_of_pouches = (itemView).findViewById(R.id.txt_history_no_of_pouches);
            txt_history_gm_pouch = (itemView).findViewById(R.id.txt_history_gm_pouch);
            txt_history_product = (itemView).findViewById(R.id.txt_history_product);
            txt_gm_head = (itemView).findViewById(R.id.txt_gm_head);
            cv_history_tag_details = (itemView).findViewById(R.id.cv_history_tag_details);
            btn_view_details = (itemView).findViewById(R.id.img_btn_view_details);
        }
    }
}
