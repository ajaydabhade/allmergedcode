package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class ShortTermListModel implements Parcelable {

    int ShortTermResolutionID;
    String ShortTermResolutionText;

    public ShortTermListModel(int shortTermResolutionID, String shortTermResolutionText) {
        ShortTermResolutionID = shortTermResolutionID;
        ShortTermResolutionText = shortTermResolutionText;
    }

    protected ShortTermListModel(Parcel in) {
        ShortTermResolutionID = in.readInt();
        ShortTermResolutionText = in.readString();
    }

    public static final Creator<ShortTermListModel> CREATOR = new Creator<ShortTermListModel>() {
        @Override
        public ShortTermListModel createFromParcel(Parcel in) {
            return new ShortTermListModel(in);
        }

        @Override
        public ShortTermListModel[] newArray(int size) {
            return new ShortTermListModel[size];
        }
    };

    public int getShortTermResolutionID() {
        return ShortTermResolutionID;
    }

    public void setShortTermResolutionID(int shortTermResolutionID) {
        ShortTermResolutionID = shortTermResolutionID;
    }

    public String getShortTermResolutionText() {
        return ShortTermResolutionText;
    }

    public void setShortTermResolutionText(String shortTermResolutionText) {
        ShortTermResolutionText = shortTermResolutionText;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ShortTermResolutionID);
        dest.writeString(ShortTermResolutionText);
    }

    @Override
    public String toString() {
        return ShortTermResolutionText;
    }
}
