package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

import java.util.Comparator;

public class IdAndModifiedDateModel implements Parcelable{

    String id;
    String modifiedDate;

    public IdAndModifiedDateModel(String id, String modifiedDate) {
        this.id = id;
        this.modifiedDate = modifiedDate;
    }

    protected IdAndModifiedDateModel(Parcel in) {
        id = in.readString();
        modifiedDate = in.readString();
    }

    public static final Creator<IdAndModifiedDateModel> CREATOR = new Creator<IdAndModifiedDateModel>() {
        @Override
        public IdAndModifiedDateModel createFromParcel(Parcel in) {
            return new IdAndModifiedDateModel(in);
        }

        @Override
        public IdAndModifiedDateModel[] newArray(int size) {
            return new IdAndModifiedDateModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public static Comparator<IdAndModifiedDateModel> getStuNameComparator() {
        return StuNameComparator;
    }

    public static void setStuNameComparator(Comparator<IdAndModifiedDateModel> stuNameComparator) {
        StuNameComparator = stuNameComparator;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        IdAndModifiedDateModel that = (IdAndModifiedDateModel) obj;

//        return !(id != null ? !id.equals(that.id) : that.id != null) && !(modifiedDate != null ? !modifiedDate.equals(that.modifiedDate) : that.modifiedDate != null);
        return !(id != null ? !id.equals(that.id) : that.id != null);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
//        result = 31 * result + (modifiedDate != null ? modifiedDate.hashCode() : 0);
        result = 31 * result;
        return result;
    }

    /*Comparator for sorting the list by Student Name*/
    public static Comparator<IdAndModifiedDateModel> StuNameComparator = new Comparator<IdAndModifiedDateModel>() {

        public int compare(IdAndModifiedDateModel s1, IdAndModifiedDateModel s2) {
            int id1 = Integer.parseInt(s1.getId());
            int id2 = Integer.parseInt(s2.getId());

            //ascending order
            return id1 - id2;

            //descending order
            //return StudentName2.compareTo(StudentName1);
        }};

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(modifiedDate);
    }

    @Override
    public String toString() {
        return "IdAndModifiedDateModel{" +
                "id='" + id + '\'' +
                ", modifiedDate='" + modifiedDate + '\'' +
                '}';
    }
}
