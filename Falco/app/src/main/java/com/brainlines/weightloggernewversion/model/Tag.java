package com.brainlines.weightloggernewversion.model;

import java.util.Date;

public class Tag {
    public String oiNum;
    public String fyyear;
    public String targetWt;
    public String fillerType;
    public Date createdDate;
    public String customer;
    public String machineType;
    public String  productType;
    public String product;
    public String operator;
    public String targetWtUnit;
    public String  inspector;
    public String  filmType;
    public String  productfeed;
    public String  pouchWidth;
    public String tagid;
    public String  pouchLength;
    public String  agitatorMotorFreq;
    public String  agitatorMode;
    public String  agitatorod;
    public String agitatorid;
    public String  agitatorPitch;
    public String targetacctype;
    public String targetaccval;
    public String mcSerialNum;
    public String prBulkMain;
    public String targetSpeed;
    public String customerProduct;
    public String  head;
    public String airPressure;
    public String qtySetting;


    public String bgLenSetting;
    public String fillValveGap;
    public String fillVfdFreq;
    public String horizCutSealSetting;
    public String horizBandSealSetting;
    public String vertSealSetting;
    public String augerAgitatorFreq;
    public String augerFillValve;
    public String augurPitch;
    public String pouchSealType;
    public String pouchTempHZF;
    public String pouchTempHZB;
    public String pouchTempVert;
    public String pouchTempVertFrontLeft;
    public String pouchTempVertFrontRight;
    public String pouchTempVertBackLeft;
    public String pouchTempVertBackRight;
    public String pouchGusset;
    public String customerFilm;
    public String pouchFilmThickness;
    public String pouchPrBulk;
    public String pouchEqHorizSerrations;
    public String pouchFillEvent;
    public String negWt;
    public  long id = 0;
    public String overallStatus = "";
    public String sealStatus = "";
    public String dropStatus = "";
    public String targetSpeedStatus = "";
    public String bgStatus;
    public String pprStatus;
    public String wtTestStatus;
    public String remarks;


    /**
     * @return the oiNum
     */
    public String getOiNum() {
        return oiNum;
    }
    /**
     * @param oiNum the oiNum to set
     */
    public void setOiNum(String oiNum) {
        this.oiNum = oiNum;
    }
    /**
     * @return the targetWt
     */
    public String getTargetWt() {
        return targetWt;
    }
    /**
     * @param targetWt the targetWt to set
     */
    public void setTargetWt(String targetWt) {
        this.targetWt = targetWt;
    }
    /**
     * @return the fillerType
     */
    public String getFillerType() {
        return fillerType;
    }
    /**
     * @param fillerType the fillerType to set
     */
    public void setFillerType(String fillerType) {
        this.fillerType = fillerType;
    }
    /**
     * @return the customer
     */
    public String getCustomer() {
        return customer;
    }
    /**
     * @param customer the customer to set
     */
    public void setCustomer(String customer) {
        this.customer = customer;
    }
    /**
     * @return the machineType
     */
    public String getMachineType() {
        return machineType;
    }
    /**
     * @param machineType the machineType to set
     */
    public void setMachineType(String machineType) {
        this.machineType = machineType;
    }
    /**
     * @return the productType
     */
    public String getProductType() {
        return productType;
    }
    /**
     * @param productType the productType to set
     */
    public void setProductType(String productType) {
        this.productType = productType;
    }
    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }
    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }
    /**
     * @return the operator
     */
    public String getOperator() {
        return operator;
    }
    /**
     * @param operator the operator to set
     */
    public void setOperator(String operator) {
        this.operator = operator;
    }
    /**
     * @return the targetWtUnit
     */
    public String getTargetWtUnit() {
        return targetWtUnit;
    }
    /**
     * @param targetWtUnit the targetWtUnit to set
     */
    public void setTargetWtUnit(String targetWtUnit) {
        this.targetWtUnit = targetWtUnit;
    }
    /**
     * @return the inspector
     */
    public String getInspector() {
        return inspector;
    }
    /**
     * @param inspector the inspector to set
     */
    public void setInspector(String inspector) {
        this.inspector = inspector;
    }
    /**
     * @return the filmType
     */
    public String getFilmType() {
        return filmType;
    }
    /**
     * @param filmType the filmType to set
     */
    public void setFilmType(String filmType) {
        this.filmType = filmType;
    }
    /**
     * @return the productfeed
     */
    public String getProductfeed() {
        return productfeed;
    }
    /**
     * @param productfeed the productfeed to set
     */
    public void setProductfeed(String productfeed) {
        this.productfeed = productfeed;
    }
    /**
     * @return the pouchWidth
     */
    public String getPouchWidth() {
        return pouchWidth;
    }
    /**
     * @param pouchWidth the pouchWidth to set
     */
    public void setPouchWidth(String pouchWidth) {
        this.pouchWidth = pouchWidth;
    }
    /**
     * @return the tagid
     */
    public String getTagid() {
        return tagid;
    }
    /**
     * @param tagid the tagid to set
     */
    public void setTagid(String tagid) {
        this.tagid = tagid;
    }
    /**
     * @return the pouchLength
     */
    public String getPouchLength() {
        return pouchLength;
    }
    /**
     * @param pouchLength the pouchLength to set
     */
    public void setPouchLength(String pouchLength) {
        this.pouchLength = pouchLength;
    }
    /**
     * @return the agitatorMotorFreq
     */
    public String getAgitatorMotorFreq() {
        return agitatorMotorFreq;
    }
    /**
     * @param agitatorMotorFreq the agitatorMotorFreq to set
     */
    public void setAgitatorMotorFreq(String agitatorMotorFreq) {
        this.agitatorMotorFreq = agitatorMotorFreq;
    }
    /**
     * @return the agitatorMode
     */
    public String getAgitatorMode() {
        return agitatorMode;
    }
    /**
     * @param agitatorMode the agitatorMode to set
     */
    public void setAgitatorMode(String agitatorMode) {
        this.agitatorMode = agitatorMode;
    }
    /**
     * @return the agitatorod
     */
    public String getAgitatorod() {
        return agitatorod;
    }
    /**
     * @param agitatorod the agitatorod to set
     */
    public void setAgitatorod(String agitatorod) {
        this.agitatorod = agitatorod;
    }
    /**
     * @return the agitatorid
     */
    public String getAgitatorid() {
        return agitatorid;
    }
    /**
     * @param agitatorid the agitatorid to set
     */
    public void setAgitatorid(String agitatorid) {
        this.agitatorid = agitatorid;
    }
    /**
     * @return the agitatorPitch
     */
    public String getAgitatorPitch() {
        return agitatorPitch;
    }
    /**
     * @param agitatorPitch the agitatorPitch to set
     */
    public void setAgitatorPitch(String agitatorPitch) {
        this.agitatorPitch = agitatorPitch;
    }
    /**
     * @return the targetacctype
     */
    public String getTargetacctype() {
        return targetacctype;
    }
    /**
     * @param targetacctype the targetacctype to set
     */
    public void setTargetacctype(String targetacctype) {
        this.targetacctype = targetacctype;
    }
    /**
     * @return the targetaccval
     */
    public String getTargetaccval() {
        return targetaccval;
    }
    /**
     * @param targetaccval the targetaccval to set
     */
    public void setTargetaccval(String targetaccval) {
        this.targetaccval = targetaccval;
    }
    /**
     * @return the mcSerialNum
     */
    public String getMcSerialNum() {
        return mcSerialNum;
    }
    /**
     * @param mcSerialNum the mcSerialNum to set
     */
    public void setMcSerialNum(String mcSerialNum) {
        this.mcSerialNum = mcSerialNum;
    }
    /**
     * @return the prBulkMain
     */
    public String getPrBulkMain() {
        return prBulkMain;
    }
    /**
     * @param prBulkMain the prBulkMain to set
     */
    public void setPrBulkMain(String prBulkMain) {
        this.prBulkMain = prBulkMain;
    }
    /**
     * @return the targetSpeed
     */
    public String getTargetSpeed() {
        return targetSpeed;
    }
    /**
     * @param targetSpeed the targetSpeed to set
     */
    public void setTargetSpeed(String targetSpeed) {
        this.targetSpeed = targetSpeed;
    }
    /**
     * @return the customerProduct
     */
    public String getCustomerProduct() {
        return customerProduct;
    }
    /**
     * @param customerProduct the customerProduct to set
     */
    public void setCustomerProduct(String customerProduct) {
        this.customerProduct = customerProduct;
    }
    /**
     * @return the head
     */
    public String getHead() {
        return head;
    }
    /**
     * @param head the head to set
     */
    public void setHead(String head) {
        this.head = head;
    }
    /**
     * @return the airPressure
     */
    public String getAirPressure() {
        return airPressure;
    }
    /**
     * @param airPressure the airPressure to set
     */
    public void setAirPressure(String airPressure) {
        this.airPressure = airPressure;
    }
    /**
     * @return the qtySetting
     */
    public String getQtySetting() {
        return qtySetting;
    }
    /**
     * @param qtySetting the qtySetting to set
     */
    public void setQtySetting(String qtySetting) {
        this.qtySetting = qtySetting;
    }
    /**
     * @return the bgLenSetting
     */
    public String getBgLenSetting() {
        return bgLenSetting;
    }
    /**
     * @param bgLenSetting the bgLenSetting to set
     */
    public void setBgLenSetting(String bgLenSetting) {
        this.bgLenSetting = bgLenSetting;
    }
    /**
     * @return the fillValveGap
     */
    public String getFillValveGap() {
        return fillValveGap;
    }
    /**
     * @param fillValveGap the fillValveGap to set
     */
    public void setFillValveGap(String fillValveGap) {
        this.fillValveGap = fillValveGap;
    }
    /**
     * @return the fillVfdFreq
     */
    public String getFillVfdFreq() {
        return fillVfdFreq;
    }
    /**
     * @param fillVfdFreq the fillVfdFreq to set
     */
    public void setFillVfdFreq(String fillVfdFreq) {
        this.fillVfdFreq = fillVfdFreq;
    }
    /**
     * @return the horizCutSealSetting
     */
    public String getHorizCutSealSetting() {
        return horizCutSealSetting;
    }
    /**
     * @param horizCutSealSetting the horizCutSealSetting to set
     */
    public void setHorizCutSealSetting(String horizCutSealSetting) {
        this.horizCutSealSetting = horizCutSealSetting;
    }
    /**
     * @return the horizBandSealSetting
     */
    public String getHorizBandSealSetting() {
        return horizBandSealSetting;
    }
    /**
     * @param horizBandSealSetting the horizBandSealSetting to set
     */
    public void setHorizBandSealSetting(String horizBandSealSetting) {
        this.horizBandSealSetting = horizBandSealSetting;
    }
    /**
     * @return the vertSealSetting
     */
    public String getVertSealSetting() {
        return vertSealSetting;
    }
    /**
     * @param vertSealSetting the vertSealSetting to set
     */
    public void setVertSealSetting(String vertSealSetting) {
        this.vertSealSetting = vertSealSetting;
    }
    /**
     * @return the augerAgitatorFreq
     */
    public String getAugerAgitatorFreq() {
        return augerAgitatorFreq;
    }
    /**
     * @param augerAgitatorFreq the augerAgitatorFreq to set
     */
    public void setAugerAgitatorFreq(String augerAgitatorFreq) {
        this.augerAgitatorFreq = augerAgitatorFreq;
    }
    /**
     * @return the augerFillValve
     */
    public String getAugerFillValve() {
        return augerFillValve;
    }
    /**
     * @param augerFillValve the augerFillValve to set
     */
    public void setAugerFillValve(String augerFillValve) {
        this.augerFillValve = augerFillValve;
    }
    /**
     * @return the augurPitch
     */
    public String getAugurPitch() {
        return augurPitch;
    }
    /**
     * @param augurPitch the augurPitch to set
     */
    public void setAugurPitch(String augurPitch) {
        this.augurPitch = augurPitch;
    }
    /**
     * @return the pouchSealType
     */
    public String getPouchSealType() {
        return pouchSealType;
    }
    /**
     * @param pouchSealType the pouchSealType to set
     */
    public void setPouchSealType(String pouchSealType) {
        this.pouchSealType = pouchSealType;
    }
    /**
     * @return the pouchTempHZF
     */
    public String getPouchTempHZF() {
        return pouchTempHZF;
    }
    /**
     * @param pouchTempHZF the pouchTempHZF to set
     */
    public void setPouchTempHZF(String pouchTempHZF) {
        this.pouchTempHZF = pouchTempHZF;
    }
    /**
     * @return the pouchTempHZB
     */
    public String getPouchTempHZB() {
        return pouchTempHZB;
    }
    /**
     * @param pouchTempHZB the pouchTempHZB to set
     */
    public void setPouchTempHZB(String pouchTempHZB) {
        this.pouchTempHZB = pouchTempHZB;
    }
    /**
     * @return the pouchTempVert
     */
    public String getPouchTempVert() {
        return pouchTempVert;
    }
    /**
     * @param pouchTempVert the pouchTempVert to set
     */
    public void setPouchTempVert(String pouchTempVert) {
        this.pouchTempVert = pouchTempVert;
    }
    /**
     * @return the pouchTempVertFrontLeft
     */
    public String getPouchTempVertFrontLeft() {
        return pouchTempVertFrontLeft;
    }
    /**
     * @param pouchTempVertFrontLeft the pouchTempVertFrontLeft to set
     */
    public void setPouchTempVertFrontLeft(String pouchTempVertFrontLeft) {
        this.pouchTempVertFrontLeft = pouchTempVertFrontLeft;
    }
    /**
     * @return the pouchTempVertFrontRight
     */
    public String getPouchTempVertFrontRight() {
        return pouchTempVertFrontRight;
    }
    /**
     * @param pouchTempVertFrontRight the pouchTempVertFrontRight to set
     */
    public void setPouchTempVertFrontRight(String pouchTempVertFrontRight) {
        this.pouchTempVertFrontRight = pouchTempVertFrontRight;
    }
    /**
     * @return the pouchTempVertBackLeft
     */
    public String getPouchTempVertBackLeft() {
        return pouchTempVertBackLeft;
    }
    /**
     * @param pouchTempVertBackLeft the pouchTempVertBackLeft to set
     */
    public void setPouchTempVertBackLeft(String pouchTempVertBackLeft) {
        this.pouchTempVertBackLeft = pouchTempVertBackLeft;
    }
    /**
     * @return the pouchTempVertBackRight
     */
    public String getPouchTempVertBackRight() {
        return pouchTempVertBackRight;
    }
    /**
     * @param pouchTempVertBackRight the pouchTempVertBackRight to set
     */
    public void setPouchTempVertBackRight(String pouchTempVertBackRight) {
        this.pouchTempVertBackRight = pouchTempVertBackRight;
    }
    /**
     * @return the pouchGusset
     */
    public String getPouchGusset() {
        return pouchGusset;
    }
    /**
     * @param pouchGusset the pouchGusset to set
     */
    public void setPouchGusset(String pouchGusset) {
        this.pouchGusset = pouchGusset;
    }
    /**
     * @return the customerFilm
     */
    public String getCustomerFilm() {
        return customerFilm;
    }
    /**
     * @param customerFilm the customerFilm to set
     */
    public void setCustomerFilm(String customerFilm) {
        this.customerFilm = customerFilm;
    }
    /**
     * @return the pouchFilmThickness
     */
    public String getPouchFilmThickness() {
        return pouchFilmThickness;
    }
    /**
     * @param pouchFilmThickness the pouchFilmThickness to set
     */
    public void setPouchFilmThickness(String pouchFilmThickness) {
        this.pouchFilmThickness = pouchFilmThickness;
    }
    /**
     * @return the pouchPrBulk
     */
    public String getPouchPrBulk() {
        return pouchPrBulk;
    }
    /**
     * @param pouchPrBulk the pouchPrBulk to set
     */
    public void setPouchPrBulk(String pouchPrBulk) {
        this.pouchPrBulk = pouchPrBulk;
    }
    /**
     * @return the pouchEqHorizSerrations
     */
    public String getPouchEqHorizSerrations() {
        return pouchEqHorizSerrations;
    }
    /**
     * @param pouchEqHorizSerrations the pouchEqHorizSerrations to set
     */
    public void setPouchEqHorizSerrations(String pouchEqHorizSerrations) {
        this.pouchEqHorizSerrations = pouchEqHorizSerrations;
    }
    /**
     * @return the pouchFillEvent
     */
    public String getPouchFillEvent() {
        return pouchFillEvent;
    }
    /**
     * @param pouchFillEvent the pouchFillEvent to set
     */
    public void setPouchFillEvent(String pouchFillEvent) {
        this.pouchFillEvent = pouchFillEvent;
    }
    /**
     * @return the negWt
     */
    public String getNegWt() {
        return negWt;
    }
    /**
     * @param negWt the negWt to set
     */
    public void setNegWt(String negWt) {
        this.negWt = negWt;
    }
    /**
     * @return the id
     */
    public long getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }
    /**
     * @return the overallStatus
     */
    public String getOverallStatus() {
        return overallStatus;
    }
    /**
     * @param overallStatus the overallStatus to set
     */
    public void setOverallStatus(String overallStatus) {
        this.overallStatus = overallStatus;
    }
    /**
     * @return the sealStatus
     */
    public String getSealStatus() {
        return sealStatus;
    }
    /**
     * @param sealStatus the sealStatus to set
     */
    public void setSealStatus(String sealStatus) {
        this.sealStatus = sealStatus;
    }
    /**
     * @return the dropStatus
     */
    public String getDropStatus() {
        return dropStatus;
    }
    /**
     * @param dropStatus the dropStatus to set
     */
    public void setDropStatus(String dropStatus) {
        this.dropStatus = dropStatus;
    }
    /**
     * @return the targetSpeedStatus
     */
    public String getTargetSpeedStatus() {
        return targetSpeedStatus;
    }
    /**
     * @param targetSpeedStatus the targetSpeedStatus to set
     */
    public void setTargetSpeedStatus(String targetSpeedStatus) {
        this.targetSpeedStatus = targetSpeedStatus;
    }
    /**
     * @return the bgStatus
     */
    public String getBgStatus() {
        return bgStatus;
    }
    /**
     * @param bgStatus the bgStatus to set
     */
    public void setBgStatus(String bgStatus) {
        this.bgStatus = bgStatus;
    }
    /**
     * @return the pprStatus
     */
    public String getPprStatus() {
        return pprStatus;
    }
    /**
     * @param pprStatus the pprStatus to set
     */
    public void setPprStatus(String pprStatus) {
        this.pprStatus = pprStatus;
    }
    /**
     * @return the wtTestStatus
     */
    public String getWtTestStatus() {
        return wtTestStatus;
    }
    /**
     * @param wtTestStatus the wtTestStatus to set
     */
    public void setWtTestStatus(String wtTestStatus) {
        this.wtTestStatus = wtTestStatus;
    }
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((agitatorMode == null) ? 0 : agitatorMode.hashCode());
        result = prime * result + ((agitatorMotorFreq == null) ? 0 : agitatorMotorFreq.hashCode());
        result = prime * result + ((agitatorPitch == null) ? 0 : agitatorPitch.hashCode());
        result = prime * result + ((agitatorid == null) ? 0 : agitatorid.hashCode());
        result = prime * result + ((agitatorod == null) ? 0 : agitatorod.hashCode());
        result = prime * result + ((airPressure == null) ? 0 : airPressure.hashCode());
        result = prime * result + ((augerAgitatorFreq == null) ? 0 : augerAgitatorFreq.hashCode());
        result = prime * result + ((augerFillValve == null) ? 0 : augerFillValve.hashCode());
        result = prime * result + ((augurPitch == null) ? 0 : augurPitch.hashCode());
        result = prime * result + ((bgLenSetting == null) ? 0 : bgLenSetting.hashCode());
        result = prime * result + ((bgStatus == null) ? 0 : bgStatus.hashCode());
        result = prime * result + ((customer == null) ? 0 : customer.hashCode());
        result = prime * result + ((customerFilm == null) ? 0 : customerFilm.hashCode());
        result = prime * result + ((customerProduct == null) ? 0 : customerProduct.hashCode());
        result = prime * result + ((dropStatus == null) ? 0 : dropStatus.hashCode());
        result = prime * result + ((fillValveGap == null) ? 0 : fillValveGap.hashCode());
        result = prime * result + ((fillVfdFreq == null) ? 0 : fillVfdFreq.hashCode());
        result = prime * result + ((fillerType == null) ? 0 : fillerType.hashCode());
        result = prime * result + ((filmType == null) ? 0 : filmType.hashCode());
        result = prime * result + ((head == null) ? 0 : head.hashCode());
        result = prime * result + ((horizBandSealSetting == null) ? 0 : horizBandSealSetting.hashCode());
        result = prime * result + ((horizCutSealSetting == null) ? 0 : horizCutSealSetting.hashCode());
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + ((inspector == null) ? 0 : inspector.hashCode());
        result = prime * result + ((machineType == null) ? 0 : machineType.hashCode());
        result = prime * result + ((mcSerialNum == null) ? 0 : mcSerialNum.hashCode());
        result = prime * result + ((negWt == null) ? 0 : negWt.hashCode());
        result = prime * result + ((oiNum == null) ? 0 : oiNum.hashCode());
        result = prime * result + ((operator == null) ? 0 : operator.hashCode());
        result = prime * result + ((overallStatus == null) ? 0 : overallStatus.hashCode());
        result = prime * result + ((pouchEqHorizSerrations == null) ? 0 : pouchEqHorizSerrations.hashCode());
        result = prime * result + ((pouchFillEvent == null) ? 0 : pouchFillEvent.hashCode());
        result = prime * result + ((pouchFilmThickness == null) ? 0 : pouchFilmThickness.hashCode());
        result = prime * result + ((pouchGusset == null) ? 0 : pouchGusset.hashCode());
        result = prime * result + ((pouchLength == null) ? 0 : pouchLength.hashCode());
        result = prime * result + ((pouchPrBulk == null) ? 0 : pouchPrBulk.hashCode());
        result = prime * result + ((pouchSealType == null) ? 0 : pouchSealType.hashCode());
        result = prime * result + ((pouchTempHZB == null) ? 0 : pouchTempHZB.hashCode());
        result = prime * result + ((pouchTempHZF == null) ? 0 : pouchTempHZF.hashCode());
        result = prime * result + ((pouchTempVert == null) ? 0 : pouchTempVert.hashCode());
        result = prime * result + ((pouchTempVertBackLeft == null) ? 0 : pouchTempVertBackLeft.hashCode());
        result = prime * result + ((pouchTempVertBackRight == null) ? 0 : pouchTempVertBackRight.hashCode());
        result = prime * result + ((pouchTempVertFrontLeft == null) ? 0 : pouchTempVertFrontLeft.hashCode());
        result = prime * result + ((pouchTempVertFrontRight == null) ? 0 : pouchTempVertFrontRight.hashCode());
        result = prime * result + ((pouchWidth == null) ? 0 : pouchWidth.hashCode());
        result = prime * result + ((pprStatus == null) ? 0 : pprStatus.hashCode());
        result = prime * result + ((prBulkMain == null) ? 0 : prBulkMain.hashCode());
        result = prime * result + ((product == null) ? 0 : product.hashCode());
        result = prime * result + ((productType == null) ? 0 : productType.hashCode());
        result = prime * result + ((productfeed == null) ? 0 : productfeed.hashCode());
        result = prime * result + ((qtySetting == null) ? 0 : qtySetting.hashCode());
        result = prime * result + ((sealStatus == null) ? 0 : sealStatus.hashCode());
        result = prime * result + ((tagid == null) ? 0 : tagid.hashCode());
        result = prime * result + ((targetSpeed == null) ? 0 : targetSpeed.hashCode());
        result = prime * result + ((targetSpeedStatus == null) ? 0 : targetSpeedStatus.hashCode());
        result = prime * result + ((targetWt == null) ? 0 : targetWt.hashCode());
        result = prime * result + ((targetWtUnit == null) ? 0 : targetWtUnit.hashCode());
        result = prime * result + ((targetacctype == null) ? 0 : targetacctype.hashCode());
        result = prime * result + ((targetaccval == null) ? 0 : targetaccval.hashCode());
        result = prime * result + ((vertSealSetting == null) ? 0 : vertSealSetting.hashCode());
        result = prime * result + ((wtTestStatus == null) ? 0 : wtTestStatus.hashCode());
        return result;
    }
    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Tag)) {
            return false;
        }
        Tag other = (Tag) obj;
        if (agitatorMode == null) {
            if (other.agitatorMode != null) {
                return false;
            }
        } else if (!agitatorMode.equals(other.agitatorMode)) {
            return false;
        }
        if (agitatorMotorFreq == null) {
            if (other.agitatorMotorFreq != null) {
                return false;
            }
        } else if (!agitatorMotorFreq.equals(other.agitatorMotorFreq)) {
            return false;
        }
        if (agitatorPitch == null) {
            if (other.agitatorPitch != null) {
                return false;
            }
        } else if (!agitatorPitch.equals(other.agitatorPitch)) {
            return false;
        }
        if (agitatorid == null) {
            if (other.agitatorid != null) {
                return false;
            }
        } else if (!agitatorid.equals(other.agitatorid)) {
            return false;
        }
        if (agitatorod == null) {
            if (other.agitatorod != null) {
                return false;
            }
        } else if (!agitatorod.equals(other.agitatorod)) {
            return false;
        }
        if (airPressure == null) {
            if (other.airPressure != null) {
                return false;
            }
        } else if (!airPressure.equals(other.airPressure)) {
            return false;
        }
        if (augerAgitatorFreq == null) {
            if (other.augerAgitatorFreq != null) {
                return false;
            }
        } else if (!augerAgitatorFreq.equals(other.augerAgitatorFreq)) {
            return false;
        }
        if (augerFillValve == null) {
            if (other.augerFillValve != null) {
                return false;
            }
        } else if (!augerFillValve.equals(other.augerFillValve)) {
            return false;
        }
        if (augurPitch == null) {
            if (other.augurPitch != null) {
                return false;
            }
        } else if (!augurPitch.equals(other.augurPitch)) {
            return false;
        }
        if (bgLenSetting == null) {
            if (other.bgLenSetting != null) {
                return false;
            }
        } else if (!bgLenSetting.equals(other.bgLenSetting)) {
            return false;
        }
        if (bgStatus == null) {
            if (other.bgStatus != null) {
                return false;
            }
        } else if (!bgStatus.equals(other.bgStatus)) {
            return false;
        }
        if (customer == null) {
            if (other.customer != null) {
                return false;
            }
        } else if (!customer.equals(other.customer)) {
            return false;
        }
        if (customerFilm == null) {
            if (other.customerFilm != null) {
                return false;
            }
        } else if (!customerFilm.equals(other.customerFilm)) {
            return false;
        }
        if (customerProduct == null) {
            if (other.customerProduct != null) {
                return false;
            }
        } else if (!customerProduct.equals(other.customerProduct)) {
            return false;
        }
        if (dropStatus == null) {
            if (other.dropStatus != null) {
                return false;
            }
        } else if (!dropStatus.equals(other.dropStatus)) {
            return false;
        }
        if (fillValveGap == null) {
            if (other.fillValveGap != null) {
                return false;
            }
        } else if (!fillValveGap.equals(other.fillValveGap)) {
            return false;
        }
        if (fillVfdFreq == null) {
            if (other.fillVfdFreq != null) {
                return false;
            }
        } else if (!fillVfdFreq.equals(other.fillVfdFreq)) {
            return false;
        }
        if (fillerType == null) {
            if (other.fillerType != null) {
                return false;
            }
        } else if (!fillerType.equals(other.fillerType)) {
            return false;
        }
        if (filmType == null) {
            if (other.filmType != null) {
                return false;
            }
        } else if (!filmType.equals(other.filmType)) {
            return false;
        }
        if (head == null) {
            if (other.head != null) {
                return false;
            }
        } else if (!head.equals(other.head)) {
            return false;
        }
        if (horizBandSealSetting == null) {
            if (other.horizBandSealSetting != null) {
                return false;
            }
        } else if (!horizBandSealSetting.equals(other.horizBandSealSetting)) {
            return false;
        }
        if (horizCutSealSetting == null) {
            if (other.horizCutSealSetting != null) {
                return false;
            }
        } else if (!horizCutSealSetting.equals(other.horizCutSealSetting)) {
            return false;
        }
        if (id != other.id) {
            return false;
        }
        if (inspector == null) {
            if (other.inspector != null) {
                return false;
            }
        } else if (!inspector.equals(other.inspector)) {
            return false;
        }
        if (machineType == null) {
            if (other.machineType != null) {
                return false;
            }
        } else if (!machineType.equals(other.machineType)) {
            return false;
        }
        if (mcSerialNum == null) {
            if (other.mcSerialNum != null) {
                return false;
            }
        } else if (!mcSerialNum.equals(other.mcSerialNum)) {
            return false;
        }
        if (negWt == null) {
            if (other.negWt != null) {
                return false;
            }
        } else if (!negWt.equals(other.negWt)) {
            return false;
        }
        if (oiNum == null) {
            if (other.oiNum != null) {
                return false;
            }
        } else if (!oiNum.equals(other.oiNum)) {
            return false;
        }
        if (operator == null) {
            if (other.operator != null) {
                return false;
            }
        } else if (!operator.equals(other.operator)) {
            return false;
        }
        if (overallStatus == null) {
            if (other.overallStatus != null) {
                return false;
            }
        } else if (!overallStatus.equals(other.overallStatus)) {
            return false;
        }
        if (pouchEqHorizSerrations == null) {
            if (other.pouchEqHorizSerrations != null) {
                return false;
            }
        } else if (!pouchEqHorizSerrations.equals(other.pouchEqHorizSerrations)) {
            return false;
        }
        if (pouchFillEvent == null) {
            if (other.pouchFillEvent != null) {
                return false;
            }
        } else if (!pouchFillEvent.equals(other.pouchFillEvent)) {
            return false;
        }
        if (pouchFilmThickness == null) {
            if (other.pouchFilmThickness != null) {
                return false;
            }
        } else if (!pouchFilmThickness.equals(other.pouchFilmThickness)) {
            return false;
        }
        if (pouchGusset == null) {
            if (other.pouchGusset != null) {
                return false;
            }
        } else if (!pouchGusset.equals(other.pouchGusset)) {
            return false;
        }
        if (pouchLength == null) {
            if (other.pouchLength != null) {
                return false;
            }
        } else if (!pouchLength.equals(other.pouchLength)) {
            return false;
        }
        if (pouchPrBulk == null) {
            if (other.pouchPrBulk != null) {
                return false;
            }
        } else if (!pouchPrBulk.equals(other.pouchPrBulk)) {
            return false;
        }
        if (pouchSealType == null) {
            if (other.pouchSealType != null) {
                return false;
            }
        } else if (!pouchSealType.equals(other.pouchSealType)) {
            return false;
        }
        if (pouchTempHZB == null) {
            if (other.pouchTempHZB != null) {
                return false;
            }
        } else if (!pouchTempHZB.equals(other.pouchTempHZB)) {
            return false;
        }
        if (pouchTempHZF == null) {
            if (other.pouchTempHZF != null) {
                return false;
            }
        } else if (!pouchTempHZF.equals(other.pouchTempHZF)) {
            return false;
        }
        if (pouchTempVert == null) {
            if (other.pouchTempVert != null) {
                return false;
            }
        } else if (!pouchTempVert.equals(other.pouchTempVert)) {
            return false;
        }
        if (pouchTempVertBackLeft == null) {
            if (other.pouchTempVertBackLeft != null) {
                return false;
            }
        } else if (!pouchTempVertBackLeft.equals(other.pouchTempVertBackLeft)) {
            return false;
        }
        if (pouchTempVertBackRight == null) {
            if (other.pouchTempVertBackRight != null) {
                return false;
            }
        } else if (!pouchTempVertBackRight.equals(other.pouchTempVertBackRight)) {
            return false;
        }
        if (pouchTempVertFrontLeft == null) {
            if (other.pouchTempVertFrontLeft != null) {
                return false;
            }
        } else if (!pouchTempVertFrontLeft.equals(other.pouchTempVertFrontLeft)) {
            return false;
        }
        if (pouchTempVertFrontRight == null) {
            if (other.pouchTempVertFrontRight != null) {
                return false;
            }
        } else if (!pouchTempVertFrontRight.equals(other.pouchTempVertFrontRight)) {
            return false;
        }
        if (pouchWidth == null) {
            if (other.pouchWidth != null) {
                return false;
            }
        } else if (!pouchWidth.equals(other.pouchWidth)) {
            return false;
        }
        if (pprStatus == null) {
            if (other.pprStatus != null) {
                return false;
            }
        } else if (!pprStatus.equals(other.pprStatus)) {
            return false;
        }
        if (prBulkMain == null) {
            if (other.prBulkMain != null) {
                return false;
            }
        } else if (!prBulkMain.equals(other.prBulkMain)) {
            return false;
        }
        if (product == null) {
            if (other.product != null) {
                return false;
            }
        } else if (!product.equals(other.product)) {
            return false;
        }
        if (productType == null) {
            if (other.productType != null) {
                return false;
            }
        } else if (!productType.equals(other.productType)) {
            return false;
        }
        if (productfeed == null) {
            if (other.productfeed != null) {
                return false;
            }
        } else if (!productfeed.equals(other.productfeed)) {
            return false;
        }
        if (qtySetting == null) {
            if (other.qtySetting != null) {
                return false;
            }
        } else if (!qtySetting.equals(other.qtySetting)) {
            return false;
        }
        if (sealStatus == null) {
            if (other.sealStatus != null) {
                return false;
            }
        } else if (!sealStatus.equals(other.sealStatus)) {
            return false;
        }
        if (tagid == null) {
            if (other.tagid != null) {
                return false;
            }
        } else if (!tagid.equals(other.tagid)) {
            return false;
        }
        if (targetSpeed == null) {
            if (other.targetSpeed != null) {
                return false;
            }
        } else if (!targetSpeed.equals(other.targetSpeed)) {
            return false;
        }
        if (targetSpeedStatus == null) {
            if (other.targetSpeedStatus != null) {
                return false;
            }
        } else if (!targetSpeedStatus.equals(other.targetSpeedStatus)) {
            return false;
        }
        if (targetWt == null) {
            if (other.targetWt != null) {
                return false;
            }
        } else if (!targetWt.equals(other.targetWt)) {
            return false;
        }
        if (targetWtUnit == null) {
            if (other.targetWtUnit != null) {
                return false;
            }
        } else if (!targetWtUnit.equals(other.targetWtUnit)) {
            return false;
        }
        if (targetacctype == null) {
            if (other.targetacctype != null) {
                return false;
            }
        } else if (!targetacctype.equals(other.targetacctype)) {
            return false;
        }
        if (targetaccval == null) {
            if (other.targetaccval != null) {
                return false;
            }
        } else if (!targetaccval.equals(other.targetaccval)) {
            return false;
        }
        if (vertSealSetting == null) {
            if (other.vertSealSetting != null) {
                return false;
            }
        } else if (!vertSealSetting.equals(other.vertSealSetting)) {
            return false;
        }
        if (wtTestStatus == null) {
            if (other.wtTestStatus != null) {
                return false;
            }
        } else if (!wtTestStatus.equals(other.wtTestStatus)) {
            return false;
        }
        return true;
    }
}
