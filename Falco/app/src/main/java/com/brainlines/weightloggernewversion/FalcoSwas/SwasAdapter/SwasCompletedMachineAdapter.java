package com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompletedMachineModel;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;


public class SwasCompletedMachineAdapter extends RecyclerView.Adapter< SwasCompletedMachineAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<SwasCompletedMachineModel> machine_list;
    View.OnClickListener mClickListener;
    Context context;
    String str_start_date,str_end_Date,str_tk_id,str_approved_by,str_tk_release_date,main_string_release_date;
    String str_tkid;
    RecyclerView rv_packingtickets;
    SwasCompletedMachineAdapter adapter;

    public SwasCompletedMachineAdapter(Context ctx, ArrayList<SwasCompletedMachineModel> machine_list){
        inflater = LayoutInflater.from(ctx);
        this.machine_list = machine_list;
        this.context=ctx;
    }


    @Override
    public int getItemCount() {
        return machine_list.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_swas_completed_machine_parameters, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final  SwasCompletedMachineModel model = machine_list.get(position);
        if (model!=null) {

            holder.txt_Machine_Date.setText(model.getServiceCallStartDate());
            holder.txt_Machine_Name.setText(model. getMachineParameters());
            holder.txt_Machine_Values.setText(model.getParameterValue());
            holder.txt_Machine_Remark.setText(model.getRemark());


        }
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{
            TextView txt_Machine_Date,txt_Machine_Name,txt_Machine_Values,txt_Machine_Remark;

        public MyViewHolder(View itemView) {
                super(itemView);
            txt_Machine_Date=itemView.findViewById(R.id.txt_Machine_Date);
            txt_Machine_Name=itemView.findViewById(R.id.txt_Machine_Name);
            txt_Machine_Values=itemView.findViewById(R.id.txt_Machine_Values);
            txt_Machine_Remark=itemView.findViewById(R.id.txt_MachineRemark);


            }

    }
}












