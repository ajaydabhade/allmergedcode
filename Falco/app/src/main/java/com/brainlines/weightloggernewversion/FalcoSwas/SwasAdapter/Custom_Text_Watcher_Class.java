package com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter;

import android.text.Editable;
import android.text.TextWatcher;

public class Custom_Text_Watcher_Class implements TextWatcher {
    private boolean mWasEdited = false;

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (mWasEdited){

            mWasEdited = false;
            return;
        }

        // get entered value (if required)
        String enteredValue  = s.toString();

        String newValue = "new value";

        // don't get trap into infinite loop
        mWasEdited = true;
        // just replace entered value with whatever you want
        s.replace(0, s.length(), newValue);


    }

}
