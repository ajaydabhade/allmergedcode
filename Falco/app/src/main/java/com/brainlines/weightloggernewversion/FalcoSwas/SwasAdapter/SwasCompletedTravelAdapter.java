package com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompletedTravelModel;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class SwasCompletedTravelAdapter extends RecyclerView.Adapter< SwasCompletedTravelAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<SwasCompletedTravelModel> travel_list;
    View.OnClickListener mClickListener;
    Context context;
    String str_start_date,str_end_Date,str_tk_id,str_approved_by,str_tk_release_date,main_string_release_date;
    String str_tkid;
    RecyclerView rv_completed_travelling_expenses;
    SwasCompletedTravelAdapter adapter;

    public SwasCompletedTravelAdapter(Context ctx, ArrayList<SwasCompletedTravelModel> travel_list){
        inflater = LayoutInflater.from(ctx);
        this.travel_list = travel_list;
        this.context=ctx;
    }


    @Override
    public int getItemCount() {
        return travel_list.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_swas_completed_travelling_expenses, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final  SwasCompletedTravelModel model = travel_list.get(position);
        if (model!=null) {

            holder.txt_TravelDate.setText(model.getVisitDates());
            holder.txt_Travel.setText(model.getService_Expense_Heads_Name());
            holder.txt_TravelCharges.setText(model.getVisitCharges());



        }
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txt_TravelDate,txt_Travel,txt_TravelCharges;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_TravelDate=itemView.findViewById(R.id.txt_TravelDate);
            txt_Travel=itemView.findViewById(R.id.txt_Travel);
            txt_TravelCharges=itemView.findViewById(R.id.txt_TravelCharges);



        }

    }
}

