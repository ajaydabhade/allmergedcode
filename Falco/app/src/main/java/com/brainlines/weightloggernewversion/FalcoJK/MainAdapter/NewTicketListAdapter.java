package com.brainlines.weightloggernewversion.FalcoJK.MainAdapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.ProdTkOperationModel;
import com.brainlines.weightloggernewversion.FalcoJK.Listeners.JKProductionUpdateListener;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class NewTicketListAdapter extends RecyclerView.Adapter<NewTicketListAdapter.MyViewHolder> {

    public String TAG = "NewTicketListAdapter";
    Context context;
    ArrayList<ProdTkOperationModel> ticketList = new ArrayList<>();
    JKProductionUpdateListener JKProductionUpdateListener;

    String upedOkayValue,upedReworkValue,upedRejectValue,upedWipValue,upedSecondValue;
    String parent_plant;


    public NewTicketListAdapter(Context context, ArrayList<ProdTkOperationModel> ticketList, JKProductionUpdateListener JKProductionUpdateListener,String parent_plant) {
        this.context = context;
        this.ticketList = ticketList;
        this.JKProductionUpdateListener = JKProductionUpdateListener;
        this.parent_plant = parent_plant;
    }

    @NonNull
    @Override
    public NewTicketListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_update_production_ticket_data,parent,false);
        return new NewTicketListAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final NewTicketListAdapter.MyViewHolder holder, final int position) {
        final ProdTkOperationModel model = ticketList.get(position);
        if (model!=null){
            String actualend_date = model.getTk_Actual_End_Date();
            if(!actualend_date.equals("null"))
            {
                holder.img_submit.setEnabled(false);
                holder.img_route.setEnabled(false);
            }
            else
            {
                holder.img_submit.setEnabled(true);
                holder.img_route.setEnabled(true);


            }

            if(!model.getPtk_ChangeRoute_Flag().equals("N"))
            {
                if (model.getPtkd_Qty()!=0){
                    holder.txtTotalQty.setText(model.getPtkd_Qty() +"");
                }

            }
            else
            {
                holder.txtTotalQty.setText("-");
            }
            if (!model.getOpr_operation_name().equals("")){
                holder.operation.setText(model.getOpr_operation_name());
            }
            if (!model.getUni_unit_name().equals("")){
                holder.changeroute.setText(model.getUni_unit_name());
            }


            if (model.getVsCode()!=null){
                holder.vsCode.setText(model.getVsCode());
            }
            if (model.getPtk_OkQty()!=0){
                holder.upedokay.setText(model.getPtk_OkQty()+"");
            }
            if (model.getPtk_ReworkQty()!=0){
                holder.upedrework.setText(model.getPtk_ReworkQty()+"");
            }
            if (model.getPtk_RejectQty()!=0){
                holder.upedreject.setText(model.getPtk_RejectQty()+"");
            }
            if (model.getPtk_SecondQty()!=0){
                holder.upedsecond.setText(model.getPtk_SecondQty()+"");
            }
            if (model.getPtk_WIPQty()!=0){
                holder.upedwip.setText(model.getPtk_WIPQty()+"");
            }
            holder.img_route.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JKProductionUpdateListener.imgRouteIsClicked(true,model);
                }
            });



            /*if(model.getTk_Actual_End_Date()!=null)
            {
                holder.img_submit.setEnabled(false);

            }
            else
            {
                holder.img_submit.setEnabled(true);
            }*/

            holder.upedokay.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    upedOkayValue = s.toString();
                    int wipflag=0;

                    if (s!=null){

                        if(!upedOkayValue.equals(""))
                        {

                            if (position!=0)
                            {
                                 JKProductionUpdateListener.getOkValue(upedOkayValue, String.valueOf(model.getPtk_RejectQty()),String.valueOf(model.getPtk_SecondQty()),model.getPtk_WIPQty() + "");
                                 ticketList.get(position).setPtk_OkQty_ans(upedOkayValue);

                                //holder.upedokay.getText().clear();
                                for(int i=position;i<ticketList.size();i++)
                                {
                                    String rowplant = model.getPtkd_Plant();
                                    String actualplant = ticketList.get(i).getPtkd_Plant();
                                    String rowoperation =model.getOpr_operation_name();
                                    String actualoperation = ticketList.get(i).getOpr_operation_name();
                                    if (rowplant.equals(actualplant) && !rowoperation.equals(actualoperation))
                                    {
                                        int wip = Integer.parseInt(upedOkayValue) + model.getPtk_WIPQty();
                                        String wipans = String.valueOf(ticketList.get(i).getPtk_WIPQty());
                                        wipflag = 1;

                                        if (wipans.equals("0"))
                                        {
                                            ticketList.get(i).setPtk_WIPQty_ans(upedOkayValue);
                                        }
                                        else
                                        {
                                            if (ticketList.get(i).getPtk_WIPQty() == 0)
                                            {
                                                ticketList.get(i).setPtk_WIPQty_ans(wip + "");
                                            }
                                            else
                                            {
                                                int wip1 = Integer.parseInt(upedOkayValue) + ticketList.get(i).getPtk_WIPQty();
                                                ticketList.get(i).setPtk_WIPQty_ans(String.valueOf(wip1));
                                            }

                                        }
                                        String curentrow_wip = URL_Constants.substraction(String.valueOf(model.getPtk_WIPQty()),upedOkayValue);
                                        if (!curentrow_wip.equals("NA"))
                                        {
                                            int cur_wip=Integer.parseInt(curentrow_wip);
                                            if (cur_wip < 0)
                                            {
                                                holder.upedwip.setText("0");
                                            }
                                            else
                                            {

                                                holder.upedwip.setText(String.valueOf(cur_wip));
                                                ticketList.get(position).setPtk_WIPQty_ans(curentrow_wip);
                                            }
                                        }

                                        break;
                                    }
                                    else
                                    {
                                        if (rowplant.equals(actualplant))
                                        {
                                            if (!rowoperation.equals(actualoperation))
                                            {
                                                int wip = Integer.parseInt(upedOkayValue) + model.getPtk_WIPQty();
                                                String wipans = String.valueOf(ticketList.get(i).getPtk_WIPQty());
                                                wipflag = 1;
                                                if (wipans.equals("0"))
                                                {
                                                    ticketList.get(i).setPtk_WIPQty_ans(upedOkayValue);
                                                }
                                                else
                                                {
                                                    if (ticketList.get(i).getPtk_WIPQty() == 0)
                                                    {
                                                        ticketList.get(i).setPtk_WIPQty_ans(wip + "");
                                                    }
                                                    else
                                                    {
                                                        int wip1 = Integer.parseInt(upedOkayValue) + ticketList.get(i).getPtk_WIPQty();
                                                        ticketList.get(i).setPtk_WIPQty_ans(String.valueOf(wip1));
                                                    }
                                                }

                                                String curentrow_wip = URL_Constants.substraction(upedOkayValue,String.valueOf(model.getPtk_WIPQty()));
                                                if (!curentrow_wip.equals("NA"))
                                                {
                                                    int cur_wip=Integer.parseInt(curentrow_wip);
                                                    if (cur_wip < 0)
                                                    {
                                                        holder.upedwip.setText("0");
                                                    }
                                                    else
                                                    {
                                                        /* model.setPtk_WIPQty(cur_wip);*/
                                                        holder.upedwip.setText(String.valueOf(cur_wip));
                                                        ticketList.get(position).setPtk_WIPQty_ans(curentrow_wip);
                                                    }
                                                }

                                                break;

                                            }
                                            else
                                            {
                                                if (i==ticketList.size()-1)
                                                {
                                                    //int wip = Integer.parseInt(upedOkayValue) + model.getPtk_WIPQty();
                                                    String wipans = String.valueOf(ticketList.get(i).getPtk_WIPQty());
                                                    wipflag = 1;
                                                    if (wipans.equals("0"))
                                                    {
                                                        ticketList.get(i).setPtk_WIPQty_ans(upedOkayValue);
                                                    }
                                                    else
                                                    {
                                                        if (ticketList.get(i).getPtk_WIPQty() == 0)
                                                        {
                                                            //ticketList.get(i).setPtk_WIPQty_ans(wip + "");
                                                        }
                                                        else
                                                        {
                                                            //int wip1 = Integer.parseInt(upedOkayValue) + ticketList.get(i).getPtk_WIPQty();
                                                            ticketList.get(i).setPtk_WIPQty_ans(String.valueOf(model.getPtk_WIPQty()));
                                                        }
                                                    }
                                                    String curentrow_wip = URL_Constants.substraction(String.valueOf(model.getPtk_WIPQty()),upedOkayValue);
                                                    if (!curentrow_wip.equals("NA"))
                                                    {
                                                        int cur_wip=Integer.parseInt(curentrow_wip);
                                                        if (cur_wip < 0)
                                                        {
                                                            holder.upedwip.setText("0");
                                                        }
                                                        else
                                                        {
                                                            /*model.setPtk_WIPQty(cur_wip);*/
                                                            holder.upedwip.setText(String.valueOf(cur_wip));
                                                            ticketList.get(position).setPtk_WIPQty_ans(curentrow_wip);
                                                        }
                                                    }

                                                    break;
                                                }

                                            }
                                        }

                                    }

                                }

                                if (wipflag == 0)
                                {
                                    for(int i=position;i<ticketList.size();i++) {

                                        String actualplant = ticketList.get(i).getPtkd_Plant();

                                        if (actualplant.equals(parent_plant))
                                        {
                                            int wip = Integer.parseInt(upedOkayValue) + model.getPtk_WIPQty();
                                            String wipans = String.valueOf(ticketList.get(i).getPtk_WIPQty());

                                            if (wipans.equals("0"))
                                            {
                                                ticketList.get(i).setPtk_WIPQty_ans(upedOkayValue);
                                            }
                                            else
                                            {
                                                if (ticketList.get(i).getPtk_WIPQty() == 0)
                                                {
                                                    ticketList.get(i).setPtk_WIPQty_ans(wip + "");
                                                }
                                                else
                                                {
                                                    int wip1 = Integer.parseInt(upedOkayValue) + ticketList.get(i).getPtk_WIPQty();
                                                    ticketList.get(i).setPtk_WIPQty_ans(String.valueOf(wip1));
                                                }

                                            }
                                            String curentrow_wip = URL_Constants.substraction(String.valueOf(model.getPtk_WIPQty()),upedOkayValue);
                                            if (!curentrow_wip.equals("NA"))
                                            {
                                                int cur_wip=Integer.parseInt(curentrow_wip);
                                                if (cur_wip < 0)
                                                {
                                                    holder.upedwip.setText("0");
                                                }
                                                else
                                                {
                                                    holder.upedwip.setText(String.valueOf(cur_wip));
                                                    ticketList.get(position).setPtk_WIPQty_ans(curentrow_wip);
                                                }
                                            }
                                            break;

                                        }
                                    }

                                }
                            }

                            else
                            {
                                ticketList.get(position).setPtk_OkQty_ans(upedOkayValue);
                                for(int i=position;i<ticketList.size();i++)
                                {
                                    String rowplant = model.getPtkd_Plant();
                                    String actualplant = ticketList.get(i).getPtkd_Plant();
                                    String rowoperation =model.getOpr_operation_name();
                                    String actualoperation = ticketList.get(i).getOpr_operation_name();
                                    if (rowplant.equals(actualplant) && !rowoperation.equals(actualoperation))
                                    {
                                        int wip = Integer.parseInt(upedOkayValue) + model.getPtk_WIPQty();
                                        String wipans = String.valueOf(ticketList.get(i).getPtk_WIPQty());
                                        wipflag = 1;

                                        if (wipans.equals("0"))
                                        {
                                            ticketList.get(i).setPtk_WIPQty_ans(upedOkayValue);
                                        }
                                        else
                                        {
                                            if (ticketList.get(i).getPtk_WIPQty() == 0)
                                            {
                                                ticketList.get(i).setPtk_WIPQty_ans(wip + "");
                                            }
                                            else
                                            {
                                                int wip1 = Integer.parseInt(upedOkayValue) + ticketList.get(i).getPtk_WIPQty();
                                                ticketList.get(i).setPtk_WIPQty_ans(String.valueOf(wip1));
                                            }

                                        }
                                        String curentrow_wip = URL_Constants.substraction(String.valueOf(model.getPtk_WIPQty()),upedOkayValue);
                                        if (!curentrow_wip.equals("NA"))
                                        {
                                            int cur_wip=Integer.parseInt(curentrow_wip);
                                            if (cur_wip < 0)
                                            {
                                                holder.upedwip.setText("0");
                                            }
                                            else
                                            {

                                                holder.upedwip.setText(String.valueOf(cur_wip));
                                                ticketList.get(position).setPtk_WIPQty_ans(curentrow_wip);
                                            }
                                        }

                                        break;
                                    }
                                    else
                                    {
                                        if (rowplant.equals(actualplant))
                                        {
                                            if (!rowoperation.equals(actualoperation))
                                            {
                                                int wip = Integer.parseInt(upedOkayValue) + model.getPtk_WIPQty();
                                                String wipans = String.valueOf(ticketList.get(i).getPtk_WIPQty());
                                                wipflag = 1;
                                                if (wipans.equals("0"))
                                                {
                                                    ticketList.get(i).setPtk_WIPQty_ans(upedOkayValue);
                                                }
                                                else
                                                {
                                                    if (ticketList.get(i).getPtk_WIPQty() == 0)
                                                    {
                                                        ticketList.get(i).setPtk_WIPQty_ans(wip + "");
                                                    }
                                                    else
                                                    {
                                                        int wip1 = Integer.parseInt(upedOkayValue) + ticketList.get(i).getPtk_WIPQty();
                                                        ticketList.get(i).setPtk_WIPQty_ans(String.valueOf(wip1));
                                                    }
                                                }

                                                String curentrow_wip = URL_Constants.substraction(upedOkayValue,String.valueOf(model.getPtk_WIPQty()));
                                                if (!curentrow_wip.equals("NA"))
                                                {
                                                    int cur_wip=Integer.parseInt(curentrow_wip);
                                                    if (cur_wip < 0)
                                                    {
                                                        holder.upedwip.setText("0");
                                                    }
                                                    else
                                                    {
                                                        /* model.setPtk_WIPQty(cur_wip);*/
                                                        holder.upedwip.setText(String.valueOf(cur_wip));
                                                        ticketList.get(position).setPtk_WIPQty_ans(curentrow_wip);
                                                    }
                                                }

                                                break;

                                            }
                                            else
                                            {
                                                if (i==ticketList.size()-1)
                                                {
                                                    //int wip = Integer.parseInt(upedOkayValue) + model.getPtk_WIPQty();
                                                    String wipans = String.valueOf(ticketList.get(i).getPtk_WIPQty());
                                                    wipflag = 1;
                                                    if (wipans.equals("0"))
                                                    {
                                                        ticketList.get(i).setPtk_WIPQty_ans(upedOkayValue);
                                                    }
                                                    else
                                                    {
                                                        if (ticketList.get(i).getPtk_WIPQty() == 0)
                                                        {
                                                            //ticketList.get(i).setPtk_WIPQty_ans(wip + "");
                                                        }
                                                        else
                                                        {
                                                            //int wip1 = Integer.parseInt(upedOkayValue) + ticketList.get(i).getPtk_WIPQty();
                                                            ticketList.get(i).setPtk_WIPQty_ans(String.valueOf(model.getPtk_WIPQty()));
                                                        }
                                                    }
                                                    String curentrow_wip = URL_Constants.substraction(String.valueOf(model.getPtk_WIPQty()),upedOkayValue);
                                                    if (!curentrow_wip.equals("NA"))
                                                    {
                                                        int cur_wip=Integer.parseInt(curentrow_wip);
                                                        if (cur_wip < 0)
                                                        {
                                                            holder.upedwip.setText("0");
                                                        }
                                                        else
                                                        {
                                                            /*model.setPtk_WIPQty(cur_wip);*/
                                                            holder.upedwip.setText(String.valueOf(cur_wip));
                                                            ticketList.get(position).setPtk_WIPQty_ans(curentrow_wip);
                                                        }
                                                    }

                                                    break;
                                                }

                                            }
                                        }

                                    }

                                }

                                if (wipflag == 0)
                                {
                                    for(int i=position;i<ticketList.size();i++) {

                                        String actualplant = ticketList.get(i).getPtkd_Plant();

                                        if (actualplant.equals(parent_plant))
                                        {
                                            int wip = Integer.parseInt(upedOkayValue) + model.getPtk_WIPQty();
                                            String wipans = String.valueOf(ticketList.get(i).getPtk_WIPQty());

                                            if (wipans.equals("0"))
                                            {
                                                ticketList.get(i).setPtk_WIPQty_ans(upedOkayValue);
                                            }
                                            else
                                            {
                                                if (ticketList.get(i).getPtk_WIPQty() == 0)
                                                {
                                                    ticketList.get(i).setPtk_WIPQty_ans(wip + "");
                                                }
                                                else
                                                {
                                                    int wip1 = Integer.parseInt(upedOkayValue) + ticketList.get(i).getPtk_WIPQty();
                                                    ticketList.get(i).setPtk_WIPQty_ans(String.valueOf(wip1));
                                                }

                                            }
                                            String curentrow_wip = URL_Constants.substraction(String.valueOf(model.getPtk_WIPQty()),upedOkayValue);
                                            if (!curentrow_wip.equals("NA"))
                                            {
                                                int cur_wip=Integer.parseInt(curentrow_wip);
                                                if (cur_wip < 0)
                                                {
                                                    holder.upedwip.setText("0");
                                                }
                                                else
                                                {
                                                    holder.upedwip.setText(String.valueOf(cur_wip));
                                                    ticketList.get(position).setPtk_WIPQty_ans(curentrow_wip);
                                                }
                                            }
                                            break;

                                        }
                                    }

                                }
                            }

                        }
                    }
                    else if (s==null){
                        ticketList.get(position).setPtk_OkQty_ans("0");
                    }
                }
            });
            holder.upedrework.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    upedReworkValue = s.toString();
                    if (s!=null){
                        ticketList.get(position).setPtk_ReworkQty_ans(upedReworkValue);
                    }
                    else if (s==null && s.equals("")){
                        ticketList.get(position).setPtk_ReworkQty_ans("0");
                    }
                }
            });
            holder.upedreject.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    upedRejectValue = s.toString();
                    if (s!=null){
                        if (s.length()>0)
                        {
                            String wip_qty = model.getPtk_WIPQty_ans();

                            String ok_qty = holder.upedokay.getText().toString();
                            if (ok_qty.equals(""))
                            {
                                ok_qty = "0";
                            }
                            else
                            {
                                ok_qty = holder.upedokay.getText().toString();
                            }

                            JKProductionUpdateListener.getOkValue(ok_qty, String.valueOf(upedRejectValue),String.valueOf(model.getPtk_SecondQty()),model.getPtk_WIPQty() + "");
                            int totalrejectok = Integer.parseInt(ok_qty) + Integer.parseInt(upedRejectValue);

                            String curentrow_wip = URL_Constants.substraction(String.valueOf(model.getPtk_WIPQty()),String.valueOf(totalrejectok));

                            model.setPtk_WIPQty_ans(curentrow_wip);
                            model.setPtk_RejectQty_ans(upedRejectValue);

                            if (!curentrow_wip.equals("NA"))
                            {
                                int cur_wip=Integer.parseInt(curentrow_wip);
                                if (cur_wip < 0)
                                {
                                    holder.upedwip.setText("0");
                                }
                                else
                                {
                                    holder.upedwip.setText(String.valueOf(cur_wip));
                                }
                            }
                        }

                    }
                    else if (s==null){
                        holder.upedreject.setText("0");
                        ticketList.get(position).setPtk_RejectQty_ans("0");
                    }
                }
            });
            holder.upedsecond.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    upedSecondValue = s.toString();
                    if (s!=null){
                        if (s.length()>0)
                        {
                            String wip_qty = model.getPtk_WIPQty_ans();

                            String reject_qty= holder.upedreject.getText().toString();

                            String ok_qty = holder.upedokay.getText().toString();
                            if (ok_qty.equals(""))
                            {
                                ok_qty = "0";
                            }
                            else
                            {
                                ok_qty = holder.upedokay.getText().toString();
                            }
                            if (reject_qty.equals(""))
                            {
                                reject_qty = "0";
                            }
                            else
                            {
                                reject_qty= holder.upedreject.getText().toString();
                            }

                            JKProductionUpdateListener.getOkValue(ok_qty, reject_qty,upedSecondValue,model.getPtk_WIPQty() + "");

                            int totaloksecreject = Integer.parseInt(ok_qty) + Integer.parseInt(reject_qty) + Integer.parseInt(upedSecondValue) ;

                            String curentrow_wip = URL_Constants.substraction(String.valueOf(model.getPtk_WIPQty()),String.valueOf(totaloksecreject));

                            model.setPtk_WIPQty_ans(curentrow_wip);
                            model.setPtk_SecondQty_ans(upedSecondValue);

                            if (!curentrow_wip.equals("NA"))
                            {
                                int cur_wip=Integer.parseInt(curentrow_wip);
                                if (cur_wip < 0)
                                {
                                    holder.upedwip.setText("0");
                                }
                                else
                                {
                                    holder.upedwip.setText(String.valueOf(cur_wip));
                                }
                            }
                        }

                    }else if (s==null && s.equals("")){
                        holder.upedsecond.setText("0");
                        ticketList.get(position).setPtk_SecondQty_ans("0");
                    }
                }
            });



            holder.img_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ArrayList<ProdTkOperationModel> list=ticketList;
                    JKProductionUpdateListener.imgSubmitButtonIsClicked(ticketList);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return ticketList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView operation,changeroute, vsCode,txtTotalQty;
        EditText upedokay,upedrework,upedreject,upedwip,upedsecond;
        ImageButton img_submit,img_route;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            operation = (itemView).findViewById(R.id.upopeartions);
            changeroute = (itemView).findViewById(R.id.up_current_route);
            vsCode = (itemView).findViewById(R.id.up_VS);
            txtTotalQty = (itemView).findViewById(R.id.txtTotalQty);

            upedokay = (itemView).findViewById(R.id.upedokay);
            upedrework = (itemView).findViewById(R.id.upedrework);
            upedreject = (itemView).findViewById(R.id.upedreject);
            upedwip = (itemView).findViewById(R.id.upedwip);
            upedsecond = (itemView).findViewById(R.id.upedsecond);

            img_submit = (itemView).findViewById(R.id.img_submit);
            img_route = (itemView).findViewById(R.id.img_route);
        }
    }

    public static String rupeeFormat(String value){
        value=value.replace(",","");
        char lastDigit=value.charAt(value.length()-1);
        String result = "";
        int len = value.length()-1;
        int nDigits = 0;

        for (int i = len - 1; i >= 0; i--)
        {
            result = value.charAt(i) + result;
            nDigits++;
            if (((nDigits % 2) == 0) && (i > 0))
            {
                result = "," + result;
            }
        }
        return (result+lastDigit);
    }

    public String getIndianCurrencyFormat(String amount) {
        StringBuilder stringBuilder = new StringBuilder();
        char amountArray[] = amount.toCharArray();
        int a = 0, b = 0;
        for (int i = amountArray.length - 1; i >= 0; i--) {
            if (a < 3) {
                stringBuilder.append(amountArray[i]);
                a++;
            } else if (b < 2) {
                if (b == 0) {
                    stringBuilder.append(",");
                    stringBuilder.append(amountArray[i]);
                    b++;
                } else {
                    stringBuilder.append(amountArray[i]);
                    b = 0;
                }
            }
        }
        return stringBuilder.reverse().toString();
    }


}
