package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MasterHeadModel implements Parcelable {
    String headID;
    String oemId;
    String headType;
    boolean isActive;
    String createdDate;
    String modifiedDate;
    String createdBy;
    String modifiedBy;

    public MasterHeadModel(String headID, String oemId, String headType, boolean isActive, String createdDate, String modifiedDate, String createdBy, String modifiedBy) {
        this.headID = headID;
        this.oemId = oemId;
        this.headType = headType;
        this.isActive = isActive;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.createdBy = createdBy;
        this.modifiedBy = modifiedBy;
    }

    protected MasterHeadModel(Parcel in) {
        headID = in.readString();
        oemId = in.readString();
        headType = in.readString();
        isActive = in.readByte() != 0;
        createdDate = in.readString();
        modifiedDate = in.readString();
        createdBy = in.readString();
        modifiedBy = in.readString();
    }

    public static final Creator<MasterHeadModel> CREATOR = new Creator<MasterHeadModel>() {
        @Override
        public MasterHeadModel createFromParcel(Parcel in) {
            return new MasterHeadModel(in);
        }

        @Override
        public MasterHeadModel[] newArray(int size) {
            return new MasterHeadModel[size];
        }
    };

    public String getHeadID() {
        return headID;
    }

    public void setHeadID(String headID) {
        this.headID = headID;
    }

    public String getOemId() {
        return oemId;
    }

    public void setOemId(String oemId) {
        this.oemId = oemId;
    }

    public String getHeadType() {
        return headType;
    }

    public void setHeadType(String headType) {
        this.headType = headType;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(headID);
        dest.writeString(oemId);
        dest.writeString(headType);
        dest.writeByte((byte) (isActive ? 1 : 0));
        dest.writeString(createdDate);
        dest.writeString(modifiedDate);
        dest.writeString(createdBy);
        dest.writeString(modifiedBy);
    }
}
