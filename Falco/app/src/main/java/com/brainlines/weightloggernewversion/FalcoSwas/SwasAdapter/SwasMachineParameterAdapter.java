package com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompletedCallMachineParameterModel;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class SwasMachineParameterAdapter extends RecyclerView.Adapter<SwasMachineParameterAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<SwasCompletedCallMachineParameterModel> machine_parameter;
    View.OnClickListener mClickListener;
    Context context;
    String str_start_date,str_end_Date,str_tk_id,str_approved_by,str_tk_release_date,main_string_release_date;
    String str_tkid;
    RecyclerView rv_packingtickets;
    TicketListRvAdapter adapter;

    public SwasMachineParameterAdapter(Context ctx, ArrayList<SwasCompletedCallMachineParameterModel>  machine_parameter){
        inflater = LayoutInflater.from(ctx);
        this. machine_parameter =  machine_parameter;
        this.context=ctx;
    }
    @Override
    public int getItemCount() {
        return  machine_parameter.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_swas_machine_parameters, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final SwasCompletedCallMachineParameterModel model = machine_parameter.get(position);
        if (model!=null) {

            holder.txt_machine_parameter.setText(model.getMachineParameters());
            holder.txt_unit.setText(model.getUnit());
            holder.edMachineValue.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String stredexpensesvalue = s.toString();
                    if (s!=null){
                        machine_parameter.get(position).setParameterValue(stredexpensesvalue);
                    }
                    else if (s==null){
                        machine_parameter.get(position).setParameterValue("0");
                    }
                }
            });
            holder.ed_machine_remark.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String remark = s.toString();
                    if (s!=null){

                        machine_parameter.get(position).setRemark(remark);
                    }
                    else if (s==null){
                        machine_parameter.get(position).setRemark("NA");
                    }

                }
            });

        }

    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txt_machine_parameter,txt_unit;
        EditText edMachineValue,ed_machine_remark;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_machine_parameter=itemView.findViewById(R.id.txtMachineParameter);
            edMachineValue=itemView.findViewById(R.id.edMachineValue);
            ed_machine_remark = itemView.findViewById(R.id.edmachine_remark);
            txt_unit = itemView.findViewById(R.id.txt_unit);


        }
    }


}

