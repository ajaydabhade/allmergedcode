package com.brainlines.weightloggernewversion.FalcoJK.JKModel;

public class EquipTypeModel {

    String Eq_id,eq_type;

    public String getEq_id() {
        return Eq_id;
    }

    public void setEq_id(String eq_id) {
        Eq_id = eq_id;
    }

    public String getEq_type() {
        return eq_type;
    }

    public void setEq_type(String eq_type) {
        this.eq_type = eq_type;
    }
}
