package com.brainlines.weightloggernewversion.Production.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.Production.Model.P_WeightReading_History_Model;
import com.brainlines.weightloggernewversion.Production.Model.ProductionTagHistoryListModel;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.listener.ProductionTagListListener;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class ProductionHistoryWeightReadingAdapter extends RecyclerView.Adapter<ProductionHistoryWeightReadingAdapter.MyViewHolder> {

    Context context;
    ArrayList<P_WeightReading_History_Model> p_tag_list = new ArrayList<>();
    ProductionTagListListener listener;

    public ProductionHistoryWeightReadingAdapter(Context context, ArrayList<P_WeightReading_History_Model> p_tag_list) {
        this.context = context;
        this.p_tag_list = p_tag_list;
       // this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_batch_report_details,parent,false);
        return new ProductionHistoryWeightReadingAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final P_WeightReading_History_Model model = p_tag_list.get(position);
        if (model != null)
        {
            DecimalFormat decimalFormat = new DecimalFormat("0.000");
            final String weight = model.getWeight();
            Double dwtreading = Double.valueOf(weight);
            final String status = model.getStatus();
            final String excess = model.getExcess();
            Double dexcess = Double.parseDouble(excess);
            String date_time = model.getDate_time();
            String shift = model.getShift();
            String sr_no = model.getSr_no();

            holder.batch_report_readin_sr_no.setText(sr_no);
            holder.batch_report_excess.setText(decimalFormat.format(dexcess));

            holder.batch_report_date_time.setText(date_time);



            if (status.equals("Okay"))
            {
                holder.batch_report_weight.setText(decimalFormat.format(dwtreading));
                holder.batch_report_status.setText("Ok");
                //holder.linear_background.setBackground(context.getResources().getDrawable(R.drawable.table_background));


            }
            else
            {

                holder.batch_report_weight.setText(decimalFormat.format(dwtreading));
                holder.batch_report_status.setText("Ign");
                holder.batch_report_weight.setTextColor(context.getResources().getColor(R.color.color_2));
                //holder.linear_background.setBackground(context.getResources().getDrawable(R.drawable.red_background));


            }

       }





    }

    @Override
    public int getItemCount() {
        return p_tag_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView batch_report_readin_sr_no,batch_report_weight,batch_report_excess,batch_report_status,batch_report_date_time;
        LinearLayout linear_background;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            batch_report_readin_sr_no = (itemView).findViewById(R.id.batch_report_readin_sr_no1);
            batch_report_weight = (itemView).findViewById(R.id.batch_report_weight);
            batch_report_excess = (itemView).findViewById(R.id.batch_report_excess);
            batch_report_status = (itemView).findViewById(R.id.batch_report_status);
            batch_report_date_time = (itemView).findViewById(R.id.batch_report_date_time);


        }
    }
}
