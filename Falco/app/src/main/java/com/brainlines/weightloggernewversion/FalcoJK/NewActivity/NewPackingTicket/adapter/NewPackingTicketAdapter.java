package com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewPackingTicket.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewPackingTicket.Listener.NewPackingTicketListener;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewPackingTicket.Model.NewPackingTicketModel;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class NewPackingTicketAdapter extends RecyclerView.Adapter<NewPackingTicketAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<NewPackingTicketModel> ticket_list;
    Context context;
    String str_start_date,str_end_Date,str_tk_id,str_approved_by,str_tk_release_date,main_string_release_date;
    RecyclerView rv_packingtickets;
    NewPackingTicketAdapter adapter;
    NewPackingTicketListener newPackingTicketListener;


    public NewPackingTicketAdapter(Context ctx, ArrayList<NewPackingTicketModel> ticket_list, RecyclerView rv_packingtickets, NewPackingTicketAdapter adapter, NewPackingTicketListener newPackingTicketListener){
        inflater = LayoutInflater.from(ctx);
        this.ticket_list = ticket_list;
        this.rv_packingtickets=rv_packingtickets;
        this.adapter=adapter;
        this.newPackingTicketListener = newPackingTicketListener;
        this.context=ctx;
    }

    @Override
    public int getItemCount() {
        return ticket_list.size();
    }

    @NonNull
    @Override
    public NewPackingTicketAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_packing_list_details, parent, false);
        NewPackingTicketAdapter.MyViewHolder holder = new NewPackingTicketAdapter.MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final NewPackingTicketAdapter.MyViewHolder holder, final int i) {
        final NewPackingTicketModel model = ticket_list.get(i);

        String[] MainDate =ticket_list.get(i).getTk_Release_Date().split("T");
        String date=MainDate[0];
        //holder.tk_Release_date.setText(date);

        String dtStart = ticket_list.get(i).getTk_Release_Date();


        if(dtStart.equals("null"))
        {
            holder.tk_Release_date.setText("NA");
        }
        else
        {
            String strdate1= URL_Constants.DateConversion(dtStart);
            holder.tk_Release_date.setText(strdate1);
        }

        holder.tk_start_date.setText(URL_Constants.DateConversion(ticket_list.get(i).getTk_Start_Date()));
        holder.tk_end_date.setText(URL_Constants.DateConversion(ticket_list.get(i).getTk_End_Date()));
        holder.tk_no.setText(ticket_list.get(i).getTk_TicketNo());
        holder.tk_sku_13_d.setText(ticket_list.get(i).getTk_SKU());
        holder.tk_plant.setText(ticket_list.get(i).getTk_Plant());
        holder.tk_qty.setText(ticket_list.get(i).getTk_Quantity().concat(" no"));

        holder.tk_produce_qty.setText(ticket_list.get(i).getTotalqty().concat(" no"));
        String tkrelease=ticket_list.get(i).getTk_Release();
        String actualenddate=ticket_list.get(i).getTk_Actual_end_date();


        /*if (!actualenddate.equals("null"))
        {

            holder.rl_packing.setBackgroundResource(R.drawable.shape_background_for_close_ticket);

        }*/

        if (ticket_list.get(i).getTk_Unpacking().equals("N"))
        {
            holder.text_up_pack_sku.setVisibility(View.GONE);
            holder.unpacksku.setVisibility(View.GONE);
        }
        else
        {
            holder.text_up_pack_sku.setText(ticket_list.get(i).getTk_Unpaking_SKU());
        }
        if (tkrelease.equals("true"))
        {
            holder.img_btn_release.setVisibility(View.INVISIBLE);
            holder.img_icon_for_Release_ticket.setVisibility(View.INVISIBLE);
            holder.img_btn_update.setEnabled(true);
            holder.img_btn_view.setEnabled(true);
        }
        else
        {
            holder.img_btn_update.setEnabled(false);
            holder.img_btn_view.setEnabled(false);
        }

        if (holder.pack_details_layout.getVisibility()==View.VISIBLE)
        {
            holder.pack_img_plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    holder.pack_details_layout.setVisibility(View.VISIBLE);
                    holder.pack_img_plus.setImageResource(R.drawable.down_arrow20);
                }
            });
        }
        else
        {
            holder.pack_img_plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.pack_details_layout.setVisibility(View.GONE);
                    holder.pack_img_plus.setImageResource(R.drawable.up_arrow20);
                }
            });
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.pack_details_layout.getVisibility()==View.VISIBLE)
                {
                    holder.pack_img_plus.setImageResource(R.drawable.up_arrow20);
                }
                else
                {
                    holder.pack_img_plus.setImageResource(R.drawable.down_arrow20);
                }

                holder.pack_details_layout.setVisibility((holder.pack_details_layout.getVisibility() == View.VISIBLE)
                        ? View.GONE
                        : View.VISIBLE);
            }
        });
        holder.pack_details_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.pack_details_layout.setVisibility(View.GONE);
                holder.pack_img_plus.setImageResource(R.drawable.up_arrow20);

            }
        });


        holder.img_btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                newPackingTicketListener.isupdateClicked(model);



            }
        });
        holder.img_btn_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newPackingTicketListener.isviewClicked(model);

            }
        });

        holder.img_btn_release.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newPackingTicketListener.isreleaseClicked(model,holder.tk_Release_date,holder.img_btn_release,holder.img_btn_update,holder.img_btn_view,holder.img_icon_for_Release_ticket);
                //new ReleasePackingTicketCall().execute();
                //This is used for updateing the release date
                holder.tk_Release_date.setText(main_string_release_date);
                holder.img_btn_release.setVisibility(View.GONE);
                holder.img_btn_update.setEnabled(true);
                holder.img_btn_view.setEnabled(true);
                holder.img_icon_for_Release_ticket.setVisibility(View.GONE);
            }
        });
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tk_no,tk_plant,tk_sku_13_d,tk_Release_date,tk_start_date,tk_end_date,tk_qty,text_up_pack_sku,unpacksku,tk_produce_qty;
        ImageButton img_btn_update,img_btn_view,img_btn_release,pack_img_plus;
        RelativeLayout pack_details_layout;
        ImageView img_icon_for_Release_ticket;
        RelativeLayout rl_packing;
        public MyViewHolder(View itemView) {
            super(itemView);
            tk_produce_qty=itemView.findViewById(R.id.pack_produce_qty);
            tk_no= itemView.findViewById(R.id.pack_txt_ticket_no);
            tk_plant= itemView.findViewById(R.id.pack_txt_up_plant);
            tk_sku_13_d= itemView.findViewById(R.id.pack_txt_sku_13_digit);
            tk_Release_date= itemView.findViewById(R.id.pack_txt_release_date);
            tk_start_date= itemView.findViewById(R.id.pack_txt_up_start_date);
            tk_end_date= itemView.findViewById(R.id.pack_txt_end_date);
            tk_qty= itemView.findViewById(R.id.pack_tk_qty);
            img_btn_update= itemView.findViewById(R.id.pack_img_update);
            img_btn_view= itemView.findViewById(R.id.pro_img_view);
            img_btn_release= itemView.findViewById(R.id.pack_img_release);
            text_up_pack_sku=itemView.findViewById(R.id.text_up_pack_sku);
            unpacksku=itemView.findViewById(R.id.unpacksku);
            tk_produce_qty=itemView.findViewById(R.id.pack_produce_qty);
            rl_packing = itemView.findViewById(R.id.rl_packing);
            pack_img_plus= itemView.findViewById(R.id.pack_img_plus);
            pack_details_layout= itemView.findViewById(R.id.pack_details_list);
            img_icon_for_Release_ticket=itemView.findViewById(R.id.img_icon_for_Release_ticket);
        }
    }
}
