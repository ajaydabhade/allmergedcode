package com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompletedCheckListModel;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class SwasCheckboxCompletedIdDetailsAdapter extends RecyclerView.Adapter<SwasCheckboxCompletedIdDetailsAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<SwasCompletedCheckListModel> list1;
    View.OnClickListener mClickListener;
    Context context;
    String str_start_date,str_end_Date,str_tk_id,str_approved_by,str_tk_release_date,main_string_release_date;
    String str_tkid;
    RecyclerView rv_completed_customer_list,rv_comp_check_list;
    SwasCheckboxCompletedIdDetailsAdapter adapter;

    public SwasCheckboxCompletedIdDetailsAdapter(Context ctx, ArrayList<SwasCompletedCheckListModel> list1){
        inflater = LayoutInflater.from(ctx);
        this.list1 = list1;
        this.context=ctx;
    }

    @Override
    public int getItemCount() {
        return list1.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_completed_checklist_service_call_details, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }



    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int i) {
        final SwasCompletedCheckListModel model = list1.get(i);
        if (model!=null) {

            holder.txtCheckText.setText(model.getChecklistForSiteReadynessText());
            if (model.getChecked().equals("Y"))
            {
                holder.chk1.setChecked(true);
            }
            holder.chk1.setEnabled(false);

        }
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txtCheckText;
        CheckBox chk1;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtCheckText=itemView.findViewById(R.id.txtCheckText);
            chk1 = itemView.findViewById(R.id.chk1);


        }
    }


}

