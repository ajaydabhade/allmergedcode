package com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.Model.NewProductionTicketModel;
import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.listener.NewProductionTicketListener;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class NewProductionSelectionAdapter extends RecyclerView.Adapter<NewProductionSelectionAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    private ArrayList<NewProductionTicketModel> ticket_list;
    View.OnClickListener mClickListener;
    Context context;
    String str_tk_no;
    NewProductionTicketListener newProductionTicketListener;

    public NewProductionSelectionAdapter(Context ctx, ArrayList<NewProductionTicketModel> ticket_list, NewProductionTicketListener newProductionTicketListener){
        inflater = LayoutInflater.from(ctx);
        this.ticket_list = ticket_list;
        this.newProductionTicketListener =newProductionTicketListener;
        this.context=ctx;
    }

    @Override
    public int getItemCount() {
        return ticket_list.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_production_list_details, parent, false);
        final NewProductionSelectionAdapter.MyViewHolder holder = new NewProductionSelectionAdapter.MyViewHolder(view);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onClick(view);

            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int i) {
        final NewProductionTicketModel model=ticket_list.get(i);
        if (model!= null)
        {
            holder.tk_no.setText(ticket_list.get(i).getPtkd_TkNo());
            holder.tk_sku_8_d.setText(ticket_list.get(i).getPtkd_PSKU());
            holder.tk_plant.setText(ticket_list.get(i).getPtkd_Plant());

            holder.tk_value_stream.setText(ticket_list.get(i).getPtkd_ValueStreamCode());
            //holder.tk_prod_qty.setText();
            holder.tk_start_date.setText(ticket_list.get(i).getTk_Actual_Start_Date());
            holder.tk_end_date.setText(model.getTk_Actual_End_Date());


           /* if(ticket_list.get(i).getPtkd_IsRelease().equals("true"))
            {
                holder.tk_Release_date.setText("NA");
            }*/
            if (ticket_list.get(i).getPtkd_ReleaseDate().equals("null"))
            {
                holder.tk_Release_date.setText("NA");
            }
            else
            {
                holder.tk_Release_date.setText(ticket_list.get(i).getPtkd_ReleaseDate());
            }
            String total_qty = ticket_list.get(i).getPtkd_Qty();
            double constant = 12.0;
            try
            {
                double result = Double.parseDouble(total_qty) * constant;
                String qty = String.valueOf(result);
                holder.pro_txt_qty.setText(qty.concat(" no"));
            }
            catch (Exception e)
            {
                e.printStackTrace();
                holder.pro_txt_qty.setText(total_qty.concat(" no"));

            }
            String tkrelease=ticket_list.get(i).getPtkd_IsRelease();

            if (tkrelease.equals("true"))
            {
                holder.img_release.setVisibility(View.INVISIBLE);
                holder.img_icon_for_Release_ticket.setVisibility(View.INVISIBLE);
            }
            else if (holder.pro_details_layout.getVisibility()==View.VISIBLE)
            {
                holder.pro_img_plus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        holder.pro_details_layout.setVisibility(View.VISIBLE);
                        holder.pro_img_plus.setImageResource(R.drawable.down_arrow16);
                    }
                });
            }
            else
            {
                holder.pro_img_plus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        holder.pro_details_layout.setVisibility(View.GONE);
                        holder.pro_img_plus.setImageResource(R.drawable.up_arrow16);
                    }
                });
            }
            String actend_date = model.getTk_End_Date();
            if(!actend_date.equals("null"))
            {
                if (actend_date != null || !actend_date.equals("null"))
                {
                    if (!actend_date.equals("")){
                        holder.rl_production.setBackgroundResource(R.drawable.shape_background_for_close_ticket);
                    }
                }
            }

            if (tkrelease.equals("false"))
            {
                holder.img_btn_update.setEnabled(false);
                holder.img_btn_view.setEnabled(false);
                holder.img_btn_update.setClickable(false);
                holder.img_btn_view.setClickable(false);
            }
            else
            {
                holder.img_btn_update.setEnabled(true);
                holder.img_btn_view.setEnabled(true);
                holder.img_btn_update.setClickable(true);
                holder.img_btn_view.setClickable(true);
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.pro_details_layout.getVisibility()==View.VISIBLE)
                    {
                        holder.pro_img_plus.setImageResource(R.drawable.up_arrow16);
                    }
                    else
                    {
                        holder.pro_img_plus.setImageResource(R.drawable.down_arrow16);
                    }
                    holder.pro_details_layout.setVisibility((holder.pro_details_layout.getVisibility() == View.VISIBLE)
                            ? View.GONE
                            : View.VISIBLE);
                }
            });
            holder.pro_details_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.pro_details_layout.setVisibility(View.GONE);
                    holder.pro_img_plus.setImageResource(R.drawable.up_arrow16);

                }
            });

            holder.img_release.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    newProductionTicketListener.isreleaseClicked(model,holder.tk_Release_date,holder.img_release,holder.img_btn_update,holder.img_btn_view,holder.img_icon_for_Release_ticket);

                    /*str_tk_no=ticket_list.get(i).getPtkd_TkNo();
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
                    String str_tk_release_date = sdf.format(c.getTime());
                    String[] string =str_tk_release_date.split(" ");
                    String main_string_release_date=string[0];
                   // new ProductionRvAdapter.ReleaseProductionTkCall().execute();
                    holder.tk_Release_date.setText(main_string_release_date);
                    holder.img_release.setVisibility(View.GONE);
                    holder.img_btn_update.setEnabled(true);
                    holder.img_btn_view.setEnabled(true);
                    holder.img_btn_update.setClickable(true);
                    holder.img_btn_view.setClickable(true);
                    holder.img_icon_for_Release_ticket.setVisibility(View.GONE);*/


                }
            });

            holder.img_btn_update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    newProductionTicketListener.isupdateClicked(model);

                    /*Intent intent =new Intent(context, JK_ProductionTicket_Update_Actvity.class);
                    Bundle bun = new Bundle();
                    bun.putString("ptk_tk_id",ticket_list.get(i).getPtkd_PktId());
                    bun.putString("tk_no",ticket_list.get(i).getPtkd_TkNo());
                    bun.putString("tk_start_date",ticket_list.get(i).getTk_Actual_Start_Date());
                    bun.putString("tk_end_date",ticket_list.get(i).getTk_Actual_End_Date());
                    bun.putString("tk_sku",ticket_list.get(i).getPtkd_PSKU());
                    bun.putString("tk_plant",ticket_list.get(i).getPtkd_Plant());
                    bun.putString("tk_quantity",ticket_list.get(i).getPtkd_Qty());
                    bun.putString("ptyk_Active",ticket_list.get(i).getPtkd_Active());
                    intent.putExtras(bun);
                    context.startActivity(intent);*/
                }
            });
            holder.img_btn_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    newProductionTicketListener.isupdateClicked(model);

                    /*Intent intent =new Intent(context, JkViewProductionTicket.class);
                    Bundle bun = new Bundle();
                    bun.putString("ptk_tk_id",ticket_list.get(i).getPtkd_PktId());
                    bun.putString("ptyk_Active",ticket_list.get(i).getPtkd_Active());
                    bun.putString("tk_no",ticket_list.get(i).getPtkd_TkNo());
                    bun.putString("tk_start_date",ticket_list.get(i).getTk_Actual_Start_Date());
                    bun.putString("tk_end_date",ticket_list.get(i).getTk_Actual_End_Date());
                    bun.putString("tk_sku_8",ticket_list.get(i).getPtkd_PSKU());
                    bun.putString("tk_plant",ticket_list.get(i).getPtkd_Plant());
                    bun.putString("tk_quantity",ticket_list.get(i).getPtkd_Qty());
                    bun.putString("ptyk_Active",ticket_list.get(i).getPtkd_Active());
                    intent.putExtras(bun);
                    context.startActivity(intent);*/

                }
            });
        }

    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tk_no, tk_plant, tk_sku_8_d, tk_Release_date, tk_value_stream, tk_start_date, tk_end_date, pro_txt_qty, tk_prod_qty;
        ImageView img_icon_for_Release_ticket;
        RelativeLayout pro_details_layout;
        FrameLayout frame_list_card_item;
        RelativeLayout rl_production;
        ImageButton img_btn_update,img_release,img_btn_view,pro_img_plus;

        public MyViewHolder(View itemView) {
            super(itemView);
            tk_no = itemView.findViewById(R.id.pro_txt_ticket_no);
            tk_plant = itemView.findViewById(R.id.pro_txt_up_plant);
            tk_sku_8_d = itemView.findViewById(R.id.txt_sku_8_digit);
            tk_Release_date = itemView.findViewById(R.id.pro_txt_release_date);
            tk_value_stream = itemView.findViewById(R.id.pro_txt_value_stream);
            tk_start_date = itemView.findViewById(R.id.txt_up_start_date);
            tk_end_date = itemView.findViewById(R.id.pro_txt_end_date);
            img_btn_update = (ImageButton) itemView.findViewById(R.id.pro_img_update);
            img_release = (ImageButton) itemView.findViewById(R.id.pro_img_release);
            img_btn_view = (ImageButton) itemView.findViewById(R.id.pro_img_view);
            pro_img_plus = (ImageButton) itemView.findViewById(R.id.pro_img_plus);
            pro_details_layout = itemView.findViewById(R.id.pro_details_list);
            pro_txt_qty = (TextView) itemView.findViewById(R.id.pro_txt_qty);
            //tk_prod_qty=(TextView)itemView.findViewById(R.id.pro_txt_produce_qty);
            img_icon_for_Release_ticket = (ImageView) itemView.findViewById(R.id.img_icon_for_Release_ticket);
            frame_list_card_item = (FrameLayout) itemView.findViewById(R.id.frame_list_card_item);
            rl_production = (RelativeLayout) itemView.findViewById(R.id.rl_production);

        }
    }
}
