package com.brainlines.weightloggernewversion;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.brainlines.weightloggernewversion.Production.activity.AddNewBatchActivity;

public class CancelReadingsActivity extends AppCompatActivity {
    Button btnDifferentTag;
    ImageView imgCancel,imgConsider;
    LinearLayout linearLayout1;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cancel_readings_layout);
        linearLayout1=(LinearLayout)findViewById(R.id.LinearLayout1);


        btnDifferentTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(CancelReadingsActivity.this, AddNewBatchActivity.class);
                startActivity(intent);
            }
        });
        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
    }


}
