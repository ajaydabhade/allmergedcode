package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class TagInfoModel implements Parcelable {
    private String dataLoggerIP;
    private String dataLoggerName;
    private String macaddress;
    private String inspector;
    Tag TagObject;

    public TagInfoModel(String dataLoggerIP, String dataLoggerName, String macaddress, String inspector, Tag tagObject) {
        this.dataLoggerIP = dataLoggerIP;
        this.dataLoggerName = dataLoggerName;
        this.macaddress = macaddress;
        this.inspector = inspector;
        TagObject = tagObject;
    }

    protected TagInfoModel(Parcel in) {
        dataLoggerIP = in.readString();
        dataLoggerName = in.readString();
        macaddress = in.readString();
        inspector = in.readString();
        TagObject = in.readParcelable(Tag.class.getClassLoader());
    }

    public static final Creator<TagInfoModel> CREATOR = new Creator<TagInfoModel>() {
        @Override
        public TagInfoModel createFromParcel(Parcel in) {
            return new TagInfoModel(in);
        }

        @Override
        public TagInfoModel[] newArray(int size) {
            return new TagInfoModel[size];
        }
    };

    public String getDataLoggerIP() {
        return dataLoggerIP;
    }

    public void setDataLoggerIP(String dataLoggerIP) {
        this.dataLoggerIP = dataLoggerIP;
    }

    public String getDataLoggerName() {
        return dataLoggerName;
    }

    public void setDataLoggerName(String dataLoggerName) {
        this.dataLoggerName = dataLoggerName;
    }

    public String getMacaddress() {
        return macaddress;
    }

    public void setMacaddress(String macaddress) {
        this.macaddress = macaddress;
    }

    public String getInspector() {
        return inspector;
    }

    public void setInspector(String inspector) {
        this.inspector = inspector;
    }

    public Tag getTagObject() {
        return TagObject;
    }

    public void setTagObject(Tag tagObject) {
        TagObject = tagObject;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dataLoggerIP);
        dest.writeString(dataLoggerName);
        dest.writeString(macaddress);
        dest.writeString(inspector);
        dest.writeParcelable((Parcelable) TagObject, flags);
    }
}
