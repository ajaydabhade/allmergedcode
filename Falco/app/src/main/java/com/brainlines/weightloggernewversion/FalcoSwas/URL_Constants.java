package com.brainlines.weightloggernewversion.FalcoSwas;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class URL_Constants {
    private static Context context;
    Context context1;

    public static final String TAG = "URL_Call";
    public static final String ERROR_MSG_UNKNOWN_HOST_EXCEPTION = "UnknownHostException";
    public static final String ERROR_MSG_IO_EXCEPTION = "IOException";
    public static final String ERROR_MSG_CLIENT_PROTOCOL_EXCEPTION = "ClientProtocolException";
    public static final String ERROR_MSG_CONNECTION_TIMEOUT_EXCEPTION = "ConnectTimeoutException";
    public static final String ERROR_MSG_EXCEPTION = "Exception";

    public static final String LoginApi ="http://fgmobileapi.decintell.co.in/SWAS_Stg1/token?";

//    public static final String Token="http://apps.brainlines.net/DecintellMembershipWebAPI/Token";
    public static final String Token="https://fgapi.decintell.co.in/token";
//    public static final String encrypteed="http://apps.brainlines.net/DecintellMembershipWebAPI/api/providers/ClientMembershipEncryption?Password=";
    public static final String encrypted="https://fgapi.decintell.co.in/api/providers/ClientMembershipEncryption?Password=";

    public static String makeHttpPostRequest(String url) {
        String getResponse = "";
        try {
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, 60000);
            HttpConnectionParams.setSoTimeout(httpParams, 60000);

            HttpClient client = new DefaultHttpClient(httpParams);
            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader(HTTP.CONTENT_TYPE, "applicatidion/json");
            httpPost.setEntity(new StringEntity(url, "UTF-8"));

            //httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
            httpPost.setParams(httpParams);
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();

            if (statusLine.getStatusCode() == HttpStatus.SC_OK || statusLine.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
                HttpEntity entity = response.getEntity();
                InputStream inputStream = entity.getContent();
                getResponse = convertStreamToString(inputStream);
            }
            else {
                response.getEntity().getContent().close();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return getResponse;
    }
    public static String httppost(String url)
    {
        String getResponse = "";
        try {
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, 60000);
            HttpConnectionParams.setSoTimeout(httpParams, 60000);

            HttpClient client = new DefaultHttpClient(httpParams);
            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setEntity(new StringEntity(url, "UTF-8"));

            httpPost.setParams(httpParams);
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();

            if (statusLine.getStatusCode() == HttpStatus.SC_OK || statusLine.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
                HttpEntity entity = response.getEntity();
                InputStream inputStream = entity.getContent();
                getResponse = convertStreamToString(inputStream);
            }
            else {
                response.getEntity().getContent().close();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return getResponse;
    }

    public static String makeHttpPutRequest(String url) {
        String getResponse = "";

        try {
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, 60000);
            HttpConnectionParams.setSoTimeout(httpParams, 60000);

            HttpClient client = new DefaultHttpClient(httpParams);
            HttpPut httpput = new HttpPut(url);
            httpput.setHeader(HTTP.CONTENT_TYPE, "application/json");
            httpput.setEntity(new StringEntity(url, "UTF-8"));

            //httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
            httpput.setParams(httpParams);

            HttpResponse response = client.execute(httpput);
            StatusLine statusLine = response.getStatusLine();

            if (statusLine.getStatusCode() == HttpStatus.SC_OK || statusLine.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
                HttpEntity entity = response.getEntity();
                InputStream inputStream = entity.getContent();

                getResponse = convertStreamToString(inputStream);

            }

            else {
                response.getEntity().getContent().close();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return getResponse;
    }
    public static String makeHttpGetRequest(String url) {
        String getResponse = "";
        if(url != null && url.contains(" ")){
            url = url.replace(" ", "%20");
        }
        try {
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, 60000);
            HttpConnectionParams.setSoTimeout(httpParams, 60000);

            HttpClient client = new DefaultHttpClient(httpParams);
            HttpGet httpGet = new HttpGet(url);
            httpGet.setParams(httpParams);

            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() == HttpStatus.SC_OK || statusLine.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
                HttpEntity entity = response.getEntity();
                InputStream inputStream = entity.getContent();

                getResponse = convertStreamToString(inputStream);
            }

            else {
                response.getEntity().getContent().close();
            }

        }catch (UnknownHostException e) {
            Log.i(JK_URL_Constants.TAG, "Error Message in UnknownHostException :-" + e.getMessage());

        }catch (ConnectTimeoutException e) {
            Log.i(JK_URL_Constants.TAG, "Error Message in ConnectTimeoutException :-" + e.getMessage());

        }catch (ClientProtocolException e) {
            Log.i(JK_URL_Constants.TAG, "Error Message in ClientProtocolException :-" + e.getMessage());

        }catch (IOException e) {
            Log.i(JK_URL_Constants.TAG, "Error Message in IOException :-" + e.getMessage());

        }
        catch (Exception e) {
            Log.i(JK_URL_Constants.TAG, "Error Message in Exception :-" + e.getMessage());
        }
        return getResponse;
    }


    public static String convertStreamToString(InputStream inputStream) {
        StringBuilder stringBuilder;

        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        stringBuilder = new StringBuilder();

        String line = null;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return stringBuilder.toString();
    }

    public static String DateConversion(String date)
    {
        String date1="";
        String strdate=date.replaceAll("T"," ");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        try {
            Date dat = format.parse(strdate);
            SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.US);
            String dateTime = format1.format(dat);
            String str[]=dateTime.split(" ");
            date1=str[0];
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date1;
    }


    public static String substraction(String s1, String s)
    {
        int number = 0;
        int number1 = 0;
        int calculation= 0;
        String cal = "NA";
        try {
            number = Integer.parseInt(s);
            number1 = Integer.parseInt(s1);
            calculation=number1 - number;
            cal=String.valueOf(calculation);

        } catch(NumberFormatException e) {
            System.out.println("parse value is not valid : " + e);
        }
        return cal;
    }


    public static void checkNetworkConnection(){
        AlertDialog.Builder builder =new AlertDialog.Builder(context);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static boolean isNetworkConnectionAvailable(){
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
        if(isConnected) {
            Log.d("Network", "Connected");
            return true;
        }
        else{
            checkNetworkConnection();
            Log.d("Network","Not Connected");
            return false;
        }
    }


    public static String packsubstraction(String s1, String s)
    {
        int number = 0;
        int number1 = 0;
        int calculation= 0;
        String cal = "NA";
        try {
            number = Integer.parseInt(s);
            number1 = Integer.parseInt(s1);
            calculation=number1 - number;
            cal=String.valueOf(calculation);

        } catch(NumberFormatException e) {
            System.out.println("parse value is not valid : " + e);
        }
        return cal;
    }
}
