package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

public class UserModel implements Parcelable {

    int UserID;
    int OEMID;
    int RoleID;
    String RoleName;
    int ModuleID;
    String ModuleName;

    public UserModel(int userID, int OEMID, int roleID, String roleName, int moduleID, String moduleName) {
        UserID = userID;
        this.OEMID = OEMID;
        RoleID = roleID;
        RoleName = roleName;
        ModuleID = moduleID;
        ModuleName = moduleName;
    }

    protected UserModel(Parcel in) {
        UserID = in.readInt();
        OEMID = in.readInt();
        RoleID = in.readInt();
        RoleName = in.readString();
        ModuleID = in.readInt();
        ModuleName = in.readString();
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    public int getOEMID() {
        return OEMID;
    }

    public void setOEMID(int OEMID) {
        this.OEMID = OEMID;
    }

    public int getRoleID() {
        return RoleID;
    }

    public void setRoleID(int roleID) {
        RoleID = roleID;
    }

    public String getRoleName() {
        return RoleName;
    }

    public void setRoleName(String roleName) {
        RoleName = roleName;
    }

    public int getModuleID() {
        return ModuleID;
    }

    public void setModuleID(int moduleID) {
        ModuleID = moduleID;
    }

    public String getModuleName() {
        return ModuleName;
    }

    public void setModuleName(String moduleName) {
        ModuleName = moduleName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(UserID);
        dest.writeInt(OEMID);
        dest.writeInt(RoleID);
        dest.writeString(RoleName);
        dest.writeInt(ModuleID);
        dest.writeString(ModuleName);
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "UserID=" + UserID +
                ", OEMID=" + OEMID +
                ", RoleID=" + RoleID +
                ", RoleName='" + RoleName + '\'' +
                ", ModuleID=" + ModuleID +
                ", ModuleName='" + ModuleName + '\'' +
                '}';
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        UserModel that = (UserModel) obj;

        if (Double.compare(that.ModuleID, ModuleID) != 0) return false;
        if (Double.compare(that.ModuleID, ModuleID) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(ModuleID);
        result = (int) (temp ^ (temp >>> 32));
        return result;
    }
}
