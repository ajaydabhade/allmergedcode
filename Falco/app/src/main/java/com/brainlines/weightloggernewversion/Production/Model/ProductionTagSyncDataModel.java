package com.brainlines.weightloggernewversion.Production.Model;

public class ProductionTagSyncDataModel {
    String ProductionTagName,OEMID,MacID,BatchID,ProductName,GmPerPouch,Unit,TotalQuantity,TargetAccurancyType,TargetAccurancyValue,IsNegAllowed,ModifiedDate;
    String TagShift,TagNumberOfPouches,SessionPositiveTolerance,SessionNegativeTolerance,MasterTolerancePositive,MasterToleranceNegative;
    String UCLgm,LCLgm,TotalQuantityUOM,Operator;

    public ProductionTagSyncDataModel(String productionTagName, String OEMID, String macID, String batchID, String productName, String gmPerPouch, String unit, String totalQuantity, String targetAccurancyType, String targetAccurancyValue, String isNegAllowed, String tagModifiedDate, String tagShift, String tagNumberOfPouches, String sessionPositiveTolerance, String sessionNegativeTolerance, String masterTolerancePositive, String masterToleranceNegetive, String UCLgm, String LCLgm, String totalQuantityUOM, String operator) {
        this.ProductionTagName = productionTagName;
        this.OEMID = OEMID;
        this.MacID = macID;
        this.BatchID = batchID;
        this.ProductName = productName;
        this.GmPerPouch = gmPerPouch;
        this.Unit = unit;
        this.TotalQuantity = totalQuantity;
        this.TargetAccurancyType = targetAccurancyType;
        this.TargetAccurancyValue = targetAccurancyValue;
        this.IsNegAllowed = isNegAllowed;
        this.ModifiedDate = tagModifiedDate;
        this.TagShift = tagShift;
        this.TagNumberOfPouches = tagNumberOfPouches;
        this.SessionPositiveTolerance = sessionPositiveTolerance;
        this.SessionNegativeTolerance = sessionNegativeTolerance;
        this.MasterTolerancePositive = masterTolerancePositive;
        this.MasterToleranceNegative = masterToleranceNegetive;
        this.UCLgm = UCLgm;
        this.LCLgm = LCLgm;
        this.TotalQuantityUOM = totalQuantityUOM;
        this.Operator = operator;
    }

    public String getProductionTagName() {
        return ProductionTagName;
    }

    public void setProductionTagName(String productionTagName) {
        ProductionTagName = productionTagName;
    }

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getMacID() {
        return MacID;
    }

    public void setMacID(String macID) {
        MacID = macID;
    }

    public String getBatchID() {
        return BatchID;
    }

    public void setBatchID(String batchID) {
        BatchID = batchID;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getGmPerPouch() {
        return GmPerPouch;
    }

    public void setGmPerPouch(String gmPerPouch) {
        GmPerPouch = gmPerPouch;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String unit) {
        Unit = unit;
    }

    public String getTotalQuantity() {
        return TotalQuantity;
    }

    public void setTotalQuantity(String totalQuantity) {
        TotalQuantity = totalQuantity;
    }

    public String getTargetAccurancyType() {
        return TargetAccurancyType;
    }

    public void setTargetAccurancyType(String targetAccurancyType) {
        TargetAccurancyType = targetAccurancyType;
    }

    public String getTargetAccurancyValue() {
        return TargetAccurancyValue;
    }

    public void setTargetAccurancyValue(String targetAccurancyValue) {
        TargetAccurancyValue = targetAccurancyValue;
    }

    public String getIsNegAllowed() {
        return IsNegAllowed;
    }

    public void setIsNegAllowed(String isNegAllowed) {
        IsNegAllowed = isNegAllowed;
    }

    public String getTagModifiedDate() {
        return ModifiedDate;
    }

    public void setTagModifiedDate(String tagModifiedDate) {
        ModifiedDate = tagModifiedDate;
    }

    public String getTagShift() {
        return TagShift;
    }

    public void setTagShift(String tagShift) {
        TagShift = tagShift;
    }

    public String getTagNumberOfPouches() {
        return TagNumberOfPouches;
    }

    public void setTagNumberOfPouches(String tagNumberOfPouches) {
        TagNumberOfPouches = tagNumberOfPouches;
    }

    public String getSessionPositiveTolerance() {
        return SessionPositiveTolerance;
    }

    public void setSessionPositiveTolerance(String sessionPositiveTolerance) {
        SessionPositiveTolerance = sessionPositiveTolerance;
    }

    public String getSessionNegativeTolerance() {
        return SessionNegativeTolerance;
    }

    public void setSessionNegativeTolerance(String sessionNegativeTolerance) {
        SessionNegativeTolerance = sessionNegativeTolerance;
    }

    public String getMasterTolerancePositive() {
        return MasterTolerancePositive;
    }

    public void setMasterTolerancePositive(String masterTolerancePositive) {
        MasterTolerancePositive = masterTolerancePositive;
    }

    public String getMasterToleranceNegetive() {
        return MasterToleranceNegative;
    }

    public void setMasterToleranceNegetive(String masterToleranceNegetive) {
        MasterToleranceNegative = masterToleranceNegetive;
    }

    public String getUCLgm() {
        return UCLgm;
    }

    public void setUCLgm(String UCLgm) {
        this.UCLgm = UCLgm;
    }

    public String getLCLgm() {
        return LCLgm;
    }

    public void setLCLgm(String LCLgm) {
        this.LCLgm = LCLgm;
    }

    public String getTotalQuantityUOM() {
        return TotalQuantityUOM;
    }

    public void setTotalQuantityUOM(String totalQuantityUOM) {
        TotalQuantityUOM = totalQuantityUOM;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }
}
