package com.brainlines.weightloggernewversion.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.adapter.recalltestadapter.Record_bag_length_list_Adapter;

import java.util.ArrayList;

public class Weight_Reading_Adapter extends RecyclerView.Adapter<Weight_Reading_Adapter.MyViewHolder> {

    public String TAG = "WorkDetailsListener";
    Context context;
    ArrayList<String> weight_reading = new ArrayList<>();


    public Weight_Reading_Adapter(Context context, ArrayList<String> weight_reading) {
        this.context = context;
        this.weight_reading = weight_reading;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_weight_reading, parent, false);
        return new Weight_Reading_Adapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        String reading = weight_reading.get(position);
        holder.txt_degree.setText(reading);

    }

    @Override
    public int getItemCount() {
        return weight_reading.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_degree;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_degree = (itemView).findViewById(R.id.txt_weight_reading);


        }

    }

}
