package com.brainlines.weightloggernewversion.activity.history;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class History_Paper_Shift_Reading_Adapter extends RecyclerView.Adapter<History_Paper_Shift_Reading_Adapter.MyViewHolder> {

    Context context;
    ArrayList<String> papershiftReading = new ArrayList<>();

    public History_Paper_Shift_Reading_Adapter(Context context, ArrayList<String> papershiftReading) {
        this.context = context;
        this.papershiftReading = papershiftReading;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_paper_shift_history_reading,parent,false);
        return new History_Paper_Shift_Reading_Adapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        String readingValue = papershiftReading.get(position);
        if (!readingValue.equals("")){
            holder.txt_weight_reading.setText(readingValue);
        }
    }

    @Override
    public int getItemCount() {
        return papershiftReading.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_weight_reading;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_weight_reading = (itemView).findViewById(R.id.txt_paper_shift_Reading);
        }
    }
}
