package com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.InProcessCall;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Feedback_Activity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Home_Actvity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter.DocumentsAdapter;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasListener.UploadImageDisplayListener;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.DocumentModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasIdDetailsModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.android.material.navigation.NavigationView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants.convertStreamToString;

public class Swas_Layout_Upload_Report_Activity extends AppCompatActivity implements View.OnClickListener, UploadImageDisplayListener, NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "Swas_Layout_Upload_Repo";
    ImageButton btnFeedback, btnSubmit, btnChooseFile;
    TextView txt_reports_service_call_id, document_name;
    RecyclerView rv_upload_report;
    String status_id, service_call_id, type_of_service_status_id, customer_id, oem_id;
    private static final int MY_CAMERA_REQUEST_CODE = 100;
    private static final int MY_PERMISSION = 21;
    Bitmap photo;
    String picturePath;
    byte[] imageBytes;
    File file;
    Uri fileUri;
    String imageName;
    DocumentsAdapter documentsAdapter;
    ImageView img_user_info,nav_view_img,img_back;
    String user_email,user_token,user_role;
    DrawerLayout drawer;
    TextView profile_user,profile_role;
    String created_by="",service_eng_id;
    ArrayList<DocumentModel> model = new ArrayList<>();
    private Bitmap bitmap;
    private DateFormat dateFormat = new SimpleDateFormat("E, MMMM dd, yyyy HH:mm:ss.SSS aa");
    private String currentDate;
    private Bitmap scaledBitmap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_swas_inprocess_upload_doc_navigation);
        currentDate = dateFormat.format(new Date());
        initUi();
        setData();
        img_back=(ImageView)findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Swas_Layout_Upload_Report_Activity.this, Swas_Deatails_of_Inprocess_Call_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_service_status_id);
                bundle.putString("customer_id",customer_id);
                i.putExtras(bundle);
                startActivity(i);
                finish();
            }
        });
        if (user_email.equals("yogesh@brainlines.in"))
        {
            created_by = "Yogesh Jadhav";
//            service_eng_id = "1";
        }
        else
        {
            created_by = "Deepak Kulkarni";
//            service_eng_id = "2";
        }
        img_user_info=(ImageView)findViewById(R.id.img_userinfo);
        img_user_info.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img=(ImageView)findViewById(R.id.nav_view_img);

        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_planning).setVisible(false);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            status_id = bundle.getString("status_id");
            service_call_id = bundle.getString("service_call_id");
            type_of_service_status_id = bundle.getString("Typeofservicecall_id");
            customer_id = bundle.getString("customer_id");
        }
        setData();

        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
//        if(isConnected) {
//            Log.d("Network", "Connected");
//            Id_Details();
//            GetDocuments();
//
//        }
//        else{
//            checkNetworkConnection();
//            Log.d("Network","Not Connected");
//            getDocsFromLocalDB();
//        }
        getDocsFromLocalDB();

        btnFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent( Swas_Layout_Upload_Report_Activity.this, Swas_Feedback_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("status_id",status_id);
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("Typeofservicecall_id",type_of_service_status_id);
                bundle.putString("customer_id",customer_id);
                i.putExtras(bundle);
                startActivity(i);
            }
        });
    }

    public void initUi() {
        btnFeedback = findViewById(R.id.btn_reports_feedback);
        btnChooseFile = findViewById(R.id.btn_reports_choose_file);
        btnSubmit = findViewById(R.id.btn_reports_submit);
        rv_upload_report = (RecyclerView) findViewById(R.id.rv_upload_report);
        txt_reports_service_call_id = (TextView) findViewById(R.id.txt_reports_service_call_id);
        document_name = (TextView) findViewById(R.id.document_name);
        btnChooseFile.setOnClickListener(this);
        btnFeedback.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
    }

    private void Id_Details() {
        API api = Retroconfig.swasretrofit().create(API.class);

        Call<ResponseBody> call = api.getalldetailsofsingleservicecall(service_call_id, status_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {

                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            ArrayList<SwasIdDetailsModel> model = new ArrayList<>();
                            if (array.length() != 0) {

                                for (int i = 0; i < array.length(); i++) {
                                    SwasIdDetailsModel swasIdDetailsModel = new SwasIdDetailsModel();
                                    JSONObject object1 = array.getJSONObject(i);

                                    swasIdDetailsModel.setService_Id(object1.getString("ServiceCallID"));
                                    swasIdDetailsModel.setLOCATIONNAME(object1.getString("LOCATIONNAME"));
                                    swasIdDetailsModel.setCreatedBy(object1.getString("CreatedBy"));
                                    swasIdDetailsModel.setCreatedDate(object1.getString("CreatedDate"));
                                    swasIdDetailsModel.setUpdatedBy(object1.getString("UpdatedBy"));
                                    swasIdDetailsModel.setUpdatedDate(object1.getString("UpdatedDate"));
                                    swasIdDetailsModel.setTypeOfServiceCallText(object1.getString("TypeOfServiceCallText"));
                                    swasIdDetailsModel.setNAME(object1.getString("NAME"));
                                    swasIdDetailsModel.setBriefComplaint(object1.getString("BriefComplaint"));
                                    swasIdDetailsModel.setTypeOfFaultText(object1.getString("TypeOfFaultText"));
                                    swasIdDetailsModel.setStatusText(object1.getString("StatusText"));
                                    swasIdDetailsModel.setCustomerID(object1.getString("CustomerID"));
                                    swasIdDetailsModel.setITEMID(object1.getString("ITEMID"));
                                    swasIdDetailsModel.setCURRENCY(object1.getString("CURRENCY"));
                                    swasIdDetailsModel.setTypeOfServiceCallID(object1.getString("TypeOfServiceCallID"));
                                    swasIdDetailsModel.setServiceEngineerName(object1.getString("ServiceEngineerName"));
                                    swasIdDetailsModel.setClosedDateTime(object1.getString("ClosedDateTime"));
                                    swasIdDetailsModel.setClosedBy(object1.getString("ClosedBy"));
                                    swasIdDetailsModel.setService_call_status_Id(object1.getString("ServiceCallStatusID"));
                                    swasIdDetailsModel.setComments(object1.getString("Comments"));
                                    swasIdDetailsModel.setServiceEngineerID(object1.getString("ServiceEngineerID"));
                                    swasIdDetailsModel.setNumberOfVisits(object1.getString("NumberOfVisits"));
                               /* txt_type_of_fault_text.setText(object1.getString("TypeOfFaultText"));
                                type_of_Service_call_id = object1.getString("TypeOfServiceCallID");

                                txt_type_in_service_call.setText(object1.getString("TypeOfServiceCallText"));
                                //txt_in_complaint.setText(object1.getString("BriefComplaint"));
                                txt_in_product.setText(object1.getString("NAME"));
                                service_call_id=object1.getString("ServiceCallID");
                                //created_by = object1.getString("CreatedBy");
                                complaint = object1.getString("BriefComplaint");
                                customer_id = object1.getString("CustomerID");
                                txt_dtls_service_call_id.setText(service_call_id);*/
                                    txt_reports_service_call_id.setText(object1.getString("ServiceCallID"));

                                }

                            /*GetChecklist();
                            GetPartOrderBY();
                            FaultyCharacteristicsList();*/


                            } else if (array.length() == 0) {
                            }
                        }
                    }


                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == btnChooseFile.getId()) {
            selectImage(Swas_Layout_Upload_Report_Activity.this);
        }
        else if (view.getId() == btnSubmit.getId()) {
            ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null &&
                    activeNetwork.isConnected();
            if(isConnected) {
                new Insert_Document_Call().execute();
            }else {
                insertDocumentInSqliteDB();
            }
        }
    }

    private void insertDocumentInSqliteDB() {
        int count = 1;
        String base64 = getEncoded64ImageStringFromBitmap(scaledBitmap);
        byte[] data = Base64.decode(base64, Base64.DEFAULT);
        Log.d(TAG, "insertDocumentInSqliteDB: ");
        //String Document_path = document_name.getText().toString();

        String a = "Document_";
        int position= model.size();
        String Document_path = a.concat(String.valueOf(position+1));
        DataBaseHelper helper=new DataBaseHelper(Swas_Layout_Upload_Report_Activity.this);
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues cv=new ContentValues();
//        cv.put(Constants.DOCUMENTS_ID,"");
        cv.put(Constants.DOCUMENTS_REPORT_DOCUMENTS_ID,String.valueOf(count));
        cv.put(Constants.DOCUMENTS_REPORT_OEM_ID,oem_id);
        cv.put(Constants.DOCUMENTS_REPORT_CUSTOMER_ID,customer_id);
        cv.put(Constants.DOCUMENTS_REPORT_SERVICE_CALL_ID,service_call_id);
        cv.put(Constants.DOCUMENTS_REPORT_DOCUMENT_PATH,data);
//        cv.put(Constants.DOCUMENTS_REPORT_DOCUMENT_PATH,base64);
        cv.put(Constants.DOCUMENTS_REPORT_DOCUMENT_NAME,Document_path);
        cv.put(Constants.DOCUMENTS_REPORT_IS_ACTIVE,"1");
        cv.put(Constants.DOCUMENTS_REPORT_CREATED_BY,user_email);
        cv.put(Constants.DOCUMENTS_REPORT_CREATED_DATE,currentDate);
        cv.put(Constants.DOCUMENTS_REPORT_UPDATED_BY,user_email);
        cv.put(Constants.DOCUMENTS_REPORT_UPDATED_DATE,currentDate);
        cv.put(Constants.DOCUMENTS_REPORT_IS_SYNC,0);
        long d=db.insert(Constants.TABLE_DOCUMENTS,null,cv);
        Log.d("user check", String.valueOf(d));
        if (d==-1){
            Log.d(TAG, "onResponse: "+"Data not Inserted");
        }else {
                Log.d(TAG, "onResponse: "+"Data Inserted");
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK && data != null) {
                        photo = (Bitmap) data.getExtras().get("data");
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        photo.compress(Bitmap.CompressFormat.PNG, 100, baos);
                        imageBytes = baos.toByteArray();
                        getImageUri(Swas_Layout_Upload_Report_Activity.this, photo);
                    }

                    break;
                case 1:
                    if (resultCode == RESULT_OK && data != null) {

//                        Uri selectedImage = data.getData();
//                        picturePath = getPath(selectedImage);
//                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
//                        Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
//                        cursor.moveToFirst();
//                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//                        String picturePath = cursor.getString(columnIndex);
//                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                        photo= BitmapFactory.decodeFile(picturePath);
//
//                        File f = new File(picturePath);
//                        imageName = f.getName();
//                        document_name.setText(imageName);
//                        FileInputStream fis;
//                        try {
//                            fis = new FileInputStream(new File(selectedImage.getPath()));
//                            bitmap = BitmapFactory.decodeStream(fis);
//                            byte[] buf = new byte[1024];
//                            int n;
//                            while (-1 != (n = fis.read(buf)))
//                                baos.write(buf, 0, n);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                        //img_btn_viewimage.setVisibility(View.VISIBLE);
//                        imageBytes = baos.toByteArray();
//                        cursor.close();
                        Uri selectedImage = data == null ? null : data.getData();
                        picturePath = getPath(selectedImage);
                        File f = new File(picturePath);
                        imageName = f.getName();
                        document_name.setText(imageName);
                        Log.d("ImageURI", selectedImage.getLastPathSegment());
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 8;
                        try {
                            InputStream input = getContentResolver().openInputStream(selectedImage);
                            bitmap = BitmapFactory.decodeStream(input);
                            scaledBitmap = Bitmap.createScaledBitmap(bitmap,250,250,false);
                            Log.d("ImageURI", input.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                    break;
            }
        }
    }

    private void selectImage(final Context context) {
        final CharSequence[] options = {"camera", "gallary", "cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("camera")) {
                    if (checkSelfPermission(Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                MY_CAMERA_REQUEST_CODE);
                    } else {
                        Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(takePicture, 0);
                    }
                } else if (options[item].equals("gallary")) {
                    if (ContextCompat.checkSelfPermission(Swas_Layout_Upload_Report_Activity.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(Swas_Layout_Upload_Report_Activity.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(Swas_Layout_Upload_Report_Activity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSION);

                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_PICK);
                        /*file = new File(context.getExternalCacheDir(),
                                String.valueOf(System.currentTimeMillis()) + ".jpg");
                        fileUri = Uri.fromFile(file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);*/
                        startActivityForResult(intent, 1);
                    }

                } else if (options[item].equals("cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private String getPath(Uri uri) {

        String result;
        Cursor cursor = Swas_Layout_Upload_Report_Activity.this.getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
            result = uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            //document_name.setText(result);
            photo= BitmapFactory.decodeFile(result);
            cursor.close();
        }
        return result;

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(),
                inImage, "Title", null);
        //document_name.setText(path);
        File f = new File(path);
        document_name.setText(f.getName());
        return Uri.parse(path);
    }

    private void GetDocuments() {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getreportdocumentdetails(service_call_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {

                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            document_name.setText("");
                            model.clear();
                            if (array.length() != 0) {
                                for (int i = 0;i<array.length();i++)
                                {
                                    DocumentModel model1 = new DocumentModel();
                                    JSONObject obj = array.getJSONObject(i);
                                    model1.setReportDocumentID(obj.getString("ReportDocumentID"));
                                    model1.setOEMID(obj.getString("OEMID"));
                                    model1.setCustomerID(obj.getString("CustomerID"));
                                    model1.setServiceCallID(obj.getString("ServiceCallID"));
                                    model1.setDocumentPath(obj.getString("DocumentPath"));
                                    model1.setDocumentName(obj.getString("DocumentName"));
                                    model1.setIsActive(obj.getString("IsActive"));
                                    model1.setCreatedBy(obj.getString("CreatedBy"));
                                    model1.setCreatedDate(obj.getString("CreatedDate"));
                                    model.add(model1);
                                }
                                documentsAdapter = new DocumentsAdapter(Swas_Layout_Upload_Report_Activity.this,model,Swas_Layout_Upload_Report_Activity.this);
                                rv_upload_report.setLayoutManager(new LinearLayoutManager(Swas_Layout_Upload_Report_Activity.this, LinearLayoutManager.VERTICAL, false));
                                rv_upload_report.setAdapter(documentsAdapter);

                            }
                            else if (array.length() == 0) {
                            }
                        }
                    }

                }
                catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void getDocsFromLocalDB() {
        Cursor cursor = new DataBaseHelper(this).getDocumnetFromLocalDb(service_call_id);
        while (cursor.moveToNext()){
            String rportDocID = cursor.getString(1);
            String oemID = cursor.getString(2);
            String CustomerID = cursor.getString(3);
            String serviceCallID = cursor.getString(4);
            String docPath = cursor.getString(5);
            String docName = cursor.getString(6);
            String isActive = cursor.getString(7);
            String createdBy = cursor.getString(8);
            String createdDate = cursor.getString(9);
            String updatedBy = cursor.getString(10);
            String updatedDate = cursor.getString(11);
            DocumentModel model1 = new DocumentModel();
            model1.setReportDocumentID(rportDocID);
            model1.setOEMID(oemID);
            model1.setCustomerID(CustomerID);
            model1.setServiceCallID(serviceCallID);
            model1.setDocumentPath(docPath);
            model1.setDocumentName(docName);
            model1.setIsActive(isActive);
            model1.setCreatedBy(createdBy);
            model1.setCreatedDate(createdDate);
            model.add(model1);
        }
        documentsAdapter = new DocumentsAdapter(Swas_Layout_Upload_Report_Activity.this,model,Swas_Layout_Upload_Report_Activity.this);
        rv_upload_report.setLayoutManager(new LinearLayoutManager(Swas_Layout_Upload_Report_Activity.this, LinearLayoutManager.VERTICAL, false));
        rv_upload_report.setAdapter(documentsAdapter);
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }

    @Override
    public void displayImage(DocumentModel model) {

        if (model!=null)
        {
            String img_string = model.getDocumentPath();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] imageBytes = baos.toByteArray();
            imageBytes = Base64.decode(img_string, Base64.DEFAULT);
            Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
            img_display(decodedImage);
        }
    }

    public class Insert_Document_Call extends AsyncTask<String,String,String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Swas_Layout_Upload_Report_Activity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String getResponse = "";
            String url= Retroconfig.BASEURL_SWAS + "savereportsdocumentdetails";
            String base64 = getEncoded64ImageStringFromBitmap(bitmap);

            //String Document_path = document_name.getText().toString();

            String a = "Document_";
            int position= model.size();
            String Document_path = a.concat(String.valueOf(position+1));

            try {
                HttpParams httpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(httpParams, 60000);
                HttpConnectionParams.setSoTimeout(httpParams, 60000);
                HttpClient client = new DefaultHttpClient(httpParams);
                HttpPost httpPost = new HttpPost(url);

                httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");

                JSONArray array = new JSONArray();
                JSONObject object = new JSONObject();
                object.put("OEMID", oem_id);
                object.put("CustomerID", customer_id);
                object.put("ServiceCallID", service_call_id);
                object.put("DocumentPath",base64);
                object.put("DocumentName", Document_path);
                object.put("CreatedBy", created_by);
                array.put(object);

                httpPost.setEntity(new StringEntity(array.toString(), "UTF-8"));
                httpPost.setParams(httpParams);

                HttpResponse response = client.execute(httpPost);
                StatusLine statusLine = response.getStatusLine();

                if (statusLine.getStatusCode() == HttpStatus.SC_OK || statusLine.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();

                    getResponse = convertStreamToString(inputStream);
                } else if (statusLine.getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();

                    getResponse = convertStreamToString(inputStream);
                } else {
                    response.getEntity().getContent().close();
                }
            }
            catch (Exception e) {
                Log.i(URL_Constants.TAG, "Error Message in Exception :-" + e.getMessage());
                getResponse = URL_Constants.ERROR_MSG_EXCEPTION;

            }
            return getResponse;
        }

        @Override
        protected void onPostExecute(String getResponse) {
            super.onPostExecute(getResponse);
            progressDialog.dismiss();
            if (!getResponse.equals("Error"))
            {
                Toast.makeText(Swas_Layout_Upload_Report_Activity.this, "Document uploaded successfully", Toast.LENGTH_SHORT).show();
                GetDocuments();

            }
        }
    }

    public void img_display(Bitmap img)
    {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.item_img_upload_display, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        ImageView pop_img_profile = (ImageView)dialogView.findViewById(R.id.pop_img_profile);

        pop_img_profile.setImageBitmap(img);


        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(Swas_Layout_Upload_Report_Activity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);
        setData();
        // Set content width and height
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(Swas_Layout_Upload_Report_Activity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }

    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(Swas_Layout_Upload_Report_Activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();

                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    oem_id= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));
                    service_eng_id= cursor.getString(cursor.getColumnIndex(Constants.USER_LOGIN_ID));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));


                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout()
    {
        finish();
        Toast toast= Toast.makeText(Swas_Layout_Upload_Report_Activity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(Swas_Layout_Upload_Report_Activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();


        if (id==R.id.nav_swas)
        {
            Intent i=new Intent(Swas_Layout_Upload_Report_Activity.this, Swas_Home_Actvity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    public void checkNetworkConnection(){
        AlertDialog.Builder builder =new AlertDialog.Builder(Swas_Layout_Upload_Report_Activity.this);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i= new Intent(Swas_Layout_Upload_Report_Activity.this,Swas_Deatails_of_Inprocess_Call_Activity.class);
        Bundle bundle = new Bundle();
        bundle.putString("service_call_id",service_call_id);
        bundle.putString("status_id",status_id);
        bundle.putString("Typeofservicecall_id",type_of_service_status_id);
        bundle.putString("customer_id",customer_id);
        i.putExtras(bundle);
        startActivity(i);
        finish();
    }
}