package com.brainlines.weightloggernewversion.FalcoJK.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.PackingTicketModel;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.SpinnerAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.TicketListRvAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.UrlConstants.JK_URL_Constants;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class JKPackingTicketSelection extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    RecyclerView rv_packing_ticket_list;
    TicketListRvAdapter adapter;
    Spinner spin_ticket_type,spin_pack_release,spin_ticket;
    String ticket_type,ticket_release,ticket_active;
    ImageView img_back,img_user_info,nav_view_img;
    DrawerLayout drawer;
    String user_email,user_role,user_token;
    SwipeRefreshLayout swipeto_refresh;
    TextView profile_user,profile_role;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_packing_ticket_selection_navigation);
        setData();
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img = findViewById(R.id.nav_view_img);
        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });

        rv_packing_ticket_list = findViewById(R.id.rv_packing_ticket_list);

        swipeto_refresh = findViewById(R.id.swipeto_refresh);
        spin_pack_release = findViewById(R.id.spin_pack_release);
        spin_ticket = findViewById(R.id.spin_ticket);
        spin_ticket_type = findViewById(R.id.spin_pack_ticket);
        img_back = findViewById(R.id.img_back);
        img_user_info = findViewById(R.id.img_userinfo);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_swas).setVisible(false);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(JKPackingTicketSelection.this,FalcoJKHomeActivity.class);
                startActivity(i);
            }
        });

        img_user_info.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        //This call is used for displaying all tickets
        new GetAllTicketsCall().execute();

        final ArrayList<String> arrayrelese_type = new ArrayList<>();
        arrayrelese_type.add("Select Released");
        arrayrelese_type.add("ReleasedTicket");
        arrayrelese_type.add("UnreleasedTicket");
        SpinnerAdapter arrayAdapter = new SpinnerAdapter(this,arrayrelese_type);
        spin_pack_release.setAdapter(arrayAdapter);

        final ArrayList<String> arrayticket_type = new ArrayList<>();
        arrayticket_type.add("Select Ticket Type");
        arrayticket_type.add("PackingTicket");
        arrayticket_type.add("RepackingTicket");
        final SpinnerAdapter arrayAdapter1 = new SpinnerAdapter(this,arrayticket_type);
        spin_ticket_type.setAdapter(arrayAdapter1);

        final ArrayList<String> arrayactiveticket_type = new ArrayList<>();
        arrayactiveticket_type.add("Select Ticket");
        arrayactiveticket_type.add("ActiveTicket");
        arrayactiveticket_type.add("ClosedTicket");
        SpinnerAdapter arrayAdapter2 = new SpinnerAdapter(this,arrayactiveticket_type);
        spin_ticket.setAdapter(arrayAdapter2);

        spin_ticket_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i!=0)
                {
                    ticket_type=arrayticket_type.get(i).toString();
                    new GetTicketsCall().execute();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spin_pack_release.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i!=0)
                {
                    ticket_release=arrayrelese_type.get(i).toString();
                    new GetPackingReleasedTicketCall().execute();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        spin_ticket.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i!=0)
                {
                    ticket_active=arrayactiveticket_type.get(i).toString();
                    new GetActiveTicketsCall().execute();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        swipeto_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new GetAllTicketsCall().execute();
                swipeto_refresh.setRefreshing(false);
            }
        });

    }

    public class GetTicketsCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JKPackingTicketSelection.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... strings) {
            String response="NA";
            String url = null;
            if (ticket_type.equals("RepackingTicket"))
            {
                ticket_type="UnPackingTicket";
                url= JK_URL_Constants.MAIN_JK_URL+JK_URL_Constants.GET_TICKETS + ticket_type + "&plantuser=" + JK_URL_Constants.PLANT;
                response = URL_Constants.makeHttpGetRequest(url);
                Log.d(URL_Constants.TAG, "Whole data = " + response);
            }
            if (ticket_type.equals("Select Ticket Type"))
            {
                ticket_type="null";
                url= JK_URL_Constants.MAIN_JK_URL+JK_URL_Constants.GET_TICKETS + ticket_type + "&plantuser=" + JK_URL_Constants.PLANT;
                response = URL_Constants.makeHttpGetRequest(url);
                Log.d(URL_Constants.TAG, "Whole data = " + response);

            }
            if (ticket_type.equals("PackingTicket"))
            {
                url= JK_URL_Constants.MAIN_JK_URL+JK_URL_Constants.GET_TICKETS + ticket_type + "&plantuser=" + JK_URL_Constants.PLANT;
                response = URL_Constants.makeHttpGetRequest(url);
                Log.d(URL_Constants.TAG, "Whole data = " + response);

            }
            return response;


        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            {
                if (!response.equals("Error"))
                {
                    try
                    {
                        JSONObject jsonObject=new JSONObject(response);
                        JSONArray array=jsonObject.getJSONArray("data");
                        if (array.length()==0){
                            Toast toast = Toast.makeText(getApplicationContext(),
                                    "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                            rv_packing_ticket_list.setAdapter(null);
                        }
                        else {
                            ArrayList<PackingTicketModel> ticketmodel= new ArrayList<>();

                            for (int i=0;i<array.length();i++)
                            {
                                JSONObject obj=new JSONObject(array.get(i).toString());
                                PackingTicketModel model=new PackingTicketModel();

                                model.setID(obj.getString("ID"));
                                model.setTk_Id(obj.getString("tk_Id"));
                                model.setTk_TicketNo(obj.getString("tk_TicketNo"));
                                model.setTk_SONO(obj.getString("tk_SONO"));
                                model.setTk_UploadRefNumber(obj.getString("tk_UploadRefNumber"));
                                model.setTk_Plant(obj.getString("tk_Plant"));
                                model.setTk_SKU(obj.getString("tk_SKU"));
                                model.setTk_Prod_SKU(obj.getString("tk_Prod_SKU"));
                                model.setTk_Release_Date(obj.getString("tk_Release_Date"));
                                model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                                model.setEDD(obj.getString("EDD"));
                                model.setTk_End_Date(obj.getString("tk_End_Date"));
                                model.setTk_Quantity(obj.getString("tk_Quantity"));
                                model.setPln_Packing_Norms(obj.getString("pln_Packing_Norms"));
                                model.setTk_Unpaking_SKU(obj.getString("tk_Unpaking_SKU"));
                                model.setTk_Unpacking(obj.getString("tk_Unpacking"));
                                model.setMonthStart(obj.getString("MonthStart"));
                                model.setMonthEnd(obj.getString("MonthEnd"));
                                model.setTk_Active(obj.getString("tk_Active"));
                                model.setTk_Release(obj.getString("tk_Release"));
                                model.setTk_produce_qty(obj.getString("tk_totalQty"));
                                model.setTk_actual_end_Date(obj.getString("tk_Actual_End_Date"));
                                ticketmodel.add(model);
                            }
                            adapter=new TicketListRvAdapter(JKPackingTicketSelection.this,ticketmodel,rv_packing_ticket_list,adapter);
                            rv_packing_ticket_list.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL, false));
                            rv_packing_ticket_list.setAdapter(adapter);
                        }
                    }
                    catch (Exception e){e.printStackTrace();}
                }
            }
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(JKPackingTicketSelection.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);
        setData();

        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(JKPackingTicketSelection.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }

    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(JKPackingTicketSelection.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();

                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));


                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout()
    {
        finish();
        Toast toast= Toast.makeText(JKPackingTicketSelection.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(JKPackingTicketSelection.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);
    }

    public class GetPackingReleasedTicketCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JKPackingTicketSelection.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... strings) {
            String response="NA";

            String url = null;
            url= JK_URL_Constants.MAIN_JK_URL+JK_URL_Constants.GET_TICKETS + ticket_type + "&plantuser=" + JK_URL_Constants.PLANT;
            response = URL_Constants.makeHttpGetRequest(url);
            Log.d(URL_Constants.TAG, "Whole data = " + response);
            return response;

        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            {
                if (!response.equals("Error"))
                {
                    try
                    {
                        JSONObject jsonObject=new JSONObject(response);
                        JSONArray array=jsonObject.getJSONArray("data");
                        if (array.length()==0){
                            Toast toast = Toast.makeText(getApplicationContext(),
                                    "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                        }
                        else {
                            ArrayList<PackingTicketModel> ticketmodel= new ArrayList<>();

                            for (int i=0;i<array.length();i++)
                            {

                                JSONObject obj=new JSONObject(array.get(i).toString());
                                PackingTicketModel model=new PackingTicketModel();
                                if (ticket_release.equals("ReleasedTicket"))
                                {
                                    if (obj.getString("tk_Release").equals("true"))
                                    {
                                        model.setID(obj.getString("ID"));
                                        model.setTk_Id(obj.getString("tk_Id"));
                                        model.setTk_TicketNo(obj.getString("tk_TicketNo"));
                                        model.setTk_SONO(obj.getString("tk_SONO"));
                                        model.setTk_UploadRefNumber(obj.getString("tk_UploadRefNumber"));
                                        model.setTk_Plant(obj.getString("tk_Plant"));
                                        model.setTk_SKU(obj.getString("tk_SKU"));
                                        model.setTk_Prod_SKU(obj.getString("tk_Prod_SKU"));
                                        model.setTk_Release_Date(obj.getString("tk_Release_Date"));
                                        model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                                        model.setEDD(obj.getString("EDD"));
                                        model.setTk_End_Date(obj.getString("tk_End_Date"));
                                        model.setTk_Quantity(obj.getString("tk_Quantity"));
                                        model.setPln_Packing_Norms(obj.getString("pln_Packing_Norms"));
                                        model.setTk_Unpaking_SKU(obj.getString("tk_Unpaking_SKU"));
                                        model.setTk_Unpacking(obj.getString("tk_Unpacking"));
                                        model.setMonthStart(obj.getString("MonthStart"));
                                        model.setMonthEnd(obj.getString("MonthEnd"));
                                        model.setTk_Active(obj.getString("tk_Active"));
                                        model.setTk_Release(obj.getString("tk_Release"));
                                        model.setTk_produce_qty(obj.getString("tk_totalQty"));
                                        model.setTk_actual_end_Date(obj.getString("tk_Actual_End_Date"));
                                        ticketmodel.add(model);
                                    }
                                }



                            }
                            adapter=new TicketListRvAdapter(JKPackingTicketSelection.this,ticketmodel,rv_packing_ticket_list,adapter);
                            rv_packing_ticket_list.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL, false));
                            rv_packing_ticket_list.setAdapter(adapter);
                        }
                    }
                    catch (Exception e){e.printStackTrace();}
                }
            }
        }

    }

    public class GetAllTicketsCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JKPackingTicketSelection.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="NA";
            String url = null;
            //This call is used for display all tickets
            url= JK_URL_Constants.MAIN_JK_URL+JK_URL_Constants.GET_TICKETS + "null" + "&plantuser=" + JK_URL_Constants.PLANT;
            response = URL_Constants.makeHttpGetRequest(url);
            Log.d(URL_Constants.TAG, "Whole data = " + response);
            return response;
        }
        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            {
                if (!response.equals("Error"))
                {
                    try
                    {
                        JSONObject jsonObject=new JSONObject(response);
                        JSONArray array=jsonObject.getJSONArray("Data");
                        if (array.length()==0){
                            Toast toast = Toast.makeText(getApplicationContext(),
                                    "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                        }
                        else {
                            ArrayList<PackingTicketModel> ticketmodel= new ArrayList<>();

                            for (int i=0;i<array.length();i++)
                            {

                                JSONObject obj=new JSONObject(array.get(i).toString());
                                PackingTicketModel model=new PackingTicketModel();
                               /* if (ticket_release.equals("ReleasedTicket"))
                                {
                                    if (obj.getString("tk_Release").equals("true"))
                                    {
                                        model.setID(obj.getString("ID"));
                                        model.setTk_Id(obj.getString("tk_Id"));
                                        model.setTk_TicketNo(obj.getString("tk_TicketNo"));
                                        model.setTk_SONO(obj.getString("tk_SONO"));
                                        model.setTk_UploadRefNumber(obj.getString("tk_UploadRefNumber"));
                                        model.setTk_Plant(obj.getString("tk_Plant"));
                                        model.setTk_SKU(obj.getString("tk_SKU"));
                                        model.setTk_Prod_SKU(obj.getString("tk_Prod_SKU"));
                                        model.setTk_Release_Date(obj.getString("tk_Release_Date"));
                                        model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                                        model.setEDD(obj.getString("EDD"));
                                        model.setTk_End_Date(obj.getString("tk_End_Date"));
                                        model.setTk_Quantity(obj.getString("tk_Quantity"));
                                        model.setPln_Packing_Norms(obj.getString("pln_Packing_Norms"));
                                        model.setTk_Unpaking_SKU(obj.getString("tk_Unpaking_SKU"));
                                        model.setTk_Unpacking(obj.getString("tk_Unpacking"));
                                        model.setMonthStart(obj.getString("MonthStart"));
                                        model.setMonthEnd(obj.getString("MonthEnd"));
                                        model.setTk_Active(obj.getString("tk_Active"));
                                        model.setTk_Release(obj.getString("tk_Release"));
                                        ticketmodel.add(model);
                                    }
                                }*/
                                /*if (ticket_release.equals("UnreleasedTicket"))
                                {
                                    if (obj.getString("tk_Release").equals("false"))
                                    {
                                        model.setID(obj.getString("ID"));
                                        model.setTk_Id(obj.getString("tk_Id"));
                                        model.setTk_TicketNo(obj.getString("tk_TicketNo"));
                                        model.setTk_SONO(obj.getString("tk_SONO"));
                                        model.setTk_UploadRefNumber(obj.getString("tk_UploadRefNumber"));
                                        model.setTk_Plant(obj.getString("tk_Plant"));
                                        model.setTk_SKU(obj.getString("tk_SKU"));
                                        model.setTk_Prod_SKU(obj.getString("tk_Prod_SKU"));
                                        model.setTk_Release_Date(obj.getString("tk_Release_Date"));
                                        model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                                        model.setEDD(obj.getString("EDD"));
                                        model.setTk_End_Date(obj.getString("tk_End_Date"));
                                        model.setTk_Quantity(obj.getString("tk_Quantity"));
                                        model.setPln_Packing_Norms(obj.getString("pln_Packing_Norms"));
                                        model.setTk_Unpaking_SKU(obj.getString("tk_Unpaking_SKU"));
                                        model.setTk_Unpacking(obj.getString("tk_Unpacking"));
                                        model.setMonthStart(obj.getString("MonthStart"));
                                        model.setMonthEnd(obj.getString("MonthEnd"));
                                        model.setTk_Active(obj.getString("tk_Active"));
                                        model.setTk_Release(obj.getString("tk_Release"));
                                        ticketmodel.add(model);
                                    }
                                }
                                else
                                {*/
                                if (obj.has("tk_Release_Date")){
                                    model.setID(obj.getString("ID"));
                                    model.setTk_Id(obj.getString("tk_Id"));
                                    model.setTk_TicketNo(obj.getString("tk_TicketNo"));
                                    model.setTk_SONO(obj.getString("tk_SONO"));
                                    model.setTk_UploadRefNumber(obj.getString("tk_UploadRefNumber"));
                                    model.setTk_Plant(obj.getString("tk_Plant"));
                                    model.setTk_SKU(obj.getString("tk_SKU"));
                                    model.setTk_Prod_SKU(obj.getString("tk_Prod_SKU"));
                                    model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                                    model.setEDD(obj.getString("EDD"));
                                    model.setTk_End_Date(obj.getString("tk_End_Date"));
                                    model.setTk_Quantity(obj.getString("tk_Quantity"));
                                    model.setPln_Packing_Norms(obj.getString("pln_Packing_Norms"));
                                    //model.setTk_Unpaking_SKU(obj.getString("tk_Unpaking_SKU"));
                                    model.setTk_Unpacking(obj.getString("tk_Unpacking"));
                                    model.setMonthStart(obj.getString("MonthStart"));
                                    model.setMonthEnd(obj.getString("MonthEnd"));
                                    model.setTk_Active(obj.getString("tk_Active"));
                                    model.setTk_Release(obj.getString("tk_Release"));
                                    model.setTk_produce_qty(obj.getString("tk_totalQty"));
                                    // model.setTk_actual_end_Date(obj.getString("tk_Actual_End_Date"));
                                    model.setTk_Release_Date(obj.getString("tk_Release_Date"));
                                    ticketmodel.add(model);
                                }
                                else
                                {
                                    model.setID(obj.getString("ID"));
                                    model.setTk_Id(obj.getString("tk_Id"));
                                    model.setTk_TicketNo(obj.getString("tk_TicketNo"));
                                    model.setTk_SONO(obj.getString("tk_SONO"));
                                    model.setTk_UploadRefNumber(obj.getString("tk_UploadRefNumber"));
                                    model.setTk_Plant(obj.getString("tk_Plant"));
                                    model.setTk_SKU(obj.getString("tk_SKU"));
                                    model.setTk_Prod_SKU(obj.getString("tk_Prod_SKU"));
                                    model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                                    model.setEDD(obj.getString("EDD"));
                                    model.setTk_End_Date(obj.getString("tk_End_Date"));
                                    model.setTk_Quantity(obj.getString("tk_Quantity"));
                                    model.setPln_Packing_Norms(obj.getString("pln_Packing_Norms"));
                                    //model.setTk_Unpaking_SKU(obj.getString("tk_Unpaking_SKU"));
                                    model.setTk_Unpacking(obj.getString("tk_Unpacking"));
                                    model.setMonthStart(obj.getString("MonthStart"));
                                    model.setMonthEnd(obj.getString("MonthEnd"));
                                    model.setTk_Active(obj.getString("tk_Active"));
                                    model.setTk_Release(obj.getString("tk_Release"));
                                    model.setTk_produce_qty(obj.getString("tk_totalQty"));
                                    //model.setTk_actual_end_Date(obj.getString("tk_Actual_End_Date"));
                                    model.setTk_Release_Date("null");
                                    ticketmodel.add(model);
                                }

                            }
                            adapter=new TicketListRvAdapter(JKPackingTicketSelection.this,ticketmodel,rv_packing_ticket_list,adapter);
                            rv_packing_ticket_list.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL, false));
                            rv_packing_ticket_list.setAdapter(adapter);
                        }
                    }
                    catch (Exception e){e.printStackTrace();}
                }
            }
        }
    }

    public class GetActiveTicketsCall extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JKPackingTicketSelection.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... strings) {
            String response="NA";

            String url = null;
            url= JK_URL_Constants.MAIN_JK_URL+JK_URL_Constants.GET_TICKETS + "null" + "&plantuser=" + JK_URL_Constants.PLANT;
            response = URL_Constants.makeHttpGetRequest(url);
            Log.d(URL_Constants.TAG, "Whole data = " + response);
            return response;

        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            {
                if (!response.equals("Error"))
                {
                    try
                    {
                        JSONObject jsonObject=new JSONObject(response);
                        JSONArray array=jsonObject.getJSONArray("data");
                        if (array.length()==0){
                            Toast toast = Toast.makeText(getApplicationContext(),
                                    "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                        }
                        else {
                            ArrayList<PackingTicketModel> ticketmodel= new ArrayList<>();

                            for (int i=0;i<array.length();i++)
                            {

                                JSONObject obj=new JSONObject(array.get(i).toString());
                                PackingTicketModel model=new PackingTicketModel();
                                String ticketactive=obj.getString("tk_Active");
                                if (!ticketactive.equals("false"))
                                {
                                    Toast toast = Toast.makeText(getApplicationContext(),
                                            "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                                    toast.show();

                                }
                                if (ticketactive.equals("false"))
                                {
                                    model.setID(obj.getString("ID"));
                                    model.setTk_Id(obj.getString("tk_Id"));
                                    model.setTk_TicketNo(obj.getString("tk_TicketNo"));
                                    model.setTk_SONO(obj.getString("tk_SONO"));
                                    model.setTk_UploadRefNumber(obj.getString("tk_UploadRefNumber"));
                                    model.setTk_Plant(obj.getString("tk_Plant"));
                                    model.setTk_SKU(obj.getString("tk_SKU"));
                                    model.setTk_Prod_SKU(obj.getString("tk_Prod_SKU"));
                                    model.setTk_Release_Date(obj.getString("tk_Release_Date"));
                                    model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                                    model.setEDD(obj.getString("EDD"));
                                    model.setTk_End_Date(obj.getString("tk_End_Date"));
                                    model.setTk_Quantity(obj.getString("tk_Quantity"));
                                    model.setPln_Packing_Norms(obj.getString("pln_Packing_Norms"));
                                    model.setTk_Unpaking_SKU(obj.getString("tk_Unpaking_SKU"));
                                    model.setTk_Unpacking(obj.getString("tk_Unpacking"));
                                    model.setMonthStart(obj.getString("MonthStart"));
                                    model.setMonthEnd(obj.getString("MonthEnd"));
                                    model.setTk_Active(obj.getString("tk_Active"));
                                    model.setTk_Release(obj.getString("tk_Release"));
                                    model.setTk_produce_qty(obj.getString("tk_totalQty"));
                                    model.setTk_actual_end_Date(obj.getString("tk_Actual_End_Date"));
                                    ticketmodel.add(model);
                                }
                            }
                            if (ticketmodel.size()==0)
                            {
                                Toast toast = Toast.makeText(getApplicationContext(),
                                        "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                                toast.show();
                                rv_packing_ticket_list.setAdapter(null);
                            }
                            else {adapter=new TicketListRvAdapter(JKPackingTicketSelection.this,ticketmodel,rv_packing_ticket_list,adapter);
                                rv_packing_ticket_list.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL, false));
                                rv_packing_ticket_list.setAdapter(adapter);}
                            /*adapter=new TicketListRvAdapter(JKPackingTicketSelection.this,ticketmodel,rv_packing_ticket_list,adapter);
                            rv_packing_ticket_list.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL, false));
                            rv_packing_ticket_list.setAdapter(adapter);*/
                        }
                    }
                    catch (Exception e){e.printStackTrace();}
                }
            }
        }


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id==R.id.nav_planning)
        {
            Intent i=new Intent(JKPackingTicketSelection.this,FalcoJKHomeActivity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    public String getIndianCurrencyFormat(String amount) {
        StringBuilder stringBuilder = new StringBuilder();
        char amountArray[] = amount.toCharArray();
        int a = 0, b = 0;
        for (int i = amountArray.length - 1; i >= 0; i--) {
            if (a < 3) {
                stringBuilder.append(amountArray[i]);
                a++;
            } else if (b < 2) {
                if (b == 0) {
                    stringBuilder.append(",");
                    stringBuilder.append(amountArray[i]);
                    b++;
                } else {
                    stringBuilder.append(amountArray[i]);
                    b = 0;
                }
            }
        }
        return stringBuilder.reverse().toString();
    }
}

