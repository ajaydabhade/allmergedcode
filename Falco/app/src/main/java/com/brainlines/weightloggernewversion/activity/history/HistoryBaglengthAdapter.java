package com.brainlines.weightloggernewversion.activity.history;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class HistoryBaglengthAdapter extends RecyclerView.Adapter<HistoryBaglengthAdapter.MyViewHolder> {
    Context context;
    ArrayList<String> baglengthreading = new ArrayList<>();

    public HistoryBaglengthAdapter(Context context, ArrayList<String> weightReadingsList) {
        this.context = context;
        this.baglengthreading = weightReadingsList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_bag_length_history_reading,parent,false);
        return new HistoryBaglengthAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        String readingValue = baglengthreading.get(position);
        if (!readingValue.equals("")){
            holder.txt_weight_reading.setText(readingValue);
        }
    }

    @Override
    public int getItemCount() {
        return baglengthreading.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_weight_reading;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_weight_reading = (itemView).findViewById(R.id.txt_bag_length_Reading);
        }
    }
}
