package com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.AssignedCall;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.FalcoJK.activity.FalcoJKHomeActivity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.InProcessCall.Swas_Deatails_of_Inprocess_Call_Activity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.InProcessCall.Swas_Inprocess_Call_Service_Id_Details_Activity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Home_Actvity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter.Chekc_List_Adapter;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter.FaultyCharacteristicsAdapter;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter.PartOrderBy_Customer_Adapter;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.Checklist_Model;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.FaultyCharactristics_Model;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.PartOrderByCustomerModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasIdDetailsModel;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Swas_Assigned_Call_Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    String user_email,user_role,user_token,created_by,service_eng_id;
    ImageView img_user_profile,nav_view_img,img_back;
    DrawerLayout drawer;
    String status_id,service_call_id,type_of_service_status_id,type_of_service_call_id;
    RecyclerView rv_checklist,rv_part_order_by;
    ImageButton img_btn_accept,img_btn_reject;
    Chekc_List_Adapter chekc_list_adapter;
    String customer_id;
    String rejectReason;
    String complaintG = "";
    LinearLayout relativeLayout1;
    TextView txt_service_call_status_text;
    PartOrderBy_Customer_Adapter partOrderBy_customer_adapter;
    FaultyCharacteristicsAdapter faultyCharacteristicsAdapter;
    RecyclerView rv_faulty_characteristics;
    TextView txt_type_of_fault_text;
    TextView txt_assign_sr_id,txt_assign_custom_name,txt_assign_call_created_by,txt_assign_create_on,txt_last_modified,txt_type_service_call,txt_assign_product,txt_assign_complaint,txt_assign_no,txt_note_for_service_engineer, txt_service_call_status;
    DataBaseHelper helper;

    //New Varibles
    private ArrayList<Checklist_Model> serviceCallCheckListModels = new ArrayList<>();
    private ArrayList<PartOrderByCustomerModel> partOrderModels = new ArrayList<>();
    private ArrayList<FaultyCharactristics_Model> faultCharTextList = new ArrayList<>();
    private String dbName;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_swas_assigned_call_id_details_navigation);
        initUi();
        setData();
//        dbName = AppPreferences.getDatabseName(this);
////        helper = new DataBaseHelper(this,dbName);
        helper = new DataBaseHelper(this);
//        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));
        //user_email = "yogesh@brainlines.in";
        if (user_email.equals("yogesh@brainlines.in"))
        {
            created_by = "Yogesh Jadhav";
//            service_eng_id = "1";
        }
        else
        {
            created_by = "Deepak Kulkarni";
//            service_eng_id = "2";
        }
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img=(ImageView)findViewById(R.id.nav_view_img);
        img_user_profile=(ImageView)findViewById(R.id.img_userinfo);
        img_user_profile.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });
        img_back=(ImageView)findViewById(R.id.img_back);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Swas_Assigned_Call_Activity.this,Swas_Home_Actvity.class);
                startActivity(i);
                finish();
            }
        });
        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
        {
            status_id = bundle.getString("status_id");
            service_call_id = bundle.getString("service_call_id");
            type_of_service_status_id = bundle.getString("Typeofservicecall_id");
            customer_id = bundle.getString("customer_id");
        }

        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnected();
//        if(isConnected) {
//            Log.d("Network", "Connected");
//            Id_Details();
////            getDataFromListOfServiceCallByStatus();
////            getServiceCallCheckList();
////            getPartOrders();
////            getFaultCharas();
//
//        }
//        else{
//            checkNetworkConnection();
//            Log.d("Network","Not Connected");
//            getDataFromListOfServiceCallByStatus();
//            getServiceCallCheckList();
//            getPartOrders();
//            getFaultCharas();
//        }

        getDataFromListOfServiceCallByStatus();
        getServiceCallCheckList();
        getPartOrders();
        getFaultCharas();

        txt_assign_complaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view_detail_complaint(complaintG);
            }
        });
        img_btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AcceptCall();
                //new AceeptServiceCall().execute();

            }
        });
        img_btn_reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View_details();
            }
        });
    }

    public void initUi() {
        img_btn_accept = (ImageButton)findViewById(R.id.img_btn_assign_accept);
        img_btn_reject = (ImageButton)findViewById(R.id.img_btn_assign_recject);
        relativeLayout1 = (LinearLayout)findViewById(R.id.relativeLayout1);

        txt_type_of_fault_text = (TextView)findViewById(R.id.txt_type_of_fault_text);

        txt_service_call_status_text = (TextView)findViewById(R.id.txt_service_call_status_text);
        txt_assign_sr_id=(TextView)findViewById(R.id.txt_assign_sr_id);
        txt_assign_custom_name=(TextView)findViewById(R.id.txt_assign_custom_name);
        txt_assign_call_created_by=(TextView)findViewById(R.id.txt_assign_call_created_by);
        txt_assign_create_on=(TextView)findViewById(R.id.txt_assign_create_on);
        // txt_last_modified=(TextView)findViewById(R.id.txt_last_modified);
        txt_type_service_call=(TextView)findViewById(R.id.txt_type_service_call);
        txt_assign_product=(TextView)findViewById(R.id.txt_assign_product);
        txt_assign_complaint=(TextView)findViewById(R.id.txt_assign_complaint);
        //txt_assign_no=(TextView)findViewById(R.id.txt_assign_no);
        txt_last_modified=(TextView)findViewById(R.id.txt_last_modified);
        txt_note_for_service_engineer =(TextView)findViewById(R.id.txt_note_for_service_engineer);

        rv_checklist  = (RecyclerView)findViewById(R.id.rv_checklist);
        rv_part_order_by = (RecyclerView)findViewById(R.id.rv_part_order_by);
        rv_faulty_characteristics = (RecyclerView)findViewById(R.id.rv_faulty_characteristics);
        rv_checklist.setEnabled(false);

        rv_part_order_by.setEnabled(false);
    }

    private void getFaultCharas() {
        faultCharTextList.clear();
        Cursor cursor = helper.getFaultCharList(service_call_id);
        while (cursor.moveToNext()){
            String customerId = cursor.getString(1);
            String serviceCallID = cursor.getString(2);
            String faultCharText = cursor.getString(3);
            FaultyCharactristics_Model model = new FaultyCharactristics_Model();
            model.setCustomerID(customerId);;
            model.setServiceCallID(serviceCallID);
            model.setFaultCharacteristicsText(faultCharText);
            faultCharTextList.add(model);
        }
        faultyCharacteristicsAdapter = new FaultyCharacteristicsAdapter(Swas_Assigned_Call_Activity.this,faultCharTextList);
        rv_faulty_characteristics.setLayoutManager(new LinearLayoutManager(Swas_Assigned_Call_Activity.this,LinearLayoutManager.VERTICAL, false));
        rv_faulty_characteristics.setAdapter(faultyCharacteristicsAdapter);
    }
    private void getPartOrders() {
        partOrderModels.clear();
        Cursor cursor = helper.getPartOrdersAgainstServiceCallIDForDetails(service_call_id);
        while(cursor.moveToNext()){
            String ServiceCallID = cursor.getString(1);
            String OEMID = cursor.getString(2);
            String customerId = cursor.getString(3);
            String itemId = cursor.getString(4);
            String quantity = cursor.getString(5);
            String name = cursor.getString(6);
            String price = cursor.getString(7);
            String applicable = cursor.getString(8);
            String finalPrice = cursor.getString(9);
            String excludeInclude = cursor.getString(10);
            String createdBy = cursor.getString(11);
            String createdDate = cursor.getString(12);
            PartOrderByCustomerModel model= new PartOrderByCustomerModel();
            model.setServiceCallID(ServiceCallID);
            model.setOEMID(OEMID);
            model.setCustomerID(customerId);
            model.setITEMID(itemId);
            model.setQuantity(quantity);
            model.setNAME(name);
            model.setPRICE(price);
            model.setApplicable(applicable);
            model.setFinalPrice(finalPrice);
            model.setExcludeInclude(excludeInclude);
            model.setCreatedBy(createdBy);
            model.setCreatedBy(createdDate);
            partOrderModels.add(model);

        }
        relativeLayout1.setVisibility(View.VISIBLE);
        partOrderBy_customer_adapter = new PartOrderBy_Customer_Adapter(Swas_Assigned_Call_Activity.this,partOrderModels);
        rv_part_order_by.setLayoutManager(new LinearLayoutManager(Swas_Assigned_Call_Activity.this,LinearLayoutManager.VERTICAL, false));
        rv_part_order_by.setAdapter(partOrderBy_customer_adapter);
    }
    private void getServiceCallCheckList() {
        serviceCallCheckListModels.clear();
        Cursor cursor = helper.getServiceCallCheckListAgainstServiceCallIDForDetails(service_call_id);
        while (cursor.moveToNext()){
            String checkListForSiteReadynessId = cursor.getString(2);
            String checkListForSiteReadynesstext = cursor.getString(3);
            String chcked = cursor.getString(4);
            Checklist_Model model = new Checklist_Model();
            model.setCheck_id(checkListForSiteReadynessId);
            model.setCheck_name(checkListForSiteReadynesstext);
            model.setChecked(chcked);
            serviceCallCheckListModels.add(model);
        }
        chekc_list_adapter = new Chekc_List_Adapter(Swas_Assigned_Call_Activity.this,serviceCallCheckListModels);
        rv_checklist.setLayoutManager(new LinearLayoutManager(Swas_Assigned_Call_Activity.this,LinearLayoutManager.VERTICAL, false));
        rv_checklist.setAdapter(chekc_list_adapter);
    }
    private void getDataFromListOfServiceCallByStatus() {
        Cursor cursor = helper.getDetailsDataFromServiceCallStatusTable(service_call_id);
        while (cursor.moveToNext()){
            String customer_name = cursor.getString(0);
            String created_By = cursor.getString(1);
            String created_on = cursor.getString(2);
            String last_modified_on = cursor.getString(3);
            String product = cursor.getString(4);
            String complaint = cursor.getString(5);
            String type_of_fault_text= cursor.getString(6);
            String typeOfServiceCallText= cursor.getString(7);
//            String statusText= cursor.getString(8);
            txt_assign_sr_id.setText(service_call_id);
            txt_assign_custom_name.setText(customer_name);
            txt_assign_call_created_by.setText(created_By);
            txt_assign_create_on.setText(created_on);
            txt_type_service_call.setText(typeOfServiceCallText);
            txt_assign_product.setText(product);
            txt_last_modified.setText(last_modified_on);
            txt_type_of_fault_text.setText(type_of_fault_text);
//            txt_service_call_status_text.setText(statusText);
            complaintG = complaint;
//            view_detail_complaint(complaint);

        }
    }

    private void Id_Details() {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getalldetailsofsingleservicecall(service_call_id,status_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                    }
                    String res = response.body().string();
                    if (res != null) {

                        JSONObject object = new JSONObject(res);
                        JSONArray array = object.getJSONArray("data");
                        ArrayList<SwasIdDetailsModel> model = new ArrayList<>();
                        if (array.length() != 0) {

                            for (int i = 0; i < array.length(); i++) {
                                SwasIdDetailsModel swasIdDetailsModel = new SwasIdDetailsModel();
                                JSONObject object1 = array.getJSONObject(i);
                                swasIdDetailsModel.setService_Id(object1.getString("ServiceCallID"));
                                swasIdDetailsModel.setLOCATIONNAME(object1.getString("LOCATIONNAME"));
                                swasIdDetailsModel.setCreatedBy(object1.getString("CreatedBy"));
                                swasIdDetailsModel.setCreatedDate(object1.getString("CreatedDate"));
                                swasIdDetailsModel.setUpdatedBy(object1.getString("UpdatedBy"));
                                swasIdDetailsModel.setUpdatedDate(object1.getString("UpdatedDate"));
                                swasIdDetailsModel.setTypeOfServiceCallText(object1.getString("TypeOfServiceCallText"));
                                swasIdDetailsModel.setNAME(object1.getString("NAME"));
                                swasIdDetailsModel.setBriefComplaint(object1.getString("BriefComplaint"));
                                swasIdDetailsModel.setTypeOfFaultText(object1.getString("TypeOfFaultText"));
                                swasIdDetailsModel.setStatusText(object1.getString("StatusText"));
                                swasIdDetailsModel.setFaultyPart(object1.getString("FaultyPart"));
                                swasIdDetailsModel.setCustomerID(object1.getString("CustomerID"));
                                swasIdDetailsModel.setITEMID(object1.getString("ITEMID"));
                                swasIdDetailsModel.setCURRENCY(object1.getString("CURRENCY"));
                                swasIdDetailsModel.setDocumentPath(object1.getString("DocumentPath"));
                                swasIdDetailsModel.setTypeOfServiceCallID(object1.getString("TypeOfServiceCallID"));
                                swasIdDetailsModel.setCurrentlyAssignedToSE(object1.getString("CurrentlyAssignedToSE"));
                                swasIdDetailsModel.setServiceEngineerName(object1.getString("ServiceEngineerName"));
                                swasIdDetailsModel.setClosedDateTime(object1.getString("ClosedDateTime"));
                                swasIdDetailsModel.setClosedBy(object1.getString("ClosedBy"));
                                swasIdDetailsModel.setServiceCallStatusID(object1.getString("ServiceCallStatusID"));
                                swasIdDetailsModel.setComments(object1.getString("Comments"));
                                swasIdDetailsModel.setServiceEngineerID(object1.getString("ServiceEngineerID"));
                                swasIdDetailsModel.setNumberOfVisits(object1.getString("NumberOfVisits"));

                                //code of update
                                helper.updateListOfServicesByStatus(service_call_id,object1.getString("NAME"),object1.getString("BriefComplaint"),
                                        object1.getString("TypeOfFaultText"),object1.getString("FaultyPart"),object1.getString("CURRENCY"),
                                        object1.getString("DocumentPath"),object1.getString("TypeOfServiceCallID"),object1.getString("CurrentlyAssignedToSE"),
                                        object1.getString("ClosedDateTime"),object1.getString("ClosedBy"),object1.getString("Comments"),
                                        object1.getString("ServiceEngineerID"),object1.getString("NumberOfVisits"));

                                txt_assign_sr_id.setText(object1.getString("ServiceCallID"));
                                txt_assign_custom_name.setText(object1.getString("LOCATIONNAME"));
                                txt_assign_call_created_by.setText(object1.getString("CreatedBy"));
                                txt_assign_create_on.setText(object1.getString("CreatedDate"));
                                txt_type_service_call.setText(object1.getString("TypeOfServiceCallText"));
                                txt_service_call_status_text.setText(object1.getString("StatusText"));
                                txt_assign_product.setText(object1.getString("NAME"));
                                txt_last_modified.setText(object1.getString("UpdatedDate"));

                                if(!object1.getString("Comments").equals("null"))
                                {
                                    txt_note_for_service_engineer.setText(object1.getString("Comments"));
                                }

                                txt_type_of_fault_text.setText(object1.getString("TypeOfFaultText"));
                                complaintG = object1.getString("BriefComplaint");
                                customer_id = object1.getString("CustomerID");
                                status_id = object1.getString("ServiceCallStatusID");
                                service_call_id = object1.getString("ServiceCallID");
                                //type_of_service_status_id = object1.getString("TypeOfServiceCallID");
                                type_of_service_call_id = object1.getString("TypeOfServiceCallID");
                                model.add(swasIdDetailsModel);
                            }
                            GetChecklist();
                            GetPartOrderBY();
                            FaultyCharacteristicsList();
                        }
                        else if (array.length() == 0) {
                        }
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void View_details() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.swas_rejecting_call_status, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        EditText ed_Reason = (EditText)dialogView.findViewById(R.id.ed_Reason);
        ImageButton btn_submit = dialogView.findViewById(R.id.btnSubmit);
        rejectReason = ed_Reason.getText().toString();

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                RejectCall();
                //new RejectServiceCall().execute();
            }
        });
    }

    private void GetChecklist() {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getchecklistofservicecall(service_call_id,type_of_service_call_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                    }
                    String res = response.body().string();
                    if (res != null) {

                        JSONObject object = new JSONObject(res);
                        JSONArray array = object.getJSONArray("data");
                        ArrayList<Checklist_Model> checklist_models = new ArrayList<>();
                        if (array.length() != 0) {

                            for (int i = 0; i < array.length(); i++) {
                                Checklist_Model swasIdDetailsModel = new Checklist_Model();
                                Checklist_Model model = new Checklist_Model();
                                JSONObject obj = array.getJSONObject(i);
                                model.setCheck_id(obj.getString("ChecklistForSiteReadynessID"));
                                model.setCheck_name(obj.getString("ChecklistForSiteReadynessText"));
                                model.setChecked(obj.getString("Checked"));
                                checklist_models.add(model);

                            }
                            chekc_list_adapter = new Chekc_List_Adapter(Swas_Assigned_Call_Activity.this,checklist_models);
                            rv_checklist.setLayoutManager(new LinearLayoutManager(Swas_Assigned_Call_Activity.this,LinearLayoutManager.VERTICAL, false));
                            rv_checklist.setAdapter(chekc_list_adapter);


                        } else if (array.length() == 0) {
                        }
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void GetPartOrderBY() {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getpartordered(service_call_id,customer_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                    }
                    String res = response.body().string();
                    if (res != null) {

                        JSONArray array = new JSONArray(res);
                        ArrayList<PartOrderByCustomerModel> partOrderByCustomerModelArrayList = new ArrayList<>();
                        if (array.length() != 0) {

                            for (int i = 0; i < array.length(); i++) {
                                PartOrderByCustomerModel model = new PartOrderByCustomerModel();
                                JSONObject obj = array.getJSONObject(i);
                                model.setApplicable(obj.getString("Applicable"));
                                model.setConsumables(obj.getString("Consumables"));
                                model.setCreatedBy(obj.getString("CreatedBy"));
                                model.setCreatedDate(obj.getString("CreatedDate"));
                                model.setCustomerID(obj.getString("CustomerID"));
                                model.setExcludeInclude(obj.getString("ExcludeInclude"));
                                model.setFinalPrice(obj.getString("FinalPrice"));
                                model.setITEMID(obj.getString("ITEMID"));
                                model.setMachineID(obj.getString("MachineID"));
                                model.setNAME(obj.getString("NAME"));
                                model.setPRICE(obj.getString("PRICE"));
                                model.setOEMID(obj.getString("OEMID"));
                                model.setQuantity(obj.getString("Quantity"));

                                partOrderByCustomerModelArrayList.add(model);
                            }
                            relativeLayout1.setVisibility(View.VISIBLE);
                            partOrderBy_customer_adapter = new PartOrderBy_Customer_Adapter(Swas_Assigned_Call_Activity.this,partOrderByCustomerModelArrayList);
                            rv_part_order_by.setLayoutManager(new LinearLayoutManager(Swas_Assigned_Call_Activity.this,LinearLayoutManager.VERTICAL, false));
                            rv_part_order_by.setAdapter(partOrderBy_customer_adapter);


                        } else if (array.length() == 0) {
                            relativeLayout1.setVisibility(View.VISIBLE);
                        }
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void FaultyCharacteristicsList() {
        API api = Retroconfig.swasretrofit().create(API.class);
        Call<ResponseBody> call = api.getfaultcharacteristicslist(service_call_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                    }
                    String res = response.body().string();
                    if (res != null) {

                        JSONObject object =  new JSONObject(res);
                        JSONArray array = object.getJSONArray("data");
                        ArrayList<FaultyCharactristics_Model> faultyCharactristics_modelArrayList = new ArrayList<>();
                        if (array.length() != 0) {

                            for (int i = 0; i < array.length(); i++) {
                                FaultyCharactristics_Model model = new FaultyCharactristics_Model();
                                JSONObject obj = array.getJSONObject(i);
                                model.setCustomerID(obj.getString("CustomerID"));
                                model.setServiceCallID(obj.getString("ServiceCallID"));
                                model.setFaultCharacteristicsText(obj.getString("FaultCharacteristicsText"));

                                faultyCharactristics_modelArrayList.add(model);
                            }
                            //relativeLayout1.setVisibility(View.VISIBLE);
                            faultyCharacteristicsAdapter = new FaultyCharacteristicsAdapter(Swas_Assigned_Call_Activity.this,faultyCharactristics_modelArrayList);
                            rv_faulty_characteristics.setLayoutManager(new LinearLayoutManager(Swas_Assigned_Call_Activity.this,LinearLayoutManager.VERTICAL, false));
                            rv_faulty_characteristics.setAdapter(faultyCharacteristicsAdapter);

                        } else if (array.length() == 0) {
                            relativeLayout1.setVisibility(View.VISIBLE);
                        }
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public class AceeptServiceCall extends AsyncTask<String,String,String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Swas_Assigned_Call_Activity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            final String status_id = "8";
            String isCurrent = "1";
            String isMobile = "1";

            String url="http://apps.brainlines.net/SWAS-ServiceCallWebAPI/api/servicecall/saveacceptservicecallstatus?ServiceCallID=";
            String url1=  url +  service_call_id + "&ServiceCallStatusID=" + status_id + "&IsCurrent=" + isCurrent + "&CreatedBy=" + created_by + "&IsMobile=" + isMobile;
            url1 = url1.replaceAll(" ", "%20");
            response= URL_Constants.makeHttpPostRequest(url1);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                Toast.makeText(Swas_Assigned_Call_Activity.this,"You Accepted this Call",Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Swas_Assigned_Call_Activity.this, Swas_Deatails_of_Inprocess_Call_Activity.class);
                Bundle bundle  = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_service_status_id);
                bundle.putString("customer_id",customer_id);
                i.putExtras(bundle);
                startActivity(i);
                finish();

            }
        }
    }

    public class RejectServiceCall extends AsyncTask<String,String,String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Swas_Assigned_Call_Activity.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String isCurrent = "1";
            String isMobile = "1";
            //this is status_id for reject status
            String Status_id = "4";

            String url="http://apps.brainlines.net/SWAS-ServiceCallWebAPI/api/servicecall/saverejectedservicecallstatus";
            String url1=  url + "&ServiceCallID=" +  service_call_id + "&ServiceCallStatusID=" + Status_id + "&IsCurrent=" + isCurrent + "&CreatedBy=" + created_by + "&IsMobile=" + isMobile;
            response= URL_Constants.makeHttpPostRequest(url1);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("Error"))
            {
                Toast.makeText(Swas_Assigned_Call_Activity.this,"You Accepted this Call",Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Swas_Assigned_Call_Activity.this, Swas_Inprocess_Call_Service_Id_Details_Activity.class);
                Bundle bundle  = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_service_status_id);
                bundle.putString("customer_id",customer_id);
                i.putExtras(bundle);
                startActivity(i);
                finish();

            }
        }
    }

    private void AcceptCall() {
        API api = Retroconfig.swasretrofit().create(API.class);
        final int ServiceCallStatusID = 8;
        int isCurrent = 1;
        String isMobile = "1";
        int QEStatusID = 2;
        Call<ResponseBody> call = api.saveAcceptCall(
                Integer.parseInt(service_call_id),
                ServiceCallStatusID,
                isCurrent,
                created_by,
                QEStatusID,
                isMobile);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {

                    String res = response.body().string();
                    Toast.makeText(Swas_Assigned_Call_Activity.this,"You have Accepted this Call",Toast.LENGTH_SHORT).show();
                    updateListOfSericeCall(service_call_id,8);
                    Intent i = new Intent(Swas_Assigned_Call_Activity.this, Swas_Inprocess_Call_Service_Id_Details_Activity.class);
                    Bundle bundle  = new Bundle();
                    bundle.putString("service_call_id",service_call_id);
                    bundle.putString("status_id",status_id);
                    bundle.putString("Typeofservicecall_id",type_of_service_status_id);
                    bundle.putString("customer_id",customer_id);
                    i.putExtras(bundle);
                    startActivity(i);
                    finish();
                    if (response.body() != null) {


                        /*if (res != null) {

                            JSONObject object = new JSONObject(res);
                            String status_code = object.getString("statusCode");
                            if (status_code.equals("200"))
                            {
                                Toast.makeText(Swas_Assigned_Call_Activity.this,"You Accepted this Call",Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(Swas_Assigned_Call_Activity.this, Swas_Inprocess_Call_Service_Id_Details_Activity.class);
                                Bundle bundle  = new Bundle();
                                bundle.putString("service_call_id",service_call_id);
                                bundle.putString("",customer_id);
                                bundle.putString("",type_of_service_status_id);
                                bundle.putString("status_id",status_id);
                                i.putExtras(bundle);
                                startActivity(i);
                                finish();
                            }
                            else if (status_code.equals("404"))
                            {
                                Toast.makeText(Swas_Assigned_Call_Activity.this,"You already reject this call",Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(Swas_Assigned_Call_Activity.this, Swas_Home_Actvity.class);
                                startActivity(i);
                                finish();

                            }
                        }*/

                    }
                   /* else
                    {
                        Toast.makeText(Swas_Assigned_Call_Activity.this,"You already reject this call",Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(Swas_Assigned_Call_Activity.this,Swas_Home_Actvity.class);
                        startActivity(i);
                        finish();
                    }*/

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(Swas_Assigned_Call_Activity.this, "Error", Toast.LENGTH_SHORT).show();


            }
        });
    }

    private void updateListOfSericeCall(String service_call_id,int statusID) {
        helper.updateAssignedCallRowToCompletedCall(service_call_id,statusID);
    }

    private void RejectCall() {
        API api = Retroconfig.swasretrofit().create(API.class);
        String isCurrent = "1";
        String isMobile = "1";
        //this is status_id for reject status
        String Status_id = "4";
        Call<ResponseBody> call = api.saverejectedservicecallstatus(service_call_id,Status_id,isCurrent,rejectReason,isMobile,created_by);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                    }
                    String res = response.body().string();
                    Toast.makeText(Swas_Assigned_Call_Activity.this,"You have Rejected this Call",Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(Swas_Assigned_Call_Activity.this, Swas_Home_Actvity.class);
                    startActivity(i);
                    finish();
                   /* Bundle bundle  = new Bundle();
                    bundle.putString("service_call_id",service_call_id);
                    bundle.putString("status_id",status_id);
                    bundle.putString("Typeofservicecall_id",type_of_service_status_id);
                    bundle.putString("customer_id",customer_id);
                    i.putExtras(bundle);
                    if (res != null) {

                        JSONObject object = new JSONObject(res);
                       String status_code = object.getString("statusCode");
                       if (status_code.equals("200"))
                       {
                           Toast.makeText(Swas_Assigned_Call_Activity.this,"You Reject this Call",Toast.LENGTH_SHORT).show();
                           Intent i = new Intent(Swas_Assigned_Call_Activity.this,Swas_Home_Actvity.class);
                           startActivity(i);
                           finish();
                       }



                    }*/

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    private void view_detail_complaint(String complaint) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.alert_view_detailed_complaint, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        TextView textView = (TextView)dialogView.findViewById(R.id.txt_details_complaint);
        textView.setText(complaint);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(Swas_Assigned_Call_Activity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);
        setData();
        // Set content width and height
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(Swas_Assigned_Call_Activity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }

    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(Swas_Assigned_Call_Activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel model = new  com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel();
                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    service_eng_id = cursor.getString(cursor.getColumnIndex(Constants.USER_LOGIN_ID));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout() {
        finish();
        Toast toast= Toast.makeText(Swas_Assigned_Call_Activity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(Swas_Assigned_Call_Activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);

        //Preferences.Logout();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id==R.id.nav_planning)
        {
            Intent i=new Intent(Swas_Assigned_Call_Activity.this, FalcoJKHomeActivity.class);
            startActivity(i);
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    public void checkNetworkConnection(){
        AlertDialog.Builder builder =new AlertDialog.Builder(Swas_Assigned_Call_Activity.this);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}