package com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.CompletedCall;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Feedback_Activity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasActivity.Swas_Home_Actvity;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter.SwasCompletedTravelAdapter;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompletedTravelModel;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Swas_Completed_Travelling_Expenses_Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    TextView txt_travel_service_call_id;
    RecyclerView rv_completed_travelling_expenses;
    SwasCompletedTravelAdapter adapter;
    String status_id,service_call_id,type_of_service_status_id,customer_id,oem_id;
    ImageButton btn_feedback;
    ImageView img_user_info,nav_view_img,img_back;
    String user_email,user_token,user_role;
    DrawerLayout drawer;
    TextView profile_user,profile_role;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swas_completed_travelling_expenses_navigation);

        initUi();
        setData();
        img_back=(ImageView)findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Swas_Completed_Travelling_Expenses_Activity.this, Swas_Details_Of_Completed_Call.class);
                Bundle bundle = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_service_status_id);
                bundle.putString("customer_id",customer_id);
                i.putExtras(bundle);
                startActivity(i);
                finish();
            }
        });
        img_user_info=(ImageView)findViewById(R.id.img_userinfo);
        img_user_info.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img=(ImageView)findViewById(R.id.nav_view_img);

        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_planning).setVisible(false);
        Bundle bundle = getIntent().getExtras();

        if (bundle != null)
        {
            status_id = bundle.getString("status_id");
            service_call_id = bundle.getString("service_call_id");
            txt_travel_service_call_id.setText(service_call_id);
            type_of_service_status_id = bundle.getString("Typeofservicecall_id");
            customer_id = bundle.getString("customer_id");
        }

        btn_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Swas_Completed_Travelling_Expenses_Activity.this, Swas_Feedback_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("service_call_id",service_call_id);
                bundle.putString("status_id",status_id);
                bundle.putString("Typeofservicecall_id",type_of_service_status_id);
                bundle.putString("customer_id",customer_id);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
//        if(isConnected) {
//            Log.d("Network", "Connected");
//            Travel();
//        }
//        else{
//            checkNetworkConnection();
//            Log.d("Network","Not Connected");
//            getExpenseDetailsDataFromLocalDB();
//        }
        getExpenseDetailsDataFromLocalDB();

    }

    private void getExpenseDetailsDataFromLocalDB() {
        ArrayList<SwasCompletedTravelModel> model = new ArrayList<>();
        Cursor cursor = new DataBaseHelper(this).getExpenseDetailsFromLocalDbAgainsterviceCallID(service_call_id);
        while (cursor.moveToNext()){
            String id = cursor.getString(0);
            String serviceCallID = cursor.getString(1);
            String oemid = cursor.getString(2);
            String custid = cursor.getString(3);
            String serviceExpenseHeadsid = cursor.getString(4);
            String serviceExpenseHeadsName = cursor.getString(5);
            String visitDates = cursor.getString(6);
            String vistCharges = cursor.getString(7);
            String createdBy = cursor.getString(8);
            String createdDate = cursor.getString(9);
            SwasCompletedTravelModel travelModel = new SwasCompletedTravelModel();
            travelModel.setTravellingExpensesHeadsID(id);
            travelModel.setServiceCallID(serviceCallID);
            travelModel.setOEMID(oemid);
            travelModel.setCustomerID(custid);
            travelModel.setService_Expense_Heads_id(serviceExpenseHeadsid);
            travelModel.setService_Expense_Heads_Name(serviceExpenseHeadsName);
            travelModel.setVisitDates(visitDates);
            travelModel.setVisitCharges(vistCharges);
            travelModel.setCreatedBy(createdBy);
            travelModel.setCreatedDate(createdDate);

            //txt_travel_service_call_id.setText(object1.getString("ServiceCallID"));
            //swasServiceModel .setServicecall_status_id(object1.getString("ServiceCallStatusID"));

            model.add(travelModel);
        }
        adapter = new SwasCompletedTravelAdapter(Swas_Completed_Travelling_Expenses_Activity.this,model);
        rv_completed_travelling_expenses.setLayoutManager(new LinearLayoutManager(Swas_Completed_Travelling_Expenses_Activity.this, LinearLayoutManager.VERTICAL, false));
        rv_completed_travelling_expenses.setAdapter(adapter);
    }

    public void initUi()
    {   txt_travel_service_call_id=(TextView)findViewById(R.id.txt_travel_service_call_id);
        rv_completed_travelling_expenses=(RecyclerView)findViewById(R.id.rv_completed_travelling_expenses);
        btn_feedback = findViewById(R.id.btn_feedback);

    }
    private void Travel(){
        API api = Retroconfig.swasretrofit().create(API.class);
        /*dateformat1 = "2020-01-20";*/
        Call<ResponseBody> call = api.gettravellingexpensesdetails(service_call_id,customer_id,oem_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        String res = response.body().string();
                        if (res != null) {
                            JSONObject object = new JSONObject(res);
                            JSONArray array = object.getJSONArray("data");
                            ArrayList<SwasCompletedTravelModel> model = new ArrayList<>();
                            if (array.length() != 0) {
                                for (int i = 0; i < array.length(); i++) {
                                    SwasCompletedTravelModel travelModel = new SwasCompletedTravelModel();
                                    JSONObject object1 = array.getJSONObject(i);
                                    travelModel.setTravellingExpensesHeadsID(object1.getString("TravellingExpensesHeadsID"));
                                    travelModel.setServiceCallID(object1.getString("ServiceCallID"));
                                    travelModel.setOEMID(object1.getString("OEMID"));
                                    travelModel.setCustomerID(object1.getString("CustomerID"));
                                    travelModel.setService_Expense_Heads_id(object1.getString("Service_Expense_Heads_id"));
                                    travelModel.setService_Expense_Heads_Name(object1.getString("Service_Expense_Heads_Name"));
                                    travelModel.setVisitDates(object1.getString("VisitDates"));
                                    travelModel.setVisitCharges(object1.getString("VisitCharges"));
                                    travelModel.setCreatedBy(object1.getString("CreatedBy"));
                                    travelModel.setCreatedDate(object1.getString("CreatedDate"));

                                    //txt_travel_service_call_id.setText(object1.getString("ServiceCallID"));
                                    //swasServiceModel .setServicecall_status_id(object1.getString("ServiceCallStatusID"));

                                    model.add(travelModel);
                                }
                                adapter = new SwasCompletedTravelAdapter(Swas_Completed_Travelling_Expenses_Activity.this,model);
                                rv_completed_travelling_expenses.setLayoutManager(new LinearLayoutManager(Swas_Completed_Travelling_Expenses_Activity.this, LinearLayoutManager.VERTICAL, false));
                                rv_completed_travelling_expenses.setAdapter(adapter);


                            } else if (array.length() == 0) {
//                                Toast.makeText(Swas_Completed_Travelling_Expenses_Activity.this,"Sorry there is no data",Toast.LENGTH_SHORT).show();

                            }
                        }
                    }


                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(Swas_Completed_Travelling_Expenses_Activity.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);
        setData();
        // Set content width and height
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(Swas_Completed_Travelling_Expenses_Activity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }

    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(Swas_Completed_Travelling_Expenses_Activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();

                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));
                    oem_id= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout() {
        finish();
        Toast toast= Toast.makeText(Swas_Completed_Travelling_Expenses_Activity.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(Swas_Completed_Travelling_Expenses_Activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);

        //Preferences.Logout();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();


        if (id==R.id.nav_swas)
        {
            Intent i=new Intent(Swas_Completed_Travelling_Expenses_Activity.this, Swas_Home_Actvity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    public void checkNetworkConnection(){
        AlertDialog.Builder builder =new AlertDialog.Builder(Swas_Completed_Travelling_Expenses_Activity.this);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(Swas_Completed_Travelling_Expenses_Activity.this,Swas_Details_Of_Completed_Call.class);
        Bundle bundle = new Bundle();
        bundle.putString("service_call_id",service_call_id);
        bundle.putString("status_id",status_id);
        bundle.putString("Typeofservicecall_id",type_of_service_status_id);
        bundle.putString("customer_id",customer_id);
        i.putExtras(bundle);
        startActivity(i);
        finish();
    }

}
