package com.brainlines.weightloggernewversion.FalcoJK.JKModel;

public class ProdTicketModel {

    String ptkd_PktId,ptkd_TkNo,ptkd_PSKU,ptkd_Plant,ptkd_ValueStreamCode,ptkd_Ftypecode,ptkd_Qty,ptkd_IsRelease,tk_Actual_Start_Date;
    String tk_Actual_End_Date,ptkd_ReleaseDate,ptkd_IsRelease1,ptkd_Active,tk_act_End_Date;

    public String getTk_act_End_Date() {
        return tk_act_End_Date;
    }

    public void setTk_act_End_Date(String tk_act_End_Date) {
        this.tk_act_End_Date = tk_act_End_Date;
    }

    public String getPtkd_PktId() {
        return ptkd_PktId;
    }

    public void setPtkd_PktId(String ptkd_PktId) {
        this.ptkd_PktId = ptkd_PktId;
    }

    public String getPtkd_TkNo() {
        return ptkd_TkNo;
    }

    public void setPtkd_TkNo(String ptkd_TkNo) {
        this.ptkd_TkNo = ptkd_TkNo;
    }

    public String getPtkd_PSKU() {
        return ptkd_PSKU;
    }

    public void setPtkd_PSKU(String ptkd_PSKU) {
        this.ptkd_PSKU = ptkd_PSKU;
    }

    public String getPtkd_Plant() {
        return ptkd_Plant;
    }

    public void setPtkd_Plant(String ptkd_Plant) {
        this.ptkd_Plant = ptkd_Plant;
    }

    public String getPtkd_ValueStreamCode() {
        return ptkd_ValueStreamCode;
    }

    public void setPtkd_ValueStreamCode(String ptkd_ValueStreamCode) {
        this.ptkd_ValueStreamCode = ptkd_ValueStreamCode;
    }

    public String getPtkd_Ftypecode() {
        return ptkd_Ftypecode;
    }

    public void setPtkd_Ftypecode(String ptkd_Ftypecode) {
        this.ptkd_Ftypecode = ptkd_Ftypecode;
    }

    public String getPtkd_Qty() {
        return ptkd_Qty;
    }

    public void setPtkd_Qty(String ptkd_Qty) {
        this.ptkd_Qty = ptkd_Qty;
    }

    public String getPtkd_IsRelease() {
        return ptkd_IsRelease;
    }

    public void setPtkd_IsRelease(String ptkd_IsRelease) {
        this.ptkd_IsRelease = ptkd_IsRelease;
    }

    public String getTk_Actual_Start_Date() {
        return tk_Actual_Start_Date;
    }

    public void setTk_Actual_Start_Date(String tk_Actual_Start_Date) {
        this.tk_Actual_Start_Date = tk_Actual_Start_Date;
    }

    public String getTk_Actual_End_Date() {
        return tk_Actual_End_Date;
    }

    public void setTk_Actual_End_Date(String tk_Actual_End_Date) {
        this.tk_Actual_End_Date = tk_Actual_End_Date;
    }

    public String getPtkd_ReleaseDate() {
        return ptkd_ReleaseDate;
    }

    public void setPtkd_ReleaseDate(String ptkd_ReleaseDate) {
        this.ptkd_ReleaseDate = ptkd_ReleaseDate;
    }

    public String getPtkd_IsRelease1() {
        return ptkd_IsRelease1;
    }

    public void setPtkd_IsRelease1(String ptkd_IsRelease1) {
        this.ptkd_IsRelease1 = ptkd_IsRelease1;
    }

    public String getPtkd_Active() {
        return ptkd_Active;
    }

    public void setPtkd_Active(String ptkd_Active) {
        this.ptkd_Active = ptkd_Active;
    }
}
