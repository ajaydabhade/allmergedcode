package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

public class SwasCompletedMachineModel {
    String MachineParameterLogID,OEMID,CustomerID,ServiceCallID,MachineParameters,ServiceCallStartDate,ServiceCallEndDate,ParameterValue,Remark,CreatedBy,CreatedDate;

    public String getMachineParameterLogID() {
        return MachineParameterLogID;
    }

    public void setMachineParameterLogID(String machineParameterLogID) {
        MachineParameterLogID = machineParameterLogID;
    }

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getServiceCallID() {
        return ServiceCallID;
    }

    public void setServiceCallID(String serviceCallID) {
        ServiceCallID = serviceCallID;
    }

    public String getMachineParameters() {
        return MachineParameters;
    }

    public void setMachineParameters(String machineParameters) {
        MachineParameters = machineParameters;
    }

    public String getServiceCallStartDate() {
        return ServiceCallStartDate;
    }

    public void setServiceCallStartDate(String serviceCallStartDate) {
        ServiceCallStartDate = serviceCallStartDate;
    }

    public String getServiceCallEndDate() {
        return ServiceCallEndDate;
    }

    public void setServiceCallEndDate(String serviceCallEndDate) {
        ServiceCallEndDate = serviceCallEndDate;
    }

    public String getParameterValue() {
        return ParameterValue;
    }

    public void setParameterValue(String parameterValue) {
        ParameterValue = parameterValue;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }
}
