package com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.listener;

import com.brainlines.weightloggernewversion.FalcoJK.NewActivity.NewProductionTicket.Model.NewWIPModel;

public interface NewWipDisplayListener {
    public void getWIPData(NewWIPModel model);

}
