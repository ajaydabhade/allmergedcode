package com.brainlines.weightloggernewversion.adapter.newtestadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.adapter.recalltestadapter.Record_bag_length_list_Adapter;

import java.util.ArrayList;

public class NewTestBagLengthAdapter extends RecyclerView.Adapter<NewTestBagLengthAdapter.MyViewHolder> {

    public String TAG = "WorkDetailsListener";
    Context context;
    ArrayList<String> bag_length_reading = new ArrayList<>();


    public NewTestBagLengthAdapter(Context context, ArrayList<String> bag_length_reading) {
        this.context = context;
        this.bag_length_reading = bag_length_reading;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_bag_length_reading, parent, false);
        return new NewTestBagLengthAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        String reading = bag_length_reading.get(position);
        holder.txt_degree.setText(reading);


    }

    @Override
    public int getItemCount() {
        return bag_length_reading.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_degree,txt_edu_details;
        ImageView img_remove_School;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_degree = (itemView).findViewById(R.id.txt_filter_degree_name);


        }

    }
}
