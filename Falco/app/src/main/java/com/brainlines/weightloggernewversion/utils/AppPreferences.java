package com.brainlines.weightloggernewversion.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.brainlines.weightloggernewversion.database.DataBaseHelper;

public class AppPreferences {

    private static final String FILE_NAME = "weightlogger.shapref";

    private static final String UserName = "username";
    private static final String Password = "password";
    private static final String EMAIL = "email";
    private static final String MARKETSANDSCRIPTS = "markets_and_scripts";
    private static final String ROLE = "role";
    private static final String CurrentTagId = "current_tag_id";
    private static final String DATABSE_NAME = "dbName";


    public static void setUser(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(FILE_NAME,Context.MODE_PRIVATE);
        preferences.edit().putString(UserName,value).commit();
    }

    public static String getUser(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(FILE_NAME,Context.MODE_PRIVATE);
        return preferences.getString(UserName,"");
    }
    public static void setPassword(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(Password, value).commit();
    }

    public static String getPassword(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(Password, "");
    }

    public static void setCurrentTagId(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(FILE_NAME,Context.MODE_PRIVATE);
        preferences.edit().putString(CurrentTagId,value).commit();
    }

    public static String getCurrentTagId(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(FILE_NAME,Context.MODE_PRIVATE);
        return preferences.getString(CurrentTagId,"");
    }

    public static void setDatabseName(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(FILE_NAME,Context.MODE_PRIVATE);
        preferences.edit().putString(DATABSE_NAME,value).commit();
    }

    public static String getDatabseName(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(FILE_NAME,Context.MODE_PRIVATE);
        return preferences.getString(DATABSE_NAME,"");
    }

}
