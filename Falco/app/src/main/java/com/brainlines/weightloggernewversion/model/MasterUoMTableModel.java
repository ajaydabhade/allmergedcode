package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MasterUoMTableModel implements Parcelable {
    String uomId;
    int oemId;
    String productType;
    String productUOM;
    boolean isActive;
    boolean isDeleted;
    String createdDate;
    String modifiedDate;
    String createdBy;
    String modifiedBy;

    public MasterUoMTableModel(String uomId, int oemId, String productType, String productUOM, boolean isActive, boolean isDeleted, String createdDate, String modifiedDate, String createdBy, String modifiedBy) {
        this.uomId = uomId;
        this.oemId = oemId;
        this.productType = productType;
        this.productUOM = productUOM;
        this.isActive = isActive;
        this.isDeleted = isDeleted;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.createdBy = createdBy;
        this.modifiedBy = modifiedBy;
    }

    protected MasterUoMTableModel(Parcel in) {
        uomId = in.readString();
        oemId = in.readInt();
        productType = in.readString();
        productUOM = in.readString();
        isActive = in.readByte() != 0;
        isDeleted = in.readByte() != 0;
        createdDate = in.readString();
        modifiedDate = in.readString();
        createdBy = in.readString();
        modifiedBy = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(uomId);
        dest.writeInt(oemId);
        dest.writeString(productType);
        dest.writeString(productUOM);
        dest.writeByte((byte) (isActive ? 1 : 0));
        dest.writeByte((byte) (isDeleted ? 1 : 0));
        dest.writeString(createdDate);
        dest.writeString(modifiedDate);
        dest.writeString(createdBy);
        dest.writeString(modifiedBy);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MasterUoMTableModel> CREATOR = new Creator<MasterUoMTableModel>() {
        @Override
        public MasterUoMTableModel createFromParcel(Parcel in) {
            return new MasterUoMTableModel(in);
        }

        @Override
        public MasterUoMTableModel[] newArray(int size) {
            return new MasterUoMTableModel[size];
        }
    };

    public String getUomId() {
        return uomId;
    }

    public void setUomId(String uomId) {
        this.uomId = uomId;
    }

    public int getOemId() {
        return oemId;
    }

    public void setOemId(int oemId) {
        this.oemId = oemId;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductUOM() {
        return productUOM;
    }

    public void setProductUOM(String productUOM) {
        this.productUOM = productUOM;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}
