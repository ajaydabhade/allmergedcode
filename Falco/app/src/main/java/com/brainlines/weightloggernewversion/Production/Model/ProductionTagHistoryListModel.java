package com.brainlines.weightloggernewversion.Production.Model;

public class ProductionTagHistoryListModel {

    String p_tag_string,product,gm_pouch,total_weight,no_of_pouches,batch_id;

    public String getBatch_id() {
        return batch_id;
    }

    public void setBatch_id(String batch_id) {
        this.batch_id = batch_id;
    }

    public String getP_tag_string() {
        return p_tag_string;
    }

    public void setP_tag_string(String p_tag_string) {
        this.p_tag_string = p_tag_string;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getGm_pouch() {
        return gm_pouch;
    }

    public void setGm_pouch(String gm_pouch) {
        this.gm_pouch = gm_pouch;
    }

    public String getTotal_weight() {
        return total_weight;
    }

    public void setTotal_weight(String total_weight) {
        this.total_weight = total_weight;
    }

    public String getNo_of_pouches() {
        return no_of_pouches;
    }

    public void setNo_of_pouches(String no_of_pouches) {
        this.no_of_pouches = no_of_pouches;
    }
}
