package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class WeightTestResultsDataModel implements Parcelable {

    String TestWeightID;
    String OEMID;
    String MacID;
    String LoggerID;
    String TagID;
    String Weight;
    String OINumber;
    String ModifiedDate;

    public WeightTestResultsDataModel(String testWeightID, String OEMID, String macID, String loggerID, String tagID, String weight, String oinum, String modifiedDate) {
        TestWeightID = testWeightID;
        this.OEMID = OEMID;
        MacID = macID;
        LoggerID = loggerID;
        TagID = tagID;
        Weight = weight;
        OINumber = oinum;
        ModifiedDate = modifiedDate;
    }

    protected WeightTestResultsDataModel(Parcel in) {
        TestWeightID = in.readString();
        OEMID = in.readString();
        MacID = in.readString();
        LoggerID = in.readString();
        TagID = in.readString();
        Weight = in.readString();
        OINumber = in.readString();
        ModifiedDate = in.readString();
    }

    public static final Creator<WeightTestResultsDataModel> CREATOR = new Creator<WeightTestResultsDataModel>() {
        @Override
        public WeightTestResultsDataModel createFromParcel(Parcel in) {
            return new WeightTestResultsDataModel(in);
        }

        @Override
        public WeightTestResultsDataModel[] newArray(int size) {
            return new WeightTestResultsDataModel[size];
        }
    };

    public String getTestWeightID() {
        return TestWeightID;
    }

    public void setTestWeightID(String testWeightID) {
        TestWeightID = testWeightID;
    }

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getMacID() {
        return MacID;
    }

    public void setMacID(String macID) {
        MacID = macID;
    }

    public String getLoggerID() {
        return LoggerID;
    }

    public void setLoggerID(String loggerID) {
        LoggerID = loggerID;
    }

    public String getTagID() {
        return TagID;
    }

    public void setTagID(String tagID) {
        TagID = tagID;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    public String getOinum() {
        return OINumber;
    }

    public void setOinum(String oinum) {
        OINumber = oinum;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(TestWeightID);
        dest.writeString(OEMID);
        dest.writeString(MacID);
        dest.writeString(LoggerID);
        dest.writeString(TagID);
        dest.writeString(Weight);
        dest.writeString(OINumber);
        dest.writeString(ModifiedDate);
    }
}
