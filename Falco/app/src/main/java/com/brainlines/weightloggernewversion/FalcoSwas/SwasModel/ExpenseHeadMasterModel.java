package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

import android.os.Parcel;
import android.os.Parcelable;

public class ExpenseHeadMasterModel implements Parcelable {

    String Service_Expense_Heads_id;
    String Service_Expense_Heads_Name;

    public ExpenseHeadMasterModel(String service_Expense_Heads_id, String service_Expense_Heads_Name) {
        Service_Expense_Heads_id = service_Expense_Heads_id;
        Service_Expense_Heads_Name = service_Expense_Heads_Name;
    }

    protected ExpenseHeadMasterModel(Parcel in) {
        Service_Expense_Heads_id = in.readString();
        Service_Expense_Heads_Name = in.readString();
    }

    public static final Creator<ExpenseHeadMasterModel> CREATOR = new Creator<ExpenseHeadMasterModel>() {
        @Override
        public ExpenseHeadMasterModel createFromParcel(Parcel in) {
            return new ExpenseHeadMasterModel(in);
        }

        @Override
        public ExpenseHeadMasterModel[] newArray(int size) {
            return new ExpenseHeadMasterModel[size];
        }
    };

    public String getService_Expense_Heads_id() {
        return Service_Expense_Heads_id;
    }

    public void setService_Expense_Heads_id(String service_Expense_Heads_id) {
        Service_Expense_Heads_id = service_Expense_Heads_id;
    }

    public String getService_Expense_Heads_Name() {
        return Service_Expense_Heads_Name;
    }

    public void setService_Expense_Heads_Name(String service_Expense_Heads_Name) {
        Service_Expense_Heads_Name = service_Expense_Heads_Name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Service_Expense_Heads_id);
        dest.writeString(Service_Expense_Heads_Name);
    }
}
