package com.brainlines.weightloggernewversion.FalcoSwas.SwasAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.Checklist_Model;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class Chekc_List_Adapter extends RecyclerView.Adapter<Chekc_List_Adapter.MyViewHolder> {
    private LayoutInflater inflater;
    private ArrayList<Checklist_Model> checklist_modelArrayList;
    View.OnClickListener mClickListener;
    Context context;

    public Chekc_List_Adapter(Context ctx, ArrayList<Checklist_Model> checklist_modelArrayList) {
        inflater = LayoutInflater.from(ctx);
        this.checklist_modelArrayList = checklist_modelArrayList;
        this.context = ctx;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_swas_site_readiness_chekclist, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Checklist_Model model = checklist_modelArrayList.get(position);
        if (model!=null) {

            holder.item_check_name.setText(model.getCheck_name());
            if (model.getChecked().equals("Y"))
            {
                holder.item_check_box.setChecked(true);
            }
           holder.item_check_box.setEnabled(false);
            //holder.itemView.setEnabled(false);
            //holder.item_check_name.setEnabled(false);
           // holder.item_txt_customer_name.setText(model.getLOCATIONNAME());

        }
    }

    @Override
    public int getItemCount() {
        return checklist_modelArrayList.size();
    }

    class MyViewHolder  extends RecyclerView.ViewHolder{
        TextView item_check_name;
        CheckBox item_check_box;

        public MyViewHolder(View itemView) {
            super(itemView);
            item_check_name = itemView.findViewById(R.id.txt_checklist_name);
            item_check_box = itemView.findViewById(R.id.check_site_readiness);


        }
    }
}
