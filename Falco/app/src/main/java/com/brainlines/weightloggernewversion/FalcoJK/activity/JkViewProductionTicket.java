package com.brainlines.weightloggernewversion.FalcoJK.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.ProdTkOperationModel;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.SkuDetailsModel;
import com.brainlines.weightloggernewversion.FalcoJK.Listeners.JKProductionUpdateListener;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.NewProViewTicketAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.ProductionViewAdapter;
import com.brainlines.weightloggernewversion.FalcoJK.MainAdapter.SKU_Adapter;
import com.brainlines.weightloggernewversion.FalcoJK.UrlConstants.JK_URL_Constants;
import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.UserModel;
import com.brainlines.weightloggernewversion.FalcoSwas.URL_Constants;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JkViewProductionTicket extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, JKProductionUpdateListener {

    TextView txt_view_ticket_no,txt_view_plant,txt_view_tk_filetype,txt_view_tk_qty,txt_view_sku_8,txt_view_sku_13,txt_view_startdate,txt_updated_griddate;
    TextView txt_view_enddate,txt_view_act_strat_date,txt_view_act_end_date,txt_view_value_stream;
    RecyclerView rv_prod_view_ticket;
    ImageView img_back,img_userinfo,img_view_sku_details,nav_view_img;
    DrawerLayout drawer;;
    String user_email,user_role,user_token;
    ProductionViewAdapter adapter;
    String blackodizing,tangcolor,stamptype,stampchart;
    String str_ticketno,str_tkid,strplant,str_file_type,str_tk_qty,str_sku_8,str_sku_13,str_start_Date,str_act_start_date,str_act_end_date,str_start_date,str_end_date;
    RelativeLayout relativelayout_sku;
    TextView stamp_type,stamp_chart,sku_blackodizing,sku_tang_color,sku_no;
    NewProViewTicketAdapter newTicketListAdapter;
    ArrayList<ProdTkOperationModel> getTicketList = new ArrayList<>();
    SwipeRefreshLayout swipeRefreshLayout;
    ImageButton next_nav,previous_nav;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
    String dateformat1 = "";
    Calendar c = Calendar.getInstance();
    SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
    private TextView profile_user,profile_role;
    RecyclerView rv_sku_details;
    SKU_Adapter sku_adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actviyt_production_ticket_view_navigation);
        initUi();
        setData();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profile_user = (TextView) headerView.findViewById(R.id.profile_user);
        rv_sku_details = (RecyclerView)findViewById(R.id.rv_sku_details);
        profile_user.setText(user_email);
        profile_role = (TextView)headerView.findViewById(R.id.profile_role);
        profile_role.setText(user_role);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_swas).setVisible(false);
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view_img=(ImageView)findViewById(R.id.nav_view_img);
        swipeRefreshLayout=(SwipeRefreshLayout)findViewById(R.id.swipeto_refresh);
        next_nav = (ImageButton) findViewById(R.id.next_nav);
        previous_nav = (ImageButton)findViewById(R.id.previous_nav);

        Calendar c = Calendar.getInstance();

        String date = sdf.format(c.getTime());
        String[] string =date.split(" ");
        String date1=string[0];
        txt_updated_griddate.setText(date);
        nav_view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START); //use drawer from parent activity
            }


        });
        Bundle bundle=getIntent().getExtras();
        str_tkid=bundle.getString("ptk_tk_id");
        str_ticketno=bundle.getString("tk_no");
        strplant=bundle.getString("tk_plant");
        str_tk_qty=bundle.getString("tk_quantity");
        str_sku_8=bundle.getString("tk_sku_8");

        txt_view_plant.setText(strplant);
        txt_view_ticket_no.setText(str_ticketno);
        txt_view_tk_qty.setText(str_tk_qty.concat(" no"));
        txt_view_sku_8.setText(str_sku_8);

        date = sdf.format(c.getTime());
        txt_updated_griddate.setText(date);
        dateformat1 = sdf.format(c.getTime());

        getViewData();
        new GetSkuOfTickets().execute();

        img_back=(ImageView)findViewById(R.id.img_back);
        img_userinfo=(ImageView)findViewById(R.id.img_userinfo);

        img_userinfo.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                displayPopupWindow(view);
            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(JkViewProductionTicket.this, JK_Production_Selection_list.class);
                startActivity(i);
            }
        });
        img_view_sku_details.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if(relativelayout_sku.getVisibility()==View.GONE)
                {
                    relativelayout_sku.setVisibility(View.VISIBLE);
                }
                else
                {
                    relativelayout_sku.setVisibility(View.GONE);
                }
            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getViewData();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        next_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String date = txt_updated_griddate.getText().toString();
                /*SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMMyyyy");*/
                Date convertedDate = new Date();
                try {
                    convertedDate = sdf.parse(date);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                System.out.println(convertedDate);
                Calendar cal = Calendar.getInstance();
                cal.setTime(convertedDate);
                cal.add(Calendar.DAY_OF_MONTH, 1);

                String previous_date= sdf.format(cal.getTime());
                dateformat1 = previous_date;
                txt_updated_griddate.setText(previous_date);

                getViewData();


            }
        });
        previous_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String date = txt_updated_griddate.getText().toString();
                /*SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMMyyyy");*/
                Date convertedDate = new Date();
                try {
                    convertedDate = sdf.parse(date);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                System.out.println(convertedDate);
                Calendar cal = Calendar.getInstance();
                cal.setTime(convertedDate);
                cal.add(Calendar.DAY_OF_MONTH, -1);

                String previous_date= sdf.format(cal.getTime());
                dateformat1 = previous_date;
                txt_updated_griddate.setText(previous_date);
                getViewData();

            }
        });
    }

    public void initUi()
    {
        txt_view_ticket_no=(TextView)findViewById(R.id.txt_view_ticket_no);
        txt_view_plant=(TextView)findViewById(R.id.txt_view_plant);
        txt_view_tk_filetype=(TextView)findViewById(R.id.txt_view_tk_filetype);
        txt_view_tk_qty=(TextView)findViewById(R.id.txt_view_tk_qty);
        txt_view_sku_8=(TextView)findViewById(R.id.txt_view_sku_8);
        // txt_view_sku_13=(TextView)findViewById(R.id.txt_view_sku_13);
        txt_view_startdate=(TextView)findViewById(R.id.txt_view_startdate);
        txt_view_enddate=(TextView)findViewById(R.id.txt_view_enddate);
        txt_view_act_strat_date=(TextView)findViewById(R.id.txt_view_act_strat_date);
        txt_view_act_end_date=(TextView)findViewById(R.id.txt_view_act_end_date);
        img_view_sku_details=(ImageView)findViewById(R.id.view_sku_details);
        txt_updated_griddate=(TextView)findViewById(R.id.txt_updated_griddate);
        txt_view_value_stream=(TextView)findViewById(R.id.txt_view_valuestream);

        rv_prod_view_ticket=(RecyclerView) findViewById(R.id.rv_view_pro_ticket);
        relativelayout_sku=(RelativeLayout)findViewById(R.id.relativelayout_sku);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(JkViewProductionTicket.this);
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popup.setContentView(layout);
        setData();
        // Set content width and height
        TextView profile_user=(TextView)popup.getContentView().findViewById(R.id.profile_user);
        TextView profile_role=(TextView)popup.getContentView().findViewById(R.id.profile_role);
        ImageButton img_logout=(ImageButton)popup.getContentView().findViewById(R.id.img_logout);
        ImageView img_cancel=(ImageView)popup.getContentView().findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                Intent i=new Intent(JkViewProductionTicket.this, LoginActivity.class);
                startActivity(i);
            }
        });

        profile_user.setText(user_email);
        profile_role.setText(user_role);


        popup.setOutsideTouchable(true);
        popup.setOverlapAnchor(false);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }


    public void setData() {
        DataBaseHelper dbHelper = new DataBaseHelper(JkViewProductionTicket.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UserModel model = new UserModel();

                    user_email = cursor.getString(cursor.getColumnIndex(Constants.USER_EMAILID));
                    user_token = cursor.getString(cursor.getColumnIndex(Constants.USER_TOKEN));
                    user_role = cursor.getString(cursor.getColumnIndex(Constants.USER_ROLE_NAME));
                    String user_oemid= cursor.getString(cursor.getColumnIndex(Constants.USER_OEMID));


                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    public void logout()
    {
        finish();
        Toast toast= Toast.makeText(JkViewProductionTicket.this,
                "Successfully logout", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        DataBaseHelper dbHelper = new DataBaseHelper(JkViewProductionTicket.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_USER_DETAILS,null,null);

        //Preferences.Logout();

    }

    @Override
    public void imgRouteIsClicked(boolean flag, ProdTkOperationModel model) {

    }

    @Override
    public void imgSubmitButtonIsClicked(ArrayList<ProdTkOperationModel> ticketList) {

    }

    @Override
    public void getOkValue(String str_ok, String rejectqty, String secondqty, String str_wip) {

    }

    @Override
    public void checktotalokqty(String totalqty, String str_ok, String rejectqty, String secondqty, String str_wip) {

    }


    public class GetSkuOfTickets extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(JkViewProductionTicket.this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response="";
            String url= JK_URL_Constants.MAIN_JK_URL + JK_URL_Constants.GETSKUBLACKIDIZING + str_tkid ;
            response= URL_Constants.makeHttpPostRequest(url);
            Log.d(JK_URL_Constants.TAG, "Whole data = " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (!response.equals("ERROR"))
            {
                try {

                    JSONArray array=new JSONArray(response);
                    ArrayList<SkuDetailsModel> skuDetailsModelArrayList = new ArrayList<>();
                    if (array.length()==0){
                       /* Toast toast = Toast.makeText(getApplicationContext(),
                                "Sorry there is no data for this plant", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();*/
                    }
                    else
                    {
                        for (int i=0;i<array.length();i++)
                        {
                            JSONObject object=new JSONObject(array.get(i).toString());
                            SkuDetailsModel model = new SkuDetailsModel();
                            model.setSKU_No(object.getString("SKU_No"));
                            model.setDp_stamp_type(object.getString("dp_stamp_type"));
                            model.setDp_stamp_chart(object.getString("dp_stamp_chart"));
                            model.setDp_blko_flg(object.getString("dp_blko_flg"));
                            model.setDp_tang_color(object.getString("dp_tang_color"));
                            model.setQTY(object.getString("QTY"));
                            skuDetailsModelArrayList.add(model);

                         /*   String sku_13=object.getString("SKU_No");
                            blackodizing=object.getString("dp_blko_flg");
                            tangcolor=object.getString("dp_tang_color");
                            stampchart=object.getString("dp_stamp_chart");
                            stamptype=object.getString("dp_stamp_type");
                            txt_view_sku_13.setText(sku_13);

                            stamp_type.setText(stamptype); stamp_chart.setText(stampchart);
                            sku_blackodizing.setText(blackodizing); sku_tang_color.setText(tangcolor);
                            sku_no.setText(sku_13);*/
                        }
                        LinearLayoutManager manager = new LinearLayoutManager(JkViewProductionTicket.this);
                        rv_sku_details.setLayoutManager(manager);
                        sku_adapter = new SKU_Adapter(JkViewProductionTicket.this,skuDetailsModelArrayList);
                        rv_sku_details.setAdapter(sku_adapter);


                    }

                }
                catch (Exception e)
                {
                    e.printStackTrace();}
            }

        }
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id== R.id.nav_planning)
        {
            Intent i=new Intent(JkViewProductionTicket.this, FalcoJKHomeActivity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

   /* private void getViewData() {
        API api = Retroconfig.retrofit().create(API.class);
        Call<ResponseBody> call = api.getViewData(str_tkid,dateformat1,str_sku_8);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String res = response.body().string();
                    if (res!=null){
                        JSONArray array = null;
                        try {
                            array = new JSONArray(res);
                            getTicketList.clear();
                            if (array.length()!=0){
                                for (int i = 0 ; i < array.length() ; i++){
                                    JSONObject obj = array.getJSONObject(i);
                                    ProdTkOperationModel model = new ProdTkOperationModel();
                                    //model.setPtk_Id(obj.getString("ptkd_Id"));
                                    model.setPtk_PtkdId(obj.getInt("ptk_PtkdId"));
                                    model.setPtk_TkNo(obj.getString("ptk_TkNo"));
                                    model.setPtk_Plant(obj.getString("ptkd_Plant"));
                                    model.setVsCode(obj.getString("ptk_ValueStream"));
                                    model.setPtk_Operation(obj.getInt("ptk_Operation"));
                                    model.setPtk_UpdateDate(obj.getString("ptk_UpdateDate"));
                                    model.setPtk_OkQty(obj.getInt("ptk_OkQty"));
                                    model.setPtk_RejectQty(obj.getInt("ptk_ReworkQty"));
                                    model.setPtk_ReworkQty(obj.getInt("ptk_RejectQty"));
                                    model.setPtk_WIPQty(obj.getInt("ptk_WIPQty"));
                                    model.setPtk_SecondQty(obj.getInt("ptk_SecondQty"));
                                    model.setPtk_TotalQty(obj.getString("ptkd_Qty"));
                                    model.setOpr_operation_name(obj.getString("opr_operation_name"));
                                    model.setPtkd_Qty(obj.getInt("ptkd_Qty"));
                                    model.setUni_unit_name(obj.getString("uni_unit_name"));
                                    model.setVal_valuestream_name(obj.getString("val_valuestream_name"));
                                    //model.setFt_ftype_desc(obj.getString("ft_ftype_desc"));
                                    model.setTk_Actual_Start_Date(obj.getString("tk_Actual_Start_Date"));
                                    model.setTk_Actual_End_Date(obj.getString("tk_Actual_End_Date"));
                                    model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                                    model.setTk_End_Date(obj.getString("tk_End_Date"));

                                    txt_view_act_strat_date.setText(obj.getString("tk_Actual_Start_Date"));
                                    txt_view_act_end_date.setText(obj.getString("tk_Actual_End_Date"));
                                    txt_view_startdate.setText(obj.getString("tk_Start_Date"));
                                    txt_view_enddate.setText(obj.getString("tk_End_Date"));

                                    getTicketList.add(model);
                                }
                            }

                            else if (array.length()==0)
                            {
                                getTicketList.clear();
                                rv_prod_view_ticket.setAdapter(null);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        ArrayList<ProdTkOperationModel> list = getTicketList;
                        LinearLayoutManager manager = new LinearLayoutManager(JkViewProductionTicket.this);
                        rv_prod_view_ticket.setLayoutManager(manager);
                        newTicketListAdapter = new NewProViewTicketAdapter(JkViewProductionTicket.this,getTicketList,JkViewProductionTicket.this);
                        rv_prod_view_ticket.setAdapter(newTicketListAdapter);
                    }else {

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(JkViewProductionTicket.this, "Failed", Toast.LENGTH_SHORT).show();
                Log.d("", "onFailure: Failed");
            }
        });

    }*/

    private void getViewData() {

        API api = Retroconfig.retrofit().create(API.class);
        // dateformat1 = "2020-02-07";
        //dateformat1 = sdf1.format(dateformat1);
        Call<ResponseBody> call = api.getViewData(str_tkid,dateformat1,str_sku_8);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if(response.body()!=null)
                    {
                        String res = response.body().string();
                        if (res!=null){
                            getTicketList.clear();
                            JSONObject object = new JSONObject(res);

                            JSONArray array = object.getJSONArray("Data");

                            if (array.length()!=0){

                                for (int i = 0 ; i < array.length() ; i++){
                                    JSONObject obj = array.getJSONObject(i);
                                    ProdTkOperationModel model = new ProdTkOperationModel();

                                    model.setPtk_PtkdId(obj.getInt("ptk_PtkdId"));
                                    model.setPtkd_TkNo(obj.getString("ptk_TkNo"));
                                    model.setPtkd_Plant(obj.getString("ptkd_Plant"));
                                    model.setVsCode(obj.getString("ptk_ValueStream"));
                                    model.setPtkd_Operation(obj.getInt("ptk_Operation"));
                                    model.setPtk_UpdateDate(obj.getString("ptk_UpdateDate"));
                                    model.setPtk_OkQty(obj.getInt("ptk_OkQty"));
                                    model.setPtk_RejectQty(obj.getInt("ptk_RejectQty"));
                                    model.setPtk_ReworkQty(obj.getInt("ptk_ReworkQty"));
                                    model.setPtk_WIPQty(obj.getInt("ptk_WIPQty"));
                                    model.setPtk_SecondQty(obj.getInt("ptk_SecondQty"));
                                    model.setPtk_TotalQty(obj.getString("ptkd_Qty"));
                                    model.setOpr_operation_name(obj.getString("opr_operation_name"));
                                    model.setPtkd_Qty(obj.getInt("ptkd_Qty"));
                                    model.setUni_unit_name(obj.getString("uni_unit_name"));
                                    model.setVal_valuestream_name(obj.getString("val_valuestream_name"));
                                    model.setTk_Actual_Start_Date(obj.getString("tk_Actual_Start_Date"));
                                    if(obj.has("tk_Actual_End_Date")) {
                                        model.setTk_Actual_End_Date(obj.getString("tk_Actual_End_Date"));
                                        txt_view_act_end_date.setText(obj.getString("tk_Actual_End_Date"));
                                        if (!obj.getString("tk_Actual_End_Date").equals("null")) {
                                            txt_view_act_end_date.setText(obj.getString("tk_Actual_End_Date"));
                                        } else {
                                            txt_view_act_end_date.setText("NA");

                                        }
                                    }

                                    model.setTk_Start_Date(obj.getString("tk_Start_Date"));
                                    model.setTk_End_Date(obj.getString("tk_End_Date"));
                                    model.setPtk_ChangeRoute_Flag(obj.getString("ptk_ChangeRoute_Flag"));

                                    txt_view_act_strat_date.setText(obj.getString("tk_Actual_Start_Date"));
                                    txt_view_startdate.setText(obj.getString("tk_Start_Date"));
                                    txt_view_enddate.setText(obj.getString("tk_End_Date"));
                                    txt_view_value_stream.setText(obj.getString("val_valuestream_name"));
                                    txt_updated_griddate.setText(obj.getString("tk_Actual_Start_Date"));

                                    getTicketList.add(model);


                                }
                                ArrayList<ProdTkOperationModel> list = getTicketList;
                                LinearLayoutManager manager = new LinearLayoutManager(JkViewProductionTicket.this);
                                rv_prod_view_ticket.setLayoutManager(manager);
                                newTicketListAdapter = new NewProViewTicketAdapter(JkViewProductionTicket.this,getTicketList, JkViewProductionTicket.this);
                                rv_prod_view_ticket.setAdapter(newTicketListAdapter);

                            }
                            else if (array.length()==0)
                            {
                                getTicketList.clear();
                                rv_prod_view_ticket.setAdapter(null);
                            }

                        }
                    }


                }
                catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }




}
