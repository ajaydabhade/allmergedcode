package com.brainlines.weightloggernewversion.FalcoSwas.SwasListener;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.DocumentModel;

public interface UploadImageDisplayListener {

    void displayImage(DocumentModel model);

}
