package com.brainlines.weightloggernewversion.FalcoJK.Listeners;
import com.brainlines.weightloggernewversion.FalcoJK.JKModel.ProdTkOperationModel;

import java.util.ArrayList;

public interface JKProductionUpdateListener {

    void imgRouteIsClicked(boolean flag, ProdTkOperationModel model);

    void imgSubmitButtonIsClicked(ArrayList<ProdTkOperationModel> ticketList);


    void getOkValue(String str_ok, String rejectqty, String secondqty, String str_wip);

    void checktotalokqty(String totalqty, String str_ok, String rejectqty, String secondqty, String str_wip);

}
