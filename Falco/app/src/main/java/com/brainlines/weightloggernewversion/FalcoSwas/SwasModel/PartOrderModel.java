package com.brainlines.weightloggernewversion.FalcoSwas.SwasModel;

import android.os.Parcel;
import android.os.Parcelable;

public class PartOrderModel implements Parcelable {

    String itemId;
    String name;
    String quantity;
    String price;

    public PartOrderModel(String itemId, String name, String quantity, String price) {
        this.itemId = itemId;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    protected PartOrderModel(Parcel in) {
        itemId = in.readString();
        name = in.readString();
        quantity = in.readString();
        price = in.readString();
    }

    public static final Creator<PartOrderModel> CREATOR = new Creator<PartOrderModel>() {
        @Override
        public PartOrderModel createFromParcel(Parcel in) {
            return new PartOrderModel(in);
        }

        @Override
        public PartOrderModel[] newArray(int size) {
            return new PartOrderModel[size];
        }
    };

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(itemId);
        dest.writeString(name);
        dest.writeString(quantity);
        dest.writeString(price);
    }
}
