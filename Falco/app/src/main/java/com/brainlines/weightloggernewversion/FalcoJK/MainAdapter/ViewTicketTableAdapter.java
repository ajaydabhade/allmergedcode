package com.brainlines.weightloggernewversion.FalcoJK.MainAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.FalcoJK.JKModel.PacketTicketTrackingModel;
import com.brainlines.weightloggernewversion.R;

import java.util.ArrayList;

public class ViewTicketTableAdapter extends RecyclerView.Adapter<ViewTicketTableAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<PacketTicketTrackingModel> ticket_list;
    View.OnClickListener mClickListener;
    Context context;
    String str_tk_no;

    public ViewTicketTableAdapter(Context ctx, ArrayList<PacketTicketTrackingModel> ticket_list){
        inflater = LayoutInflater.from(ctx);
        this.ticket_list = ticket_list;
        this.context=ctx;
    }

    @Override
    public int getItemCount() {
        return ticket_list.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_recycler_view_pack_tickets_data, parent, false);
        final MyViewHolder holder = new MyViewHolder(view);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onClick(view);

            }
        });


        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int i) {
        holder.txt_date.setText(ticket_list.get(i).getTt_Date());
        holder.txt_compl_qty.setText(ticket_list.get(i).getTt_Qty());
        holder.txt_pending_qty.setText(ticket_list.get(i).getTt_PendingQty());


    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView txt_date,txt_compl_qty,txt_pending_qty;


        public MyViewHolder(View itemView) {
            super(itemView);
            txt_date= itemView.findViewById(R.id.txtdate);
            txt_compl_qty= itemView.findViewById(R.id.txtcompletedqty1);
            txt_pending_qty= itemView.findViewById(R.id.txtpendingqty1);


        }
    }


}
