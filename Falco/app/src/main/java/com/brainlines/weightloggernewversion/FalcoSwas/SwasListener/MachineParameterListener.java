package com.brainlines.weightloggernewversion.FalcoSwas.SwasListener;

import com.brainlines.weightloggernewversion.FalcoSwas.SwasModel.SwasCompletedCallMachineParameterModel;

public interface MachineParameterListener {
    void MachineParameterListener(SwasCompletedCallMachineParameterModel model);
}
