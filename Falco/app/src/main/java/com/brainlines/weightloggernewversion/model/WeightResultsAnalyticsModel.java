package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class WeightResultsAnalyticsModel implements Parcelable {

    String WeightResultsAnalyticsID;
    String OEMID;
    String LoggerID;
    String TagID;
    String MinReading;
    String MaxReading;
    String RangeReading;
    String MeanReading;
    String SDReading;
    String Remarks;
    String RunStatus;
    String ModifiedDate;
    String GM;
    String GMPercent;
    String GMSD;
    String GMSDPercent;
    String AverageGiveAway;

    public WeightResultsAnalyticsModel(String weightResultsAnalyticsID, String OEMID, String loggerID, String tagID, String minReading, String maxReading, String rangeReading, String meanReading, String SDReading, String remarks, String runStatus, String modifiedDate, String GM, String GMPercent, String GMSD, String GMSDPercent, String averageGiveAway) {
        WeightResultsAnalyticsID = weightResultsAnalyticsID;
        this.OEMID = OEMID;
        LoggerID = loggerID;
        TagID = tagID;
        MinReading = minReading;
        MaxReading = maxReading;
        RangeReading = rangeReading;
        MeanReading = meanReading;
        this.SDReading = SDReading;
        Remarks = remarks;
        RunStatus = runStatus;
        ModifiedDate = modifiedDate;
        this.GM = GM;
        this.GMPercent = GMPercent;
        this.GMSD = GMSD;
        this.GMSDPercent = GMSDPercent;
        this.AverageGiveAway = averageGiveAway;
    }

    protected WeightResultsAnalyticsModel(Parcel in) {
        WeightResultsAnalyticsID = in.readString();
        OEMID = in.readString();
        LoggerID = in.readString();
        TagID = in.readString();
        MinReading = in.readString();
        MaxReading = in.readString();
        RangeReading = in.readString();
        MeanReading = in.readString();
        SDReading = in.readString();
        Remarks = in.readString();
        RunStatus = in.readString();
        ModifiedDate = in.readString();
        GM = in.readString();
        GMPercent = in.readString();
        GMSD = in.readString();
        GMSDPercent = in.readString();
        AverageGiveAway = in.readString();
    }

    public static final Creator<WeightResultsAnalyticsModel> CREATOR = new Creator<WeightResultsAnalyticsModel>() {
        @Override
        public WeightResultsAnalyticsModel createFromParcel(Parcel in) {
            return new WeightResultsAnalyticsModel(in);
        }

        @Override
        public WeightResultsAnalyticsModel[] newArray(int size) {
            return new WeightResultsAnalyticsModel[size];
        }
    };

    public String getWeightResultsAnalyticsID() {
        return WeightResultsAnalyticsID;
    }

    public void setWeightResultsAnalyticsID(String weightResultsAnalyticsID) {
        WeightResultsAnalyticsID = weightResultsAnalyticsID;
    }

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getLoggerID() {
        return LoggerID;
    }

    public void setLoggerID(String loggerID) {
        LoggerID = loggerID;
    }

    public String getTagID() {
        return TagID;
    }

    public void setTagID(String tagID) {
        TagID = tagID;
    }

    public String getMinReading() {
        return MinReading;
    }

    public void setMinReading(String minReading) {
        MinReading = minReading;
    }

    public String getMaxReading() {
        return MaxReading;
    }

    public void setMaxReading(String maxReading) {
        MaxReading = maxReading;
    }

    public String getRangeReading() {
        return RangeReading;
    }

    public void setRangeReading(String rangeReading) {
        RangeReading = rangeReading;
    }

    public String getMeanReading() {
        return MeanReading;
    }

    public void setMeanReading(String meanReading) {
        MeanReading = meanReading;
    }

    public String getSDReading() {
        return SDReading;
    }

    public void setSDReading(String SDReading) {
        this.SDReading = SDReading;
    }

    public String getRemarks() {
        return Remarks;
    }

    public void setRemarks(String remarks) {
        Remarks = remarks;
    }

    public String getRunStatus() {
        return RunStatus;
    }

    public void setRunStatus(String runStatus) {
        RunStatus = runStatus;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public String getGM() {
        return GM;
    }

    public void setGM(String GM) {
        this.GM = GM;
    }

    public String getGMPercent() {
        return GMPercent;
    }

    public void setGMPercent(String GMPercent) {
        this.GMPercent = GMPercent;
    }

    public String getGMSD() {
        return GMSD;
    }

    public void setGMSD(String GMSD) {
        this.GMSD = GMSD;
    }

    public String getGMSDPercent() {
        return GMSDPercent;
    }

    public void setGMSDPercent(String GMSDPercent) {
        this.GMSDPercent = GMSDPercent;
    }

    public String getAverageGiveAway() {
        return AverageGiveAway;
    }

    public void setAverageGiveAway(String averageGiveAway) {
        AverageGiveAway = averageGiveAway;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(WeightResultsAnalyticsID);
        dest.writeString(OEMID);
        dest.writeString(LoggerID);
        dest.writeString(TagID);
        dest.writeString(MinReading);
        dest.writeString(MaxReading);
        dest.writeString(RangeReading);
        dest.writeString(MeanReading);
        dest.writeString(SDReading);
        dest.writeString(Remarks);
        dest.writeString(RunStatus);
        dest.writeString(ModifiedDate);
        dest.writeString(GM);
        dest.writeString(GMPercent);
        dest.writeString(GMSD);
        dest.writeString(GMSDPercent);
        dest.writeString(AverageGiveAway);
    }
}
