package com.brainlines.weightloggernewversion.utils;

public class Queries {
    public static final String DROP_PRODUCTION_KG_VALUES_MASTER_TABLES = "DROP TABLE IF EXISTS " +Constants.PRODUCTION_KG_VALUES_MASTER_TABLES;
    public static final String CREATE_PRODUCTION_KG_VALUES_MASTER_TABLES= "CREATE TABLE " + Constants.PRODUCTION_KG_VALUES_MASTER_TABLES+" ("
            + Constants.PRODUCTION_GRAMMAGE_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.PRODUCTION_GRAMMAGE +" VARCHAR(50) ,"
            + Constants.PRODUCTION_KG_VALUES +" VARCHAR(50))";
    public static final String CREATE_USER_LOGIN_TABLE = "CREATE TABLE "
            + Constants.USER_LOGIN_TABLE+" ("
            + Constants.LOGIN_ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.USER_ID+" INTEGER ,"
            + Constants.ROLE_ID+" INTEGER ,"
            + Constants.USER_NAME+" VARCHAR(50) ,"
            + Constants.PASSWORD+" VARCHAR(50) ,"
            + Constants.IS_ACTIVE+" BIT ,"
            + Constants.IS_DELETED+" BIT ,"
            + Constants.CREATED_DATE+" VARCHAR(255) ,"
            + Constants.MODIFIED_DATE+" VARCHAR(255))";


   /* //Create Tables
    //Create User Login Table

    //Create Data Logger Table
    public static final String CREATE_DATA_LOGGER_TABLE =
            "CREATE TABLE "
                    + Constants.DATA_LOGGER_TABLE+" ("
                    + Constants.LOGGER_ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + Constants.OEMID+" INTEGER,"
                    + Constants.LOGGER_SERAIL_NUM+" VARCHAR(50) ,"
                    + Constants.LOGGER_NAME+" VARCHAR(150) ,"
                    + Constants.LOGGER_IP_ADDRESS+" VARCHAR(50) ,"
                    + Constants.LOGGER_MAC_ID+" VARCHAR(150) ,"//*********************Check
                    + Constants.LOGGER_LOCATION+" VARCHAR(150) ,"
                    + Constants.LOGGER_CREATE_TIMESTAMP+" VARCHAR(255) ,"
                    + Constants.LOGGER_CREATED_BY+" VARCHAR(50) ,"//************Check
                    + Constants.LOGGER_UPDATE_TIMESTAMP+" VARCHAR(100) ,"
                    + Constants.LOGGER_MODIFIED_BY+" VARCHAR(50) ,"
                    + Constants.LOGGER_IS_ACTIVE+" BIT ,"
                    + Constants.LOGGER_IN_USE+" BIT)";

    //Create Master Filler Type Table
    public static final String CREATE_MASTER_FILLER_TABLE = "CREATE TABLE "
            + Constants.MASTER_FILLER_TYPE_TABLE+" ("
            + Constants.FILLER_TYPE_ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.FILLER_TYPE_OEMID+" INTEGER ,"
            + Constants.FILLER_TYPE_PRODUCT_TYPE_ID+" VARCHAR(200) ,"
            + Constants.FILLER_TYPE_FILTER_TYPE+" VARCHAR(200) ,"
            + Constants.FILLER_TYPE_CREATED_DATE+" VARCHAR(255) ,"
            + Constants.FILLER_TYPE_MODIFIED_DATE+" VARCHAR(255) ,"
            + Constants.FILLER_TYPE_CREATED_BY+" VARCHAR(255) ,"
            + Constants.FILLER_TYPE_MODIFIED_BY+" VARCHAR(255) ,"
            + Constants.FILLER_TYPE_IS_ACTIVE+" BIT ,"
            + Constants.FILLER_TYPE_IS_DELETED+" BIT)";

    //Create Master Head Table
    public static final String CREATE_MASTER_HEAD_TABLE = "CREATE TABLE "
            + Constants.MASTER_HEAD_TABLE+" ("
            + Constants.MASTER_HEAD_HEAD_ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.MASTER_HEAD_OEM_ID+" INTEGER ,"
            //+Constants.MASTER_HEAD_HEAD_ID_+" INTEGER ,"
            + Constants.MASTER_HEAD_HEAD_TYPE_+" VARCHAR(500) ,"
            + Constants.MASTER_HEAD_IS_ACTIVE+" BIT ,"
            + Constants.MASTER_HEAD_CREATED_DATE+" VARCHAR(255) ,"
            + Constants.MASTER_HEAD_MODIFIED_DATE+" VARCHAR(255) ,"
            + Constants.MASTER_HEAD_CREATED_BY+" VARCHAR(255) ,"
            + Constants.MASTER_HEAD_MODIFIED_BY+" VARCHAR(255))";

    //Create Comments Table
    public static final String CREATE_COMMENTS_TABLE = "CREATE TABLE "
            + Constants.COMMENTS_TABLE+" ("
            + Constants.COMMENTS_ID+" INTEGER ,"
            + Constants.COMMENTS_OEM_ID+" INTEGER ,"
            + Constants.COMMENTS_MAC_ID+" INTEGER ,"
            + Constants.COMMENTS_LOGGER_ID+" INTEGER ,"
            + Constants.COMMENTS_TAG_ID+" VARCHAR(900) ,"
            + Constants.COMMENTS_COMMENTS_COLUMN+" VARCHAR(300) ,"
            + Constants.COMMENTS_FUN_COMMENTS_1+" VARCHAR(255) ,"
            + Constants.COMMENTS_FUN_COMMENTS_2+" VARCHAR(255) ,"
            + Constants.COMMENTS_FUN_COMMENTS_3+" VARCHAR(255) ,"
            + Constants.COMMENTS_FUN_COMMENTS_4+" VARCHAR(255) ,"
            + Constants.COMMENTS_FUN_COMMENTS_5+" VARCHAR(255) ,"
            + Constants.COMMENTS_MODIFIED_DATE+" VARCHAR(255) ,"
            + Constants.COMMENTS_IS_SYNC+" BIT)";

    //Create Bag Length Analytics
    public static final String CREATE_BAG_LENGTH_ANALYTICS_TABLE = "CREATE TABLE "
            + Constants.BAG_LENGTH_ANALYTICS_TABLE+" ("
            + Constants.BAG_LEN_ANA_ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.BAG_LEN_ANA_OEM_ID+" INTEGER ,"
            + Constants.BAG_LEN_ANA_LOGGER_ID+" INTEGER ,"
            + Constants.BAG_LEN_ANA_TAG_ID+" VARCHAR(900) ,"
            + Constants.BAG_LEN_ANA_MIN_READING+" VARCHAR(45) ,"
            + Constants.BAG_LEN_ANA_MAX_READING+" VARCHAR(45) ,"
            + Constants.BAG_LEN_ANA_RANGE_READING+" VARCHAR(45) ,"
            + Constants.BAG_LEN_ANA_RUN_STATUS+" VARCHAR(45) ,"
            + Constants.BAG_LEN_ANA_MODIFIED_DATE+" VARCHAR(255) ,"
            + Constants.BAG_LEN_ANA_MEAN_READING+" VARCHAR(255) ,"
            + Constants.BAG_LEN_ANA_IS_SYNC+" BIT)";

    //Create SD Mean Table
    public static final String CREATE_SD_MEAN_TABLE = "CREATE TABLE "
            + Constants.SD_MEAN_TABLE+" ("
            + Constants.SD_MEAN_OEM_ID+" INTEGER ,"
            + Constants.SD_MEAN_LOGEGR_ID+" INTEGER ,"
            + Constants.SD_MEAN_TAG_ID+" VARCHAR(900) ,"
            + Constants.SD_MEAN_STD_DEVIATION+" VARCHAR(255) ,"
            + Constants.SD_MEAN_MEAN_COLUMN+" VARCHAR(255) ,"
            + Constants.SD_MEAN_CREATED_DATE+" VARCHAR(255) ,"
            + Constants.SD_MEAN_IS_SYNC+" BIT)";

    //Create Observations Table
    public static final String CREATE_OBSERVATIONS_TABLE = "CREATE TABLE "
            + Constants.OBSERVATIONS_TABLE+" ("
            + Constants.OBSERVATIONS_ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.OBSERVATIONS_OEM_ID+" INTEGER ,"
            + Constants.OBSERVATIONS_MAC_ID+" VARCHAR(50) ,"
            + Constants.OBSERVATIONS_LOGGER_ID+" VARCHAR(50) ,"
            + Constants.OBSERVATIONS_TAG_ID+" VARCHAR(900) ,"
            + Constants.OBSERVATIONS_AUGER_COUNT+" VARCHAR(50) ,"
            + Constants.OBSERVATIONS_AUGER_SPEED+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_AUGER_PRE_AUGER+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_AUGER_NO_OF_AGITATORS+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_MACHINE_VERTICAL_TEFLON+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_MACHINE_HORIZONTAL_LAW_OPENING+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_MACHINE_HORIZONTAL_LAW_MOVEMENT+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_MACHINE_VERTICAL_ALIGN_CENTER+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_MACHINE_VERTICAL_LAW_MOVEMENT+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_PACKAGE_WIRE_CUP+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_PACKAGE_EMPTY_POUCHES+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_PACKAGE_BAG_LENGTH_VARIATION+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_PACKAGE_PAPER_SHIFTING+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_PACKAGE_VERTICAL_OVERLAP+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_PACKAGE_POUCH_SYMMETRY+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_PACKAGE_OBSERV_SPEED+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_SEAL_REJECTION_REASON+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_PACKAGE_IS_SYNC+" BIT)";

    //Create Paper Shift Analytics
    public static final String CREATE_PAPER_SHIFT_ANALYTICS_TABLE = "CREATE TABLE "
            + Constants.PAPER_SHIFT_ANALYTICS_TABLE+" ("
            + Constants.PAPER_SHIFT_ANALYTICS_ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.PAPER_SHIFT_ANALYTICS_OEM_ID+" INTEGER ,"
            + Constants.PAPER_SHIFT_ANALYTICS_LOGGER_ID+" INTEGER ,"
            + Constants.PAPER_SHIFT_ANALYTICS_TAG_ID+" VARCHAR(255) ,"
            + Constants.PAPER_SHIFT_ANALYTICS_MIN_READING+" VARCHAR(255) ,"
            + Constants.PAPER_SHIFT_ANALYTICS_MAX_READING+" VARCHAR(255) ,"
            + Constants.PAPER_SHIFT_ANALYTICS_RANGE_READING+" VARCHAR(255) ,"
            + Constants.PAPER_SHIFT_ANALYTICS_RUN_STATUS+" VARCHAR(255) ,"
            + Constants.PAPER_SHIFT_ANALYTICS_MODIFIED_DATE+" VARCHAR(255) ,"
            + Constants.PAPER_SHIFT_ANALYTICS_MEAN_READING+" VARCHAR(255) ,"
            + Constants.PAPER_SHIFT_ANALYTICS_IS_SYNC+" Bit)";

    //Create Weight Results Analytics
    public static final String CREATE_WEIGHT_RESULTS_ANALYTICS_TABLE = "CREATE TABLE "
            + Constants.WEIGHT_RESULTS_ANALYTICS_TABLE+" ("
            + Constants.WEIGHT_RESULTS_ANALYTICS_ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_OEM_ID+" INTEGER ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_LOGGER_ID+" INTEGER ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_TAG_ID+" VARCHAR(255) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_MIN_READING+" VARCHAR(255) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_MAX_READING+" VARCHAR(255) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_RANGE_READING+" VARCHAR(255) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_MEAN_READING+" VARCHAR(255) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_SD_READING+" VARCHAR(255) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_REMARKS+" VARCHAR(255) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_RUN_STATUS+" VARCHAR(255) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_MODIFIED_DATE+" VARCHAR(255) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_GM+" VARCHAR(60) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_GM_PERCENT+" VARCHAR(60) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_GM_SD+" VARCHAR(60) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_GM_SD_PERCENT+" VARCHAR(60) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_AVERAGE_GIVE_AWAY+" VARCHAR(255) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_IS_SYNC+" BIT)";

    //Create Master Products
    public static final String CREATE_MASTER_PRODUCTS_TABLE = "CREATE TABLE "
            + Constants.MASTER_PRODUCTS_TABLE+" ("
            + Constants.MASTER_PRODUCTS_PRODUCTS_ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.MASTER_PRODUCTS_OEM_ID+" INTEGER ,"
            + Constants.MASTER_PRODUCTS_PRODUCTS+" VARCHAR(50) ,"
            + Constants.MASTER_PRODUCTS_TYPE_ID+" INTEGER ,"
            + Constants.MASTER_PRODUCTS_IS_ACTIVE+" BIT ,"
            + Constants.MASTER_PRODUCTS_IS_DELETED+" BIT ,"
            + Constants.MASTER_PRODUCTS_CREATED_DATE+" VARCHAR(255) ,"
            + Constants.MASTER_PRODUCTS_MODIFIED_DATE+" VARCHAR(255) ,"
            + Constants.MASTER_PRODUCTS_CREATED_BY+" VARCHAR(255) ,"
            + Constants.MASTER_PRODUCTS_MODIFIED_BY+" VARCHAR(255),"
            + Constants.MASTER_PRODUCTS_PRICE_PER_KG+" VARCHAR(255),"
            + Constants.MASTER_PRODUCTS_CURRENCY+" VARCHAR(255))";

    //Create Weight Results Analytics
    public static final String CREATE_MASTER_POUCH_SYMMETRY_TABLE = "CREATE TABLE "
            + Constants.MASTER_POUCH_SYMMETRY_TABLE+" ("
            + Constants.MASTER_POUCH_SYMMETRY_ID+" INTEGER ,"
            + Constants.MASTER_POUCH_OEM_ID+" INTEGER ,"
            + Constants.MASTER_POUCH_ID+" INTEGER ,"
            + Constants.MASTER_POUCH_TYPE_ID+" VARCHAR(255) ,"
            + Constants.MASTER_POUCH_DESC+" VARCHAR(255) ,"
            + Constants.MASTER_POUCH_IS_ACTIVE+" BIT ,"
            + Constants.MASTER_POUCH_IS_DELETED+" BIT ,"
            + Constants.MASTER_POUCH_CREATED_DATE+" VARCHAR(255) ,"
            + Constants.MASTER_POUCH_MODIFIED_DATE+" VARCHAR(255) ,"
            + Constants.MASTER_POUCH_CREATED_BY+" VARCHAR(255) ,"
            + Constants.MASTER_POUCH_MODIFIED_BY+" VARCHAR(255))";


    public static final String CREATE_C_TAGS_TABLE = "CREATE TABLE " + Constants.C_TAGS_TABLE+" ("
            + Constants.C_TAG_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.C_TAG_OEM_ID +" INT ,"
            + Constants.C_TAG_MACID +" VARCHAR(60) ,"
            + Constants.C_TAG_LOGGER_ID +" VARCHAR(60) ,"
            + Constants.C_TAG_TAGS_ID +" VARCHAR(1000) ,"
            + Constants.C_TAG_OEM_NUMBER +" VARCHAR(100) ,"
            + Constants.C_TAG_TARGET_WIGHT +" VARCHAR(100) ,"
            + Constants.C_TAG_CUSTOMER_NAME +" VARCHAR(60) ,"
            + Constants.C_TAG_MACHINE_TYPE_ID +" VARCHAR(60) ,"
            + Constants.C_TAG_PRODUCT_TYPE_ID +" VARCHAR(60) ,"
            + Constants.C_TAG_PRODUCT_TYPE_NAME +" VARCHAR(60) ,"
            + Constants.C_TAG_MACHINE_TYPE_NAME +" VARCHAR(60) ,"
            + Constants.C_TAG_PRODUCT +" VARCHAR(60) ,"
            + Constants.C_TAG_OPERATOR +" VARCHAR(60) ,"
            + Constants.C_TAG_TARGET_WEIGHT_UNIT +" VARCHAR(60) ,"
            + Constants.C_TAG_INSPECTOR +" VARCHAR(100) ,"
            + Constants.C_TAG_FILEM_TYPE +" VARCHAR(60) ,"
            + Constants.C_TAG_PRODUCT_FEED +" VARCHAR(60) ,"
            + Constants.C_TAG_AGITATOR_MOTOR_FREQ +" VARCHAR(60) ,"
            + Constants.C_TAG_AGITATOR_MODE +" VARCHAR(60) ,"
            + Constants.C_TAG_AGITATOR_OD +" VARCHAR(60) ,"
            + Constants.C_TAG_AGITATOR_ID +" VARCHAR(60) ,"
            + Constants.C_TAG_AGITATOR_PITCH +" VARCHAR(60) ,"
            + Constants.C_TAG_TARGET_ACCOUNT_TYPE +" VARCHAR(60) ,"
            + Constants.C_TAG_TARGET_ACCOUNT_VALUE +" VARCHAR(60) ,"
            + Constants.C_TAG_MACHINE_SERIAL_NUMBER +" VARCHAR(60) ,"
            + Constants.C_TAG_PRODUCT_BULK_MAIN +" VARCHAR(60) ,"
            + Constants.C_TAG_TARGET_SPEED +" VARCHAR(60) ,"
            + Constants.C_TAG_HEAD +" VARCHAR(50) ,"
            + Constants.C_TAG_AIR_PRESSURE +" VARCHAR(50) ,"
            + Constants.C_TAG_QUANTITY_SETTING +" VARCHAR(50) ,"
            + Constants.C_TAG_BAG_LENGTH_SETTING +" VARCHAR(50) ,"
            + Constants.C_TAG_FILLVALVE_GAP +" VARCHAR(50) ,"
            + Constants.C_TAG_FILL_VFD_FREQUENCY +" VARCHAR(50) ,"
            + Constants.C_TAG_HORIZONTAL_CUT_SEAL_SETTING +" VARCHAR(50) ,"
            + Constants.C_TAG_HORIZONTAL_BAND_SEAL_SETTING +" VARCHAR(50) ,"
            + Constants.C_TAG_VERTICAL_SEAL_SETTING +" VARCHAR(50) ,"
            + Constants.C_TAG_AUGER_AGITATOR_FREQUENCY +" VARCHAR(50) ,"
            + Constants.C_TAG_AUGER_FILL_VALVE +" VARCHAR(50) ,"
            + Constants.C_TAG_AUGER_PITCH +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_WIDTH +" VARCHAR(60) ,"
            + Constants.C_TAG_POUCH_LENGTH +" VARCHAR(60) ,"
            + Constants.C_TAG_POUCH_SEAL_TYPE +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_TEMP_HORIZONTAL_FREQUENCY +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_TEMP_HZB +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_TEMP_VERTICAL +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_TEMP_VERT_FL +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_TEMP_VERTICAL_FREQUENCY +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_TEMP_VERT_BL +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_TEMP_VERT_BR +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_GUSSET +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_FILM_THICKNESS +" VARCHAR(50) ,"
            + Constants.C_TAG_PUCH_PRODUCT_BULK +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_EQ_HORIZ_SERRATIONS +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_FILL_eVENT +" VARCHAR(50) ,"
            + Constants.C_TAG_OVERALL_STATUS +" VARCHAR(255) ,"
            + Constants.C_TAG_REMARK +" VARCHAR(255) ,"
            + Constants.C_TAG_SEAL_STATUS +" VARCHAR(255) ,"
            + Constants.C_TAG_DROP_STATUS +" VARCHAR(255) ,"
            + Constants.C_TAG_TARGET_SPEED_STATUS +" VARCHAR(255) ,"
            + Constants.C_TAG_WEIGHT_TEST_STATUS +" VARCHAR(255) ,"
            + Constants.C_TAG_BAG_STATUS +" VARCHAR(255) ,"
            + Constants.C_TAG_PAPER_STATUS +" VARCHAR(255) ,"
            + Constants.C_TAG_FY_YEAR +" VARCHAR(255) ,"
            + Constants.C_TAG_FILLER_TYPE +" VARCHAR(255) ,"
            + Constants.C_TAG_SEAL_REJECTION_REASON +" VARCHAR(255) ,"
            + Constants.C_TAG_CUSTOMER_PRODUCT +" VARCHAR(50) ,"
            + Constants.C_TAG_CUSTOMER_FILM +" VARCHAR(50) ,"
            + Constants.C_TAG_NEGATIVE_WEIGHT +" VARCHAR(50) ,"
            + Constants.C_TAG_LOGGER_CREATE_TIMESTAMP +" VARCHAR(50) ,"
            + Constants.C_TAG_CREATED_DATE +" VARCHAR(255) ,"
            + Constants.C_TAG_MODIFIED_DATE +" VARCHAR(255) ,"
            + Constants.C_TAGS_IS_SYNC +" BIT)";

    public static final String CREATE_MASTER_WIRE_CUP_TABLE = "CREATE TABLE " + Constants.MASTER_WIRE_CUP_TABLE+" ("
            + Constants.MASTER_WIRE_CUP_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.MASTER_WIRE_CUP_OEM_ID +" INT ,"
            + Constants.MASTER_WIRE_CUP +" INT ,"
            + Constants.MASTER_WIRE_CUP_IS_ACTIVE +" INT ,"
            + Constants.MASTER_WIRE_CUP_CREATED_DATE +" VARCHAR(100) ,"
            + Constants.MASTER_WIRE_CUP_MODIFIED_DATE +" VARCHAR(100) ,"
            + Constants.MASTER_WIRE_CUP_CREATED_BY +" VARCHAR(60) ,"
            + Constants.MASTER_WIRE_CUP_MODIFIED_BY +" VARCHAR(255))";

    public static final String CREATE_TOLERANCE_TABLE = "CREATE TABLE " + Constants.TOLERANCE_TABLE+" ("
            + Constants.TOLERANCE_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.TOLERANCE_OEM_ID +" INT ,"
            + Constants.TOLERANCE_PRODUCT_TYPE +" VARCHAR(100) ,"
            + Constants.TOLERANCE_TARGET_WEIGHT +" INT ,"
            + Constants.TOLERANCE_MIN_TARGET_WEIGHT +" INT ,"
            + Constants.TOLERANCE_MAX_TARGET_WEIGHT +" INT ,"
            + Constants.TOLERANCE_MIN_WEIGHT_SAMPLES +" INT ,"
            + Constants.TOLERANCE_MIN_PAPER_SHIFT_SAMPLES +" INT ,"
            + Constants.TOLERANCE_PAPER_SHIFT_TOLERANCE +" INT ,"
            + Constants.TOLERANCE_MIN_BAG_LENGTH_SAMPLES +" INT ,"
            + Constants.TOLERANCE_BAG_LENGTH_TOLERANCE +" VARCHAR(60) ,"
            + Constants.TOLERANCE_LOGIN_ID +" INT ,"
            + Constants.TOLERANCE_USER_ID +" INT ,"
            + Constants.TOLERANCE_ROLE_ID +" INT ,"
            + Constants.TOLERANCE_USER_NAME +" VARCHAR(100) ,"
            + Constants.TOLERANCE_PASSWORD +" VARCHAR(100) ,"
            + Constants.TOLERANCE_IS_ACTIVE +" VARCHAR(50) ,"
            + Constants.TOLERANCE_IS_DELETED +" VARCHAR(60) ,"
            + Constants.TOLERANCE_CREATED_DATE +" VARCHAR(60) ,"
            + Constants.TOLERANCE_MODIFIED_DATE +" VARCHAR(60) ,"
            + Constants.TOLERANCE_CREATED_BY +" VARCHAR(60) ,"
            + Constants.TOLERANCE_MODIFIED_BY +" VARCHAR(255) ,"
            + Constants.TOLERANCE_POSITIVE_TOLERANCE + " VARCHAR(255) ,"
            + Constants.TOLERANCE_NEGATIVE_TOLERANCE + " VARCHAR(255))";

    public static final String CREATE_SEAL_REJECTION_TABLE = "CREATE TABLE " + Constants.SEAL_REJECTION_TABLE+" ("
            + Constants.SEAL_REJECTION_REASON_ID +" INT ,"
            + Constants.SEAL_REJECTION_OEM_ID +" INT ,"
            + Constants.SEAL_REJECTION_LOGGER_ID +" INT ,"
            + Constants.SEAL_REJECTION_PRODUCT_TYPE +" VARCHAR(100) ,"
            + Constants.SEAL_REJECTION_REASON_DESCIPTION +" VARCHAR(100) ,"
            + Constants.SEAL_REJECTION_IS_ACTIVE +" VARCHAR(100) ,"
            + Constants.SEAL_REJECTION_IS_DELETED +" VARCHAR(100) ,"
            + Constants.SEAL_REJECTION_CREATED_DATE +" VARCHAR(50) ,"
            + Constants.SEAL_REJECTION_MODIFIED_DATE +" VARCHAR(60) ,"
            + Constants.SEAL_REJECTION_CREATED_BY +" VARCHAR(60) ,"
            + Constants.SEAL_REJECTION_MODIFIED_BY +" VARCHAR(60) ,"
            + Constants.SEAL_REJECTION_IS_SYNC +" BIT)";


    public static final String CREATE_TARGET_ACCURACY_TABLE = "CREATE TABLE " + Constants.TARGET_ACCURACY_TABLE+" ("
            + Constants.TARGET_ACCURACY_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.TARGET_ACCURACY_OEM_ID +" INT ,"
            + Constants.TARGET_ACCURACY_TARGET_ID +" INT ,"
            + Constants.TARGET_ACCURACY +" INT ,"
            + Constants.TARGET_ACCURACY_DESCRIPTION +" VARCHAR(100) ,"
            + Constants.TARGET_ACCURACY_IS_ACTIVE +" VARCHAR(100) ,"
            + Constants.TARGET_ACCURACY_IS_DELETED +" VARCHAR(100) ,"
            + Constants.TARGET_ACCURACY_CREATED_DATE +" VARCHAR(50) ,"
            + Constants.TARGET_ACCURACY_MODIFIED_DATE +" VARCHAR(60) ,"
            + Constants.TARGET_ACCURACY_CREATED_BY +" VARCHAR(60) ,"
            + Constants.TARGET_ACCURACY_MODIFIED_BY +" VARCHAR(255))";

    public static final String CREATE_MACHINE_TYPE_TABLE = "CREATE TABLE " + Constants.MACHINE_TYPE_TABLE+" ("
            + Constants.MACHINE_TYPE_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.MACHINE_TYPE_OEM_ID +" INT ,"
            + Constants.MACHINE_TYPE_MACHINE_ID +" INT ,"
            + Constants.MACHINE_TYPE_PRODUCT_TYPE_ID +" INT ,"
            + Constants.MACHINE_TYPE_MACHINE_NAME +" VARCHAR(100) ,"
            + Constants.MACHINE_TYPE_MACHINE_DESCRIPTION +" VARCHAR(100) ,"
            + Constants.MACHINE_TYPE_IS_ACTIVE +" VARCHAR(100) ,"
            + Constants.MACHINE_TYPE_IS_DELETED +" VARCHAR(50) ,"
            + Constants.MACHINE_TYPE_CREATED_DATE +" VARCHAR(60) ,"
            + Constants.MACHINE_TYPE_MODIFIED_DATE +" VARCHAR(60) ,"
            + Constants.MACHINE_TYPE_CREATED_BY +" VARCHAR(60) ,"
            + Constants.MACHINE_TYPE_MODIFIED_BY +" VARCHAR(255))";

    public static final String CREATE_POUCH_ATTRIBUTES_TABLE = "CREATE TABLE " + Constants.POUCH_ATTRIBUTES_TABLE+" ("
            + Constants.POUCH_ATTRIBUTES_ID +" INT ,"
            + Constants.POUCH_ATTRIBUTES_OEM_ID +" INT ,"
            + Constants.POUCH_ATTRIBUTES_PRODUCT_TYPE_ID +" INT ,"
            + Constants.POUCH_ATTRIBUTES_DESCRIPTION +" TEXT ,"
            + Constants.POUCH_ATTRIBUTES_IS_ACTVIE +" INT ,"
            + Constants.POUCH_ATTRIBUTES_IS_DELETED +" VARCHAR(100) ,"
            + Constants.POUCH_ATTRIBUTES_CREATED_DATE +" VARCHAR(100) ,"
            + Constants.POUCH_ATTRIBUTES_MODIFIED_DATE +" VARCHAR(50) ,"
            + Constants.POUCH_ATTRIBUTES_CREATED_BY +" VARCHAR(60) ,"
            + Constants.POUCH_ATTRIBUTES_MODIFIED_BY +" VARCHAR(255))";

    public static final String CREATE_SEAL_TYPE_TABLE = "CREATE TABLE " + Constants.SEAL_TYPE_TABLE+" ("
            + Constants.SEAL_TYPE_SEAL_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.SEAL_TYPE +" VARCHAR(100) ,"
            + Constants.SEAL_TYPE_OEM_ID +" INT ,"
            + Constants.SEAL_TYPE_PRODUCT_TYPE_ID +" INT ,"
            + Constants.SEAL_TYPE_IS_ACTIVE +" INT ,"
            + Constants.SEAL_TYPE_IS_DELETED +" VARCHAR(100) ,"
            + Constants.SEAL_TYPE_CREATED_DATE +" VARCHAR(100) ,"
            + Constants.SEAL_TYPE_MODIFIED_DATE +" VARCHAR(50) ,"
            + Constants.SEAL_TYPE_CREATED_BY +" VARCHAR(60) ,"
            + Constants.SEAL_TYPE_MODIFIED_BY +" VARCHAR(255))";

    public static final String CREATE_P_TAG_RESULT_TABLE = "CREATE TABLE " + Constants.P_TAG_RESULT_TABLE+" ("
            + Constants.P_TAG_RESULT_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.P_TAG_RESULT_OEM_ID +" INT ,"
            + Constants.P_TAG_RESULT_MAC_ID +" INT ,"
            + Constants.P_TAG_RESULT_LOGGER_ID +" INT ,"
            + Constants.P_TAG_RESULT_RUN_STATUS_ID +" INT ,"
            + Constants.P_TAG_RESULT_TAG_ID +" VARCHAR(100) ,"+ Constants.P_TAG_RESULT_BATCH_ID +" VARCHAR(100) ,"
            + Constants.P_TAG_RESULT_TARGET_WEIGHT +" VARCHAR(100) ,"
            + Constants.P_TAG_RESULT_HEAD +" VARCHAR(50) ,"
            + Constants.P_TAG_RESULT_STATUS_ABORTED +" VARCHAR(60) ,"
            + Constants.P_TAG_RESULT_MODIFIED_DATE +" VARCHAR(60) ,"
            + Constants.P_TAG_RESULT_IS_AUTHORIZED +" VARCHAR(60) ,"
            + Constants.P_TAG_RESULT_MODIFIED_BY +" VARCHAR(255),"
             + Constants.P_TAG_RESULT_IS_SYNC +" BIT)";

    public static final String CREATE_PRODUCTION_TAG_TABLE = "CREATE TABLE " + Constants.PRODUCTION_TAG_TABLE+" ("
            + Constants.PRODUCTION_TAG_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.PRODUCTION_TAG +" VARCHAR(100) ,"
            + Constants.PRODUCTION_TAG_OEM_ID +" INT ,"
            + Constants.PRODUCTION_TAG_MAC_ID +" VARCHAR(100) ,"
            + Constants.PRODUCTION_TAG_BATCH_ID +" VARCHAR(100) ,"
            + Constants.PRODUCTION_TAG_PRODUCT_ID +" VARCHAR(100) ,"
            + Constants.PRODUCTION_TAG_GM_PER_POUCH +" VARCHAR(50) ,"
            + Constants.PRODUCTION_TAG_UNIT +" VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_TOTAL_QTY +" VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_TOTAL_QTY_UOM +" VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_IS_NEGATIVE_ALLOWED + " VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_MODIFIED_DATE + " VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_SHIFT + " VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_NO_OF_POUCHES + " VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_SESSION_POSITVIE_TOLERANCE + " VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_SESSION_NEAGTIVE_TOLERANCE + " VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_MASTER_TOLERANCE_POSITIVE + " VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_MASTER_TOLERANCE_NEGATIVE + " VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_UCL_GM + " VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_LCL_GM + " VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_OPERATOR_NAME + " VARCHAR(50) ,"
            + Constants.PRODUCTION_TAG_BATCH_STATUS + " VARCHAR(250) ,"
            + Constants.PRODUCTION_TAG_IS_SYNC +" BIT)";

    public static final String CREATE_P_WEIGHT_READING_TABLE = "CREATE TABLE " + Constants.P_WEIGHT_READING_TABLE+" ("
            + Constants.P_WEIGHT_READING_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.P_WEIGHT_READING_OEM_ID +" INT ,"
            + Constants.P_WEIGHT_READING_MAC_ID +" INT ,"
            + Constants.P_WEIGHT_READING_LOGGER_ID +" INT ,"
            + Constants.P_WEIGHT_READING_WEIGHT_ID +" INT ,"
            + Constants.P_WEIGHT_READING_BATCHID +" VARCHAR(60) ,"
            + Constants.P_WEIGHT_READING_PRODUCTION_TAG_NAME +" VARCHAR(100) ,"
            + Constants.P_WEIGHT_READING_WEIGHT +" VARCHAR(100) ,"
            + Constants.P_WEIGHT_READING_SHIFT +" VARCHAR(50) ,"
            + Constants.P_WEIGHT_READING_TARGETGMPERPOUCH +" VARCHAR(50) ,"
            + Constants.P_WEIGHT_READING_ACTUALGMPERPOUCH +" VARCHAR(50) ,"
            + Constants.P_WEIGHT_READING_EXCESSGIVEAWAYPERPOUCH +" VARCHAR(50) ,"
            + Constants.P_WEIGHT_READING_CUMILATIVEEXCESSGIVEAWAY +" VARCHAR(50) ,"
            + Constants.P_WEIGHT_READING_CUMILATIVEQUANTITY +" VARCHAR(50) ,"
            + Constants.P_WEIGHT_READING_CUMILATIVE_POUCHES +" VARCHAR(50) ,"
            + Constants.P_WEIGHT_READING_NEGATIVE_GIVEAWAY_FROM_TAG + " VARCHAR(50) ,"
            + Constants.P_WEIGHT_READING_MODIFIED_DATE +" VARCHAR(60) ,"
            + Constants.P_WEIGHT_READING_STATUS +" VARCHAR(60) ,"
            + Constants.P_WEIGHT_READING_CREATED_DATE +" VARCHAR(60) ,"
            + Constants.P_WEIGHT_READING_CREATED_BY +" VARCHAR(255) ,"
            + Constants.P_WEIGHT_READING_MODIFIED_BY +" VARCHAR(255) ,"
            + Constants.P_WEIGHT_READING_IS_SYNC +" BIT)";

    public static final String CREATE_PRODUCTION_WEIGHT_REPORT_TABLE = "CREATE TABLE " + Constants.PRODUCTION_WEIGHT_REPORT+" ("
            + Constants.PRODUCTION_WEIGHT_REPORT_REPORTID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_OEMID +" INT ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_BATCH_ID +" INT ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_TAGID +" INT ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_MIN_WEIGHT +" VARCHAR(100) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_MAX_WEIGHT +" VARCHAR(100) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_MEAN_WEIGHT +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_AVERAGE_WEIGHT +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_RANGE +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_SD +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_UNFILLED_QUANTITY +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_FILLED_QUANTITY +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_UNFILLED_POUCHES +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_FILLED_POUCHES +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_EXCESS_GIVEAWAY_PRICE +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_EXCESS_GIVEAWAY_GM +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_REASON +" VARCHAR(60) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_REMARK +" VARCHAR(60) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_ISACTIVE +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_CREATED_DATE +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_MODIFIED_DATE +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_IS_SYNC +" BIT)";

    public static final String CREATE_BAG_LENGTH_VARIATION_RESULT_TABLE = "CREATE TABLE " + Constants.BAG_LENGTH_VARIATION_RESULT+" ("
            + Constants.BAG_LENGTH_VARIATION_OEM_ID +" INT ,"
            + Constants.BAG_LENGTH_VARIATION_MAC_ID +" INT ,"
            + Constants.BAG_LENGTH_VARIATION_LOGGER_ID +" INT ,"
            + Constants.BAG_LENGTH_VARIATION_BAG_LENGTH_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.BAG_LENGTH_VARIATION_TAG_ID +" VARCHAR(100) ,"
            + Constants.BAG_LENGTH_VARIATION_LENGTH +" VARCHAR(100) ,"
            + Constants.BAG_LENGTH_VARIATION_OINUM +" VARCHAR(100) ,"
            + Constants.BAG_LENGTH_VARIATION_MODIFIED_DATE +" VARCHAR(255) ,"
            + Constants.BAG_LENGTH_VARIATION_IS_SYNC +" BIT)";

    public static final String CREATE_PAPER_SHIFT_RESULT_TABLE = "CREATE TABLE " + Constants.PAPER_SHIFT_RESULTS+" ("
            + Constants.PAPER_SHIFT_RESULTS_OEM_ID +" INT ,"
            + Constants.PAPER_SHIFT_RESULTS_MAC_ID +" INT ,"
            + Constants.PAPER_SHIFT_RESULTS_LOGGER_ID +" INT ,"
            + Constants.PAPER_SHIFT_RESULTS_PAPER_SHIFT_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.PAPER_SHIFT_RESULTS_TAG_ID +" VARCHAR(100) ,"
            + Constants.PAPER_SHIFT_RESULTS_SHIFT +" VARCHAR(100) ,"
            + Constants.PAPER_SHIFT_RESULTS_OINUM +" VARCHAR(100) ,"
            + Constants.PAPER_SHIFT_RESULTS_MODIFIED_DATE +" VARCHAR(255) ,"
            + Constants.PAPER_SHIFT_RESULTS_IS_SYNC +" BIT)";

    public static final String CREATE_MASTER_UOM_TABLE = "CREATE TABLE " + Constants.MASTER_UOM_TABLE+" ("
            + Constants.UOM_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.UOM_TABLE_OEMID +" INTEGER  ,"
            + Constants.UOM_PRODUCT_TYPE +" VARCHAR(50) ,"
            + Constants.UOM_PRODUCT_UOM +" VARCHAR(50) ,"
            + Constants.UOM_PRODUCT_IS_ACTIVE +" BIT ,"
            + Constants.UOM_PRODUCT_IS_DELETED +" BIT ,"
            + Constants.UOM_PRODUCT_CREATED_DATE +" VARCHAR(100) ,"
            + Constants.UOM_PRODUCT_MODIFIED_DATE +" VARCHAR(100) ,"
            + Constants.UOM_PRODUCT_CREATED_BY +" VARCHAR(100) ,"
            + Constants.UOM_PRODUCT_MODIFIED_BY +" VARCHAR(100))";

    public static final String CRETAE_TABLE_SEAL_REJECTION_REASON_MASTER_TABLE =
            "CREATE TABLE "
                    + Constants.SEAL_REJECTION_REASON_MASTER_TABLE+" ("
                    + Constants.SEAL_REJECTION_REASON_REASON_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + Constants.SEAL_REJECTION_REASON_OEM_ID +" VARCHAR(100) ,"
                    + Constants.SEAL_REJECTION_REASON_PRODUCT_TYPE +" VARCHAR(100) ,"
                    + Constants.SEAL_REJECTION_REASON_REASON_DESC +" VARCHAR(100) ,"
                    + Constants.SEAL_REJECTION_REASON_IS_ACTIVE +" BIT ,"
                    + Constants.SEAL_REJECTION_REASON_IS_DELETED +" BIT ,"
                    + Constants.SEAL_REJECTION_REASON_CREATED_DATE +" VARCHAR(100) ,"
                    + Constants.SEAL_REJECTION_REASON_MODIFIED_DATE +" VARCHAR(100) ,"
                    + Constants.SEAL_REJECTION_REASON_CREATED_BY +" VARCHAR(100) ,"
                    + Constants.SEAL_REJECTION_REASON_MODIFIED_BY +" VARCHAR(100))";

    public static final String CREATE_PRODUCT_TABLE = "CREATE TABLE " + Constants.PRODUCT_TYPE_TABLE+" ("
            + Constants.PRODUCT_OEM_ID +" INT ,"
            + Constants.PRODUCT_TYPE_ID +" INT ,"
            + Constants.PRODUCT_IS_ACTVIE +" INT ,"
            + Constants.PRODUCT_IS_DELETED +" INT ,"
            + Constants.PRODUCT_CREATED_BY +" VARCHAR(100) ,"
            + Constants.PRODUCT_MODIFIED_BY +" VARCHAR(100) ,"
            + Constants.PRODUCT_UNIT_OF_MEASUREMENT +" VARCHAR(100) ,"
            + Constants.PRODUCT_UNIT_OF_MEASUREMENT_ID +" VARCHAR(50) ,"
            + Constants.PRODUCT_CREATED_DATE +" VARCHAR(60) ,"
            + Constants.PRODUCT_MODIFIED_DATE +" VARCHAR(60))";

    public static final String CREATE_WEIGHT_TEST_RESULT = "CREATE TABLE " + Constants.WEIGHT_TEST_RESULT+" ("
            + Constants.WEIGHT_TEST_RESULT_TEST_WEIGHT_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.WEIGHT_TEST_RESULT_OEMID +" INT ,"
            + Constants.WEIGHT_TEST_RESULT_MAC_ID +" INT ,"
            + Constants.WEIGHT_TEST_RESULT_LOGGER_ID +" VARCHAR(100) ,"
            + Constants.WEIGHT_TEST_RESULT_TAG_ID +" VARCHAR(100) ,"
            + Constants.WEIGHT_TEST_RESULT_WEIGHT +" VARCHAR(100) ,"
            + Constants.WEIGHT_TEST_RESULT_OINUM +" VARCHAR(100) ,"
            + Constants.WEIGHT_TEST_RESULT_MODIFIED_DATE +" VARCHAR(50) ,"
            + Constants.WEIGHT_TEST_RESULTS_IS_SYNC +" BIT)";

    public static final String CREATE_PRODUCTION_REASON_TABLE = "CREATE TABLE " + Constants.PRODUCTION_REASON_MASTER+" ("
            + Constants.PRODUCTION_REASON_REASONID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.PRODUCTION_REASON_REASONDESC +" VARCHAR(100) ,"
            + Constants.PRODUCTION_REASON_CREATED_DATE +" VARCHAR(100) ,"
            + Constants.PRODUCTION_REASON_MODIFIED_DATE +" VARCHAR(100))";

    public static final String CREATE_PRODUCTION_TOLERANCE_TABLE= "CREATE TABLE " + Constants.PRODUCTION_TOLERANCE_MASTER+" ("
            + Constants.PRODUCTION_TOLERANCE_TOLERANCEID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.PRODUCTION_TOLERANCE_OEMID +" INT ,"
            + Constants.PRODUCTION_TOLERANCE_GRAMMAGE +" VARCHAR(100) ,"
            + Constants.PRODUCTION_TOLERANCE_TOLERANCE_POSITIVE +" VARCHAR(100) ,"
            + Constants.PRODUCTION_TOLERANCE_TOLERANCE_NEGATIVE +" VARCHAR(100) ,"
            + Constants.PRODUCTION_TOLERANCE_ISACTIVE +" VARCHAR(100) ,"
            + Constants.PRODUCTION_TOLERANCE_ISDELETED +" VARCHAR(100) ,"
            + Constants.PRODUCTION_TOLERANCE_CREATED_DATE +" VARCHAR(50) ,"
            + Constants.PRODUCTION_TOLERANCE_MODIFIED_DATE +" VARCHAR(50))";

    public static final String CREATE_LIST_MASTER_TABLES= "CREATE TABLE " + Constants.LIST_OF_MASTER_TABLES+" ("
            + Constants.MASTER_TABLES_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.MASTER_TABLES_NAME +" VARCHAR(50) ,"
            + Constants.MASTER_TABLES_DISLAY_NAME +" VARCHAR(50))";








    public static final String QUERY_INSERT_TAGTBL_PRODUCTION = "INSERT INTO tags " +
            "(tagid,oinum,targetwt,fillertype,customer,machinetype," +
            "producttype,product,operator,targetwtunit,inspector," +
            "filmtype,productfeed,pouchwidth,pouchlength,agitatormotorfreq," +
            "agitatormode,agitatorod,agitatorid,agitatorpitch,targetacctype," +
            "targetaccval,mcserialnum,prbulkmain,targetspeed,customerproduct," +
            "head,airpressure,qtysetting,bglensetting,fillvalvegap,fillvfdfreq," +
            "horizcutsealsetting,horizbandsealsetting,vertsealsetting,augeragitatorfreq," +
            "augerfillvalve,augurpitch,pouchsealtype,pouchtemphzf,pouchtemphzb,pouchtempvert," +
            "pouchtempvertfl,pouchtempvertfr,pouchtempvertbl,pouchtempvertbr,pouchgusset," +
            "customerfilm,pouchfilmthickness,pouchprbulk,poucheqhorizserrations,pouchfillevent,negWt,fyyear,createddate,shift,uom,totalqty,unitoftotalqty,totalqty1)" +
            "values (?,?,?,?,?,?,?,?,?," +
            "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )";*/

    public static final String QUERY_INSERT_TAGTBL = "INSERT INTO tags " +
            "(tagid,oinum,targetwt,fillertype,customer,machinetype," +
            "producttype,product,operator,targetwtunit,inspector," +
            "filmtype,productfeed,pouchwidth,pouchlength,agitatormotorfreq," +
            "agitatormode,agitatorod,agitatorid,agitatorpitch,targetacctype," +
            "targetaccval,mcserialnum,prbulkmain,targetspeed,customerproduct," +
            "head,airpressure,qtysetting,bglensetting,fillvalvegap,fillvfdfreq," +
            "horizcutsealsetting,horizbandsealsetting,vertsealsetting,augeragitatorfreq," +
            "augerfillvalve,augurpitch,pouchsealtype,pouchtemphzf,pouchtemphzb,pouchtempvert," +
            "pouchtempvertfl,pouchtempvertfr,pouchtempvertbl,pouchtempvertbr,pouchgusset," +
            "customerfilm,pouchfilmthickness,pouchprbulk,poucheqhorizserrations,pouchfillevent,negWt,fyyear,createddate)" +
            "values (?,?,?,?,?,?,?,?,?," +
            "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )";


    //SWAS CREATE TABLES QUERIES
    private static final String COMMA_SEP = ",";
    private static final String TEXT_TYPE = " TEXT";


    public static final String CREATE_TABLE_USER="CREATE TABLE "
            + Constants.TABLE_USER_DETAILS + "("
            + Constants.SWAS_USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT " + COMMA_SEP
            + Constants.USER_TOKEN + " VARCHAR(1000) " + COMMA_SEP
            + Constants.USER_TOKEN_TYPE + " VARCHAR(100) " + COMMA_SEP
            + Constants.USER_EMAILID + " VARCHAR(100) " + COMMA_SEP
            + Constants.USER_OEMID + " VARCHAR(100) " + COMMA_SEP
            + Constants.USER_ROLE_NAME + " VARCHAR(100) " + COMMA_SEP
            + Constants.USER_EXPIRES_IN + TEXT_TYPE + COMMA_SEP
            + Constants.USER_OEM_NAME + " VARCHAR(500) " + COMMA_SEP
            + Constants.USER_FIRST_NAME + " VARCHAR(100) " + COMMA_SEP
            + Constants.USER_LAST_NAME + " VARCHAR(100) " + COMMA_SEP
            + Constants.USER_LOGIN_ID + " VARCHAR(500) " + COMMA_SEP
            + Constants.USER_IS_ISSUED + " VARCHAR(500) " + COMMA_SEP
            + Constants.USER_EXPIRES + " VARCHAR(500) " +
            ")";

    public static final String CREATE_TABLE_LIST_OF_SERVICES_BY_STATUS="CREATE TABLE "
            + Constants.TABLE_LIST_OF_SERVICES_BY_STATUS + "("
            + Constants.LIST_OF_SERVICES_BY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_SERVICE_CALL_ID + " bigint " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_CUSTOMER_ID + " VARCHAR(100) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_LOCATION_NAME + " nvarchar(50) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_ITEM_ID + " nvarchar(20) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_SERVICE_ENGINEER_NAME + " VARCHAR(50) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_STATUS_TEXT + " VARCHAR(50) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_TYPE_OF_SERVICE_CALL_TEXT + " VARCHAR(50) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_CREATED_BY + " nvarchar(50) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_CREATED_DATE + " datetime " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_UPDATED_BY + " bigint " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_UPDATED_DATE + " datetime " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_SERVICE_CALL_STATUS_ID + " bigint " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_NAME + " VARCHAR(100) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_BRIEF_COMPLAINT + " VARCHAR(1000) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_TYPE_OF_FAULT_TEXT + " VARCHAR(50) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_FAULTY_PART + " VARCHAR(60) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_CURRENCY + " VARCHAR(60) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_DOCUMENTS_PATH + " nvarchar(60) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_TYPE_OF_SERVICE_CALL_ID + " bigint " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_CURRENTLY_ASSIGNED_TO_SE + " VARCHAR(50) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_CLOSED_DATE_TIME + " VARCHAR(50) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_CLOSED_BY + " VARCHAR(50) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_COMMENTS + " VARCHAR(500) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_SERVICE_ENGINEER_ID + " bigint " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_NUMBER_OF_VISITS + " VARCHAR(500) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_SHORT_TERM + " VARCHAR(500) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_LONG_TERM + " VARCHAR(500) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_SHORT_TERM_ID + " bigint " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_LONG_TERM_ID + " bigint " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_ROOT_CAUSE_ID + " bigint " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_SE_REMARK + " VARCHAR(500) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_QE_REMARK + " VARCHAR(500) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_ROOT_CAUSE_NAME + " VARCHAR(500) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_DESCRIPTION + " VARCHAR(500) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_DEPARTMENT_ID + " bigint " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_DEPARTMENTS + " VARCHAR(500) " + COMMA_SEP
            + Constants.LIST_OF_SERVICES_BY_STATUS_USER_ID + " VARCHAR(500) "
            + ")";

    public static final String CREATE_TABLE_PART_ORDERS="CREATE TABLE "
            + Constants.TABLE_PART_ORDERS + "("
            + Constants.PART_ORDERS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT " + COMMA_SEP
            + Constants.PART_ORDERS_SERVICE_CALL_ID + " bigint " + COMMA_SEP
            + Constants.PART_ORDERS_OEMID + " VARCHAR(100) " + COMMA_SEP
            + Constants.PART_ORDERS_CUSTOMER_ID + " VARCHAR(50) " + COMMA_SEP
            + Constants.PART_ORDERS_ITEMID + " nvarchar(20) " + COMMA_SEP
            + Constants.PART_ORDERS_QUANTITY + " VARCHAR(500) " + COMMA_SEP
            + Constants.PART_ORDERS_NAME + " VARCHAR(100) " + COMMA_SEP
            + Constants.PART_ORDERS_PRICE + " numeric " + COMMA_SEP
            + Constants.PART_ORDERS_APPLICABLE + " VARCHAR(50) " + COMMA_SEP
            + Constants.PART_ORDERS_FINAL_PRICE + " VARCHAR(50) " + COMMA_SEP
            + Constants.PART_ORDERS_EXCLUDE_INCLUDE + " VARCHAR(50) " + COMMA_SEP
            + Constants.PART_ORDERS_CREATED_BY + " VARCHAR(50) " + COMMA_SEP
            + Constants.PART_ORDERS_CREATED_DATE + " bigint"
            + ")";

    public static final String CREATE_TABLE_SERVICE_CALL_FEEDBACK="CREATE TABLE "
            + Constants.TABLE_SERVICE_CALL_FEEDBACK + "("
            + Constants.TABLE_SERVICE_CALL_FEEDBACK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT " + COMMA_SEP
            + Constants.SERVICE_CALL_FEEDBACK_FEEDBACK_DETAILS_ID + " bigint " + COMMA_SEP
            + Constants.SERVICE_CALL_FEEDBACK_OEM_ID + " VARCHAR(100) " + COMMA_SEP
            + Constants.SERVICE_CALL_FEEDBACK_SERVICE_CALL_ID + " bigint " + COMMA_SEP
            + Constants.SERVICE_CALL_FEEDBACK_USER_ID + " VARCHAR(60) " + COMMA_SEP
            + Constants.SERVICE_CALL_FEEDBACK_USER_FEEDBACK + " VARCHAR(100) " + COMMA_SEP
            + Constants.SERVICE_CALL_FEEDBACK_CREATED_BY + " VARCHAR(50) " + COMMA_SEP
            + Constants.SERVICE_CALL_FEEDBACK_CREATED_DATE + " bigint " + COMMA_SEP
            + Constants.SERVICE_CALL_FEEDBACK_IS_SYNC + " BIT "
            + ")";

    public static final String CREATE_TABLE_EXPENSE_DETAILS="CREATE TABLE "
            + Constants.TABLE_EXPENSE_DETAILS + "("
            + Constants.EXPENSE_DETAILS_TRAVELLING_EXPENSES_ID + " INTEGER PRIMARY KEY AUTOINCREMENT " + COMMA_SEP
            + Constants.EXPENSE_DETAILS_TRAVELLING_EXPENSES_HEADS_ID + " bigint " + COMMA_SEP
            + Constants.EXPENSE_DETAILS_SERVICE_CALL_ID + " bigint " + COMMA_SEP
            + Constants.EXPENSE_DETAILS_OEMID + " VARCHAR(100) " + COMMA_SEP
            + Constants.EXPENSE_DETAILS_CUSTOMER_ID + " VARCHAR(50) " + COMMA_SEP
            + Constants.EXPENSE_DETAILS_SERVICE_EXPENSE_HEADS_ID + " INT " + COMMA_SEP
            + Constants.EXPENSE_DETAILS_SERVICE_EXPENSE_HEADS_NAME + " VARCHAR(50) " + COMMA_SEP
            + Constants.EXPENSE_DETAILS_VISIT_DATES + " bigint " + COMMA_SEP
            + Constants.EXPENSE_DETAILS_VISIT_CHARGES_RS + " VARCHAR(50) " + COMMA_SEP
            + Constants.EXPENSE_DETAILS_CREATED_BY + " VARCHAR(50) " + COMMA_SEP
            + Constants.EXPENSE_DETAILS_CREATED_DATE + " bigint " + COMMA_SEP
            + Constants.EXPENSE_DETAILS_IS_SYNC + " BIT "
            + ")";

    public static final String CREATE_TABLE_SERVICE_CALL_MC_PARAMETERS="CREATE TABLE "
            + Constants.TABLE_SERVICE_CALL_MC_PARAMETERS + "("
            + Constants.SERVICE_CALL_MC_PARAMETERS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT " + COMMA_SEP
            + Constants.SERVICE_CALL_MC_PARAMETERS_MACHINE_PARAMETER_LOG_ID + " bigint " + COMMA_SEP
            + Constants.SERVICE_CALL_MC_PARAMETERS_OEM_ID + " VARCHAR(100) " + COMMA_SEP
            + Constants.SERVICE_CALL_MC_PARAMETERS_CUSTOMER_ID + " VARCHAR(50) " + COMMA_SEP
            + Constants.SERVICE_CALL_MC_PARAMETERS_SERVICE_CALL_ID + " bigint " + COMMA_SEP
            + Constants.SERVICE_CALL_MC_PARAMETERS_MACHINE_PARAMETERS + " VARCHAR(500) " + COMMA_SEP
            + Constants.SERVICE_CALL_MC_PARAMETERS_SERVICE_CALL_START_DATE + " datetime " + COMMA_SEP
            + Constants.SERVICE_CALL_MC_PARAMETERS_SERVICE_CALL_END_DATE + " datetime " + COMMA_SEP
            + Constants.SERVICE_CALL_MC_PARAMETERS_PARAMETER_VALUE + " VARCHAR(1000) " + COMMA_SEP
            + Constants.SERVICE_CALL_MC_PARAMETERS_UNIT + " VARCHAR(50) " + COMMA_SEP
            + Constants.SERVICE_CALL_MC_PARAMETERS_REMARK + " VARCHAR(500) " + COMMA_SEP
            + Constants.SERVICE_CALL_MC_PARAMETERS_CREATED_BY + " VARCHAR(50) " + COMMA_SEP
            + Constants.SERVICE_CALL_MC_PARAMETERS_CREATED_DATE + " datetime" + COMMA_SEP
            + Constants.SERVICE_CALL_MC_PARAMETERS_IS_SYNC + " BIT"
            + ")";

    public static final String CREATE_TABLE_SERVICE_CALL_COMP_PARAMETERS="CREATE TABLE "
            + Constants.TABLE_SERVICE_CALL_COMP_PARAMETERS + "("
            + Constants.SERVICE_CALL_COMP_PARAMETERS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT " + COMMA_SEP
            + Constants.SERVICE_CALL_COMP_PARAMETERS_COMPITITORS_PARMETER_ID + " bigint " + COMMA_SEP
            + Constants.SERVICE_CALL_COMP_PARAMETERS_OEM_ID + " VARCHAR(100) " + COMMA_SEP
            + Constants.SERVICE_CALL_COMP_PARAMETERS_CUSTOMER_ID + " VARCHAR(50) " + COMMA_SEP
            + Constants.SERVICE_CALL_COMP_PARAMETERS_SERVICE_CALL_ID + " bigint " + COMMA_SEP
            + Constants.SERVICE_CALL_COMP_PARAMETERS_COMPITITORS_PARMETER + " VARCHAR(500) " + COMMA_SEP
            + Constants.SERVICE_CALL_COMP_PARAMETERS_SERVICE_CALL_START_DATE + " datetime " + COMMA_SEP
            + Constants.SERVICE_CALL_COMP_PARAMETERS_MACHINE_PARA_VALUE + " VARCHAR(1000) " + COMMA_SEP
            + Constants.SERVICE_CALL_COMP_PARAMETERS_REMARK + " VARCHAR(500) " + COMMA_SEP
            + Constants.SERVICE_CALL_COMP_PARAMETERS_CREATED_BY + " VARCHAR(50) " + COMMA_SEP
            + Constants.SERVICE_CALL_COMP_PARAMETERS_CREATED_DATE + " bigint " + COMMA_SEP
            + Constants.SERVICE_CALL_COMP_PARAMETERS_UNIT + " VARCHAR(50) " + COMMA_SEP
            + Constants.SERVICE_CALL_COMP_PARAMETERS_IS_SYNC + " BIT "
            + ")";

    public static final String CREATE_TABLE_VISIT_LOGS="CREATE TABLE "
            + Constants.TABLE_VISIT_LOGS + "("
            + Constants.VISIT_LOGS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT " + COMMA_SEP
            + Constants.VISIT_LOGS_SERVICE_LOG_ID + " bigint " + COMMA_SEP
            + Constants.VISIT_LOGS_OEM_ID + " VARCHAR(100) " + COMMA_SEP
            + Constants.VISIT_LOGS_SERVICE_CALL_ID + " bigint " + COMMA_SEP
            + Constants.VISIT_LOGS_CUSTOMER_ID + " VARCHAR(50) " + COMMA_SEP
            + Constants.VISIT_LOGS_SERVICE_ENGINEER_ID + " bigint " + COMMA_SEP
            + Constants.VISIT_LOGS_PART_STATUS + " VARCHAR(500) " + COMMA_SEP
            + Constants.VISIT_LOGS_SERVICE_CALL_START_DATE + " datetime " + COMMA_SEP
            + Constants.VISIT_LOGS_SERVICE_CALL_END_DATE + " datetime " + COMMA_SEP
            + Constants.VISIT_LOGS_SERVICE_CALL_ENTRY_TIME + " VARCHAR(50) " + COMMA_SEP
            + Constants.VISIT_LOGS_SERVICE_CALL_EXIT_TIME + " VARCHAR(50) " + COMMA_SEP
            + Constants.VISIT_LOGS_GATE_PASS_IMAGE_PATH + " VARCHAR(1000) " + COMMA_SEP
            + Constants.VISIT_LOGS_REMARK + " VARCHAR(50) " + COMMA_SEP
            + Constants.VISIT_LOGS_CREATED_BY + " VARCHAR(100) " + COMMA_SEP
            + Constants.VISIT_LOGS_CREATED_DATE + " VARCHAR(100) " + COMMA_SEP
            + Constants.VISIT_LOGS_IS_SYNC + " BIT "
            + ")";

    public static final String CREATE_TABLE_SCHEDULED_DATES="CREATE TABLE "
            + Constants.TABLE_SCHEDULED_DATES + "("
            + Constants.SCHEDULED_DATES_ID + " INTEGER PRIMARY KEY AUTOINCREMENT " + COMMA_SEP
            + Constants.SCHEDULED_DATES_SERVICE_ENGINEER_DETAILS_ID + " bigint " + COMMA_SEP
            + Constants.SCHEDULED_DATES_OEM_ID + " VARCHAR(100) " + COMMA_SEP
            + Constants.SCHEDULED_DATES_CUSTOMER_ID + " VARCHAR(100) " + COMMA_SEP
            + Constants.SCHEDULED_DATES_SERVICE_CALL_ID + " bigint " + COMMA_SEP
            + Constants.SCHEDULED_DATES_SERVICE_ENGINEER_ID + " bigint " + COMMA_SEP
            + Constants.SCHEDULED_DATES_IS_ASSIGNED + " VARCHAR(100) " + COMMA_SEP
            + Constants.SCHEDULED_DATES_IS_ACCEPTED + " nchar(1) " + COMMA_SEP
            + Constants.SCHEDULED_DATES_ASSIGNED_DATE + " datetime " + COMMA_SEP
            + Constants.SCHEDULED_DATES_ASSIGNED_TIME + " VARCHAR(50) " + COMMA_SEP
            + Constants.SCHEDULED_DATES_NOTE + " VARCHAR(500) " + COMMA_SEP
            + Constants.SCHEDULED_DATES_CREATED_BY + " VARCHAR(50) " + COMMA_SEP
            + Constants.SCHEDULED_DATES_CREATED_DATE + " datetime " + COMMA_SEP
            + Constants.SCHEDULED_DATES_UPDATE_BY + " VARCHAR(50) " + COMMA_SEP
            + Constants.SCHEDULED_DATES_IS_ACTIVE + " INT " + COMMA_SEP
            + Constants.SCHEDULED_DATES_UPDATE_DATE + " datetime " + COMMA_SEP
            + Constants.SCHEDULED_DATES_LOCATION_NAME + " nvarchar(60) " + COMMA_SEP
            + Constants.SCHEDULED_DATES_COMMON_NAME + " VARCHAR(50) " + COMMA_SEP
            + Constants.SCHEDULED_DATES_IS_SYNC + " BIT "
            + ")";

    public static final String CREATE_TABLE_SERVICE_CALL_CHECKLIST="CREATE TABLE "
            + Constants.TABLE_SERVICE_CALL_CHECKLIST + "("
            + Constants.SERVICE_CALL_CHECKLIST_ID + " INTEGER PRIMARY KEY AUTOINCREMENT " + COMMA_SEP
            + Constants.SERVICE_CALL_CHECKLIST_SERVICE_CALL_ID + " bigint " + COMMA_SEP
            + Constants.SERVICE_CALL_CHECKLIST_CHECKLIST_FOR_SITE_READYNESS_ID + " VARCHAR(100) " + COMMA_SEP
            + Constants.SERVICE_CALL_CHECKLIST_CHECKLIST_FOR_SITE_READYNESS_TEXT + " bigint " + COMMA_SEP
            + Constants.SERVICE_CALL_CHECKLIST_CHECKED + " VARCHAR(50) "
            + ")";

    public static final String CREATE_TABLE_SERVICE_CALL_FAULT_CHAR="CREATE TABLE "
            + Constants.TABLE_SERVICE_CALL_FAULT_CHAR + "("
            + Constants.SERVICE_CALL_FAULT_CHAR_ID + " INTEGER PRIMARY KEY AUTOINCREMENT " + COMMA_SEP
            + Constants.SERVICE_CALL_FAULT_CHAR_CUSTOMER_ID + " VARCHAR(50) " + COMMA_SEP
            + Constants.SERVICE_CALL_FAULT_CHAR_SERVICE_CALL_ID + " bigint " + COMMA_SEP
            + Constants.SERVICE_CALL_FAULT_CHAR_FAULT_CHAR_TEXT + " bigint "
            + ")";

    public static final String CREATE_TABLE_EXPENSE_HEAD_MASTER="CREATE TABLE "
            + Constants.TABLE_EXPENSE_HEAD_MASTER + "("
            + Constants.EXPENSE_HEAD_MASTER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT " + COMMA_SEP
            + Constants.EXPENSE_HEAD_MASTER_SERVICE_EXPENSE_HEADS_ID + " INT " + COMMA_SEP
            + Constants.EXPENSE_HEAD_MASTER_SERVICE_EXPENSE_HEADS_NAME + " VARCHAR(50) "
            + ")";

    public static final String CREATE_TABLE_DOCUMENTS="CREATE TABLE "
            + Constants.TABLE_DOCUMENTS + "("
            + Constants.DOCUMENTS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT " + COMMA_SEP
            + Constants.DOCUMENTS_REPORT_DOCUMENTS_ID + " bigint " + COMMA_SEP
            + Constants.DOCUMENTS_REPORT_OEM_ID + " VARCHAR(100) " + COMMA_SEP
            + Constants.DOCUMENTS_REPORT_CUSTOMER_ID + " VARCHAR(50) " + COMMA_SEP
            + Constants.DOCUMENTS_REPORT_SERVICE_CALL_ID + " bigint " + COMMA_SEP
            + Constants.DOCUMENTS_REPORT_DOCUMENT_PATH + " BLOB " + COMMA_SEP
            + Constants.DOCUMENTS_REPORT_DOCUMENT_NAME + " VARCHAR(100) " + COMMA_SEP
            + Constants.DOCUMENTS_REPORT_IS_ACTIVE + " nchar(1) " + COMMA_SEP
            + Constants.DOCUMENTS_REPORT_CREATED_BY + " VARCHAR(50) " + COMMA_SEP
            + Constants.DOCUMENTS_REPORT_CREATED_DATE + " datetime " + COMMA_SEP
            + Constants.DOCUMENTS_REPORT_UPDATED_BY + " VARCHAR(50) " + COMMA_SEP
            + Constants.DOCUMENTS_REPORT_UPDATED_DATE + " datetime " + COMMA_SEP
            + Constants.DOCUMENTS_REPORT_IS_SYNC + " BIT "
            + ")";

    public static final String CREATE_TABLE_MACHINE_PARAMETER_INPROCESS_CALL="CREATE TABLE "
            + Constants.TABLE_MACHINE_PARAMETER_INPROCESS_CALL + "("
            + Constants.MACHINE_PARAMETER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT " + COMMA_SEP
            + Constants.MACHINE_PARAMETER_INPROCESS_CALL_ID + " bigint " + COMMA_SEP
            + Constants.MACHINE_PARAMETER_INPROCESS_CALL_NAME + " VARCHAR(100) " + COMMA_SEP
            + Constants.MACHINE_PARAMETER_INPROCESS_CALL_UNIT + " VARCHAR(50) "
            + ")";

    public static final String CREATE_TABLE_MACHINE_PARAMETER_COMPLTEDC_CALLS="CREATE TABLE "
            + Constants.TABLE_MACHINE_PARAMETER_COMPLTEDC_CALLS + "("
            + Constants.MACHINE_PARAMETER_COMPLTEDC_ID + " INTEGER PRIMARY KEY AUTOINCREMENT " + COMMA_SEP
            + Constants.MACHINE_PARAMETER_COMPLTEDC_CALLS_MACHINE_PARAMETER_LOG_ID + " bigint " + COMMA_SEP
            + Constants.MACHINE_PARAMETER_COMPLTEDC_CALLS_OEM_ID + " VARCHAR(100) " + COMMA_SEP
            + Constants.MACHINE_PARAMETER_COMPLTEDC_CALLS_CUSTOMER_ID + " VARCHAR(100) " + COMMA_SEP
            + Constants.MACHINE_PARAMETER_COMPLTEDC_CALLS_SERVICE_CALL_ID + " VARCHAR(100) " + COMMA_SEP
            + Constants.MACHINE_PARAMETER_COMPLTEDC_CALLS_MACHINE_PARAMETERS + " VARCHAR(100) " + COMMA_SEP
            + Constants.MACHINE_PARAMETER_COMPLTEDC_CALLS_SERVICE_CALL_START_DATE + " datetime " + COMMA_SEP
            + Constants.MACHINE_PARAMETER_COMPLTEDC_CALLS_SERVICE_CALL_END_DATE + " datetime " + COMMA_SEP
            + Constants.MACHINE_PARAMETER_COMPLTEDC_CALLS_PARAMETER_VALUE + " VARCHAR(100) " + COMMA_SEP
            + Constants.MACHINE_PARAMETER_COMPLTEDC_CALLS_UNIT + " VARCHAR(100) " + COMMA_SEP
            + Constants.MACHINE_PARAMETER_COMPLTEDC_CALLS_REMARK + " VARCHAR(100) " + COMMA_SEP
            + Constants.MACHINE_PARAMETER_COMPLTEDC_CALLS_CREATED_BY + " VARCHAR(100) " + COMMA_SEP
            + Constants.MACHINE_PARAMETER_COMPLTEDC_CALLS_CREATED_DATE + " datetime "
            + ")";

    public static final String CREATE_TABLE_COMPITITIVE_MACHINE_PARAMS_IN_PROCESS_CALL="CREATE TABLE "
            + Constants.TABLE_COMPITITIVE_MACHINE_PARAMS_IN_PROCESS_CALL + "("
            + Constants.COMPITITIVE_MACHINE_PARAMS_IN_PROCESS_CALL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT " + COMMA_SEP
            + Constants.COMPITITIVE_MACHINE_PARAMS_IN_PROCESS_CALL_COMP_PARAMS_ID + " bigint " + COMMA_SEP
            + Constants.COMPITITIVE_MACHINE_PARAMS_IN_PROCESS_CALL__COMP_PARAMS + " VARCHAR(100) " + COMMA_SEP
            + Constants.COMPITITIVE_MACHINE_PARAMS_IN_PROCESS_CALL_UNIT + " VARCHAR(50) "
            + ")";

    public static final String CREATE_TABLE_SHORT_TERM_LIST_TABLE="CREATE TABLE "
            + Constants.TABLE_SHORT_TERM_LIST_TABLE + "("
            + Constants.TABLE_SHORT_TERM_RESOLUTION_ID + " bigint " + COMMA_SEP
            + Constants.TABLE_SHORT_TERM_RESOLUTION_TEXT + " varchar(100) "
            + ")";

    public static final String CREATE_TABLE_STATUS_CALL="CREATE TABLE "
            + Constants.TABLE_STATUS_CALL + "("
            + Constants.TABLE_STATUS_CALL_SERVICE_CALL_STATUS_ID + " bigint " + COMMA_SEP
            + Constants.TABLE_STATUS_CALL_STATUS_TEXT + " varchar(100) "
            + ")";

    public static final String DROP_TABLE_USER_DETAILS = "DROP TABLE IF EXISTS " +Constants.TABLE_USER_DETAILS;
    public static final String DROP_TABLE_LIST_OF_SERVICES_BY_STATUS = "DROP TABLE IF EXISTS " +Constants.TABLE_LIST_OF_SERVICES_BY_STATUS;
    public static final String DROP_TABLE_PART_ORDERS = "DROP TABLE IF EXISTS " +Constants.TABLE_PART_ORDERS;
    public static final String DROP_TABLE_SERVICE_CALL_FEEDBACK = "DROP TABLE IF EXISTS " +Constants.TABLE_SERVICE_CALL_FEEDBACK;
    public static final String DROP_TABLE_EXPENSE_DETAILS = "DROP TABLE IF EXISTS " +Constants.TABLE_EXPENSE_DETAILS;
    public static final String DROP_TABLE_SERVICE_CALL_MC_PARAMETERS = "DROP TABLE IF EXISTS " +Constants.TABLE_SERVICE_CALL_MC_PARAMETERS;
    public static final String DROP_TABLE_SERVICE_CALL_COMP_PARAMETERS = "DROP TABLE IF EXISTS " +Constants.TABLE_SERVICE_CALL_COMP_PARAMETERS;
    public static final String DROP_TABLE_VISIT_LOGS = "DROP TABLE IF EXISTS " +Constants.TABLE_VISIT_LOGS;
    public static final String DROP_TABLE_SCHEDULED_DATES = "DROP TABLE IF EXISTS " +Constants.TABLE_SCHEDULED_DATES;
    public static final String DROP_TABLE_SERVICE_CALL_CHECKLIST = "DROP TABLE IF EXISTS " +Constants.TABLE_SERVICE_CALL_CHECKLIST;
    public static final String DROP_TABLE_SERVICE_CALL_FAULT_CHAR = "DROP TABLE IF EXISTS " +Constants.TABLE_SERVICE_CALL_FAULT_CHAR;
    public static final String DROP_TABLE_EXPENSE_HEAD_MASTER = "DROP TABLE IF EXISTS " +Constants.TABLE_EXPENSE_HEAD_MASTER;
    public static final String DROP_TABLE_DOCUMENTS = "DROP TABLE IF EXISTS " +Constants.TABLE_DOCUMENTS;
    public static final String DROP_TABLE_MACHINE_PARAMETER_INPROCESS_CALL = "DROP TABLE IF EXISTS " +Constants.TABLE_MACHINE_PARAMETER_INPROCESS_CALL;
    public static final String DROP_TABLE_MACHINE_PARAMETER_COMPLTEDC_CALLS = "DROP TABLE IF EXISTS " +Constants.TABLE_MACHINE_PARAMETER_COMPLTEDC_CALLS;
    public static final String DROP_TABLE_COMPITITIVE_MACHINE_PARAMS_IN_PROCESS_CALL = "DROP TABLE IF EXISTS " +Constants.TABLE_COMPITITIVE_MACHINE_PARAMS_IN_PROCESS_CALL;
    public static final String DROP_TABLE_SHORT_TERM_LIST_TABLE = "DROP TABLE IF EXISTS " +Constants.TABLE_SHORT_TERM_LIST_TABLE;
    public static final String DROP_TABLE_STATUS_CALL = "DROP TABLE IF EXISTS " +Constants.TABLE_STATUS_CALL;
    
    
    /*
    * Falco JK Create Statements
    * */

    public static final String CREATE_PACKING_TICKET_TABLE="CREATE TABLE "
            + Constants.PACKING_TICKET_TABLE + "("
            + Constants.PACKING_TICKET_ID + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_TK_ID + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_TICKET_NO + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_TICKET_TYPE + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_TK_SONO + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_TK_PLANT + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_TK_SKU + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_TK_PROD_SKU + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_TK_RELEASE_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_TK_START_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_TK_END_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_TK_QUANTITY + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_TK_UNPACKING_SKU + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_TK_ACTIVE + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_TK_UNPACKING + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_TK_RELEAE + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_MONTH_START + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_MONTH_END + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_TOTAL_QTY + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_CRETAED_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_CREATED_BY + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_APPROVED_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_APPROVED_BY + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_TK_ACTUAL_START_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.PACKING_TICKET_TK_ACTUAL_END_DATE + TEXT_TYPE +
            ")";

    public static final String CREATE_PRODUCTION_TICKET_TABLE="CREATE TABLE "
            + Constants.PRODUCTION_TICKET_TABLE + "("
            + Constants.PRODUCTION_TICKET_ID + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_TK_ID + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_NO + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_TK_PSKU + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_TK_PLANT + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_TK_VALUESTREAM_CODE + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_TK_FTYPE_CODE + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_TK_QTY + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_TK_ISRELEASE + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_TK_START_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_TK_END_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_TK_IS_RELEASE1 + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_TK_RELEASE_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_TK_ACTIVE + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_TK_ACTUAL_START_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_TK_IS_WIP_ENTER + TEXT_TYPE + COMMA_SEP


            + Constants.PRODUCTION_TICKET_TK_ACTUAL_END_DATE + TEXT_TYPE +
            ")";


    public static final String CREATE_DATE_WISE_QUANTITY_PACKING_TABLE="CREATE TABLE "
            + Constants.DATE_WISE_QUANTITY_PACKING_TABLE + "("
            + Constants.DATE_WISE_QUANTITY_PACKING_TICKET_ID + TEXT_TYPE + COMMA_SEP
            + Constants.DATE_WISE_QUANTITY_PACKING_TICKET_TT_ID + TEXT_TYPE + COMMA_SEP
            + Constants.DATE_WISE_QUANTITY_PACKING_TICKET_NO + TEXT_TYPE + COMMA_SEP
            + Constants.DATE_WISE_QUANTITY_PACKING_TICKET_PLANT + TEXT_TYPE + COMMA_SEP
            + Constants.DATE_WISE_QUANTITY_PACKING_TICKET_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.DATE_WISE_QUANTITY_PACKING_TICKET_TOTAL_QTY + TEXT_TYPE + COMMA_SEP
            + Constants.DATE_WISE_QUANTITY_PACKING_TICKET_DAYWISE_QTY + TEXT_TYPE + COMMA_SEP
            + Constants.DATE_WISE_QUANTITY_PACKING_TICKET_PENDING + TEXT_TYPE + COMMA_SEP
            + Constants.DATE_WISE_QUANTITY_PACKING_TICKET_DAY + TEXT_TYPE + COMMA_SEP
            + Constants.DATE_WISE_QUANTITY_PACKING_TICKET_CREATED_BY + TEXT_TYPE + COMMA_SEP
            + Constants.DATE_WISE_QUANTITY_PACKING_TICKET_CREATED_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.DATE_WISE_QUANTITY_PACKING_TICKET_APPROVED_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.DATE_WISE_QUANTITY_PACKING_TICKET_APPROVED_BY + TEXT_TYPE + COMMA_SEP
            + Constants.DATE_WISE_QUANTITY_PACKING_TICKET_ACTIVE + TEXT_TYPE + COMMA_SEP
            + Constants.DATE_WISE_QUANTITY_PACKING_TICKET_APPROVE + TEXT_TYPE + COMMA_SEP
            + Constants.DATE_WISE_QUANTITY_PACKING_TICKET_FLAG1 + TEXT_TYPE + COMMA_SEP
            + Constants.DATE_WISE_QUANTITY_PACKING_TICKET_SKU + TEXT_TYPE + COMMA_SEP
            + Constants.DATE_WISE_QUANTITY_PACKING_TICKET_START_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.DATE_WISE_QUANTITY_PACKING_TICKET_END_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.DATE_WISE_QUANTITY_PACKING_TICKET_ACTUAL_START_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.DATE_WISE_QUANTITY_PACKING_TICKET_ACTUAL_END_DATE + TEXT_TYPE +
            ")";

    public static final String CREATE_PRODUCTION_TICKET_DETAILS_TABLE="CREATE TABLE "
            + Constants.PRODUCTION_TICKET_DETAILS_TABLE + "("
            + Constants.PRODUCTION_TICKET_DETAILS_PTKD_PACKET_ID + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_DETAILS_PTKD_TICKET_NO + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_DETAILS_PTKD_PLANT + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_DETAILS_PTKD_VALUE_STREAM_CODE + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_DETAILS_PTKD_OPERATION + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_DETAILS_PTKD_UPDATEDATE + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_DETAILS_PTKD_OK_QTY + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_DETAILS_PTKD_REJECT_QTY + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_DETAILS_PTKD_REWORK_QTY + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_DETAILS_PTKD_WIP_QTY + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_DETAILS_PTKD_SECOND_QTY + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_DETAILS_OPERATION_NAME + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_DETAILS_PTKD_QTY + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_DETAILS_UNIT_NAME + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_DETAILS_VALUESTREAM_NAME + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_DETAILS_TICKET_ACTUAL_START_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_DETAILS_TICKET_ACTUAL_END_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_DETAILS_TICKET_START_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_DETAILS_TICKET_END_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_DETAILS_PTKD_CHANGE_ROUTE_FLAG + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_DETAILS_CREATED_BY + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_DETAILS_CREATED_DATE + TEXT_TYPE +
            ")";

    public static final String CREATE_PRODUCTION_TICKET_MULTIPLE_SKU_TABLE="CREATE TABLE "
            + Constants.PRODUCTION_TICKET_MULTIPLE_SKU_TABLE + "("
            + Constants.PRODUCTION_TICKET_MULTIPLE_SKU_PTKD_TICKET_NO + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_MULTIPLE_SKU_PTKD_SKU_NO + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_MULTIPLE_SKU_DP_STAMP_TYPE + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_MULTIPLE_SKU_DP_STAMP_CHART + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_MULTIPLE_SKU_DP_BLKO_FLAG + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_MULTIPLE_SKU_DP_TANG_COLOR + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_MULTIPLE_SKU_QTY + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_MULTIPLE_SKU_ORG_QTY + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_MULTIPLE_SKU_CREATED_BY + TEXT_TYPE + COMMA_SEP
            + Constants.PRODUCTION_TICKET_MULTIPLE_SKU_CREATED_DATE + TEXT_TYPE +
            ")";

    public static final String CREATE_WIP_TABLE="CREATE TABLE "
            + Constants.WIP_TABLE + "("
            + Constants.WIP_PTKD_ID + TEXT_TYPE + COMMA_SEP
            + Constants.WIP_PTKD_PKTID + TEXT_TYPE + COMMA_SEP
            + Constants.WIP_PTKD_TICKET_NO + TEXT_TYPE + COMMA_SEP
            + Constants.WIP_PTKD_PSKU + TEXT_TYPE + COMMA_SEP
            + Constants.WIP_PTKD_PLANT + TEXT_TYPE + COMMA_SEP
            + Constants.WIP_PTKD_VALUE_STREAM_CODE + TEXT_TYPE + COMMA_SEP
            + Constants.WIP_PTKD_OPERATION + TEXT_TYPE + COMMA_SEP
            + Constants.WIP_PTKD_WIP + TEXT_TYPE + COMMA_SEP
            + Constants.WIP_OPR_OPEARTION_NAME + TEXT_TYPE + COMMA_SEP
            + Constants.WIP_UNI_UNIT_NAME + TEXT_TYPE + COMMA_SEP
            + Constants.WIP_PTKD_QTY + TEXT_TYPE + COMMA_SEP
            + Constants.WIP_CREATED_BY + TEXT_TYPE + COMMA_SEP
            + Constants.WIP_CREATED_DATE + TEXT_TYPE +
            ")";

    public static final String CREATE_TICKET_EQUIPMENT_TABLE="CREATE TABLE "
            + Constants.TICKET_EQUIPMENT_TABLE + "("
            + Constants.TICKET_EQUIPMENT_PTKD_PACKAT_ID + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_EQUIPMENT_PTKD_TICKET_NO + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_EQUIPMENT_PTKD_VALUE_STRAEM + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_EQUIPMENT_PTKD_OPERATION + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_EQUIPMENT_PTKD_PLANT + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_EQUIPMENT_EQUIPMENT_ID + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_EQUIPMENT_EQUIPMENT_NAME + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_EQUIPMENT_EQUIPMENT_TYPE + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_EQUIPMENT_EQUIPMENT_ASSET_NO + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_EQUIPMENT_CREATED_BY + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_EQUIPMENT_CREATED_DATE + TEXT_TYPE +
            ")";

    public static final String CREATE_SAVE_DOWN_TIME_TABLE="CREATE TABLE "
            + Constants.SAVE_DOWN_TIME_TABLE + "("
            + Constants.SAVE_DOWN_TIME_PTKD_TICKET_NO + TEXT_TYPE + COMMA_SEP
            + Constants.SAVE_DOWN_TIME_PTKD_VALUE_STRAEM_CODE + TEXT_TYPE + COMMA_SEP
            + Constants.SAVE_DOWN_TIME_TT_PLANT + TEXT_TYPE + COMMA_SEP
            + Constants.SAVE_DOWN_TIME_OPERATION_NO + TEXT_TYPE + COMMA_SEP
            + Constants.SAVE_DOWN_TIME_TE_EQUIPMENT_ID + TEXT_TYPE + COMMA_SEP
            + Constants.SAVE_DOWN_TIME_TICKET_ID_NO + TEXT_TYPE + COMMA_SEP
            + Constants.SAVE_DOWN_TIME_TE_RESEASENDT + TEXT_TYPE + COMMA_SEP
            + Constants.SAVE_DOWN_TIME_TE_NUMBER_OF_SHIP_T + TEXT_TYPE + COMMA_SEP
            + Constants.SAVE_DOWN_TIME_TE_AB_BR_QT + TEXT_TYPE + COMMA_SEP
            + Constants.SAVE_DOWN_TIME_TE_MOVED_QUANTITY + TEXT_TYPE + COMMA_SEP
            + Constants.SAVE_DOWN_TIME_TE_ASSET_NO + TEXT_TYPE + COMMA_SEP
            + Constants.SAVE_DOWN_TIME_USER_ID + TEXT_TYPE + COMMA_SEP
            + Constants.SAVE_DOWN_CR_REMARK + TEXT_TYPE + COMMA_SEP
            + Constants.SAVE_DOWN_CREATED_BY + TEXT_TYPE + COMMA_SEP
            + Constants.SAVE_DOWN_CREATED_DATE + TEXT_TYPE +
            ")";


    public static final String CREATE_TICKET_WISE_OPERATION_LIST="CREATE TABLE "
            + Constants.TICKET_WISE_OPERATION_LIST + "("
            + Constants.TICKET_WISE_OPERATION_PTKD_ID + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_WISE_OPERATION_PTKID + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_WISE_OPERATION_TICKET_NO + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_WISE_OPERATION_PSKU + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_WISE_OPERATION_PLANT + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_WISE_OPERATION_QTY + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_WISE_OPERATION_VALUE_STREAM_CODE + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_WISE_OPERATION_FTYPE_CODE + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_WISE_OPERATION + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_WISE_OPERATION_CREATED_BY + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_WISE_OPERATION_CREATED_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_WISE_OPERATION_APPROVED_BY + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_WISE_OPERATION_APPROVE_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_WISE_OPERATION_RELEASE_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.TICKET_WISE_OPERATION_IS_RELEASE + TEXT_TYPE +COMMA_SEP
            + Constants.TICKET_WISE_OPERATION_ACTIVE + TEXT_TYPE +
            ")";

    public static final String CREATE_EQUPMENT_MASTER_TABLE="CREATE TABLE "
            + Constants.EQUPMENT_MASTER_TABLE + "("
            + Constants.EQUPMENT_ID + TEXT_TYPE + COMMA_SEP
            + Constants.EQUPMENT_NAME + TEXT_TYPE + COMMA_SEP
            + Constants.EQUPMENT_TYPE + TEXT_TYPE + COMMA_SEP
            + Constants.EQUPMENT_REMARKS + TEXT_TYPE + COMMA_SEP
            + Constants.EQUPMENT_IS_APPROVED + TEXT_TYPE + COMMA_SEP
            + Constants.EQUPMENT_IS_ACTIVE + TEXT_TYPE + COMMA_SEP
            + Constants.EQUPMENT_CREATED_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.EQUPMENT_CREATED_BY + TEXT_TYPE + COMMA_SEP
            + Constants.EQUPMENT_APPROVED_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.EQUPMENT_APPROVED_BY + TEXT_TYPE +
            ")";

    public static final String CREATE_ASSET_NO_MASTER_TABLE="CREATE TABLE "
            + Constants.ASSET_NO_MASTER_TABLE + "("
            + Constants.ASSET_NO + TEXT_TYPE + COMMA_SEP
            + Constants.ASSET_EQUIPMENT_ID + TEXT_TYPE + COMMA_SEP
            + Constants.ASSET_NO_CREATED_BY + TEXT_TYPE + COMMA_SEP
            + Constants.ASSET_NO_CREATED_DATE + TEXT_TYPE + COMMA_SEP
            + Constants.ASSET_NO_IS_ACTIVE + TEXT_TYPE + COMMA_SEP
            + Constants.ASSET_IS_APPROVED + TEXT_TYPE + COMMA_SEP
            + Constants.ASSET_APPROVED_BY + TEXT_TYPE + COMMA_SEP
            + Constants.ASSET_APPROVED_DATE + TEXT_TYPE +
            ")";



    public static final String CREATE_REASON_MASTER_TABLE="CREATE TABLE "
            + Constants.REASON_MASTER_TABLE + "("
            + Constants.REASON_MASTER_ID + TEXT_TYPE + COMMA_SEP
            + Constants.REASON_MASTER_NAME + TEXT_TYPE + COMMA_SEP
            + Constants.REASON_MASTER_CREATED_BY + TEXT_TYPE + COMMA_SEP
            + Constants.REASON_MASTER_CREATED_DATE + TEXT_TYPE +
            ")";

    /*
     * Falco JK Create Statements Ends Here
     * */

    /*
    * NOTE : Falco JK Drop Statements
    * */
    public static final String DROP_PACKING_TICKET_TABLE = "DROP TABLE IF EXISTS " +Constants.PACKING_TICKET_TABLE;
    public static final String DROP_PRODUCTION_TICKET_TABLE = "DROP TABLE IF EXISTS " +Constants.PRODUCTION_TICKET_TABLE;
    public static final String DROP_DATE_WISE_QUANTITY_PACKING_TABLE = "DROP TABLE IF EXISTS " +Constants.DATE_WISE_QUANTITY_PACKING_TABLE;
    public static final String DROP_PRODUCTION_TICKET_DETAILS_TABLE = "DROP TABLE IF EXISTS " +Constants.PRODUCTION_TICKET_DETAILS_TABLE;
    public static final String DROP_PRODUCTION_TICKET_MULTIPLE_SKU_TABLE = "DROP TABLE IF EXISTS " +Constants.PRODUCTION_TICKET_MULTIPLE_SKU_TABLE;
    public static final String DROP_WIP_TABLE = "DROP TABLE IF EXISTS " +Constants.WIP_TABLE;
    public static final String DROP_TICKET_EQUIPMENT_TABLE = "DROP TABLE IF EXISTS " +Constants.TICKET_EQUIPMENT_TABLE;
    public static final String DROP_SAVE_DOWN_TIME_TABLE = "DROP TABLE IF EXISTS " +Constants.SAVE_DOWN_TIME_TABLE;
    public static final String DROP_REASON_MASTER_TABLE = "DROP TABLE IF EXISTS " +Constants.REASON_MASTER_TABLE;
    public static final String DROP_REASON_EQUIPMENT_MASTER_TABLE = "DROP TABLE IF EXISTS " +Constants.EQUPMENT_MASTER_TABLE;
    public static final String DROP_REASON_MASTER_ASSET_MASTER_TABLE = "DROP TABLE IF EXISTS " +Constants.ASSET_NO_MASTER_TABLE;
    public static final String DROP_REASON_MASTER_OPERATION_LIST = "DROP TABLE IF EXISTS " +Constants.TICKET_WISE_OPERATION_LIST;

    /*
     * Falco JK Drop Statements Ends Here
     * */
}
